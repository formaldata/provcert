# ProvCert

A Coq specification and formalization of K-algebras, i.e. extensions of
the relational algebra to encompass provenance of data


## License

This code is released under the terms of the LGPLv3 license; see LICENSE for details.


## Requirements
This library compiles with menhir and Coq-8.8.2. We recommend to install it using
opam as follows:
```
opam switch create 4.07.1
opam install -y -v menhir coq.8.8.2
```


## Compilation
```
./build_make
make
```


## Companion paper
To come.


## Organization of the code
The formalization of provenance is contained in the "provenance" and the
"data/proof_of_concept" directories.

### provenance/KAlgebra.v
Formalization of the K-relations and of the semantics of the
provenance-aware relational algebra extended with aggregates.
Instantiation of the annotations with natural integers. Proof of the
theorem of adequacy between Datacert semantics and ours.

### provenance/SemiRing.v
Formalization of semirings and some instantiations: natural integers,
tropical integers and Booleans.

### provenance/SemiModule.v
Formalization of semimodules and two instantiations: natural integers
and Booleans.

### provenance/Bool_fun.v
Formalization of Boolean functions as a semiring

### provenance/FiniteMap.v and provenance/FiniteMapImpl.v
Formalization of finite maps. Proposition of instantiation for the
finite map.

### provenance/Pol.v
Formalization of polynomials and proof that polynomials are a semiring.

### provenance/Facts.v
Several simple lemmas used in the differents files.

### data/proof_of_concept/KAlgebraproof.v
Proposition of a instantiation of our semantics.

### data/proof_of_concept/Example.v
Examples.

### Other directories
The other directories and files contain the implementation of the
Datacert library.
