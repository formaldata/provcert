(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import NArith FiniteSet FiniteBag.

Section Sec.

Hypothesis A : Type.
Hypothesis FA : Feset.Rcd A.
Hypothesis BA : Febag.Rcd A.

Inductive collection : Type :=
  | Fset : forall (s : Feset.set FA), collection
  | Bag : forall (s : Febag.bag BA), collection. 

Inductive flag : Type :=
  | FlagSet
  | FlatBag
  | FlagDefSetPrio
  | FlagDefBagPrio.

Definition to_set c :=
  match c with
    | Fset _ => c
    | Bag b => Fset (Feset.mk_set FA (Febag.elements BA b))
  end.

Definition to_bag c :=
  match c with
    | Fset s => Bag (Febag.mk_bag BA (Feset.elements FA s))
    | Bag _ => b
  end.

End Sec.
