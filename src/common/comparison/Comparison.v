(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Bool List String Ascii Sorted SetoidList.

Require Import BasicFacts ListFacts Mem NArith ListPermut.

Lemma compare_eq_true :
  forall c, match c with Eq => true | _ => false end = true <-> c = Eq.
Proof.
intro c; case c; split; intro H.
- apply refl_equal.
- apply refl_equal.
- discriminate H.
- discriminate H.
- discriminate H.
- discriminate H.
Qed.

Lemma match_compare_eq :
  forall (A : Type) (a1 a2 a3 a1' a2' a3' : A) c1 c2, 
    a1 = a1' -> a2 = a2' -> a3 = a3' -> c1 = c2 ->
    match c1 with 
      | Eq => a1
      | Lt => a2
      | Gt => a3
    end =  
    match c2 with 
      | Eq => a1'
      | Lt => a2'
      | Gt => a3'
    end.
Proof.
intros; subst; trivial.
Qed.

Section BuildPair.

Hypothesis A B : Type.
Hypothesis compareA : A -> A -> comparison.
Hypothesis compareB : B -> B -> comparison.

(** How to build a comparison function [compareAB] over the pairs [(A * B)] from a comparison function [compareA] over [A], and a comparison function [compareB] over [B]. *)
Definition compareAB (ab1 ab2 : A * B) : comparison :=
  match ab1, ab2 with
  | (a1, b1), (a2, b2) =>
     match compareA a1 a2 with
     | Eq => compareB b1 b2
     | Lt => Lt
     | Gt => Gt
     end
  end.

Lemma compareAB_unfold :
  forall ab1 ab2, compareAB ab1 ab2 =
  match ab1, ab2 with
  | (a1, b1), (a2, b2) =>
     match compareA a1 a2 with
     | Eq => compareB b1 b2
     | Lt => Lt
     | Gt => Gt
     end
  end.
Proof.
intros ab1 ab2; case ab1; case ab2; intros; apply refl_equal.
Qed.

Lemma compareAB_eq_bool_ok :
  forall a1 b1 a2 b2, 
    match compareA a1 a2 with Eq => a1 = a2 | _ => ~ a1 = a2 end ->
    match compareB b1 b2 with Eq => b1 = b2 | _ => ~ b1 = b2 end ->
    match compareAB (a1, b1) (a2, b2) with 
      | Eq => (a1, b1) = (a2, b2) 
      | _ => ~ (a1, b1) = (a2, b2) 
    end.
Proof.
intros a1 b1 a2 b2; unfold compareAB.
case (compareA a1 a2).
intro; subst a2.
case (compareB b1 b2).
intro; subst b2; apply refl_equal.
intros Hb H; apply Hb; injection H; exact (fun h => h).
intros Hb H; apply Hb; injection H; exact (fun h => h).
intros Ha _ H; apply Ha; injection H; exact (fun _ h => h).
intros Ha _ H; apply Ha; injection H; exact (fun _ h => h).
Qed.

Lemma compareAB_eq_refl : forall a b, compareA a a = Eq -> compareB b b = Eq -> compareAB (a, b) (a, b) = Eq.
Proof.
intros a b Ha Hb; unfold compareAB; rewrite Ha, Hb; apply refl_equal.
Qed.

Lemma compareAB_eq_sym : 
  forall a1 b1 a2 b2, 
    (compareA a1 a2 = Eq -> compareA a2 a1 = Eq) ->
    (compareB b1 b2 = Eq -> compareB b2 b1 = Eq) ->
    compareAB (a1, b1) (a2, b2) = Eq -> compareAB (a2, b2) (a1, b1) = Eq.
Proof.
intros a1 b1 a2 b2; unfold compareAB.
case (compareA a1 a2).
intro HA; rewrite HA; [ | apply refl_equal].
exact (fun h => h).
intros _ _ Abs; discriminate Abs.
intros _ _ Abs; discriminate Abs.
Qed.

Lemma compareAB_eq_trans : 
  forall a1 b1 a2 b2 a3 b3,
   (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
  (compareB b1 b2 = Eq -> compareB b2 b3 = Eq -> compareB b1 b3 = Eq) ->
  compareAB (a1, b1) (a2, b2) = Eq -> compareAB (a2, b2) (a3, b3) = Eq -> compareAB (a1, b1) (a3, b3) = Eq.
Proof.
intros a1 b1 a2 b2 a3 b3 HA HB; simpl.
case_eq (compareA a1 a2); intro A12; [ | intro Abs; discriminate Abs | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ | intro Abs; discriminate Abs | intro Abs; discriminate Abs].
intro B23; rewrite (HA A12 A23), (HB B12 B23); apply refl_equal.
Qed.

Lemma compareAB_le_lt_trans :
    forall a1 b1 a2 b2 a3 b3,
    (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
    (compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) -> 
    (compareB b1 b2 = Eq -> compareB b2 b3 = Lt -> compareB b1 b3 = Lt) -> 
    compareAB (a1, b1) (a2, b2) = Eq -> compareAB (a2, b2) (a3, b3) = Lt -> compareAB (a1, b1) (a3, b3) = Lt.
Proof.
intros a1 b1 a2 b2 a3 b3 HA KA HB; simpl.
case_eq (compareA a1 a2); intro A12; [ | intro Abs; discriminate Abs | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ |  | intro Abs; discriminate Abs].
intro B23; rewrite (HA A12 A23), (HB B12 B23); apply refl_equal.
intros _; rewrite (KA A12 A23); apply refl_equal.
Qed.

Lemma compareAB_lt_le_trans :
    forall a1 b1 a2 b2 a3 b3,
    (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
    (compareA a1 a2 = Lt -> compareA a2 a3 = Eq -> compareA a1 a3 = Lt) -> 
    (compareB b1 b2 = Lt -> compareB b2 b3 = Eq -> compareB b1 b3 = Lt) -> 
    compareAB (a1, b1) (a2, b2) = Lt -> compareAB (a2, b2) (a3, b3) = Eq -> compareAB (a1, b1) (a3, b3) = Lt.
Proof.
intros a1 b1 a2 b2 a3 b3 HA KA HB; simpl.
case_eq (compareA a1 a2); intro A12; [ | | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ | intro Abs; discriminate Abs  | intro Abs; discriminate Abs].
intro B23; rewrite (HA A12 A23), (HB B12 B23); apply refl_equal.
intros _; case_eq (compareA a2 a3); intro A23; [ | intro Abs; discriminate Abs  | intro Abs; discriminate Abs].
intro B23; rewrite (KA A12 A23); apply refl_equal.
Qed.

Lemma compareAB_lt_trans :
    forall a1 b1 a2 b2 a3 b3,
    (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
    (compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) -> 
    (compareA a1 a2 = Lt -> compareA a2 a3 = Eq -> compareA a1 a3 = Lt) -> 
    (compareA a1 a2 = Lt -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) -> 
    (compareB b1 b2 = Lt -> compareB b2 b3 = Lt -> compareB b1 b3 = Lt) -> 
    compareAB (a1, b1) (a2, b2) = Lt -> compareAB (a2, b2) (a3, b3) = Lt -> compareAB (a1, b1) (a3, b3) = Lt.
Proof.
intros a1 b1 a2 b2 a3 b3 HA KA JA LA HB; simpl.
case_eq (compareA a1 a2); intro A12; [ | | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ |  | intro Abs; discriminate Abs].
intro B23; rewrite (HA A12 A23), (HB B12 B23); apply refl_equal.
intros _; rewrite (KA A12 A23); apply refl_equal.
intros _; case_eq (compareA a2 a3); intro A23; [ |  | intro Abs; discriminate Abs].
intro B23; rewrite (JA A12 A23); apply refl_equal.
intros _; rewrite (LA A12 A23); apply refl_equal.
Qed.

Lemma compareAB_lt_gt : 
    forall a1 b1 a2 b2, 
    (compareA a1 a2 = CompOpp (compareA a2 a1)) ->
    (compareB b1 b2 = CompOpp (compareB b2 b1)) ->
    compareAB (a1, b1) (a2, b2) = CompOpp (compareAB (a2, b2) (a1, b1)).
Proof.
intros a1 b1 a2 b2 HA HB; unfold compareAB.
rewrite HA, HB.
case (compareA a2 a1); simpl; apply refl_equal.
Qed.

End BuildPair.

Section BuildPairLeft.

Hypothesis A B : Type.
Hypothesis compareA : A -> A -> comparison.

(** How to build a comparison function [compareAl] over the pairs [(A * B)] from a comparison function [compareA] over [A]. *)
Definition compareAl (ab1 ab2 : A * B) : comparison :=
  match ab1, ab2 with
  | (a1, b1), (a2, b2) => compareA a1 a2
  end.

Lemma compareAl_eq_refl : forall a b, compareA a a = Eq -> compareAl (a, b) (a, b) = Eq.
Proof.
intros a b Ha; unfold compareAl; rewrite Ha; apply refl_equal.
Qed.

Lemma compareAl_eq_sym :
  forall a1 b1 a2 b2,
    (compareA a1 a2 = Eq -> compareA a2 a1 = Eq) ->
    compareAl (a1, b1) (a2, b2) = Eq -> compareAl (a2, b2) (a1, b1) = Eq.
Proof.
intros a1 b1 a2 b2; unfold compareAl.
case (compareA a1 a2).
intro HA; rewrite HA; [ | apply refl_equal].
exact (fun h => h).
discriminate.
discriminate.
Qed.

Lemma compareAl_eq_trans :
  forall a1 b1 a2 b2 a3 b3,
   (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
  compareAl (a1, b1) (a2, b2) = Eq -> compareAl (a2, b2) (a3, b3) = Eq -> compareAl (a1, b1) (a3, b3) = Eq.
Proof.
intros a1 b1 a2 b2 a3 b3 HA HB; simpl.
case_eq (compareA a1 a2); intro A12; auto.
Qed.

Lemma compareAl_le_lt_trans :
    forall a1 b1 a2 b2 a3 b3,
    (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
    (compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
    compareAl (a1, b1) (a2, b2) = Eq -> compareAl (a2, b2) (a3, b3) = Lt -> compareAl (a1, b1) (a3, b3) = Lt.
Proof.
intros a1 b1 a2 b2 a3 b3 HA KA; simpl.
case_eq (compareA a1 a2); intro A12; [ | intro Abs; discriminate Abs | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ |  | intro Abs; discriminate Abs].
discriminate.
intros _; rewrite (KA A12 A23); apply refl_equal.
Qed.

Lemma compareAl_lt_le_trans :
    forall a1 b1 a2 b2 a3 b3,
    (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
    (compareA a1 a2 = Lt -> compareA a2 a3 = Eq -> compareA a1 a3 = Lt) ->
    compareAl (a1, b1) (a2, b2) = Lt -> compareAl (a2, b2) (a3, b3) = Eq -> compareAl (a1, b1) (a3, b3) = Lt.
Proof.
intros a1 b1 a2 b2 a3 b3 HA KA; simpl.
case_eq (compareA a1 a2); intro A12; [ | | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ | intro Abs; discriminate Abs  | intro Abs; discriminate Abs].
discriminate.
intros _; case_eq (compareA a2 a3); intro A23; [ | intro Abs; discriminate Abs  | intro Abs; discriminate Abs].
intro B23; rewrite (KA A12 A23); apply refl_equal.
Qed.

Lemma compareAl_lt_trans :
    forall a1 b1 a2 b2 a3 b3,
    (compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
    (compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
    (compareA a1 a2 = Lt -> compareA a2 a3 = Eq -> compareA a1 a3 = Lt) ->
    (compareA a1 a2 = Lt -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
    compareAl (a1, b1) (a2, b2) = Lt -> compareAl (a2, b2) (a3, b3) = Lt -> compareAl (a1, b1) (a3, b3) = Lt.
Proof.
intros a1 b1 a2 b2 a3 b3 HA KA JA LA; simpl.
case_eq (compareA a1 a2); intro A12; [ | | intro Abs; discriminate Abs].
intro B12; case_eq (compareA a2 a3); intro A23; [ |  | intro Abs; discriminate Abs].
discriminate.
intros _; rewrite (KA A12 A23); apply refl_equal.
intros _; case_eq (compareA a2 a3); intro A23; [ |  | intro Abs; discriminate Abs].
intro B23; rewrite (JA A12 A23); apply refl_equal.
intros _; rewrite (LA A12 A23); apply refl_equal.
Qed.

Lemma compareAl_lt_gt :
    forall a1 b1 a2 b2,
    (compareA a1 a2 = CompOpp (compareA a2 a1)) ->
    compareAl (a1, b1) (a2, b2) = CompOpp (compareAl (a2, b2) (a1, b1)).
Proof.
intros a1 b1 a2 b2 HA; unfold compareAl.
rewrite HA.
case (compareA a2 a1); simpl; apply refl_equal.
Qed.

End BuildPairLeft.

Section BuildList.
Require Import List.

Hypothesis A : Type.
Hypothesis compareA : A -> A -> comparison.

(** How to build a lexicographic comparison function [comparelA] over the lists [(list A)] from a comparison function [compareA] over [A]. *)
Fixpoint comparelA l1 l2 :=
  match l1, l2 with
  | nil, nil => Eq
  | nil, _ :: _ => Lt
  | _ :: _, nil => Gt
  | a1 :: l1, a2 :: l2 => 
      match compareA a1 a2 with
      | Eq => comparelA l1 l2
      | Lt => Lt
      | Gt => Gt
      end
    end.

Lemma comparelA_unfold :
  forall l1 l2, comparelA l1 l2 =
  match l1, l2 with
  | nil, nil => Eq
  | nil, _ :: _ => Lt
  | _ :: _, nil => Gt
  | a1 :: l1, a2 :: l2 => 
      match compareA a1 a2 with
      | Eq => comparelA l1 l2
      | Lt => Lt
      | Gt => Gt
      end
    end.
Proof.
intros l1 l2; case l1; case l2; intros; apply refl_equal.
Qed.

Lemma comparelA_rev_eq :
  forall l1 l2, comparelA l1 l2 = Eq -> comparelA (rev l1) (rev l2) = Eq.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] H; try discriminate H.
- trivial.
- simpl in H.
  case_eq (compareA a1 a2); intro Ha; rewrite Ha in H; try discriminate H.
  assert (IH := IHl1 _ H).
  simpl.
  set (rl1 := rev l1) in *; clearbody rl1.
  set (rl2 := rev l2) in *; clearbody rl2.
  clear IHl1.
  revert rl2 IH; induction rl1 as [ | b1 rl1]; intros [ | b2 rl2] IH; simpl; try discriminate IH.
  + rewrite Ha; trivial.
  + simpl in IH.
    case_eq (compareA b1 b2); intro Hb; rewrite Hb in IH; try discriminate IH.
    apply IHrl1; assumption.
Qed.

Lemma comparelA_eq_bool_ok :
  forall l1 l2, (forall a1 a2, In a1 l1 -> In a2 l2 -> 
                               match compareA a1 a2 with
                                 | Eq => a1 = a2
                                 | _ => a1 <> a2
                               end) ->
   match comparelA l1 l2 with
   | Eq => l1 = l2
   | Lt => l1 <> l2
   | Gt => l1 <> l2
   end.
Proof.
- intros l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] Hl; simpl.
  + apply refl_equal.
  + discriminate.
  + discriminate.
  + case_eq (compareA a1 a2); intro Ha.
    * generalize (Hl a1 a2 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))); 
      rewrite Ha; intro; subst a2.
      {
        generalize (IHl1 l2 (fun x y hx hy =>  Hl x y (or_intror _ hx) (or_intror _ hy))).
        case (comparelA l1 l2).
        - apply f_equal.
        - intros Kl H; apply Kl.
          injection H; exact (fun h => h).
        - intros Kl H; apply Kl.
          injection H; exact (fun h => h).
      }
    * generalize (Hl a1 a2 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))); 
      rewrite Ha; clear Ha; intro Ha.
      intro Kl; apply Ha.
      injection Kl; exact (fun _ h => h).
    * generalize (Hl a1 a2 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))); 
      rewrite Ha; clear Ha; intro Ha.
      intro Kl; apply Ha.
      injection Kl; exact (fun _ h => h).
Qed.

Lemma comparelA_eq_refl : 
  forall l, (forall a, In a l -> compareA a a = Eq) -> comparelA l l = Eq.
Proof.
intro l; induction l as [ | a l]; intros H; simpl.
apply refl_equal.
rewrite (H a (or_introl _ (refl_equal _))).
apply (IHl (fun s h => H s (or_intror _  h))).
Qed.

Lemma comparelA_eq_refl_alt : 
  forall l1, (forall a, In a l1 -> compareA a a = Eq) -> 
             forall l2, l1 = l2 -> comparelA l1 l2 = Eq.
Proof.
intros l1 Hl1 l2 H; subst l2; apply comparelA_eq_refl; assumption.
Qed.

Lemma comparelA_eq_app :
  forall l1 l1' l2 l2', 
    comparelA l1 l2 = Eq -> comparelA l1' l2' = Eq -> comparelA (l1 ++ l1') (l2 ++ l2') = Eq.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros l1' [ | a2 l2] l2' H H'; simpl in H; 
simpl; trivial; try discriminate.
revert H; case (compareA a1 a2); try discriminate.
intro; apply IHl1; trivial.
Qed.

Lemma comparelA_eq_filter :
  forall l1 l2 f, 
    (forall a1 a2, In a1 l1 -> compareA a1 a2 = Eq -> f a1 = f a2) -> 
    comparelA l1 l2 = Eq -> comparelA (filter f l1) (filter f l2) = Eq.
Proof.
intros l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] f Hf Hl;
  try discriminate Hl; trivial.
simpl in Hl.
case_eq (compareA a1 a2); intro Ha; rewrite Ha in Hl; try discriminate Hl.
simpl.
rewrite <- (Hf a1 a2 (or_introl _ (refl_equal _)) Ha).
case (f a1).
- simpl; rewrite Ha; apply IHl1; trivial.
  do 3 intro; apply Hf; right; assumption.
- apply IHl1; trivial.
  do 3 intro; apply Hf; right; assumption.
Qed.

Lemma comparelA_eq_trans : 
  forall l1 l2 l3, 
  (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
  comparelA l1 l2 = Eq -> comparelA l2 l3 = Eq -> comparelA l1 l3 = Eq.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] [ | a3 l3] H; simpl; trivial.
intro Abs; discriminate Abs.
generalize (H a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))).
case (compareA a1 a2).
case (compareA a2 a3).
intro Ha13; rewrite Ha13; trivial.
apply IHl1; do 6 intro; apply H; right; assumption.
intros _ _ Abs; discriminate Abs.
intros _ _ Abs; discriminate Abs.
intros _ Abs; discriminate Abs.
intros _ Abs; discriminate Abs.
Qed.

Lemma comparelA_le_lt_trans :
  forall l1 l2 l3, 
  (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
 (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
  comparelA l1 l2 = Eq -> comparelA l2 l3 = Lt -> comparelA l1 l3 = Lt.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] [ | a3 l3] H1 H2; simpl; trivial; try discriminate.
generalize (H1 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)))
(H2 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))).
case (compareA a1 a2).
case (compareA a2 a3).
intros Ha13 _; rewrite Ha13; trivial.
apply IHl1.
do 6 intro; apply H1; right; assumption.
do 6 intro; apply H2; right; assumption.
intros _ Ha13; rewrite Ha13; trivial.
intros _ _ _ Abs; discriminate Abs.
intros _ _ Abs; discriminate Abs.
intros _ _ Abs; discriminate Abs.
Qed.

Lemma comparelA_lt_le_trans :
  forall l1 l2 l3, 
  (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
 (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Lt -> compareA a2 a3 = Eq -> compareA a1 a3 = Lt) ->
  comparelA l1 l2 = Lt -> comparelA l2 l3 = Eq -> comparelA l1 l3 = Lt.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] [ | a3 l3] H1 H2; simpl; trivial; try discriminate.
generalize (H1 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)))
(H2 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))).
case (compareA a1 a2).
case (compareA a2 a3).
intros Ha13 _; rewrite Ha13; trivial.
apply IHl1.
do 6 intro; apply H1; right; assumption.
do 6 intro; apply H2; right; assumption.
intros _ _ _ Abs; discriminate Abs.
intros _ _ _ Abs; discriminate Abs.
case (compareA a2 a3).
intros _ Ha13; rewrite Ha13; trivial.
intros _ _ _ Abs; discriminate Abs.
intros _ _ _ Abs; discriminate Abs.
intros _ _ Abs; discriminate Abs.
Qed.

Lemma comparelA_lt_trans :
  forall l1 l2 l3, 
  (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
  (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
 (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Lt -> compareA a2 a3 = Eq -> compareA a1 a3 = Lt) ->
 (forall a1 a2 a3, In a1 l1 -> In a2 l2 -> In a3 l3 ->   
     compareA a1 a2 = Lt -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
  comparelA l1 l2 = Lt -> comparelA l2 l3 = Lt -> comparelA l1 l3 = Lt.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] [ | a3 l3] H1 H2 H3 H4; simpl; trivial; try discriminate.
generalize (H1 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)))
(H2 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)))
(H3 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)))
(H4 a1 a2 a3 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))).
case (compareA a1 a2).
case (compareA a2 a3).
intros Ha13 _ _ _; rewrite Ha13; trivial.
apply IHl1.
do 6 intro; apply H1; right; assumption.
do 6 intro; apply H2; right; assumption.
do 6 intro; apply H3; right; assumption.
do 6 intro; apply H4; right; assumption.
intros _ Ha13 _ _; rewrite Ha13; trivial.
intros _ _ _ _ _ Abs; discriminate Abs.
case (compareA a2 a3).
intros _ _ Ha13 _; rewrite Ha13; trivial.
intros _ _ _ Ha13; rewrite Ha13; trivial.
intros _ _ _ _ _ Abs; discriminate Abs.
intros _ _ _ _ Abs; discriminate Abs.
Qed.

Lemma comparelA_lt_gt : 
    forall l1 l2, (forall a1 a2, In a1 l1 -> In a2 l2 ->  compareA a1 a2 = CompOpp (compareA a2 a1)) ->
    comparelA l1 l2 = CompOpp (comparelA l2 l1).
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] H; simpl; try apply refl_equal.
rewrite (H a1 a2 (or_introl _ (refl_equal _)) (or_introl _ (refl_equal _))).
rewrite (IHl1 l2 (fun a1 a2 h1 h2 => H a1 a2 (or_intror _ h1) (or_intror _ h2))).
case (compareA a2 a1); simpl; apply refl_equal.
Qed.

Lemma comparelA_gt_eq_trans :
  forall l1 l2 l3, 
  (forall a1 a2 a3, In a1 l3 -> In a2 l2 -> In a3 l1 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Eq -> compareA a1 a3 = Eq) ->
 (forall a1 a2 a3, In a1 l3 -> In a2 l2 -> In a3 l1 ->   
     compareA a1 a2 = Eq -> compareA a2 a3 = Lt -> compareA a1 a3 = Lt) ->
 (forall a1 a2 : A, In a1 l1 -> In a2 l3 -> compareA a1 a2 = CompOpp (compareA a2 a1)) ->
 (forall a1 a2 : A, In a1 l2 -> In a2 l3 -> compareA a1 a2 = CompOpp (compareA a2 a1)) ->
 (forall a1 a2 : A, In a1 l1 -> In a2 l2 -> compareA a1 a2 = CompOpp (compareA a2 a1)) ->
  comparelA l1 l2 = Gt -> comparelA l2 l3 = Eq -> comparelA l1 l3 = Gt.
Proof.
intros l1 l2 l3 H1 H2 H3 H4 H5.
rewrite (comparelA_lt_gt l1 l2), (comparelA_lt_gt l2 l3), (comparelA_lt_gt l1 l3), 3 CompOpp_iff.
- simpl.
  intros K1 K2; revert K2 K1.
  apply comparelA_le_lt_trans.
  + apply H1.
  + apply H2.
- assumption.
- assumption.
- assumption.
Qed.

Lemma comparelA_eq_length_eq :
  forall l1 l2, comparelA l1 l2 = Eq -> length l1 = length l2.
Proof.
intros l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] H; try (trivial || discriminate H).
simpl; apply f_equal; apply IHl1.
simpl in H.
destruct (compareA a1 a2); try (trivial || discriminate H).
Qed.

End BuildList.

Section BuildOption.

Hypothesis A : Type.
Hypothesis compareA : A -> A -> comparison.

Definition option_to_list (o : option A) :=
  match o with
  | Some x => x :: nil
  | None => nil
  end.

Definition compareoA o1 o2 := comparelA compareA (option_to_list o1) (option_to_list o2).

End BuildOption.

Lemma comparelA_comparelA_eq :
  forall (A : Type) (compareA : A -> A -> comparison) l1 l2,
    comparelA (comparelA compareA) l1 l2 = Eq ->
    comparelA compareA (flat_map (fun x : list A => x) l1) (flat_map (fun x : list A => x) l2) = Eq.
Proof.
intros A compareA l1; induction l1 as [ | x1 l1]; intros [ | x2 l2] H; simpl; try discriminate H; trivial.
simpl in H.
case_eq (comparelA compareA x1 x2); intro Hx; rewrite Hx in H; try discriminate H.
apply comparelA_eq_app; [ | apply IHl1]; assumption.
Qed.

Ltac comparelA_tac :=
  match goal with
    | |- comparelA ?comp ?a1 ?a2 = Eq ->
         comparelA ?comp ?a2 ?a3 = Eq ->
         comparelA ?comp ?a1 ?a3 = Eq =>
      apply comparelA_eq_trans; do 6 intro
    | |- comparelA ?comp ?a1 ?a2 = Eq ->
         comparelA ?comp ?a2 ?a3 = Lt ->
         comparelA ?comp ?a1 ?a3 = Lt =>
      apply comparelA_le_lt_trans; do 6 intro
    | |- comparelA ?comp ?a1 ?a2 = Lt ->
         comparelA ?comp ?a2 ?a3 = Eq ->
         comparelA ?comp ?a1 ?a3 = Lt =>
      apply comparelA_lt_le_trans; do 6 intro
    | |- comparelA ?comp ?a1 ?a2 = Lt ->
         comparelA ?comp ?a2 ?a3 = Lt ->
         comparelA ?comp ?a1 ?a3 = Lt =>
      apply comparelA_lt_trans; do 6 intro
    | |- comparelA ?comp ?a1 ?a2 = CompOpp (comparelA ?comp ?a2 ?a1) =>
      apply comparelA_lt_gt; do 4 intro
  end.

Ltac comparelA_eq_bool_ok_tac :=
  match goal with
    | |- match comparelA ?comp ?x1 ?x2 with
           | Eq => ?f ?x1 = ?f ?x2
           | Lt => ?f ?x1 <> ?f ?x2
           | Gt => ?f ?x1 <> ?f ?x2
         end =>
      let Aux := fresh "Aux" in
      assert (Aux : forall a1 a2,
                      List.In a1 x1 ->
                      List.In a2 x2 ->
                      match comp a1 a2 with
                        | Eq => a1 = a2
                        | Lt => a1 <> a2
                        | Gt => a1 <> a2
                      end); 
        [do 4 intro 
        | generalize (comparelA_eq_bool_ok _ x1 x2 Aux);
          case (comparelA comp x1 x2);
          [apply f_equal
          | let H1 := fresh in
            let H2 := fresh in
            intros H1 H2; apply H1; injection H2; exact (fun h => h)
          | let H1 := fresh in
            let H2 := fresh in
            intros H1 H2; apply H1; injection H2; exact (fun h => h)]]
  end.



Ltac compareAB_eq_bool_ok_tac :=
  match goal with
    | |- match compareAB ?comp1 ?comp2 (?a1,?b1) (?a2,?b2) with
           | Eq => ?f (?a1,?b1) = ?f (?a2,?b1)
           | Lt => ?f (?a1,?b1) <> ?f (?a2,?b1)
           | Gt => ?f (?a1,?b1) <> ?f (?a2,?b1)
         end => idtac
    | |- match compareAB ?comp1 ?comp2 ?x1 ?x2 with
           | Eq => ?f ?x1 = ?f ?x2
           | Lt => ?f ?x1 <> ?f ?x2
           | Gt => ?f ?x1 <> ?f ?x2
         end => 
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2]
    | |- match compareAB ?comp1 ?comp2 (?a1,?b1) (?a2,?b2) with
           | Eq => (?a1,?b1) = (?a2,?b1)
           | Lt => (?a1,?b1) <> (?a2,?b1)
           | Gt => (?a1,?b1) <> (?a2,?b1)
         end => apply compareAB_eq_bool_ok
    | |- match compareAB ?comp1 ?comp2 ?x1 ?x2 with
           | Eq => ?x1 = ?x2
           | Lt => ?x1 <> ?x2
           | Gt => ?x1 <> ?x2
         end => 
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2];
        apply compareAB_eq_bool_ok
  end.



