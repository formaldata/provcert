(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Bool List String Ascii Sorted SetoidList NArith.

Require Import BasicFacts ListFacts Mem ListPermut.

Require Export Comparison Oeset Oset.

Require Import List.


Lemma compare_eq_true :
  forall c, match c with Eq => true | _ => false end = true <-> c = Eq.
Proof.
intro c; case c; split; intro H.
- apply refl_equal.
- apply refl_equal.
- discriminate H.
- discriminate H.
- discriminate H.
- discriminate H.
Qed.

Section Group.

Hypothesis (A B : Type).
Hypothesis OA : Oeset.Rcd A.
Hypothesis (proj : B -> A).

Fixpoint group x ls :=
  match ls with
  | nil => (proj x, x :: nil) :: nil
  | (k, lk) :: ls =>
    match Oeset.compare OA k (proj x) with
    | Eq => (k, x :: lk) :: ls
    | _ => (k, lk) :: group x ls
    end
  end.

Lemma group_unfold :
  forall x ls,
    group x ls =
  match ls with
  | nil => (proj x, x :: nil) :: nil
  | (k, lk) :: ls =>
    match Oeset.compare OA k (proj x) with
    | Eq => (k, x :: lk) :: ls
    | _ => (k, lk) :: group x ls
    end
  end.
Proof.
intros x ls.
case_eq ls; intros; apply refl_equal.
Qed.

Lemma keys_of_group : 
  forall x ls,
    map fst (group x ls) =
    if Oeset.mem_bool OA (proj x) (map fst ls) then map fst ls else  map fst ls ++ (proj x) :: nil.
Proof.
intros x ls; induction ls as [ | [k1 lk1] ls]; simpl; [apply refl_equal | ].
- unfold Oeset.eq_bool; rewrite Oeset.compare_lt_gt.
  case (Oeset.compare OA (proj x) k1); simpl; trivial.
  + destruct (Oeset.mem_bool OA (proj x) (map fst ls)); rewrite IHls; trivial.
  + destruct (Oeset.mem_bool OA (proj x) (map fst ls)); rewrite IHls; trivial.
Qed.

Lemma all_diff_keys_of_group :
  forall x lg, all_diff_bool (Oeset.eq_bool OA) (map fst lg) = true ->
               all_diff_bool (Oeset.eq_bool OA) (map fst (group x lg)) = true.
Proof.
intros x lg H.
rewrite keys_of_group.
case_eq (Oeset.mem_bool OA (proj x) (map fst lg)); intro K; [assumption | ].
set (lk := (map fst lg)) in *; clearbody lk; clear lg.
induction lk as [ | k1 lk]; simpl; trivial.
rewrite all_diff_bool_unfold, Bool.andb_true_iff, negb_true_iff in H.
rewrite Oeset.mem_bool_unfold, Bool.orb_false_iff in K.
assert (IH := IHlk (proj2 H) (proj2 K)).
rewrite IH, Bool.andb_true_r.
rewrite Mem.mem_bool_app; simpl.
rewrite (proj1 H), Bool.orb_false_l, Bool.orb_false_r.
unfold Oeset.eq_bool.
rewrite Oeset.compare_lt_gt.
destruct (Oeset.compare OA (proj x) k1).
- destruct K as [K _]; discriminate K.
- simpl; destruct (lk ++ proj x :: nil); trivial.
- simpl; destruct (lk ++ proj x :: nil); trivial.
Qed.

Lemma group_app :
  forall x ls1 ls2,
    group x (ls1 ++ ls2) =
    if Oeset.mem_bool OA (proj x) (map fst ls1) 
    then (group x ls1) ++ ls2 else  ls1 ++ (group x ls2).
Proof.
intros x ls1; induction ls1 as [ | [k1 lk1] ls1]; intros ls2; simpl; [trivial | ].
assert (IH := IHls1 ls2).
unfold Oeset.eq_bool.
rewrite (Oeset.compare_lt_gt OA (proj x)).
case (Oeset.compare OA k1 (proj x)); simpl; [trivial | | ].
+ destruct (Oeset.mem_bool OA (proj x) (map fst ls1)); rewrite IH; trivial.
+ destruct (Oeset.mem_bool OA (proj x) (map fst ls1)); rewrite IH; trivial.
Qed.

Lemma insert_in_groups :
  forall t lg, all_diff_bool (Oeset.eq_bool OA) (map fst lg) = true ->
               group t lg =
               if Oeset.mem_bool OA (proj t) (map fst lg)
               then map (fun x => match x with 
                                    (k, lk) => 
                                    if Oeset.eq_bool OA (proj t) k
                                    then (k, t :: lk)
                                    else (k, lk)
                                  end) lg
               else lg ++ (proj t, t :: nil) :: nil.
Proof.
intros t lg; induction lg as [ | [k1 lk1] lg]; intros H.
- apply refl_equal.
- rewrite group_unfold, (map_unfold _ (_ :: _)), (Oeset.mem_bool_unfold _ _ (_ :: _)).
  simpl fst.
  rewrite Oeset.compare_lt_gt; simpl; unfold Oeset.eq_bool.
  case_eq (Oeset.compare OA (proj t) k1); intro Ht; simpl.
  + apply f_equal.
    assert (Hg : forall g, In g lg -> Oeset.eq_bool OA (proj t) (fst g) = false).
    {
      intros [k lk] Hk; simpl.
      simpl map in H; rewrite all_diff_bool_unfold, Bool.andb_true_iff in H.
      rewrite negb_true_iff in H.
      unfold Oeset.eq_bool; rewrite (Oeset.compare_eq_1 _ _ _ _ Ht).
      case_eq (Oeset.compare OA k1 k); intro Kk; trivial.
      rewrite <- (proj1 H); apply sym_eq.
      rewrite (Oeset.mem_bool_true_iff OA k1 (map fst lg)).
      exists k; split; [assumption | ].
      rewrite in_map_iff; eexists; split; [ | apply Hk]; trivial.
      }
    clear IHlg H; induction lg as [ | [k lk] lg]; [trivial | ].
    simpl; rewrite <- IHlg.
    * assert (H1 := Hg _ (or_introl _ (refl_equal _))); unfold Oeset.eq_bool in H1; simpl in H1.
      destruct (Oeset.compare OA (proj t) k); trivial.
      discriminate H1.
    * intros; apply Hg; right; trivial.
  + rewrite IHlg.
    * case_eq (Oeset.mem_bool OA (proj t) (map fst lg)); intro Kt; apply refl_equal.
    * simpl map in H; rewrite all_diff_bool_unfold, Bool.andb_true_iff in H.
      apply (proj2 H).
  + rewrite IHlg.
    * case_eq (Oeset.mem_bool OA (proj t) (map fst lg)); intro Kt; apply refl_equal.
    * simpl map in H; rewrite all_diff_bool_unfold, Bool.andb_true_iff in H.
      apply (proj2 H).
Qed.

Lemma insert_in_groups_weak :
  forall t lg, Oeset.mem_bool OA (proj t) (map fst lg) = false ->
               group t lg = lg ++ (proj t, t :: nil) :: nil.
Proof.
intros t lg; induction lg as [ | [k1 lk1] lg]; intros H.
- apply refl_equal.
- rewrite (map_unfold _ (_ :: _)), (Oeset.mem_bool_unfold _ _ (_ :: _)), Bool.orb_false_iff in H.
  destruct H as [H1 H2]; simpl fst in H1.
  rewrite group_unfold, Oeset.compare_lt_gt.
  destruct (Oeset.compare OA (proj t) k1); try discriminate H1; simpl;
    rewrite (IHlg H2); trivial.
Qed.

Lemma insert_in_one_group :
  forall t lg, Oeset.mem_bool OA (proj t) (map fst lg) = true ->
               exists lg1, exists lg2, exists k, exists lk,
                     lg = lg1 ++ (k, lk) :: lg2 /\
                     Oeset.mem_bool OA (proj t) (map fst lg1) = false /\
                     Oeset.eq_bool OA (proj t) k = true /\
                     group t lg = lg1 ++ (k, t :: lk) :: lg2.
Proof.
intros t lg Ht.
induction lg as [ | [k1 lk1] lg]; [discriminate Ht | ].
rewrite map_unfold, Oeset.mem_bool_unfold in Ht; simpl fst in Ht.
case_eq (Oeset.compare OA (proj t) k1); intro Kt.
- exists nil; exists lg; exists k1; exists lk1.
  split; [apply refl_equal | ].
  split; [apply refl_equal | ].
  split; [unfold Oeset.eq_bool; rewrite Kt; trivial | ].
  rewrite group_unfold, Oeset.compare_lt_gt, Kt; apply refl_equal.
- rewrite Kt in Ht; simpl in Ht.
  destruct (IHlg Ht) as [lg1 [lg2 [k [lk [H1 [H2 [H3 H4]]]]]]].
  exists ((k1, lk1) :: lg1); exists lg2; exists k; exists lk.
  split; [rewrite H1; simpl; apply refl_equal | ].
  split; [simpl; unfold Oeset.eq_bool; rewrite Kt, H2; trivial | ].
  split; [assumption | ].
  rewrite group_unfold, Oeset.compare_lt_gt, Kt, H1.
  simpl; rewrite H1 in H4; rewrite H4; trivial.
- rewrite Kt in Ht; simpl in Ht.
  destruct (IHlg Ht) as [lg1 [lg2 [k [lk [H1 [H2 [H3 H4]]]]]]].
  exists ((k1, lk1) :: lg1); exists lg2; exists k; exists lk.
  split; [rewrite H1; simpl; apply refl_equal | ].
  split; [simpl; unfold Oeset.eq_bool; rewrite Kt, H2; trivial | ].
  split; [assumption | ].
  rewrite group_unfold, Oeset.compare_lt_gt, Kt, H1.
  simpl; rewrite H1 in H4; rewrite H4; trivial.
Qed.

Lemma insert_in_one_group_alt :
  forall t lg1 lg2 k lk,  Oeset.mem_bool OA (proj t) (map fst lg1) = false ->
                          Oeset.eq_bool OA (proj t) k = true ->
                          group t (lg1 ++ (k, lk) :: lg2) = lg1 ++ (k, t :: lk) :: lg2.
Proof.
intros t lg1; induction lg1 as [ | [k1 lk1] lg1]; intros lg2 k lk H1 H2. 
- unfold Oeset.eq_bool in H2; rewrite compare_eq_true in H2; simpl.
  rewrite Oeset.compare_lt_gt, H2; apply refl_equal.
- rewrite map_unfold, Oeset.mem_bool_unfold, Bool.orb_false_iff in H1.
  destruct H1 as [H1 K1]; simpl in H1; simpl.
  rewrite Oeset.compare_lt_gt.
  destruct (Oeset.compare OA (proj t) k1); [discriminate H1 | | ]; simpl; 
    apply f_equal; apply IHlg1; trivial.
Qed.

Lemma group_eq_1 : 
  forall (OB : Oeset.Rcd B),
    (forall b1 b2, Oeset.compare OB b1 b2 = Eq -> Oeset.compare OA (proj b1) (proj b2) = Eq) ->
    forall x1 x2 lg, 
    Oeset.compare OB x1 x2 = Eq ->
    comparelA (compareAB (Oeset.compare OA) (comparelA (Oeset.compare OB)))
              (group x1 lg) (group x2 lg) = Eq.
Proof.
intros OB proj_eq x1 x2 lg Hx.
induction lg as [ | [k1 lk1] lg]; simpl.
- rewrite Hx, (proj_eq _ _ Hx); trivial.
- rewrite <- (Oeset.compare_eq_2 _ _ _ _ (proj_eq _ _ Hx)).
  case (Oeset.compare OA k1 (proj x1)).
  + simpl.
    rewrite Oeset.compare_eq_refl, Hx.
    rewrite 2 comparelA_eq_refl; trivial.
    * intros [k lk] _; apply compareAB_eq_refl; [apply Oeset.compare_eq_refl | ].
      apply comparelA_eq_refl; intros; apply Oeset.compare_eq_refl.
    * intros; apply Oeset.compare_eq_refl.
  + simpl; rewrite Oeset.compare_eq_refl, comparelA_eq_refl;
      [ | intros; apply Oeset.compare_eq_refl].
    apply IHlg.
  + simpl; rewrite Oeset.compare_eq_refl, comparelA_eq_refl;
      [ | intros; apply Oeset.compare_eq_refl].
    apply IHlg.
Qed.

End Group.

Lemma group_eq_0 :
  forall (A B : Type) (OA : Oeset.Rcd A) (proj1 proj2 : B -> A), 
    (forall t, proj1 t = proj2 t) ->
    forall x s, group OA proj1 x s = group OA proj2 x s.
Proof.
intros A B OA proj1 proj2 H x s; induction s as [ | [k1 lk1] s]; simpl.
- rewrite H; trivial.
- rewrite H, IHs; trivial.
Qed.

Lemma comparelA_fold_left_eq
      (A B : Type) (OA : Oeset.Rcd A) (OB : Oeset.Rcd B)
      (f : A -> B -> A)
      (Hf : forall x1 x2 y1 y2, Oeset.compare OA x1 x2 = Eq -> Oeset.compare OB y1 y2 = Eq -> Oeset.compare OA (f x1 y1) (f x2 y2) = Eq)
      l1 l2 acc
      (Hl : comparelA (Oeset.compare OB) l1 l2 = Eq) :
  Oeset.compare OA (fold_left f l1 acc) (fold_left f l2 acc) = Eq.
Proof.
  apply (Oeset.compare_eq_trans _ _ (fold_right (fun b a => f a b) acc (rev l1))).
  - apply (Oeset.fold_left_rev_right_eq _ OB).
    + intros; now apply Hf.
    + now apply Oeset.compare_eq_refl.
    + rewrite rev_involutive. apply comparelA_eq_refl. intros; now apply Oeset.compare_eq_refl.
  - apply Oeset.compare_eq_sym. apply (Oeset.fold_left_rev_right_eq _ OB).
    + intros; now apply Hf.
    + now apply Oeset.compare_eq_refl.
    + rewrite rev_involutive. now apply (Oeset.compare_eq_sym (Oeset.mk_list OB)).
Qed.


Lemma fold_left_rev_right_eq_oset
      (A B : Type) (OA : Oset.Rcd A) (OB : Oset.Rcd B)
      (f : B -> A -> A)
      (Hf : forall x1 x2 y1 y2, Oset.compare OB x1 x2 = Eq -> Oset.compare OA y1 y2 = Eq -> Oset.compare OA (f x1 y1) (f x2 y2) = Eq) :
  forall l1 l2 acc1 acc2,
    Oset.compare OA acc1 acc2 = Eq ->
    comparelA (Oset.compare OB) l1 (rev l2) = Eq ->
    Oset.compare OA (fold_left (fun a e => f e a) l1 acc2) (fold_right f acc1 l2) = Eq.
Proof.
  induction l1 as [ |x xs IHxs].
  - intros [ |y ys] acc1 acc2; simpl.
    + intros H _; now apply Oset.compare_eq_sym.
    + case_eq (rev ys ++ y :: nil); try discriminate.
      intro H. elim (snoc_not_nil _ _ _ H).
  - intros l' acc1 acc2 Hacc. destruct (list_nil_snoc l') as [H1|[z [zs H1]]]; subst l'.
    + discriminate.
    + simpl. rewrite rev_app_distr. simpl.
      case_eq (Oset.compare OB x z); try discriminate. intros Heq1 Heq2.
      rewrite fold_right_app. simpl.
      apply IHxs; trivial.
      apply Hf; trivial.
      now apply Oset.compare_eq_sym.
Qed.


Lemma comparelA_fold_left_eq_oset
      (A B : Type) (OA : Oset.Rcd A) (OB : Oset.Rcd B)
      (f : A -> B -> A)
      (Hf : forall x1 x2 y1 y2, Oset.compare OA x1 x2 = Eq -> Oset.compare OB y1 y2 = Eq -> Oset.compare OA (f x1 y1) (f x2 y2) = Eq)
      l1 l2 acc
      (Hl : comparelA (Oset.compare OB) l1 l2 = Eq) :
  Oset.compare OA (fold_left f l1 acc) (fold_left f l2 acc) = Eq.
Proof.
  apply (Oset.compare_eq_trans _ _ (fold_right (fun b a => f a b) acc (rev l1))).
  - apply (fold_left_rev_right_eq_oset _ OB).
    + intros; now apply Hf.
    + now apply Oset.compare_eq_refl.
    + rewrite rev_involutive. apply comparelA_eq_refl. intros; now apply Oset.compare_eq_refl.
  - apply Oset.compare_eq_sym. apply (fold_left_rev_right_eq_oset _ OB).
    + intros; now apply Hf.
    + now apply Oset.compare_eq_refl.
    + rewrite rev_involutive. now apply (Oset.compare_eq_sym (Oset.mk_list OB)).
Qed.

Ltac compareAB_tac :=
  match goal with
    | |- compareAB (Oset.compare ?OA) ?cB (?a,?b1) (?a,?b2) = Eq => 
         unfold compareAB; rewrite (Oset.compare_eq_refl OA)
      
    | |- compareAB ?OS1 ?OS2 (?a1,?b1) (?a2,?b2) = Eq ->
         compareAB ?OS1 ?OS2 (?a2,?b2) (?a3,?b3) = Eq ->
         compareAB ?OS1 ?OS2 (?a1,?b1) (?a3,?b3) = Eq =>
      apply compareAB_eq_trans

    | |- compareAB ?OS1 ?OS2 ?x1 ?x2 = Eq ->
         compareAB ?OS1 ?OS2 ?x2 ?x3 = Eq ->
         compareAB ?OS1 ?OS2 ?x1 ?x3 = Eq =>
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let a3 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      let b3 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2];
        destruct x3 as [a3 b3];
        apply compareAB_eq_trans

    | |- compareAB ?OS1 ?OS2 (?a1,?b1) (?a2,?b2) = Eq ->
         compareAB ?OS1 ?OS2 (?a2,?b2) (?a3,?b3) = Lt ->
         compareAB ?OS1 ?OS2 (?a1,?b1) (?a3,?b3) = Lt =>
      apply compareAB_le_lt_trans

    | |- compareAB ?OS1 ?OS2 ?x1 ?x2 = Eq ->
         compareAB ?OS1 ?OS2 ?x2 ?x3 = Lt ->
         compareAB ?OS1 ?OS2 ?x1 ?x3 = Lt =>
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let a3 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      let b3 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2];
        destruct x3 as [a3 b3];
        apply compareAB_le_lt_trans

    | |- compareAB ?OS1 ?OS2 (?a1,?b1) (?a2,?b2) = Lt ->
         compareAB ?OS1 ?OS2 (?a2,?b2) (?a3,?b3) = Eq ->
         compareAB ?OS1 ?OS2 (?a1,?b1) (?a3,?b3) = Lt =>
      apply compareAB_lt_le_trans

    | |- compareAB ?OS1 ?OS2 ?x1 ?x2 = Lt ->
         compareAB ?OS1 ?OS2 ?x2 ?x3 = Eq ->
         compareAB ?OS1 ?OS2 ?x1 ?x3 = Lt =>
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let a3 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      let b3 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2];
        destruct x3 as [a3 b3];
        apply compareAB_lt_le_trans

    | |- compareAB ?OS1 ?OS2 (?a1,?b1) (?a2,?b2) = Lt ->
         compareAB ?OS1 ?OS2 (?a2,?b2) (?a3,?b3) = Lt ->
         compareAB ?OS1 ?OS2 (?a1,?b1) (?a3,?b3) = Lt =>
      apply compareAB_lt_trans

    | |- compareAB ?OS1 ?OS2 ?x1 ?x2 = Lt ->
         compareAB ?OS1 ?OS2 ?x2 ?x3 = Lt ->
         compareAB ?OS1 ?OS2 ?x1 ?x3 = Lt =>
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let a3 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      let b3 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2];
        destruct x3 as [a3 b3];
        apply compareAB_lt_trans

    | |- compareAB ?OS1 ?OS2 (?a1,?b1) (?a2,?b2) = 
         CompOpp (compareAB ?OS1 ?OS2 (?a2,?b2) (?a1,?b1)) =>
      apply compareAB_lt_gt

    | |- compareAB ?OS1 ?OS2 ?x1 ?x2 = CompOpp (compareAB ?OS1 ?OS2 ?x2 ?x1) =>
      let a1 := fresh "a" in
      let a2 := fresh "a" in
      let b1 := fresh "b" in
      let b2 := fresh "b" in
      destruct x1 as [a1 b1];
        destruct x2 as [a2 b2];
        apply compareAB_lt_gt
  end.
