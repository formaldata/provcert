(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Bool List String Ascii Sorted SetoidList.

Require Import BasicFacts ListFacts Mem NArith ListPermut Comparison Oeset.

Module Oset.
Record Rcd (A : Type) : Type :=
  mk_R
    { 
      compare : A -> A -> comparison;
      eq_bool_ok : 
        forall a1 a2, match compare a1 a2 with Eq => a1 = a2 | _ => ~ a1 = a2 end;
      compare_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_gt : forall a1 a2, compare a1 a2 = CompOpp (compare a2 a1)
}.

Section Sec.

Hypothesis A : Type.
Hypothesis OA : Rcd A.

Lemma compare_eq_trans : 
   forall a1 a2 a3, compare OA a1 a2 = Eq -> compare OA a2 a3 = Eq -> compare OA a1 a3 = Eq.
Proof.
intros a1 a2 a3 H1.
generalize (eq_bool_ok OA a1 a2); rewrite H1.
intro; subst a2; exact (fun h => h).
Qed.

Lemma  compare_eq_lt_trans : 
  forall a1 a2 a3, compare OA a1 a2 = Eq -> compare OA a2 a3 = Lt -> compare OA a1 a3 = Lt.
Proof.
intros a1 a2 a3 H1.
generalize (eq_bool_ok OA a1 a2); rewrite H1.
intro; subst a2; exact (fun h => h).
Qed.

Lemma  compare_lt_eq_trans : 
  forall a1 a2 a3, compare OA a1 a2 = Lt -> compare OA a2 a3 = Eq -> compare OA a1 a3 = Lt.
intros a1 a2 a3 H1 H2.
generalize (eq_bool_ok OA a2 a3); rewrite H2.
intro; subst a3; exact H1.
Qed.

Definition oeset_of_oset : Oeset.Rcd A.
Proof.
apply Oeset.mk_R with (Oset.compare OA).
- do 3 intro; apply compare_eq_trans.
- do 3 intro; apply compare_eq_lt_trans.
- do 3 intro; apply compare_lt_eq_trans.
- do 3 intro; apply compare_lt_trans.
- do 2 intro; apply compare_lt_gt.
Defined.

Open Scope N.

Definition eq_bool := (fun x y => match compare OA x y with Eq => true | _ => false end).
Definition mem_bool a l := Mem.mem_bool eq_bool a l.
Definition all_diff_bool l := Mem.all_diff_bool eq_bool l.
Fixpoint nb_occ a l : N :=
  match l with
    | nil => 0
    | a1 :: l => (match compare OA a a1 with Eq => 1 | _ => 0 end) + nb_occ a l
  end.

Lemma mem_bool_unfold : 
  forall a l, 
    mem_bool a l = 
    match l with
      | nil => false
      | a1 :: l => match compare OA a a1 with Eq => true | _ => false end || mem_bool a l
    end.
Proof.
intros a l; case l; intros; apply refl_equal.
Qed.

Lemma all_diff_bool_unfold :
  forall a l, all_diff_bool (a :: l) = negb (mem_bool a l) && (all_diff_bool l).
Proof.
intros a l; unfold all_diff_bool, mem_bool.
apply Mem.all_diff_bool_unfold.
Qed.

Lemma nb_occ_unfold :
  forall a l, nb_occ a l = 
              match l with
                | nil => 0
                | a1 :: l => (match compare OA a a1 with Eq => 1 | _ => 0 end) + nb_occ a l
              end.                                                                                        
Proof.
intros a l; destruct l; apply refl_equal.
Qed.

Lemma compare_eq_refl :
  forall a, compare OA a a = Eq.
Proof.
apply (Oeset.compare_eq_refl oeset_of_oset).
Qed.

Lemma compare_eq_refl_alt :
  forall a1 a2, a1 = a2 -> compare OA a1 a2 = Eq.
Proof.
apply (Oeset.compare_eq_refl_alt oeset_of_oset).
Qed.

Lemma compare_eq_sym :
  forall a b, compare OA a b = Eq -> compare OA b a = Eq.
Proof.
apply (Oeset.compare_eq_sym oeset_of_oset).
Qed.

Lemma compare_eq_iff :
  forall a1 a2, compare OA a1 a2 = Eq <-> a1 = a2.
Proof.
intros a1 a2; split; intro H.
- generalize (eq_bool_ok OA a1 a2); rewrite H.
  exact (fun h => h).
- subst a2; apply (Oeset.compare_eq_refl oeset_of_oset).
Qed.

Lemma compare_eq_gt_trans :
  forall a b c, compare OA a b = Eq -> compare OA b c = Gt -> compare OA a c = Gt.
Proof.
apply (Oeset.compare_eq_gt_trans oeset_of_oset).
Qed.

Lemma compare_gt_eq_trans :
  forall a b c, compare OA a b = Gt -> compare OA b c = Eq -> compare OA a c = Gt.
Proof.
apply (Oeset.compare_gt_eq_trans oeset_of_oset).
Qed. 

Lemma compare_gt_trans :
  forall a b c, compare OA a b = Gt -> compare OA b c = Gt -> compare OA a c = Gt.
Proof.
apply (Oeset.compare_gt_trans oeset_of_oset).
Qed.

Lemma compare_eq_1 :
  forall a1 a2 b, compare OA a1 a2 = Eq -> compare OA a1 b = compare OA a2 b.
Proof.
apply (Oeset.compare_eq_1 oeset_of_oset).
Qed.

Lemma compare_eq_2 :
  forall a b1 b2, compare OA b1 b2 = Eq -> compare OA a b1 = compare OA a b2.
Proof.
apply (Oeset.compare_eq_2 oeset_of_oset).
Qed.

Lemma eq_bool_eq_1 :
  forall x1 x2 x, eq_bool x1 x2 = true -> eq_bool x1 x = eq_bool x2 x.
Proof.
apply (Oeset.eq_bool_eq_1 oeset_of_oset).
Qed.

Lemma eq_bool_eq_2 :
  forall x1 x2 x, eq_bool x1 x2 = true -> eq_bool x x1 = eq_bool x x2.
Proof.
apply (Oeset.eq_bool_eq_2 oeset_of_oset).
Qed.

Lemma mem_bool_eq_1 :
  forall a1 a2 l, compare OA a1 a2 = Eq -> mem_bool a1 l = mem_bool a2 l.
Proof.
apply (Oeset.mem_bool_eq_1 oeset_of_oset).
Qed.

Lemma mem_bool_eq_2 :
  forall a l1 l2, comparelA (compare OA) l1 l2 = Eq -> mem_bool a l1 = mem_bool a l2.
Proof.
apply (Oeset.mem_bool_eq_2 oeset_of_oset).
Qed.

Lemma nb_occ_eq_1 :
  forall a1 a2 l, compare OA a1 a2 = Eq -> nb_occ a1 l = nb_occ a2 l.
Proof.
apply (Oeset.nb_occ_eq_1 oeset_of_oset).
Qed.

Lemma nb_occ_eq_2 :
  forall a l1 l2, comparelA (compare OA) l1 l2 = Eq -> nb_occ a l1 = nb_occ a l2.
Proof.
apply (Oeset.nb_occ_eq_2 oeset_of_oset).
Qed.

Lemma Equivalence :
  RelationClasses.Equivalence (fun x y => eq_bool x y = true).
Proof.
apply (Oeset.Equivalence oeset_of_oset).
Qed.

Lemma StrictOrder : RelationClasses.StrictOrder (fun x y => compare OA x y = Lt).
Proof.
apply (Oeset.StrictOrder oeset_of_oset).
Qed.

Lemma Compat :
  Morphisms.Proper
   (Morphisms.respectful (fun x y  => eq_bool x y = true)
      (Morphisms.respectful (fun x y => eq_bool x y = true) iff))
   (fun x y => compare OA x y = Lt).
Proof.
apply (Oeset.Compat oeset_of_oset).
Qed.

Lemma eq_bool_true_iff :
  forall a1 a2, eq_bool a1 a2 = true <-> a1 = a2.
Proof.
intros a1 a2; unfold eq_bool.
rewrite compare_eq_true.
split; intro H.
- generalize (eq_bool_ok OA a1 a2); rewrite H.
  exact (fun h => h).
- subst a2; apply (Oeset.compare_eq_refl oeset_of_oset).
Qed.

Lemma eq_bool_true_compare_eq :
  forall x y, eq_bool x y = true <-> compare OA x y = Eq.
Proof.
apply (Oeset.eq_bool_true_compare_eq oeset_of_oset).
Qed.

Lemma eq_bool_refl : forall x, eq_bool x x = true.
Proof.
apply (Oeset.eq_bool_refl oeset_of_oset).
Qed.

Lemma eq_bool_sym : forall x y, eq_bool x y = eq_bool y x.
Proof.
apply (Oeset.eq_bool_sym oeset_of_oset).
Qed.

Lemma in_mem_bool :
  forall a l, In a l -> mem_bool a l = true.
Proof.
apply (Oeset.in_mem_bool oeset_of_oset).
Qed.

Lemma mem_bool_true_iff : 
  forall a l, mem_bool a l = true <-> In a l.
Proof.
intros a l; unfold mem_bool, eq_bool; rewrite mem_bool_true_iff; split.
- intros [a' [Ha H]].
  generalize (eq_bool_ok OA a a').
  rewrite compare_eq_true in Ha; rewrite Ha.
  intro; subst a'; exact H.
- intro H; exists a; split; [ | assumption].
  assert (Aux := Oeset.compare_eq_refl oeset_of_oset); simpl in Aux.
  rewrite Aux; apply refl_equal.
Qed.


Lemma mem_bool_app : 
  forall a l1 l2, mem_bool a (l1 ++ l2) = mem_bool a l1 || mem_bool a l2.
Proof.
apply (Oeset.mem_bool_app oeset_of_oset).
Qed.

Lemma mem_bool_rev :
  forall a l,  mem_bool a (rev l) = mem_bool a l.
Proof.
apply (Oeset.mem_bool_rev oeset_of_oset).
Qed.

Lemma mem_bool_id :
  forall l f, (forall x, mem_bool x l = true -> compare OA (f x) x = Eq) ->
                forall x, mem_bool x (map f l) = mem_bool x l.
Proof.
apply (Oeset.mem_bool_id oeset_of_oset).
Qed.

Lemma mem_bool_filter :
  forall f l a, mem_bool a (filter f l) = f a && mem_bool a l.
Proof.
refine (fun f l a => Oeset.mem_bool_filter oeset_of_oset f l _ a).
intros x1 x2 _ Hx; simpl in Hx; apply f_equal.
rewrite compare_eq_iff in Hx; apply Hx.
Qed.

Lemma mem_bool_filter_eq_2 :
  forall f1 f2 l, (forall x, mem_bool x l = true -> f1 x = f2 x) ->
                  forall x, mem_bool x (filter f1 l) = mem_bool x (filter f2 l).
Proof.
apply (Oeset.mem_bool_filter_eq_2 oeset_of_oset).
Qed.

Lemma not_mem_nb_occ : forall a l, mem_bool a l = false -> nb_occ a l = 0.
Proof.
apply (Oeset.not_mem_nb_occ oeset_of_oset).
Qed.

Lemma mem_nb_occ : forall a l, mem_bool a l = true -> nb_occ a l <> 0.
Proof.
apply (Oeset.mem_nb_occ oeset_of_oset).
Qed.

Lemma mem_nb_occ_pos : forall a l, mem_bool a l = true -> nb_occ a l > 0.
Proof.
apply (Oeset.mem_nb_occ_pos oeset_of_oset).
Qed.

Lemma nb_occ_mem : forall a l, nb_occ a l <> 0 -> mem_bool a l = true.
Proof.
apply (Oeset.nb_occ_mem oeset_of_oset).
Qed.

Lemma nb_occ_not_mem : forall a l, nb_occ a l = 0 -> mem_bool a l = false.
Proof.
apply (Oeset.nb_occ_not_mem oeset_of_oset).
Qed.

Lemma in_nb_occ : forall a l, In a l -> nb_occ a l <> 0.
Proof.
apply (Oeset.in_nb_occ oeset_of_oset).
Qed.

Lemma nb_occ_list_size :
  forall a l, nb_occ a l = 
              N.of_nat (list_size 
                          (fun x => match compare OA a x with Eq => 1%nat | _ => 0%nat end) l).
Proof.
apply (Oeset.nb_occ_list_size oeset_of_oset).
Qed.

Lemma nb_occ_app :
  forall a l1 l2, nb_occ a (l1 ++ l2) = nb_occ a l1 + nb_occ a l2.
Proof.
apply (Oeset.nb_occ_app oeset_of_oset).
Qed.

Lemma nb_occ_if :
  forall a (b : bool) l1 l2, 
    nb_occ a (if b then l1 else l2) = if b then nb_occ a l1 else nb_occ a l2.
Proof.
apply (Oeset.nb_occ_if oeset_of_oset).
Qed.

Lemma nb_occ_diff_0_pos : forall a l, nb_occ a l <> 0 <-> 0 < nb_occ a l.
Proof.
apply (Oeset.nb_occ_diff_0_pos oeset_of_oset).
Qed.

Lemma nb_occ_id :
  forall l f, (forall x, mem_bool x l = true -> compare OA (f x) x = Eq) ->
                forall x, nb_occ x (map f l) = nb_occ x l.
Proof.
apply (Oeset.nb_occ_id oeset_of_oset).
Qed.

Lemma nb_occ_filter :
  forall (f : A -> bool) l, 
    forall x, nb_occ x (filter f l) = if f x then nb_occ x l else 0.
Proof.
intros f l; apply (Oeset.nb_occ_filter oeset_of_oset f l).
intros x1 x2 _ Hx; apply f_equal.
simpl in Hx; rewrite compare_eq_iff in Hx; assumption.
Qed.

Lemma nb_occ_filter_alt :
  forall (f : A -> bool) l, 
    forall x, nb_occ x (filter f l) = (nb_occ x l) * (if f x then 1 else 0).
Proof.
intros f l; apply (Oeset.nb_occ_filter_alt oeset_of_oset f l).
intros x1 x2 _ Hx; apply f_equal.
simpl in Hx; rewrite compare_eq_iff in Hx; assumption.
Qed.

Lemma nb_occ_filter_eq :
  forall f1 f2 s1 s2, 
    (forall x, nb_occ x s1 = nb_occ x s2) ->
    (forall x, nb_occ x s1 >= 1 -> f1 x = f2 x) -> 
    forall x, nb_occ x (filter f1 s1) = nb_occ x (filter f2 s2).
Proof.
intros f1 f2 s1 s2 Hs Hf.
apply (Oeset.nb_occ_filter_eq oeset_of_oset).
- apply Hs.
- intros x1 x2 Hx1 Hx.
  simpl in Hx; rewrite compare_eq_iff in Hx; subst x2.
  apply Hf; apply Hx1.
Qed.

Lemma nb_occ_partition_1 :
  forall (f : A -> bool) l, 
    forall x, nb_occ x (fst (partition f l)) = if f x then nb_occ x l else 0.
Proof.
intros f l; apply (Oeset.nb_occ_partition_1 oeset_of_oset).
intros x1 x2 Hx1 Hx.
simpl in Hx; rewrite compare_eq_iff in Hx; subst x2; apply refl_equal.
Qed.

Lemma nb_occ_partition_2 :
  forall (f : A -> bool) l, 
    forall x, nb_occ x (snd (partition f l)) = if f x then 0 else nb_occ x l.
Proof.
intros f l; apply (Oeset.nb_occ_partition_2 oeset_of_oset).
intros x1 x2 Hx1 Hx.
simpl in Hx; rewrite compare_eq_iff in Hx; subst x2; apply refl_equal.
Qed.

Lemma all_diff_bool_ok : 
  forall l, all_diff_bool l = true <-> all_diff l.
Proof.
intro l; split; [apply (Oeset.all_diff_bool_ok oeset_of_oset) | ].
induction l as [ | a1 l]; intro H; [apply refl_equal | ].
rewrite all_diff_unfold in H; rewrite all_diff_bool_unfold.
rewrite (IHl (proj2 H)), Bool.Bool.andb_true_r.
case_eq (mem_bool a1 l); intro Ha1; [ | apply refl_equal].
apply False_rec; refine (proj1 H a1 _ (refl_equal _)).
rewrite mem_bool_true_iff in Ha1; assumption.
Qed.

Lemma all_diff_bool_nb_occ_mem :
  forall l, all_diff_bool l = true -> 
            forall a, nb_occ a l = if mem_bool a l then 1 else 0.
Proof.
apply (Oeset.all_diff_bool_nb_occ_mem oeset_of_oset).
Qed.

Lemma all_diff_bool_false :
  forall l, all_diff_bool l = false ->
            exists a, exists l1, exists l2, exists l3,
                      l = l1 ++ a :: l2 ++ a :: l3.
Proof.
intros l Hl; 
destruct (Oeset.all_diff_bool_false oeset_of_oset l Hl) as [a1 [a2 [l1 [l2 [l3 [H1 H2]]]]]].
simpl in H1; rewrite compare_eq_iff in H1; subst a2.
exists a1; exists l1; exists l2; exists l3; assumption.
Qed.

Lemma all_diff_bool_app_1 :
  forall l1 l2, all_diff_bool (l1 ++ l2) = true -> all_diff_bool l1 = true.
Proof.
intros l1 l2; apply Mem.all_diff_bool_app1.
Qed.

Lemma all_diff_bool_app_2 :
  forall l1 l2, all_diff_bool (l1 ++ l2) = true -> all_diff_bool l2 = true.
Proof.
intros l1 l2; apply Mem.all_diff_bool_app2.
Qed.

Lemma all_diff_bool_app :
  forall l1 l2, all_diff_bool (l1 ++ l2) = true ->
   forall a, mem_bool a l1 = true -> mem_bool a l2 = true -> False.
Proof.
intros l1 l2 H a Ha1 Ha2; 
  refine (Oeset.all_diff_bool_app oeset_of_oset l1 l2 H a a (compare_eq_refl _) Ha1 Ha2).
Qed.

Lemma all_diff_bool_app_iff :
  forall l1 l2, 
    (all_diff_bool l1 = true /\ all_diff_bool l2 = true /\
     (forall a, mem_bool a l1 = true -> mem_bool a l2 = true -> False)) <->
     all_diff_bool (l1 ++ l2) = true.
Proof.
intros l1 l2; split; rewrite <- (Oeset.all_diff_bool_app_iff oeset_of_oset).
- intros [H1 [H2 H3]]; split; [apply H1 | split; [apply H2 | ]].
  intros a1 a2 Ha; simpl in Ha; rewrite compare_eq_iff in Ha; subst a2.
  apply H3.
- intros [H1 [H2 H3]]; split; [apply H1 | split; [apply H2 | ]].
  intro a; apply H3; apply compare_eq_refl.
Qed.

Lemma all_diff_bool_fst :
  forall (B : Type) (l : list (A * B)), 
    all_diff_bool (List.map (@fst _ _) l) = true ->
    forall a b1 b2, List.In (a, b1) l -> List.In (a, b2) l -> b1 = b2.
Proof.
apply (Oeset.all_diff_bool_fst oeset_of_oset).
Qed.

Lemma comparelA_Eq :
  let ltA := fun x y => compare OA x y = Lt in
  forall l1 l2, (forall e, mem_bool e l1 = mem_bool e l2) ->
    Sorted ltA l1 -> Sorted ltA l2 ->
    comparelA (compare OA) l1 l2 = Eq.
Proof.
apply (Oeset.comparelA_Eq oeset_of_oset).
Qed.

Lemma all_diff_bool_mapI_rec :
  forall (B : Type) f l n, (forall i1 i2, f i1 = f i2 -> i1 = i2) -> 
                all_diff_bool (mapI_rec (A := B) (fun i x => f i) n l) = true.
Proof.
intros B f l n H; revert n.
induction l as [ | x1 l]; intro n; simpl; [trivial | ].
case_eq (mapI_rec (fun (i : N) (_ : B) => f i) (n + 1) l); [trivial | ].
intros x k Hn; rewrite <- Hn, IHl, Bool.andb_true_r, negb_true_iff, <- not_true_iff_false.
intro K; rewrite mem_bool_true_iff, in_mapI_rec_iff in K.
destruct K as [i [bi [Hi [Ki K]]]].
assert (J := H _ _ K).
apply (N.lt_irrefl n).
rewrite J at 2.
apply N.lt_le_trans with (n + 1)%N.
- rewrite N.add_1_r, N.lt_succ_r; apply N.le_refl.
- apply N.le_add_r.
Qed.

Section Permut.

Definition permut := (_permut (fun x y => compare OA x y = Eq)).

(** ** Permutation is a equivalence relation. 
      Reflexivity. *)

(** Reflexivity. *)
Theorem permut_refl : forall l : list A, permut l l.
Proof.
apply (Oeset.permut_refl oeset_of_oset).
Qed.

Lemma permut_refl_alt :
  forall l1 l2, comparelA (compare OA) l1 l2 = Eq -> permut l1 l2.
Proof.
apply (Oeset.permut_refl_alt oeset_of_oset).
Qed.

Lemma permut_refl_alt2 :
  forall l1 l2, l1 = l2 -> permut l1 l2.
Proof.
apply (Oeset.permut_refl_alt2 oeset_of_oset).
Qed.

(** Symetry. *)
Theorem permut_sym : forall l1 l2 : list A, permut l1 l2 -> permut l2 l1.
Proof.
apply (@Oeset.permut_sym _ oeset_of_oset).
Qed.

(** Transitivity. *)
Theorem permut_trans :
  forall l1 l2 l3 : list A, permut l1 l2 -> permut l2 l3 -> permut l1 l3.
Proof.
apply (@Oeset.permut_trans _ oeset_of_oset).
Qed.

Lemma permut_length :
  forall l1 l2, permut l1 l2 -> length l1 = length l2.
Proof.
apply (@Oeset.permut_length _ oeset_of_oset).
Qed.

Lemma in_permut_in :
  forall l1 l2 e, In e l1 -> permut l1 l2 -> In e l2.
Proof.
fix 1.
intro l1; case l1; clear l1; [ | intros a1 l1]; intros l2 e He P.
- contradiction He.
- inversion P; clear P; subst.
  rewrite compare_eq_iff in H1; subst b.
  simpl in He; destruct He as [He | He].
  + subst e.
    apply in_or_app; right; left; apply refl_equal.
  + assert (IH := in_permut_in _ _ _ He H3).
    apply in_or_app; destruct (in_app_or _ _ _ IH) as [H | H].
    * left; assumption.
    * do 2 right; assumption.
Qed.

Lemma permut_mem_bool_eq :
  forall l1 l2 e, permut l1 l2 -> mem_bool e l1 = mem_bool e l2.
Proof.
apply (@Oeset.permut_mem_bool_eq _ oeset_of_oset).
Qed.

Lemma mem_permut_mem_strong :
  forall l1 l2 e, _permut (@eq A) l1 l2 -> mem_bool e l1 = mem_bool e l2.
Proof.
apply (@Oeset.mem_permut_mem_strong _ oeset_of_oset).
Qed.

Lemma mem_morph : 
  forall x y, compare OA x y = Eq ->
  forall l1 l2, permut l1 l2 -> (mem_bool x l1 = mem_bool y l2).
Proof.
apply (@Oeset.mem_morph _ oeset_of_oset).
Qed.

Lemma permut_cons :
  forall e1 e2 l1 l2, compare OA e1 e2 = Eq -> (permut l1 l2 <-> permut (e1 :: l1) (e2 :: l2)).
Proof.
apply (Oeset.permut_cons oeset_of_oset).
Qed.

Lemma cons_permut_mem :
  forall l1 l2 e1 e2, compare OA e1 e2 = Eq -> permut (e1 :: l1) l2 -> mem_bool e2 l2 = true.
Proof.
apply (@Oeset.cons_permut_mem _ oeset_of_oset).
Qed.

Lemma permut_add_inside :
    forall e1 e2 l1 l2 l3 l4, compare OA e1 e2 = Eq -> 
      (permut (l1 ++ l2) (l3 ++ l4) <-> permut (l1 ++ e1 :: l2) (l3 ++ e2 :: l4)).
Proof.
apply (Oeset.permut_add_inside oeset_of_oset).
Qed.

Lemma permut_cons_inside :
  forall e1 e2 l l1 l2, compare OA e1 e2 = Eq ->
    (permut l (l1 ++ l2) <-> permut (e1 :: l) (l1 ++ e2 :: l2)).
Proof.
apply (Oeset.permut_cons_inside oeset_of_oset).
Qed.

Lemma permut_app_1 :
  forall l l1 l2, permut l1 l2 <-> permut (l ++ l1) (l ++ l2).
Proof.
apply (Oeset.permut_app_1 oeset_of_oset).
Qed.

 Lemma permut_app_2 :
  forall l l1 l2, permut l1 l2 <-> permut (l1 ++ l) (l2 ++ l).
Proof.
apply (Oeset.permut_app_2 oeset_of_oset).
Qed.

Lemma nb_occ_permut :
 forall l1 l2, (forall x, nb_occ x l1 = nb_occ x l2) -> permut l1 l2.
Proof.
apply (Oeset.nb_occ_permut oeset_of_oset).
Qed.

Lemma permut_nb_occ :
  forall x l1 l2, permut l1 l2 -> nb_occ x l1 = nb_occ x l2.
Proof.
apply (@Oeset.permut_nb_occ _ oeset_of_oset).
Qed.

Lemma all_diff_bool_permut_all_diff_bool :
  forall l1 l2, all_diff_bool l1 = true -> permut l1 l2 -> all_diff_bool l2 = true.
Proof.
apply (@Oeset.all_diff_bool_permut_all_diff_bool _ oeset_of_oset).
Qed.

Lemma all_diff_permut_all_diff :
  forall l1 l2, all_diff l1 -> permut l1 l2 -> all_diff l2.
Proof.
intros l1 l2; apply all_diff_permut_all_diff.
intros a1 a2 H1; rewrite compare_eq_iff in H1; assumption.
Qed.

Lemma partition_spec3 :
  forall (f : A -> bool) l l1 l2, partition f l = (l1, l2) -> permut l (l1 ++ l2).
Proof.
apply (Oeset.partition_spec3 oeset_of_oset).
Qed.

Lemma partition_permut :
  forall (f1 f2 : A -> bool) l,
    (forall x, List.In x l -> f1 x = f2 x) ->
    forall l1 l2 k k1 k2, partition f1 l = (l1, l2) -> partition f2 k = (k1, k2) ->
    permut l k -> permut l1 k1 /\ permut l2 k2.
Proof.
intros f1 f2 l Hf; apply (@Oeset.partition_permut _ oeset_of_oset).
intros x y H H'; simpl in H'.
rewrite compare_eq_iff in H'; subst y.
apply Hf; assumption.
Qed.

Lemma all_diff_mem_bool_eq_permut :
  forall l1 l2, all_diff_bool l1 = true -> all_diff_bool l2 = true -> 
                (forall x, mem_bool x l1 = mem_bool x l2) ->
                permut l1 l2.
Proof.
apply (Oeset.all_diff_bool_mem_bool_eq_permut oeset_of_oset).
Qed.

Definition permut_bool := 
  _permut_bool (fun x y => 
                  match compare OA x y with 
                    | Eq => true 
                    | _ => false 
                  end).

Lemma permut_bool_is_sound :
  forall (l1 l2 : list A),
    match permut_bool l1 l2 with 
      | true => permut l1 l2 
      | false => ~permut l1 l2 
    end.
Proof.
apply (Oeset.permut_bool_is_sound oeset_of_oset).
Qed.

End Permut.

Section Find.

Hypothesis B : Type.

Definition find a (l : list (A * B)) := Mem.find eq_bool a l.

Lemma find_eq :
  forall a l1 l2, l1 = l2 -> find a l1 = find a l2.
Proof.
intros a l1 l2 H; apply f_equal; assumption.
Qed.

Lemma not_mem_find_none :
  forall a l, mem_bool a (map (@fst _ _) l) = false -> find a l = None.
Proof.
intros a l; induction l as [ | [a1 b1] l]; intro H; [trivial | ].
rewrite map_unfold, mem_bool_unfold, Bool.orb_false_iff in H; simpl in H; destruct H as [H1 H2].
simpl; unfold eq_bool; rewrite H1.
apply IHl; apply H2.
Qed.

Lemma find_some :
  forall a l b, find a l = Some b -> In (a, b) l.
Proof.
intros a l b H.
destruct (Mem.find_some _ _ _ H) as [_a [H1 H2]].
rewrite eq_bool_true_iff in H1; subst _a; assumption.
Qed.

Lemma find_none :
  forall a l, find a l = None -> forall b, In (a, b) l -> False.
Proof.
intros a l H1 b H2.
assert (H3 : eq_bool a a = true).
{
  rewrite eq_bool_true_iff; apply refl_equal.
}
apply (Mem.find_none _ _ l H1 a H3 _ H2).
Qed.

Lemma find_none_alt : forall a l, find a l = None -> In a (map (@fst _ _) l) -> False.
Proof.
intros a l H1 H2.
rewrite in_map_iff in H2.
destruct H2 as [[_a b] [H2 H3]]; simpl in H2; subst _a.
apply (find_none _ _ H1 _ H3). 
Qed.

Lemma find_app :
  forall a l1 l2, 
    find a (l1 ++ l2) = match find a l1 with Some b1 => Some b1 | _ => find a l2 end.
Proof.
intros a l1 l2.
unfold find.
apply Mem.find_app.
Qed.

Lemma find_app_alt :
  forall a l1 l2,
    let l2' := 
        filter (fun x2 => match x2 with (a2, _) => negb (mem_bool a2 (map fst l1)) end) l2 in
    find a (l1 ++ l2) = find a (l1 ++ l2').
Proof.
intros a l1 l2 l2'.
rewrite !find_app.
case_eq (find a l1); [intros; apply refl_equal | ].
intro H.
assert (Ha : mem_bool a (map fst l1) = false).
{
  case_eq (mem_bool a (map fst l1)); intro Ha; [ | apply refl_equal].
  rewrite mem_bool_true_iff in Ha.
  apply False_rec.
  apply (find_none_alt _ _ H Ha).
}
subst l2'.
induction l2 as [ | [a2 b2] l2]; [apply refl_equal | simpl].
case_eq (eq_bool a a2); intro Ha2.
- rewrite eq_bool_true_iff in Ha2; subst a2.
  rewrite Ha; simpl; rewrite eq_bool_refl; apply refl_equal.
- case (negb (mem_bool a2 (map fst l1))); [simpl; rewrite Ha2 | ]; apply IHl2.
Qed.

Definition merge_find l1 l2 :=
  map 
    (fun x2 => match x2 with 
               | (a, b2) => match find a l1 with 
                            | Some b1 => (a, (b1,b2)) 
                            | None => (a, (b2,b2))
                            end 
               end) 
    (filter (fun x2 => match x2 with (a, _) => mem_bool a (map fst l1) end) l2).

Lemma all_diff_bool_fst_find :
  forall (l : list (A * B)) a b, all_diff_bool (map (@fst _ _) l) = true ->
     In (a, b) l -> find a l = Some b.
Proof.
intros l a b H Ha.
case_eq (find a l).
- intros b' Ha'; apply f_equal.
  rewrite all_diff_bool_ok in H.
  apply (all_diff_fst _ H a); [ | assumption].
  destruct (Mem.find_some _ _ _ Ha') as [_a [H1 H2]].
  rewrite eq_bool_true_iff in H1; subst _a.
  apply H2.
- intro K; apply False_rec.
  refine (Mem.find_none _ _ _ K a _ b Ha).
  rewrite eq_bool_true_iff; apply refl_equal.
Qed.

Lemma all_diff_fst_find :
  forall (l : list (A * B)) a b, all_diff (map (@fst _ _) l) ->
     In (a, b) l -> find a l = Some b.
Proof.
intros l a b H; apply all_diff_bool_fst_find.
rewrite all_diff_bool_ok; assumption.
Qed.

Lemma all_diff_fst_permut_find_eq :
  forall (l1 l2 : list (A * B)), all_diff (map fst l1) -> _permut (@eq _) l1 l2 ->
  forall a, find a l1 = find a l2. 
Proof.
intros l1; induction l1 as [ | [a1 b1] l1]; intros l2 Hd1 Hp a;
  [inversion Hp; apply refl_equal | ].
inversion Hp; subst.
rewrite find_app; simpl.
case_eq (eq_bool a a1); intro Ha1.
- rewrite eq_bool_true_iff in Ha1; subst a1.
  case_eq (find a l0); [ | intros; apply refl_equal].
  intros b Hb; apply False_rec.
  assert (Hd2 : all_diff (map fst (l0 ++ (a, b1) :: l3))).
  {
    apply (all_diff_permut_all_diff Hd1).
    apply _permut_map with eq; [ | assumption].
    intros [] [] _ _ H; injection H; clear H; intros; subst.
    rewrite compare_eq_iff; apply refl_equal.
  }
  rewrite map_app, <- all_diff_app_iff in Hd2.
  apply (proj2 (proj2 Hd2) a); [ | left; apply refl_equal].
  rewrite in_map_iff; exists (a, b); split; [apply refl_equal | apply (find_some _ _ Hb)].
- rewrite map_unfold, all_diff_unfold in Hd1.
  rewrite (IHl1 _ (proj2 Hd1) H3), find_app.
  apply refl_equal.
Qed.

End Find.  

Lemma all_diff_bool_permut_find_map_eq :
  forall (B : Type) ll1 l2 (f : A -> B), 
    all_diff (map fst ll1) -> 
    all_diff (map snd ll1) -> 
    permut (map fst ll1) l2 ->
    forall a, find a
                   (map
                      (fun a0 =>
                         (match find a0 ll1 with
                          | Some b => b
                          | None => a0
                          end, f a0)) (map fst ll1)) =
              find a
                   (map
                      (fun a0 =>
                         (match find a0 ll1 with
                          | Some b => b
                          | None => a0
                          end, f a0)) l2).
Proof.
intros B ll1 l2 f Df Ds P a.
apply all_diff_fst_permut_find_eq.
- rewrite !map_map; simpl.
  assert (all_diff_eq : forall (l1 l2 : list A), l1 = l2 -> all_diff l1 -> all_diff l2);
    [intros; subst; assumption | ].
  refine (all_diff_eq _ _ _ Ds).
  rewrite <- map_eq.
  clear a P; induction ll1 as [ | [a1 b1] ll1]; intros [a b] H; [contradiction H | simpl].
  case_eq (eq_bool a a1); intro Ha1.
  + rewrite eq_bool_true_iff in Ha1; subst a1.
    simpl in H; destruct H as [H | H].
    * injection H; clear H; intros; subst; simpl; apply refl_equal.
    * rewrite map_unfold, all_diff_unfold in Df.
      apply False_rec; apply (proj1 Df a); [ | apply refl_equal].
      rewrite in_map_iff; eexists; split; [ | apply H]; apply refl_equal.
  + simpl in H; destruct H as [H | H].
    * injection H; clear H; do 2 intro; subst a1 b1.
      rewrite eq_bool_refl in Ha1; discriminate Ha1.
    * rewrite map_unfold, all_diff_unfold in Df, Ds.
      apply (IHll1 (proj2 Df) (proj2 Ds) _ H).
- apply _permut_map with eq.
  + intros; subst; apply refl_equal.
  + revert P; apply _permut_incl.
    do 2 intro; apply compare_eq_iff.
Qed.

Lemma merge_find_ok :
  forall (B : Type) (l1 l2 : list (A * B)) a b1 b2,
    find a (merge_find l1 l2) = Some (b1, b2) <-> (find a l1 = Some b1 /\ find a l2 = Some b2).
Proof.
intros B l1 l2; unfold merge_find; revert l1.
induction l2 as [ | [a2 c2] l2]; intros l1 a b1 b2; split; simpl.
- intro Abs; discriminate Abs.
- intros [_ Abs]; discriminate Abs.
- case_eq (mem_bool a2 (map fst l1)); intro Ha2; simpl.
  + case_eq (find a2 l1).
    * intros b Ka2.
      case_eq (eq_bool a a2); intro Ja2.
      -- rewrite eq_bool_true_iff in Ja2; subst a2.
         intro H; injection H; clear H; do 2 intro; subst; split; trivial.
      -- apply IHl2.
    * intro Ka2.
      case_eq (eq_bool a a2); intro Ja2.
      -- rewrite eq_bool_true_iff in Ja2; subst a2.
         intro H; injection H; clear H; do 2 intro; subst.
         apply False_rec; apply (find_none_alt _ _ Ka2).
         rewrite mem_bool_true_iff in Ha2; apply Ha2.
      -- apply IHl2.
  + intro Ka2.
    case_eq (eq_bool a a2); intro Ja2.
    * rewrite eq_bool_true_iff in Ja2; subst a2.
      assert (Ja2 := find_some _ _ Ka2); rewrite in_map_iff in Ja2.
      destruct Ja2 as [[_a _b2] [_Ja2 Ja2]].
      rewrite filter_In in Ja2.
      destruct Ja2 as [Ja2 La2].
      case_eq (find _a l1).
      -- intros _b1 _Hb1; rewrite _Hb1 in _Ja2.
         injection _Ja2; do 3 intro; subst _a _b1 _b2.
         rewrite Ha2 in La2; discriminate La2.
      -- intro _Hb1; rewrite _Hb1 in _Ja2.
         injection _Ja2; do 3 intro; subst _a b2 _b2.
         rewrite Ha2 in La2; discriminate La2.
    * revert Ka2; apply IHl2.
- intros [H1 H2].
  case_eq (eq_bool a a2); intro Ha2; rewrite Ha2 in H2.
  + rewrite eq_bool_true_iff in Ha2; subst a2.
    injection H2; clear H2; intro; subst c2.
    assert (Ha : mem_bool a (map fst l1) = true).
    {
      rewrite mem_bool_true_iff, in_map_iff.
      exists (a, b1); split; [apply refl_equal | ].
      apply (find_some _ _ H1).
    }
    rewrite Ha; simpl; rewrite H1, eq_bool_refl; apply refl_equal.
  + case_eq (mem_bool a2 (map fst l1)); intro Ka2; simpl.
    * case_eq (find a2 l1).
      -- intros _b2 _Hb2.
         rewrite Ha2.
         apply IHl2; split; trivial.
      -- intro _Hb2; rewrite Ha2.
         apply IHl2; split; trivial.
    * apply IHl2; split; trivial.
Qed.

Lemma merge_find_ok_alt :
  forall (B : Type) (l1 l2 : list (A * B)) a b1 b2,
    all_diff (map fst l2) ->
    In (a, (b1, b2)) (merge_find l1 l2) -> (find a l1 = Some b1 /\ find a l2 = Some b2).
Proof.
intros B l1 l2; unfold merge_find; revert l1.
induction l2 as [ | [a2 c2] l2]; intros l1 a b1 b2 D H; [contradiction H | split].
- rewrite map_unfold, all_diff_unfold in D.
  simpl in H.
  case_eq (mem_bool a2 (map fst l1)); intro Ha2; rewrite Ha2 in H.
  + simpl in H; destruct H as [H | H].
    * case_eq (find a2 l1).
      -- intros b1' Hb1'; rewrite Hb1' in H.
         injection H; do 3 intro; subst; assumption.
      -- intro Ka2; rewrite Ka2 in H.
         injection H; do 3 intro; subst.
         apply False_rec.
         apply (find_none_alt _ _ Ka2); rewrite <- mem_bool_true_iff; assumption.
    * apply (proj1 (IHl2 _ _ _ _ (proj2 D) H)).
  + apply (proj1 (IHl2 _ _ _ _ (proj2 D) H)).
- rewrite map_unfold, all_diff_unfold in D.
  simpl in H.
  case_eq (mem_bool a2 (map fst l1)); intro Ha2; rewrite Ha2 in H.
  + simpl in H; destruct H as [H | H].
    * case_eq (find a2 l1).
      -- intros b1' Hb1'; rewrite Hb1' in H.
         injection H; do 3 intro; subst; simpl.
         rewrite eq_bool_refl; apply refl_equal.
      -- intro Ka2; rewrite Ka2 in H.
         injection H; do 3 intro; subst.
         apply False_rec.
         apply (find_none_alt _ _ Ka2); rewrite <- mem_bool_true_iff; assumption.
    * simpl.
      case_eq (eq_bool a a2); intro Ka2.
      -- rewrite eq_bool_true_iff in Ka2; subst a2.
         rewrite in_map_iff in H.
         destruct H as [[_a _b2] [_H H]].
         rewrite filter_In in H.
         destruct H as [H1 H2].
         apply False_rec.
         apply (proj1 D a); [ | apply refl_equal].
         destruct (find _a l1); simpl in _H; injection _H; do 3 intro; subst;
           (rewrite in_map_iff; eexists; split; [ | apply H1]; apply refl_equal).
      -- apply (proj2 (IHl2 _ _ _ _ (proj2 D) H)).
  + rewrite in_map_iff in H.
    destruct H as [[_a _b2] [_H H]].
    case_eq (find _a l1).
    * intros _b1 Hb1; rewrite Hb1 in _H; injection _H; do 3 intro; subst.
      simpl; case_eq (eq_bool a a2); intro Ka2.
      -- rewrite eq_bool_true_iff in Ka2; subst a2.
         apply False_rec.
         apply (proj1 D a); [ | apply refl_equal].
         rewrite filter_In in H.
         rewrite in_map_iff; eexists; split; [ | apply (proj1 H)]; apply refl_equal.
      -- refine (proj2 (IHl2 _ _ _ _ (proj2 D) _)).
         rewrite in_map_iff; eexists; split; [ | apply H].
        simpl; rewrite Hb1; apply refl_equal.
    * intro _Ha; rewrite _Ha in _H; injection _H; do 3 intro; subst.
      simpl; case_eq (eq_bool a a2); intro Ka2.
      -- rewrite eq_bool_true_iff in Ka2; subst a2.
         apply False_rec.
         apply (proj1 D a); [ | apply refl_equal].
         rewrite filter_In in H.
         rewrite in_map_iff; eexists; split; [ | apply (proj1 H)]; apply refl_equal.
      -- refine (proj2 (IHl2 _ _ _ _ (proj2 D) _)).
         rewrite in_map_iff; eexists; split; [ | apply H].
         simpl; rewrite _Ha; apply refl_equal.
Qed.

(* Lemma find_map2 :
  forall (C : Type) (f : C -> A) (g : C -> B) l a, 
    (forall a1 a2, List.In a1 l -> List.In a2 l -> f a1 = f a2 -> a1 = a2) ->
    In a l -> find (f a) (map (fun x => (f x, g x)) l) = Some (g a).
Proof.
intros C f g l a; 
induction l as [ | a1 l];
intros H Ha.
- contradiction Ha.
- simpl.
  case_eq (eq_bool (f a) (f a1)); intro Ja.
  + rewrite eq_bool_true_iff in Ja.
    do 2 apply f_equal; apply sym_eq; apply (H _ _ Ha (or_introl _ (refl_equal _)) Ja).
  + simpl in Ha; destruct Ha as [Ha | Ha].
    * subst a1; unfold eq_bool in Ja; rewrite compare_eq_refl in Ja; discriminate Ja.
    * apply IHl; [ | assumption].
      do 4 intro; apply H; right; assumption.
Qed.
*)

End Sec.

Section Sec2.

Hypotheses A B : Type.
Hypothesis OA : Rcd A.
Hypothesis OB : Rcd B.

Definition mem_bool_map :
  forall a l, mem_bool OA a l = true -> forall (f : A -> B), mem_bool OB (f a) (map f l) = true.
Proof.
intros a l Ha f.
apply (@Oeset.mem_bool_map _ _ (oeset_of_oset OA) (oeset_of_oset OB) _ _ Ha).
intros x Hx; simpl in Hx; rewrite compare_eq_iff in Hx; subst x.
apply Oeset.compare_eq_refl.
Qed.

Lemma mem_bool_flat_map :
  forall b (f : A -> list B) l, 
    mem_bool OB b (flat_map f l) = existsb (fun x => mem_bool OB b (f x)) l.
Proof.
apply (Oeset.mem_bool_flat_map (oeset_of_oset OB)).
Qed.

Definition one_to_one_bool sa (rho : A -> B) :=
  forallb 
    (fun a1 => 
          forallb
            (fun a2 => match compare OB (rho a1) (rho a2) with 
                            | Eq => 
                              match compare OA a1 a2 with
                                | Eq => true
                                | Lt | Gt => false
                              end
                            | Lt | Gt => true
                          end) sa)
    sa.

Lemma one_to_one_bool_ok :
  forall sa rho, one_to_one sa rho <-> one_to_one_bool sa rho = true.
Proof.
intros fa rho.
unfold one_to_one, one_to_one_bool.
rewrite forallb_forall; split.
- (* 1/2 *)
  intros H a1 Ha1; rewrite forallb_forall.
  intros a2 Ha2.
  generalize (eq_bool_ok OB (rho a1) (rho a2)).
  case (compare OB (rho a1) (rho a2)); 
    [ intro Ha
    | exact (fun _ => refl_equal _) 
    | exact (fun _ => refl_equal _) ].
  rewrite (H a1 a2 Ha1 Ha2 Ha), compare_eq_refl.
  apply refl_equal.
- (* 1/1 *)
  intros H a1 a2 Ha1 Ha2 Ha.
  assert (IHa1 := H _ Ha1).
  rewrite forallb_forall in IHa1.
  assert (IH := IHa1 _ Ha2).
  rewrite Ha, compare_eq_refl, compare_eq_true in IH.
  generalize (eq_bool_ok OA a1 a2); rewrite IH.
  exact (fun h => h).
Qed.

Lemma all_diff_bool_map :
  forall (f : A -> B) l, 
    all_diff_bool OA l = true -> (forall x1 x2, In x1 l -> f x1 = f x2 -> x1 = x2) -> 
    all_diff_bool OB (map f l) = true.
Proof.
intros f l; induction l as [ | a1 l]; intros H1 H2;  [trivial | ].
rewrite all_diff_bool_unfold, Bool.andb_true_iff in H1.
rewrite map_unfold, all_diff_bool_unfold.
rewrite IHl, Bool.andb_true_r.
- rewrite negb_true_iff, <- not_true_iff_false in H1.
  rewrite negb_true_iff, <- not_true_iff_false.
  intro H; apply H1.
  rewrite mem_bool_true_iff, in_map_iff in H.
  destruct H as [x [H Hx]].
  rewrite (H2 a1 x (or_introl _ (refl_equal _)) (sym_eq H)).
  rewrite mem_bool_true_iff; assumption.
- apply (proj2 H1).
- do 3 intro; apply H2; right; assumption.
Qed.

Lemma all_diff_bool_snd :
  forall (l : list (A * B)), 
    all_diff_bool OB (List.map (@snd _ _) l) = true ->
    forall a1 a2 b, List.In (a1, b) l -> List.In (a2, b) l -> a1 = a2.
Proof.
intros l H a1 a2 b Ha1 Ha2.
apply (all_diff_bool_fst OB (map (fun x => match x with (a, b) => (b, a) end) l)) with b.
- rewrite map_map, <- H; apply f_equal.
  rewrite <- map_eq; intros x _; destruct x; trivial.
- rewrite in_map_iff; exists (a1, b); split; trivial.
- rewrite in_map_iff; exists (a2, b); split; trivial.
Qed.

Lemma nb_occ_map_eq_3 :
  forall (f : A -> B) l1 l2,
    (forall x, nb_occ OA x l1 = nb_occ OA x l2) -> 
    forall x, nb_occ OB x (map f l1) = nb_occ OB x (map f l2).
Proof.
intros f l1 l2 H y.
revert l2 H; induction l1 as [ | a1 l1]; intros l2 H.
- destruct l2 as [ | a2 l2]; [apply refl_equal | ].
  assert (Ha2 := H a2); simpl in Ha2.
  rewrite compare_eq_refl in Ha2; destruct (nb_occ OA a2 l2); simpl in Ha2; discriminate Ha2.
- assert (Ha1 : In a1 l2).
  {
    rewrite <- (mem_bool_true_iff OA).
    rewrite (permut_mem_bool_eq (l2 := a1 :: l1)).
    - rewrite mem_bool_unfold, compare_eq_refl; apply refl_equal.
    - apply nb_occ_permut.
      intros; apply sym_eq; apply H.
  }
  destruct (in_split _ _ Ha1) as [k1 [k2 Hl2]]; rewrite Hl2.
  rewrite map_app, 2 (map_unfold _ (_ :: _)).
  rewrite nb_occ_app, 2 (nb_occ_unfold _ _ (_ :: _)).
  assert (Aux : forall (n1 n2 n3 n4 : N), n2 = (n3 + n4)%N <-> (n1 + n2 = n3 + (n1 + n4))%N).
  {
    intros; split; intro.
    - subst; rewrite N.add_comm, <- N.add_assoc; apply f_equal.
      apply N.add_comm.
    - revert n2 n3 n4 H0; pattern n1; apply N.peano_rec.
      + intros; assumption.
      + intros n IH n2 n3 n4 _H.
        rewrite 2 N.add_succ_comm in _H.
        assert (IH' := IH _ _ _ _H).
        rewrite N.add_succ_r in IH'.
        apply N.succ_inj; apply IH'.
  }
  apply Aux; rewrite <- nb_occ_app, <- map_app; apply IHl1.
  intro x; rewrite nb_occ_app.
  assert (Hx := H x); rewrite Hl2, nb_occ_app, 2 (nb_occ_unfold _ _ (_ :: _)), <- Aux in Hx.
  assumption.
Qed.

(** Merging two compatible substitutions *)
Fixpoint merge (partial_res l : list (A * B)) {struct l} : option (list (A * B)) :=
  match l with
  | nil => Some partial_res
  | (x, t) as p :: l => 
       match find OA x partial_res with
       | Some t' => if eq_bool OB t t' then merge partial_res l else None
       | None => merge (p :: partial_res) l
       end
   end.

Lemma merge_correct :
   forall l1 l2,
     match merge l1 l2 with
       | Some l => 
         (forall a b, find OA a l1 = Some b -> find OA a l = Some b) /\
         (forall a b, find OA a l2 = Some b -> 
                      (exists b', find OA a l = Some b' /\ eq_bool OB b b' = true)) /\
         (forall a b, find OA a l = Some b -> 
                      (find OA a l1 = Some b \/ (find OA a l1 = None /\ find OA a l2 = Some b)))
       | None => exists a, 
                   exists a', 
                     exists b, 
                       exists b', 
                         (find OA a l1 = Some b \/ In (a, b) l2) /\ In (a',  b') l2 /\
                         eq_bool OA a a' = true /\ eq_bool OB b' b = false
     end.
Proof.
fix 2.
intros l1 l2; case l2; clear l2; simpl.
- (* 1/2 l2 = [] *)
  repeat split.
  + exact (fun _ _ h => h).
  + intros a b H; discriminate H.
  + intros a b H; left; exact H.
- (* 1/1 l2 = _ :: _ *)
  intros [a2 b2] l2.
  case_eq (find OA a2 l1).
  + (* 1/2 find a2 l1 = Some _ *)
    intros b Hb; case_eq (eq_bool OB b2 b).
    * (* 1/3 b2 = b *)
      intro b2_eq_b; rewrite eq_bool_true_iff in b2_eq_b; subst b2.
      {
        generalize (merge_correct l1 l2); case (merge l1 l2).
        - (* 1/4 merge l1 l2 = Some l *)
          intros l H; case H; clear H.
          intros H1 H; case H; clear H.
          intros H2 H3.
          repeat split.
          + (* 1/6 *)
            assumption.
          + (* 1/5 *)
            intros a b'; case_eq (eq_bool OA a a2).
            * intros a_eq_a2 H; injection H; clear H; intro H; subst b'.
              rewrite eq_bool_true_iff in a_eq_a2; subst a2.
              {
                exists b; split.
                - apply H1; assumption.
                - rewrite eq_bool_true_iff; apply refl_equal.
              }
            * intros a_diff_a2 F'; apply H2; assumption.
          + (* 1/4 *)
            intros a b' F'; case_eq (eq_bool OA a a2).
            * intros a_eq_a2.
              rewrite eq_bool_true_iff in a_eq_a2; subst a2.
              destruct (H3 _ _ F') as [K3 | K3]; [left; assumption | ].
              rewrite (proj1 K3) in Hb; discriminate Hb.
            * intro a_diff_a2; apply H3; assumption.
        - (* 1/3 merge l1 l2 = None *)
          intros [a [a' [b1 [b1' [[F' | F'] [F'' [a_eq_a' b1_diff_b1']]]]]]];
          exists a; exists a'; exists b1; exists b1'; repeat split.
          + left; assumption.
          + right; assumption.
          + assumption.
          + assumption.
          + do 2 right; assumption.
          + right; assumption.
          + assumption.
          + assumption.
      }
    * (* 1/2 b2 <> b *)
      intro b2_diff_b.
      {
        exists a2; exists a2; exists b; exists b2; repeat split.
        - left; assumption.
        - left; reflexivity.
        - rewrite eq_bool_true_iff; apply refl_equal.
        - assumption.
      }
  + (* 1/1 find a2 l1 = None *)
    intro F.
    generalize (merge_correct ((a2, b2) :: l1) l2).
    case (merge ((a2, b2) :: l1) l2); simpl.
    * (* 1/2 merge ((a2,b2) :: l1) l2 = Some l *)
      intros l H; case H; clear H.
      intros H1 H; case H; clear H.
      intros H2 H3.
      {
        repeat split.
        - (* 1/4 *)
          intros a b F'; generalize (H1 a b); case_eq (eq_bool OA a a2).
          + intro a_eq_a2; rewrite eq_bool_true_iff in a_eq_a2; subst a2.
            rewrite F' in F; discriminate F.
          + intros _ H1a; apply H1a; assumption.
        - (* 1/3 *)
          intros a b'; case_eq (eq_bool OA a a2).
          intros a_eq_a2 H; injection H; clear H; intro H; subst b'.
          rewrite eq_bool_true_iff in a_eq_a2; subst a2.
          exists b2; split.
          + apply (H1 a b2); rewrite eq_bool_refl; apply refl_equal.
          + apply eq_bool_refl.
          + intros a_diff_a2 F'; exact (H2 a b' F').
        - (* 1/2 *)
          intros a b F'; generalize (H3 a b F'); case_eq (eq_bool OA a a2).
          + intros a_eq_a2; rewrite eq_bool_true_iff in a_eq_a2; subst a2.
            intros [F'' | FF].
            * right; split; trivial.
            * destruct FF as [FF _]; discriminate FF.
          + intros _ H; exact H.
      } 
    * (* 1/1 *)
      intros [a [a' [b1 [b1' [[F' | F'] [F'' [a_eq_a' b1_diff_b1']]]]]]].
      rewrite eq_bool_true_iff in a_eq_a'; subst a'.
      {
        case_eq (eq_bool OA a a2).
        - intros a_eq_a2; rewrite a_eq_a2 in F'; injection F'; clear F'; intro; subst b2.
          rewrite eq_bool_true_iff in a_eq_a2; subst a2.
          exists a; exists a; exists b1'; exists b1; repeat split.
          + do 2 right; assumption.
          + left; trivial.
          + apply eq_bool_refl.
          + rewrite eq_bool_sym; assumption.
        - intros a_diff_a2; rewrite a_diff_a2 in F'.
          exists a; exists a; exists b1; exists b1'; repeat split.
          + left; assumption.
          + right; assumption.
          + apply eq_bool_refl.
          + assumption.
      } 
      {
        exists a; exists a'; exists b1; exists b1'; repeat split.
        - do 2 right; assumption.
        - right; assumption.
        - assumption.
        - assumption.
      }
Qed.

Lemma merge_inv :
  forall (l1 l2 : list (A * B)), 
    (forall a1 a1' b1 c1, In (a1,b1) l1 -> In (a1',c1) l1 -> eq_bool OA a1 a1' = true -> 
                          eq_bool OB b1 c1 =true) ->
       (forall a2 a2' b2 c2, In (a2,b2) l2 -> In (a2',c2) l2 -> eq_bool OA a2 a2' = true -> 
                             eq_bool OB b2 c2 = true) ->
       match merge l1 l2 with
       | Some l => (forall a a' b c, In (a,b) l -> In (a',c) l -> eq_bool OA a a' = true -> 
                                     eq_bool OB b c = true) 
       | None => True
       end.
Proof.
fix 2.
intros l1 l2; case l2; clear l2.
- (* 1/2 l2 = [] *)
  intros Inv1 _; exact Inv1.
- (* 1/1 l2 = _ :: _ *)
  intros p l2; case p; clear p; simpl.
  intros a2 b2 Inv1 Inv2.
  case_eq (find OA a2 l1).
  + (* 1/2 find eqA a2 l1 = Some b *)
    intros b H; case_eq (eq_bool OB b2 b); [intro b2_eq_b | intro b2_diff_b].
    * (* 1/3 b = b2 *)
      apply merge_inv.
      exact Inv1.
      {
        intros a a' b' c ab_in_l2 ac_in_l2; apply (Inv2 a a' b' c).
        - right; assumption.
        - right; assumption.
      }
    * (* b <> b2 *)
      exact I.
  + (* 1/1 find eqA a2 l1 = None *)
    intro H; apply merge_inv; simpl.
    * {
        intros a a' b' c [H1 | ab_in_l1] [H2 | ac_in_l1].
        - injection H1; injection H2; intros; subst a a' b' c.
          apply eq_bool_refl.
        - rewrite eq_bool_true_iff; intro; subst a'.
          injection H1; clear H1; do 2 intro; subst a2 b2.
          apply False_rec.
          apply (find_none _ _ _ H _ ac_in_l1).
        - rewrite eq_bool_true_iff; intro; subst a'.
          injection H2; clear H2; do 2 intro; subst a2 b2.
          apply False_rec.
          apply (find_none _ _ _ H _ ab_in_l1).
        - apply Inv1; trivial.
      }
    * do 6 intro; apply Inv2; right; assumption.
Qed.

End Sec2.

Lemma find_map :
  forall (A : Type) (OA : Rcd A) (B : Type) (f : A -> B) l a, 
    In a l -> find OA a (map (fun x => (x, f x)) l) = Some (f a).
Proof.
intros A OA B f l a; induction l as [ | a1 l]; intro Ha.
- contradiction Ha.
- simpl in Ha; simpl.
  case_eq (eq_bool OA a a1); intro Ka.
  + rewrite eq_bool_true_iff in Ka; subst a1; apply refl_equal.
  + destruct Ha as [Ha | Ha]; [subst a1; rewrite eq_bool_refl in Ka; discriminate Ka | ].
    apply IHl; assumption.
Qed.

Lemma find_some_map :
  forall (A : Type) (OA : Rcd A) (B C : Type) (f : B -> C) l a b, 
    find OA a l = Some b -> 
    find OA a (map (fun xy => match xy with (x, y) => (x, f y) end) l) = Some (f b).
Proof.
intros A OA B C f l a b; induction l as [ | [a1 b1] l]; intro Ha.
- discriminate Ha.
- simpl in Ha; simpl.
  case_eq (eq_bool OA a a1); intro Ka.
  + rewrite Ka in Ha; injection Ha; clear Ha; intro; subst b1; trivial.
  + rewrite Ka in Ha; apply IHl; assumption.
Qed.

Lemma find_none_map :
  forall (A : Type) (OA : Rcd A) (B C : Type) (f : B -> C) l a, 
    find OA a l = None ->
    find OA a (map (fun xy => match xy with (x, y) => (x, f y) end) l) = None.
Proof.
intros A OA B C f l a; induction l as [ | [a1 b1] l]; intro Ha.
- apply refl_equal.
- simpl in Ha; simpl.
  case_eq (eq_bool OA a a1); intro Ka; rewrite Ka in Ha.
  + discriminate Ha.
  + apply IHl; assumption.
Qed.

Definition mk_pair (A B : Type) (OA : Rcd A) (SB : Rcd B) : Rcd (A * B).
Proof.
split with (compareAB (Oset.compare OA) (Oset.compare SB)).
- intros [a1 b1] [a2 b2]; apply compareAB_eq_bool_ok.
  + intros; apply Oset.eq_bool_ok.
  + intros; apply Oset.eq_bool_ok.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAB_lt_trans.
  + apply Oset.compare_eq_trans.
  + apply Oset.compare_eq_lt_trans.
  + apply Oset.compare_lt_eq_trans.
  + apply Oset.compare_lt_trans.
  + apply Oset.compare_lt_trans.
- intros [a1 b1] [a2 b2]; apply compareAB_lt_gt.
  + apply Oset.compare_lt_gt.
  + apply Oset.compare_lt_gt.
Defined.

Definition mk_list (A : Type) (OA : Rcd A) : Rcd (list A).
Proof.
split with (comparelA (Oset.compare OA)).
- intros l1 l2; apply (comparelA_eq_bool_ok (Oset.compare OA) l1 l2).
  intros; apply Oset.eq_bool_ok.
- intros l1 l2 l3; apply comparelA_lt_trans.
  + do 6 intro; apply Oset.compare_eq_trans.
  + do 6 intro; apply Oset.compare_eq_lt_trans.
  + do 6 intro; apply Oset.compare_lt_eq_trans.
  + do 6 intro; apply Oset.compare_lt_trans.
- intros l1 l2; apply comparelA_lt_gt.
  do 4 intro; apply Oset.compare_lt_gt.
Defined.

Definition mk_option (A : Type) (OA : Rcd A) : Rcd (option A).
Proof.
split with (compareoA (Oset.compare OA)).
- intros a1 a2; unfold compareoA, option_to_list.
  destruct a1 as [a1 | ]; destruct a2 as [a2 | ]; simpl; try discriminate; trivial.
  case_eq (Oset.compare OA a1 a2); intro Ha.
  + apply f_equal; assert (H := Oset.eq_bool_ok OA a1 a2); rewrite Ha in H; apply H.
  + intro H; injection H; clear H; intro H; rewrite H, Oset.compare_eq_refl in Ha;
      discriminate Ha.
  + intro H; injection H; clear H; intro H; rewrite H, Oset.compare_eq_refl in Ha;
      discriminate Ha.
- do 3 intro; unfold compareoA, option_to_list; apply comparelA_lt_trans.
  + do 6 intro; apply Oset.compare_eq_trans.
  + do 6 intro; apply Oset.compare_eq_lt_trans.
  + do 6 intro; apply Oset.compare_lt_eq_trans.
  + do 6 intro; apply Oset.compare_lt_trans.
- do 2 intro; unfold compareoA, option_to_list; apply comparelA_lt_gt.
  do 4 intro; apply Oset.compare_lt_gt.
Defined.

End Oset.


(** Some concrete ordered sets. *)
Require Import Arith NArith.

Definition Ounit : Oset.Rcd unit.
split with (fun _ _ => Eq).
- intros a1 a2; destruct a1; destruct a2; apply refl_equal.
- intros; discriminate.
- intros; apply refl_equal.
Defined.

Definition bool_compare b1 b2 :=
  match b1, b2 with
    | true, true => Eq
    | true, false => Lt
    | false, true => Gt
    | false, false => Eq
  end.

Definition Obool : Oset.Rcd bool.
split with bool_compare.
- intros [ | ] [ | ]; simpl; trivial; discriminate.
- intros [ | ] [ | ] [ | ]; simpl; trivial; discriminate.
- intros [ | ] [ | ]; simpl; trivial.
Defined.

Definition Onat : Oset.Rcd nat.
split with nat_compare.
(* 1/3 *)
intros a1 a2; case_eq (nat_compare a1 a2); intro H12.
rewrite (nat_compare_eq _ _ H12); apply refl_equal.
rewrite <- nat_compare_lt in H12.
intro; subst a2; apply False_rec; apply (lt_irrefl _ H12).
rewrite <- nat_compare_gt in H12.
intro; subst a2; apply False_rec; apply (lt_irrefl _ H12).
(* 1/2 *)
intros n1 n2 n3.
case_eq (nat_compare n1 n2); intro H12; [ intro Abs; discriminate Abs | | intro Abs; discriminate Abs].
intros _; case_eq (nat_compare n2 n3); intro H23; [ intro Abs; discriminate Abs | | intro Abs; discriminate Abs].
intros _.
rewrite <- nat_compare_lt in H12, H23.
rewrite <- nat_compare_lt; apply lt_trans with n2; assumption.
(* 1/1 *)
intros a1 a2; case_eq (nat_compare a1 a2); intro H12.
rewrite nat_compare_eq_iff in H12; subst a2.
generalize (refl_equal a1); rewrite <- nat_compare_eq_iff; intro Hn; rewrite Hn; apply refl_equal.
rewrite <- nat_compare_lt in H12.
assert (H21 : a2 > a1); [apply H12 | ].
rewrite nat_compare_gt in H21; rewrite H21; apply refl_equal.
rewrite <- nat_compare_gt in H12.
assert (H21 : a2 < a1); [apply H12 | ].
rewrite nat_compare_lt in H21; rewrite H21; apply refl_equal.
Defined.

Definition ON : Oset.Rcd N.
split with Ncompare.
- (* 1/3 *)
  intros a1 a2; case_eq (Ncompare a1 a2); intro H12.
  + rewrite (Ncompare_Eq_eq _ _ H12); apply refl_equal.
  + intro H; subst a2; rewrite Ncompare_refl in H12; discriminate H12.
  + intro H; subst a2; rewrite Ncompare_refl in H12; discriminate H12.
- (* 1/2 *)
  intros n1 n2 n3; rewrite 3 nat_of_Ncompare, <- 3 nat_compare_lt; apply lt_trans.
- (* 1/1 *)
  intros n1 n2; case_eq (Ncompare n1 n2); intro A12; rewrite <- Ncompare_antisym, A12; simpl; apply refl_equal.
Defined.

Require Import ZArith.

Definition OZ : Oset.Rcd Z.
split with Zcompare.
(* 1/3 *)
intros a1 a2; case_eq (Zcompare a1 a2); intro H12.
rewrite (Zcompare_Eq_eq _ _ H12); apply refl_equal.
intro H; subst a2; rewrite Zcompare_refl in H12; discriminate H12.
intro H; subst a2; rewrite Zcompare_refl in H12; discriminate H12.
(* 1/2 *)
intros n1 n2 n3 H12 H23; apply (Zcompare_Lt_trans _ _ _ H12 H23).
(* 1/1 *)
intros n1 n2; case_eq (Zcompare n1 n2); intro H12.
rewrite (Zcompare_Eq_eq _ _ H12), Zcompare_refl; apply refl_equal.
rewrite <- Zcompare_Gt_Lt_antisym in H12; rewrite H12; apply refl_equal.
rewrite Zcompare_Gt_Lt_antisym in H12; rewrite H12; apply refl_equal.
Defined.

Section NEmbedding.

Hypothesis A : Type.
Hypothesis N_of_A : A -> N.
Hypothesis N_of_A_inj : forall a1 a2, N_of_A a1 = N_of_A a2 -> a1 = a2.

Definition Oemb : Oset.Rcd A.
set (compare := fun a1 a2 => Ncompare (N_of_A a1) (N_of_A a2)).
split with compare.
(* 1/3 *)
intros a1 a2; unfold compare; simpl.
generalize (Ncompare_eq_correct (N_of_A a1) (N_of_A a2)).
case (Ncompare (N_of_A a1) (N_of_A a2)); intro H.
apply N_of_A_inj; rewrite <- H; apply refl_equal.
intro Abs; subst a2; generalize (refl_equal (N_of_A a1)); rewrite <- H; discriminate.
intro Abs; subst a2; generalize (refl_equal (N_of_A a1)); rewrite <- H; discriminate.
(* 1/2 *)
intros n1 n2 n3; unfold compare; simpl; rewrite 3 nat_of_Ncompare, <- 3 nat_compare_lt; apply lt_trans.
(* 1/1 *)
intros n1 n2; unfold compare; simpl.
case_eq (Ncompare (N_of_A n1) (N_of_A n2)); intro A12;
rewrite <- Ncompare_antisym, A12; apply refl_equal.
Defined.

End NEmbedding.

Fixpoint string_compare s1 s2 : comparison :=
  match s1, s2 with
  | EmptyString, EmptyString => Eq
  | EmptyString, String _ _ => Lt
  | String _ _, EmptyString => Gt
  | String a1 s1, String a2 s2 =>
       match Ncompare (N_of_ascii a1) (N_of_ascii a2) with
       | Eq => string_compare s1 s2
       | Lt => Lt
       | Gt => Gt 
       end
  end.

Definition Ostring : Oset.Rcd string.
split with string_compare.
(* 1/3 *)
intro s1; induction s1 as [ | a1 s1]; intros [ | a2 s2]; simpl.
apply refl_equal.
discriminate.
discriminate.
case_eq (Ncompare (N_of_ascii a1) (N_of_ascii a2)); intro A12.
generalize (IHs1 s2); case (string_compare s1 s2); intro S12.
apply f_equal2; [ | apply S12].
rewrite <- (ascii_N_embedding a1), <- (ascii_N_embedding a2); apply f_equal; apply Ncompare_Eq_eq; apply A12.
intro H12; apply S12; injection H12; intros; subst; apply refl_equal.
intro H12; apply S12; injection H12; intros; subst; apply refl_equal.
intro H12; injection H12; intros; subst; rewrite Ncompare_refl in A12; discriminate A12.
intro H12; injection H12; intros; subst; rewrite Ncompare_refl in A12; discriminate A12.
(* 1/2 *)
intro s1; induction s1 as [ | a1 s1]; intros [ | a2 s2]; simpl.
intros s3 Abs; discriminate Abs.
intros [ | a3 s3].
intros _ Abs; discriminate Abs.
exact (fun _ _ => refl_equal _).
intros s3 Abs; discriminate Abs.
intros [ | a3 s3].
intros _ Abs; discriminate Abs.
case_eq (Ncompare (N_of_ascii a1) (N_of_ascii a2)); intro A12.
rewrite (Ncompare_Eq_eq _ _ A12).
intro S12; case_eq (Ncompare (N_of_ascii a2) (N_of_ascii a3)); intro A23.
apply IHs1; assumption.
exact (fun h => h).
exact (fun h => h).
intros _; case_eq (Ncompare (N_of_ascii a2) (N_of_ascii a3)); intro A23.
rewrite <- (Ncompare_Eq_eq _ _ A23), A12.
exact (fun _ => refl_equal _).
rewrite nat_of_Ncompare, <- nat_compare_lt in A12.
rewrite nat_of_Ncompare, <- nat_compare_lt in A23.
assert (A13 := lt_trans _ _ _ A12 A23).
rewrite nat_compare_lt, <- nat_of_Ncompare in A13; rewrite A13.
exact (fun _ => refl_equal _).
intros Abs; discriminate Abs.
intros Abs; discriminate Abs.
(* 1/1 *)
intro s1; induction s1 as [ | a1 s1]; intros [ | a2 s2]; simpl.
apply refl_equal.
apply refl_equal.
apply refl_equal.
case_eq (Ncompare (N_of_ascii a1) (N_of_ascii a2)); intro A12;
rewrite <- Ncompare_antisym, A12; simpl.
apply IHs1.
apply refl_equal.
apply refl_equal.
Defined.

Ltac oset_compare_tac :=
  match goal with
    | |- Oset.compare ?OS ?a1 ?a2 = Eq -> 
         Oset.compare ?OS ?a2 ?a3 = Eq ->
         Oset.compare ?OS ?a1 ?a3 = Eq =>
      apply Oset.compare_eq_trans
    | |- Oset.compare ?OS ?a1 ?a2 = Eq -> 
         Oset.compare ?OS ?a2 ?a3 = Lt ->
         Oset.compare ?OS ?a1 ?a3 = Lt =>
      apply Oset.compare_eq_lt_trans
    | |- Oset.compare ?OS ?a1 ?a2 = Lt -> 
         Oset.compare ?OS ?a2 ?a3 = Eq ->
         Oset.compare ?OS ?a1 ?a3 = Lt =>
      apply Oset.compare_lt_eq_trans
    | |- Oset.compare ?OS ?a1 ?a2 = Lt -> 
         Oset.compare ?OS ?a2 ?a3 = Lt ->
         Oset.compare ?OS ?a1 ?a3 = Lt =>
      apply Oset.compare_lt_trans
    | |- Oset.compare ?OS ?a1 ?a2 = CompOpp (Oset.compare ?OS ?a2 ?a1) =>
      apply Oset.compare_lt_gt

    | H : Oset.eq_bool ?OS ?x1 ?x2 = true |- ?x1 = ?x2  =>
      rewrite Oset.eq_bool_true_iff in H; apply H
    | H : match Oset.compare ?OS ?x1 ?x2 with
           | Eq => true
           | Lt => false
           | Gt => false
         end = true |- ?x1 = ?x2  =>
      revert H;
        generalize (Oset.eq_bool_ok OS x1 x2);
        case (Oset.compare OS x1 x2);
        [ exact (fun h _ => h) 
        | let Abs := fresh "Abs" in intros _ Abs; discriminate Abs
        | let Abs := fresh "Abs" in intros _ Abs; discriminate Abs]
    | |- match Oset.compare ?OS ?x1 ?x2 with
           | Eq => true
           | Lt => false
           | Gt => false
         end = true -> ?x1 = ?x2 =>
       generalize (Oset.eq_bool_ok OS x1 x2);
         case (Oset.compare OS x1 x2);
         [ exact (fun h _ => h) 
         | let Abs := fresh "Abs" in intros _ Abs; discriminate Abs
         | let Abs := fresh "Abs" in intros _ Abs; discriminate Abs]
    | |- match Oset.compare ?OS ?x1 ?x2 with
           | Eq => ?x1 = ?x2
           | Lt => ?x1 <> ?x2
           | Gt => ?x1 <> ?x2
         end =>
      apply (Oset.eq_bool_ok OS x1 x2)
    | |- match Oset.compare ?OS ?x1 ?x2 with
           | Eq => ?f ?x1 = ?f ?x2
           | Lt => ?f ?x1 <> ?f ?x2
           | Gt => ?f ?x1 <> ?f ?x2
         end =>
      generalize (Oset.eq_bool_ok OS x1 x2);
        case (Oset.compare OS x1 x2);
        [apply f_equal 
        | let H1 := fresh in 
          let H2 := fresh in
          intros H1 H2;
            apply H1; injection H2; exact (fun h => h)
        | let H1 := fresh in 
          let H2 := fresh in
          intros H1 H2;
            apply H1; injection H2; exact (fun h => h)]
    | |- match Oset.compare ?OS ?x1 ?x2 with
           | Eq => ?f2 (?f1 ?x1) = ?f2 (?f1 ?x2)
           | Lt => ?f2 (?f1 ?x1) <> ?f2 (?f1 ?x2)
           | Gt => ?f2 (?f1 ?x1) <> ?f2 (?f1 ?x2)
         end =>
      generalize (Oset.eq_bool_ok OS x1 x2);
        case (Oset.compare OS x1 x2);
        [apply (f_equal (fun x => f2 (f1 x)))
        | let H1 := fresh in 
          let H2 := fresh in
          intros H1 H2;
            apply H1; injection H2; exact (fun h => h)
        | let H1 := fresh in 
          let H2 := fresh in
          intros H1 H2;
            apply H1; injection H2; exact (fun h => h)]
  end.

