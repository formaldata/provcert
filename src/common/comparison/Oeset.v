(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)


Set Implicit Arguments.

Require Import Bool List String Ascii Sorted SetoidList.

Require Import BasicFacts ListFacts Mem NArith ListPermut Comparison.

Module Oeset.
Record Rcd (A : Type) : Type :=
  mk_R 
    {
      compare : A -> A -> comparison;
      compare_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Eq -> compare a1 a3 = Eq;
      compare_eq_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Eq -> compare a1 a3 = Lt;
      compare_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_gt : forall a1 a2, compare a1 a2 = CompOpp (compare a2 a1)
    }.

Section Sec.

Hypothesis A : Type.
Hypothesis OA : Rcd A.

Open Scope N.

Definition eq_bool := (fun x y => match compare OA x y with Eq => true | _ => false end).
Definition mem_bool a l := Mem.mem_bool eq_bool a l.
Definition all_diff_bool l := Mem.all_diff_bool eq_bool l.
Fixpoint nb_occ a l : N :=
  match l with
    | nil => 0
    | a1 :: l => (match compare OA a a1 with Eq => 1 | _ => 0 end) + nb_occ a l
  end.

Lemma mem_bool_unfold : 
  forall a l, 
    mem_bool a l = 
    match l with
      | nil => false
      | a1 :: l => match compare OA a a1 with Eq => true | _ => false end || mem_bool a l
    end.
Proof.
intros a l; case l.
- apply refl_equal.
- clear l; intros a1 l; simpl.
  apply refl_equal.
Qed.

Lemma all_diff_bool_unfold :
  forall a l, all_diff_bool (a :: l) = negb (mem_bool a l) && (all_diff_bool l).
Proof.
intros a l; unfold all_diff_bool, mem_bool.
apply Mem.all_diff_bool_unfold.
Qed.

Lemma nb_occ_unfold :
  forall a l, nb_occ a l = 
              match l with
                | nil => 0
                | a1 :: l => (match compare OA a a1 with Eq => 1 | _ => 0 end) + nb_occ a l
              end.                                                                                        
Proof.
intros a l; destruct l; apply refl_equal.
Qed.

Lemma compare_eq_refl :
  forall a, compare OA a a = Eq.
Proof.
intros a.
generalize (compare_lt_gt OA a a).
case (compare OA a a); try discriminate.
exact (fun _ => refl_equal _).
Qed.

Lemma compare_eq_refl_alt :
  forall a1 a2, a1 = a2 -> compare OA a1 a2 = Eq.
Proof.
intros a1 a2 H; subst a2.
apply compare_eq_refl.
Qed.

Lemma compare_eq_sym :
  forall a b, compare OA a b = Eq -> compare OA b a = Eq.
Proof.
intros a b H.
rewrite compare_lt_gt, H.
apply refl_equal.
Qed.

Lemma compare_eq_gt_trans :
  forall a b c, compare OA a b = Eq -> compare OA b c = Gt -> compare OA a c = Gt.
Proof.
intros a b c H1 H2.
rewrite compare_lt_gt, CompOpp_iff in H2.
rewrite compare_lt_gt, CompOpp_iff.
apply compare_lt_eq_trans with b; [apply H2 | ].
apply compare_eq_sym; assumption.
Qed.

Lemma compare_gt_eq_trans :
  forall a b c, compare OA a b = Gt -> compare OA b c = Eq -> compare OA a c = Gt.
Proof.
intros a b c H1 H2.
rewrite compare_lt_gt, CompOpp_iff in H1.
rewrite compare_lt_gt, CompOpp_iff.
apply compare_eq_lt_trans with b; [ | apply H1].
apply compare_eq_sym; assumption.
Qed. 

Lemma compare_gt_trans :
  forall a b c, compare OA a b = Gt -> compare OA b c = Gt -> compare OA a c = Gt.
Proof.
intros a b c H1 H2.
rewrite compare_lt_gt, CompOpp_iff in H1, H2.
rewrite compare_lt_gt, CompOpp_iff.
apply compare_lt_trans with b; [apply H2 | apply H1].
Qed.

Lemma compare_eq_1 :
  forall a1 a2 b, compare OA a1 a2 = Eq -> compare OA a1 b = compare OA a2 b.
Proof.
intros a1 a2 b Ha.
case_eq (compare OA a2 b); intro Hb.
- apply (compare_eq_trans OA _ _ _ Ha Hb).
- apply (compare_eq_lt_trans OA _ _ _ Ha Hb).
- apply (compare_eq_gt_trans _ _ _ Ha Hb).
Qed.

Lemma compare_eq_2 :
  forall a b1 b2, compare OA b1 b2 = Eq -> compare OA a b1 = compare OA a b2.
Proof.
intros a b1 b2 Hb; apply sym_eq.
case_eq (compare OA a b1); intro Ha.
- apply (compare_eq_trans OA _ _ _ Ha Hb).
- apply (compare_lt_eq_trans OA _ _ _ Ha Hb).
- apply (compare_gt_eq_trans _ _ _ Ha Hb).
Qed.

Lemma eq_bool_eq_1 :
  forall x1 x2 x, eq_bool x1 x2 = true -> eq_bool x1 x = eq_bool x2 x.
Proof.
intros x1 x2 x H; unfold eq_bool in *.
rewrite compare_eq_true in H.
rewrite (compare_eq_1 _ _ _ H); apply refl_equal.
Qed.

Lemma eq_bool_eq_2 :
  forall x1 x2 x, eq_bool x1 x2 = true -> eq_bool x x1 = eq_bool x x2.
Proof.
intros x1 x2 x H; unfold eq_bool in *.
rewrite compare_eq_true in H.
rewrite (compare_eq_2 _ _ _ H); apply refl_equal.
Qed.

Lemma mem_bool_eq_1 :
  forall a1 a2 l, compare OA a1 a2 = Eq -> mem_bool a1 l = mem_bool a2 l.
Proof.
fix 3.
intros a1 a2 l Ha; case l; clear l.
- apply refl_equal.
- intros a l; rewrite (mem_bool_unfold a1), (mem_bool_unfold a2).
  apply f_equal2; [ | apply (mem_bool_eq_1 a1 a2 l Ha)].
  case_eq (compare OA a1 a); intro H.
  + assert (K := compare_eq_sym _ _ Ha).
    rewrite (compare_eq_trans _ _ _ _ K H).
    apply refl_equal.
  + assert (K := compare_eq_sym _ _ Ha).
    rewrite (compare_eq_lt_trans _ _ _ _ K H).
    apply refl_equal.
  + rewrite compare_lt_gt.
    rewrite compare_lt_gt, CompOpp_iff in H.
    rewrite (compare_lt_eq_trans _ _ _ _ H Ha).
    apply refl_equal.
Qed.

Lemma mem_bool_eq_2 :
  forall a l1 l2, comparelA (compare OA) l1 l2 = Eq -> mem_bool a l1 = mem_bool a l2.
Proof.
intros a l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] H; try discriminate H.
- apply refl_equal.
- simpl in H.
  case_eq (compare OA a1 a2); intro Ha; rewrite Ha in H; try discriminate H.
  simpl; apply f_equal2; [ | apply IHl1; trivial].
  unfold eq_bool.
  case_eq (compare OA a a1); intro Ha1.
  + rewrite (compare_eq_trans _ _ _ _ Ha1 Ha); trivial.
  + rewrite (compare_lt_eq_trans _ _ _ _ Ha1 Ha); trivial.
  + rewrite (compare_gt_eq_trans _ _ _ Ha1 Ha); trivial.
Qed.    

Lemma nb_occ_eq_1 :
  forall a1 a2 l, compare OA a1 a2 = Eq -> nb_occ a1 l = nb_occ a2 l.
Proof.
intros a1 a2 l Ha; induction l as [ | a l]; [apply refl_equal | ].
rewrite 2 (nb_occ_unfold _ (a :: l)); apply f_equal2; [ | apply IHl].
case_eq (compare OA a2 a); intro Ka.
- rewrite (compare_eq_trans _ _ _ _ Ha Ka); apply refl_equal.
- rewrite (compare_eq_lt_trans _ _ _ _ Ha Ka); apply refl_equal.
- rewrite (compare_eq_gt_trans _ _ _ Ha Ka); apply refl_equal.
Qed.

Lemma nb_occ_eq_2 :
  forall a l1 l2, comparelA (compare OA) l1 l2 = Eq -> nb_occ a l1 = nb_occ a l2.
Proof.
intros a l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] H; try discriminate H.
- apply refl_equal.
- simpl in H.
  case_eq (compare OA a1 a2); intro Ha; rewrite Ha in H; try discriminate H.
  simpl; apply f_equal2; [ | apply IHl1; trivial].
  rewrite (compare_eq_2 _ _ _ Ha); apply refl_equal.
Qed.

Lemma Equivalence :
  RelationClasses.Equivalence (fun x y => eq_bool x y = true).
Proof.
unfold eq_bool; split.
- intro x; rewrite compare_eq_refl; apply refl_equal.
- intros x y H; rewrite compare_eq_true in H.
  rewrite compare_lt_gt, H; apply refl_equal.
- intros x y z; rewrite 3 compare_eq_true; apply compare_eq_trans.
Qed.

Lemma StrictOrder : RelationClasses.StrictOrder (fun x y => compare OA x y = Lt).
Proof.
split.
- intros x Hx.
  rewrite compare_eq_refl in Hx; discriminate Hx.
- intros x y z; apply compare_lt_trans.
Qed.

Lemma Compat :
  Morphisms.Proper
   (Morphisms.respectful (fun x y  => eq_bool x y = true)
      (Morphisms.respectful (fun x y => eq_bool x y = true) iff))
   (fun x y => compare OA x y = Lt).
Proof.
intros x1 x2 Hx y1 y2 Hy.
unfold eq_bool in Hx, Hy; rewrite compare_eq_true in Hx, Hy.
rewrite (compare_eq_1 _ _ _ Hx), (compare_eq_2 _ _ _ Hy).
split; exact (fun h => h).
Qed.

Lemma eq_bool_true_compare_eq :
  forall x y, eq_bool x y = true <-> compare OA x y = Eq.
Proof.
intros x y; split; unfold eq_bool.
destruct (compare OA x y); trivial; discriminate.
intro H; rewrite H; trivial.
Qed.

Lemma eq_bool_refl :
  forall x, eq_bool x x = true.
Proof.
intro x; unfold eq_bool; rewrite compare_eq_refl; apply refl_equal.
Qed.

Lemma eq_bool_sym : forall x y, eq_bool x y = eq_bool y x.
Proof.
intros x y; unfold eq_bool; rewrite compare_lt_gt.
case (compare OA y x); apply refl_equal.
Qed.

Lemma in_mem_bool :
  forall a l, In a l -> mem_bool a l = true.
Proof.
intros a l H; induction l as [ | a1 l]; simpl in H; [contradiction H |  destruct H as [H | H]].
- subst a1; simpl; unfold eq_bool; rewrite compare_eq_refl; apply refl_equal.
- simpl; rewrite IHl, Bool.orb_true_r; trivial.
Qed.


Lemma mem_bool_true_iff : 
  forall a l, mem_bool a l = true <-> (exists a', compare OA a a' = Eq /\ In a' l).
Proof.
intros a l; unfold mem_bool, eq_bool; rewrite Mem.mem_bool_true_iff; 
split; intros [a' [H1 H2]].
- rewrite compare_eq_true in H1.
  exists a'; split; trivial.
- exists a'; rewrite compare_eq_true; split; trivial.
Qed.

Lemma mem_bool_app :
forall a l1 l2, mem_bool a (l1 ++ l2) = mem_bool a l1 || mem_bool a l2.
Proof.
intros; apply Mem.mem_bool_app.
Qed.

Lemma mem_bool_rev :
  forall a l,  mem_bool a (rev l) = mem_bool a l.
Proof.
intros a l; induction l as [ | a1 l]; simpl; [apply refl_equal | ].
rewrite mem_bool_app, IHl, Bool.orb_comm; apply f_equal2; [ | apply refl_equal].
rewrite (mem_bool_unfold _ (_ :: _)), Bool.orb_false_r; apply refl_equal.
Qed.

Lemma mem_bool_id :
  forall l f, (forall x, mem_bool x l = true -> compare OA (f x) x = Eq) ->
                forall x, mem_bool x (map f l) = mem_bool x l.
Proof.
intro l; induction l as [ | a1 l]; intros f H x; simpl; [apply refl_equal | ].
apply f_equal2.
- assert (Ha1 : compare OA (f a1) a1 = Eq).
  {
    apply H; simpl; unfold eq_bool; rewrite compare_eq_refl.
    apply refl_equal.
  }
  rewrite compare_lt_gt, CompOpp_iff in Ha1.
  unfold eq_bool; case_eq (compare OA x a1); intro H1.
  + rewrite (compare_eq_trans _ _ _ _ H1 Ha1); apply refl_equal.
  + rewrite (compare_lt_eq_trans _ _ _ _ H1 Ha1); apply refl_equal.
  + rewrite (compare_gt_eq_trans _ _ _ H1 Ha1); apply refl_equal.
- apply IHl; intros a Ha; apply H; simpl; rewrite Ha, Bool.orb_true_r; apply refl_equal.
Qed.

Lemma mem_bool_filter :
  forall f l, (forall x1 x2, mem_bool x1 l = true -> compare OA x1 x2 = Eq -> f x1 = f x2) ->
                  forall x, mem_bool x (filter f l) = andb (f x) (mem_bool x l).
Proof.
intros f l H x; induction l as [ | a1 l]; simpl.
- rewrite Bool.andb_false_r; apply refl_equal.
- case_eq (eq_bool x a1); intro Hx.
  + unfold eq_bool in Hx.
    rewrite compare_eq_true in Hx.
    assert (Kx : f x = f a1).
    {
      apply H; [ | assumption].
      rewrite mem_bool_unfold; rewrite Hx; apply refl_equal.
    }
    rewrite Kx; case_eq (f a1); intro Ha1.
    * rewrite mem_bool_unfold, Hx; apply refl_equal.
    * rewrite IHl, Kx, Ha1; [apply refl_equal | ].
      intros x1 x2 K; apply H.
      rewrite mem_bool_unfold, K, Bool.orb_true_r; apply refl_equal.
  + destruct (f a1); simpl.
    * rewrite Hx, IHl; [apply refl_equal | ].
      intros x1 x2 K; apply H.
      rewrite mem_bool_unfold, K, Bool.orb_true_r; apply refl_equal.
    * apply IHl; intros x1 x2 K; apply H.
      rewrite mem_bool_unfold, K, Bool.orb_true_r; apply refl_equal.
Qed.

(* Lemma mem_filter : *)
(*   forall (f : A -> bool) l,  *)
(*     (forall x1 x2, mem_bool x1 l = true -> compare OA x1 x2 = Eq -> f x1 = f x2) -> *)
(*     forall x, mem_bool x (filter f l) = if f x then mem_bool x l else false. *)
(* Proof. *)
(* intros f l Hl x; induction l as [ | a1 l]; [destruct (f x); apply refl_equal | simpl]. *)
(* unfold eq_bool. *)
(* case_eq (compare OA x a1); intro Hx. *)
(* - assert (Kx : f x = f a1). *)
(*   { *)
(*     apply Hl; [ | assumption]. *)
(*     simpl; unfold eq_bool; rewrite Hx; apply refl_equal. *)
(*   }     *)
(*   rewrite Kx; case_eq (f a1); intro Ha1. *)
(*   + rewrite mem_bool_unfold, Hx; apply f_equal. *)
(*     rewrite IHl, Kx, Ha1; [apply refl_equal | ]. *)
(*     intros y1 y2 Hy; apply Hl; simpl. *)
(*     rewrite Hy; apply Bool.orb_true_r. *)
(*   + rewrite IHl, Kx, Ha1; [apply refl_equal | ]. *)
(*     intros y1 y2 Hy; apply Hl; simpl. *)
(*     rewrite Hy; apply Bool.orb_true_r. *)
(* - simpl; rewrite <- IHl. *)
(*   + destruct (f a1); simpl; try rewrite Hx; trivial. *)
(*     unfold eq_bool; rewrite Hx; apply refl_equal. *)
(*   + intros y1 y2 Hy; apply Hl; simpl. *)
(*     rewrite Hy; apply Bool.orb_true_r. *)
(* - simpl; rewrite <- IHl. *)
(*   + destruct (f a1); simpl; try rewrite Hx; trivial. *)
(*     unfold eq_bool; rewrite Hx; apply refl_equal. *)
(*   + intros y1 y2 Hy; apply Hl; simpl. *)
(*     rewrite Hy; apply Bool.orb_true_r. *)
(* Qed. *)

Lemma mem_bool_filter_eq_2 :
  forall f1 f2 l, (forall x, mem_bool x l = true -> f1 x = f2 x) ->
                  forall x, mem_bool x (filter f1 l) = mem_bool x (filter f2 l).
Proof.
intros f1 f2 l H x; induction l as [ | a1 l]; [apply refl_equal | simpl].
rewrite (H a1); 
  [ | simpl; rewrite Bool.orb_true_iff; left; 
      unfold eq_bool; rewrite compare_eq_refl; apply refl_equal].
destruct (f2 a1); [simpl; apply f_equal | ];
apply IHl; intros y Hy; apply H; simpl; rewrite Hy; apply Bool.orb_true_r.
Qed.

Lemma not_mem_nb_occ : forall a l, mem_bool a l = false -> nb_occ a l = 0.
Proof.
intros a l; induction l as [ | a1 l]; intro H; [apply refl_equal | ].
rewrite mem_bool_unfold, Bool.orb_false_iff in H; destruct H as [H1 H2].
rewrite nb_occ_unfold, (IHl H2).
case_eq (compare OA a a1); intro Ha; rewrite Ha in H1; (trivial || discriminate H1).
Qed.

Lemma mem_nb_occ : forall a l, mem_bool a l = true -> nb_occ a l <> 0.
Proof.
intros a l; induction l as [ | a1 l]; intro H; [discriminate | ].
rewrite mem_bool_unfold, Bool.orb_true_iff in H; destruct H as [H1 | H2].
- rewrite compare_eq_true in H1.
  rewrite nb_occ_unfold, H1; destruct (nb_occ a l); discriminate.
- rewrite nb_occ_unfold; generalize (IHl H2).
  case_eq (compare OA a a1); trivial.
  intros _; destruct (nb_occ a l); simpl; trivial.
  + intro; discriminate.
  + intros _; destruct p; discriminate.
Qed.

Lemma mem_nb_occ_pos : forall a l, mem_bool a l = true -> nb_occ a l > 0.
Proof.
intros a l Ha.
assert (Ka := mem_nb_occ a l Ha).
case_eq (nb_occ a l).
- intro H; rewrite H in Ka; apply False_rec; apply Ka; apply refl_equal.
- intros p _; apply refl_equal.
Qed.

Lemma nb_occ_mem : forall a l, nb_occ a l <> 0 -> mem_bool a l = true.
Proof.
intros a l H; generalize (not_mem_nb_occ a l).
case (mem_bool a l); [intros _; apply refl_equal | ].
intro K; rewrite (K (refl_equal _)) in H.
apply False_rec; apply H; apply refl_equal.
Qed.

Lemma nb_occ_not_mem : forall a l, nb_occ a l = 0 -> mem_bool a l = false.
Proof.
intros a l H; generalize (mem_nb_occ a l).
case (mem_bool a l); [ | intros _; apply refl_equal].
intro K; rewrite H in K; apply False_rec; apply (K (refl_equal _)).
apply refl_equal.
Qed.

Lemma in_nb_occ : forall a l, In a l -> nb_occ a l <> 0.
Proof.
intros a l H; apply mem_nb_occ.
rewrite mem_bool_true_iff.
exists a; split; [apply compare_eq_refl | assumption].
Qed.

Lemma nb_occ_list_size :
  forall a l, nb_occ a l = 
              N.of_nat (list_size 
                          (fun x => match compare OA a x with Eq => 1%nat | _ => 0%nat end) l).
Proof.
intros a l; induction l as [ | a1 l]; simpl; trivial.
rewrite Nat2N.inj_add; apply f_equal2; [ | apply IHl].
case (compare OA a a1); apply refl_equal.
Qed.

Lemma nb_occ_app :
  forall a l1 l2, nb_occ a (l1 ++ l2) = nb_occ a l1 + nb_occ a l2.
Proof.
intros a l1; induction l1 as [ | a1 l1]; intros l2; simpl; [trivial | ].
rewrite IHl1, N.add_assoc; apply refl_equal.
Qed.

Lemma nb_occ_if :
  forall a (b : bool) l1 l2, 
    nb_occ a (if b then l1 else l2) = if b then nb_occ a l1 else nb_occ a l2.
Proof.
intros a [|] l1 l2; apply refl_equal.
Qed.

Lemma nb_occ_diff_0_pos : forall a l, nb_occ a l <> 0 <-> 0 < nb_occ a l.
Proof.
intros a l; apply N.neq_0_lt_0.
Qed.

Lemma nb_occ_id :
  forall l f, (forall x, mem_bool x l = true -> compare OA (f x) x = Eq) ->
                forall x, nb_occ x (map f l) = nb_occ x l.
Proof.
intro l; induction l as [ | a1 l]; intros f H x; simpl; [apply refl_equal | ].
apply f_equal2.
- assert (Ha1 : compare OA (f a1) a1 = Eq).
  {
    apply H; simpl; unfold eq_bool; rewrite compare_eq_refl.
    apply refl_equal.
  }
  rewrite compare_lt_gt, CompOpp_iff in Ha1.
  case_eq (compare OA x a1); intro H1.
  + rewrite (compare_eq_trans _ _ _ _ H1 Ha1); apply refl_equal.
  + rewrite (compare_lt_eq_trans _ _ _ _ H1 Ha1); apply refl_equal.
  + rewrite (compare_gt_eq_trans _ _ _ H1 Ha1); apply refl_equal.
- apply IHl; intros a Ha; apply H; simpl; rewrite Ha, Bool.orb_true_r; apply refl_equal.
Qed.

Lemma nb_occ_filter :
  forall (f : A -> bool) l, 
    (forall x1 x2, mem_bool x1 l = true -> compare OA x1 x2 = Eq -> f x1 = f x2) ->
    forall x, nb_occ x (filter f l) = if f x then nb_occ x l else 0.
Proof.
intros f l Hl x; induction l as [ | a1 l]; [destruct (f x); apply refl_equal | simpl].
case_eq (compare OA x a1); intro Hx.
- assert (Kx : f x = f a1).
  {
    apply Hl; [ | assumption].
    simpl; unfold eq_bool; rewrite Hx; apply refl_equal.
  }    
  rewrite Kx; case_eq (f a1); intro Ha1.
  + rewrite nb_occ_unfold, Hx; apply f_equal.
    rewrite IHl, Kx, Ha1; [apply refl_equal | ].
    intros y1 y2 Hy; apply Hl; simpl.
    rewrite Hy; apply Bool.orb_true_r.
  + rewrite IHl, Kx, Ha1; [apply refl_equal | ].
    intros y1 y2 Hy; apply Hl; simpl.
    rewrite Hy; apply Bool.orb_true_r.
- simpl; rewrite <- IHl.
  + destruct (f a1); simpl; try rewrite Hx; trivial.
  + intros y1 y2 Hy; apply Hl; simpl.
    rewrite Hy; apply Bool.orb_true_r.
- simpl; rewrite <- IHl.
  + destruct (f a1); simpl; try rewrite Hx; trivial.
  + intros y1 y2 Hy; apply Hl; simpl.
    rewrite Hy; apply Bool.orb_true_r.
Qed.

Lemma nb_occ_filter_alt :
  forall (f : A -> bool) l, 
    (forall x1 x2, mem_bool x1 l = true -> compare OA x1 x2 = Eq -> f x1 = f x2) ->
    forall x, nb_occ x (filter f l) = (nb_occ x l) * (if f x then 1 else 0).
Proof.
intros f l Hl x.
rewrite (nb_occ_filter _ _ Hl).
case (f x).
- rewrite N.mul_1_r; apply refl_equal.
- rewrite N.mul_0_r; apply refl_equal.
Qed.

Lemma nb_occ_filter_eq :
  forall f1 f2 s1 s2, 
    (forall x, nb_occ x s1 = nb_occ x s2) ->
    (forall x1 x2, nb_occ x1 s1 >= 1 -> compare OA x1 x2 = Eq -> f1 x1 = f2 x2) -> 
    forall x, nb_occ x (filter f1 s1) = nb_occ x (filter f2 s2).
Proof.
intros f1 f2 s1 s2 Hs Ks t.
rewrite 2 nb_occ_filter, <- Hs.
- case_eq (nb_occ t s1); [intro Ht | ].
  + case (f1 t); case (f2 t); trivial.
  + intros p Hp; apply if_eq; trivial.
    apply Ks.
    * rewrite Hp; destruct p; discriminate.
    * apply compare_eq_refl.
- intros e1 e2 He1 He.
  assert (Ke1 := mem_nb_occ _ _ He1).
  rewrite <- Hs in Ke1.
  case_eq (nb_occ e1 s1); [intro Je1 | intros p Hp].
  + rewrite Je1 in Ke1; apply False_rec; apply Ke1; apply refl_equal.
  + rewrite <- (Ks e1 e2).
    * apply sym_eq; apply Ks; [ | apply compare_eq_refl].
      rewrite Hp; destruct p; discriminate.
    * rewrite Hp; destruct p; discriminate.
    *  assumption.
- intros e1 e2 He1 He.
  assert (Ke1 := mem_nb_occ _ _ He1).
  case_eq (nb_occ e1 s1); [intro Je1 | intros p Hp].
  + rewrite Je1 in Ke1; apply False_rec; apply Ke1; apply refl_equal.
  + rewrite (Ks e1 e2).
    * apply sym_eq; apply Ks; [ | apply compare_eq_refl].
      rewrite <- (nb_occ_eq_1 _ _ _ He), Hp; destruct p; discriminate.
    * rewrite Hp; destruct p; discriminate.
    *  assumption.
Qed.

Lemma nb_occ_partition_1 :
  forall (f : A -> bool) l, 
    (forall x1 x2, mem_bool x1 l = true -> compare OA x1 x2 = Eq -> f x1 = f x2) ->
    forall x, nb_occ x (fst (partition f l)) = if f x then nb_occ x l else 0%N.
Proof.
intros f l Hf x; induction l as [ | a1 l]; [destruct (f x); apply refl_equal | ].
assert (IH : nb_occ x (fst (partition f l)) = (if f x then nb_occ x l else 0)).
{
  apply IHl.
  do 3 intro; apply Hf; simpl.
  rewrite Bool.orb_true_iff; right; assumption.
}
simpl.
destruct (partition f l) as [l1 l2]; simpl in IH; simpl.
case_eq (f a1); intro Ha1; simpl.
- case_eq (compare OA x a1); intro Hx.
  + rewrite IH, (Hf x a1), Ha1; trivial.
    rewrite mem_bool_unfold, Hx; apply refl_equal.
  + rewrite IH; apply refl_equal.
  + rewrite IH; apply refl_equal.
- rewrite IH; case_eq (compare OA x a1); intro Hx.
  + rewrite (Hf x a1), Ha1; trivial.
    rewrite mem_bool_unfold, Hx; apply refl_equal.
  + apply refl_equal.
  + apply refl_equal.
Qed.

Lemma nb_occ_partition_2 :
  forall (f : A -> bool) l, 
    (forall x1 x2, mem_bool x1 l = true -> compare OA x1 x2 = Eq -> f x1 = f x2) ->
    forall x, nb_occ x (snd (partition f l)) = if f x then 0%N else nb_occ x l.
Proof.
intros f l Hf x; induction l as [ | a1 l]; [destruct (f x); apply refl_equal | ].
assert (IH : nb_occ x (snd (partition f l)) = (if f x then 0 else nb_occ x l)).
{
  apply IHl.
  do 3 intro; apply Hf; simpl.
  rewrite Bool.orb_true_iff; right; assumption.
}
simpl.
destruct (partition f l) as [l1 l2]; simpl in IH; simpl.
case_eq (f a1); intro Ha1; simpl.
- case_eq (compare OA x a1); intro Hx.
  + rewrite IH, (Hf x a1), Ha1; trivial.
    rewrite mem_bool_unfold, Hx; apply refl_equal.
  + rewrite IH; apply refl_equal.
  + rewrite IH; apply refl_equal.
- rewrite IH; case_eq (compare OA x a1); intro Hx.
  + rewrite (Hf x a1), Ha1; trivial.
    rewrite mem_bool_unfold, Hx; apply refl_equal.
  + apply refl_equal.
  + apply refl_equal.
Qed.

Lemma all_diff_bool_ok :
  forall l, all_diff_bool l = true -> all_diff l.
Proof.
intro l; induction l as [ | a l]; intro H; [simpl; trivial | ].
rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in H.
rewrite all_diff_unfold; split.
- intros b Hb K; subst b.
  rewrite (in_mem_bool _ _ Hb) in H; discriminate (proj1 H).
- apply IHl; apply (proj2 H).
Qed.

Lemma all_diff_bool_nb_occ_mem :
  forall l, all_diff_bool l = true -> forall a, nb_occ a l = if mem_bool a l then 1 else 0.
Proof.
intro l; induction l as [ | a1 l]; intros H a; simpl; [apply refl_equal | ].
rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in H.
case_eq (compare OA a a1); intro Ha.
- unfold eq_bool; rewrite Ha, Bool.Bool.orb_true_l, (IHl (proj2 H)).
  case_eq (mem_bool a l); [ | intros; apply refl_equal].
  intro Ka.
  rewrite <- (mem_bool_eq_1 _ _ _ Ha), Ka in H; discriminate (proj1 H).
- unfold eq_bool; rewrite Ha; simpl; apply IHl; apply (proj2 H).
- unfold eq_bool; rewrite Ha; simpl; apply IHl; apply (proj2 H).
Qed.

Lemma all_diff_bool_false :
  forall l, all_diff_bool l = false ->
            exists a1, exists a2, exists l1, exists l2, exists l3,
                      compare OA a1 a2 = Eq /\ l = l1 ++ a1 :: l2 ++ a2 :: l3.
Proof.
intros l H; induction l as [ | a l]; [discriminate H | ].
rewrite all_diff_bool_unfold, Bool.Bool.andb_false_iff in H; destruct H as [H | H].
- clear IHl; rewrite negb_false_iff in H.
  induction l as [ | a' l]; [discriminate H | ].
  rewrite mem_bool_unfold, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
  + case_eq (compare OA a a'); intro Ha; rewrite Ha in H; try discriminate H.
    exists a; exists a'; exists nil; exists nil; exists l; split; [assumption | apply refl_equal].
  + destruct (IHl H) as [a1 [a2 [l1 [l2 [l3 [IH1 IHl2]]]]]].
    exists a1; exists a2.
    destruct l1 as [ | b1 l1]; simpl in IHl2; injection IHl2; clear IHl2; do 2 intro; subst a l.
    * exists nil; exists (a' :: l2); exists l3; split; [assumption | apply refl_equal].
    * exists (b1 :: a' :: l1); exists l2; exists l3; split; [assumption | apply refl_equal].
- destruct (IHl H) as [a1 [a2 [l1 [l2 [l3 [IH1 IH2]]]]]].
  exists a1; exists a2; exists (a :: l1); exists l2; exists l3; 
    split; [ | simpl; apply f_equal]; assumption.
Qed.

Lemma all_diff_bool_app_1 :
  forall l1 l2, all_diff_bool (l1 ++ l2) = true -> all_diff_bool l1 = true.
Proof.
intros l1 l2; apply Mem.all_diff_bool_app1.
Qed.

Lemma all_diff_bool_app_2 :
  forall l1 l2, all_diff_bool (l1 ++ l2) = true -> all_diff_bool l2 = true.
Proof.
intros l1 l2; apply Mem.all_diff_bool_app2.
Qed.

Lemma all_diff_bool_app :
  forall l1 l2, all_diff_bool (l1 ++ l2) = true ->
   forall a1 a2, compare OA a1 a2 = Eq -> mem_bool a1 l1 = true -> mem_bool a2 l2 = true -> False.
Proof.
intro l1; induction l1 as [ | a l1]; intros l2 Hl a1 a2 Ha Ha1 Ha2; [discriminate Ha1 | ].
rewrite mem_bool_unfold, Bool.Bool.orb_true_iff, compare_eq_true in Ha1.
case_eq (compare OA a1 a); intro Ka.
- simpl app in Hl; rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in Hl.
  destruct Hl as [Hl _].
  rewrite negb_true_iff, <- not_true_iff_false in Hl.
  apply Hl.
  rewrite mem_bool_app, Bool.Bool.orb_true_iff; right.
  rewrite <- (mem_bool_eq_1 _ _ _ Ka), (mem_bool_eq_1 _ _ _ Ha); apply Ha2.
- rewrite Ka in Ha1; destruct Ha1 as [Ha1 | Ha1]; [discriminate Ha1 | ].
  simpl app in Hl; rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in Hl.
  destruct Hl as [_ Hl].
  apply (IHl1 _ Hl _ _ Ha Ha1 Ha2).  
- rewrite Ka in Ha1; destruct Ha1 as [Ha1 | Ha1]; [discriminate Ha1 | ].
  simpl app in Hl; rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in Hl.
  destruct Hl as [_ Hl].
  apply (IHl1 _ Hl _ _ Ha Ha1 Ha2).  
Qed.

Lemma all_diff_bool_app_iff :
  forall l1 l2, 
    (all_diff_bool l1 = true /\ all_diff_bool l2 = true /\
     (forall a1 a2, compare OA a1 a2 = Eq -> mem_bool a1 l1 = true -> 
                    mem_bool a2 l2 = true -> False)) <->
     all_diff_bool (l1 ++ l2) = true.
Proof.
intros l1 l2; split.
- revert l2; induction l1 as [ | a1 l1]; intros l2 [H1 [H2 H3]]; [apply H2 | ].
  rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in H1.
  simpl app; rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff; split.
  + rewrite mem_bool_app, negb_orb, (proj1 H1), Bool.Bool.andb_true_l.
    case_eq (mem_bool a1 l2); intro H4; [ | apply refl_equal].
    apply False_rec; apply (H3 a1 a1 (compare_eq_refl _)); 
      [simpl; rewrite eq_bool_refl; apply refl_equal | assumption].
  + apply IHl1; split; [apply (proj2 H1) | split; [apply H2 | ]].
    intros a b H Ha Hb; refine (H3 a b H _ Hb); simpl.
    rewrite Ha, Bool.Bool.orb_true_r; apply refl_equal.
- intro H1; split; [ | split].
  + apply (all_diff_bool_app_1 _ _ H1).
  + apply (all_diff_bool_app_2 _ _ H1).
  + apply all_diff_bool_app; assumption.
Qed.

Lemma all_diff_bool_fst :
  forall (B : Type) (l : list (A * B)), 
    all_diff_bool (List.map (@fst _ _) l) = true ->
    forall a b1 b2, List.In (a, b1) l -> List.In (a, b2) l -> b1 = b2.
Proof.
intros B l Hl a b1 b2.
apply all_diff_fst.
apply (all_diff_bool_ok _ Hl).
Qed.

Lemma comparelA_Eq :
  let ltA := fun x y => compare OA x y = Lt in
  forall l1 l2, (forall e, mem_bool e l1 = mem_bool e l2) ->
    Sorted ltA l1 -> Sorted ltA l2 -> comparelA (compare OA) l1 l2 = Eq.
Proof.
intros ltA l1; induction l1 as [ | a1 l1]; intros [ | a2 l2].
- intros _ _ _; apply refl_equal.
- intros H; apply False_rec; generalize (H a2); simpl; unfold eq_bool.
  rewrite compare_eq_refl; discriminate.
- intros H; apply False_rec; generalize (H a1); simpl; unfold eq_bool.
  rewrite compare_eq_refl; discriminate.
- intros H Sl1 Sl2; simpl.
  assert (Aux : compare OA a1 a2 = Eq).
  {
    generalize (H a1) (H a2); simpl; unfold eq_bool; rewrite 2 compare_eq_refl; simpl.
    case_eq (compare OA a1 a2); intro Ha12; simpl.
    - exact (fun _ _ => refl_equal _).
    - rewrite compare_lt_gt, Ha12; simpl.
      intros H1 H2; generalize (sym_eq H1); clear H1; intro H1.
      unfold mem_bool in H1, H2; rewrite Mem.mem_bool_true_iff in H1.
      destruct H1 as [e' [H1 J1]].
      generalize (Sorted_extends (compare_lt_trans OA) Sl2).
      rewrite Forall_forall.
      intro L; assert (L1 := L e' J1).
      assert (Abs : compare OA a1 a1 = Lt).
      {
        apply compare_lt_trans with a2; [assumption | ].
        apply compare_lt_eq_trans with e'; [assumption | ].
        unfold eq_bool in H1; rewrite compare_eq_true in H1.
        rewrite compare_lt_gt, H1; apply refl_equal.
      }
      generalize (compare_lt_gt OA a1 a1); rewrite Abs; discriminate.
    - rewrite compare_lt_gt, Ha12; simpl.
      intros H1 H2; generalize (sym_eq H1); clear H1; intro H1.
      unfold mem_bool in H2; rewrite Mem.mem_bool_true_iff in H2.
      destruct H2 as [e' [H2 J2]].
      generalize (Sorted_extends (compare_lt_trans OA) Sl1).
      rewrite Forall_forall.
      intro L; assert (L2 := L e' J2).
      assert (Abs : compare OA a2 a2 = Lt).
      {
        rewrite compare_lt_gt, CompOpp_iff in Ha12; simpl in Ha12.
        apply compare_lt_trans with a1; [assumption | ].
        apply compare_lt_eq_trans with e'; [assumption | ].
        unfold eq_bool in H2; rewrite compare_eq_true in H2.
        rewrite compare_lt_gt, H2; apply refl_equal.
      }
      generalize (compare_lt_gt OA a2 a2); rewrite Abs; discriminate.
  }
  rewrite Aux; apply IHl1.
  + intros e; generalize (H e); simpl.
    case_eq (compare OA e a1); intro He.
    * intros _.
      {
        case_eq (mem_bool e l1); intro H1.
        - apply False_rec.
          rewrite (mem_bool_eq_1 _ _ _ He), mem_bool_true_iff in H1.
          destruct H1 as [e' [H1 J1]].
          generalize (Sorted_extends (compare_lt_trans OA) Sl1).
          rewrite Forall_forall.
          intro L; assert (L1 := L e' J1).
          assert (Abs : compare OA a1 a1 = Lt).
          {
            apply compare_lt_eq_trans with e'; [assumption | ].
            simpl in H1; rewrite compare_lt_gt, H1; apply refl_equal.
          }
          generalize (compare_lt_gt OA a1 a1); rewrite Abs; discriminate.
        - case_eq (mem_bool e l2); intro H2.
          + apply False_rec.
            rewrite (mem_bool_eq_1 _ _ _ He), mem_bool_true_iff in H2.
            destruct H2 as [e' [H2 J2]].
            generalize (Sorted_extends (compare_lt_trans OA) Sl2).
            rewrite Forall_forall.
            intro L; assert (L2 := L e' J2).
            assert (Abs : compare OA a2 a2 = Lt).
            {
              apply compare_lt_eq_trans with e'; [assumption | ].
              refine (compare_eq_trans _ _ _ _ _ Aux).
              rewrite compare_lt_gt, H2; apply refl_equal.
            }
            rewrite compare_eq_refl in Abs; discriminate Abs.
          + apply refl_equal.
      }
    * unfold eq_bool; rewrite He, (compare_lt_eq_trans _ _ _ _ He Aux). 
      exact (fun h => h).
    * unfold eq_bool; rewrite He, (compare_gt_eq_trans _ _ _ He Aux). 
      exact (fun h => h).
  + inversion Sl1; assumption.
  + inversion Sl2; assumption.
Qed.

Lemma forallb_eq :
  forall f1 f2 l1 l2, 
    (forall x, mem_bool x l1 = mem_bool x l2) ->
    (forall x1 x2, mem_bool x1 l1 = true -> compare OA x1 x2 = Eq -> f1 x1 = f2 x2) ->
    forallb f1 l1 = forallb f2 l2.
Proof.
assert (H : forall f1 f2 l1 l2, 
           (forall x, mem_bool x l1 = mem_bool x l2) ->
           (forall x1 x2, mem_bool x1 l1 = true -> compare OA x1 x2 = Eq -> f1 x1 = f2 x2) ->
           forallb f1 l1 = true -> forallb f2 l2 = true).
{
  intros f1 f2 l1 l2 Hl Hf H.
  rewrite forallb_forall in H; rewrite forallb_forall; intros x Hx.
  assert (Kx : mem_bool x l1 = true).
  {
    rewrite Hl; apply in_mem_bool; assumption.
  }
  rewrite mem_bool_true_iff in Kx.
  destruct Kx as [x' [Kx Hx']].
  rewrite <- (H _ Hx'); apply sym_eq; apply Hf.
  - apply in_mem_bool; assumption.
  - rewrite compare_lt_gt, Kx; apply refl_equal.
}
intros f1 f2 l1 l2 Hl Hf; apply eq_bool_iff; split; [apply H; assumption | ].
apply H.
- intro; rewrite Hl; apply refl_equal.
- intros x1 x2 Hx1 Hx.
  apply sym_eq; apply Hf.
  + rewrite Hl, <- Hx1; apply sym_eq.
    apply mem_bool_eq_1; assumption.
  + rewrite compare_lt_gt, Hx; apply refl_equal.
Qed.

Lemma existsb_eq :
  forall f1 f2 l1 l2, 
    (forall x, mem_bool x l1 = mem_bool x l2) ->
    (forall x1 x2, mem_bool x1 l1 = true -> compare OA x1 x2 = Eq -> f1 x1 = f2 x2) ->
    existsb f1 l1 = existsb f2 l2.
Proof.
assert (H : forall f1 f2 l1 l2, 
           (forall x, mem_bool x l1 = mem_bool x l2) ->
           (forall x1 x2, mem_bool x1 l1 = true -> compare OA x1 x2 = Eq -> f1 x1 = f2 x2) ->
           existsb f1 l1 = true -> existsb f2 l2 = true).
{
  intros f1 f2 l1 l2 Hl Hf H.
  rewrite existsb_exists in H; rewrite existsb_exists.
  destruct H as[x [Hx H]].
  assert (Kx : mem_bool x l1 = true).
  {
    apply in_mem_bool; assumption.
  }
  rewrite Hl, mem_bool_true_iff in Kx.
  destruct Kx as [x' [Kx Hx']].
  exists x'; split; [assumption | ].
  rewrite <- H; apply sym_eq; apply Hf.
  - apply in_mem_bool; assumption.
  - assumption.
}
intros f1 f2 l1 l2 Hl Hf; apply eq_bool_iff; split; [apply H; assumption | ].
apply H.
- intro; rewrite Hl; apply refl_equal.
- intros x1 x2 Hx1 Hx.
  apply sym_eq; apply Hf.
  + rewrite Hl, <- Hx1; apply sym_eq.
    apply mem_bool_eq_1; assumption.
  + rewrite compare_lt_gt, Hx; apply refl_equal.
Qed.

Close Scope N.

Section Permut.

Definition permut := (_permut (fun x y => compare OA x y = Eq)).

(** ** Permutation is a equivalence relation. 
      Reflexivity. *)
  Theorem permut_refl :  forall (l : list A), permut l l.
  Proof.
  intro l; apply _permut_refl.
  intros a _; apply compare_eq_refl.
  Qed.

Lemma permut_refl_alt :
  forall l1 l2, comparelA (compare OA) l1 l2 = Eq -> permut l1 l2.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros [ | a2 l2] H; try discriminate H.
- apply Pnil.
- simpl in H.
  case_eq (compare OA a1 a2); intro Ha; rewrite Ha in H; try discriminate H.
  apply (Pcons (R := fun x y => compare OA x y = Eq) a1 a2 nil l2 Ha (IHl1 _ H)).
Qed.

Lemma permut_refl_alt2 :
  forall l1 l2, l1 = l2 -> permut l1 l2.
  Proof.
    intros l1 l2 H; subst l2; apply permut_refl.
  Qed.

(** Symetry. *)
Lemma permut_sym : forall l1 l2 : list A, permut l1 l2 -> permut l2 l1.
Proof.
  intros l1 l2 P; apply _permut_sym; trivial.
  intros a b _ _ H.
  rewrite compare_lt_gt, H; apply refl_equal.
Qed.

(** Transitivity. *)
Lemma permut_trans :
  forall l1 l2 l3 : list A, permut l1 l2 -> permut l2 l3 -> permut l1 l3.
Proof.
  intros l1 l2 l3 P1 P2; apply _permut_trans with l2; trivial.
  intros a b c _ _ _; apply compare_eq_trans.
Qed.

Lemma permut_length :
  forall l1 l2, permut l1 l2 -> length l1 = length l2.
Proof.
intros l1 l2; unfold permut; apply _permut_length.
Qed.

(** ** Compatibility Properties. 
      Permutation is compatible with mem. *)
Lemma permut_mem_bool_eq :
  forall l1 l2 e, permut l1 l2 -> mem_bool e l1 = mem_bool e l2.
Proof.
fix 1.
intro l1; case l1; clear l1; [ | intros a1 l1]; intros l2 e P.
- inversion P; subst; apply refl_equal.
- inversion P; clear P; subst.
  rewrite mem_bool_unfold, mem_bool_app.
  rewrite (mem_bool_unfold e (b :: l3)).
  case_eq (compare OA e a1); intro He.
  + rewrite (compare_eq_trans _ _ _ _ He H1), Bool.orb_true_r.
    apply refl_equal.
  + rewrite Bool.orb_false_l.
    rewrite (permut_mem_bool_eq _ _ _ H3), mem_bool_app.
    rewrite (compare_lt_eq_trans _ _ _ _ He H1).
    apply refl_equal.
  + rewrite Bool.orb_false_l.
    rewrite (permut_mem_bool_eq _ _ _ H3), mem_bool_app.
    rewrite compare_lt_gt.
    rewrite compare_lt_gt, CompOpp_iff in H1, He.
    rewrite (compare_eq_lt_trans OA _ _ _ H1 He).
    apply refl_equal.
Qed.

Lemma mem_permut_mem_strong :
  forall l1 l2 e, _permut (@eq A) l1 l2 -> mem_bool e l1 = mem_bool e l2.
Proof.
fix 1.
intro l1; case l1; clear l1; [ | intros a1 l1]; intros l2 e P.
- inversion P; subst; apply refl_equal.
- inversion P; clear P; subst.
  rewrite mem_bool_unfold, mem_bool_app.
  rewrite (mem_bool_unfold e (b :: l3)).
  case_eq (compare OA e b); intro He.
  + rewrite Bool.orb_true_r; apply refl_equal.
  + rewrite 2 Bool.orb_false_l.
    rewrite (mem_permut_mem_strong _ _ _ H3), mem_bool_app.
    apply refl_equal.
  + rewrite 2 Bool.orb_false_l.
    rewrite (mem_permut_mem_strong _ _ _ H3), mem_bool_app.
    apply refl_equal.
Qed.

Lemma mem_morph : 
  forall x y, compare OA x y = Eq ->
  forall l1 l2, permut l1 l2 -> (mem_bool x l1 = mem_bool y l2).
Proof.
  intros e1 e2 e1_eq_e2 l1 l2 P.
  rewrite (permut_mem_bool_eq e1 P).
  clear l1 P.
  induction l2 as [ | a l]; simpl; [trivial | ].
  unfold eq_bool; rewrite IHl; apply f_equal2; [ | apply refl_equal].
  case_eq (compare OA e2 a); intro H.
  - rewrite (compare_eq_trans _ _ _ _ e1_eq_e2 H).
    apply refl_equal.
  - rewrite (compare_eq_lt_trans _ _ _ _ e1_eq_e2 H).
    apply refl_equal.
  - rewrite compare_lt_gt, CompOpp_iff in e1_eq_e2, H.
    rewrite compare_lt_gt.
    rewrite (compare_lt_eq_trans _ _ _ _ H e1_eq_e2).
    apply refl_equal.
Qed.

 (** Permutation is compatible with addition and removal of common elements *)
  
Lemma permut_cons :
  forall e1 e2 l1 l2, compare OA e1 e2 = Eq -> (permut l1 l2 <-> permut (e1 :: l1) (e2 :: l2)).
Proof.
intros e1 e2 l1 l2 e1_eq_e2; split; intro P.
- apply (@Pcons _ _ _ e1 e2 l1 (@nil A) l2); trivial.
- replace l2 with (nil ++ l2); trivial;
    apply _permut_cons_inside with e1 e2; trivial.
  intros a1 b1 a2 b2 _ _ _ _ a1_eq_b1 a2_eq_b1 a2_eq_b2.
    apply compare_eq_trans with a2; trivial.
    apply compare_eq_trans with b1; trivial.
    rewrite compare_lt_gt, a2_eq_b1; apply refl_equal.
Qed.

Lemma cons_permut_mem :
  forall l1 l2 e1 e2, compare OA e1 e2 = Eq -> permut (e1 :: l1) l2 -> mem_bool e2 l2 = true.
Proof.
  intros l1 l2 e1 e2 e1_eq_e2 P.
  rewrite <- (mem_morph _ _ e1_eq_e2 P), mem_bool_unfold, compare_eq_refl.
  apply refl_equal.
Qed.

Lemma permut_add_inside :
    forall e1 e2 l1 l2 l3 l4, compare OA e1 e2 = Eq -> 
      (permut (l1 ++ l2) (l3 ++ l4) <-> permut (l1 ++ e1 :: l2) (l3 ++ e2 :: l4)).
Proof.
  intros e1 e2 l1 l2 l3 l4 e1_eq_e2; split; intro P.
  apply _permut_strong; trivial.
  apply _permut_add_inside with e1 e2; trivial.
  intros a1 b1 a2 b2 _ _ _ _ a1_eq_b1 a2_eq_b1 a2_eq_b2;
  apply compare_eq_trans with a2; trivial.
  apply compare_eq_trans with b1; trivial.
  rewrite compare_lt_gt, a2_eq_b1; apply refl_equal.
Qed.

Lemma permut_cons_inside :
  forall e1 e2 l l1 l2, compare OA e1 e2 = Eq ->
    (permut l (l1 ++ l2) <-> permut (e1 :: l) (l1 ++ e2 :: l2)).
Proof.
  intros e1 e2 l l1 l2 e1_eq_e2; apply (permut_add_inside _ _ nil l l1 l2 e1_eq_e2).
Qed.

(** Permutation is compatible with append. *)
Lemma permut_app_1 :
  forall l l1 l2, permut l1 l2 <-> permut (l ++ l1) (l ++ l2).
Proof.
intros l l1 l2; apply _permut_app1.
split.
- intro; apply compare_eq_refl.
- do 3 intro; apply compare_eq_trans.
- do 2 intro; intro H; rewrite compare_lt_gt, H.
  apply refl_equal.
Qed.

 Lemma permut_app_2 :
  forall l l1 l2, permut l1 l2 <-> permut (l1 ++ l) (l2 ++ l).
Proof.
intros l l1 l2; apply _permut_app2.
split.
- intro; apply compare_eq_refl.
- do 3 intro; apply compare_eq_trans.
- do 2 intro; intro H; rewrite compare_lt_gt, H.
  apply refl_equal.
Qed.

(** ** Link with AC syntactic decomposition.*)
Lemma ac_syntactic_aux :
 forall (l1 l2 l3 l4 : list A),
 permut (l1 ++ l2) (l3 ++ l4) ->
 (exists u1, exists u2, exists u3, exists u4, 
 permut l1 (u1 ++ u2) /\
 permut l2 (u3 ++ u4) /\
 permut l3 (u1 ++ u3) /\
 permut l4 (u2 ++ u4)).
Proof.
fix 1.
intro l1; case l1; clear l1; [ | intros a1 l1]; intros l2 l3 l4 P.
- exists (nil : list A); exists (nil : list A); exists l3; exists l4.
  split; [apply permut_refl | ].
  split; [assumption | split; apply permut_refl].
- simpl in P.
  inversion P; clear P; subst.
  destruct (split_list _ _ _ _ H1) as [l [[K1 K2] | [K1 K2]]]; clear H1.
  + destruct l as [ | _b l].
    * rewrite <- app_nil_end in K1; simpl in K2; subst l3 l4.
      destruct (ac_syntactic_aux _ _ _ _ H3) as [u1 [u2 [u3 [u4 [P1 [P2 [P3 P4]]]]]]].
      {
        exists u1; exists (b :: u2); exists u3; exists u4; repeat split.
        - apply Pcons; assumption.
        - assumption.
        - assumption.
        - assert (H := @Pcons _ _ 
                              (fun x y => compare OA x y = Eq) b b l5 nil (u2 ++ u4) 
                              (compare_eq_refl _) P4).
          simpl; simpl in H; apply H.
      }
    * simpl in K2; injection K2; clear K2.
      do 2 intro; subst l3 l5 _b.
      rewrite ass_app in H3.
      destruct (ac_syntactic_aux _ _ _ _ H3) as [u1 [u2 [u3 [u4 [P1 [P2 [P3 P4]]]]]]].
      {
        exists (b :: u1); exists u2; exists u3; exists u4; repeat split.
        - assert (H := @Pcons _ _ 
                              (fun x y => compare OA x y = Eq) a1 b l1 nil (u1 ++ u2) 
                              H2 P1).
          simpl; simpl in H; apply H.
        - assumption.
        - simpl.
          assert (H := permut_add_inside b b l0 l nil (u1 ++ u3) (compare_eq_refl _)).
          rewrite H in P3; apply P3.
        - assumption.
      }
  + subst l0 l4.
    rewrite <- ass_app in H3.
    destruct (ac_syntactic_aux _ _ _ _ H3) as [u1 [u2 [u3 [u4 [P1 [P2 [P3 P4]]]]]]].
    exists u1; exists (b :: u2); exists u3; exists u4; repeat split.
    * apply Pcons; assumption.
    * assumption.
    * assumption.
    * assert (H := permut_add_inside b b l l5 nil (u2 ++ u4) (compare_eq_refl _)).
      rewrite H in P4; apply P4.
Qed.

Lemma ac_syntactic :
 forall (l1 l2 l3 l4 : list A),
 permut (l2 ++ l1) (l4 ++ l3) ->
 (exists u1, exists u2, exists u3, exists u4, 
 permut l1 (u1 ++ u2) /\
 permut l2 (u3 ++ u4) /\
 permut l3 (u1 ++ u3) /\
 permut l4 (u2 ++ u4)).
Proof.
intros l1 l2 l3 l4 P; apply ac_syntactic_aux.
apply permut_trans with (l2 ++ l1).
apply _permut_swapp; apply permut_refl.
apply permut_trans with (l4 ++ l3); trivial.
apply _permut_swapp; apply permut_refl.
Qed.

Lemma nb_occ_permut :
 forall l1 l2, (forall x, nb_occ x l1 = nb_occ x l2) -> permut l1 l2.
Proof.
intros l1; induction l1 as [ | a1 l1]; intros l2 Hl.
- destruct l2 as [ | a2 l2].
  + apply Pnil.
  + apply False_rec.
    assert (Ha2 := Hl a2); simpl in Ha2.
    rewrite compare_eq_refl in Ha2.
    destruct (nb_occ a2 l2); discriminate Ha2.
- assert (Ha1 : mem_bool a1 l2 = true).
  {
    generalize (not_mem_nb_occ a1 l2).
    destruct (mem_bool a1 l2); [trivial | ].
    intro Abs; assert (Ha1 := Hl a1); simpl in Ha1.
    rewrite compare_eq_refl, (Abs (refl_equal _)) in Ha1.
    destruct (nb_occ a1 l1); discriminate Ha1.
  }
  rewrite mem_bool_true_iff in Ha1.
  destruct Ha1 as [b1 [Ha1 Hb1]].
  destruct (in_split _ _ Hb1) as [k1 [k2 K]]; subst l2.
  apply Pcons; [assumption | ].
  apply IHl1.
  intros x; generalize (Hl x).
  rewrite 2 nb_occ_app, 2 (nb_occ_unfold _ (_ :: _)).
  case_eq (compare OA x a1); intro Ka1.
  + rewrite (compare_eq_trans _ _ _ _ Ka1 Ha1); intro H.
    apply (BinNat.Nplus_reg_l 1); rewrite H, BinNat.N.add_comm, <- BinNat.N.add_assoc.
    apply f_equal; apply BinNat.N.add_comm.
  + rewrite (compare_lt_eq_trans _ _ _ _ Ka1 Ha1); exact (fun h => h).
  + rewrite (compare_gt_eq_trans _ _ _ Ka1 Ha1); exact (fun h => h).
Qed.

Lemma permut_nb_occ :
  forall x l1 l2, permut l1 l2 -> nb_occ x l1 = nb_occ x l2.
Proof.
intros x l1 l2 H.
rewrite 2 nb_occ_list_size; apply f_equal.
apply (_permut_size (R := (fun x y => compare OA x y = Eq))); [ | apply H].
intros a1 a2 _ _ K.
case_eq (compare OA x a1); intro H1.
- rewrite (compare_eq_trans _ _ _ _ H1 K); apply refl_equal.
- rewrite (compare_lt_eq_trans _ _ _ _ H1 K); apply refl_equal.
- rewrite (compare_gt_eq_trans _ _ _ H1 K); apply refl_equal.
Qed.

Lemma all_diff_bool_permut_all_diff_bool :
  forall l1 l2, all_diff_bool l1 = true -> permut l1 l2 -> all_diff_bool l2 = true.
Proof.
intro l1; induction l1 as [ | a1 l1]; intros l2 D1 H.
- inversion H; subst l2; apply refl_equal.
- inversion H as [ | _a1 a2 _l1 k1 k2 H1 H2]; clear H; subst.
  rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in D1.
  assert (IH := IHl1 _ (proj2 D1) H2).
  rewrite <- all_diff_bool_app_iff; repeat split.
  + apply (all_diff_bool_app_1 _ _ IH).
  + rewrite all_diff_bool_unfold, (all_diff_bool_app_2 _ _ IH), Bool.Bool.andb_true_r.
    case_eq (mem_bool a2 k2); intro Ha2; [ | apply refl_equal].
    rewrite <- not_false_iff_true in D1; apply False_rec; apply (proj1 D1).
    rewrite (permut_mem_bool_eq _ H2), mem_bool_app, !(mem_bool_eq_1 _ _ _ H1), Ha2.
    rewrite Bool.Bool.orb_true_r; apply refl_equal.
  + intros b1 b2 Hb Hb1 Hb2.
    rewrite mem_bool_unfold in Hb2.
    case_eq (compare OA b2 a2); intro K2; rewrite K2 in Hb2.
    * rewrite <- not_false_iff_true in D1; apply False_rec; apply (proj1 D1).
      rewrite (permut_mem_bool_eq _ H2), mem_bool_app, !(mem_bool_eq_1 _ _ _ H1).
      rewrite <- (mem_bool_eq_1 _ _ _ K2), <- (mem_bool_eq_1 _ _ _ Hb), Hb1.
      apply refl_equal.
    * rewrite <- all_diff_bool_app_iff in IH.
      apply (proj2 (proj2 IH) b1 b2); trivial.
    * rewrite <- all_diff_bool_app_iff in IH.
      apply (proj2 (proj2 IH) b1 b2); trivial.
Qed.

Lemma partition_spec3 :
  forall (f : A -> bool) l l1 l2, partition f l = (l1, l2) -> permut l (l1 ++ l2).
Proof.
intros f l l1 l2 H.
generalize (partition_spec3_strong _ _ H).
apply _permut_incl.
intros a b K; subst b; apply compare_eq_refl.
Qed.

Lemma partition_permut :
  forall (f1 f2 : A -> bool) l,
    (forall x y, List.In x l -> compare OA x y = Eq -> f1 x = f2 y) ->
    forall l1 l2 k k1 k2, partition f1 l = (l1, l2) -> partition f2 k = (k1, k2) ->
    permut l k -> permut l1 k1 /\ permut l2 k2.
Proof.
intros f1 f2 l; induction l as [ | a1 l]; intros Hl l1 l2 k k1 k2 Pl Pk H.
- inversion H; subst k; simpl in Pl, Pk; injection Pl; injection Pk; do 4 intro; subst.
  split; apply permut_refl.
- simpl in Pl.
  case_eq (partition f1 l); intros ll1 ll2; intro Pll; rewrite Pll in Pl.
  inversion H; clear H; subst.
  assert (Hb := sym_eq (Hl a1 b (or_introl _ (refl_equal _)) H2)).
  assert (Pk1 := partition_app1 f2 l3 (b :: l4)); rewrite Pk in Pk1.
  assert (Pk2 := partition_app2 f2 l3 (b :: l4)); rewrite Pk in Pk2.
  simpl in Pk1, Pk2; rewrite Hb in Pk1, Pk2.
  case_eq (partition f2 l3); intros l3' l3'' Hl3; rewrite Hl3 in Pk1, Pk2.
  case_eq (partition f2 l4); intros l4' l4'' Hl4; rewrite Hl4 in Pk1, Pk2.
  assert (Pkk : partition f2 (l3 ++ l4) = (l3' ++ l4', l3'' ++ l4'')).
  {
    generalize (partition_app1 f2 l3 l4); rewrite Hl3, Hl4; simpl.
    generalize (partition_app2 f2 l3 l4); rewrite Hl3, Hl4; simpl.
    destruct (partition f2 (l3 ++ l4)); simpl; do 2 intro; subst; apply refl_equal.
  }
  assert (IH := IHl (fun x y h => Hl x y (or_intror _ h)) _ _ _ _ _ Pll Pkk H4).
  destruct IH as [IH1 IH2].
  case_eq (f1 a1); intro Ha1; rewrite Ha1 in Hb, Pl, Pk1, Pk2; 
  injection Pl; clear Pl; do 2 intro; subst l1 l2; simpl in Pk1, Pk2; subst k1 k2.
  + split; [apply Pcons; [apply H2 | apply IH1] | apply IH2].
  + split; [apply IH1 | apply Pcons; [apply H2 | apply IH2]].
Qed.

Lemma all_diff_bool_mem_bool_eq_permut :
  forall l1 l2, all_diff_bool l1 = true -> all_diff_bool l2 = true -> 
                (forall x, mem_bool x l1 = mem_bool x l2) ->
                permut l1 l2.
Proof.
intro l1; induction l1 as [ | x1 l1]; intros l2 H1 H2 H.
- destruct l2 as [ | x2 l2]; [apply _permut_refl; intros; apply compare_eq_refl | ].
  assert (Hx2 := H x2); simpl in Hx2.
  rewrite eq_bool_refl in Hx2; discriminate Hx2.
- assert (Hx1 := sym_eq (H x1)); simpl in Hx1.
  rewrite eq_bool_refl in Hx1; simpl in Hx1.
  rewrite mem_bool_true_iff in Hx1; destruct Hx1 as [x2 [Hx Hx2]].
  destruct (in_split _ _ Hx2) as [k1 [k2 K]]; subst l2.
  apply Pcons; [assumption | ].
  rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in H1.
  apply IHl1.
  + apply (proj2 H1).
  + rewrite <- all_diff_bool_app_iff; repeat split.
    * apply (all_diff_bool_app_1 _ _ H2).
    * assert (K2 := all_diff_bool_app_2 _ _ H2); 
        rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in K2.
      apply (proj2 K2).
    * intros a1 a2 Ha Ha1 Ha2.
      rewrite <- all_diff_bool_app_iff in H2; destruct H2 as [_ [_ H2]].
      apply (H2 a1 a2 Ha Ha1).
      rewrite mem_bool_unfold, Ha2; apply Bool.Bool.orb_true_r.
  + intros y; assert (Hy := H y); simpl in Hy.
    case_eq (eq_bool y x1); intro Kx1.
    * unfold eq_bool in Kx1; rewrite compare_eq_true in Kx1.
      rewrite !(mem_bool_eq_1 _ _ _ Kx1).
      case_eq (mem_bool x1 l1); intro Hx1; rewrite Hx1 in H1;
        [simpl in H1; discriminate (proj1 H1) | ].
      apply sym_eq; rewrite mem_bool_app, Bool.Bool.orb_false_iff.
      rewrite <- all_diff_bool_app_iff in H2.
      destruct H2 as [H2 [H3 H4]]; rewrite all_diff_bool_unfold, Bool.Bool.andb_true_iff in H3.
      case_eq (mem_bool x1 k1); intro Jx1.
      -- apply False_rec.
         apply (H4 x1 x2 Hx Jx1); rewrite mem_bool_unfold, compare_eq_refl.
         apply refl_equal.
      -- split; [apply refl_equal | ].
         rewrite (mem_bool_eq_1 _ _ _ Hx).
         case_eq (mem_bool x2 k2); intro Kx2; [ | apply refl_equal].
         rewrite Kx2 in H3; simpl in H3; discriminate (proj1 H3).
    * rewrite Kx1 in Hy; simpl in Hy; rewrite Hy.
      rewrite mem_bool_app, (mem_bool_unfold _ (_ :: _)).
      unfold eq_bool in Kx1.
      rewrite <- (compare_eq_2 _ _ _ Hx).
      destruct (compare OA y x1); try discriminate Kx1; simpl;
        rewrite mem_bool_app; apply refl_equal.
Qed.

Definition permut_bool := 
  _permut_bool (fun x y => 
                  match compare OA x y with 
                    | Eq => true 
                    | _ => false 
                  end).

Lemma permut_bool_is_sound :
  forall (l1 l2 : list A),
    match permut_bool l1 l2 with 
      | true => permut l1 l2 
      | false => ~permut l1 l2 
    end.
Proof.
intros l1 l2.
unfold permut_bool.
generalize (_permut_bool_is_sound 
              (fun x y => match compare OA x y with Eq => true | _ => false end) 
              l1 l2).
case (_permut_bool
         (fun x y : A =>
          match compare OA x y with
          | Eq => true
          | Lt => false
          | Gt => false
          end) l1 l2).
- intro H.
  apply _permut_incl with (fun x y : A =>
         match compare OA x y with
         | Eq => true
         | Lt => false
         | Gt => false
         end = true).
  + intros a b K.
    rewrite compare_eq_true in K; apply K.
  + apply H.
    intros a3 b1 a4 b2 _ _ _ _ H1 H2 H3.
    rewrite compare_eq_true in H1, H2, H3.
    rewrite compare_lt_gt, CompOpp_iff in H2.
    rewrite (compare_eq_trans _ _ _ _ H1 (compare_eq_trans _ _ _ _ H2 H3)).
    apply refl_equal.
- intros H H'.
  apply H.
  + intros a3 b1 a4 b2 _ _ _ _ H1 H2 H3.
    rewrite compare_eq_true in H1, H2, H3.
    rewrite compare_lt_gt, CompOpp_iff in H2.
    rewrite (compare_eq_trans _ _ _ _ H1 (compare_eq_trans _ _ _ _ H2 H3)).
    apply refl_equal.
  + revert H'; apply _permut_incl.
    intros a b K; rewrite K.
    apply refl_equal.
Qed.

End Permut.

End Sec.

Section Sec2.

Hypothesis A B : Type.
Hypothesis OA : Rcd A.
Hypothesis OB : Rcd B.

Lemma mem_bool_map :
  forall a l, mem_bool OA a l = true -> 
  forall (f : A -> B), (forall x, compare OA a x = Eq -> compare OB (f a) (f x) = Eq)-> 
                       mem_bool OB (f a) (map f l) = true.
Proof.
intros a l H f Hf; rewrite mem_bool_true_iff.
rewrite mem_bool_true_iff in H.
destruct H as [a' [_H H]]; exists (f a'); split.
- apply Hf; assumption.
- rewrite in_map_iff; exists a'; split; trivial.
Qed.

Lemma mem_bool_flat_map :
  forall b (f : A -> list B) l, 
    mem_bool OB b (flat_map f l) = existsb (fun x => mem_bool OB b (f x)) l.
Proof.
intros b f.
fix 1.
intro l; case l; clear l.
- apply refl_equal.
- intros a l; simpl.
  rewrite mem_bool_app; apply f_equal.
  apply mem_bool_flat_map.
Qed.

Lemma comparelA_map_eq_1 : 
  forall (f1 f2 : A -> B) l, (forall x, compare OB (f1 x) (f2 x) = Eq) ->
         comparelA (compare OB) (map f1 l) (map f2 l) = Eq.
Proof.
intros f1 f2 l; induction l as [ | x1 l]; intros Hf.
- apply refl_equal.
- simpl.
  rewrite Hf; apply IHl; assumption.
Qed.

Lemma comparelA_map_eq_1b : 
  forall (f1 f2 : A -> B) l,
         (forall x, In x l -> Oeset.compare OB (f1 x) (f2 x) = Eq) ->
         comparelA (Oeset.compare OB) (map f1 l) (map f2 l) = Eq.
Proof.
intros f1 f2 l; induction l as [ | x1 l]; intros Hf.
- apply refl_equal.
- simpl.
  assert (H:In x1 (x1::l)) by now constructor.
  rewrite (Hf _ H). apply IHl.
  intros x Hx. apply Hf. now constructor 2.
Qed.

Lemma comparelA_map_eq_2 : 
  forall (f : A -> B) l1 l2,
         (forall x1 x2, compare OA x1 x2 = Eq -> compare OB (f x1) (f x2) = Eq) ->
         comparelA (compare OA) l1 l2 = Eq ->
         comparelA (compare OB) (map f l1) (map f l2) = Eq.
Proof.
intros f l1; induction l1 as [ | x1 l1]; intros [ | x2 l2] Hf Hl; try discriminate Hl.
- apply refl_equal.
- simpl in Hl; simpl.
  case_eq (compare OA x1 x2); intro Hx; rewrite Hx in Hl; try discriminate Hl.
  rewrite (Hf _ _ Hx); apply IHl1; trivial.
Qed.

Lemma comparelA_flat_map_eq_1 : 
  forall (f1 f2 : A -> list B) l,
         (forall x, comparelA (compare OB) (f1 x) (f2 x) = Eq) ->
         comparelA (compare OB) (flat_map f1 l) (flat_map f2 l) = Eq.
Proof.
intros f1 f2 l; induction l as [ | x1 l]; intros Hf.
- apply refl_equal.
- simpl; apply comparelA_eq_app; [ | apply IHl; trivial].
  apply Hf.
Qed.

Lemma comparelA_flat_map_eq_2 : 
  forall (f : A -> list B) l1 l2,
         (forall x1 x2, compare OA x1 x2 = Eq -> comparelA (compare OB) (f x1) (f x2) = Eq) ->
         comparelA (compare OA) l1 l2 = Eq ->
         comparelA (compare OB) (flat_map f l1) (flat_map f l2) = Eq.
Proof.
intros f l1; induction l1 as [ | x1 l1]; intros [ | x2 l2] Hf Hl; try discriminate Hl.
- apply refl_equal.
- simpl in Hl; simpl.
  case_eq (compare OA x1 x2); intro Hx; rewrite Hx in Hl; try discriminate Hl.
  apply comparelA_eq_app; [apply (Hf _ _ Hx) | apply IHl1]; trivial.
Qed.

Lemma nb_occ_map_inj :
  forall f l x, 
  (forall x2, mem_bool OA x2 l = true -> 
              (compare OA x x2 = Eq <-> compare OB (f x) (f x2) = Eq)) -> 
    nb_occ OB (f x) (map f l) = nb_occ OA x l.
Proof.
intros f l x; induction l as [ | a1 l];
intros Hl; simpl; [apply refl_equal | ].
apply f_equal2.
- case_eq (compare OA x a1); intro Hx.
  + rewrite (Hl a1) in Hx; [rewrite Hx; trivial | ].
    simpl; unfold eq_bool; rewrite compare_eq_refl; trivial.
  + case_eq (compare OB (f x) (f a1)); intro Hfx; trivial.
    rewrite <- (Hl a1), Hx in Hfx; [discriminate Hfx | ].
    simpl; unfold eq_bool; rewrite compare_eq_refl; trivial.
  + case_eq (compare OB (f x) (f a1)); intro Hfx; trivial.
    rewrite <- (Hl a1), Hx in Hfx; [discriminate Hfx | ].
    simpl; unfold eq_bool; rewrite compare_eq_refl; trivial.
- apply IHl.
  intros x2 Hx2; apply Hl; simpl.
  rewrite Hx2, Bool.orb_true_r; apply refl_equal.
Qed.

Lemma nb_occ_map_eq_weak :
  forall (f : A -> B) l1 l2,
    (forall x1 x2, mem_bool OA x1 l1 = true -> mem_bool OA x2 l2 = true ->
                   compare OA x1 x2 = Eq -> 
                   compare OB (f x1) (f x2) = Eq) ->
    (forall x, nb_occ OA x l1 = nb_occ OA x l2) -> 
    forall x, nb_occ OB (f x) (map f l1) = nb_occ OB (f x) (map f l2).
Proof.
intros f l1; induction l1 as [ | x1 l1]; intros l2 Hf H x.
- destruct l2 as [ | x2 l2]; [apply refl_equal | ].
  assert (Hx2 := H x2).
  simpl in Hx2; rewrite compare_eq_refl in Hx2.
  destruct (nb_occ OA x2 l2); discriminate Hx2.
- rewrite map_unfold, nb_occ_unfold.
  assert (Hx1 : mem_bool OA x1 l2 = true).
  {
    case_eq (mem_bool OA x1 l2); [intros _; apply refl_equal | ].
    intro Abs; generalize (not_mem_nb_occ OA _ _ Abs).
    rewrite <- H, nb_occ_unfold, compare_eq_refl.
    destruct (nb_occ OA x1 l1); discriminate.
  }
  rewrite mem_bool_true_iff in Hx1.
  destruct Hx1 as [x2 [Hx1 Hx2]].
  destruct (in_split _ _ Hx2) as [k1 [k2 Hk]].
  rewrite Hk, map_app, nb_occ_app.
  rewrite (map_unfold f (_ :: _)), (nb_occ_unfold _ _ (_ :: _)).
  assert (Hf' : forall x2 x3 : A, mem_bool OA x2 l1 = true -> mem_bool OA x3 (k1 ++ k2) = true ->
                                  compare OA x2 x3 = Eq -> compare OB (f x2) (f x3) = Eq).
  {
    intros y1 y2 Hy1 Hy2; apply Hf; simpl.
    - rewrite Hy1, Bool.orb_true_r; apply refl_equal. 
    - rewrite Hk; rewrite mem_bool_app, Bool.orb_true_iff in Hy2.
      destruct Hy2 as [Hy2 | Hy2].
      + rewrite mem_bool_app, Hy2; apply refl_equal.
      + rewrite mem_bool_app, (mem_bool_unfold _ _ (_ :: _)), Hy2.
        rewrite 2 Bool.orb_true_r; apply refl_equal.
  }
  assert (H' : forall x : A, nb_occ OA x l1 = nb_occ OA x (k1 ++ k2)).
  {
    intros y.
    generalize (H y); rewrite Hk, 2 nb_occ_app; simpl.
    case_eq (compare OA y x1); intro Hy.
    - rewrite (compare_eq_trans _ _ _ _ Hy Hx1); intro Ky.
      apply (Nplus_reg_l 1); rewrite Ky, N.add_comm, <- N.add_assoc; apply f_equal.
      apply N.add_comm.
    - rewrite (compare_lt_eq_trans _ _ _ _ Hy Hx1); intro Ky; apply Ky.
    - rewrite (compare_gt_eq_trans _ _ _ _ Hy Hx1); intro Ky; apply Ky.
  }
  assert (IH := IHl1 (k1 ++ k2) Hf' H' x).
  rewrite IH, map_app, nb_occ_app.
  rewrite N.add_comm, <- N.add_assoc; apply f_equal.
  rewrite N.add_comm; apply f_equal2; [ | apply refl_equal].
  assert (Kx1 : compare OB (f x) (f x1) = compare OB (f x) (f x2)).
  {
    assert (Kx1 : compare OB (f x1) (f x2) = Eq).
    {
      apply Hf.
      - rewrite mem_bool_unfold, compare_eq_refl; apply refl_equal.
      - case_eq (mem_bool OA x2 l2); [intros; trivial | ].
        intro _Hx2.
        assert (Kx2 := not_mem_nb_occ OA _ _ _Hx2).
        assert (Jx2 := H x2).
        rewrite Kx2, nb_occ_unfold, compare_lt_gt, Hx1 in Jx2.
        simpl in Jx2.
        destruct (nb_occ OA x2 l1); discriminate Jx2.
      - apply Hx1. 
    }
    apply sym_eq; case_eq (compare OB (f x) (f x1)); intro Jx1.
    - apply (compare_eq_trans _ _ _ _ Jx1 Kx1).
    - apply (compare_lt_eq_trans _ _ _ _ Jx1 Kx1).
    - apply (compare_gt_eq_trans _ _ _ _ Jx1 Kx1).
  }
  rewrite Kx1; apply refl_equal.
Qed.

Lemma nb_occ_map_eq_3 :
  forall (f : A -> B) l1 l2,
    (forall x1 x2, mem_bool OA x1 l1 = true -> mem_bool OA x2 l2 = true -> 
                   compare OA x1 x2 = Eq -> 
                   compare OB (f x1) (f x2) = Eq) ->
    (forall x, nb_occ OA x l1 = nb_occ OA x l2) -> 
    forall x, nb_occ OB x (map f l1) = nb_occ OB x (map f l2).
Proof.
intros f l1 l2 Hf H y.
case_eq (mem_bool OB y (map f l1)); [intros Hy | intros Hy].
- rewrite mem_bool_true_iff in Hy.
  destruct Hy as [fx [Hy Hfx]].
  rewrite in_map_iff in Hfx; destruct Hfx as [x [Hfx Hx]]; subst fx.
  rewrite 2 (nb_occ_eq_1 _ _ _ _ Hy).
  apply nb_occ_map_eq_weak; trivial.
- case_eq (mem_bool OB y (map f l2)); [intros Hy' | intros Hy'].
  + rewrite mem_bool_true_iff in Hy'.
    destruct Hy' as [fx [Hy' Hfx]].
    rewrite in_map_iff in Hfx; destruct Hfx as [x [Hfx Hx]]; subst fx.
    rewrite 2 (nb_occ_eq_1 _ _ _ _ Hy').
    apply nb_occ_map_eq_weak; trivial.
  + rewrite (not_mem_nb_occ OB _ _ Hy), (not_mem_nb_occ OB _ _ Hy').
    apply refl_equal.
Qed.

Lemma nb_occ_map_eq_2_3 :
  forall (f1 f2 : A -> B) l1 l2,
    (forall x1 x2, compare OA x1 x2 = Eq -> 
                   compare OB (f1 x1) (f2 x2) = Eq) ->
    (forall x, nb_occ OA x l1 = nb_occ OA x l2) -> 
    forall x, nb_occ OB x (map f1 l1) = nb_occ OB x (map f2 l2).
Proof.
intros f1 f2 l1; induction l1 as [ | x1 l1]; intros l2 Hf H x.
- destruct l2 as [ | x2 l2]; [trivial | ].
  apply False_rec.
  assert (Hx2 := H x2); simpl in Hx2.
  rewrite compare_eq_refl in Hx2; destruct (nb_occ OA x2 l2); discriminate Hx2.
- assert (Hx1 : mem_bool OA x1 l2 = true).
  {
    case_eq (mem_bool OA x1 l2); [intros _; apply refl_equal | ].
    intro Abs; generalize (not_mem_nb_occ OA _ _ Abs).
    rewrite <- H, nb_occ_unfold, compare_eq_refl.
    destruct (nb_occ OA x1 l1); discriminate.
  }
  rewrite mem_bool_true_iff in Hx1.
  destruct Hx1 as [x2 [Hx1 Hx2]].
  destruct (in_split _ _ Hx2) as [k1 [k2 Hk]].
  rewrite Hk, map_app, nb_occ_app.
  rewrite (map_unfold f1 (_ :: _)), (nb_occ_unfold _ _ (_ :: _)).
  assert (H' : forall x : A, nb_occ OA x l1 = nb_occ OA x (k1 ++ k2)).
  {
    intros y.
    generalize (H y); rewrite Hk, 2 nb_occ_app; simpl.
    case_eq (compare OA y x1); intro Hy.
    - rewrite (compare_eq_trans _ _ _ _ Hy Hx1); intro Ky.
      apply (Nplus_reg_l 1); rewrite Ky, N.add_comm, <- N.add_assoc; apply f_equal.
      apply N.add_comm.
    - rewrite (compare_lt_eq_trans _ _ _ _ Hy Hx1); intro Ky; apply Ky.
    - rewrite (compare_gt_eq_trans _ _ _ _ Hy Hx1); intro Ky; apply Ky.
  }
  assert (IH := IHl1 (k1 ++ k2) Hf H' x).
  rewrite IH, map_app, nb_occ_app.
  rewrite N.add_comm, <- N.add_assoc; apply f_equal.
  rewrite  (map_unfold _ (_ :: _)), (nb_occ_unfold _ _ (_ :: _)),
  N.add_comm; apply f_equal2; [ | apply refl_equal].
  assert (Kx1 : compare OB x (f1 x1) = compare OB x (f2 x2)).
  {
    assert (Kx1 : compare OB (f1 x1) (f2 x2) = Eq).
    {
      apply Hf; assumption.
    }
    apply sym_eq; case_eq (compare OB x (f1 x1)); intro Jx1.
    - apply (compare_eq_trans _ _ _ _ Jx1 Kx1).
    - apply (compare_lt_eq_trans _ _ _ _ Jx1 Kx1).
    - apply (compare_gt_eq_trans _ _ _ _ Jx1 Kx1).
  }
  rewrite Kx1; apply refl_equal.
Qed.

Lemma nb_occ_map_eq_2_3_alt :
  forall (f1 f2 : A -> B) l1 l2,
    (forall x1 x2, mem_bool OA x1 l1 = true -> compare OA x1 x2 = Eq -> 
                   compare OB (f1 x1) (f2 x2) = Eq) ->
    (forall x, nb_occ OA x l1 = nb_occ OA x l2) -> 
    forall x, nb_occ OB x (map f1 l1) = nb_occ OB x (map f2 l2).
Proof.
intros f1 f2 l1; induction l1 as [ | x1 l1]; intros l2 Hf H x.
- destruct l2 as [ | x2 l2]; [trivial | ].
  apply False_rec.
  assert (Hx2 := H x2); simpl in Hx2.
  rewrite compare_eq_refl in Hx2; destruct (nb_occ OA x2 l2); discriminate Hx2.
- assert (Hx1 : mem_bool OA x1 l2 = true).
  {
    case_eq (mem_bool OA x1 l2); [intros _; apply refl_equal | ].
    intro Abs; generalize (not_mem_nb_occ OA _ _ Abs).
    rewrite <- H, nb_occ_unfold, compare_eq_refl.
    destruct (nb_occ OA x1 l1); discriminate.
  }
  rewrite mem_bool_true_iff in Hx1.
  destruct Hx1 as [x2 [Hx1 Hx2]].
  destruct (in_split _ _ Hx2) as [k1 [k2 Hk]].
  rewrite Hk, map_app, nb_occ_app.
  rewrite (map_unfold f1 (_ :: _)), (nb_occ_unfold _ _ (_ :: _)).
  assert (H' : forall x : A, nb_occ OA x l1 = nb_occ OA x (k1 ++ k2)).
  {
    intros y.
    generalize (H y); rewrite Hk, 2 nb_occ_app; simpl.
    case_eq (compare OA y x1); intro Hy.
    - rewrite (compare_eq_trans _ _ _ _ Hy Hx1); intro Ky.
      apply (Nplus_reg_l 1); rewrite Ky, N.add_comm, <- N.add_assoc; apply f_equal.
      apply N.add_comm.
    - rewrite (compare_lt_eq_trans _ _ _ _ Hy Hx1); intro Ky; apply Ky.
    - rewrite (compare_gt_eq_trans _ _ _ _ Hy Hx1); intro Ky; apply Ky.
  }
  assert (Hf' : forall x1 x2 : A,
                  mem_bool OA x1 l1 = true ->
                  compare OA x1 x2 = Eq -> compare OB (f1 x1) (f2 x2) = Eq).
  {
    intros y1 y2 Hy1; apply Hf.
    rewrite mem_bool_unfold, Hy1, Bool.orb_true_r; trivial.
  }
  assert (IH := IHl1 (k1 ++ k2) Hf' H' x).
  rewrite IH, map_app, nb_occ_app.
  rewrite N.add_comm, <- N.add_assoc; apply f_equal.
  rewrite  (map_unfold _ (_ :: _)), (nb_occ_unfold _ _ (_ :: _)),
  N.add_comm; apply f_equal2; [ | apply refl_equal].
  assert (Kx1 : compare OB x (f1 x1) = compare OB x (f2 x2)).
  {
    assert (Kx1 : compare OB (f1 x1) (f2 x2) = Eq).
    {
      apply Hf; [ | assumption].
      rewrite mem_bool_unfold, compare_eq_refl; apply refl_equal.
    }
    apply sym_eq; case_eq (compare OB x (f1 x1)); intro Jx1.
    - apply (compare_eq_trans _ _ _ _ Jx1 Kx1).
    - apply (compare_lt_eq_trans _ _ _ _ Jx1 Kx1).
    - apply (compare_gt_eq_trans _ _ _ _ Jx1 Kx1).
  }
  rewrite Kx1; apply refl_equal.
Qed.

Lemma nb_occ_map_eq_2_alt :
  forall (f1 f2 : A -> B) l,
    (forall x1, In x1 l -> compare OB (f1 x1) (f2 x1) = Eq) ->
    forall x, nb_occ OB x (map f1 l) = nb_occ OB x (map f2 l).
Proof.
intros f1 f2; induction l as [ | a1 l]; intros Hf y.
- apply refl_equal.
- simpl; apply f_equal2; [ | apply IHl; intros; apply Hf; right; assumption].
  assert (Ha1 := Hf _ (or_introl _ (refl_equal _))).
  case_eq (compare OB y (f1 a1)); intro Hy.
  + rewrite (compare_eq_trans _ _ _ _ Hy Ha1); apply refl_equal.
  + rewrite (compare_lt_eq_trans _ _ _ _ Hy Ha1); apply refl_equal.
  + rewrite (compare_gt_eq_trans _ _ _ _ Hy Ha1); apply refl_equal.
Qed.

Lemma nb_occ_map_filter :
  forall (f : A -> bool) (g : A -> B) l,
    (forall x1 x2, 
       mem_bool OA x1 l = true -> compare OB (g x1) (g x2) = Eq -> f x1 = f x2) ->
    forall a, nb_occ OB (g a) (map g (filter f l)) = 
              if (f a) then nb_occ OB (g a) (map g l) else 0%N.
Proof.
intros f g l Hl a; revert Hl a.
induction l as [ | a1 l]; intros Hl a; simpl; [case (f a); apply refl_equal | ].
case_eq (f a1); intro Ha1; simpl.
- case_eq (compare OB (g a) (g a1)); intro Ha.
  + rewrite IHl.
    rewrite <- (Hl a1 a), Ha1; trivial.
    * rewrite mem_bool_unfold, compare_eq_refl; apply refl_equal.
    * apply compare_eq_sym; assumption.
    * intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
  + case_eq (f a); intro Ka.
    * apply f_equal; rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
    * rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
  + case_eq (f a); intro Ka.
    * apply f_equal; rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
    * rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
- case_eq (compare OB (g a) (g a1)); intro Ha.
  + rewrite IHl.
    rewrite <- (Hl a1 a), Ha1; trivial.
    * rewrite mem_bool_unfold, compare_eq_refl; apply refl_equal.
    * apply compare_eq_sym; assumption.
    * intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
  + case_eq (f a); intro Ka.
    * rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
    * rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
  + case_eq (f a); intro Ka.
    * rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
    * rewrite IHl, Ka; trivial.
      intros x1 x2 H; apply Hl; rewrite mem_bool_unfold, H, Bool.orb_true_r.
      apply refl_equal.
Qed.

Lemma map_filter_and :
  forall (f1 f2 : A -> bool) (g : A -> B) l,
    (forall x1 x2, mem_bool OA x1 l = true -> 
                   compare OB (g x1) (g x2) = Eq -> f1 x1 = f1 x2) ->
    (forall x1 x2, mem_bool OA x1 l = true -> 
                   compare OB (g x1) (g x2) = Eq -> f2 x1 = f2 x2) ->
  forall x,
    nb_occ OB x (map g (filter (fun x : A => f1 x && f2 x) l)) =
   N.min (nb_occ OB x (map g (filter f1 l)))
         (nb_occ OB x (map g (filter f2 l))).
Proof.
intros f1 f2 g l H1 H2 x.
case_eq (mem_bool OB x (map g l)).
- intros Hx; rewrite mem_bool_true_iff in Hx.
  destruct Hx as [b [Hx Hb]]; rewrite in_map_iff in Hb.
  destruct Hb as [a [Hb Ha]]; subst b.
  rewrite 3 (nb_occ_eq_1 _ _ _ _ Hx), 3 nb_occ_map_filter; trivial.
  + case (f1 a); simpl.
    * {
        case (f2 a); simpl.
        - apply sym_eq; apply N.min_l.
          apply N.le_refl.
        - rewrite N.min_0_r; apply refl_equal.
      } 
    * rewrite N.min_0_l; apply refl_equal.
  + intros x1 x2 Hx1 _Hx; apply f_equal2; [apply H1 | apply H2]; trivial.
- intro Hx.
  apply trans_eq with 0%N.
  + case_eq (nb_occ OB x (map g (filter (fun x0 : A => f1 x0 && f2 x0) l))); [trivial | ].
    intros p Hp; rewrite <- not_true_iff_false in Hx.
    apply False_rec; apply Hx.
    generalize (not_mem_nb_occ OB x (map g (filter (fun x0 : A => f1 x0 && f2 x0) l))).
    rewrite Hp.
    case_eq (mem_bool OB x (map g (filter (fun x0 : A => f1 x0 && f2 x0) l))).
    * intros Hl _; rewrite mem_bool_true_iff in Hl.
      destruct Hl as [b [Hb Kb]].
      rewrite in_map_iff in Kb; destruct Kb as [a [Ha Ka]]; subst b.
      rewrite filter_In in Ka.
      rewrite mem_bool_true_iff; exists (g a); split; [assumption | ].
      rewrite in_map_iff; exists a; split; [trivial | apply (proj1 Ka)].
    * intros _ Abs; apply False_rec; generalize (Abs (refl_equal _)); discriminate.
  + case_eq (nb_occ OB x (map g (filter f1 l))); [rewrite N.min_0_l; trivial | ].
    intros p Hp; rewrite <- not_true_iff_false in Hx.
    apply False_rec; apply Hx.
    generalize (not_mem_nb_occ OB x (map g (filter f1 l))).
    rewrite Hp.
    case_eq (mem_bool OB x (map g (filter f1 l))).
    * intros Hl _; rewrite mem_bool_true_iff in Hl.
      destruct Hl as [b [Hb Kb]].
      rewrite in_map_iff in Kb; destruct Kb as [a [Ha Ka]]; subst b.
      rewrite filter_In in Ka.
      rewrite mem_bool_true_iff; exists (g a); split; [assumption | ].
      rewrite in_map_iff; exists a; split; [trivial | apply (proj1 Ka)].
    * intros _ Abs; apply False_rec; generalize (Abs (refl_equal _)); discriminate.
Qed.

Lemma fold_left_rev_right_eq
      (f : B -> A -> A)
      (Hf : forall x1 x2 y1 y2, 
          compare OB x1 x2 = Eq -> compare OA y1 y2 = Eq -> compare OA (f x1 y1) (f x2 y2) = Eq) :
  forall l1 l2 acc1 acc2,
    compare OA acc1 acc2 = Eq ->
    comparelA (compare OB) l1 (rev l2) = Eq ->
    compare OA (fold_left (fun a e => f e a) l1 acc2) (fold_right f acc1 l2) = Eq.
Proof.
  induction l1 as [ |x xs IHxs].
  - intros [ |y ys] acc1 acc2; simpl.
    + intros H _; now apply compare_eq_sym.
    + case_eq (rev ys ++ y :: nil); try discriminate.
      intro H. elim (snoc_not_nil _ _ _ H).
  - intros l' acc1 acc2 Hacc. destruct (list_nil_snoc l') as [H1|[z [zs H1]]]; subst l'.
    + discriminate.
    + simpl. rewrite rev_app_distr. simpl.
      case_eq (compare OB x z); try discriminate. intros Heq1 Heq2.
      rewrite fold_right_app. simpl.
      apply IHxs; trivial.
      apply Hf; trivial.
      now apply (compare_eq_sym OB).
Qed.

End Sec2.

Definition mk_pair (A B : Type) (OA : Rcd A) (SB : Rcd B) : Rcd (A * B).
Proof.
split with (compareAB (compare OA) (compare SB)).
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAB_eq_trans.
  + apply compare_eq_trans.
  + apply compare_eq_trans.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAB_le_lt_trans.
  + apply compare_eq_trans.
  + apply compare_eq_lt_trans.
  + apply compare_eq_lt_trans.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAB_lt_le_trans.
  + apply compare_eq_trans.
  + apply compare_lt_eq_trans.
  + apply compare_lt_eq_trans.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAB_lt_trans.
  + apply compare_eq_trans.
  + apply compare_eq_lt_trans.
  + apply compare_lt_eq_trans.
  + apply compare_lt_trans.
  + apply compare_lt_trans.
- intros [a1 b1] [a2 b2]; apply compareAB_lt_gt.
  + apply compare_lt_gt.
  + apply compare_lt_gt.
Defined.

Definition mk_pairleft (A B : Type) (OA : Rcd A) : Rcd (A * B).
Proof.
split with (compareAl (compare OA)).
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAl_eq_trans.
  apply compare_eq_trans.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAl_le_lt_trans.
  + apply compare_eq_trans.
  + apply compare_eq_lt_trans.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAl_lt_le_trans.
  + apply compare_eq_trans.
  + apply compare_lt_eq_trans.
- intros [a1 b1] [a2 b2] [a3 b3]; apply compareAl_lt_trans.
  + apply compare_eq_trans.
  + apply compare_eq_lt_trans.
  + apply compare_lt_eq_trans.
  + apply compare_lt_trans.
- intros [a1 b1] [a2 b2]; apply compareAl_lt_gt.
  apply compare_lt_gt.
Defined.

Definition mk_list (A : Type) (OA : Rcd A) : Rcd (list A).
Proof.
split with (comparelA (compare OA)).
- intros l1 l2 l3; apply comparelA_eq_trans.
  + do 6 intro; apply compare_eq_trans.
- intros l1 l2 l3; apply comparelA_le_lt_trans.
  + do 6 intro; apply compare_eq_trans.
  + do 6 intro; apply compare_eq_lt_trans.
- intros l1 l2 l3; apply comparelA_lt_le_trans.
  + do 6 intro; apply compare_eq_trans.
  + do 6 intro; apply compare_lt_eq_trans.
- intros l1 l2 l3; apply comparelA_lt_trans.
  + do 6 intro; apply compare_eq_trans.
  + do 6 intro; apply compare_eq_lt_trans.
  + do 6 intro; apply compare_lt_eq_trans.
  + do 6 intro; apply compare_lt_trans.
- intros l1 l2; apply comparelA_lt_gt.
  do 4 intro; apply compare_lt_gt.
Defined.


Definition mk_option (A : Type) (OA : Rcd A) : Rcd (option A).
Proof.
split with (compareoA (compare OA)).
- do 3 intro; unfold compareoA, option_to_list; apply comparelA_eq_trans.
  do 6 intro; apply compare_eq_trans.
- do 3 intro; unfold compareoA, option_to_list; apply comparelA_le_lt_trans.
  + do 6 intro; apply compare_eq_trans.
  + do 6 intro; apply compare_eq_lt_trans.
- do 3 intro; unfold compareoA, option_to_list; apply comparelA_lt_le_trans.
  + do 6 intro; apply compare_eq_trans.
  + do 6 intro; apply compare_lt_eq_trans.
- do 3 intro; unfold compareoA, option_to_list; apply comparelA_lt_trans.
  + do 6 intro; apply compare_eq_trans.
  + do 6 intro; apply compare_eq_lt_trans.
  + do 6 intro; apply compare_lt_eq_trans.
  + do 6 intro; apply compare_lt_trans.
- do 2 intro; unfold compareoA, option_to_list; apply comparelA_lt_gt.
  do 4 intro; apply compare_lt_gt.
Defined.

End Oeset.

Ltac oeset_compare_tac :=
  match goal with
    | |- Oeset.compare ?OS ?a1 ?a1 = Eq => apply Oeset.compare_eq_refl
    | h1 : Oeset.compare ?OS ?a1 ?a2 = Eq, 
      h2 : Oeset.compare ?OS ?a2 ?a3 = Eq
      |- Oeset.compare ?OS ?a1 ?a3 = Eq => apply (Oeset.compare_eq_trans _ _ _ _ h1 h2)
    | |- Oeset.compare ?OS ?a1 ?a2 = Eq -> 
         Oeset.compare ?OS ?a2 ?a3 = Eq -> 
         Oeset.compare ?OS ?a1 ?a3 = Eq => apply Oeset.compare_eq_trans
    | h1 : Oeset.compare ?OS ?a1 ?a2 = Eq,
      h2 : Oeset.compare ?OS ?a2 ?a3 = Lt 
      |- Oeset.compare ?OS ?a1 ?a3 = Lt => apply (Oeset.compare_eq_lt_trans _ _ _ _ h1 h2)
    | |- Oeset.compare ?OS ?a1 ?a2 = Eq -> 
         Oeset.compare ?OS ?a2 ?a3 = Lt ->
         Oeset.compare ?OS ?a1 ?a3 = Lt => apply Oeset.compare_eq_lt_trans
    | h1 : Oeset.compare ?OS ?a1 ?a2 = Lt,
      h2 : Oeset.compare ?OS ?a2 ?a3 = Eq 
      |- Oeset.compare ?OS ?a1 ?a3 = Lt => apply (Oeset.compare_lt_eq_trans _ _ _ _ h1 h2)
    | |- Oeset.compare ?OS ?a1 ?a2 = Lt -> 
         Oeset.compare ?OS ?a2 ?a3 = Eq ->
         Oeset.compare ?OS ?a1 ?a3 = Lt => apply Oeset.compare_lt_eq_trans
    | h1 : Oeset.compare ?OS ?a1 ?a2 = Lt,
      h2 : Oeset.compare ?OS ?a2 ?a3 = Lt 
      |- Oeset.compare ?OS ?a1 ?a3 = Lt => apply (Oeset.compare_lt_trans _ _ _ _ h1 h2)
    | |- Oeset.compare ?OS ?a1 ?a2 = Lt -> 
         Oeset.compare ?OS ?a2 ?a3 = Lt ->
         Oeset.compare ?OS ?a1 ?a3 = Lt => apply Oeset.compare_lt_trans
    | |- Oeset.compare ?OS ?a1 ?a2 = CompOpp (Oeset.compare ?OS ?a2 ?a1) =>
      apply Oeset.compare_lt_gt
  end.
