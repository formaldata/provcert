(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import List Bool.

Require Import BasicFacts ListFacts ListPermut OrderedSet Term Substitution Bool3 Formula FiniteSet.

Section Sec.

Hypothesis dom : Type.
Hypothesis predicate : Type.
Hypothesis symbol : Type.
Hypothesis ODom : Oset.Rcd dom.
Hypothesis OP : Oset.Rcd predicate.
Hypothesis OSymb : Oset.Rcd symbol.

Notation variable := (variable dom).

Notation term := (term dom symbol).

Inductive atom : Type := Atom : predicate -> list term -> atom.

Inductive clause : Type := Clause : list atom -> list atom -> clause.

Definition variables_a a := 
  match a with 
      | Atom _ l => Fset.Union _ (List.map (variables_t (FVR ODom OP OSymb)) l)
  end.

Definition universal_vars c :=
  match c with
    | Clause b h => Fset.Union _ (List.map variables_a (b ++ h))
  end.

Definition atom_of_atom a :=
  match a with
    | Atom p l => Formula.Atom p l
  end.

Definition formula_of_clause c :=
  let u_vars := universal_vars c in
  match c with
    | Clause body head =>
      let b := conj_list And_F (List.map atom_of_atom body) in
      let h := conj_list Or_F (List.map atom_of_atom head) in 
      let b_implies_h := Conj Or_F (Not b) h in
      quant_list Forall_F (Fset.elements _ u_vars) (Conj Or_F (Not b) h)
  end.

Lemma variables_a_unfold : 
  forall a, variables_a a = free_variables_f ODom OP OSymb (atom_of_atom a).
Proof.
intros [p l]; apply refl_equal.
Qed.

Lemma universal_vars_unfold :
  forall c, universal_vars c =
            match c with
              | Clause b h => Fset.Union _ (List.map variables_a (b ++ h))
            end.
Proof.                                                                                   
  intro c; case c; intros; apply refl_equal.
Qed.

Lemma formula_of_clause_unfold :
  forall c, formula_of_clause c =   
            let u_vars := universal_vars c in
            match c with
              | Clause body head =>
                let b := conj_list And_F (List.map atom_of_atom body) in
                let h := conj_list Or_F (List.map atom_of_atom head) in 
                let b_implies_h := Conj Or_F (Not b) h in
                quant_list Forall_F (Fset.elements _ u_vars) (Conj Or_F (Not b) h)
            end.
Proof.
  intros c; case c; intros; trivial.
Qed.

Lemma universal_vars_eq : 
  forall b1 b2 h1 h2, _permut (@eq _) b1 b2 -> _permut (@eq _) h1 h2 ->
                  universal_vars (Clause b1 h1) =S= universal_vars (Clause b2 h2).
Proof.
intros b1 b2 h1 h2 P1 P2; unfold universal_vars.
rewrite Fset.equal_spec; intro x.
rewrite eq_bool_iff, 2 Fset.mem_Union; split; intros [s [Hs Hx]]; exists s; split; trivial.
- rewrite in_map_iff in Hs; destruct Hs as [a [Hs Ha]]; subst s.
  rewrite in_map_iff; exists a; split; trivial.
  rewrite <- (in_permut_in (_permut_app P1 P2)); assumption.
- rewrite in_map_iff in Hs; destruct Hs as [a [Hs Ha]]; subst s.
  rewrite in_map_iff; exists a; split; trivial.
  rewrite (in_permut_in (_permut_app P1 P2)); assumption.
Qed.

Lemma mem_Union_variables_a :
  forall x l, x inS (Fset.Union (FVR ODom OP OSymb) (map variables_a l)) <->
              exists p, exists k, exists t, 
                                    In (Atom p k) l /\ In t k /\ 
                                    x inS variables_t (FVR ODom OP OSymb) t.
Proof.
intros x l.
rewrite Fset.mem_Union; split.
- intros [s [Hs Hx]].
  rewrite in_map_iff in Hs.
  destruct Hs as [[p k] [Hs Ha]]; subst s.
  exists p; exists k.
  unfold variables_a in Hx; rewrite Fset.mem_Union in Hx.
  destruct Hx as [s [Hs Hx]].
  rewrite in_map_iff in Hs.
  destruct Hs as [t [Hs Ht]]; subst s.
  exists t; split; [ | split]; trivial.
- intros [p [k [t [Ha [Ht Hx]]]]].
  exists (variables_a (Atom p k));
  split; [rewrite in_map_iff; exists (Atom p k); split; trivial | ].
  unfold variables_a; rewrite Fset.mem_Union.
  exists (variables_t (FVR ODom OP OSymb) t); split; [ | assumption].
  rewrite in_map_iff; exists t; split; trivial.
Qed.

Lemma var_conj_list :
  forall a l, free_variables_f ODom OP OSymb (conj_list a (map atom_of_atom l)) =
              Fset.Union _ (map variables_a l).
Proof.
intros a l.
induction l as [ | [p k] l]; [destruct a; apply refl_equal | ].
rewrite 2 (map_unfold _ (_ :: _)), conj_list_unfold, free_variables_f_unfold.
unfold Fset.Union; apply f_equal2; [apply refl_equal | apply IHl].
Qed.

Lemma free_variables_clause :
  forall c, free_variables_f  ODom OP OSymb (formula_of_clause c) =S= (emptysetS).
Proof.
intros [b h].
rewrite formula_of_clause_unfold; cbv zeta.
rewrite Fset.equal_spec; intro x.
rewrite (Fset.mem_eq_2 _ _ _ (free_variables_f_quant_list _ _ _ _ _ _)), Fset.mem_diff.
rewrite free_variables_f_unfold, Fset.mem_union.
rewrite free_variables_f_unfold.
rewrite Fset.mem_mk_set, <- Fset.mem_elements, universal_vars_unfold.
rewrite 2 var_conj_list, <- Fset.mem_Union_app, <- map_app.
rewrite Fset.mem_empty.
destruct (x inS? Fset.Union (FVR ODom OP OSymb) (map variables_a (b ++ h))); apply refl_equal.
Qed.

Definition apply_subst_a sigma (a : atom) :=
  match a with
    | Atom p l => Atom p (List.map (apply_subst (OVR ODom OP OSymb) sigma) l)
  end.

Definition apply_subst_c sigma (c : clause) :=
  match c with
    | Clause body head => 
      Clause (List.map (apply_subst_a sigma) body) (List.map (apply_subst_a sigma) head)
  end.

Lemma universal_vars_apply_subst_c :
  forall sigma c,       
    universal_vars (apply_subst_c sigma c) =S= 
    Fset.Union _ (map (fun x => 
                         variables_t (FVR ODom OP OSymb) 
                                     (apply_subst (OVR ODom OP OSymb) sigma (Var x)))
                      (Fset.elements _ (universal_vars c))).
Proof.
intros sigma [b h]; unfold universal_vars.
unfold apply_subst_c, universal_vars; rewrite map_app, 2 map_map, <- map_app.
rewrite Fset.equal_spec; intro x.
rewrite eq_bool_iff, 2 Fset.mem_Union; split.
- intros [s [Hs Hx]]; rewrite in_map_iff in Hs.
  destruct Hs as [[p l] [Hs Kx]]; subst s.
  unfold apply_subst_a in Hx; rewrite variables_a_unfold in Hx.
  unfold atom_of_atom in Hx; rewrite free_variables_f_unfold, map_map in Hx.
  rewrite Fset.mem_Union in Hx.
  destruct Hx as [s [Hs Hx]]; rewrite in_map_iff in Hs.
  destruct Hs as [t [Hs Ht]]; subst s.
  rewrite var_in_apply_subst in Hx.
  destruct Hx as [x' [Hx' Hx]].
  exists (variables_t (FVR ODom OP OSymb)
             (apply_subst (OVR ODom OP OSymb) sigma (Var x'))); split; [ | assumption].
  rewrite in_map_iff; exists x'; split; [trivial | ].
  apply Fset.mem_in_elements, Fset.mem_Union.
  exists (variables_a (Atom p l)); split.
  + rewrite in_map_iff; exists (Atom p l); split; trivial.
  + unfold variables_a; rewrite Fset.mem_Union.
    exists (variables_t (FVR ODom OP OSymb) t); split; [ | assumption].
    rewrite in_map_iff; exists t; split; trivial.
- intros [s [Hs Hx]]; rewrite in_map_iff in Hs.
  destruct Hs as [x' [Hs H]]; subst s.
  generalize (Fset.in_elements_mem _ _ _ H); clear H; intro H.  
  rewrite Fset.mem_Union in H.
  destruct H as [s [Hs H]]; rewrite in_map_iff in Hs.
  destruct Hs as [[p l] [Hs H']]; subst s.
  exists (variables_a (apply_subst_a sigma (Atom p l))); split;
  [rewrite in_map_iff; exists (Atom p l); split; trivial | ].
  unfold variables_a in *; rewrite Fset.mem_Union in H.
  destruct H as [s [Hs H]]; rewrite in_map_iff in Hs.
  destruct Hs as [t [Hs Ht]]; subst s.
  unfold apply_subst_a; rewrite map_map, Fset.mem_Union.
  exists (variables_t (FVR ODom OP OSymb)
             (apply_subst (OVR ODom OP OSymb) sigma t)); split;
  [rewrite in_map_iff; exists t; split; trivial | ].
  rewrite var_in_apply_subst.
  exists x'; split; trivial.
Qed.

Section Interp.

Hypothesis B : Bool.Rcd.
Hypothesis value : Type.
Hypothesis OVal : Oset.Rcd value.
Hypothesis ip : predicate -> list value -> Bool.b B.
Hypothesis is : symbol -> list value -> value.
Hypothesis default_value : dom -> value.
Hypothesis I : dom -> list value.
Hypothesis default_value_in_dom : forall d, In (default_value d) (I d).

Fixpoint interp_term (i : variable -> value) (t : term) : value :=
  match t with
    | Var x => i x
    | Term f l => is f (map (interp_term i) l)
  end.

Lemma interp_term_unfold :
  forall i t, 
    interp_term i t = match t with
                          | Var x => i x
                          | Term f l => is f (map (interp_term i) l)
                      end.
Proof.
intros i t; case t; intros; apply refl_equal.
Qed.

Lemma interp_term_eq_rec :
  forall (i1 i2 : variable -> value) (v : variable) 
     (x1 x2 : value) (s : Fset.set (FVR ODom OP OSymb)),
    Oeset.compare (Oset.oeset_of_oset OVal) x1 x2 = Eq ->
   (forall t : term,
    variables_t (FVR ODom OP OSymb) t
    subS Fset.filter (FVR ODom OP OSymb)
           (fun y : variable => negb (Oset.eq_bool (OVR ODom OP OSymb) v y))
           s -> interp_term i1 t = interp_term i2 t) ->
   forall t : term,
   variables_t (FVR ODom OP OSymb) t subS s ->
   interp_term (ivar_xt ODom OP OSymb i1 v x1) t =
   interp_term (ivar_xt ODom OP OSymb i2 v x2) t.
Proof.
intros i1 i2 x1 x2 v s Hx H t; simpl in Hx.
rewrite Oset.compare_eq_iff in Hx; subst x2.
pattern t; apply term_rec3; clear t.
- intros y Hy; rewrite 2 interp_term_unfold.
  unfold ivar_xt.
  assert (Ky := H (Var y)); rewrite 2 interp_term_unfold in Ky.
  case_eq (Oset.compare (OVR ODom OP OSymb) x1 y); intro Jy; [apply refl_equal | | ].
  + apply Ky.
    rewrite Fset.subset_spec in Hy; rewrite Fset.subset_spec; intros z Hz.
    rewrite Fset.mem_filter, (Hy z Hz).
    rewrite variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff in Hz; subst z.
    unfold Oset.eq_bool; rewrite Jy; apply refl_equal.
  + apply Ky.
    rewrite Fset.subset_spec in Hy; rewrite Fset.subset_spec; intros z Hz.
    rewrite Fset.mem_filter, (Hy z Hz).
    rewrite variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff in Hz; subst z.
    unfold Oset.eq_bool; rewrite Jy; apply refl_equal.
- intros f l IH Hl.
  rewrite 2 (interp_term_unfold _ (Term _ _)).
  apply f_equal; rewrite <- map_eq.
  intros t Ht; apply IH; trivial.
  rewrite Fset.subset_spec in Hl; rewrite Fset.subset_spec; intros z Hz.
  apply Hl; rewrite variables_t_unfold, Fset.mem_Union.
  exists (variables_t (FVR ODom OP OSymb) t); split; [ | assumption].
  rewrite in_map_iff; exists t; split; trivial.
Qed.

Lemma interp_term_eq :
  forall i1 i2 t, 
    (forall x, x inS variables_t (FVR ODom OP OSymb) t -> 
               Oeset.compare (Oset.oeset_of_oset OVal) (i1 x) (i2 x) = Eq) -> 
    interp_term i1 t = interp_term i2 t.
Proof.
intros i1 i2 t _Ht.
assert (Ht : forall x, x inS variables_t (FVR ODom OP OSymb) t -> i1 x = i2 x).
{
  intros x Hx.
  generalize (_Ht _ Hx); simpl.
  rewrite Oset.compare_eq_iff; exact (fun h => h).
}
clear _Ht; revert Ht; pattern t; apply term_rec3; clear t.
- intros v H; simpl; apply H.
  rewrite variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff.
  apply refl_equal.
- intros f l IH Hl.
  rewrite 2 (interp_term_unfold _ (Term _ _)).
  apply f_equal; rewrite <- map_eq.
  intros t Ht; apply IH; trivial.
  intros z Hz.
  apply Hl; rewrite variables_t_unfold, Fset.mem_Union.
  exists (variables_t (FVR ODom OP OSymb) t); split; [ | assumption].
  rewrite in_map_iff; exists t; split; trivial.
Qed.

Definition interp_clause i c := 
  interp_formula ODom OP OSymb (value := value) (base_value := value) B ip interp_term I i 
                 (formula_of_clause c).

Lemma interp_clause_eq : 
  forall b1 b2 h1 h2,
    _permut (@eq _) b1 b2 -> _permut (@eq _) h1 h2 ->
    forall i, interp_clause i (Clause b1 h1) = interp_clause i (Clause b2 h2).
Proof.
intros b1 b2 h1 h2 Hb Hh i.
unfold interp_clause; rewrite 2 formula_of_clause_unfold; cbv zeta.
rewrite <- (Fset.elements_spec1 _ _ _ (universal_vars_eq Hb Hh)).
set (l1 := Fset.elements _ (universal_vars (Clause b1 h1))).
clearbody l1.
revert i; induction l1 as [ | [d1 n1] l1]; intro i; simpl.
- apply f_equal2; [apply f_equal | ].
  + apply conj_list_permut_true.
    refine (_permut_map _ _ _ _ Hb); intros; subst; trivial.
  + apply conj_list_permut_true.
    refine (_permut_map _ _ _ _ Hh); intros; subst; trivial.
- apply (Bool.forallb_eq B (Oset.oeset_of_oset OVal)); [apply Oeset.permut_refl | ].
  intros x1 x2 H Hx.
  rewrite IHl1.
  apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); apply interp_term_eq_rec; trivial.
Qed.

Lemma clauses_are_ground :
  forall c i1 i2, interp_clause i1 c = interp_clause i2 c.
Proof.
intros [b h] i1 i2; unfold interp_clause.
apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); [apply interp_term_eq_rec | ].
intros t Ht; apply interp_term_eq.
intros x Hx.
rewrite Fset.subset_spec in Ht.
assert (Kx := Ht _ Hx).
rewrite (Fset.mem_eq_2 _ _ _ (free_variables_clause _)), Fset.mem_empty in Kx.
discriminate Kx.
Qed.

Lemma interp_clause_true_iff :
  forall b h,
    forall i, 
    interp_clause i (Clause b h) = Bool.true B <->
     (forall i_u, (forall d n, (Vrbl d n) inS universal_vars (Clause b h) ->
                               In (i_u (Vrbl d n)) (I d)) ->
       interp_formula 
         ODom OP OSymb (value := value) (base_value := value) 
          B ip interp_term I i_u (conj_list And_F (List.map atom_of_atom b)) <> Bool.false B ->
       interp_formula 
         ODom OP OSymb (value := value) (base_value := value) 
         B ip interp_term I i_u (conj_list Or_F (List.map atom_of_atom h)) = Bool.true B).
Proof.
intros b h i.
unfold interp_clause; rewrite formula_of_clause_unfold.
rewrite (@quant_forall_set_true_iff _ _ _ ODom OP OSymb _ _ (Oset.oeset_of_oset OVal) _ _ _  
                                    interp_term_eq_rec interp_term_eq); split.
- intros H ii Wii Hii.
  assert (K : interp_formula ODom OP OSymb B ip interp_term I ii
        (Conj Or_F (Not (conj_list And_F (map atom_of_atom b)))
              (conj_list Or_F (map atom_of_atom h))) = Bool.true B).
  {
    rewrite <- (H ii).
    - apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); [apply interp_term_eq_rec | ].
      intros t Ht; apply interp_term_eq.
      intros x Hx.
      rewrite Fset.subset_spec in Ht.
      assert (Kx := Ht _ Hx).
      rewrite free_variables_f_unfold, Fset.mem_union, free_variables_f_unfold in Kx.
      rewrite 2 var_conj_list, <- Fset.mem_Union_app, <- map_app in Kx.
      rewrite <- Fset.mem_elements, universal_vars_unfold, Kx.
      apply Oeset.compare_eq_refl.
    - intros d n Hn.
      apply Wii; apply Fset.in_elements_mem; assumption.
  } 
  rewrite interp_formula_unfold in K; unfold interp_conj in K.
  rewrite Bool.orb_true_iff in K.
  rewrite interp_formula_unfold in K; destruct K as [K | K]; [ | apply K].
  generalize (f_equal (Bool.negb B) K).
  rewrite Bool.negb_negb, Bool.negb_true; clear K; intro K.
  rewrite conj_and_list_false_iff in K; destruct K as [f [Hf Kf]].
  apply False_rec; apply Hii; rewrite conj_and_list_false_iff.
  exists f; split; assumption.
- intros H ii Wii.
  rewrite interp_formula_unfold; unfold interp_conj.
  rewrite Bool.orb_true_iff, interp_formula_unfold.
  case_eq (Bool.is_true B
             (Bool.negb B
                (interp_formula ODom OP OSymb B ip interp_term I
                   (fun x : variable =>
                      if Oset.mem_bool (OVR ODom OP OSymb) x ({{{universal_vars (Clause b h)}}})
                      then ii x
                      else i x) (conj_list And_F (map atom_of_atom b))))); intro Hii.
  + left; rewrite Bool.true_is_true in Hii; apply Hii.
  + right; rewrite conj_or_list_true_iff.
    assert (Aux : let i_u := 
                      (fun x : variable =>
                         if Oset.mem_bool (OVR ODom OP OSymb) x ({{{universal_vars (Clause b h)}}})
                         then ii x
                         else i x) in
              interp_formula 
                ODom OP OSymb B ip interp_term I i_u (conj_list Or_F (map atom_of_atom h)) =
              Bool.true B).
    {
      apply H.
      - intros d n Hdn; rewrite <- Fset.mem_elements, Hdn; apply Wii.
        rewrite Fset.mem_elements, Oset.mem_bool_true_iff in Hdn; apply Hdn.
      - intro Hb; rewrite Hb, Bool.negb_false in Hii.
        assert (Aux := refl_equal (Bool.true B)).
        rewrite <- Bool.true_is_true in Aux; rewrite Aux in Hii; discriminate Hii.
    }
    rewrite conj_or_list_true_iff in Aux; apply Aux.
Qed.

Section WF.

Hypothesis dom_of_value : value -> dom.
Hypothesis WI : forall d v, In v (I d) -> dom_of_value v = d.
Hypothesis WI' : forall v, In v (I (dom_of_value v)).

Hypothesis arity_p : predicate -> (list dom). 
Hypothesis arity_s : symbol -> (list dom * dom). 

Hypothesis ip_err_top : 
  forall p l, map dom_of_value l <> arity_p p -> ip p l = Bool.false B.

Hypothesis w_is : 
  forall f l, map dom_of_value l = fst (arity_s f) -> dom_of_value (is f l) = snd (arity_s f).

Definition dom_of_term t :=
  match t with
    | Var (Vrbl d _) => d
    | Term f _ => snd (arity_s f)
  end.

Fixpoint wf_t (t : term) :=
(*  negb (Oset.eq_bool ODom (dom_of_term t) (dom_error)) && *)
  match t with
    | Var (Vrbl d _) => true
    | Term f l => 
      Oset.eq_bool (Oset.mk_list ODom) (map dom_of_term l) (fst (arity_s f)) && 
                   (forallb wf_t l) 
  end.

Definition wf_subst (sigma : substitution dom symbol) : bool :=
  forallb 
    (fun xt => match xt with 
                 | (Vrbl d _, t) => Oset.eq_bool ODom (dom_of_term t) d && wf_t t
               end) sigma.

Definition wf_i (t : term) i :=
  forall d n, Vrbl d n inS (variables_t (FVR ODom OP OSymb) t) -> dom_of_value (i (Vrbl d n)) = d.

Lemma wf_t_unfold :
  forall t, wf_t t =
            match t with
              | Var (Vrbl d _) => true
              | Term f l => 
                Oset.eq_bool (Oset.mk_list ODom) (map dom_of_term l) (fst (arity_s f)) && 
                             (forallb wf_t l) 
            end.
Proof.
intros t; case t; intros; apply refl_equal.
Qed.

Lemma dom_of_value_interp_term :
  forall t i, wf_t t = true -> wf_i t i -> dom_of_value (interp_term i t) = dom_of_term t.
Proof.
intro t; pattern t; apply term_rec3; clear t.
- intros [d n] i W Hi; simpl; apply Hi.
  rewrite variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff.
  apply refl_equal.
- intros f l IH i W Hi; simpl.
  apply w_is.
  rewrite wf_t_unfold, Bool.Bool.andb_true_iff, Oset.eq_bool_true_iff, forallb_forall in W.
  rewrite <- (proj1 W), map_map, <- map_eq.
  intros t Ht; apply (IH _ Ht).
  + apply (proj2 W _ Ht).
  + intros n d Hn; apply Hi.
    rewrite variables_t_unfold, Fset.mem_Union.
    exists (variables_t (FVR ODom OP OSymb) t); split; [ | assumption].
    rewrite in_map_iff; exists t; split; trivial.
Qed.

Lemma interp_term_dom_of_term :
  forall i t, wf_t t = true -> wf_i t i -> In (interp_term i t) (I (dom_of_term t)).
Proof.
intros i t W Hi.
rewrite <- (dom_of_value_interp_term (i := i) W); [ | assumption].
apply WI'.
Qed.

Lemma instantiation_is_sound :
  forall c sigma, wf_subst sigma = true -> 
  forall i, interp_clause i c = Bool.true B -> interp_clause i (apply_subst_c sigma c) = Bool.true B.
Proof.
intros [b h] sigma W i H.
unfold apply_subst_c.
rewrite interp_clause_true_iff.
intros i_u Hv Hb.
assert (Kb : interp_formula
               ODom OP OSymb B ip interp_term I 
               (fun x => interp_term i_u (apply_subst (OVR ODom OP OSymb) sigma (Var x)))
               (conj_list And_F (map atom_of_atom b)) <> Bool.false B).
{
  rewrite conj_and_list_diff_false_iff in Hb.
  rewrite conj_and_list_diff_false_iff; intros f Hf Kf.
  rewrite in_map_iff in Hf; destruct Hf as [[p l] [Hf Ha]]; subst f.
  refine (Hb _ _ _).
  + rewrite map_map, in_map_iff; eexists; split; [apply refl_equal | apply Ha].
  + rewrite <- Kf; simpl atom_of_atom; rewrite 2 interp_formula_unfold.
    apply f_equal; rewrite map_map, <- map_eq.
    intros t _.
    pattern t; apply term_rec3; clear t.
    * intro v; rewrite apply_subst_unfold, (interp_term_unfold _ (Var _)).
      apply refl_equal.
    * intros f l1 IH.
      rewrite apply_subst_unfold, 2 (interp_term_unfold _ (Term _ _)).
      apply f_equal; rewrite map_map, <- map_eq.
      intros t Ht; apply IH; trivial.
}
rewrite interp_clause_true_iff in H.
assert (H' : interp_formula ODom OP OSymb B ip interp_term I
                            (fun x : variable =>
                               interp_term i_u
                                           (apply_subst (OVR ODom OP OSymb) sigma (Var x)))
                            (conj_list Or_F (map atom_of_atom h)) = Bool.true B).
{
  apply H.
  - intros d n Hn.
    assert (Hd : dom_of_value 
                    (interp_term 
                       i_u
                       (apply_subst (OVR ODom OP OSymb) sigma (Var (Vrbl d n)))) = d).
    {
      unfold wf_subst in W; rewrite forallb_forall in W.
      simpl.
      case_eq (Oset.find (OVR ODom OP OSymb) (Vrbl d n) sigma).
      - intros t Ht.
        assert (Wt := W _ (Oset.find_some _ _ _ Ht)); simpl in Wt.
        rewrite Bool.Bool.andb_true_iff, Oset.eq_bool_true_iff in Wt.
        rewrite <- (proj1 Wt).
        apply dom_of_value_interp_term; [apply (proj2 Wt) | ].
        intros d1 n1 Hn1; apply WI; apply Hv.
        rewrite universal_vars_unfold, mem_Union_variables_a in Hn.
        destruct Hn as [p [k [u [Hn [Kn Jn]]]]].
        rewrite universal_vars_unfold, mem_Union_variables_a.
        exists p; exists (map (apply_subst (OVR ODom OP OSymb) sigma) k);
        exists (apply_subst (OVR ODom OP OSymb) sigma u); split; [ | split].
        + rewrite <- map_app, in_map_iff.
          exists (Atom p k); split; trivial.
        + rewrite in_map_iff; exists u; split; trivial.
        + rewrite var_in_apply_subst.
          exists (Vrbl d n); split; trivial.
          simpl apply_subst; rewrite Ht; assumption.
      - intros Ht; apply WI; apply Hv.
        rewrite universal_vars_unfold, mem_Union_variables_a in Hn.
        destruct Hn as [p [k [u [Hn [Kn Jn]]]]].
        rewrite universal_vars_unfold, mem_Union_variables_a.
        exists p; exists (map (apply_subst (OVR ODom OP OSymb) sigma) k);
        exists (apply_subst (OVR ODom OP OSymb) sigma u); split; [ | split].
        + rewrite <- map_app, in_map_iff.
          exists (Atom p k); split; trivial.
        + rewrite in_map_iff; exists u; split; trivial.
        + rewrite var_in_apply_subst.
          exists (Vrbl d n); split; trivial.
          simpl apply_subst.
          rewrite Ht, variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff.
          apply refl_equal.
    }
    rewrite <- Hd at 2; apply WI'.
  - assumption.
}
rewrite conj_or_list_true_iff in H'.
destruct H' as [f [Hf H']]; rewrite in_map_iff in Hf.
destruct Hf as [[p l] [Hf Ha]]; subst f.
simpl atom_of_atom in H'; rewrite interp_formula_unfold in H'.
rewrite conj_or_list_true_iff.
exists (atom_of_atom (apply_subst_a sigma (Atom p l))); 
  split; [rewrite map_map, in_map_iff; exists (Atom p l); split; trivial | ].
rewrite <- H'; simpl atom_of_atom; rewrite interp_formula_unfold.
apply f_equal; rewrite map_map, <- map_eq.
intros t _; pattern t; apply term_rec3; clear t.
- intro v; apply refl_equal.
- intros f k IH; simpl; apply f_equal.
  rewrite map_map, <- map_eq; apply IH.
Qed.

Inductive step : clause -> clause -> clause -> Type :=
| Resolution : 
    forall a1 b1 h1 a2 b2 h2 sigma1 sigma2
           (Ws1 : wf_subst sigma1 = true)
           (Ws2 : wf_subst sigma2 = true)
           (Hs : apply_subst_a sigma1 a1 = apply_subst_a sigma2 a2),
      step (Clause (a1 :: b1) h1) (Clause b2 (a2 :: h2)) 
           (Clause (map (apply_subst_a sigma1) b1 ++ map (apply_subst_a sigma2) b2) 
                   (map (apply_subst_a sigma1) h1 ++ map (apply_subst_a sigma2) h2)).

Lemma resolution_is_sound :
  forall c1 c2 c, step c1 c2 c -> 
    forall i, interp_clause i c1 = Bool.true B -> interp_clause i c2 = Bool.true B -> 
              interp_clause i c = Bool.true B.
Proof.
intros [_b1 h1] [_b2 _h2] c H i; 
  inversion H as [a1 b1 k1 a2 b2 h2 sigma1 sigma2 Ws1 Ws2 Hs];
  subst _b1 _b2 _h2 k1 c.
intros Hc1 Hc2.
rewrite interp_clause_true_iff.
intros i_u K1 K2.
rewrite conj_and_list_diff_false_iff, map_app, 2 map_map in K2.
set (i_u' := fun x => if x inS? universal_vars
                           (Clause
                              (map (apply_subst_a sigma1) b1 ++
                                   map (apply_subst_a sigma2) b2)
                              (map (apply_subst_a sigma1) h1 ++
                                   map (apply_subst_a sigma2) h2))
                      then i_u x
                      else match x with 
                             | Vrbl d n => default_value d
                           end).
rewrite (clauses_are_ground _ i i_u') in Hc1.
rewrite (clauses_are_ground _ i i_u') in Hc2.
assert (Kc1 := instantiation_is_sound (Clause (a1 :: b1) h1) sigma1 Ws1 i_u' Hc1).
assert (Kc2 := instantiation_is_sound (Clause b2 (a2 :: h2)) sigma2 Ws2 i_u' Hc2).
unfold apply_subst_c in Kc1, Kc2; rewrite interp_clause_true_iff in Kc1, Kc2.
case_eq (Bool.is_true 
           _
           (interp_formula 
           ODom OP OSymb B ip interp_term I i_u' (atom_of_atom (apply_subst_a sigma1 a1)))); 
  intro Ha.
- assert (Hh1 : interp_formula ODom OP OSymb B ip interp_term I i_u'
                             (conj_list Or_F (map atom_of_atom (map (apply_subst_a sigma1) h1))) =
              Bool.true B).
  {
    apply Kc1.
    - intros d n Hn; unfold i_u'.
      case_eq (Vrbl d n
                    inS? universal_vars
                    (Clause
                       (map (apply_subst_a sigma1) b1 ++
                            map (apply_subst_a sigma2) b2)
                       (map (apply_subst_a sigma1) h1 ++
                            map (apply_subst_a sigma2) h2))); intro Kn.
      + apply K1; assumption.
      + apply default_value_in_dom.
    - rewrite conj_and_list_diff_false_iff, map_map, (map_unfold _ (_ :: _)).
      intros f Hf; simpl in Hf; destruct Hf as [Hf | Hf].
      + rewrite Bool.true_is_true in Ha; subst f; rewrite Ha.
        apply Bool.true_diff_false.
      + intro Kf; apply (K2 f); [apply in_or_app; left; assumption | ].
        rewrite <- Kf; apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); 
          [apply interp_term_eq_rec | ].
        intros t Ht; apply interp_term_eq.
        intros x Hx; unfold i_u'.
        assert (Kx : x
                       inS universal_vars
                       (Clause
                          (map (apply_subst_a sigma1) b1 ++
                               map (apply_subst_a sigma2) b2)
                          (map (apply_subst_a sigma1) h1 ++
                               map (apply_subst_a sigma2) h2))).
        {
          unfold universal_vars; rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; left.
          rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; left.
          rewrite map_map, Fset.mem_Union.
          rewrite in_map_iff in Hf.
          destruct Hf as [a [Hf _Ha]]; subst f.
          exists (variables_a (apply_subst_a sigma1 a)); split.
          - rewrite in_map_iff; exists a; split; trivial.
          - rewrite variables_a_unfold.
            rewrite Fset.subset_spec in Ht; apply Ht; assumption.
        } 
        rewrite Kx; apply Oeset.compare_eq_refl.
  }
  rewrite conj_or_list_true_iff in Hh1.
  destruct Hh1 as [f [Hf Hh1]].
  rewrite conj_or_list_true_iff.
  exists f; split.
  + rewrite map_app; apply in_or_app; left; assumption.
  + rewrite <- Hh1.
    apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); [apply interp_term_eq_rec | ].
    intros t Ht; apply interp_term_eq.
    intros x Hx; unfold i_u'.
    assert (Kx : x
                   inS universal_vars
                   (Clause
                      (map (apply_subst_a sigma1) b1 ++
                           map (apply_subst_a sigma2) b2)
                      (map (apply_subst_a sigma1) h1 ++
                           map (apply_subst_a sigma2) h2))).
    {
      unfold universal_vars; rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; right.
      rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; left.
      rewrite map_map, Fset.mem_Union.
      rewrite map_map, in_map_iff in Hf.
      destruct Hf as [a [Hf _Ha]]; subst f.
      exists (variables_a (apply_subst_a sigma1 a)); split.
      - rewrite in_map_iff; exists a; split; trivial.
      - rewrite variables_a_unfold.
        rewrite Fset.subset_spec in Ht; apply Ht; assumption.
    } 
    rewrite Kx; apply Oeset.compare_eq_refl.
- assert (Hh2 : interp_formula 
                  ODom OP OSymb B ip interp_term I i_u'
                  (conj_list Or_F (map atom_of_atom (map (apply_subst_a sigma2) (a2 :: h2)))) =
              Bool.true B).
  {
    apply Kc2.
    - intros d n Hn; unfold i_u'.
      case_eq (Vrbl d n
                    inS? universal_vars
                    (Clause
                       (map (apply_subst_a sigma1) b1 ++
                            map (apply_subst_a sigma2) b2)
                       (map (apply_subst_a sigma1) h1 ++
                            map (apply_subst_a sigma2) h2))); intro Kn.
      + apply K1; assumption.
      + apply default_value_in_dom.
    - rewrite conj_and_list_diff_false_iff, map_map.
      intros f Hf Kf.
      apply (K2 f); [apply in_or_app; right; assumption | ].
      rewrite <- Kf; apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); 
        [apply interp_term_eq_rec | ].
      intros t Ht; apply interp_term_eq.
      intros x Hx; unfold i_u'.
      assert (Kx : x
                     inS universal_vars
                     (Clause
                        (map (apply_subst_a sigma1) b1 ++
                             map (apply_subst_a sigma2) b2)
                        (map (apply_subst_a sigma1) h1 ++
                             map (apply_subst_a sigma2) h2))).
      {
        unfold universal_vars; rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; left.
        rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; right.
        rewrite map_map, Fset.mem_Union.
        rewrite in_map_iff in Hf.
        destruct Hf as [a [Hf _Ha]]; subst f.
        exists (variables_a (apply_subst_a sigma2 a)); split.
        - rewrite in_map_iff; exists a; split; trivial.
        - rewrite variables_a_unfold.
          rewrite Fset.subset_spec in Ht; apply Ht; assumption.
      } 
      rewrite Kx; apply Oeset.compare_eq_refl.
  }
  rewrite conj_or_list_true_iff, map_map, (map_unfold _ (_ :: _)) in Hh2.
  destruct Hh2 as [f [Hf Hh2]].
  simpl in Hf; destruct Hf as [Hf | Hf].
  + subst f; rewrite Hs, Hh2 in Ha.
    assert (Aux := refl_equal (Bool.true B)).
    rewrite <- Bool.true_is_true, Ha in Aux; discriminate Aux.
  + rewrite conj_or_list_true_iff.
    exists f; split.
    * rewrite map_app, 2 map_map; apply in_or_app; right; assumption.
    * rewrite <- Hh2.
      apply (interp_formula_eq B (Oset.oeset_of_oset OVal)); [apply interp_term_eq_rec | ].
      intros t Ht; apply interp_term_eq.
      intros x Hx; unfold i_u'.
      assert (Kx : x
                     inS universal_vars
                     (Clause
                        (map (apply_subst_a sigma1) b1 ++
                             map (apply_subst_a sigma2) b2)
                        (map (apply_subst_a sigma1) h1 ++
                             map (apply_subst_a sigma2) h2))).
      {
        unfold universal_vars; rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; right.
        rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff; right.
        rewrite map_map, Fset.mem_Union.
        rewrite in_map_iff in Hf.
        destruct Hf as [a [Hf _Ha]]; subst f.
        exists (variables_a (apply_subst_a sigma2 a)); split.
        - rewrite in_map_iff; exists a; split; trivial.
        - rewrite variables_a_unfold.
          rewrite Fset.subset_spec in Ht; apply Ht; assumption.
      } 
      rewrite Kx; apply Oeset.compare_eq_refl.
Qed.

End WF.

End Interp.

End Sec.

