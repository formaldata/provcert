(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Asymmetric Patterns.
Set Implicit Arguments.

Require Import Bool List NArith.
Require Import BasicFacts ListFacts OrderedSet Term Tree FiniteSet ListPermut ListSort.

Inductive and_or : Type :=
  | And_F 
  | Or_F.

Inductive quantifier : Type :=
  | Forall_F
  | Exists_F.

Section Sec.

Hypothesis dom : Type.
Hypothesis predicate : Type.
Hypothesis symbol : Type.

Notation variable := (variable dom).

Notation term := (term dom symbol).

Inductive formula : Type :=
  | TTrue : formula
  | Atom : predicate -> list term -> formula
  | Conj : and_or -> formula -> formula -> formula
  | Not : formula -> formula 
  | Quant : quantifier -> variable -> formula -> formula.

(* begin hide *)

Inductive All_F : Type :=
  | All_F_dom : dom -> All_F
  | All_F_N : N -> All_F
  | All_F_predicate : predicate -> All_F
  | All_F_symbol : symbol -> All_F.

Open Scope N_scope.

Definition N_of_All_F (a : All_F) : N :=
  match a with
  | All_F_dom _ => 0
  | All_F_N _ => 1
  | All_F_predicate _ => 2
  | All_F_symbol _ => 3
  end.

Definition all_f_compare dom_compare predicate_compare symbol_compare a1 a2 :=
   match Ncompare (N_of_All_F a1) (N_of_All_F a2) with
   | Lt => Lt
   | Gt => Gt
   | Eq =>
     match a1, a2 with
       | All_F_dom s1, All_F_dom s2 => dom_compare s1 s2
       | All_F_N n1, All_F_N n2 => Oset.compare ON n1 n2
       | All_F_predicate p1, All_F_predicate p2 => predicate_compare p1 p2
       | All_F_symbol s1, All_F_symbol s2 => symbol_compare s1 s2
       | _, _ => Eq
     end
   end.

Section Compare.

Hypothesis ODom : Oset.Rcd dom.
Hypothesis OP : Oset.Rcd predicate.
Hypothesis OSymb : Oset.Rcd symbol.

Definition OAll_F : Oset.Rcd All_F.
split with (all_f_compare (Oset.compare ODom) (Oset.compare OP) (Oset.compare OSymb)); 
            unfold all_f_compare.
(* 1/3 *)
- intros a1 a2.
  case a1; clear a1;
  (intro x1; case a2; clear a2; try discriminate; intro x2; simpl).
  + generalize (Oset.eq_bool_ok ODom x1 x2);
    case (Oset.compare ODom x1 x2).
    * apply f_equal.
    * intros H AH; injection AH; apply H.
    * intros H AH; injection AH; apply H.
  + generalize (Oset.eq_bool_ok ON x1 x2); simpl;
    case (Ncompare x1 x2).
    * apply f_equal.
    * intros H AH; injection AH; apply H.
    * intros H AH; injection AH; apply H.
  + generalize (Oset.eq_bool_ok OP x1 x2);
    case (Oset.compare OP x1 x2).
    * apply f_equal.
    * intros H AH; injection AH; apply H.
    * intros H AH; injection AH; apply H.
  + generalize (Oset.eq_bool_ok OSymb x1 x2);
    case (Oset.compare OSymb x1 x2).
    * apply f_equal.
    * intros H AH; injection AH; apply H.
    * intros H AH; injection AH; apply H.
- (* 2/3 *)
  intros a1 a2 a3.
  case a1; clear a1; 
  (intro x1; case a2; clear a2; try discriminate; 
   (intro x2; case a3; clear a3; intro x3; simpl; 
    try (trivial || discriminate || apply (Oset.compare_lt_trans _ x1 x2 x3)))).
  apply (Oset.compare_lt_trans ON x1 x2 x3).
- (* 3/3 *)
  intros a1 a2.
  case a1; clear a1; 
  (intro x1; case a2; clear a2; intro x2; simpl; 
   try (trivial || discriminate || apply (Oset.compare_lt_gt _ x1 x2))).
  apply (Oset.compare_lt_gt ON).
Defined.

Definition tree_of_dom (s : dom) : tree All_F := 
  Node 1 (Leaf (All_F_dom s) :: nil).

Definition tree_of_var (v : variable) : tree All_F :=
    match v with
  | Vrbl s n => Node 2 ((tree_of_dom s) :: (Leaf (All_F_N n)) :: nil)
  end.

Fixpoint tree_of_term (t : term) : tree All_F :=
  match t with
    | Var v => Node 3 (tree_of_var v :: nil)
    | Term f l => Node 4 (Leaf (All_F_symbol f) :: (List.map tree_of_term l))
  end.

Fixpoint tree_of_formula (f : formula) : tree All_F :=
  match f with
 | TTrue => Node 6 (Leaf (All_F_N 0):: nil) 
 | Atom p l => Node 5 (Leaf (All_F_predicate p) :: (List.map tree_of_term l))
 | Conj c f1 g1 => 
     Node 
       (match c with And_F => 10 | Or_F => 11 end)
       ((tree_of_formula f1) :: (tree_of_formula g1) :: nil)
 | Not f1 => Node 12 ((tree_of_formula f1) :: nil)
 | Quant q x f1 => 
     Node 
       (match q with Forall_F => 13 | Exists_F => 14 end)
       ((tree_of_var x) :: (tree_of_formula f1) :: nil)
 end.

Close Scope N_scope.

Definition OVR : Oset.Rcd variable.
split with (fun v1 v2 => Oset.compare (OTree OAll_F) (tree_of_var v1) (tree_of_var v2)).
- intros a1 a2.
  generalize (Oset.eq_bool_ok (OTree OAll_F) (tree_of_var a1) (tree_of_var a2)).
  case (Oset.compare (OTree OAll_F) (tree_of_var a1) (tree_of_var a2)).
  + destruct a1 as [d1 v1]; destruct a2 as [d2 v2]; simpl.
    intro H; injection H; clear H.
    do 2 intro; apply f_equal2; assumption.
  + intros H K; apply H; apply f_equal; assumption.
  + intros H K; apply H; apply f_equal; assumption.
- do 3 intro; apply Oset.compare_lt_trans.
- do 2 intro; apply Oset.compare_lt_gt.
Defined.

Definition FVR := Fset.build OVR.

(* end hide *)

Section Var.

(** Variables occurring in terms, atoms and formulae. *)

Fixpoint free_variables_f (f : formula) :=
  match f with
  | TTrue => Fset.empty FVR
  | Atom _ l => Fset.Union _ (List.map (variables_t FVR) l)
  | Conj _ f1 f2  => (free_variables_f f1) unionS (free_variables_f f2)
  | Not f1 => free_variables_f f1
  | Quant _ x f1 =>
     Fset.filter FVR (fun y => negb (Oset.eq_bool OVR x y)) (free_variables_f f1)
  end.

Lemma free_variables_f_unfold :
  forall f, free_variables_f f = 
            match f with
              | TTrue => Fset.empty FVR
              | Atom _ l => Fset.Union _ (List.map (variables_t FVR) l)
              | Conj _ f1 f2 => (free_variables_f f1) unionS (free_variables_f f2)
              | Not f1 => free_variables_f f1
              | Quant _ x f1 =>
                Fset.filter FVR (fun y => negb (Oset.eq_bool OVR x y)) (free_variables_f f1)
            end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Fixpoint variables_f (f : formula) :=
  match f with
  | TTrue => Fset.empty FVR
  | Atom _ l => Fset.Union _ (List.map (variables_t FVR) l)
  | Conj _ f1 f2  => (variables_f f1) unionS (variables_f f2)
  | Not f1 => variables_f f1
  | Quant _ x f1 => Fset.add _ x (variables_f f1)
  end.

Lemma variables_f_unfold :
  forall f, variables_f f = 
            match f with
              | TTrue => Fset.empty FVR
              | Atom _ l =>  Fset.Union _ (List.map (variables_t FVR) l)
              | Conj _ f1 f2  => (variables_f f1) unionS (variables_f f2)
              | Not f1 => variables_f f1
              | Quant _ x f1 => Fset.add _ x (variables_f f1)
  end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma in_variables_t_size :
  forall x t, x inS variables_t FVR t -> tree_size (tree_of_var x) < tree_size (tree_of_term t).
Proof.
intros x t; pattern t; apply term_rec3; clear t.
- intros v H; rewrite variables_t_unfold in H.
  rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in H.
  subst v; simpl.
  rewrite <- plus_n_O; apply le_n.
- intros f l IH H; rewrite variables_t_unfold in H.
  rewrite Fset.mem_Union in H.
  destruct H as [s [Hs H]].
  rewrite in_map_iff in Hs.
  destruct Hs as [t [Ht Hs]]; subst s.
  refine (Le.le_trans _ _ _ (IH t Hs H) _).
  simpl; do 2 apply le_S.
  apply in_list_size; rewrite in_map_iff.
  exists t; split; trivial.
Qed.

Lemma in_variables_f_size :
  forall x f, x inS variables_f f -> tree_size (tree_of_var x) <= tree_size (tree_of_formula f).
Proof.
intros x f H; induction f; rewrite variables_f_unfold in H.
- rewrite Fset.empty_spec in H; discriminate H.
- rewrite Fset.mem_Union in H.
  destruct H as [s [Hs H]].
  rewrite in_map_iff in Hs.
  destruct Hs as [t [Ht Hs]]; subst s.
  apply le_S_n; refine (Le.le_trans _ _ _ (in_variables_t_size x t H) _).
  apply Lt.lt_le_weak; apply Le.le_n_S; simpl.
  do 2 apply le_S; apply in_list_size; rewrite in_map_iff; exists t; split; trivial.
- simpl; apply le_S.
  rewrite Fset.mem_union, Bool.orb_true_iff in H.
  destruct H as [H | H].
  + refine (Le.le_trans _ _ _ (IHf1 H) _).
    apply Plus.le_plus_l.
  + refine (Le.le_trans _ _ _ (IHf2 H) _).
    rewrite <- plus_n_O; apply Plus.le_plus_r.
- simpl; apply le_S.
  rewrite <- plus_n_O; apply (IHf H).
- simpl; rewrite <- plus_n_O; apply le_S.
  rewrite Fset.add_spec, Bool.orb_true_iff in H.
  destruct H as [H | H].
  + rewrite Oset.eq_bool_true_iff in H; subst v.
    apply Plus.le_plus_l.
  + refine (Le.le_trans _ _ _ (IHf H) _).
    apply Plus.le_plus_r.
Qed.

Lemma free_variables_f_variables_f :
  forall f, free_variables_f f subS variables_f f.
Proof.
intro f; rewrite Fset.subset_spec; induction f; 
rewrite free_variables_f_unfold, variables_f_unfold; trivial.
- intro x; rewrite 2 Fset.mem_union, 2 Bool.orb_true_iff.
  intros [H | H]; [left; apply IHf1 | right; apply IHf2]; assumption.
- intros x Hx; rewrite Fset.mem_filter, Bool.andb_true_iff in Hx.
  rewrite Fset.add_spec, (IHf _ (proj1 Hx)), Bool.orb_true_r.
  apply refl_equal.
Qed.

End Var.

Section Interp.

Hypothesis dom_value : Type.
Hypothesis ODVal : Oeset.Rcd dom_value.
Hypothesis value : Type.
Hypothesis interp_pred : predicate -> list value -> bool.
Hypothesis interp_term : (variable -> dom_value) -> term -> value.
Hypothesis default_value : dom -> dom_value.

Notation "s1 '=PE=' s2" := (forall x, Oeset.mem_bool ODVal x s1 = Oeset.mem_bool ODVal x s2) (at level 70, no associativity).
Notation "a 'inE' s" := (Oeset.mem_bool ODVal a s = true) (at level 70, no associativity).

Definition interp_conj a := 
  match a with 
    | And_F => andb 
    | Or_F => orb 
  end.

Definition interp_quant qf :=
  match qf with
      Forall_F => forallb
    | Exists_F => existsb
  end.

Definition empty_ivar : variable -> dom_value :=
  fun x => match x with Vrbl s _ => default_value s end.

(** [ivar_xt ivar x t] is a valuation which behaves as [ivar], except on [x], which is interpreted as [t]. *)
Definition ivar_xt (ivar : variable -> dom_value) (x : variable) (v : dom_value) :=
  (fun y => match Oset.compare OVR x y with Eq => v | _ => ivar y end).

Lemma interp_quant_eq :
  forall q i1 i2 s1 s2, 
    s1 =PE= s2 -> 
    (forall x1 x2, x1 inE s1 -> Oeset.compare ODVal x1 x2 = Eq -> i1 x1 = i2 x2) ->
    interp_quant q i1 s1 = interp_quant q i2 s2.
Proof.
intros q i1 i2 s1 s2 Hs Hi; unfold interp_quant; destruct q; rewrite eq_bool_iff.
- rewrite 2 forallb_forall; split.
  + intros H x Hx.
    assert (Kx : x inE s1).
    {
      rewrite Hs, Oeset.mem_bool_true_iff.
      exists x; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    }
    assert (Jx := Kx).
    rewrite Oeset.mem_bool_true_iff in Jx.
    destruct Jx as [x' [Jx Hx']].
    assert (Jx' : x' inE s1).
    {
      rewrite Oeset.mem_bool_true_iff.
      exists x'; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    }
    rewrite <- (H x' Hx'); apply sym_eq.
    apply (Hi _ _ Jx').
    rewrite Oeset.compare_lt_gt, Jx; apply refl_equal.
  + intros H x Hx.
    assert (Kx : x inE s2).
    {
      rewrite <- Hs, Oeset.mem_bool_true_iff.
      exists x; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    }
    assert (Jx := Kx).
    rewrite Oeset.mem_bool_true_iff in Jx.
    destruct Jx as [x' [Jx Hx']].
    assert (Jx' : x' inE s2).
    {
      rewrite Oeset.mem_bool_true_iff.
      exists x'; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    }
    rewrite <- (H x' Hx').
    apply Hi.
    rewrite Hs; assumption.
    assumption.
- rewrite 2 existsb_exists.
  split.
  + intros [x [Hx Kx]].
    assert (Kx1 : x inE s1).
    {
      rewrite Oeset.mem_bool_true_iff.
      exists x; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    }
    assert (Jx := Kx1).
    rewrite Hs, Oeset.mem_bool_true_iff in Jx.
    destruct Jx as [x' [Jx Hx']].
    exists x'; split; [assumption | ].
    rewrite <- Kx; apply sym_eq; apply Hi; trivial.
  + intros [x [Hx Kx]].
    assert (Kx1 : x inE s1).
    {
      rewrite Hs, Oeset.mem_bool_true_iff.
      exists x; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    }
    assert (Jx := Kx1).
    rewrite Oeset.mem_bool_true_iff in Jx.
    destruct Jx as [x' [Jx Hx']].
    exists x'; split; [assumption | ].
    rewrite <- Kx; apply Hi.
    * rewrite Oeset.mem_bool_true_iff.
      exists x'; split; [ | assumption].
      apply Oeset.compare_eq_refl.
    * rewrite Oeset.compare_lt_gt, Jx; apply refl_equal.
Qed.

Hypothesis interp_term_eq_rec :
  forall i1 i2 v x s,
    (forall t, variables_t FVR t subS 
                           Fset.filter FVR
                           (fun y : variable => negb (Oset.eq_bool OVR v y)) s ->
               interp_term i1 t = interp_term i2 t) ->
    forall t, variables_t FVR t subS s -> 
              interp_term (ivar_xt i1 v x) t = interp_term (ivar_xt i2 v x) t.

Section Sec.
(** [I] is an "instance", that is a function which associates a set of values to each [dom]. *)
Hypothesis I : dom -> list dom_value.

Fixpoint interp_formula ivar (f : formula) : bool :=
  match f with
  | TTrue => true
  | Atom p l => interp_pred p (List.map (interp_term ivar) l)
  | Conj a f1 f2 => 
    (interp_conj a)
      (interp_formula ivar f1) (interp_formula ivar f2)
  | Not f1 => negb (interp_formula ivar f1)
  | Quant q (Vrbl d1 n1 as x1) f1 =>
    (** the domain [d1] occuring inside variable [x1] = [Vrbl d1 n1] indicates the scope of the universal quantifier. *)
    (interp_quant q) _
      (fun t => interp_formula (ivar_xt ivar x1 t) f1) 
       (I d1)
  end.

Lemma interp_formula_unfold :
  forall ivar f, interp_formula ivar f =
  match f with
  | TTrue => true
  | Atom p l => interp_pred p (List.map (interp_term ivar) l)
  | Conj a f1 f2 => 
    (interp_conj a)
      (interp_formula ivar f1) (interp_formula ivar f2)
  | Not f1 => negb (interp_formula ivar f1)
  | Quant q (Vrbl d1 n1 as x1) f1 =>
    (interp_quant q) _
      (fun t => interp_formula (ivar_xt ivar x1 t) f1) 
       (I d1)
  end.
Proof.
intros ivar f; case f; intros; apply refl_equal.
Qed.

Lemma interp_formula_eq :
  forall f i1 i2,
    (forall t, Fset.subset _ (variables_t FVR t) (free_variables_f f) = true -> 
               interp_term i1 t = interp_term i2 t) ->
    interp_formula i1 f = interp_formula i2 f.
Proof.
intro f; induction f; intros i1 i2 Hi; simpl.
- apply refl_equal.
- apply f_equal; rewrite <- map_eq.
  intros t Ht; apply Hi.
  simpl free_variables_f.
  rewrite Fset.subset_spec; intros x Hx.
  rewrite Fset.mem_Union.
  exists (variables_t FVR t); split; [ | assumption].
  rewrite in_map_iff; exists t; split; trivial.
- apply f_equal2.
  + apply IHf1.
    intros t Ht; apply Hi.
    rewrite Fset.subset_spec in Ht.
    rewrite Fset.subset_spec; intros x Hx.
    rewrite free_variables_f_unfold, Fset.mem_union.
    rewrite (Ht _ Hx).
    apply refl_equal.
  + apply IHf2.
    intros t Ht; apply Hi.
    rewrite Fset.subset_spec in Ht.
    rewrite Fset.subset_spec; intros x Hx.
    rewrite free_variables_f_unfold, Fset.mem_union.
    rewrite (Ht _ Hx), Bool.orb_true_r.
    apply refl_equal.
- apply f_equal; apply IHf; apply Hi.
- destruct v as [dv nv].
  case q; unfold interp_quant.
  + apply forallb_eq.
    intros x Hx; apply IHf.
    intros t Ht.
    revert Ht; apply interp_term_eq_rec.
    apply Hi.
  + rewrite eq_bool_iff, 2 existsb_exists; split.
    * intros [x [Hx Kx]]; exists x; split; [assumption | ].
      rewrite <- Kx; apply IHf.
      apply interp_term_eq_rec.
      intros t Ht; apply sym_eq; apply Hi; assumption.
    * intros [x [Hx Kx]]; exists x; split; [assumption | ].
      rewrite <- Kx; apply IHf.
      apply interp_term_eq_rec; apply Hi.
Qed.

End Sec.

Hypothesis interp_term_eq :
  forall i1 i2 t, 
    (forall x, x inS variables_t FVR t -> Oeset.compare ODVal (i1 x) (i2 x) = Eq) -> 
    interp_term i1 t = interp_term i2 t.

Lemma interp_formula_interp_dom_eq : 
  forall f I1 I2 i, (forall d n, Vrbl d n inS (variables_f f) -> I1 d =PE= I2 d) -> 
                    interp_formula I1 i f = interp_formula I2 i f. 
Proof.
intro f; induction f; intros I1 I2 i H; trivial.
- simpl; apply f_equal2; [apply IHf1 | apply IHf2]; 
  intros d m K; apply (H d m);
  rewrite variables_f_unfold, Fset.mem_union, Bool.orb_true_iff;
  [left | right]; assumption.
- simpl; apply f_equal; apply IHf; assumption.
- rewrite 2 (interp_formula_unfold _ _ (Quant _ _ _)).
  destruct v as [d1 v1].
  assert (Hd1 : I1 d1 =PE= I2 d1).
  {
    apply (H d1 v1).
    rewrite variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; left.
    rewrite Oset.eq_bool_true_iff; apply refl_equal.
  }
  destruct q; unfold interp_quant.
  + rewrite eq_bool_iff; rewrite 2 forallb_forall; split.
    * intros K x Hx.
      assert (Kx : x inE (I2 d1)).
      {
        rewrite Oeset.mem_bool_true_iff; exists x; split.
        - apply Oeset.compare_eq_refl.
        - assumption.
      }
      rewrite <- Hd1, Oeset.mem_bool_true_iff in Kx.
      destruct Kx as [x' [Kx Kx']].
      rewrite <- (K x' Kx'); apply sym_eq.
      {
        refine (eq_trans (IHf I1 I2 _ _) _).
        - intros d n J; apply (H d n).
          rewrite variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; right; assumption.
        - apply interp_formula_eq; intros t _; apply interp_term_eq.
          intros y Hy; unfold ivar_xt.
          case (Oset.compare OVR (Vrbl d1 v1) y);
            [apply Oeset.compare_eq_sym; assumption | | ]; apply Oeset.compare_eq_refl.
      } 
    * intros K x Hx.
      assert (Kx : x inE (I1 d1)).
      {
        rewrite Oeset.mem_bool_true_iff; exists x; split.
        - apply Oeset.compare_eq_refl.
        - assumption.
      }
      rewrite Hd1, Oeset.mem_bool_true_iff in Kx.
      destruct Kx as [x' [Kx Kx']].
      rewrite <- (K x' Kx').
      {
        refine (eq_trans (IHf I1 I2 _ _) _).
        - intros d n J; apply (H d n).
          rewrite variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; right; assumption.
        - apply interp_formula_eq; intros t _; apply interp_term_eq.
          intros y Hy; unfold ivar_xt.
          case (Oset.compare OVR (Vrbl d1 v1) y);
            [assumption | | ]; apply Oeset.compare_eq_refl.
      } 
  + rewrite eq_bool_iff; rewrite 2 existsb_exists; split.
    * intros [x [Hx K]].
      assert (Kx : x inE (I1 d1)).
      {
        rewrite Oeset.mem_bool_true_iff; exists x; split.
        - apply Oeset.compare_eq_refl.
        - assumption.
      }
      rewrite Hd1, Oeset.mem_bool_true_iff in Kx.
      destruct Kx as [x' [Kx Kx']].
      exists x'; split; [assumption | ].
      rewrite <- K; apply sym_eq.
      {
        refine (eq_trans (IHf I1 I2 _ _) _).
        - intros d n J; apply (H d n).
          rewrite variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; right; assumption.
        - apply interp_formula_eq; intros t _; apply interp_term_eq.
          intros y Hy; unfold ivar_xt.
          case (Oset.compare OVR (Vrbl d1 v1) y);
            [assumption | | ]; apply Oeset.compare_eq_refl.
      } 
    * intros [x [Hx K]].
      assert (Kx : x inE (I2 d1)).
      {
        rewrite Oeset.mem_bool_true_iff; exists x; split.
        - apply Oeset.compare_eq_refl.
        - assumption.
      }
      rewrite <- Hd1, Oeset.mem_bool_true_iff in Kx.
      destruct Kx as [x' [Kx Kx']].
      exists x'; split; [assumption | ].
      rewrite <- K.
      {
        refine (eq_trans (IHf I1 I2 _ _) _).
        - intros d n J; apply (H d n).
          rewrite variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; right; assumption.
        - apply interp_formula_eq; intros t _; apply interp_term_eq.
          intros y Hy; unfold ivar_xt.
          case (Oset.compare OVR (Vrbl d1 v1) y);
            [apply Oeset.compare_eq_sym; assumption | | ]; apply Oeset.compare_eq_refl.
      } 
Qed.

Lemma quant_swapp_true :
  forall qtf x1 x2 f I i, interp_formula i I (Quant qtf x1 (Quant qtf x2 f)) = true ->
                    interp_formula i I (Quant qtf x2 (Quant qtf x1 f)) = true.
Proof.
intros qtf x1 x2 f I i.
case_eq (Oset.eq_bool OVR x1 x2); intro Hx;
[rewrite Oset.eq_bool_true_iff in Hx; subst x2; exact (fun h => h) | ].
destruct x1 as [d1 n1]; destruct x2 as [d2 n2].
destruct qtf.
- simpl; rewrite 2 forallb_forall; intros H v2 Hv2.
  rewrite forallb_forall; intros v1 Hv1.
  assert (Kv1 := H v1 Hv1).
  rewrite forallb_forall in Kv1.
  assert (K := Kv1 v2 Hv2).
  rewrite <- K.
  apply interp_formula_eq.
  intros t Hy; apply interp_term_eq.
  intros x Kx; unfold ivar_xt.
  case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Jx.
  + rewrite Oset.compare_eq_iff in Jx; subst x.
    rewrite Oset.compare_lt_gt.
    unfold Oset.eq_bool in Hx.
    destruct (Oset.compare OVR (Vrbl d1 n1) (Vrbl d2 n2)); try discriminate Hx; simpl;
    apply Oeset.compare_eq_refl.
  + apply Oeset.compare_eq_refl.
  + apply Oeset.compare_eq_refl.
- simpl; rewrite 2 existsb_exists; intros [x1 [Hx1 H]].
  rewrite existsb_exists in H; destruct H as [x2 [Hx2 H]].
  exists x2; split; trivial.
  rewrite existsb_exists.
  exists x1; split; trivial.
  rewrite <- H.
  apply interp_formula_eq.
  intros t Hy; apply interp_term_eq.
  intros x Kx; unfold ivar_xt.
  case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Jx.
  + rewrite Oset.compare_eq_iff in Jx; subst x.
    rewrite Oset.compare_lt_gt.
    unfold Oset.eq_bool in Hx.
    destruct (Oset.compare OVR (Vrbl d1 n1) (Vrbl d2 n2)); try discriminate Hx; simpl;
    apply Oeset.compare_eq_refl.
  + apply Oeset.compare_eq_refl.
  + apply Oeset.compare_eq_refl.
Qed.

Lemma quant_swapp :
  forall qtf x1 x2 f I i, interp_formula I i (Quant qtf x1 (Quant qtf x2 f)) = 
                    interp_formula I i (Quant qtf x2 (Quant qtf x1 f)).
Proof.
intros qtf x1 x2 f I i.
rewrite eq_bool_iff; split; apply quant_swapp_true.
Qed.

Fixpoint quant_list qtf l f :=
  match l with
    | nil => f
    | x :: lx => Quant qtf x (quant_list qtf lx f)
  end.

Lemma quant_list_unfold :
  forall qtf l f, quant_list qtf l f =   
              match l with
                | nil => f
                | x :: lx => Quant qtf x (quant_list qtf lx f)
              end.
Proof.
intros qtf l f; case l; intros; apply refl_equal.
Qed.

Lemma free_variables_f_quant_list :
  forall qtf l f, free_variables_f (quant_list qtf l f) =S= 
                  Fset.diff _ (free_variables_f f) (Fset.mk_set _ l).
Proof.
intros qtf l f; rewrite Fset.equal_spec; intro x;
induction l as [ | x1 l]; rewrite quant_list_unfold.
- rewrite Fset.mem_diff, Fset.empty_spec, Bool.andb_true_r.
  apply refl_equal.
- rewrite Fset.mem_diff, free_variables_f_unfold, Fset.mem_filter, IHl.
  rewrite Fset.mem_diff, 2 Fset.mem_mk_set, (Oset.mem_bool_unfold _ _ (_ :: _)).
  rewrite <- Bool.andb_assoc; apply f_equal.
  rewrite Bool.negb_orb, Bool.andb_comm; apply f_equal2; [ | apply refl_equal].
  rewrite Oset.compare_lt_gt; unfold Oset.eq_bool.
  destruct (Oset.compare OVR x1 x); trivial.
Qed.

Lemma quant_list_app :
  forall qtf l1 l2 f I i,
    interp_formula I i (quant_list qtf l1 (quant_list qtf l2 f)) =
    interp_formula I i (quant_list qtf (l1 ++ l2) f).
Proof.
intros qtf l1; induction l1 as [ | [d1 n1] l1]; intros l2 f I i; simpl; [apply refl_equal | ].
apply interp_quant_eq.
- intro; apply refl_equal.
- intros x1 x2 Hx1 Hx; rewrite IHl1.
  apply interp_formula_eq; intros; apply interp_term_eq.
  intros; unfold ivar_xt.
  case (Oset.compare OVR (Vrbl d1 n1) x); trivial; apply Oeset.compare_eq_refl.
Qed.

Lemma quant_list_Pcons_true :
  forall qtf l1 x l2 f I i, interp_formula I i (quant_list qtf (x :: l1 ++ l2) f) =
                       interp_formula I i (quant_list qtf (l1 ++ x :: l2) f).
Proof.
intros qtf l1; induction l1 as [ | [d1 n1] l1]; intros [d n] l2 f I i; [apply refl_equal | ].
assert (K := quant_swapp qtf (Vrbl d n) (Vrbl d1 n1) (quant_list qtf (l1 ++ l2) f) I i).
simpl app.
rewrite 3 (quant_list_unfold _ (_ :: _)), K.
simpl; apply interp_quant_eq; [intro; apply refl_equal | ].
intros x1 x2 Hx1 Hx.
assert (IH := IHl1 (Vrbl d n) l2 f I (ivar_xt i (Vrbl d1 n1) x1)).
simpl in IH; rewrite IH.
apply interp_formula_eq; intros; apply interp_term_eq.
intros x Kx; unfold ivar_xt.
case (Oset.compare OVR (Vrbl d1 n1) x); trivial; apply Oeset.compare_eq_refl.
Qed.

Lemma quant_list_permut_true :
  forall qtf l1 l2 f I i, _permut (@eq _) l1 l2 -> 
                      interp_formula I i (quant_list qtf l1 f) = 
                      interp_formula I i (quant_list qtf l2 f).
Proof.
intros qtf l1; induction l1 as [ | x1 l1]; intros l2 f I i H; inversion H; clear H; subst; 
[apply refl_equal | ].
rewrite <- quant_list_Pcons_true.
simpl; destruct b as [d1 n1].
apply interp_quant_eq.
- intro; apply refl_equal.
- intros x1 x2 Hx1 Hx.
  rewrite (IHl1 _ f I (ivar_xt i (Vrbl d1 n1) x1) H4).
  apply interp_formula_eq; intros; apply interp_term_eq.
  intros x Kx; unfold ivar_xt.
  case (Oset.compare OVR (Vrbl d1 n1) x); trivial; apply Oeset.compare_eq_refl.
Qed.

Lemma quant_forall_list_true_iff :
  forall lx f I i, Oset.all_diff_bool OVR lx = true ->
     (interp_formula I i (quant_list Forall_F lx f) = true <->
    (forall ii, (forall d n, In (Vrbl d n) lx -> In (ii (Vrbl d n)) (I d)) ->
                interp_formula 
                  I (fun x => if Oset.mem_bool OVR x lx then ii x else i x) f = true)).
Proof.
intro lx; induction lx as [ | [d1 n1] lx]; intros f I i Hlx; simpl; split.
- exact (fun h _ _ => h).
- intro H; apply (H i); intros; contradiction.
- rewrite forallb_forall; intros H ii Hii.
  rewrite Oset.all_diff_bool_unfold, Bool.andb_true_iff, negb_true_iff in Hlx.
  destruct Hlx as [H1 Hlx].
  assert (H' := H _ (Hii _ _ (or_introl _ (refl_equal _)))).
  rewrite (IHlx f I (ivar_xt i (Vrbl d1 n1) (ii (Vrbl d1 n1)))) in H';
    [ | intros; apply Hlx; right; assumption].
  rewrite <- (H' ii).
  + apply interp_formula_eq; intros t Ht.
    apply interp_term_eq; intros x Hx.
    case_eq (Oset.mem_bool OVR x lx); intro Kx.
    * rewrite Bool.orb_true_r; apply Oeset.compare_eq_refl.
    * rewrite Bool.orb_false_r; unfold ivar_xt, Oset.eq_bool.
      rewrite Oset.compare_lt_gt.
      case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Jx; 
      try (simpl; apply Oeset.compare_eq_refl).
      rewrite Oset.compare_eq_iff in Jx; subst x.
      apply Oeset.compare_eq_refl.
  + intros d n Hn; apply Hii; right; assumption.
- intro H; rewrite forallb_forall; intros v Hv.
  rewrite Oset.all_diff_bool_unfold, Bool.andb_true_iff, negb_true_iff in Hlx.
  destruct Hlx as [H1 Hlx].
  rewrite (IHlx f I (ivar_xt i (Vrbl d1 n1) v)); [ | assumption].
  intros ii Hii.
  rewrite <- (H (ivar_xt ii (Vrbl d1 n1) v)).
  + apply interp_formula_eq; intros t Ht.
    apply interp_term_eq; intros x Hx; unfold ivar_xt.
    case_eq (Oset.mem_bool OVR x lx); intro Kx.
    * rewrite Bool.orb_true_r.
      case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Jx;
      try apply Oeset.compare_eq_refl.
      rewrite Oset.compare_eq_iff in Jx; subst x.
      rewrite H1 in Kx; discriminate Kx.
    * rewrite Bool.orb_false_r; unfold Oset.eq_bool.
      rewrite (Oset.compare_lt_gt OVR x).
      case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Jx; 
      try (simpl; apply Oeset.compare_eq_refl).
  + intros d n Hx.
    unfold ivar_xt.
    case_eq (Oset.compare OVR (Vrbl d1 n1) (Vrbl d n)); intro Kx.
    * rewrite Oset.compare_eq_iff in Kx.
      injection Kx; clear Kx; intros; subst d n; assumption.
    * destruct Hx as [Hx | Hx]; [rewrite Hx, Oset.compare_eq_refl in Kx; discriminate Kx | ].
      apply Hii; assumption.
    * destruct Hx as [Hx | Hx]; [rewrite Hx, Oset.compare_eq_refl in Kx; discriminate Kx | ].
      apply Hii; assumption.
Qed.

Lemma quant_forall_set_true_iff :
  forall sx f I i, 
     (interp_formula I i (quant_list Forall_F (Fset.elements FVR sx) f) = true <->
    (forall ii, (forall d n, In (Vrbl d n) (Fset.elements _ sx) -> In (ii (Vrbl d n)) (I d)) ->
                interp_formula 
                  I (fun x => if Oset.mem_bool OVR x (Fset.elements _ sx) 
                              then ii x 
                              else i x) f = true)).
Proof.
intros sx f I i; apply quant_forall_list_true_iff.
apply Fset.all_diff_bool_elements.
Qed.

Lemma quant_exists_list_true_iff :
  forall lx f I i, 
    Oset.all_diff_bool OVR lx = true ->
    (interp_formula I i (quant_list Exists_F lx f) = true <->
    (exists ii, (forall d n, In (Vrbl d n) lx -> In (ii (Vrbl d n)) (I d)) /\
                interp_formula 
                  I (fun x => if Oset.mem_bool OVR x lx then ii x else i x) f = true)).
Proof.
intro lx; induction lx as [ | [d1 n1] lx]; intros f I i Hlx; split.
- intro H; exists i; split; trivial.
  intros; contradiction.
- intros [ii [Hii H]]; apply H.
- rewrite Oset.all_diff_bool_unfold, Bool.andb_true_iff, negb_true_iff in Hlx.
  destruct Hlx as [H1 Hlx]; simpl;
  rewrite existsb_exists; intros [v [Hv H]].
  rewrite IHlx in H.
  destruct H as [ii [Hii H]].
  exists (ivar_xt ii (Vrbl d1 n1) v); split.
  + unfold ivar_xt; intros d n Hn.
    case_eq (Oset.compare OVR (Vrbl d1 n1) (Vrbl d n)); intro Kx.
    * rewrite Oset.compare_eq_iff in Kx; injection Kx; clear Kx; do 2 intro; subst d1 n1.
      assumption.
    * apply Hii; destruct Hn as [Hn | Hn]; [ | assumption].
      injection Hn; clear Hn; do 2 intro; subst d1 n1; rewrite Oset.compare_eq_refl in Kx;
      discriminate Kx.
    * apply Hii; destruct Hn as [Hn | Hn]; [ | assumption].
      injection Hn; clear Hn; do 2 intro; subst d1 n1; rewrite Oset.compare_eq_refl in Kx;
      discriminate Kx.
  + rewrite <- H. 
    apply interp_formula_eq; intros t Ht.
    apply interp_term_eq; intros x Hx.
    unfold ivar_xt, Oset.eq_bool.
    rewrite Oset.compare_lt_gt.
    case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Kx; 
    try (simpl; apply Oeset.compare_eq_refl).
    rewrite Oset.compare_eq_iff in Kx; subst x; rewrite H1.
    apply Oeset.compare_eq_refl.
  + assumption.
- rewrite Oset.all_diff_bool_unfold, Bool.andb_true_iff, negb_true_iff in Hlx.
  destruct Hlx as [H1 Hlx]; simpl; intros [ii [Hii H]].
  rewrite existsb_exists.
  exists (ii (Vrbl d1 n1)); split; [apply Hii; left; apply refl_equal | ].
  rewrite IHlx; [ | assumption].
  exists ii; split; [intros; apply Hii; right; assumption | ].
  rewrite <- H.
  apply interp_formula_eq; intros t Ht.
  apply interp_term_eq; intros x Hx; unfold ivar_xt.
  case_eq (Oset.mem_bool OVR x lx); intro Kx.
  + rewrite Bool.orb_true_r; apply Oeset.compare_eq_refl.
  + rewrite Bool.orb_false_r; unfold Oset.eq_bool.
    rewrite (Oset.compare_lt_gt _ x);
      case_eq (Oset.compare OVR (Vrbl d1 n1) x); intro Jx;
      try apply Oeset.compare_eq_refl.
      rewrite Oset.compare_eq_iff in Jx; subst x; simpl.
      apply Oeset.compare_eq_refl.
Qed.

Lemma quant_exists_set_true_iff :
  forall sx f I i, 
    (interp_formula I i (quant_list Exists_F (Fset.elements FVR sx) f) = true <->
     (exists ii, (forall d n, In (Vrbl d n) (Fset.elements _ sx) -> In (ii (Vrbl d n)) (I d)) /\
                 interp_formula 
                   I (fun x => if Oset.mem_bool OVR x (Fset.elements _ sx) 
                               then ii x 
                               else i x) f = true)).
Proof.
intros sx f I i; apply quant_exists_list_true_iff.
apply Fset.all_diff_bool_elements.
Qed.

Lemma conj_swapp :
  forall a f1 f2 I i, interp_formula I i (Conj a f1 f2) = interp_formula I i (Conj a f2 f1).
Proof.
intros a f1 f2 I i; destruct a; simpl.
- apply Bool.andb_comm.
- apply Bool.orb_comm.
Qed.

Fixpoint conj_list a lf :=
  match lf with
    | nil => match a with And_F => TTrue | Or_F => Not TTrue end
    | f1 :: lf => Conj a f1 (conj_list a lf)
  end.

Lemma conj_list_unfold :
  forall a lf, conj_list a lf =
               match lf with
                 | nil => match a with And_F => TTrue | Or_F => Not TTrue end
                 | f1 :: lf => Conj a f1 (conj_list a lf)
               end.
Proof.
intros a lf; case lf; intros; apply refl_equal.
Qed.

Lemma variables_f_conj_list :
  forall a lf, variables_f (conj_list a lf) = Fset.Union _ (List.map (variables_f) lf).
Proof.
intros a lf; induction lf as [ | f1 lf].
- destruct a; rewrite conj_list_unfold; apply refl_equal.
- rewrite conj_list_unfold, variables_f_unfold, map_unfold, Fset.Union_unfold.
  apply f_equal; apply IHlf.
Qed.

Lemma free_variables_f_conj_list :
  forall a lf, free_variables_f (conj_list a lf) = Fset.Union _ (List.map (free_variables_f) lf).
Proof.
intros a lf; induction lf as [ | f1 lf].
- destruct a; rewrite conj_list_unfold; apply refl_equal.
- rewrite conj_list_unfold, free_variables_f_unfold, map_unfold, Fset.Union_unfold.
  apply f_equal; apply IHlf.
Qed.

Lemma conj_list_app :
  forall a lf1 lf2 I i,
    interp_formula I i (Conj a (conj_list a lf1) (conj_list a lf2)) =
    interp_formula I i (conj_list a (lf1 ++ lf2)).
Proof.
intros a l1; induction l1 as [ | a1 l1]; intros l2 I i; [destruct a; trivial | ].
assert (IH := IHl1 l2 I i); destruct a; simpl in IH; simpl.
- rewrite <- Bool.andb_assoc; apply f_equal; apply IH.
- rewrite <- Bool.orb_assoc; apply f_equal; apply IH.
Qed.

Lemma conj_list_Pcons_true :
  forall a lf1 f lf2 I i, interp_formula I i (conj_list a (f :: lf1 ++ lf2)) =
                       interp_formula I i (conj_list a (lf1 ++ f :: lf2)).
Proof.
intros a l1; induction l1 as [ | a1 l1]; intros f l2 I i; [apply refl_equal | ].
assert (K := conj_swapp a f (Conj a a1 (conj_list a (l1 ++ l2))) I i).
simpl app.
rewrite 3 (conj_list_unfold _ (_ :: _)), K.
destruct a; simpl in IHl1; simpl.
- rewrite <- Bool.andb_assoc; apply f_equal; rewrite <- IHl1; apply Bool.andb_comm.
- rewrite <- Bool.orb_assoc; apply f_equal; rewrite <- IHl1; apply Bool.orb_comm.
Qed.

Lemma conj_list_permut_true :
  forall a lf1 lf2 I i, _permut (@eq _) lf1 lf2 -> 
                      interp_formula I i (conj_list a lf1) = 
                      interp_formula I i (conj_list a lf2).
Proof.
intros a l1; induction l1 as [ | a1 l1]; intros l2 I i H; inversion H; clear H; subst; 
[apply refl_equal | ].
rewrite <- conj_list_Pcons_true.
simpl; apply f_equal; apply IHl1; assumption.
Qed.

Lemma conj_and_list_true_iff :
  forall lf I i, interp_formula I i (conj_list And_F lf) = true <->
                 (forall f, In f lf -> interp_formula I i f = true).
Proof.
intro lf; induction lf as [ | f1 lf]; intros I i; simpl; split.
- intros _ f Abs; contradiction Abs.
- intros _; apply refl_equal.
- rewrite Bool.andb_true_iff; intros [H1 Hl] f [H | H].
  + subst f1; apply H1.
  + rewrite IHlf in Hl; apply Hl; assumption.
- intro H; rewrite (H f1); [ | left; apply refl_equal].
  rewrite Bool.andb_true_l, IHlf; intros; apply H; right; assumption.
Qed.

Lemma conj_or_list_true_iff :
  forall lf I i, interp_formula I i (conj_list Or_F lf) = true <->
                 (exists f, In f lf /\ interp_formula I i f = true).
Proof.
intro lf; induction lf as [ | f1 lf]; intros I i; simpl; split.
- intros Abs; discriminate Abs.
- intros [f [Abs _]]; contradiction Abs.
- rewrite Bool.orb_true_iff; intros [H1 | Hl].
  + exists f1; split; [left | ]; trivial.
  + rewrite IHlf in Hl.
    destruct Hl as [f [Hf Hl]]; exists f; split; [right | ]; trivial.
- intros [f [[H1 | Hl] H]].
  + subst f; rewrite H; apply refl_equal.
  + rewrite Bool.orb_true_iff; right; rewrite IHlf.
    exists f; split; trivial.
Qed.

End Interp.

End Compare.

End Sec.
