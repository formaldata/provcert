(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Bool List Sorted SetoidList Arith NArith.

Require Import Facts BasicFacts ListFacts ListPermut ListSort OrderedSet FiniteSet FiniteBag SemiRing.

Record quotient_function elt1 elt2 Elt1 := 
  mk_qf { f : elt1 -> elt2;
    equiv_1_eq_2 : forall e1 e2, Oeset.eq_bool Elt1 e1 e2 = true -> f e1 = f e2 }.

Lemma find_spec : forall elt1 elt2 Elt1 (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)) (x : elt1),
    x inSE fst s <->
    (exists y : elt2, (if x inSE? fst s then Some (f (snd s) x) else None) = Some y).
Proof.
  split; intros.
  - rewrite H. exists (f (snd s) x); reflexivity.
  - destruct H.
    apply not_false_iff_true; intro. rewrite H0 in H. inversion H.                        
Qed.

Lemma equiv_eq : forall elt1 elt2 Elt1 (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)) (x x' : elt1),
  Oeset.eq_bool Elt1 x x' = true ->
  (if x inSE? fst s then Some (f (snd s) x) else None) =
  (if x' inSE? fst s then Some (f (snd s) x') else None).
Proof.
  intros.
  rewrite (equiv_1_eq_2 (snd s) _ _ H).
  apply Oeset.eq_bool_true_compare_eq in H.
  now rewrite (Feset.mem_eq_1 _ _ _ _ H).
Qed.

Lemma singl_spec : forall elt1 elt2 Elt1 (x1 x2 : elt1) (y1 : elt2),
  Oeset.eq_bool Elt1 x1 x2 = true ->
  (if
    x2 inSE? fst (Feset.singleton (Feset.build Elt1) x1, fun _ : elt1 => y1)
   then
    Some (snd (Feset.singleton (Feset.build Elt1) x1, fun _ : elt1 => y1) x2)
   else None) = Some y1.
Proof.
  intros.
  unfold fst. rewrite Feset.singleton_spec.
  rewrite Oeset.eq_bool_sym.
  rewrite H. reflexivity.
Qed.

Lemma support_singl : forall elt1 elt2 Elt1 (x1 : elt1) (y1 : elt2),
  fst (Feset.singleton (Feset.build Elt1) x1, fun _ : elt1 => y1) =
  Feset.singleton (Feset.build Elt1) x1.
Proof.
  reflexivity.
Qed.
  
Lemma support_add : forall elt1 elt2 Elt1 (x : elt1) (y : elt2)
    (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  fst
    (x addSE fst s,
    fun e : elt1 => if Oeset.eq_bool Elt1 e x then y else f (snd s) e) =
  (x addSE fst s).
Proof.
  reflexivity.
Qed.
  
 
Lemma spec_add : forall elt1 elt2 Elt1(x x' : elt1) (y : elt2)
    (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  x'
  inSE fst
         (x addSE fst s,
         fun e : elt1 => if Oeset.eq_bool Elt1 e x then y else f (snd s) e) ->
  Oeset.eq_bool Elt1 x x' = false ->
  (if
    x'
    inSE? fst
            (x addSE fst s,
            fun e : elt1 => if Oeset.eq_bool Elt1 e x then y else f (snd s) e)
   then
    Some
      (snd
         (x addSE fst s,
         fun e : elt1 => if Oeset.eq_bool Elt1 e x then y else f (snd s) e) x')
    else None) = (if x' inSE? fst s then Some (f (snd s) x') else None).
Proof.
  unfold fst; intros.
  destruct s.
  rewrite H.
  rewrite Feset.add_spec in H.
  rewrite Oeset.eq_bool_sym in H0.
  rewrite H0 in H. rewrite orb_false_l in H.
  rewrite H.
  unfold snd.
  rewrite H0.
  reflexivity.
Qed.

Lemma spec_add_2 : forall elt1 elt2 Elt1 (x : elt1) (y : elt2)
    (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  (if
    x
    inSE? fst
            (x addSE fst s,
            fun e : elt1 => if Oeset.eq_bool Elt1 e x then y else f (snd s) e)
   then
    Some
      (snd
         (x addSE fst s,
         fun e : elt1 => if Oeset.eq_bool Elt1 e x then y else f (snd s) e) x)
    else None) = Some y.
Proof.
  intros.
  unfold fst. unfold snd.
  rewrite Feset.add_spec.
  rewrite Oeset.eq_bool_refl.
  reflexivity.
Qed.

Definition fold_map elt1 (Elt1 : Oeset.Rcd elt1) elt2 :=
  (fun (A : Type) (f : elt1 -> A -> A) (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)) (a0 : A) => Feset.fold _ (fun (e: elt1) (a : A) => f e a) (fst s) a0).


Lemma feset_fold_refl : forall elt1 elt2 Elt1 (s : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)) 
    (A : Type) (a0 : A) (f : elt1 -> A -> A),
  Feset.fold (Feset.build Elt1) (fun (e : elt1) (a : A) => f e a) (fst s) a0 =
  Feset.fold (Feset.build Elt1) (fun (e : elt1) (a : A) => f e a) (fst s) a0.
Proof.
  reflexivity.
Qed.

Fixpoint compare_elt2 elt1 elt2 (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) (f1 f2 : elt1 -> elt2) (l : list elt1) :=
  match l with
  |nil => Eq
  |a::l' => if forallb (fun x => Oeset.eq_bool Elt2 (f1 x) (f2 x)) l'
            then Oeset.compare Elt2 (f1 a) (f2 a)
            else compare_elt2 Elt1 Elt2 f1 f2 l'
  end.


Lemma sorted_lt_mem_lt : forall elt1 (Elt1 : Oeset.Rcd elt1) a l2,
    Sorted (fun x y : elt1 => lt Elt1 x y) (a :: l2) ->
    forall x, Oeset.mem_bool Elt1 x l2 = true -> lt Elt1 a x.
Proof.
  intros.
  apply Sorted_extends in H.
  apply Oeset.mem_bool_true_iff in H0 as [x' [H0 H1]].
  apply (Oeset.compare_lt_eq_trans _ a x' x); trivial.
  now apply (proj1 (Forall_forall _ _)) with x' in H.
  apply Oeset.compare_eq_sym; trivial.
  unfold Relations_1.Transitive.
  apply Oeset.compare_lt_trans.
Qed.

Lemma feset_elements_mem_lt : forall elt1 (Elt1 : Oeset.Rcd elt1) a1 l1 a l2, Feset.elements (Feset.build Elt1) a1 = l1++ a::l2 -> forall x, Oeset.mem_bool Elt1 x l2 = true -> lt Elt1 a x.
Proof.
  intros elt1 Elt1 a1 l1 a l2 H.
  set (Feset.elements_spec3 (Feset.build Elt1) a1).
  rewrite H in s.
  clear H.
  induction l1.
  apply sorted_lt_mem_lt. trivial.
  simpl in s.
  apply Sorted_inv in s as [H _].
  now apply IHl1.
Qed.


Lemma compare_elt1_equiv : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) l l' (m1 m2 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
    (forall (l1 : list elt1) (a : elt1) (l2 : list elt1),
        l = l1 ++ a :: l2 -> forall x : elt1, Oeset.mem_bool Elt1 x l2 = true -> lt Elt1 a x) ->
    (forall (l1 : list elt1) (a : elt1) (l2 : list elt1),
        l' = l1 ++ a :: l2 -> forall x : elt1, Oeset.mem_bool Elt1 x l2 = true -> lt Elt1 a x) ->
    (forall e : elt1,
       Oeset.mem_bool Elt1 e l =
       Oeset.mem_bool Elt1 e l') ->
    compare_elt2 Elt1 Elt2 (f (snd m1)) (f (snd m2)) l =
    compare_elt2 Elt1 Elt2 (f (snd m1)) (f (snd m2)) l'.
Proof.
  induction l; intros.
  - rewrite (nb_occ_nil Elt1 l'); trivial.
    intros.
    apply Oeset.not_mem_nb_occ. now rewrite <- (H1 x).
  - assert ( Oeset.mem_bool Elt1 a l' = true).
      * rewrite <- H1; simpl. now rewrite Oeset.eq_bool_refl.
      * apply Oeset.mem_bool_true_iff in H2 as [a0 [H2 H4]].
        apply in_split in H4 as [l1' [l2' H4]].
        rewrite H4.
        assert (l1' = nil).
        -- rewrite H4 in H1.
           destruct l1'; trivial.
           specialize (H1 e). simpl in H1. rewrite Oeset.eq_bool_refl in H1.
           assert (Oeset.mem_bool Elt1 a0 (l1' ++ a0 :: l2') = true).
           rewrite Oeset.mem_bool_app; simpl. rewrite Oeset.eq_bool_refl; simpl; apply orb_true_r.
           specialize (H0 nil e (l1'++ a0::l2') H4 a0 H3).
           apply Oeset.compare_eq_sym in H2.
           apply (Oeset.compare_lt_eq_trans _ _ _ _ H0) in H2.
           specialize (H nil a l).
           assert (Oeset.eq_bool Elt1 e a = false).
           apply not_true_iff_false. intro.
           apply Oeset.eq_bool_true_compare_eq in H5.
           rewrite H5 in H2. inversion H2.
           rewrite H5 in H1.
           rewrite orb_true_l in H1.
           rewrite orb_false_l in H1.
           rewrite Oeset.compare_lt_gt in H2.
           rewrite H in H2; trivial. inversion H2.
        -- rewrite H3. rewrite H3 in H4. clear H3.
           case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (f (snd m1) x) (f (snd m2) x)) l); intros.
           ** 
              simpl.
              rewrite 2 if_eq_true; trivial.
              --- apply Oeset.eq_bool_true_compare_eq in H2.
                  now rewrite 2 (equiv_1_eq_2 _ _ _ H2).
              --- apply forallb_forall.
                  intros.
                  apply (Oeset.in_mem_bool Elt1) in H5.
                  specialize (H1 x).
                  rewrite H4 in H1.
                  simpl in H1. rewrite H5 in H1.
                  apply (H0 _ _ _) with x in H4.
                  assert (Oeset.mem_bool Elt1 x l = true).
                  apply not_false_iff_true; intro.
                  rewrite H6 in H1.
                  clear H6.
                  rewrite orb_false_r in H1.
                  rewrite orb_true_r in H1.
                  apply Oeset.eq_bool_true_compare_eq in H1.
                  apply Oeset.compare_eq_sym in H1.
                  rewrite (Oeset.compare_eq_lt_trans _ _ _ _ H2 H4) in H1.
                  inversion H1.
                  apply Oeset.mem_bool_true_iff in H6 as [a' [H6 H8]].
                  apply (forallb_forall _ _) with a' in H3.
                  apply Oeset.eq_bool_true_compare_eq in H6.
                  now rewrite 2 (equiv_1_eq_2 _ _ _ H6).
                  trivial.
                  trivial.
      ** simpl. rewrite 2 if_eq_false; trivial.
        --- apply IHl.
            *** intros.
                specialize (H (a::l1) a1 l2).
                apply H.
                rewrite H5; trivial.
                trivial.
            *** intros.
                rewrite H5 in H4.
                now apply (H0 (a0::l1) a1 l2 H4).
            *** rewrite H4 in H1.
                apply Oeset.eq_bool_true_compare_eq in H2.
                intros.
                specialize (H1 e).
                case_eq (Oeset.eq_bool Elt1 e a); intros.
                assert (Oeset.mem_bool Elt1 e l2' = false).
                specialize (H0 _ _ _ H4 e).
                apply not_true_iff_false.
                intro.
                apply H0 in H6.
                apply Oeset.eq_bool_true_compare_eq in H2.
                apply (Oeset.compare_eq_lt_trans _ _ _ _ H2) in H6.
                apply Oeset.eq_bool_true_compare_eq in H5.
                apply Oeset.compare_eq_sym in H5.
                rewrite H5 in H6; inversion H6.
                rewrite H6.
                specialize (H nil a l).
                apply not_true_iff_false.
                intro.
                apply H in H7.
                apply Oeset.eq_bool_true_compare_eq in H5.
                apply Oeset.compare_eq_sym in H5.
                rewrite H5 in H7; inversion H7.
                trivial.
                simpl in H1.
                rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H2) in H1.
                rewrite H5 in H1.
                now rewrite 2 orb_false_l in H1.
        --- apply not_true_iff_false.
            intro.
            apply not_true_iff_false in H3.
            destruct H3.
            apply forallb_forall.
            intros.
            apply (Oeset.in_mem_bool Elt1) in H3.
            specialize (H1 x).
            rewrite H4 in H1.
            simpl in H1. rewrite H3 in H1.
            apply Oeset.eq_bool_true_compare_eq in H2.
            rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H2) in H1.
            rewrite orb_true_r in H1.
            symmetry in H1.
            specialize (H nil a l (eq_refl _) x H3).
            assert (Oeset.eq_bool Elt1 x a = false).
            rewrite Oeset.compare_lt_gt in H.
            apply CompOpp_iff in H.
            apply not_true_iff_false; intro.
            simpl in H.
            apply Oeset.eq_bool_true_compare_eq in H6.
            rewrite H in H6.
            inversion H6.
            rewrite H6 in H1.
            rewrite orb_false_l in H1.
            *** apply Oeset.mem_bool_true_iff in H1 as [a' [H1 H7]].
                apply (forallb_forall _ _) with a' in H5; trivial.
                apply Oeset.eq_bool_true_compare_eq in H1.
                now rewrite 2 (equiv_1_eq_2 _ _ _ H1).
Qed.



Lemma compare_elt2_comm : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) a1 a2 (m1 m2 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
    Feset.compare (Feset.build Elt1) a1 a2 = Eq ->
    compare_elt2 Elt1 Elt2 (f (snd m1)) (f (snd m2)) (Feset.elements (Feset.build Elt1) a1) = compare_elt2 Elt1 Elt2 (f (snd m1)) (f (snd m2)) (Feset.elements (Feset.build Elt1) a2).
Proof.
  intros.
  apply compare_elt1_equiv; try apply feset_elements_mem_lt.
  apply Feset.compare_spec in H.
  intros.
  rewrite <- 2 Feset.mem_elements.
  apply (Feset.mem_eq_2 _ _ _ H).
Qed.

Definition compareb elt1 elt2 (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2)
           (m1 m2 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) :=
  match Feset.compare (Feset.build Elt1) (fst m1) (fst m2) with
  |Eq => compare_elt2 Elt1 Elt2 (f (snd m1)) (f (snd m2)) (Feset.elements (Feset.build Elt1) (fst m1))
  |x => x
  end.

Lemma compareb_spec : forall elt1 elt2 Elt1 Elt2 (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compareb Elt2 s1 s2 = Eq <->
  (fst s1 =SE?= fst s2) &&
  Feset.for_all (Feset.build Elt1)
    (fun s : elt1 => Oeset.eq_bool Elt2 (f (snd s1) s) (f (snd s2) s))
    (fst s1) = true.
Proof.
  split; intros.
  - apply FSetPositive.PositiveSet.lex_Eq in H as [H0 H1].
    rewrite (proj1 (Feset.compare_spec _ _ _) H0).
    rewrite Feset.for_all_spec.
    rewrite andb_true_l.
    induction (Feset.elements (Feset.build Elt1) (fst s1)); trivial.
    simpl.
    simpl in H1.
    case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (f (snd s1) x) (f (snd s2) x)) l); intros.
    rewrite if_eq_true in H1; trivial.
    apply Oeset.eq_bool_true_compare_eq in H1. now rewrite H1.
    rewrite if_eq_false in H1; trivial.
    apply IHl in H1. rewrite H1 in H. inversion H.
  - apply andb_prop in H as [H0 H1].
    unfold compareb.
    rewrite (proj2 (Feset.compare_spec _ _ _) H0).
    rewrite Feset.for_all_spec in H1.
    induction (Feset.elements (Feset.build Elt1) (fst s1)); trivial.
    simpl.
    simpl in H1.
    apply andb_prop in H1 as [H1 H2].
    rewrite H2.
    now apply Oeset.eq_bool_true_compare_eq.
    
Qed.

Lemma compare_elt2_spec_lt : forall elt1 elt2 (Elt1: Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) a1 (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_elt2 Elt1 Elt2 (f (snd s1)) (f (snd s2)) (Feset.elements (Feset.build Elt1) a1) = Lt <->
  exists (l1 : list elt1) (a : elt1) (l2 : list elt1),
    Feset.elements (Feset.build Elt1) a1 = l1 ++ a :: l2 /\
    lt Elt2 (f (snd s1) a) (f (snd s2) a) /\ forallb (fun x : elt1 => Oeset.eq_bool Elt2 (f (snd s1) x) (f (snd s2) x)) l2 = true.
Proof.
  split; intros.
  induction (Feset.elements (Feset.build Elt1) a1).
  inversion H.
  simpl in H.
  case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (f (snd s1) x) (f (snd s2) x)) l); intros.
  exists nil. exists a. exists l. repeat split; trivial.
  rewrite if_eq_true in H; trivial.
  rewrite if_eq_false in H; trivial.
  apply IHl in H as [l1 [a0 [l2 [H1 [H2 H3]]]]].
  exists (a::l1); exists a0; exists l2.
  rewrite H1.
  now repeat split.
  destruct H as [l1 [a [l2 [H1 [H2 H3]]]]].
  rewrite H1.
  clear H1.
  induction l1.
  simpl.
  rewrite if_eq_true; trivial.
  simpl.
  rewrite if_eq_false; trivial.
  rewrite forallb_app.
  simpl.
  assert (Oeset.eq_bool Elt2 (f (snd s1) a) (f (snd s2) a) =false).
  apply not_true_iff_false; intro.
  apply Oeset.eq_bool_true_compare_eq in H.
  rewrite H in H2. inversion H2.
  apply andb_false_iff. right.
  apply andb_false_iff. left. trivial.
Qed.

  
Lemma compareb_spec_lt : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
    compareb Elt2 s1 s2 = Lt <->
    Feset.compare (Feset.build Elt1) (fst s1) (fst s2) = Lt \/
    (Feset.compare (Feset.build Elt1) (fst s1) (fst s2) = Eq /\
    exists l1 a l2, Feset.elements (Feset.build Elt1) (fst s1) = l1 ++ a :: l2 /\
                    Oeset.compare Elt2 (f (snd s1) a) (f (snd s2) a) = Lt /\
                    forallb (fun x => Oeset.eq_bool Elt2 (f (snd s1) x) (f (snd s2) x)) l2 = true).
Proof.
  split; intros.
  - unfold compareb in H.
    case_eq (Feset.compare (Feset.build Elt1) (fst s1) (fst s2)); intros.
    right; repeat split.
    rewrite H0 in H.
    now apply compare_elt2_spec_lt.
    now left.
    rewrite H0 in H. inversion H.
  - unfold compareb. destruct H as [H|[H0 H1]].
    now rewrite H.
    rewrite H0.
    now apply compare_elt2_spec_lt.
Qed.

Lemma compareb_spec_lt2 : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
    compareb Elt2 s1 s2 = Lt <->
    Feset.compare (Feset.build Elt1) (fst s1) (fst s2) = Lt \/
    (Feset.compare (Feset.build Elt1) (fst s1) (fst s2) = Eq /\
     compare_elt2 Elt1 Elt2 (f (snd s1)) (f (snd s2)) (Feset.elements (Feset.build Elt1) (fst s1)) = Lt).
Proof.
  split; intros.
  - unfold compareb in H.
    case_eq (Feset.compare (Feset.build Elt1) (fst s1) (fst s2)); intros.
    right; repeat split.
    now rewrite H0 in H.
    now left.
    rewrite H0 in H. inversion H.
  - unfold compareb. destruct H as [H|[H H1]]; now rewrite H.
Qed.


 Lemma compareb_eq_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (a1 a2 a3 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
  compareb Elt2 a1 a2 = Eq ->
  compareb Elt2 a2 a3 = Eq -> compareb Elt2 a1 a3 = Eq.
 Proof.
   intros.
   apply compareb_spec in H.
   apply compareb_spec in H0.
   apply compareb_spec.
   apply andb_prop in H as [Ha Hb].
   apply andb_prop in H0 as [H0a H0b].
   apply andb_true_iff; split.
   apply Feset.compare_spec in Ha.
   apply Feset.compare_spec in H0a.
   apply Feset.compare_spec.
   apply (Feset.compare_eq_trans _ _ _ _ Ha H0a).
   rewrite Feset.for_all_spec in Hb.
   rewrite Feset.for_all_spec in H0b.
   rewrite Feset.for_all_spec.
   apply forallb_forall.
   intros.
   apply (proj1 (forallb_forall _ _)) with x in Hb; trivial.
   rewrite (Oeset.eq_bool_eq_1 _ _ _ _ Hb).
   apply Feset.in_elements_mem in H.
   rewrite (Feset.mem_eq_2 _ _ _ Ha) in H.
   rewrite Feset.mem_elements in H.
   apply Oeset.mem_bool_true_iff in H as [a [H H']].
   apply Oeset.eq_bool_true_compare_eq in H.
   rewrite 2 (equiv_1_eq_2 _ _ _ H).
   now apply (proj1 (forallb_forall _ _)) with a in H0b.
 Qed.

Lemma compareb_eq_lt_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (a1 a2 a3 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compareb Elt2 a1 a2 = Eq ->
  compareb Elt2 a2 a3 = Lt -> compareb Elt2 a1 a3 = Lt.
Proof.
  intros.
  apply compareb_spec in H.
  apply andb_true_iff in H as [H Ha].
  apply Feset.compare_spec in H.
  apply compareb_spec_lt2.
  apply compareb_spec_lt2 in H0 as [H0|[H0 H1]].
  left.
  apply (Feset.compare_eq_lt_trans _ _ _ _ H H0).
  right; split.
  apply (Feset.compare_eq_trans _ _ _ _ H H0).
  rewrite Feset.for_all_spec in Ha.
  rewrite <- (compare_elt2_comm _ _ _ _ _ H) in H1.
  apply compare_elt2_spec_lt.
  apply compare_elt2_spec_lt in H1 as [l1 [a [l2 [H1 [H2 H3]]]]].
  exists l1. exists a. exists l2.
  repeat split; trivial.
  apply (proj1 (forallb_forall _ _)) with a in Ha.
  apply Oeset.eq_bool_true_compare_eq in Ha.
  apply (Oeset.compare_eq_lt_trans _ _ _ _ Ha H2).
  rewrite H1.
  apply in_or_app. right; left; trivial.
  rewrite H1 in Ha.
  rewrite forallb_app in Ha.
  apply andb_true_iff in Ha as [_ Ha].
  simpl in Ha.
  apply andb_true_iff in Ha as [_ Ha].
  apply forallb_forall; intros.
  apply (proj1 (forallb_forall _ _)) with x in Ha; trivial.
  apply (proj1 (forallb_forall _ _)) with x in H3; trivial.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ Ha).
Qed.

Lemma compareb_lt_eq_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (a1 a2 a3 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compareb Elt2 a1 a2 = Lt ->
  compareb Elt2 a2 a3 = Eq -> compareb Elt2 a1 a3 = Lt.
Proof.
  intros.
  apply compareb_spec in H0.
  apply andb_true_iff in H0 as [H0 Ha].
  apply Feset.compare_spec in H0.
  apply compareb_spec_lt2.
  apply compareb_spec_lt2 in H as [H|[H H1]].
  left.
  apply (Feset.compare_lt_eq_trans _ _ _ _ H H0).
  right; split.
  apply (Feset.compare_eq_trans _ _ _ _ H H0).
  rewrite Feset.for_all_spec in Ha.
  rewrite (compare_elt2_comm _ _ _ _ _ H) in H1.
  rewrite (compare_elt2_comm _ _ _ _ _ H).
  apply compare_elt2_spec_lt.
  apply compare_elt2_spec_lt in H1 as [l1 [a [l2 [H1 [H2 H3]]]]].
  exists l1. exists a. exists l2.
  repeat split; trivial.
  apply (proj1 (forallb_forall _ _)) with a in Ha.
  apply Oeset.eq_bool_true_compare_eq in Ha.
  apply (Oeset.compare_lt_eq_trans _ _ _ _ H2 Ha).
  rewrite H1.
  apply in_or_app. right; left; trivial.
  rewrite H1 in Ha.
  rewrite forallb_app in Ha.
  apply andb_true_iff in Ha as [_ Ha].
  simpl in Ha.
  apply andb_true_iff in Ha as [_ Ha].
  apply forallb_forall; intros.
  apply (proj1 (forallb_forall _ _)) with x in Ha; trivial.
  apply (proj1 (forallb_forall _ _)) with x in H3; trivial.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H3).
Qed.

Lemma compareb_lt_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (a1 a2 a3 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compareb Elt2 a1 a2 = Lt ->
  compareb Elt2 a2 a3 = Lt -> compareb Elt2 a1 a3 = Lt.
Proof.
  intros.
  apply compareb_spec_lt2.
  apply compareb_spec_lt2 in H as [H|[H H1]];
  apply compareb_spec_lt2 in H0 as [H0|[H0 Ha]].
  left.
  apply (Feset.compare_lt_trans _ _ _ _ H H0).
  left.
  apply (Feset.compare_lt_eq_trans _ _ _ _ H H0).
  left.
  apply (Feset.compare_eq_lt_trans _ _ _ _ H H0).
  right; split.
  apply (Feset.compare_eq_trans _ _ _ _ H H0).
  rewrite <- (compare_elt2_comm _ _ _ _ _ H) in Ha.
  apply compare_elt2_spec_lt.
  apply compare_elt2_spec_lt in H1 as [l1 [a [l2 [H1 [H2 H3]]]]].
  apply compare_elt2_spec_lt in Ha as [l1' [a' [l2' [H1' [H2' H3']]]]].
  assert ((l1 = l1' /\ a = a'/\ l2 = l2') \/ ((exists l1'', l1 = l1'++a'::l1'') /\ exists l2'', l2'' ++ a :: l2 = l2')
          \/ ((exists l1'', l1' = l1++a::l1'') /\ exists l2'', l2'' ++ a' :: l2' = l2)) as [H4|[H4|H4]].
  rewrite H1 in H1'.
  clear H1.
  revert H1'; revert l1'.
  induction l1; intros; destruct l1'; inversion H1'.
  - left; now repeat split.
  - right; right.
  split; exists l1'; trivial.
  - right; left.
  split; exists l1; trivial.
  - specialize (IHl1 _ H5) as [H6|[[[l1'' H6] [l2'' H7]]|[[l1'' H6] [l2'' H7]]]].
    * left; split.
    now rewrite (proj1 H6).
    apply H6.
    * right; left.
    rewrite H6. rewrite <- H7.
    split. exists l1''; trivial. exists l2''; trivial.
    * right; right;
    rewrite H6; rewrite <- H7;
    split; [>exists l1''; trivial|exists l2''; trivial].
  - exists l1; exists a; exists l2.
    repeat split; trivial.
    * rewrite <- (proj1 (proj2 H4)) in H2'.
      apply (Oeset.compare_lt_trans _ _ _ _ H2 H2'). 
    * apply forallb_forall; intros.
      apply (proj1 (forallb_forall _ _)) with x in H3'; trivial.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H3').
      apply (proj1 (forallb_forall _ _)) with x in H3; trivial.
      now rewrite <- (proj2 (proj2 H4)).
  - destruct H4 as [[l1'' H4] [l2'' H5]].
    exists l1; exists a; exists l2.
    rewrite <- H5 in H3'.
    repeat split; trivial.
    * apply (proj1 (forallb_forall _ _)) with a in H3'.
      apply Oeset.eq_bool_true_compare_eq in H3'.
      apply (Oeset.compare_lt_eq_trans _ _ _ _ H2 H3').
      apply in_or_app; right; left; trivial.
    * apply forallb_forall; intros.
      apply (proj1 (forallb_forall _ _)) with x in H3'; trivial.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H3').
      apply (proj1 (forallb_forall _ _)) with x in H3; trivial.
      apply in_or_app; right; right; trivial.
  - destruct H4 as [[l1'' H4] [l2'' H5]].
    exists l1'; exists a'; exists l2'.
    rewrite <- H5 in H3.
    repeat split; trivial.
    * apply (proj1 (forallb_forall _ _)) with a' in H3.
      apply Oeset.eq_bool_true_compare_eq in H3.
      apply (Oeset.compare_eq_lt_trans _ _ _ _ H3 H2').
      apply in_or_app; right; left; trivial.
    * apply forallb_forall; intros.
      apply (proj1 (forallb_forall _ _)) with x in H3'; trivial.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H3').
      apply (proj1 (forallb_forall _ _)) with x in H3; trivial.
      apply in_or_app; right; right; trivial.
Qed.

Lemma compareb_lt_gt : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 (a1 a2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compareb Elt2 a1 a2 = CompOpp (compareb Elt2 a2 a1).
Proof.
  intros.
  unfold compareb.
  case_eq (Feset.compare (Feset.build Elt1) (fst a1) (fst a2)); intros;
  rewrite Feset.compare_lt_gt in H;
  apply CompOpp_iff in H; rewrite H; trivial.
  rewrite (compare_elt2_comm _ _ _ _ _ H).
  induction (Feset.elements (Feset.build Elt1) (fst a1)); trivial.
  simpl.
  case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (f (snd a1) x) (f (snd a2) x)) l); intros.
  rewrite 2 if_eq_true; trivial. apply Oeset.compare_lt_gt.
  apply forallb_forall; intros.
  apply (proj1 (forallb_forall _ _)) with x in H0; trivial.
  now rewrite Oeset.eq_bool_sym.
  rewrite 2 if_eq_false; trivial.
  apply not_true_iff_false; intro.
  apply not_true_iff_false in H0; destruct H0.
  apply forallb_forall; intros.
  apply (proj1 (forallb_forall _ _)) with x in H1; trivial.
  now rewrite Oeset.eq_bool_sym.
Qed.


Definition cst_qf elt1 (Elt1 : Oeset.Rcd elt1) elt2 (e2 : elt2) :=
  mk_qf Elt1 (fun (e : elt1) => e2) (fun e0 e1 H => eq_refl e2).

Lemma add_qf_prop : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) e1 (e2 :elt2) (m: Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) (e3 e4 : elt1),
 Oeset.eq_bool Elt1 e3 e4 = true ->
 (fun e : elt1 => if Oeset.eq_bool Elt1 e e1 then e2 else f (snd m) e) e3 =
 (fun e : elt1 => if Oeset.eq_bool Elt1 e e1 then e2 else f (snd m) e) e4.
Proof.
  intros.
  simpl. rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
  now rewrite (equiv_1_eq_2 _ _ _ H).
Qed.

Definition add_qf elt1 (Elt1 : Oeset.Rcd elt1) elt2 e1 (e2 : elt2) (m: Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) :=
  mk_qf Elt1 (fun e => if Oeset.eq_bool Elt1 e e1 then e2 else f (snd m) e) (add_qf_prop e1 e2 m).


Definition empty_qf elt1 (Elt1 : Oeset.Rcd elt1) elt2 (zero : elt2) :=
  mk_qf Elt1 (fun (e : elt1) => zero) (fun e0 e1 H => eq_refl _).


Definition coeff elt1 (Elt1 : Oeset.Rcd elt1) elt2 zero x (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) :=
  if Feset.mem (Feset.build Elt1) x (fst s) then f (snd s) x
  else zero.

Lemma coeff_spec1 : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 zero  (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1)
    (x : elt1) (y : elt2),
  (if x inSE? fst s then Some (f (snd s) x) else None) = Some y ->
  coeff zero x s = y.
Proof.
  intros. unfold coeff.
  destruct (x inSE? fst s); now inversion H.
Qed.


Lemma coeff_spec2 : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 zero (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1)
    (x : elt1),
  (if x inSE? fst s then Some (f (snd s) x) else None) = None ->
  coeff zero x s = zero.
Proof.
  intros.
  unfold coeff.
  destruct (x inSE? fst s); now inversion H.
Qed.


Lemma coeff_eq_fmap : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 zero x1 x2 (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
    eq Elt1 x1 x2 -> coeff zero x1 s = coeff zero x2 s.
Proof.
  intros.
  unfold coeff.
  rewrite (Feset.mem_eq_1 _ _ _ _ H).
  destruct (x2 inSE? fst s); trivial.
  apply equiv_1_eq_2.
  now apply Oeset.eq_bool_true_compare_eq.
Qed.

Definition equal elt1 (Elt1 : Oeset.Rcd elt1) elt2 (Elt2 : Oeset.Rcd elt2) zero (s s' : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) :=
  Feset.for_all (Feset.build Elt1) (fun e => Oeset.eq_bool Elt2 (coeff zero e s) (coeff zero e s')) (Feset.union (Feset.build Elt1) (fst s) (fst s')).

Lemma equal_spec : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 Elt2 zero (m1 m2 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
    equal Elt2 zero m1 m2 = true <->
    (forall x : elt1,
        Oeset.eq_bool Elt2 (coeff zero x m1) (coeff zero x m2) = true).
Proof.
  split; intros.
  - unfold equal in H.
    case_eq (x inSE? (Feset.union _ (fst m1) (fst m2))); intros.
    rewrite Feset.for_all_spec in H. 
    rewrite Feset.mem_elements in H0.
    apply Oeset.mem_bool_true_iff in H0 as [a [Ha Hb]].
    rewrite 2 (coeff_eq_fmap _ _ _ _ Ha).
    now apply (proj1 (forallb_forall _ _)) with a in H.
    rewrite Feset.mem_union in H0.
    apply orb_false_elim in H0 as [Ha Hb].
    unfold coeff.
    rewrite Ha, Hb.
    apply Oeset.eq_bool_refl.
  - unfold equal.
    rewrite Feset.for_all_spec.
    rewrite forallb_forall; intros.
    apply (H x).
Qed.


Lemma union_qf_prop : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) (m1 m2: Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) (plus:elt2 -> elt2 -> elt2) zero (e3 e4 : elt1),
 Oeset.eq_bool Elt1 e3 e4 = true ->
 plus (coeff zero e3 m1) (coeff zero e3 m2) =
 plus (coeff zero e4 m1) (coeff zero e4 m2).
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq in H.
  now rewrite ! (coeff_eq_fmap _ _ _ _ H).
Qed.


Definition union_qf elt1 (Elt1 : Oeset.Rcd elt1) elt2 (m1 m2: Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) plus zero :=
  mk_qf Elt1 (fun e => plus (coeff zero e m1) (coeff zero e m2)) (union_qf_prop m1 m2 plus zero).
  
Definition union_map elt1 (Elt1 : Oeset.Rcd elt1) elt2 (plus : elt2 -> elt2 -> elt2) zero :=
  (fun m1 m2 => (Feset.union (Feset.build Elt1) (fst m1) (fst m2), union_qf m1 m2 plus zero)).
       
           
Lemma union_qf_supp : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 plus (s s' : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
  fst (fst s unionSE fst s', union_qf s s' plus) = (fst s unionSE fst s').
Proof.
  trivial.
Qed.


Lemma union_qf_spec : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 plus zero (s s' : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1)
    (x : elt1),
  x inSE fst (fst s unionSE fst s', union_qf s s' plus zero) ->
  (if x inSE? fst (fst s unionSE fst s', union_qf s s' plus zero)
   then Some (f (snd (fst s unionSE fst s', union_qf s s' plus zero)) x)
   else None) = Some (plus (coeff zero x s) (coeff zero x s')).
Proof.
  intros.
  now rewrite H.
Qed.

(************************************************************)

Definition f_transpo elt1 (Elt1 : Oeset.Rcd elt1) elt2 (mul1 : elt1 -> elt1 -> elt1) (mul2 : elt2 -> elt2 -> elt2) zero x y (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) :=
  fun e =>
    match Feset.choose (Feset.build Elt1) (Feset.filter (Feset.build Elt1) (fun e1 => Oeset.eq_bool Elt1 (mul1 e1 x) e) (fst s)) with
    |None => zero
    |Some y1 => mul2 (coeff zero y1 s) y
    end.


Lemma transpo_qf_prop :
  forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 (mul1 : elt1 -> elt1 -> elt1) (mul2 : elt2 -> elt2 -> elt2) (Elt1_is_CIO : CIO mul1 Elt1) zero x y (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) e1 e2,
    Oeset.eq_bool Elt1 e1 e2 = true ->
    f_transpo mul1 mul2 zero x y s e1 = f_transpo mul1 mul2 zero x y s e2.
Proof.
  intros. unfold f_transpo.
  assert (Feset.filter (Feset.build Elt1)
         (fun e0 : elt1 => Oeset.eq_bool Elt1 (mul1 e0 x) e1)
         (fst s) =SE= Feset.filter (Feset.build Elt1)
         (fun e0 : elt1 => Oeset.eq_bool Elt1 (mul1 e0 x) e2)
         (fst s)).
  apply Feset.equal_spec.
  intros. rewrite ! Feset.filter_spec.
  now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
  intros. apply (compat_g Elt1_is_CIO _ _ x) in H1. apply Oeset.eq_bool_true_compare_eq in H1. now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).

  intros. apply (compat_g Elt1_is_CIO _ _ x) in H1. apply Oeset.eq_bool_true_compare_eq in H1. now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  apply Feset.choose_spec3 in H0.
  destruct (Feset.choose (Feset.build Elt1)
           (Feset.filter (Feset.build Elt1)
              (fun e0 : elt1 => Oeset.eq_bool Elt1 (mul1 e0 x) e1)
              (fst s)));
  destruct (Feset.choose (Feset.build Elt1)
           (Feset.filter (Feset.build Elt1)
              (fun e0 : elt1 => Oeset.eq_bool Elt1 (mul1 e0 x) e2)
              (fst s))); try inversion H0; trivial.
  now rewrite (coeff_eq_fmap _ _ _ _ H0).
Qed.




Definition transpo_spec elt1 (Elt1 : Oeset.Rcd elt1) elt2 (zero : elt2) mul1 mul2 (Elt1_is_CIO : CIO mul1 Elt1) x y s :=
  mk_qf Elt1 (f_transpo mul1 mul2 zero x y s)
        (transpo_qf_prop mul2 Elt1_is_CIO zero x y s).

Definition transpo_map elt1 (Elt1 : Oeset.Rcd elt1) elt2 (zero : elt2) mul1 mul2 (Elt1_is_CIO : CIO mul1 Elt1) :=
  (fun e1 e2 s => (Feset.map (Feset.build Elt1) (Feset.build Elt1) (fun e => mul1 e e1) (fst s),
                        @transpo_spec elt1 Elt1 elt2 zero mul1 mul2 Elt1_is_CIO e1 e2 s)).


Lemma filter_elt_inv : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 mul1 (Elt1_is_CIO : CIO mul1 Elt1) (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1) (x1 x2 : elt1), forall e,
    e inSE Feset.filter (Feset.build Elt1) (fun e1 : elt1 => Oeset.eq_bool Elt1 (mul1 e1 x2) (mul1 x1 x2)) (fst s) ->    
    Oeset.eq_bool Elt1 e x1 = true.
Proof.
  intros.
  rewrite Feset.filter_spec in H.
  apply andb_true_iff in H as [_ H].
  apply Oeset.eq_bool_true_compare_eq.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply (proj2 (compat_g Elt1_is_CIO _ _ _) H).
  intros. apply (compat_g Elt1_is_CIO _ _ x2) in H1. apply Oeset.eq_bool_true_compare_eq in H1. now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).
Qed.




Lemma transpo_spec1 : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 (zero : elt2) mul1 mul2 (Elt1_is_CIO : CIO mul1 Elt1) (s : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1)
    (x1 x2 : elt1) (y : elt2),
  mul1 x1 x2
  inSE fst
         (Feset.map (Feset.build Elt1) (Feset.build Elt1)
            (fun e : elt1 => mul1 e x2) (fst s),
         transpo_spec zero mul2 Elt1_is_CIO x2 y s) ->
  (if
    mul1 x1 x2
    inSE? fst
            (Feset.map (Feset.build Elt1) (Feset.build Elt1)
               (fun e : elt1 => mul1 e x2) (fst s),
            transpo_spec zero mul2 Elt1_is_CIO x2 y s)
   then
    Some
      (f
         (snd
            (Feset.map (Feset.build Elt1) (Feset.build Elt1)
               (fun e : elt1 => mul1 e x2) (fst s),
            transpo_spec zero mul2 Elt1_is_CIO x2 y s)) 
         (mul1 x1 x2))
    else None) = Some (mul2 (coeff zero x1 s) y).
Proof.
  intros.
  rewrite H. unfold snd.
  unfold transpo_spec. unfold f.
   unfold f_transpo. 
  case_eq (Feset.choose (Feset.build Elt1)
        (Feset.filter (Feset.build Elt1)
           (fun e1 : elt1 =>
            Oeset.eq_bool Elt1 (mul1 e1 x2) (mul1 x1 x2)) 
           (fst s))); intros.
  - apply Feset.choose_spec1 in H0.
    apply filter_elt_inv in H0; trivial.
    apply Oeset.eq_bool_true_compare_eq in H0.
    now rewrite (coeff_eq_fmap _ _ _ _ H0).
  - apply Feset.choose_spec2  in H0.
    rewrite Feset.is_empty_spec in H0.
     unfold fst in H.
    rewrite Feset.mem_map in H.
    destruct H as [a [Ha Hb]].
    apply (Oeset.in_mem_bool Elt1) in Hb.
    rewrite <- Feset.mem_elements in Hb.
    apply (proj1 (Feset.equal_spec _ _ _)) with a in H0.
    rewrite Feset.mem_empty in H0.
    rewrite Feset.filter_spec in H0.
    unfold fst in H0. rewrite Hb in H0.
    apply Oeset.compare_eq_sym in Ha.
    apply Oeset.eq_bool_true_compare_eq in Ha.
    rewrite Ha in H0; inversion H0.
    intros. apply (compat_g Elt1_is_CIO _ _ x2) in H1. apply Oeset.eq_bool_true_compare_eq in H1. now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).    
Qed.


Fixpoint compare_fmap_aux elt1 elt2 (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) zero (s1 s2 : (Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1)) (l : list elt1) :=
  match l with
  |nil => Eq
  |a::l' => if forallb (fun x => Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2)) l'
            then Oeset.compare Elt2 (coeff zero a s1) (coeff zero a s2)
            else compare_fmap_aux Elt2 zero s1 s2 l'
  end.

Definition compare_fmap elt1 elt2 (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) zero (s1 s2 : (Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1)) :=
  compare_fmap_aux Elt2 zero s1 s2 (Feset.elements (Feset.build Elt1) (Feset.union _ (fst s1) (fst s2))).


Lemma compare_equal : forall elt1 (Elt1 : Oeset.Rcd elt1) elt2 (Elt2 : Oeset.Rcd elt2) (zero : elt2) (s1 s2 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
    compare_fmap Elt2 zero s1 s2 = Eq <-> equal Elt2 zero s1 s2 = true.
Proof.
  split; intros.
  unfold equal.
  unfold compare_fmap in H.
  rewrite Feset.for_all_spec.
  induction (Feset.elements (Feset.build Elt1) (fst s1 unionSE fst s2)); trivial.
  simpl.
  simpl in H.
  case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2)) l); intros.
  rewrite H0 in H.
  apply Oeset.eq_bool_true_compare_eq in H.
  now rewrite H.
  rewrite H0 in H.
  rewrite (IHl H) in H0.
  inversion H0.
  unfold equal in H.
  unfold compare_fmap.
  rewrite Feset.for_all_spec in H.
  induction (Feset.elements (Feset.build Elt1) (fst s1 unionSE fst s2)); trivial.
  simpl.
  simpl in H.
  apply andb_prop in H as [Ha Hb].
  apply Oeset.eq_bool_true_compare_eq in Ha.
  now rewrite Hb.
Qed.


Lemma compare_fmap_eq_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 zero (a1 a2 a3 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
    compare_fmap Elt2 zero a1 a2 = Eq ->
    compare_fmap Elt2 zero a2 a3 = Eq -> compare_fmap Elt2 zero a1 a3 = Eq.
Proof.
  intros.
  apply compare_equal in H.
  apply compare_equal in H0.
  apply compare_equal.
  apply equal_spec.
  intros.
  apply (proj1 (equal_spec _ _ _ _)) with x in H.
  apply (proj1 (equal_spec _ _ _ _)) with x in H0.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
Qed.


Lemma compare_fmap_aux_spec_lt : forall elt1 elt2 (Elt1: Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) zero l (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap_aux Elt2 zero s1 s2 l = Lt <->
  exists (l1 : list elt1) (a : elt1) (l2 : list elt1),
    l = l1 ++ a :: l2 /\
    lt Elt2 (coeff zero a s1) (coeff zero a s2) /\
    forallb (fun x : elt1 => Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2)) l2 = true.
Proof.
  split; intros.
  induction l.
  inversion H.
  simpl in H.
  case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2)) l); intros.
  rewrite H0 in H.
  exists nil. exists a. exists l. repeat split; trivial.
  rewrite H0 in H.
  apply IHl in H as [l1 [a0 [l2 [H1 [H2 H3]]]]].
  exists (a::l1); exists a0; exists l2.
  rewrite H1.
  now repeat split.
  destruct H as [l1 [a [l2 [H1 [H2 H3]]]]].
  rewrite H1.
  clear H1.
  induction l1.
  simpl.
  now rewrite H3.
  simpl.
  rewrite if_eq_false; trivial.
  rewrite forallb_app.
  simpl.
  assert (Oeset.eq_bool Elt2 (coeff zero a s1) (coeff zero a s2) =false).
  apply not_true_iff_false; intro.
  apply Oeset.eq_bool_true_compare_eq in H.
  rewrite H in H2. inversion H2.
  apply andb_false_iff. right.
  apply andb_false_iff. left. trivial.
Qed.


Lemma Sorted_lt : forall (A : Type) (TA : Oeset.Rcd A) (a a' : A) l1 l2 l3,
    Sorted (fun x y : A => lt TA x y) (l1 ++ a :: l2 ++ a' :: l3) ->
    lt TA a a'.
Proof.
  induction l1; intros.
  - simpl in H.
    apply Sorted_inv in H as [H H0].
    revert H; revert H0; revert l3; induction l2; intros.
    * simpl in H0.
      now apply HdRel_inv in H0.
    * rewrite <- app_comm_cons in H.
      apply Sorted_inv in H as [H H1].
      rewrite <- app_comm_cons in H0.
      apply HdRel_inv in H0.
      assert (HdRel (fun x y : A => lt TA x y) a (l2 ++ a' :: l3)).
      inversion H1.
      apply HdRel_nil.
      apply (Oeset.compare_lt_trans _ _ _ _ H0) in H3.
      now apply HdRel_cons.
      apply (IHl2 _ H2 H).
  - rewrite <- app_comm_cons in H.
    apply Sorted_inv in H as [H _].
    apply (IHl1 _ _  H).
Qed.
  
Lemma compare_fmap_spec_lt : forall elt1 elt2 (Elt1: Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) zero (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap Elt2 zero s1 s2 = Lt <->
  exists a,
    lt Elt2 (coeff zero a s1) (coeff zero a s2) /\
    forall x, Oeset.compare Elt1 a x = Lt -> Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2) = true.
Proof.
  split; intros.
  - apply compare_fmap_aux_spec_lt in H as [l1 [a [l2 [H [H0 H1]]]]].
    exists a; split; trivial.
    intros.
    case_eq (x inSE? (fst s1 unionSE fst s2)); intros.
    rewrite Feset.mem_elements in H3.
    apply Oeset.mem_bool_true_iff in H3 as [a' [H3 H4]].
    assert (In a' l2).
    apply (Oeset.compare_lt_eq_trans _ _ _ _ H2) in H3.
    rewrite H in H4.
    apply in_app_or in H4 as [H4|H4].
    pose (Feset.elements_spec3 (Feset.build Elt1) (fst s1 unionSE fst s2)).
    rewrite H in s.
    apply in_split in H4 as [l3 [l4 H4]].
    rewrite H4 in s.
    rewrite <- app_assoc in s.
    rewrite <- app_comm_cons in s.
    apply (Sorted_lt Elt1 a' a l3 l4 l2) in s.
    rewrite Oeset.compare_lt_gt in s.
    rewrite CompOpp_iff in s.
    rewrite s in H3; inversion H3.
    destruct H4; trivial.
    rewrite H4 in H3.
    rewrite Oeset.compare_eq_refl in H3.
    inversion H3.
    rewrite 2 (coeff_eq_fmap _ _ _ _ H3).
    now apply (forallb_forall _ _) with a' in H1.
    rewrite Feset.mem_union in H3.
    apply orb_false_iff in H3 as [H3 H4].
    unfold coeff.
    rewrite H3. rewrite H4.
    apply Oeset.eq_bool_refl.
  - apply compare_fmap_aux_spec_lt.
    destruct H as [a [H H0]].
    assert (a inSE (fst s1 unionSE fst s2)).
    rewrite Feset.mem_union.
    case_eq (a inSE? fst s1); intros; trivial.
    case_eq (a inSE? fst s2); intros; trivial.
    unfold coeff in H.
    rewrite H1 in H. rewrite H2 in H.
    rewrite Oeset.compare_eq_refl in H.
    inversion H.
    rewrite Feset.mem_elements in H1.
    apply Oeset.mem_bool_true_iff in H1 as [a' [H1 H2]].
    apply in_split in H2 as [l1 [l2 H2]].
    rewrite 2 (coeff_eq_fmap _ _ _ _ H1) in H.
    exists l1; exists a'; exists l2; repeat split; trivial.
    apply forallb_forall; intros.
    apply H0.
    apply in_split in H3 as [l3 [l4 H3]].
    rewrite H3 in H2.
    pose (Feset.elements_spec3 (Feset.build Elt1) (fst s1 unionSE fst s2)).
    rewrite H2 in s.
    apply (Sorted_lt Elt1 a' x l1 l3 l4) in s.
    apply (Oeset.compare_eq_lt_trans _ _ _ _ H1 s).
Qed.



Lemma compare_fmap_aux_spec_gt : forall elt1 elt2 (Elt1: Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) zero l (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap_aux Elt2 zero s1 s2 l = Gt <->
  exists (l1 : list elt1) (a : elt1) (l2 : list elt1),
    l = l1 ++ a :: l2 /\
    gt Elt2 (coeff zero a s1) (coeff zero a s2) /\
    forallb (fun x : elt1 => Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2)) l2 = true.
Proof.
  split; intros.
  induction l.
  inversion H.
  simpl in H.
  case_eq (forallb (fun x : elt1 => Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2)) l); intros.
  rewrite H0 in H.
  exists nil. exists a. exists l. repeat split; trivial.
  rewrite H0 in H.
  apply IHl in H as [l1 [a0 [l2 [H1 [H2 H3]]]]].
  exists (a::l1); exists a0; exists l2.
  rewrite H1.
  now repeat split.
  destruct H as [l1 [a [l2 [H1 [H2 H3]]]]].
  rewrite H1.
  clear H1.
  induction l1.
  simpl.
  now rewrite H3.
  simpl.
  rewrite if_eq_false; trivial.
  rewrite forallb_app.
  simpl.
  assert (Oeset.eq_bool Elt2 (coeff zero a s1) (coeff zero a s2) =false).
  apply not_true_iff_false; intro.
  apply Oeset.eq_bool_true_compare_eq in H.
  rewrite H in H2. inversion H2.
  apply andb_false_iff. right.
  apply andb_false_iff. left. trivial.
Qed.



Lemma compare_fmap_spec_gt : forall elt1 elt2 (Elt1: Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) zero (s1 s2 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap Elt2 zero s1 s2 = Gt <->
  exists a,
    gt Elt2 (coeff zero a s1) (coeff zero a s2) /\
    forall x, Oeset.compare Elt1 a x = Lt -> Oeset.eq_bool Elt2 (coeff zero x s1) (coeff zero x s2) = true.
Proof.
  split; intros.
  - apply compare_fmap_aux_spec_gt in H as [l1 [a [l2 [H [H0 H1]]]]].
    exists a; split; trivial.
    intros.
    case_eq (x inSE? (fst s1 unionSE fst s2)); intros.
    rewrite Feset.mem_elements in H3.
    apply Oeset.mem_bool_true_iff in H3 as [a' [H3 H4]].
    assert (In a' l2).
    apply (Oeset.compare_lt_eq_trans _ _ _ _ H2) in H3.
    rewrite H in H4.
    apply in_app_or in H4 as [H4|H4].
    pose (Feset.elements_spec3 (Feset.build Elt1) (fst s1 unionSE fst s2)).
    rewrite H in s.
    apply in_split in H4 as [l3 [l4 H4]].
    rewrite H4 in s.
    rewrite <- app_assoc in s.
    rewrite <- app_comm_cons in s.
    apply (Sorted_lt Elt1 a' a l3 l4 l2) in s.
    rewrite Oeset.compare_lt_gt in s.
    rewrite CompOpp_iff in s.
    rewrite s in H3; inversion H3.
    destruct H4; trivial.
    rewrite H4 in H3.
    rewrite Oeset.compare_eq_refl in H3.
    inversion H3.
    rewrite 2 (coeff_eq_fmap _ _ _ _ H3).
    now apply (forallb_forall _ _) with a' in H1.
    rewrite Feset.mem_union in H3.
    apply orb_false_iff in H3 as [H3 H4].
    unfold coeff.
    rewrite H3. rewrite H4.
    apply Oeset.eq_bool_refl.
  - apply compare_fmap_aux_spec_gt.
    destruct H as [a [H H0]].
    assert (a inSE (fst s1 unionSE fst s2)).
    rewrite Feset.mem_union.
    case_eq (a inSE? fst s1); intros; trivial.
    case_eq (a inSE? fst s2); intros; trivial.
    unfold coeff in H.
    rewrite H1 in H. rewrite H2 in H.
    rewrite Oeset.compare_eq_refl in H.
    inversion H.
    rewrite Feset.mem_elements in H1.
    apply Oeset.mem_bool_true_iff in H1 as [a' [H1 H2]].
    apply in_split in H2 as [l1 [l2 H2]].
    rewrite 2 (coeff_eq_fmap _ _ _ _ H1) in H.
    exists l1; exists a'; exists l2; repeat split; trivial.
    apply forallb_forall; intros.
    apply H0.
    apply in_split in H3 as [l3 [l4 H3]].
    rewrite H3 in H2.
    pose (Feset.elements_spec3 (Feset.build Elt1) (fst s1 unionSE fst s2)).
    rewrite H2 in s.
    apply (Sorted_lt Elt1 a' x l1 l3 l4) in s.
    apply (Oeset.compare_eq_lt_trans _ _ _ _ H1 s).
Qed.


Lemma compare_fmap_eq_lt_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 zero (a1 a2 a3 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap Elt2 zero a1 a2 = Eq ->
  compare_fmap Elt2 zero a2 a3 = Lt -> compare_fmap Elt2 zero a1 a3 = Lt.
Proof.
  intros.
  apply compare_equal in H.
  apply compare_fmap_spec_lt.
  apply compare_fmap_spec_lt in H0 as [a [H0 H1]].
  exists a; split.
  apply (equal_spec _ _ _ _) with a in H.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply (Oeset.compare_eq_lt_trans _ _ _ _ H H0).
  intros.
  apply (equal_spec _ _ _ _) with x in H.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
  apply (H1 _ H2).
Qed.


Lemma compare_fmap_lt_eq_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 zero (a1 a2 a3 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap Elt2 zero a1 a2 = Lt ->
  compare_fmap Elt2 zero a2 a3 = Eq -> compare_fmap Elt2 zero a1 a3 = Lt.
Proof.
  intros.
  apply compare_equal in H0.
  apply compare_fmap_spec_lt.
  apply compare_fmap_spec_lt in H as [a [H H1]].
  exists a; split.
  apply (equal_spec _ _ _ _) with a in H0.
  apply Oeset.eq_bool_true_compare_eq in H0.
  apply (Oeset.compare_lt_eq_trans _ _ _ _ H H0).
  intros.
  apply (equal_spec _ _ _ _) with x in H0.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  apply (H1 _ H2).
Qed.




Lemma compare_fmap_lt_trans : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 zero (a1 a2 a3 : Feset.set (Feset.build Elt1) * (quotient_function elt2 Elt1)),
  compare_fmap Elt2 zero a1 a2 = Lt ->
  compare_fmap Elt2 zero a2 a3 = Lt -> compare_fmap Elt2 zero a1 a3 = Lt.
Proof.
  intros.
  apply compare_fmap_spec_lt.
  apply compare_fmap_spec_lt in H0 as [a [H0 H1]].
  apply compare_fmap_spec_lt in H as [a' [H H2]].
  case_eq (Oeset.compare Elt1 a a'); intros.
  - exists a; split.
    rewrite <- 2 (coeff_eq_fmap _ _ _ _ H3) in H.
    apply (Oeset.compare_lt_trans _ _ _ _ H H0).
    intros.
    rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (H1 _ H4)).
    apply H2.
    apply Oeset.compare_eq_sym in H3.
    apply (Oeset.compare_eq_lt_trans _ _ _ _ H3 H4).
  - exists a'; split; intros.
    specialize (H1 _ H3).
    apply Oeset.eq_bool_true_compare_eq in H1.
    apply (Oeset.compare_lt_eq_trans _ _ _ _ H H1).
    specialize (H2 _ H4).
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H2).
    apply H1.
    apply (Oeset.compare_lt_trans _ _ _ _ H3 H4).
  - exists a; split; intros;
    rewrite Oeset.compare_lt_gt in H3;
    rewrite CompOpp_iff in H3.
    specialize (H2 _ H3).
    apply Oeset.eq_bool_true_compare_eq in H2.
    apply (Oeset.compare_eq_lt_trans _ _ _ _ H2 H0).
    specialize (H1 _ H4).
    rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H1).
    apply H2.
    apply (Oeset.compare_lt_trans _ _ _ _ H3 H4).
Qed.


Lemma compare_fmap_lt_gt : forall elt1 elt2 (Elt1 : Oeset.Rcd elt1) Elt2 zero (a1 a2 : Feset.set (Feset.build Elt1) * quotient_function elt2 Elt1),
  compare_fmap Elt2 zero a1 a2 = CompOpp (compare_fmap Elt2 zero a2 a1).
Proof.
  intros.
  case_eq (compare_fmap Elt2 zero a2 a1); intros.
  - apply compare_equal in H.
  assert (equal Elt2 zero a1 a2 = true).
  apply equal_spec.
  intros.
  rewrite Oeset.eq_bool_sym.
  revert x.
  now apply equal_spec.
  now apply compare_equal in H0.
  - apply compare_fmap_spec_lt in H as [a [H H0]].
    apply compare_fmap_spec_gt.
    exists a; split.
    rewrite Oeset.compare_lt_gt.
    now apply CompOpp_iff.
    intros. rewrite Oeset.eq_bool_sym.
    now apply H0.
  - apply compare_fmap_spec_gt in H as [a [H H0]].
    apply compare_fmap_spec_lt.
    exists a; split.
    rewrite Oeset.compare_lt_gt.
    now apply CompOpp_iff.
    intros. rewrite Oeset.eq_bool_sym.
    now apply H0.
Qed.
