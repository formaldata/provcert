(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

(** Generalities on polynomials over a ring *)

(**Set Printing All.**)
Require Export Setoid Bool.

Require Import List NArith Facts BasicFacts.
Require Import ListFacts OrderedSet ListSort FiniteMap FiniteBag FiniteSet.
Require Import SemiRing.


Section Pol.
Hypothesis variable : Set.
Hypothesis OX : Oeset.Rcd variable.
Inductive PolVar := PV : variable -> PolVar.

Definition OEPV : Oeset.Rcd PolVar.
  split with (fun v1 v2 => 
                match v1, v2 with 
                  (PV  x1), (PV x2) => Oeset.compare OX x1 x2
                end).
  - intros [v1] [v2] [v3]. apply Oeset.compare_eq_trans.
  - intros [v1] [v2] [v3]. apply Oeset.compare_eq_lt_trans.
  - intros [v1] [v2] [v3]. apply Oeset.compare_lt_eq_trans.
  - intros [v1] [v2] [v3]. apply Oeset.compare_lt_trans.
  - intros [v1] [v2]; apply Oeset.compare_lt_gt.
Defined.

Hypothesis BePV : (Febag.Rcd OEPV).

Notation Mon := (bA BePV).
Notation OEMon := (OBA BePV).

Definition OEMon_is_CIO : CIO (Febag.union _) OEMon.
  repeat split; intros.
  now apply union_eq_2.
  now apply union_eq_2 in H.
  now apply union_eq.
  now apply union_eq in H.
Defined.

Hypothesis R : Set.
Hypothesis zero : R.
Hypothesis one : R.
Hypothesis plus : R -> R -> R.
Hypothesis mul : R -> R -> R.
Hypothesis TR : total_order R.
Hypothesis R_is_CSR : CSR zero one plus mul TR.
  
Hypothesis FePol : (FePmap.Rcd TR zero plus mul OEMon_is_CIO).
        
Notation Pol := (FePmap.pmap FePol).

Definition OEPol : Oeset.Rcd Pol.
  split with (FePmap.compare FePol);
    intros.
    - apply (FePmap.compare_eq_trans _ _ _ _ H H0).
    - apply (FePmap.compare_eq_lt_trans _ _ _ _ H H0).
    - apply (FePmap.compare_lt_eq_trans _ _ _ _ H H0).
    - apply (FePmap.compare_lt_trans _ _ _ _ H H0).
    - apply FePmap.compare_lt_gt.
Defined.


Notation Pol_0 := (FePmap.singleton FePol (emptysetBE) zero).
Notation Pol_1 := (FePmap.singleton FePol (emptysetBE) one).
Notation add_Pol := (FePmap.union FePol). 
Notation mul_Pol := (FePmap.mul_Pol FePol).

Notation plus_assoc_eq_bool := (plus_assoc_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_comm_eq_bool := (plus_comm_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_zero_l_eq_bool := (plus_zero_l_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_zero_r_eq_bool := (plus_zero_r_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_eq_bool := (plus_compat_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_l_eq_bool := (plus_compat_l_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_r_eq_bool := (plus_compat_r_eq_bool (isCM_plus_SR R_is_CSR)).

Notation mul_assoc_eq_bool := (SemiRing.plus_assoc_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_comm_eq_bool := (SemiRing.plus_comm_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_one_l_eq_bool := (SemiRing.plus_zero_l_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_one_r_eq_bool := (SemiRing.plus_zero_r_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_compat_eq_bool := (SemiRing.plus_compat_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_compat_l_eq_bool := (SemiRing.plus_compat_l_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_compat_r_eq_bool := (SemiRing.plus_compat_r_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_zero_l_eq_bool := (SemiRing.mul_zero_l_eq_bool R_is_CSR).
Notation mul_zero_r_eq_bool := (SemiRing.mul_zero_r_eq_bool R_is_CSR).
Notation mul_distr_plus_r_eq_bool := (mul_distr_plus_r_eq_bool R_is_CSR).

Notation plus_aux_1 := (plus_aux_1 (isCM_plus_SR R_is_CSR)).
Notation plus_aux_2 := (plus_aux_2_d (isCM_plus_SR R_is_CSR)).
Notation mul_aux_1 := (plus_aux_2_g (isCM_mul_SR R_is_CSR)).
Notation plus_aux_3 := (plus_aux_3 (isCM_plus_SR R_is_CSR)).
Notation plus_aux_4 := (plus_aux_4 (isCM_plus_SR R_is_CSR)).
Notation plus_if_zero := (plus_if_zero (isCM_plus_SR R_is_CSR)).

Notation fold_sortir := (fold_sortir (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l := (fold_sortir_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r := (fold_sortir_r (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_bis := (fold_sortir_l_bis (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_bis := (fold_sortir_r_bis (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_plus_l := (fold_sortir_l_plus_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_plus_r := (fold_sortir_l_plus_r (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_plus_l := (fold_sortir_r_plus_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_plus_r := (fold_sortir_r_plus_r (isCM_plus_SR R_is_CSR)).

Ltac fold_sortir_if := intros; apply fold_sortir; intros; apply plus_if_zero.
Ltac fold_sortir_2_if := intros; apply fold_sortir; intros; apply fold_sortir; intros; apply plus_if_zero.



(**************************************)
(** Pol0 =/= Pol1 **)
Lemma Pol0_Pol1_diff :  t_compare OEPol Pol_0 Pol_1 <> Eq.
Proof.
  simpl.
  apply FePmap.Pol0_Pol1_diff.
  apply R_is_CSR.
Qed.

(**CM add_Pol Pol0 **)
Lemma add_Pol_assoc :
  forall a1 a2 a3 : Pol, eq OEPol (add_Pol a1 (add_Pol a2 a3)) (add_Pol (add_Pol a1 a2) a3).
Proof.
  apply (FePmap.add_Pol_assoc _ R_is_CSR).
Qed.
  
Lemma add_Pol_0 : forall a : Pol, eq OEPol (add_Pol Pol_0 a) a.
Proof.
  intros.
  apply (FePmap.add_Pol_0 _ R_is_CSR).
Qed.

Lemma add_Pol_comm :  forall a1 a2 : Pol, eq OEPol (add_Pol a1 a2) (add_Pol a2 a1).
Proof.
  apply (FePmap.add_Pol_comm _ R_is_CSR).
Qed.

Lemma add_Pol_mono :  forall a1 a2 b1 b2 : Pol, eq OEPol a1 a2 -> eq OEPol b1 b2 -> eq OEPol (add_Pol a1 b1) (add_Pol a2 b2).
Proof.
  apply (FePmap.add_Pol_mono _ R_is_CSR).
Qed.


(********* transpo alternative def **********)
Lemma transpo_alt : forall x y e a1,
  Oeset.eq_bool TR (FePmap.coeff FePol x (FePmap.transpo FePol e y a1))
                (if Oeset.eq_bool OEMon x (Febag.union _ (Febag.diff _ x e) e) then
                   mul (FePmap.coeff FePol (Febag.diff _ x e) a1) y else zero) = true.
Proof.
  intros.
  case_eq (Oeset.eq_bool OEMon x (Febag.diff BePV x e unionBE e)); intros.
  - rewrite (FePmap.coeff_eq _ _ _ _ H); apply Oeset.eq_bool_true_compare_eq in H.
    case_eq (x inSE? FePmap.support _ (FePmap.transpo FePol e y a1));
      intros; rewrite (Feset.mem_eq_1 _ _ _ _ H) in H0.
    * apply FePmap.transpo_find in H0.
      apply FePmap.coeff_spec_1 in H0.
      rewrite <- H0; apply Oeset.eq_bool_refl.
    * rewrite 2 FePmap.coeff_spec_2.
      -- rewrite Oeset.eq_bool_sym; apply mul_zero_l_eq_bool.
      -- apply FePmap.find_spec_alt.
         apply not_true_iff_false. intro.
         apply not_true_iff_false in H0. destruct H0.
         rewrite FePmap.transpo_supp.
         apply Feset.mem_map.
         rewrite Feset.mem_elements in H1.
         apply Oeset.mem_bool_true_iff in H1 as [a [H1 H2]].
         exists a; split; trivial.
         apply (proj1 (union_eq_2 _ e _ _) H1).
      -- now apply FePmap.find_spec_alt.
  - rewrite FePmap.coeff_spec_2; try apply Oeset.eq_bool_refl.
    apply FePmap.find_spec_alt.
    apply not_true_iff_false; apply not_true_iff_false in H.
    intro; destruct H.
    apply FePmap.transpo_supp_mem in H0 as [a [_ H]].
    apply Oeset.eq_bool_true_compare_eq; apply Febag.compare_spec.
    apply Oeset.eq_bool_true_compare_eq in H; apply Febag.compare_spec in H.
    apply Febag.nb_occ_equal; intros.
    apply (proj1 (Febag.nb_occ_equal _ _ _)) with e0 in H; intros.
    rewrite Febag.nb_occ_union in H.
    now rewrite Febag.nb_occ_union,Febag.nb_occ_diff, H, N.add_sub.
Qed.


Lemma coeff_transpo_alt : forall x x' y a2,
    Oeset.eq_bool TR (FePmap.coeff FePol x (FePmap.transpo _ x' y a2))
    (fold_left (fun (a : R) (e : Mon) => if Oeset.eq_bool OEMon x (e unionBE x') then plus (mul (FePmap.coeff FePol e a2) y) a else a) (Feset.elements _ (FePmap.support _ a2)) zero) = true.
Proof.
  intros.
  case_eq (x inSE? FePmap.support _ (FePmap.transpo _ x' y a2)); intros.
  - apply FePmap.transpo_coeff in H as [x1 [H [H1 H2]]].
    rewrite H2.
    apply in_feset_elements in H1 as [l1 [l2 [H1 H3]]].
    rewrite H1.
    rewrite fold_left_app.
    apply fold_sortir_r.
    intros; apply plus_if_zero.
    simpl.
    apply fold_sortir_r_plus_l.
    intros; apply plus_if_zero.
    rewrite if_eq_true; trivial.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_aux_4 _ _ _)).
    rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_zero_r_eq_bool _)).
    apply plus_compat_r_eq_bool.
    apply fold_sortir_r_bis.
    intros; apply plus_if_zero.
    rewrite <- fold_left_app.
    clear H2; clear H1.
    induction (l1++l2); try apply Oeset.eq_bool_refl.
    simpl.
    apply fold_sortir_r.
    intros; apply plus_if_zero.
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
    rewrite <- (union_eq_2_bis BePV).
    rewrite if_eq_false; try solve [now specialize (H3 a (List.in_eq _ _))].
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_zero_l_eq_bool _)).
    apply IHl.
    intros; apply H3; now right.    
  - rewrite FePmap.coeff_spec_2.
    * rewrite FePmap.transpo_supp in H.
      rewrite Feset.map_unfold in H.
      induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol a2)).
      -- apply Oeset.eq_bool_refl.
      -- simpl.
         apply fold_sortir_r.
         intros; apply plus_if_zero.
         simpl in H. rewrite Feset.add_spec in H.
         apply orb_false_iff in H as [Ha Hb].
         rewrite if_eq_false; trivial.
         apply IHl in Hb.
         now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_zero_l_eq_bool _)).
    * now apply FePmap.find_spec_alt.
Qed.


Lemma mul_Pol_equiv : forall x s1 s2,
    Oeset.eq_bool TR (FePmap.coeff FePol x (FePmap.mul_Pol _ s1 s2))
    (fold_left (fun (a1 : R) (e1 : Mon) => 
       (fold_left (fun (a2 : R) (e2 : Mon) => if Oeset.eq_bool OEMon x (Febag.union _ e1 e2) then 
         plus (mul (FePmap.coeff _ e1 s1) (FePmap.coeff _ e2 s2)) a2 else a2) (Feset.elements _ (FePmap.support _ s2)) a1))
               (Feset.elements _ (FePmap.support _ s1)) zero) = true.
Proof.
  intros.
  rewrite FePmap.mul_Pol_spec.
  rewrite FePmap.fold_spec.
  rewrite Feset.fold_spec.
  revert s1.
  induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s2)); intros.
  - induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s1)); [>apply FePmap.coeff_empty|trivial].
  - rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.fold_union_coeff _ R_is_CSR _ _ _ _ _ _)).
    simpl.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (fold_2_sortir R_is_CSR _ _ (fun e1 e2 => Oeset.eq_bool OEMon x (e1 unionBE e2)) (fun e1 e2 => mul (FePmap.coeff FePol e1 s1) (FePmap.coeff FePol e2 s2)) _ _)).
    apply plus_compat_eq_bool; [> apply coeff_transpo_alt|apply IHl].
Qed.


Lemma mul_Pol_comm : forall a1 a2 : Pol, eq OEPol (mul_Pol a1 a2) (mul_Pol a2 a1).
Proof.
  intros.
  simpl.
  rewrite FePmap.compare_spec.
  rewrite FePmap.equal_spec; intro.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_Pol_equiv _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (mul_Pol_equiv _ _ _)).
  apply (FePmap.mul_Pol_comm_aux _ R_is_CSR); intros; try apply plus_if_zero.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (union_comm_eq_bool _ _ _)).
  destruct (Oeset.eq_bool OEMon x (a unionBE a0)); try apply Oeset.eq_bool_refl.
  apply plus_compat_l_eq_bool.
  apply mul_comm_eq_bool.
Qed.


Lemma mul_Pol_equiv_3 : forall x s1 s2 s3,
    Oeset.eq_bool TR (FePmap.coeff FePol x (FePmap.mul_Pol _ (FePmap.mul_Pol _ s1 s2) s3))
    (fold_left (fun (a1 : R) (e1 : Mon) =>
      (fold_left (fun (a2 : R) (e2 : Mon) =>
        (fold_left (fun (a3 : R) (e3 : Mon) => if Oeset.eq_bool OEMon x (Febag.union _ (Febag.union _ e1 e2) e3) then
         plus (mul (mul (FePmap.coeff _ e1 s1) (FePmap.coeff _ e2 s2)) (FePmap.coeff _ e3 s3)) a3 else a3) (Feset.elements _ (FePmap.support _ s3)) a2)) (Feset.elements _ (FePmap.support _ s2)) a1))
               (Feset.elements _ (FePmap.support _ s1)) zero) = true.
Proof.
  intros.
  rewrite FePmap.mul_Pol_spec.
  rewrite FePmap.fold_spec.
  rewrite Feset.fold_spec.
  revert s1; revert s2.
  induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s3)); intro s2.
  - induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s2)); intros.
    * induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s1)); [>apply FePmap.coeff_empty|trivial].
    * apply IHl.
  - intros.
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.fold_union_coeff _ R_is_CSR _ _ _ _ _ _)).
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl _ _))).
    clear IHl.
    simpl. rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (fold_3_sortir R_is_CSR _ _ _ (fun e1 e2 e3 => Oeset.eq_bool OEMon x ((e1 unionBE e2) unionBE e3)) (fun e1 e2 e3 => mul (mul (FePmap.coeff FePol e1 s1) (FePmap.coeff FePol e2 s2)) (FePmap.coeff FePol e3 s3)) _ _)).
    apply plus_compat_l_eq_bool.
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (transpo_alt _ _ _ _)).
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (aux_5 R_is_CSR (fun e1 e2 => Oeset.eq_bool OEMon x ((e1 unionBE e2) unionBE a)) _ _ _ _)).
    case_eq (Oeset.eq_bool OEMon x (Febag.diff BePV x a unionBE a)); intros.
    * rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_compat_l_eq_bool _ _ _ (mul_Pol_equiv _ _ _))).
      apply mul_compat_l_eq_bool.
      apply (equiv_fold_fold R_is_CSR); intros; try apply plus_if_zero.
      rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
      rewrite <- union_eq_2_bis; apply Oeset.eq_bool_refl.
    * rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (mul_zero_l_eq_bool (FePmap.coeff FePol a s3))).
      apply mul_compat_l_eq_bool.
      revert s2.
      induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s1)); intros; try apply Oeset.eq_bool_refl.
      simpl.
      apply fold_sortir_r. fold_sortir_if.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl0 _))).
      rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_zero_r_eq_bool _)); clear IHl0.
      induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol s2)); intros; try apply Oeset.eq_bool_refl.
      simpl.
      apply fold_sortir_r. intros; apply plus_if_zero.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ IHl1)).
      rewrite (aux_6 _ _ _ H); rewrite Oeset.eq_bool_sym; apply plus_zero_r_eq_bool.
Qed.


Lemma mul_Pol_1 : forall a : Pol, eq OEPol (mul_Pol Pol_1 a) a.
Proof.
  intros.
  rewrite (Oeset.compare_eq_1 _ _ _ _ (mul_Pol_comm _ _)).
  apply FePmap.compare_spec.
  apply FePmap.equal_spec.
  intro.
  rewrite FePmap.mul_Pol_spec, FePmap.fold_spec, FePmap.singleton_supp, Feset.fold_spec.
  destruct (aux_3 (FePmap.FeElt1 FePol)) as [x1 [H0 H1]].
  assert ((FePmap.coeff FePol x
       (fold_left (fun (a0 : Pol) (e : Febag.bag BePV) => add_Pol (FePmap.transpo FePol e (FePmap.coeff FePol e Pol_1) a) a0)
                  (Feset.elements (FePmap.FeElt1 FePol) (Feset.singleton (FePmap.FeElt1 FePol) (emptysetBE))) (FePmap.empty FePol))) =
          (FePmap.coeff FePol x
       (fold_left (fun (a0 : Pol) (e : Febag.bag BePV) => add_Pol (FePmap.transpo FePol e (FePmap.coeff FePol e Pol_1) a) a0)
                  (x1 :: nil) (FePmap.empty FePol)))).
  do 2 f_equal; trivial.
  rewrite H. clear H.
  simpl; rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (FePmap.coeff_empty _ _))).
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_zero_r_eq_bool _)).
  rewrite (FePmap.coeff_eq _ _ _ _ (empty_eq _ _ _ H1)).
  case_eq (x inSE? (FePmap.support FePol a)); intros.
  - rewrite Feset.mem_elements in H.
    apply Oeset.mem_bool_true_iff in H as [a' [H3 H4]].
    apply (union_eq_2 _ x1) in H3.
    assert (Febag.union _ x x1 inSE FePmap.support _ (FePmap.transpo _ x1 (FePmap.coeff FePol x1 Pol_1) a)).
    * rewrite FePmap.transpo_supp.
      apply ((proj2 (Feset.mem_map _ _ _ _ _)) (ex_intro _ a' (conj H3 H4))).
    * apply FePmap.transpo_find in H;
        apply FePmap.coeff_spec_1 in H.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (mul_one_r_eq_bool _)).
      rewrite FePmap.singleton_coeff_eq.
      rewrite FePmap.singleton_coeff_eq in H.
      apply Oeset.eq_bool_true_compare_eq.
      now apply Oeset.compare_eq_refl_alt.
      now rewrite Oeset.eq_bool_sym.
      now rewrite Oeset.eq_bool_sym.
  - rewrite ! FePmap.coeff_spec_2; try solve [apply Oeset.eq_bool_refl|apply (proj1 (FePmap.find_spec_alt _ _ _) H)].
    apply FePmap.find_spec_alt.
    apply not_true_iff_false. apply not_true_iff_false in H.
    intro; destruct H.
    rewrite FePmap.transpo_supp in H2.
    apply Feset.mem_map in H2 as [a' [H2 H3]].
    apply Feset.in_elements_mem in H3.
    apply union_eq_2 in H2.
    now rewrite (Feset.mem_eq_1 _ _ _ _ H2).
Qed.


Lemma mul_Pol_0 :  forall a : Pol, eq OEPol (mul_Pol a Pol_0) Pol_0.
Proof.
  intros.
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_Pol_comm a Pol_0)).
  simpl. apply FePmap.compare_spec.
  apply FePmap.equal_spec.
  intro.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.mul_Pol_0 _ R_is_CSR _ _ _)).
  rewrite FePmap.coeff_Pol_0.
  apply Oeset.eq_bool_refl.
Qed.


Lemma mul_Pol_mono_aux : forall a b1 b2 : Pol, eq OEPol b1 b2 -> eq OEPol (mul_Pol a b1) (mul_Pol a b2).
Proof.
  intros.
  simpl.
  rewrite FePmap.compare_spec.
  rewrite FePmap.equal_spec; intro.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_Pol_equiv _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (mul_Pol_equiv _ _ _)).
  induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol a)).
  - apply Oeset.eq_bool_refl.
  - simpl.
    apply fold_sortir_l.
    fold_sortir_if.
    apply fold_sortir_r.
    fold_sortir_if.
  apply plus_compat_eq_bool.
    * apply FePmap.compare_spec in H.
      apply (FePmap.fold_left_equiv_s _ R_is_CSR _ _ _ _ H).
      intros.
      rewrite (union_eq_1_bis _ a0) in H0.
      apply (Oeset.eq_bool_eq_2 _ _ _ _ H0).
    * apply IHl.
Qed.
 


Lemma mul_Pol_mono : forall a1 a2 b1 b2 : Pol, eq OEPol a1 a2 -> eq OEPol b1 b2 -> eq OEPol (mul_Pol a1 b1) (mul_Pol a2 b2).
Proof.
  intros.
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_Pol_mono_aux _ _ _ H0)).
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_Pol_comm _ _)).
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_Pol_mono_aux _ _ _ H)).
  apply mul_Pol_comm.
Qed.


Lemma mul_Pol_assoc : forall a1 a2 a3 : Pol, eq OEPol (mul_Pol a1 (mul_Pol a2 a3)) (mul_Pol (mul_Pol a1 a2) a3).
Proof.
  intros.
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_Pol_comm _ _)).
  rewrite (Oeset.compare_eq_2 _ _ _ _ (mul_Pol_mono (mul_Pol a1 a2) (mul_Pol a2 a1) a3 a3 (mul_Pol_comm _ _) (Oeset.compare_eq_refl _ _))).
  simpl; rewrite FePmap.compare_spec.
  rewrite FePmap.equal_spec; intro.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_Pol_equiv_3 _ _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (mul_Pol_equiv_3 _ _ _ _)).
  apply (equiv_fold R_is_CSR); intros; try fold_sortir_2_if.
  apply fold_sortir_l.
  fold_sortir_if.
  apply fold_sortir_r.
  fold_sortir_if.
  apply plus_compat_r_eq_bool.
  apply (FePmap.mul_Pol_comm_aux _ R_is_CSR); intros; try apply plus_if_zero.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (union_assoc_comm _ _ _ _)).
  destruct (Oeset.eq_bool OEMon x ((e unionBE a) unionBE a0)); try apply Oeset.eq_bool_refl.
  apply plus_compat_l_eq_bool.
  apply mul_aux_1.  
Qed.


Lemma aux_1 : forall s1 s2 b (x0 : Mon) (a1 a2 : Pol),
  FePmap.equal FePol a1 a2 = true ->
  FePmap.equal FePol (add_Pol (FePmap.transpo FePol x0 (FePmap.coeff FePol x0 b) (add_Pol s1 s2)) a1)
    (add_Pol (add_Pol (FePmap.transpo FePol x0 (FePmap.coeff FePol x0 b) s1) (FePmap.transpo FePol x0 (FePmap.coeff FePol x0 b) s2)) a2) =
  true.
Proof.
  intros.
  rewrite FePmap.equal_spec; intro.
  apply (proj1 (FePmap.equal_spec _ _ _ )) with x in H.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
  apply plus_compat_eq_bool; try apply H.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (transpo_alt _ _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_eq_bool _ _ _ _ (transpo_alt _ _ _ _) (transpo_alt _ _ _ _))).
  destruct (Oeset.eq_bool OEMon x (Febag.diff BePV x x0 unionBE x0)).
  - rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_compat_l_eq_bool _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _))). apply mul_distr_plus_r_eq_bool.
  - rewrite Oeset.eq_bool_sym. apply plus_zero_l_eq_bool.
Qed.

  
Lemma mul_add_Pol_distr : forall a1 a2 b : Pol,
    eq OEPol (mul_Pol b (add_Pol a1 a2)) (add_Pol (mul_Pol b a1) (mul_Pol b a2)).
Proof.
  intros s1 s2 b.
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_Pol_comm _ _)).
  apply Oeset.compare_eq_sym.
  apply (Oeset.compare_eq_trans _ _ _ _ (add_Pol_mono _ _ _ _(mul_Pol_comm _ _) (mul_Pol_comm _ _)) ).
  simpl. rewrite FePmap.compare_spec.
  rewrite FePmap.equal_spec; intro.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
  rewrite ! FePmap.mul_Pol_spec.
  rewrite ! FePmap.fold_spec.
  rewrite ! Feset.fold_spec.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (FePmap.fold_left_equiv_2 _ _ _ b (FePmap.empty FePol) x (aux_1 _ _ _))).
  induction (Feset.elements (FePmap.FeElt1 FePol) (FePmap.support FePol b)); simpl.
  - rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_l_eq_bool _ _ _ (FePmap.coeff_empty _ _))).
    apply plus_zero_l_eq_bool.
  - apply (FePmap.fold_sortir_coeff_r _ R_is_CSR); try solve [intros; apply (FePmap.coeff_add_Pol_unfold _ R_is_CSR)].
    apply (FePmap.fold_sortir_coeff_l_plus_l _ R_is_CSR); try solve [intros; apply (FePmap.coeff_add_Pol_unfold _ R_is_CSR)].
    apply (FePmap.fold_sortir_coeff_l_plus_r _ R_is_CSR); try solve [intros; apply (FePmap.coeff_add_Pol_unfold _ R_is_CSR)].
    rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_aux_3 _ _ _ _)).
    apply plus_compat_eq_bool; try apply IHl.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (FePmap.coeff_empty _ _))).
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_zero_r_eq_bool _)).
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _)).
    apply plus_compat_eq_bool;
      rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (FePmap.coeff_union_eq _ R_is_CSR _ _ _));
      rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (FePmap.coeff_empty _ _)));
      apply plus_zero_r_eq_bool.
Qed.



Lemma R_pol_is_CSR : CSR Pol_0 Pol_1 add_Pol mul_Pol OEPol.
Proof.
  split.
  - apply Pol0_Pol1_diff.
  - split.
    * apply add_Pol_assoc.
    * apply add_Pol_0.
    * apply add_Pol_comm.
    * apply add_Pol_mono.
  - split.
    * apply mul_Pol_assoc.
    * apply mul_Pol_1.
    * apply mul_Pol_comm.
    * apply mul_Pol_mono.
  - apply mul_add_Pol_distr.
  - apply mul_Pol_0.      
Qed.

End Pol.
