(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Export Bool.
Require Import List NArith.
Require Import OrderedSet FiniteSet.


Notation t_compare := (Oeset.compare).
Notation eq TA A B := (((t_compare TA) A B) = Eq).

(*List*)

Lemma nb_occ_nil : forall A (OA : Oeset.Rcd A) (l : list A),
    (forall x, Oeset.nb_occ OA x l = 0%N) ->
    l = nil.
Proof.
  intros.
  destruct l; trivial.
  specialize (H a).
  rewrite Oeset.nb_occ_unfold, Oeset.compare_eq_refl in H.
  rewrite N.one_succ in H.
  rewrite N.add_succ_l in H.
  apply N.succ_0_discr in H.
  inversion H.
Qed.

Lemma list_not_nil : forall A (l : list A),
    l <> nil -> exists a l', l = a :: l'.
Proof.
intros A l; case l; intros.
destruct H; trivial.
exists a, l0; trivial.
Qed.


Lemma le_plus_n : forall (n m p : nat),
n + m <= p -> n <= p /\ m <= p.
Proof.
  split; [> apply (PeanoNat.Nat.add_le_mono_r _ _ m)| apply (PeanoNat.Nat.add_le_mono_r _ _ n); rewrite PeanoNat.Nat.add_comm]; apply (PeanoNat.Nat.le_trans _ _ _ H (PeanoNat.Nat.le_add_r _ _)).
Qed.

Definition TN := Oset.oeset_of_oset ON.

Lemma eq_bool_true_N : forall a,
    Oeset.eq_bool TN a 0%N = true <-> a = 0%N.
Proof.
  intros; split; intros.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H; trivial.
  rewrite H.
  apply Oeset.eq_bool_refl.
Qed.

Lemma eq_bool_false_N : forall a,
    Oeset.eq_bool TN a 0%N = false <-> a <> 0%N.
Proof.
  intros.
  intros; split; intros H;[>apply not_true_iff_false in H|apply not_true_is_false]; intro H0; destruct H; apply eq_bool_true_N; trivial.
Qed.
 
Lemma Nadd_1_diff_0 : forall a, ((1 + a) <> 0)%N.
Proof.
intros.
rewrite N.add_1_l.
apply N.succ_0_discr.
Qed.


Lemma Fset_equal_trans : forall r (R: Oset.Rcd r) (r' : Fset.Rcd R) (s1 s2 s3 : Fset.set r'),
s1 =S= s2 -> s2 =S= s3 -> s1 =S= s3.
Proof.
intros.
apply Fset.compare_spec.
apply Fset.compare_eq_trans with s2; apply Fset.compare_spec; trivial.
Qed.



Lemma Oeset_compare_lt_gt : forall A (OA : Oeset.Rcd A) (e1 e2 : A),
Oeset.compare OA e1 e2 = Lt <-> Oeset.compare OA e2 e1 = Gt.
Proof.
intros; split; intros;
rewrite Oeset.compare_lt_gt;
rewrite H; trivial.
Qed.

Lemma eq_bool_false : forall A (r: Oeset.Rcd A) a b,
    Oeset.eq_bool r a b = false <-> Oeset.compare r a b <> Eq.
Proof.
  intros.
  intros; split; intros H;[>apply not_true_iff_false in H|apply not_true_is_false]; intro H0; destruct H; apply Oeset.eq_bool_true_compare_eq; trivial.
Qed.



Lemma in_feset_elements : forall A (OA : Oeset.Rcd A) (FeA : Feset.Rcd OA) x s,
    In x (Feset.elements FeA s) ->
    exists l1 l2,
      Feset.elements FeA s = l1 ++ x ::l2
      /\ forall x', In x' (l1 ++ l2) -> Oeset.eq_bool OA x x' = false.
Proof.
  intros.
  set (Feset.nb_occ_alt FeA x s).
  rewrite Feset.nb_occ_unfold in e. 
  rewrite (Feset.in_elements_mem _ _ _ H) in e.
  apply in_split in H as [l1 [l2 H]].
  rewrite H, Oeset.nb_occ_app, (Oeset.nb_occ_unfold _ _ (x::l2)), Oeset.compare_eq_refl, N.one_succ, N.add_succ_l, N.add_succ_r in e.
  apply N.succ_inj in e.
  apply N.eq_add_0 in e as [Ha Hb]; apply Oeset.nb_occ_not_mem in Ha.
  apply N.eq_add_0 in Hb as [_ Hb]; apply Oeset.nb_occ_not_mem in Hb.
  exists l1. exists l2. split; trivial.
  intros; apply in_app_or in H0; destruct H0; apply in_split in H0 as [l3 [l4 H0]].
  - rewrite H0 in Ha.
  rewrite Oeset.mem_bool_app in Ha.
  rewrite (Oeset.mem_bool_unfold _ _ (x'::l4)) in Ha.
  apply orb_false_iff in Ha as [_ Ha];
  now apply orb_false_iff in Ha as [Ha _].
  - rewrite H0 in Hb.
  rewrite Oeset.mem_bool_app in Hb.
  rewrite (Oeset.mem_bool_unfold _ _ (x'::l4)) in Hb.
  apply orb_false_iff in Hb as [_ Hb];
  now apply orb_false_iff in Hb as [Hb _].
Qed.



Section fold_equiv_aux. 
Require Import SemiRing.
Hypothesis R : Type.
Hypothesis zero : R.
Hypothesis one : R.
Hypothesis plus : R -> R -> R.
Hypothesis mul : R -> R -> R.
Hypothesis TR : Oeset.Rcd R.
Hypothesis R_is_CSR : CSR zero one plus mul TR.
Notation plus_compat_eq_bool := (plus_compat_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_l_eq_bool := (plus_compat_l_eq_bool (isCM_plus_SR R_is_CSR)).
Notation mul_compat_r_eq_bool := (SemiRing.plus_compat_r_eq_bool (isCM_mul_SR R_is_CSR)).
Notation plus_aux_2 := (plus_aux_2_d (isCM_plus_SR R_is_CSR)).
Notation plus_if_zero := (plus_if_zero (isCM_plus_SR R_is_CSR)).

Notation fold_sortir_l := (fold_sortir_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r := (fold_sortir_r (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_bis := (fold_sortir_l_bis (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_bis := (fold_sortir_r_bis (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_plus_l := (fold_sortir_l_plus_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_plus_l := (fold_sortir_r_plus_l (isCM_plus_SR R_is_CSR)).


Lemma eq_fold_mem_eq : forall A (OA : Oeset.Rcd A) (f: A -> bool) fmul1 fmul2 l1 l2 a b,
    (forall e1 e2, Oeset.eq_bool OA e1 e2 = true -> f e1 = f e2) ->
    (forall x, Oeset.nb_occ OA x l1 = (Oeset.nb_occ OA x l2)) ->
    (forall e1 e2, Oeset.eq_bool OA e1 e2 = true ->
                   Oeset.eq_bool TR (fmul1 e2) (fmul2 e1) = true) ->
    Oeset.eq_bool TR
    (fold_left
       (fun (a2 : R) (e : A) =>
          if f e then plus (mul a (fmul1 e)) a2 else a2)
       l1 b)
    (fold_left
       (fun (a2 : R) (e : A) =>
          if f e then plus (mul a (fmul2 e)) a2 else a2)
       l2 b) = true.
Proof.
  intros; revert H0; revert l1; induction l2; intros.
  - rewrite (nb_occ_nil OA l1); [apply Oeset.eq_bool_refl|trivial].
  - assert (Oeset.mem_bool OA a0 l1 = true).
    * apply Oeset.nb_occ_mem.
      rewrite H0. apply Oeset.in_nb_occ. now left.
    * apply Oeset.mem_bool_true_iff in H2 as [a' [H2 H3]].
      apply in_split in H3 as [l1a [l1b H3]].
      rewrite H3.
      rewrite fold_left_app.
      apply fold_sortir_l.
      intros; apply plus_if_zero.
      apply fold_sortir_r.
      intros; apply plus_if_zero.
      simpl.
      apply fold_sortir_l_plus_l.
      intros; apply plus_if_zero.
      apply fold_sortir_r_plus_l.
      intros; apply plus_if_zero.
      rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_aux_2 _ _ _)).
      rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_aux_2 _ _ _)).
      apply plus_compat_eq_bool.
      -- apply Oeset.eq_bool_true_compare_eq in H2.
         rewrite (H _ _ H2).
         destruct (f a'); try apply Oeset.eq_bool_refl.
         apply plus_compat_l_eq_bool.
         apply mul_compat_r_eq_bool.
         apply (H1 _ _ H2).
      -- apply fold_sortir_l_bis.
         intros; apply plus_if_zero.
         rewrite <- fold_left_app.
         apply fold_sortir_r_bis.
         intros; apply plus_if_zero.
         apply (IHl2 (l1a++l1b)).
         intros. rewrite H3 in H0.
         specialize (H0 x).
         rewrite Oeset.nb_occ_app in H0.
         rewrite (Oeset.nb_occ_unfold _ _ (a0::l2)) in H0.
         rewrite (Oeset.nb_occ_unfold _ _ (a'::l1b)) in H0.
         rewrite (Oeset.compare_eq_2 _ _ _ _ H2) in H0.
         rewrite N.add_shuffle3 in H0.
         rewrite <- Oeset.nb_occ_app in H0.
         apply (Nplus_reg_l _ _ _ H0).
Qed.
End fold_equiv_aux.


Section union_aux.
Require Import FiniteBag.
Hypothesis A : Set.
Hypothesis OA : Oeset.Rcd A.
Hypothesis BA : Febag.Rcd OA.
Definition bA := (Febag.bag BA).
Definition OBA : (Oeset.Rcd bA).
  split with (Febag.compare BA).
  - apply Febag.compare_eq_trans.
  - apply Febag.compare_eq_lt_trans.
  - apply Febag.compare_lt_eq_trans.
  - apply Febag.compare_lt_trans.
  - apply Febag.compare_lt_gt.
Defined.

Lemma empty_eq : forall x1 x2,
    Oeset.eq_bool OBA x1 (emptysetBE) = true ->
    Oeset.eq_bool OBA x2 (x2 unionBE x1) = true.
Proof.
  intros; apply Oeset.eq_bool_true_compare_eq; apply Febag.compare_spec;
  apply Febag.nb_occ_equal;
  intro.
  rewrite Febag.nb_occ_union.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply Febag.compare_spec in H;
    rewrite (Febag.nb_occ_eq_2 _ _ _ H).
  rewrite Febag.nb_occ_empty.
  now rewrite N.add_0_r.
Qed.


Lemma union_comm : forall x1 x2,
   eq OBA (x1 unionBE x2) (x2 unionBE x1).
Proof.
  intros; apply Febag.compare_spec;
  apply Febag.nb_occ_equal;
  intro. 
  rewrite 2 Febag.nb_occ_union.
  apply N.add_comm.
Qed.

Lemma union_comm_eq_bool : forall x1 x2,
   Oeset.eq_bool OBA (x1 unionBE x2) (x2 unionBE x1) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply union_comm.
Qed.

Lemma union_eq : forall x1 x2 x3,
    eq OBA x2 x3 <-> eq OBA (x1 unionBE x2) (x1 unionBE x3).
Proof.
  split; intro; apply Febag.compare_spec;
  apply Febag.nb_occ_equal;
  intro;
  apply Febag.compare_spec in H;
  apply (proj1 (Febag.nb_occ_equal _ _ _)) with e in H. 
  rewrite 2 Febag.nb_occ_union. now rewrite H.
  rewrite 2 Febag.nb_occ_union in H.
  apply (Nplus_reg_l _ _ _ H).
Qed.

Lemma union_eq_2 : forall x1 x2 x3,
    eq OBA x2 x3 <-> eq OBA (x2 unionBE x1) (x3 unionBE x1).
Proof.
  split;
  rewrite (Oeset.compare_eq_1 _ _ _ _ (union_comm _ _));
  rewrite (Oeset.compare_eq_2 _ _ _ _ (union_comm _ _)); intro.
  apply (proj1 (union_eq _ _ _) H).
  apply (proj2 (union_eq _ _ _) H).
Qed.

Lemma union_eq_1_bis : forall x1 x2 x3,
    Oeset.eq_bool OBA x2 x3 = Oeset.eq_bool OBA (x1 unionBE x2) (x1 unionBE x3).
Proof.
  intros.
  case_eq (Oeset.eq_bool OBA (x1 unionBE x2) (x1 unionBE x3)); intro.
  apply Oeset.eq_bool_true_compare_eq in H;
  apply Oeset.eq_bool_true_compare_eq.
  apply (proj2 (union_eq _ _ _) H).
  apply not_true_is_false; apply not_true_iff_false in H.
  intro; destruct H.
  apply Oeset.eq_bool_true_compare_eq in H0;
  apply Oeset.eq_bool_true_compare_eq.
  apply (proj1 (union_eq _ _ _) H0).
Qed.
Lemma union_eq_2_bis : forall x1 x2 x3,
    Oeset.eq_bool OBA x2 x3 = Oeset.eq_bool OBA (x2 unionBE x1) (x3 unionBE x1).
Proof.
  intros.
  case_eq (Oeset.eq_bool OBA (x2 unionBE x1) (x3 unionBE x1)); intro.
  apply Oeset.eq_bool_true_compare_eq in H;
  apply Oeset.eq_bool_true_compare_eq.
  apply (proj2 (union_eq_2 _ _ _) H).
  apply not_true_is_false; apply not_true_iff_false in H.
  intro; destruct H.
  apply Oeset.eq_bool_true_compare_eq in H0;
  apply Oeset.eq_bool_true_compare_eq.
  apply (proj1 (union_eq_2 _ _ _) H0).
Qed.

Lemma union_assoc : forall x1 x2 x3,
    eq OBA ((x1 unionBE x2) unionBE x3) (x1 unionBE (x2 unionBE x3)).
Proof.
  intros; apply Febag.compare_spec;
  apply Febag.nb_occ_equal;
  intro.
  rewrite ! Febag.nb_occ_union. symmetry. apply N.add_assoc.
Qed.

Lemma union_assoc_comm : forall x1 x2 x3,
    Oeset.eq_bool OBA ((x1 unionBE x2) unionBE x3) ((x1 unionBE x3) unionBE x2) = true.
Proof.
  intros; apply Oeset.eq_bool_true_compare_eq.
  rewrite (Oeset.compare_eq_1 _ _ _ _ (union_assoc _ _ _)).
  rewrite (Oeset.compare_eq_2 _ _ _ _ (union_assoc _ _ _)).
  apply union_eq.
  apply union_comm.
Qed.



Lemma aux_6 : forall x a,
    Oeset.eq_bool OBA x (Febag.diff BA x a unionBE a) = false ->
    forall e, Oeset.eq_bool OBA x (e unionBE a) = false.
Proof.
  intros.
  apply not_true_iff_false.
  apply not_true_iff_false in H.
  intro; destruct H.
  apply Oeset.eq_bool_true_compare_eq.
  apply Oeset.eq_bool_true_compare_eq in H0.
  apply Febag.compare_spec.
  apply Febag.compare_spec in H0.
  apply Febag.nb_occ_equal. intro.
  apply (proj1 (Febag.nb_occ_equal _ _ _)) with e0 in H0.
  now rewrite Febag.nb_occ_union, Febag.nb_occ_diff, H0, ! Febag.nb_occ_union, N.add_sub.
Qed.

Lemma aux_3 : forall (FBA : Feset.Rcd OBA),
    exists x1,
    Feset.elements FBA (Feset.singleton FBA (emptysetBE)) =
    x1 :: nil /\ Oeset.eq_bool OBA x1 (emptysetBE) = true.
Proof.
  intro; destruct (Feset.choose_singleton FBA (emptysetBE)) as [x1 [H0 H1]].
  apply Feset.choose_spec1 in H0.
  rewrite Feset.mem_elements in H0.
  pose (Feset.cardinal_singleton FBA (emptysetBE)).
  rewrite Feset.cardinal_spec in e.
  destruct (Feset.elements FBA (Feset.singleton FBA (emptysetBE))); inversion e.
  apply length_zero_iff_nil in H2; rewrite H2.
  exists b; split; trivial.
  rewrite H2 in H0.
  simpl in H0;
  rewrite orb_false_r in H0.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  apply Oeset.eq_bool_true_compare_eq; now apply Oeset.compare_eq_sym.
Qed.

Lemma elements_singleton : forall (FBA : Feset.Rcd OBA) y,
    exists x1,
    Feset.elements FBA (Feset.singleton FBA y) =
    x1 :: nil /\ Oeset.eq_bool OBA x1 y = true.
Proof.
  intros; destruct (Feset.choose_singleton FBA y) as [x1 [H0 H1]].
  apply Feset.choose_spec1 in H0.
  rewrite Feset.mem_elements in H0.
  pose (Feset.cardinal_singleton FBA y).
  rewrite Feset.cardinal_spec in e.
  destruct (Feset.elements FBA (Feset.singleton FBA y)); inversion e.
  apply length_zero_iff_nil in H2; rewrite H2.
  exists b; split; trivial.
  rewrite H2 in H0.
  simpl in H0;
  rewrite orb_false_r in H0.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  apply Oeset.eq_bool_true_compare_eq; now apply Oeset.compare_eq_sym.
Qed.

Require Import SemiRing.
Hypothesis R : Type.
Hypothesis zero : R.
Hypothesis one : R.
Hypothesis plus : R -> R -> R.
Hypothesis mul : R -> R -> R.
Hypothesis TR : Oeset.Rcd R.
Hypothesis R_is_CSR : CSR zero one plus mul TR.

Notation plus_zero_r_eq_bool := (plus_zero_r_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_eq_bool := (plus_compat_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_l_eq_bool := (plus_compat_l_eq_bool (isCM_plus_SR R_is_CSR)).
Notation plus_compat_r_eq_bool := (plus_compat_r_eq_bool (isCM_plus_SR R_is_CSR)).

Notation mul_compat_l_eq_bool := (SemiRing.plus_compat_l_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_compat_r_eq_bool := (SemiRing.plus_compat_r_eq_bool (isCM_mul_SR R_is_CSR)).
Notation mul_distr_plus_r_eq_bool := (mul_distr_plus_r_eq_bool R_is_CSR).

Notation plus_aux_2 := (plus_aux_2_d (isCM_plus_SR R_is_CSR)).
Notation plus_if_zero := (plus_if_zero (isCM_plus_SR R_is_CSR)).

Notation fold_sortir := (fold_sortir (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l := (fold_sortir_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r := (fold_sortir_r (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_bis := (fold_sortir_l_bis (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_bis := (fold_sortir_r_bis (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_l_plus_l := (fold_sortir_l_plus_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_r_plus_l := (fold_sortir_r_plus_l (isCM_plus_SR R_is_CSR)).
Notation fold_sortir_mul := (fold_sortir_mul R_is_CSR).
Ltac fold_sortir_if := intros; apply fold_sortir; intros; apply plus_if_zero.

Lemma aux_5 : forall A (f : A -> A -> bool) b g l1 l2,
  Oeset.eq_bool TR
  (fold_left
       (fun (a1 : R) (e1 : A) =>
        fold_left
          (fun (a2 : R) (e2 : A) =>
           if f e1 e2
           then plus (mul (g e1 e2) b) a2
           else a2) l2 a1)
       l1 zero)
    (mul
       (fold_left
          (fun (a1 : R) (e1 : A) =>
           fold_left
             (fun (a2 : R) (e2 : A) =>
              if f e1 e2
              then plus (g e1 e2) a2
              else a2) l2 a1)
          l1 zero)
       b)
  = true.
Proof.
  intros; revert l2; induction l1; intros; try solve[rewrite Oeset.eq_bool_sym; apply (mul_zero_l_eq_bool R_is_CSR)].
  simpl.
  apply fold_sortir_l.
  fold_sortir_if.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl1 _))).
  apply (fold_sortir_r_mul_r R_is_CSR).
  fold_sortir_if.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (mul_distr_plus_r_eq_bool _ _ _)).
  apply plus_compat_l_eq_bool.
  apply fold_sortir_mul.
Qed.

End union_aux.

Require Import FiniteBag.

Lemma not_empty_exists: forall A (r: Oeset.Rcd A) (f: Febag.Rcd r) b,
Febag.is_empty f b = false ->
exists t, Febag.nb_occ f t b <>0%N.
Proof.
  intros.
  apply not_true_iff_false in H.
  assert ({Febag.elements f b = nil} + {Febag.elements f b <> nil}) as [H0|H0].
  - case (Febag.elements f b);
    [>left; trivial| right; discriminate].
  - destruct H.
  rewrite Febag.is_empty_spec.
  apply Febag.nb_occ_equal.
  intros.
  rewrite Febag.nb_occ_elements. 
  rewrite H0. symmetry. apply Febag.nb_occ_empty.
  - apply list_not_nil in H0.
  destruct H0 as [a [l0 H0]].
  exists a.
  rewrite Febag.nb_occ_elements.
  rewrite H0; simpl. rewrite Oeset.compare_eq_refl.
  rewrite N.add_1_l.
  apply N.succ_0_discr.
Qed.

Require Import Sorted.
Lemma lt_feset_elt_aux: forall elt1 (Elt1 : Oeset.Rcd elt1) a l2,
    Sorted (fun x y : elt1 => lt Elt1 x y) (a :: l2) ->
    forall x, Oeset.mem_bool Elt1 x l2 = true -> lt Elt1 a x.
Proof.
  intros.
  apply Sorted_extends in H.
  apply Oeset.mem_bool_true_iff in H0 as [x' [H0 H1]].
  apply (Oeset.compare_lt_eq_trans _ a x' x); trivial.
  now apply (proj1 (Forall_forall _ _)) with x' in H.
  apply Oeset.compare_eq_sym; trivial.
  unfold Relations_1.Transitive.
  apply Oeset.compare_lt_trans.
Qed.


Lemma lt_feset_elt : forall elt1 (Elt1 : Oeset.Rcd elt1) (FElt1 : Feset.Rcd Elt1) a1 l1 a l2, Feset.elements FElt1 a1 = l1++ a::l2 -> forall x, Oeset.mem_bool Elt1 x l2 = true -> lt Elt1 a x.
Proof.
  intros elt1 Elt1 FElt1 a1 l1 a l2 H.
  set (Feset.elements_spec3 FElt1 a1).
  rewrite H in s.
  clear H.
  induction l1.
  apply lt_feset_elt_aux. trivial.
  simpl in s.
  apply Sorted_inv in s as [H _].
  now apply IHl1.
Qed.
