(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Relations.

Require Import Bool.

Require Import List FlatData Facts OrderedSet SemiRing FiniteSet FiniteMap.

Definition Tbool := Oset.oeset_of_oset Obool.


Section bool_f.
  Hypothesis X : Type.
  Hypothesis TX : Oeset.Rcd X.
  Hypothesis mulX : X -> X -> X.
  Hypothesis TX_is_CIO : CIO mulX TX.


  
  Hypothesis FeXB : (FePmap.Rcd Tbool false orb andb TX_is_CIO).
  Notation XB := (FePmap.pmap FeXB).
  
  Definition TXB : Oeset.Rcd XB.
    split with (FePmap.compare FeXB);
      intros.
    - apply (FePmap.compare_eq_trans _ _ _ _ H H0).
    - apply (FePmap.compare_eq_lt_trans _ _ _ _ H H0).
    - apply (FePmap.compare_lt_eq_trans _ _ _ _ H H0).
    - apply (FePmap.compare_lt_trans _ _ _ _ H H0).
    - apply FePmap.compare_lt_gt.
  Defined.
  
  Definition mul_XB := FePmap.union FeXB.
  
  Hypothesis FeXBB : (Femap2.Rcd TXB Tbool false).
  Notation XBB := (Femap2.map2 FeXBB).
  
  Fixpoint mk_fm2 l f :=
    match l with
    |nil => Femap2.empty FeXBB
    |x::tl => Femap2.add_elt FeXBB x (f x) (mk_fm2 tl f)
    end.

  Lemma Ev_mk_fm2 : forall Ev f,
      Femap2.support FeXBB (mk_fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev) f) =SE= Ev.
  Proof.
    intros.
    apply Feset.equal_spec; intro.
    case_eq (e inSE? Ev); intros.
    rewrite Feset.mem_elements in H.
    apply Oeset.mem_bool_true_iff in H as [a [H0 H1]].
    apply in_split in H1 as [l1 [l2 H1]].
    rewrite H1.
    clear H1.
    induction l1.
    simpl.
    rewrite Femap2.add_elt_supp.
    rewrite Feset.add_spec.
    apply Oeset.eq_bool_true_compare_eq in H0.
    now rewrite H0.  
    simpl.
    rewrite Femap2.add_elt_supp.
    rewrite Feset.add_spec.
    rewrite IHl1.
    apply orb_true_r.
    assert (forall x, In x (Feset.elements (Femap2.FeElt1 FeXBB) Ev) -> Oeset.eq_bool TXB e x = false).
    intros. apply (Oeset.in_mem_bool TXB) in H0.
    rewrite Feset.mem_elements in H.
    apply not_true_iff_false.
    apply not_true_iff_false in H.
    intro. destruct H.
    rewrite Oeset.eq_bool_sym in H1.
    apply Oeset.eq_bool_true_compare_eq in H1.  
    now rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H1) in H0.
    clear H.
    induction (Feset.elements (Femap2.FeElt1 FeXBB) Ev).
    simpl.
    rewrite Femap2.empty_supp.
    apply Feset.mem_empty.
    simpl.
    rewrite Femap2.add_elt_supp.
    rewrite Feset.add_spec.
    rewrite IHl.
    now rewrite (H0 a (in_eq _ _)).
    intros.
    apply H0. now right.
  Qed.  

  Lemma mk_fm_supp_mem : forall x l f,
      x inSE? Femap2.support FeXBB (mk_fm2 l f) = (Oeset.mem_bool TXB x l).
  Proof.
    intros.
    induction l; simpl.
    rewrite Femap2.empty_supp.
    apply Feset.mem_empty.
    rewrite Femap2.add_elt_supp.
    rewrite Feset.add_spec.
    now rewrite IHl.
  Qed.

 
  Lemma mk_fm_coeff : forall Ev f x,
      (forall a b, Oeset.eq_bool TXB a b = true -> f a = f b) -> 
      Femap2.coeff FeXBB x (mk_fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev) f) =
      if (x inSE? Ev) then f x else false.
  Proof.
    intros.
    rewrite Feset.mem_elements.
    induction (Feset.elements (Femap2.FeElt1 FeXBB) Ev); simpl.
    - apply Femap2.coeff_spec_2.
      apply Femap2.find_spec_alt.
      rewrite Femap2.empty_supp.
      apply Feset.mem_empty.
    - case_eq (Oeset.eq_bool TXB x a); intros.
      * rewrite (Femap2.coeff_eq _ _ _ _ H0).
        simpl.
        apply Femap2.coeff_spec_1.
        rewrite (H _ _ H0).
        apply Femap2.add_elt_find_2.
      * case_eq (x inSE? (Femap2.support _ (Femap2.add_elt FeXBB a (f a) (mk_fm2 l f)))); intros.
        -- simpl. rewrite <- IHl.
           apply Femap2.coeff_spec_1.
           rewrite Femap2.add_elt_find_1; trivial.
           rewrite Femap2.add_elt_supp in H1.
           rewrite Feset.add_spec in H1.
           rewrite H0 in H1.
           simpl in H1.
           apply Femap2.find_spec_1 in H1 as [y H1].
           rewrite H1.
           now rewrite (Femap2.coeff_spec_1 _ _ _ H1).
           now rewrite Oeset.eq_bool_sym.
        -- case_eq (Oeset.mem_bool TXB x l); intros.
           rewrite Femap2.add_elt_supp in H1.
           rewrite Feset.add_spec in H1.
           apply orb_false_iff in H1 as [_ H1].
           rewrite mk_fm_supp_mem in H1.
           rewrite H2 in H1; inversion H1.
           simpl.
           rewrite ! Femap2.coeff_spec_2; trivial.
           now  apply Femap2.find_spec_alt.
  Qed.

  (* zero *)
  Definition zero Ev := mk_fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev) (fun x => false).

  Lemma zero_supp : forall Ev,
      Femap2.support FeXBB (zero Ev) =SE= Ev.
  Proof.
    intros.
    unfold zero.
    apply Ev_mk_fm2.
  Qed.

  Lemma zero_coeff : forall x Ev,
      Femap2.coeff FeXBB x (zero Ev) = false.
  Proof.
    intros.
    unfold zero.
    rewrite mk_fm_coeff.
    apply BasicFacts.if_same.
    intros. apply eq_refl.
  Qed.

  Definition one Ev := mk_fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev) (fun x => true).

  Lemma one_supp : forall Ev,
      Femap2.support FeXBB (one Ev) =SE= Ev.
  Proof.
    intros.
    unfold one.
    apply Ev_mk_fm2.
  Qed.

  Lemma one_coeff : forall x Ev,
      Femap2.coeff FeXBB x (one Ev) = (x inSE? Ev).
  Proof.
    intros.
    unfold one.
    rewrite mk_fm_coeff.
    now destruct (x inSE? Ev).
    intros; apply eq_refl.
  Qed.

  (* plus *)  
  Definition plus Ev fm1 fm2 :=
    mk_fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev)
           (fun x => orb (Femap2.coeff FeXBB x fm1) (Femap2.coeff FeXBB x fm2)).

  Lemma plus_supp : forall Ev a1 a2,
      Femap2.support FeXBB (plus Ev a1 a2) =SE= Ev.
  Proof.
    intros.
    unfold plus.
    apply Ev_mk_fm2.
  Qed.

  Lemma plus_coeff : forall x Ev a1 a2,
      Femap2.coeff FeXBB x (plus Ev a1 a2) =
      if (x inSE? Ev) then Femap2.coeff FeXBB x a1 || Femap2.coeff FeXBB x a2
      else false.
  Proof.
    intros.
    unfold plus.
    apply mk_fm_coeff.
    intros. now rewrite ! (Femap2.coeff_eq _ _ _ _ H).
  Qed.



  
  Definition mul Ev fm1 fm2 :=
    mk_fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev)
                (fun x => andb (Femap2.coeff FeXBB x fm1) (Femap2.coeff FeXBB x fm2)).
    
  Lemma mul_supp : forall Ev a1 a2,
      Femap2.support FeXBB (mul Ev a1 a2) =SE= Ev.
  Proof.
    intros.
    unfold mul.
    apply Ev_mk_fm2.
  Qed.


  Lemma mul_coeff : forall x Ev a1 a2,
      Femap2.coeff FeXBB x (mul Ev a1 a2) =
      if (x inSE? Ev) then Femap2.coeff FeXBB x a1 && Femap2.coeff FeXBB x a2
      else false.
  Proof.
    intros.
    unfold mul.
    apply mk_fm_coeff.
    intros. now rewrite ! (Femap2.coeff_eq _ _ _ _ H).
  Qed.

  Fixpoint new_compare_aux fm1 fm2 l :=
    match l with
    |nil => Eq
    |x :: t =>
     match (new_compare_aux fm1 fm2 t) with
     |Eq => Oeset.compare Tbool (Femap2.coeff FeXBB x fm1) (Femap2.coeff FeXBB x fm2)
     |y => y 
     end
    end.

  
  Definition new_compare Ev fm1 fm2 :=
    new_compare_aux fm1 fm2 (Feset.elements (Femap2.FeElt1 FeXBB) Ev).

  Lemma new_compare_aux_app_2 : forall l1 l2 a1 a2,
      new_compare_aux a1 a2 (l1 ++ l2) = Eq -> new_compare_aux a1 a2 l2 = Eq.
  Proof.
    induction l1; intros; trivial.
    apply IHl1.
    simpl in H.
    destruct (new_compare_aux a1 a2 (l1 ++ l2)); trivial.
  Qed.
  
  Lemma new_compare_aux_app_1 : forall l1 l2 a1 a2,
      new_compare_aux a1 a2 (l1 ++ l2) = Eq -> new_compare_aux a1 a2 l1 = Eq.
  Proof.
    induction l1; intros; trivial.
    simpl.
    simpl in H.
    case_eq (new_compare_aux a1 a2 (l1 ++ l2)); intros; rewrite H0 in H; try solve [inversion H].
    apply IHl1 in H0.
    now rewrite H0.
  Qed.
 
  Lemma new_compare_aux_eq : forall l a1 a2,
      new_compare_aux a1 a2 l = Eq <-> (forall x, Oeset.mem_bool TXB x l = true -> Oeset.compare Tbool (Femap2.coeff FeXBB x a1) (Femap2.coeff FeXBB x a2) = Eq).
  Proof.
    split; intros.
    - apply Oeset.mem_bool_true_iff in H0 as [a [H0 H1]].
    apply in_split in H1 as [l1 [l2 H1]].
    rewrite H1 in H.
    apply new_compare_aux_app_2 in H.
    rewrite ListFacts.cons_app_nil in H.
    apply new_compare_aux_app_1 in H.
    apply Oeset.eq_bool_true_compare_eq in H0.
    now rewrite ! (Femap2.coeff_eq _ _ _ _ H0).
    - induction l; trivial.
    simpl.
    simpl in H.
    rewrite (H a).
    rewrite IHl; trivial.
    intros.
    simpl.
    apply (H x).
    rewrite H0; apply orb_true_r.
    now rewrite Oeset.eq_bool_refl.
  Qed.
  
  Lemma new_compare_eq : forall Ev a1 a2,
      new_compare Ev a1 a2 = Eq <-> (forall x, Oeset.mem_bool TXB x (Feset.elements (Femap2.FeElt1 FeXBB) Ev) = true -> Oeset.compare Tbool (Femap2.coeff FeXBB x a1) (Femap2.coeff FeXBB x a2) = Eq).
  Proof.
    intros; apply new_compare_aux_eq.
  Qed.

  Lemma new_compare_lt : forall l a1 a2,
      new_compare_aux a1 a2 l = Lt <->
      exists l1 l2 y,
        l = l1 ++ y :: l2 /\
        lt Tbool (Femap2.coeff FeXBB y a1) (Femap2.coeff FeXBB y a2)/\
        (forall x, Oeset.mem_bool TXB x l2 = true ->
                  eq Tbool (Femap2.coeff FeXBB x a1) (Femap2.coeff FeXBB x a2)).
  Proof.
    split; intros.
    - induction l.
    inversion H.
    simpl in H.
      case_eq (new_compare_aux a1 a2 l); intros; rewrite H0 in H; try solve [inversion H].
      * exists nil; exists l; exists a; repeat split; trivial.
        intros.
        now apply (proj1 (new_compare_aux_eq _ _ _)) with x in H0.
      * apply IHl in H0 as [l1 [l2 [y [H1 [H2 H3]]]]].
        rewrite H1.
        exists (a::l1); exists l2; exists y; repeat split; trivial.
    - destruct H as [l1 [l2 [y [H1 [H2 H3]]]]].
      rewrite H1.
      apply new_compare_aux_eq in H3.
      clear H1.
      induction l1; simpl.
      now rewrite H3.
      now rewrite IHl1.
  Qed.
        
      
  Lemma new_compare_gt : forall l a1 a2,
      new_compare_aux a1 a2 l = Gt <->
      exists l1 l2 y,
        l = l1 ++ y :: l2 /\
        gt Tbool (Femap2.coeff FeXBB y a1) (Femap2.coeff FeXBB y a2)/\
        (forall x, Oeset.mem_bool TXB x l2 = true ->
                  eq Tbool (Femap2.coeff FeXBB x a1) (Femap2.coeff FeXBB x a2)).
  Proof.
    split; intros.
    - induction l.
    inversion H.
    simpl in H.
      case_eq (new_compare_aux a1 a2 l); intros; rewrite H0 in H; try solve [inversion H].
      * exists nil; exists l; exists a; repeat split; trivial.
        intros.
        now apply (proj1 (new_compare_aux_eq _ _ _)) with x in H0.
      * apply IHl in H0 as [l1 [l2 [y [H1 [H2 H3]]]]].
        rewrite H1.
        exists (a::l1); exists l2; exists y; repeat split; trivial.
    - destruct H as [l1 [l2 [y [H1 [H2 H3]]]]].
      rewrite H1.
      apply new_compare_aux_eq in H3.
      clear H1.
      induction l1; simpl.
      now rewrite H3.
      now rewrite IHl1.
  Qed.

  Lemma new_compare_eq_trans : forall Ev (a1 a2 a3 : XBB), new_compare Ev a1 a2 = Eq -> new_compare Ev a2 a3 = Eq -> new_compare Ev a1 a3 = Eq.
  Proof.
    intros.
    apply new_compare_eq.
    intros.
    apply (proj1 (new_compare_eq _ _ _)) with x in H; trivial.
    apply (proj1 (new_compare_eq _ _ _)) with x in H0; trivial.
    apply (Oeset.compare_eq_trans _ _ _ _ H H0).
  Qed.

  Lemma new_compare_eq_lt_trans : forall Ev (a1 a2 a3 : XBB),
      new_compare Ev a1 a2 = Eq ->
      new_compare Ev a2 a3 = Lt -> new_compare Ev a1 a3 = Lt.
  Proof.
    intros.
    apply new_compare_lt.
    apply new_compare_lt in H0 as [l1 [l2 [y [H1 [H2 H3]]]]].
    exists l1; exists l2; exists y; repeat split; trivial.
    apply (proj1 (new_compare_eq _ _ _)) with y in H.
    apply (Oeset.compare_eq_lt_trans _ _ _ _ H H2).
    rewrite H1.
    rewrite Oeset.mem_bool_app. simpl.
    rewrite Oeset.eq_bool_refl. apply orb_true_r.
    intros.
    apply (proj1 (new_compare_eq _ _ _)) with x in H.
    apply H3 in H0.
    apply (Oeset.compare_eq_trans _ _ _ _ H H0).
    rewrite H1.
    rewrite Oeset.mem_bool_app.
    simpl.
    rewrite H0.
    rewrite orb_true_r.
    apply orb_true_r.
  Qed.



  Lemma new_compare_lt_eq_trans : forall Ev (a1 a2 a3 : XBB),
      new_compare Ev a1 a2 = Lt ->
      new_compare Ev a2 a3 = Eq -> new_compare Ev a1 a3 = Lt.
  Proof.
    intros.
    apply new_compare_lt.
    apply new_compare_lt in H as [l1 [l2 [y [H1 [H2 H3]]]]].
    exists l1; exists l2; exists y; repeat split; trivial.
    apply (proj1 (new_compare_eq _ _ _)) with y in H0.
    apply (Oeset.compare_lt_eq_trans _ _ _ _ H2 H0).
    rewrite H1.
    rewrite Oeset.mem_bool_app. simpl.
    rewrite Oeset.eq_bool_refl. apply orb_true_r.
    intros.
    apply (proj1 (new_compare_eq _ _ _)) with x in H0.
    apply H3 in H.
    apply (Oeset.compare_eq_trans _ _ _ _ H H0).
    rewrite H1.
    rewrite Oeset.mem_bool_app.
    simpl.
    rewrite H.
    rewrite orb_true_r.
    apply orb_true_r.
  Qed.


  Lemma feset_elt_in_1 : forall Ev l1 l2 l3 l4 y y0,
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l1 ++ y :: l2 ->
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l3 ++ y0 :: l4 ->
      Oeset.compare TXB y0 y = Lt ->
      In y0 l1.
  Proof.
   intros.
    assert (In y0 (l1 ++ y :: l2)).
    rewrite <- H.
    rewrite H0.
    apply in_or_app.
    right; now left.
    apply in_app_or in H2 as [H2|[H2|H2]]; trivial.
    * rewrite H2 in H1.
      rewrite Oeset.compare_eq_refl in H1.
      inversion H1.
    * apply (Oeset.in_mem_bool TXB) in H2.
      rewrite Oeset.compare_lt_gt in H1.
      apply CompOpp_iff in H1.
      rewrite (Facts.lt_feset_elt (Femap2.FeElt1 FeXBB) Ev l1 y l2 H y0 H2) in H1.
      inversion H1.
  Qed.

  Lemma feset_elt_in_2 : forall Ev l1 l2 l3 l4 y y0,
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l1 ++ y :: l2 ->
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l3 ++ y0 :: l4 ->
      Oeset.compare TXB y0 y = Lt ->
      In y l4.
  Proof.
   intros.
   assert (In y (l3 ++ y0 :: l4)).
   rewrite <- H0.
   rewrite H.
   apply in_or_app.
   right; now left.
   apply in_app_or in H2 as [H2|[H2|H2]]; trivial.
   * apply in_split in H2 as [l5 [l6 H2]].
     rewrite H2 in H0.
     rewrite app_assoc_reverse in H0.
     rewrite <- app_comm_cons in H0.
     rewrite Oeset.compare_lt_gt in H1.
     apply CompOpp_iff in H1.
     pose (Facts.lt_feset_elt (Femap2.FeElt1 FeXBB) Ev l5 y (l6 ++ y0 :: l4) H0 y0).
     rewrite e in H1.
     inversion H1.
     rewrite Oeset.mem_bool_app. simpl.
     rewrite Oeset.eq_bool_refl.
     apply orb_true_r.
   * rewrite H2 in H1.
     rewrite Oeset.compare_eq_refl in H1.
     inversion H1.
  Qed.

 
  Lemma feset_elt_in_3 : forall Ev l1 l2 l3 l4 y y0,
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l1 ++ y :: l2 ->
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l3 ++ y0 :: l4 ->
      Oeset.compare TXB y0 y = Lt ->
      (forall x, Oeset.mem_bool TXB x l2 = true ->
                 Oeset.mem_bool TXB x l4 = true).
  Proof.
    intros.
    apply Oeset.mem_bool_true_iff in H2 as [a [H2a H3a]].
    apply in_split in H3a as [l1a [l2a H3a]].
    rewrite H3a in H.
    assert (lt TXB y0 a).
    assert (Oeset.mem_bool TXB a (l1a ++ a :: l2a) = true).
    rewrite Oeset.mem_bool_app. simpl.
    rewrite Oeset.eq_bool_refl.
    apply orb_true_r.
    pose (Facts.lt_feset_elt (Femap2.FeElt1 FeXBB) Ev _ _ _ H a H2).
    apply (Oeset.compare_lt_trans _ _ _ _ H1 e).
    rewrite  app_comm_cons in H.
    rewrite <- app_assoc_reverse in H.     
    pose (feset_elt_in_2 _ _ _ _ _ _ _ H H0 H2).
    apply (Oeset.in_mem_bool TXB) in i.
    now rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H2a).
Qed.

  Lemma feset_elt_in_4 : forall Ev l1 l2 l3 l4 y y0,
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l1 ++ y :: l2 ->
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l3 ++ y0 :: l4 ->
      Oeset.compare TXB y0 y = Eq -> y0 = y.
  Proof.
    intros.
    assert (In y (l3 ++ y0 :: l4)).
    rewrite <- H0.
    rewrite H.
    apply in_or_app.
    right; now left.
    apply in_app_or in H2 as [H2|[H2|H2]]; trivial.
    * apply in_split in H2 as [l5 [l6 H2]].
      rewrite H2 in H0.
      rewrite app_assoc_reverse in H0.
      rewrite <- app_comm_cons in H0.
      apply Oeset.compare_eq_sym in H1.
      pose (Facts.lt_feset_elt (Femap2.FeElt1 FeXBB) Ev l5 y (l6 ++ y0 :: l4) H0 y0).
      rewrite e in H1.
      inversion H1.
      rewrite Oeset.mem_bool_app. simpl.
      rewrite Oeset.eq_bool_refl.
      apply orb_true_r.
    * apply in_split in H2 as [l5 [l6 H2]].
      rewrite H2 in H0.
      pose (Facts.lt_feset_elt (Femap2.FeElt1 FeXBB) Ev _ _ _ H0 y).
      rewrite e in H1.
      inversion H1.
      rewrite Oeset.mem_bool_app. simpl.
      rewrite Oeset.eq_bool_refl.
      apply orb_true_r.
  Qed.

  Lemma feset_elt_in_5 : forall Ev l1 l2 l3 l4 y y0,
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l1 ++ y :: l2 ->
      Feset.elements (Femap2.FeElt1 FeXBB) Ev = l3 ++ y0 :: l4 ->
      Oeset.compare TXB y0 y = Eq ->
      (forall x, Oeset.mem_bool TXB x l2 = true ->
                 Oeset.mem_bool TXB x l4 = true).
  Proof.
    intros.
    apply Oeset.mem_bool_true_iff in H2 as [a [H2a H3a]].
    apply in_split in H3a as [l1a [l2a H3a]].
    rewrite H3a in H.
    assert (lt TXB y0 a).
    assert (Oeset.mem_bool TXB a (l1a ++ a :: l2a) = true).
    rewrite Oeset.mem_bool_app. simpl.
    rewrite Oeset.eq_bool_refl.
    apply orb_true_r.
    pose (Facts.lt_feset_elt (Femap2.FeElt1 FeXBB) Ev _ _ _ H a H2).
    apply (Oeset.compare_eq_lt_trans _ _ _ _ H1 e).
    rewrite  app_comm_cons in H.
    rewrite <- app_assoc_reverse in H.     
    pose (feset_elt_in_2 _ _ _ _ _ _ _ H H0 H2).
    apply (Oeset.in_mem_bool TXB) in i.
    now rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H2a).
Qed.
    
  Lemma new_compare_lt_trans : forall Ev (a1 a2 a3 : XBB),
      new_compare Ev a1 a2 = Lt ->
      new_compare Ev a2 a3 = Lt -> new_compare Ev a1 a3 = Lt.
 Proof.
    intros.
    apply new_compare_lt.
    apply new_compare_lt in H as [l1 [l2 [y [H1 [H2 H3]]]]].
    apply new_compare_lt in H0 as [l01 [l02 [y0 [H01 [H02 H03]]]]].
    case_eq (Oeset.compare TXB y0 y); intros.
    - exists l1; exists l2; exists y. 
      repeat split; trivial.
      apply (feset_elt_in_4 _ _ _ _ _ _ _ H1 H01) in H.
      rewrite H in H02.
      apply (Oeset.compare_lt_trans _ _ _ _ H2 H02).
      intros.
      specialize (H3 x H0).
      pose (feset_elt_in_5 _ _ _ _ _ _ _ H1 H01 H _ H0).
      specialize (H03 x e).
      apply (Oeset.compare_eq_trans _ _ _ _ H3 H03).
    - exists l1; exists l2; exists y. 
      repeat split; trivial.
      apply (feset_elt_in_2 _ _ _ _ _ _ _ H1 H01) in H.
      apply (Oeset.in_mem_bool TXB) in H.
      apply H03 in H.
      apply (Oeset.compare_lt_eq_trans _ _ _ _ H2 H).
      intros.
      specialize (H3 x H0).
      pose (feset_elt_in_3 _ _ _ _ _ _ _ H1 H01 H _ H0).
      specialize (H03 x e).
      apply (Oeset.compare_eq_trans _ _ _ _ H3 H03).
    - rewrite Oeset.compare_lt_gt in H.
      apply CompOpp_iff in H.
      exists l01; exists l02; exists y0. 
      repeat split; trivial.
      apply (feset_elt_in_2 _ _ _ _ _ _ _ H01 H1) in H.
      apply (Oeset.in_mem_bool TXB) in H.
      apply H3 in H.
      apply (Oeset.compare_eq_lt_trans _ _ _ _ H H02).
      intros.
      specialize (H03 x H0).
      pose (feset_elt_in_3 _ _ _ _ _ _ _ H01 H1 H _ H0).
      specialize (H3 x e).
      apply (Oeset.compare_eq_trans _ _ _ _ H3 H03).
 Qed.
 
  Lemma new_compare_lt_gt : forall Ev (a1 a2: XBB),
      new_compare Ev a1 a2 = CompOpp (new_compare Ev a2 a1).
  Proof.
    intros.
    case_eq (new_compare Ev a2 a1); intros.
    apply new_compare_eq.
    intros.
    apply Oeset.compare_eq_sym.
    revert H0; revert x. now apply new_compare_eq.
    apply new_compare_lt in H as [l1 [l2 [y [H [H0 H1]]]]].
    apply new_compare_gt.
    exists l1; exists l2; exists y. repeat split; trivial.
    rewrite Oeset.compare_lt_gt.
    now rewrite CompOpp_iff.
    intros.
    apply Oeset.compare_eq_sym; now apply H1.
    apply new_compare_gt in H as [l1 [l2 [y [H [H0 H1]]]]].
    apply new_compare_lt.
    exists l1; exists l2; exists y. repeat split; trivial.
    rewrite Oeset.compare_lt_gt.
    now rewrite CompOpp_iff.
    intros.
    apply Oeset.compare_eq_sym; now apply H1.
  Qed.

    
  Definition TXBB : Feset.set (Femap2.FeElt1 FeXBB) -> Oeset.Rcd XBB.
    intro Ev; split with
                  (new_compare Ev).
    - apply new_compare_eq_trans.
    - apply new_compare_eq_lt_trans.
    - apply new_compare_lt_eq_trans.
    - apply new_compare_lt_trans.
    - apply new_compare_lt_gt.
  Defined.


  Lemma Feset_equal_trans : forall A (OA : Oeset.Rcd A) (FA : Feset.Rcd OA) (a1 a2 a3 : Feset.set FA),
      a1 =SE= a2 -> a2 =SE= a3 -> a1 =SE= a3.
  Proof.
    intros.
    apply Feset.compare_spec.
    apply Feset.compare_spec in H.
    apply Feset.compare_spec in H0.
    apply (Feset.compare_eq_trans _ _ _ _ H H0).
  Qed.


  Lemma Feset_equal_eq_trans : forall A (OA : Oeset.Rcd A) (FA : Feset.Rcd OA) (a1 a2 a3 : Feset.set FA),
      a1 =SE= a2 -> a3 =SE?= a1 = (a3 =SE?= a2).
  Proof.
    intros.
    case_eq (a3 =SE?= a1); intros; symmetry.
    apply (Feset_equal_trans _ _ _ _ H0 H).
    apply not_true_iff_false.
    apply not_true_iff_false in H0.
    intro. destruct H0.
    apply Feset.equal_true_sym in H.
    apply (Feset_equal_trans _ _ _ _ H1 H).
  Qed.

  Lemma Feset_equal_eq_trans_2 : forall A (OA : Oeset.Rcd A) (FA : Feset.Rcd OA) (a1 a2 a3 : Feset.set FA),
      a1 =SE= a2 -> a1 =SE?= a3 = (a2 =SE?= a3).
  Proof.
    intros.
    case_eq (a1 =SE?= a3); intros; symmetry.
    apply Feset.equal_true_sym in H.
    apply (Feset_equal_trans _ _ _ _ H H0).
    apply not_true_iff_false.
    apply not_true_iff_false in H0.
    intro. destruct H0.
    apply (Feset_equal_trans _ _ _ _ H H1).
  Qed.

  Lemma FB_is_CSR : forall Ev,
      Feset.is_empty (Femap2.FeElt1 FeXBB) Ev = false -> CSR (zero Ev) (one Ev) (plus Ev) (mul Ev) (TXBB Ev).
  Proof.
    split; intros.
    - unfold zero; unfold one.
      case_eq (Feset.choose (Femap2.FeElt1 FeXBB) Ev); intros.
      apply Feset.choose_spec1 in H0.
      rewrite Feset.mem_elements in H0.
      simpl.
      apply Oeset.mem_bool_true_iff in H0 as [a [H0 H1]].
      apply in_split in H1 as [l1 [l2 H1]].
      rewrite H1.
      clear H.
      destruct l1.
      simpl.
      intro.
      apply (proj1 (new_compare_eq _ _ _)) with a in H.
      pose (Femap2.add_elt_find_2 FeXBB a false (mk_fm2 l2 (fun _ : XB => false))).
      rewrite (Femap2.coeff_spec_1 _ _ _ e) in H.
      clear e.
      pose (Femap2.add_elt_find_2 FeXBB a true (mk_fm2 l2 (fun _ : XB => true))).
      rewrite (Femap2.coeff_spec_1 _ _ _ e) in H.
      inversion H.
      rewrite H1.
      simpl.
      now rewrite Oeset.eq_bool_refl.
      simpl.
      intro.
      apply (proj1 (new_compare_eq _ _ _)) with p0 in H.
      pose (Femap2.add_elt_find_2 FeXBB p0 false (mk_fm2 (l1 ++ a :: l2) (fun _ : XB => false))).
      rewrite (Femap2.coeff_spec_1 _ _ _ e) in H.
      clear e.
      pose (Femap2.add_elt_find_2 FeXBB p0 true (mk_fm2 (l1 ++ a :: l2) (fun _ : XB => true))).
      rewrite (Femap2.coeff_spec_1 _ _ _ e) in H.
      inversion H.
      rewrite H1.
      simpl.
      now rewrite Oeset.eq_bool_refl.
      apply Feset.choose_spec2 in H0.
      rewrite H in H0; inversion H0.
    - split; intros.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite ! plus_coeff.
        rewrite <- Feset.mem_elements in H0.
        rewrite H0; rewrite orb_assoc;
          apply Oeset.compare_eq_refl.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite plus_coeff.
        rewrite <- Feset.mem_elements in H0.
        rewrite zero_coeff.
        rewrite H0.
        apply Oeset.compare_eq_refl.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite ! plus_coeff.
        rewrite <- Feset.mem_elements in H0.
        rewrite H0.
        rewrite ! (orb_comm (Femap2.coeff FeXBB x a2)).
        apply Oeset.compare_eq_refl.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite ! plus_coeff.
        apply (proj1 (new_compare_eq _ _ _)) with x in H0; trivial.
        apply (proj1 (new_compare_eq _ _ _)) with x in H1; trivial.
        rewrite <- Feset.mem_elements in H2.
        rewrite H2.
        case_eq (Femap2.coeff FeXBB x a1); case_eq (Femap2.coeff FeXBB x a2); case_eq (Femap2.coeff FeXBB x b1); case_eq (Femap2.coeff FeXBB x b2); intros; simpl; trivial;
        simpl in H0;
        simpl in H1;
        unfold bool_compare in H0;
        unfold bool_compare in H1; rewrite H6 in H0; rewrite H5 in H0; rewrite H4 in H1; rewrite H3 in H1; try solve [inversion H0|inversion H1].
    - split; intros.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite ! mul_coeff.
        rewrite <- Feset.mem_elements in H0.
        rewrite H0; rewrite andb_assoc;
          apply Oeset.compare_eq_refl.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite mul_coeff.
        rewrite <- Feset.mem_elements in H0.
        rewrite one_coeff.
        rewrite H0.
        apply Oeset.compare_eq_refl.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite ! mul_coeff.
        rewrite <- Feset.mem_elements in H0.
        rewrite H0.
        rewrite ! (andb_comm (Femap2.coeff FeXBB x a2)).
        apply Oeset.compare_eq_refl.
      * simpl.
        apply new_compare_eq.
        intros.
        rewrite ! mul_coeff.
        apply (proj1 (new_compare_eq _ _ _)) with x in H0; trivial.
        apply (proj1 (new_compare_eq _ _ _)) with x in H1; trivial.
        rewrite <- Feset.mem_elements in H2.
        rewrite H2.
        case_eq (Femap2.coeff FeXBB x a1); case_eq (Femap2.coeff FeXBB x a2); case_eq (Femap2.coeff FeXBB x b1); case_eq (Femap2.coeff FeXBB x b2); intros; simpl; trivial;
        simpl in H0;
        simpl in H1;
        unfold bool_compare in H0;
        unfold bool_compare in H1; rewrite H6 in H0; rewrite H5 in H0; rewrite H4 in H1; rewrite H3 in H1; try solve [inversion H0|inversion H1].
    - simpl.
      apply new_compare_eq.
      intros.
      rewrite ! mul_coeff.
      rewrite ! plus_coeff.
      rewrite ! mul_coeff.
      rewrite <- Feset.mem_elements in H0.
      rewrite H0. rewrite andb_orb_distrib_r.
      apply Oeset.compare_eq_refl.
    - simpl.
      apply new_compare_eq.
      intros.
      rewrite ! mul_coeff.
      rewrite <- Feset.mem_elements in H0.
      rewrite H0.
      rewrite zero_coeff.
      rewrite andb_false_r.
      apply Oeset.compare_eq_refl.
  Qed.
End bool_f.
