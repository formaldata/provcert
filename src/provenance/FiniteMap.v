(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Bool List Sorted SetoidList Arith NArith.

Require Import Facts BasicFacts ListFacts ListPermut ListSort OrderedSet FiniteSet SemiRing. 
Module Femap.
Record Rcd (elt1 : Type) (elt2 : Type) (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2): Type :=
  mk_R
    {
      femap : Type;
      FeElt1 : Feset.Rcd Elt1;
      support : femap -> Feset.set FeElt1;
      find : elt1 -> femap -> option elt2;
      find_spec_1 : forall s x,
          Feset.mem FeElt1 x (support s) = true <->
          exists y, find x s = Some y;
      find_spec_2 : forall s x x',
          Oeset.eq_bool Elt1 x x' = true -> find x s = find x' s;
      equal : femap -> femap -> bool;
      singleton : elt1 -> elt2 -> femap;      
      singleton_find : 
        forall (x1 x2 : elt1) (y1 : elt2),
          Oeset.eq_bool Elt1 x1 x2 = true ->
          find x2 (singleton x1 y1) = Some y1;
      singleton_supp : 
        forall (x1 : elt1) (y1: elt2),
          support (singleton x1 y1) = Feset.singleton FeElt1 x1;
      add : elt1 -> elt2 -> femap -> femap;
      add_supp : forall x y s, support (add x y s) = Feset.add FeElt1 x (support s);
      add_find_1 : forall x x' y s,
          x' inSE (support (add x y s)) -> Oeset.eq_bool Elt1 x x' = false ->
          find x' (add x y s) = find x' s;
      add_find_2 : forall x y s,
          find x (add x y s) = Some y;
      fold : forall (A : Type), (elt1 -> A -> A) -> femap -> A -> A;
      fold_spec : forall (s : femap) (A : Type) a0 (f : elt1 -> A -> A),
          fold f s a0 = Feset.fold _ (fun (e: elt1) (a : A) => f e a) (support s) a0;
      compare : femap -> femap -> comparison;
      compare_spec : forall s1 s2, compare s1 s2 = Eq <-> equal s1 s2 = true;
      compare_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Eq -> compare a1 a3 = Eq;
      compare_eq_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Eq -> compare a1 a3 = Lt;
      compare_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_gt : forall a1 a2, compare a1 a2 = CompOpp (compare a2 a1);
    }.

Section Sec.
Hypothesis A : Type.
Hypothesis EltA : Oeset.Rcd A.
Hypothesis B : Type.
Hypothesis EltB : Oeset.Rcd B.
Hypothesis Fm : Rcd EltA EltB.


Lemma find_spec_alt : forall s x,
          x inSE? (support _ s) = false <->
          find Fm x s = None.
Proof.
  split; intros.
  apply not_true_iff_false in H.
  case_eq (find _ x s); intros; trivial.
  destruct H.
  apply find_spec_1.
  exists b; trivial.
  apply not_true_iff_false.
  intro. apply find_spec_1 in H0 as [y H0].
  rewrite H0 in H; inversion H.
Qed.
End Sec.

Section Build.
  
Require Import FiniteMapImpl.

Definition build (elt1 : Type) (elt2 : Type) (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) :=
  mk_R Elt2 (Feset.build Elt1) fst (fun x m => if Feset.mem (Feset.build Elt1) x (fst m) then Some (f (snd m) x) else None)
       (@find_spec elt1 elt2 Elt1) (@equiv_eq elt1 elt2 Elt1)
       (fun m1 m2 => Feset.equal (Feset.build Elt1) (fst m1) (fst m2) &&
                                 Feset.for_all (Feset.build Elt1) (fun s => Oeset.eq_bool Elt2 (f (snd m1) s) (f (snd m2) s)) (fst m1))
       (fun e1 e2 => (Feset.singleton (Feset.build Elt1) e1, cst_qf Elt1 e2))
       (@singl_spec elt1 elt2 Elt1)
       (@support_singl elt1 elt2 Elt1)
       (fun e1 e2 m => (Feset.add (Feset.build Elt1) e1 (fst m), add_qf e1 e2 m))
       (@support_add elt1 elt2 Elt1)
       (@spec_add elt1 elt2 Elt1)
       (@spec_add_2 elt1 elt2 Elt1)
       (fun A f s (a0 : A) => Feset.fold _ (fun (e: elt1) (a : A) => f e a) (fst s) a0)
       (@feset_fold_refl elt1 elt2 Elt1)
       (compareb Elt2)
       (compareb_spec Elt2)
       (compareb_eq_trans Elt2)
       (compareb_eq_lt_trans Elt2)
       (compareb_lt_eq_trans Elt2)
       (compareb_lt_trans Elt2)
       (compareb_lt_gt Elt2).
End Build.
End Femap.
    

Module Femap2.
Record Rcd (elt1 : Type) (elt2 : Type) (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) (zero : elt2) : Type :=
  mk_R
    {
      map2 : Type;
      FeElt1 : Feset.Rcd Elt1;
      support : map2 -> Feset.set FeElt1;
      find : elt1 -> map2 -> option elt2;
      find_spec_1 : forall s x,
          Feset.mem FeElt1 x (support s) = true <->
          exists y, find x s = Some y;
      find_spec_2 : forall s x x',
          Oeset.eq_bool Elt1 x x' = true -> find x s = find x' s;
      coeff : elt1 -> map2 -> elt2;
      coeff_spec_1 : forall (s: map2) (x:elt1) (y: elt2),
          find x s = Some y -> coeff x s = y;
      coeff_spec_2 : forall (s: map2) (x:elt1),
          find x s = None -> coeff x s = zero;
      equal : map2 -> map2 -> bool;
      equal_spec : forall s1 s2, equal s1 s2 = true <-> (forall x, Oeset.eq_bool Elt2 (coeff x s1) (coeff x s2) = true);  
      singleton : elt1 -> elt2 -> map2;      
      singleton_find : 
        forall (x1 x2 : elt1) (y1 : elt2),
          Oeset.eq_bool Elt1 x1 x2 = true ->
          find x2 (singleton x1 y1) = Some y1;
      singleton_supp : 
        forall (x1 : elt1) (y1: elt2),
          support (singleton x1 y1) = Feset.singleton FeElt1 x1;
      add_elt : elt1 -> elt2 -> map2 -> map2;
      add_elt_supp : forall x y s, support (add_elt x y s) = Feset.add FeElt1 x (support s);
      add_elt_find_1 : forall x x' y s,
          x' inSE (support (add_elt x y s)) -> Oeset.eq_bool Elt1 x x' = false ->
          find x' (add_elt x y s) = find x' s;
      add_elt_find_2 : forall x y s,
          find x (add_elt x y s) = Some y;
      empty : map2;
      empty_supp : support empty = Feset.empty FeElt1;
      fold : forall (A : Type), (elt1 -> A -> A) -> map2 -> A -> A;
      fold_spec : forall (s : map2) (A : Type) a0 (f : elt1 -> A -> A),
          fold f s a0 = Feset.fold _ (fun (e: elt1) (a : A) => f e a) (support s) a0;
      compare : map2 -> map2 -> comparison;
      compare_spec : forall s1 s2, compare s1 s2 = Eq <-> equal s1 s2 = true;
      compare_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Eq -> compare a1 a3 = Eq;
      compare_eq_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Eq -> compare a1 a3 = Lt;
      compare_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_gt : forall a1 a2, compare a1 a2 = CompOpp (compare a2 a1);
      }.

Section Sec.

Hypothesis A : Type.
Hypothesis EltA : Oeset.Rcd A.
Hypothesis B : Type.
Hypothesis zero : B.
Hypothesis EltB : Oeset.Rcd B.
Hypothesis Fm : Rcd EltA EltB zero.
Notation EltAB := (Oeset.mk_pair EltA EltB).


Lemma find_spec_alt : forall s x,
          x inSE? (support _ s) = false <->
          find Fm x s = None.
Proof.
  split; intros.
  apply not_true_iff_false in H.
  case_eq (find _ x s); intros; trivial.
  destruct H.
  apply find_spec_1.
  exists b; trivial.
  apply not_true_iff_false.
  intro. apply find_spec_1 in H0 as [y H0].
  rewrite H0 in H; inversion H.
Qed.



Lemma coeff_eq : forall x1 x2 s,
    Oeset.eq_bool EltA x1 x2 = true -> coeff Fm x1 s = coeff Fm x2 s.
Proof.
  intros.
  case_eq (find Fm x2 s); intros.
  rewrite (coeff_spec_1 _ _ _ H0).
  rewrite <- (find_spec_2 _ _ _ _ H) in H0.
  apply (coeff_spec_1 _ _ _ H0).
  rewrite (coeff_spec_2 _ _ _ H0).
  rewrite <- (find_spec_2 _ _ _ _ H) in H0.
  apply (coeff_spec_2 _ _ _ H0).
Qed.


Lemma unique_monome : forall x x' s y1 y2, Oeset.eq_bool EltA x x' = true ->
                                        coeff Fm x s = y1 /\ coeff Fm x' s = y2 -> y1 = y2.
Proof.
  intros x x' s y1 y2 H [H0 H1].
  rewrite <- H0; rewrite <- H1.
  apply (coeff_eq _ _ _ H).
Qed.

Lemma equal_refl : forall s, equal Fm s s = true.
Proof.
intro s; rewrite equal_spec; intro e. apply Oeset.eq_bool_refl.
Qed.

Lemma equal_true_sym : forall s1 s2, equal Fm s1 s2 = true -> equal Fm s2 s1 = true.
Proof.
  intros s1 s2. rewrite !equal_spec. intros H e. rewrite Oeset.eq_bool_sym.
  apply H.
Qed.

Lemma eq_compare_AB :
  forall x1 x2 y1 y2,
    Oeset.compare EltAB (x1, y1) (x2, y2) = Eq ->
    Oeset.compare EltA x1 x2 = Eq /\  Oeset.compare EltB y1 y2 = Eq.
Proof.
  intros. simpl in H.
  destruct (Oeset.compare EltA x1 x2); destruct (Oeset.compare EltB y1 y2); try solve [split; trivial|inversion H].
Qed.

Lemma singleton_coeff_eq : forall x x' y, Oeset.eq_bool EltA x x' = true -> coeff Fm x' (singleton _ x y) = y.
Proof.
  intros.
  apply coeff_spec_1.
  apply (singleton_find _ _ _ _ H).
Qed.

Lemma singleton_coeff_neq : forall x x' y, Oeset.eq_bool EltA x x' = false -> coeff Fm x' (singleton _ x y) = zero.
Proof.
  intros.
  apply coeff_spec_2.
  apply find_spec_alt.
  rewrite singleton_supp.
  rewrite Feset.singleton_spec.
  rewrite Oeset.eq_bool_sym; trivial.
Qed.
  

Lemma singleton_eq :
  forall x1 x2 y1 y2,
    Oeset.compare EltAB (x1,y1) (x2,y2) = Eq -> equal Fm (singleton Fm x1 y1) (singleton Fm x2 y2) = true.
Proof.
  intros; rewrite equal_spec; intros.
  apply eq_compare_AB in H as [Ha Hb].
  case_eq (Oeset.eq_bool EltA x x1); intros.
  rewrite (coeff_eq _ _ _ H).
  apply Oeset.eq_bool_true_compare_eq in Ha.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ Ha) in H.
  rewrite (coeff_eq _ _ _ H).
  rewrite 2 singleton_coeff_eq; try apply Oeset.eq_bool_refl.
  apply Oeset.eq_bool_true_compare_eq; trivial.
  rewrite 2 singleton_coeff_neq; try rewrite Oeset.eq_bool_sym; trivial.
  apply Oeset.eq_bool_refl.
  apply Oeset.eq_bool_true_compare_eq in Ha.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ Ha) in H.
  trivial.  
Qed.


Lemma coeff_Pol_0 : forall x x',
    coeff Fm x (singleton _ x' zero) = zero.
Proof.
  intros.
  case_eq (Oeset.eq_bool EltA x' x); intro.
  apply singleton_coeff_eq. trivial.
  apply singleton_coeff_neq. trivial.
Qed.


Lemma coeff_empty : forall x,
    Oeset.eq_bool EltB (coeff Fm x (empty _)) zero = true.
Proof.
  intros.
  assert (x inSE? (support Fm (empty _)) = false).
  rewrite empty_supp.
  apply Feset.mem_empty.
  apply find_spec_alt in H.
  rewrite coeff_spec_2; trivial.
  apply Oeset.eq_bool_refl.  
Qed.



Lemma fold_left_equiv_1 : forall (s2 s3: map2 Fm) l f,
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f x a1) (f x a2) = true) ->
    equal _ s2 s3 = true ->
    forall x,
     Oeset.eq_bool EltB
    (coeff Fm x (fold_left (fun (a : map2 Fm) (e : A) => f e a) l s2))
    (coeff Fm x (fold_left (fun (a : map2 Fm) (e : A) => f e a) l s3)) = true.
Proof.
  intros.
  revert H0; revert s3; revert s2.
  induction l; intros.
  apply (proj1 (equal_spec _ _ _) H0); trivial.
  simpl. apply IHl.
  apply H. trivial.
Qed.
Lemma fold_equiv_1 : forall (s1 s2 s3: map2 Fm) s f,
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f x a1) (f x a2) = true) ->
    equal _ s2 s3 = true ->
    forall x,
     Oeset.eq_bool EltB
    (coeff Fm x (fold _ (fun (e : A) (a : map2 Fm) => f e a) s s2))
    (coeff Fm x (fold Fm (fun (e : A) (a : map2 Fm) => f e a) s s3)) = true.
Proof.
  intros.
  rewrite 2 fold_spec.
  rewrite 2 Feset.fold_spec.
  revert H0; revert s3; revert s2.
  induction (Feset.elements (FeElt1 Fm) (support Fm s)); intros.
  apply (proj1 (equal_spec _ _ _) H0); trivial.
  simpl. apply IHl.
  apply H. trivial.
Qed.

Lemma fold_left_equiv_2 : forall f1 f2 s1 s2 x',
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f1 x a1) (f2 x a2) = true) ->
    Oeset.eq_bool EltB (coeff Fm x' (fold_left (fun (a : map2 Fm) (e : A) => f1 e a) (Feset.elements (FeElt1 Fm) (support Fm s1)) s2))
    (coeff Fm x' (fold_left (fun (a : map2 Fm) (e : A) => f2 e a) (Feset.elements (FeElt1 Fm) (support Fm s1)) s2)) = true.
Proof.
  intros.
  revert s2.
  induction (Feset.elements (FeElt1 Fm) (support Fm s1)); intros.
  apply Oeset.eq_bool_refl.
  simpl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl (f1 a s2))).
  apply fold_left_equiv_1.
  intros.
  apply equal_spec; intros.
  assert (equal Fm (f1 x a1) (f2 x a1) = true).
  apply H.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
  apply (proj1 (equal_spec _ _ _)) with x0 in H1.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  assert (equal Fm (f1 x a1) (f2 x a2) = true).
  apply H; trivial.
  apply (proj1 (equal_spec _ _ _) H2).
  apply H; trivial.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
Qed.

Lemma fold_equiv_2 : forall f1 f2 s1 s2 x',
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f1 x a1) (f2 x a2) = true) ->
    Oeset.eq_bool EltB (coeff _ x' (fold Fm (fun x a => f1 x a) s1 s2)) (coeff Fm x' (fold _ (fun x a => f2 x a) s1 s2)) = true.
Proof.
  intros.
  rewrite 2 fold_spec.
  rewrite 2 Feset.fold_spec.
  revert s2.
  induction (Feset.elements (FeElt1 Fm) (support Fm s1)); intros.
  apply Oeset.eq_bool_refl.
  simpl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl (f1 a s2))).
  apply fold_left_equiv_1.
  intros.
  apply equal_spec; intros.
  assert (equal Fm (f1 x a1) (f2 x a1) = true).
  apply H.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
  apply (proj1 (equal_spec _ _ _)) with x0 in H1.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  assert (equal Fm (f1 x a1) (f2 x a2) = true).
  apply H; trivial.
  apply (proj1 (equal_spec _ _ _) H2).
  apply H; trivial.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
Qed.







Lemma aux_4 : forall x b,
    Oeset.eq_bool EltB zero (Femap2.coeff Fm x b) = false ->
    x inSE (Femap2.support Fm b).
Proof.
  intros.
  apply not_false_iff_true.
  intro.
  apply not_true_iff_false in H.
  destruct H.
  apply find_spec_alt in H0.
  apply coeff_spec_2 in H0.
  rewrite H0;  
  apply Oeset.eq_bool_refl.
Qed.



Lemma eq_supp_suppr_zero : forall b1 b2 ,
    Femap2.equal Fm b1 b2 = true ->
    (forall x,
        Oeset.nb_occ EltA x (filter (fun x => negb (Oeset.eq_bool EltB zero (Femap2.coeff _ x b1))) (Feset.elements _ (Femap2.support _ b1))) =
        (Oeset.nb_occ EltA x (filter (fun x => negb (Oeset.eq_bool EltB zero (Femap2.coeff _ x b2))) (Feset.elements _ (Femap2.support _ b2))))).
Proof.
  intros.
  rewrite ! Oeset.nb_occ_filter; intros; try solve [apply Oeset.eq_bool_true_compare_eq in H1;
    now rewrite (coeff_eq _ _ _ H1)].
  rewrite <- ! Feset.nb_occ_unfold.
  apply (proj1 (Femap2.equal_spec _ _ _)) with x in H.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
  rewrite ! Feset.nb_occ_alt.
  case_eq (negb (Oeset.eq_bool EltB zero (Femap2.coeff Fm x b2))); intros; trivial.
  rewrite negb_true_iff in H0.
  rewrite (aux_4 _ _ H0).
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H) in H0.
  now rewrite (aux_4 _ _ H0).
Qed.

End Sec.

Notation "s1 '=M2E=' s2" := (Femap2.equal _ s1 s2 = true) (at level 70, no associativity).
Notation "s1 '=M2E?=' s2" := (Femap2.equal _ s1 s2) (at level 70, no associativity).
Require Import FiniteMapImpl.


Section Build.
Definition build (elt1 : Type) (elt2 : Type) (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) (zero : elt2) :=
  mk_R Elt2 (Feset.build Elt1) fst (fun x m => if Feset.mem (Feset.build Elt1) x (fst m) then Some (f (snd m) x) else None)
       (@find_spec elt1 elt2 Elt1) (@equiv_eq elt1 elt2 Elt1)
       (@coeff elt1 Elt1 elt2 zero)
       (@coeff_spec1 elt1 Elt1 elt2 zero)
       (@coeff_spec2 elt1 Elt1 elt2 zero)
       (@equal elt1 Elt1 elt2 Elt2 zero)
       (@equal_spec elt1 Elt1 elt2 Elt2 zero)
       (fun e1 e2 => (Feset.singleton (Feset.build Elt1) e1, cst_qf Elt1 e2))
       (@singl_spec elt1 elt2 Elt1)
       (@support_singl elt1 elt2 Elt1)
       (fun e1 e2 m => (Feset.add (Feset.build Elt1) e1 (fst m), add_qf e1 e2 m))
       (@support_add elt1 elt2 Elt1)
       (@spec_add elt1 elt2 Elt1)
       (@spec_add_2 elt1 elt2 Elt1)
       (Feset.empty (Feset.build Elt1), empty_qf Elt1 zero)
       (eq_refl _)
       (@fold_map elt1 Elt1 elt2)
       (@feset_fold_refl elt1 elt2 Elt1)
       (compare_fmap Elt2 zero)
       (compare_equal Elt2 zero)
       (compare_fmap_eq_trans Elt2 zero)
       (compare_fmap_eq_lt_trans Elt2 zero)
       (compare_fmap_lt_eq_trans Elt2 zero)
       (compare_fmap_lt_trans Elt2 zero)
       (compare_fmap_lt_gt Elt2 zero).
End Build.      
End Femap2.

Module FePmap.
Record Rcd (elt1 : Type) (elt2 : Type) (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) (zero : elt2) (plus : elt2 -> elt2 -> elt2) (mul1 : elt1 -> elt1 -> elt1) (mul2 : elt2 -> elt2 -> elt2) (Elt1_is_CIO : CIO mul1 Elt1): Type :=
  mk_R
    {
      pmap : Type;
      FeElt1 : Feset.Rcd Elt1;
      support : pmap -> Feset.set FeElt1;
      find : elt1 -> pmap -> option elt2;
      find_spec_1 : forall s x,
          Feset.mem FeElt1 x (support s) = true <->
          exists y, find x s = Some y;
      find_spec_2 : forall s x x',
          Oeset.eq_bool Elt1 x x' = true -> find x s = find x' s;
      coeff : elt1 -> pmap -> elt2;
      coeff_spec_1 : forall (s: pmap) (x:elt1) (y: elt2),
          find x s = Some y -> coeff x s = y;
      coeff_spec_2 : forall (s: pmap) (x:elt1),
          find x s = None -> coeff x s = zero;
      equal : pmap -> pmap -> bool;
      equal_spec : forall s1 s2, equal s1 s2 = true <-> (forall x, Oeset.eq_bool Elt2 (coeff x s1) (coeff x s2) = true);  
      singleton : elt1 -> elt2 -> pmap;      
      singleton_find : 
        forall (x1 x2 : elt1) (y1 : elt2),
          Oeset.eq_bool Elt1 x1 x2 = true ->
          find x2 (singleton x1 y1) = Some y1;
      singleton_supp : 
        forall (x1 : elt1) (y1: elt2),
          support (singleton x1 y1) = Feset.singleton FeElt1 x1;
            add_elt : elt1 -> elt2 -> pmap -> pmap;
      add_elt_supp : forall x y s, support (add_elt x y s) = Feset.add FeElt1 x (support s);
      add_elt_find_1 : forall x x' y s,
          x' inSE (support (add_elt x y s)) -> Oeset.eq_bool Elt1 x x' = false ->
          find x' (add_elt x y s) = find x' s;
      add_elt_find_2 : forall x y s,
          find x (add_elt x y s) = Some y;
      union : pmap -> pmap -> pmap;
      union_supp : forall s s',
          support (union s s') = Feset.union FeElt1 (support s) (support s');
      union_find : forall (s s' : pmap) (x : elt1),
          x inSE (support (union s s')) ->
          find x (union s s') = Some (plus (coeff x s) (coeff x s'));
      empty : pmap;
      empty_supp : support empty = Feset.empty FeElt1;
      fold : forall (A : Type), (elt1 -> A -> A) -> pmap -> A -> A;
      fold_spec : forall (s : pmap) (A : Type) a0 (f : elt1 -> A -> A),
          fold f s a0 = Feset.fold _ (fun (e: elt1) (a : A) => f e a) (support s) a0;
      transpo : elt1 -> elt2 -> pmap -> pmap;
      transpo_supp : forall x y s,
          support (transpo x y s) = Feset.map FeElt1 FeElt1 (fun e => mul1 e x) (support s);
      transpo_find : forall s x1 x2 y,
          (mul1 x1 x2) inSE (support (transpo x2 y s)) ->
          find (mul1 x1 x2) (transpo x2 y s) = Some (mul2 (coeff x1 s) y);                          
      mul_Pol : pmap -> pmap -> pmap;
      mul_Pol_spec: forall s s',
          mul_Pol s s' = fold (fun x a => union (transpo x (coeff x s') s) a) s' empty;
      compare : pmap -> pmap -> comparison;
      compare_spec : forall s1 s2, compare s1 s2 = Eq <-> equal s1 s2 = true;
      compare_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Eq -> compare a1 a3 = Eq;
      compare_eq_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Eq -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_eq_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Eq -> compare a1 a3 = Lt;
      compare_lt_trans : 
        forall a1 a2 a3, compare a1 a2 = Lt -> compare a2 a3 = Lt -> compare a1 a3 = Lt;
      compare_lt_gt : forall a1 a2, compare a1 a2 = CompOpp (compare a2 a1);
      }.

Section Sec.

Hypothesis A : Type.
Hypothesis EltA : Oeset.Rcd A.
Hypothesis B : Type.
Hypothesis zero : B.
Hypothesis plus : B -> B -> B.
Hypothesis EltB : Oeset.Rcd B.
Hypothesis mulA : A -> A -> A.
Hypothesis mulB : B -> B -> B.
Hypothesis EltA_is_CIO : CIO mulA EltA.
Hypothesis Fm : Rcd EltB zero plus mulB EltA_is_CIO.
Notation EltAB := (Oeset.mk_pair EltA EltB).


Lemma find_spec_alt : forall s x,
          x inSE? (support _ s) = false <->
          find Fm x s = None.
Proof.
  split; intros.
  apply not_true_iff_false in H.
  case_eq (find _ x s); intros; trivial.
  destruct H.
  apply find_spec_1.
  exists b; trivial.
  apply not_true_iff_false.
  intro. apply find_spec_1 in H0 as [y H0].
  rewrite H0 in H; inversion H.
Qed.



Lemma coeff_eq : forall x1 x2 s,
    Oeset.eq_bool EltA x1 x2 = true -> coeff Fm x1 s = coeff Fm x2 s.
Proof.
  intros.
  case_eq (find Fm x2 s); intros.
  rewrite (coeff_spec_1 _ _ _ H0).
  rewrite <- (find_spec_2 _ _ _ _ H) in H0.
  apply (coeff_spec_1 _ _ _ H0).
  rewrite (coeff_spec_2 _ _ _ H0).
  rewrite <- (find_spec_2 _ _ _ _ H) in H0.
  apply (coeff_spec_2 _ _ _ H0).
Qed.


Lemma unique_monome : forall x x' s y1 y2, Oeset.eq_bool EltA x x' = true ->
                                        coeff Fm x s = y1 /\ coeff Fm x' s = y2 -> y1 = y2.
Proof.
  intros x x' s y1 y2 H [H0 H1].
  rewrite <- H0; rewrite <- H1.
  apply (coeff_eq _ _ _ H).
Qed.

Lemma equal_refl : forall s, equal Fm s s = true.
Proof.
intro s; rewrite equal_spec; intro e. apply Oeset.eq_bool_refl.
Qed.

Lemma equal_true_sym : forall s1 s2, equal Fm s1 s2 = true -> equal Fm s2 s1 = true.
Proof.
  intros s1 s2. rewrite !equal_spec. intros H e. rewrite Oeset.eq_bool_sym.
  apply H.
Qed.

Lemma eq_compare_AB :
  forall x1 x2 y1 y2,
    Oeset.compare EltAB (x1, y1) (x2, y2) = Eq ->
    Oeset.compare EltA x1 x2 = Eq /\  Oeset.compare EltB y1 y2 = Eq.
Proof.
  intros. simpl in H.
  destruct (Oeset.compare EltA x1 x2); destruct (Oeset.compare EltB y1 y2); try solve [split; trivial|inversion H].
Qed.

Lemma singleton_coeff_eq : forall x x' y, Oeset.eq_bool EltA x x' = true -> coeff Fm x' (singleton _ x y) = y.
Proof.
  intros.
  apply coeff_spec_1.
  apply (singleton_find _ _ _ _ H).
Qed.

Lemma singleton_coeff_neq : forall x x' y, Oeset.eq_bool EltA x x' = false -> coeff Fm x' (singleton _ x y) = zero.
Proof.
  intros.
  apply coeff_spec_2.
  apply find_spec_alt.
  rewrite singleton_supp.
  rewrite Feset.singleton_spec.
  rewrite Oeset.eq_bool_sym; trivial.
Qed.
  

Lemma singleton_eq :
  forall x1 x2 y1 y2,
    Oeset.compare EltAB (x1,y1) (x2,y2) = Eq -> equal Fm (singleton Fm x1 y1) (singleton Fm x2 y2) = true.
Proof.
  intros; rewrite equal_spec; intros.
  apply eq_compare_AB in H as [Ha Hb].
  case_eq (Oeset.eq_bool EltA x x1); intros.
  rewrite (coeff_eq _ _ _ H).
  apply Oeset.eq_bool_true_compare_eq in Ha.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ Ha) in H.
  rewrite (coeff_eq _ _ _ H).
  rewrite 2 singleton_coeff_eq; try apply Oeset.eq_bool_refl.
  apply Oeset.eq_bool_true_compare_eq; trivial.
  rewrite 2 singleton_coeff_neq; try rewrite Oeset.eq_bool_sym; trivial.
  apply Oeset.eq_bool_refl.
  apply Oeset.eq_bool_true_compare_eq in Ha.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ Ha) in H.
  trivial.  
Qed.


Lemma coeff_Pol_0 : forall x x',
    coeff Fm x (singleton _ x' zero) = zero.
Proof.
  intros.
  case_eq (Oeset.eq_bool EltA x' x); intro.
  apply singleton_coeff_eq. trivial.
  apply singleton_coeff_neq. trivial.
Qed.


(* transpo *)

Lemma transpo_supp_mem : forall x x' y s,
    x' inSE support Fm (transpo _ x y s) ->
    exists x1,
      x1 inSE support Fm s /\ Oeset.eq_bool EltA x' (mulA x1 x) = true.
Proof.
  intros.
  rewrite transpo_supp in H.
  apply Feset.mem_map in H as [a [Ha Hb]].
  exists a; split.
  apply Feset.in_elements_mem; trivial.
  apply Oeset.eq_bool_true_compare_eq in Ha. trivial.
Qed.

Lemma transpo_supp_in_mem : forall x x' y s x1,
      In x1 (Feset.elements _ (support Fm s)) -> Oeset.eq_bool EltA x' (mulA x1 x) = true ->
      x' inSE support Fm (transpo _ x y s).
Proof.
  intros.
  rewrite transpo_supp.
  apply Feset.mem_map.
  exists x1; split; trivial.
  apply Oeset.eq_bool_true_compare_eq. trivial.
Qed.

Lemma transpo_coeff : forall x x' y s,
    x' inSE support _ (transpo Fm x y s) ->
    exists x1,
      Oeset.eq_bool EltA x' (mulA x1 x) = true /\ In x1 (Feset.elements _ (support _ s)) /\
      coeff _ x' (transpo Fm x y s) = mulB (coeff _ x1 s) y.
Proof.
  intros.
  generalize H; intro.
  rewrite transpo_supp in H.
  apply Feset.mem_map in H as [a [Ha Hb]].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ha) in H0.
  apply Oeset.eq_bool_true_compare_eq in Ha.
  exists a; intros.
  rewrite (coeff_eq _ _ _ Ha).
  apply transpo_find in H0.
  repeat split; trivial.
  apply (coeff_spec_1 _ _ _ H0).
Qed.

(*fold-mul*)


Lemma fold_union_mem : forall x l b s s' s'',
    x inSE? support Fm
   (fold_left (fun (a : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a)
              (b::l) s'') =
    (x inSE? support Fm
   (union _ (transpo Fm b (coeff Fm b s') s) (fold_left (fun (a : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a)
                l s''))).
Proof.
  intros x; induction l; intros; trivial.
  rewrite fold_left_unfold.
  rewrite IHl.
  rewrite 2 union_supp.
  rewrite 2 Feset.mem_union.
  rewrite IHl.
  rewrite union_supp.
  rewrite Feset.mem_union.
  case_eq (x inSE? support Fm (transpo Fm a (coeff Fm a s') s)); intros.
  rewrite orb_true_r; trivial.
  rewrite 2 orb_false_l.
  rewrite <- Feset.mem_union.
  rewrite <- union_supp.
  rewrite <- IHl.
  trivial.
Qed.



Lemma mul_supp_mem : forall x s s',
    x inSE support Fm (mul_Pol _ s s') ->
    exists x1 x2,
      x1 inSE support Fm s /\ In x2 (Feset.elements _ (support Fm s')) /\ Oeset.eq_bool EltA x (mulA x1 x2) = true.
Proof.
  intros.
  rewrite mul_Pol_spec in H.
  rewrite fold_spec in H.
  rewrite Feset.fold_spec in H.
  induction (Feset.elements _ (support Fm s')).
  rewrite empty_supp in H.
  rewrite Feset.mem_empty in H. inversion H.
  rewrite fold_union_mem in H.
  rewrite union_supp in H.
  rewrite Feset.mem_union in H.
  apply orb_true_iff in H as [H|H].
  apply transpo_supp_mem in H as [x1 [H0 H1]].
  exists x1; exists a; repeat split; trivial.
  left; reflexivity.
  apply IHl in H as [x1 [x2 [H0 [H1 H2]]]].
  exists x1; exists x2; repeat split; trivial.
  right; apply H1.
Qed.

Lemma mul_supp_mem_2 : forall x x1 x2 s s',
    In x1 (Feset.elements _ (support Fm s)) ->
    In x2 (Feset.elements _ (support Fm s')) ->
    Oeset.eq_bool EltA x (mulA x1 x2) = true ->
    x inSE support Fm (mul_Pol _ s s').
Proof.
  intros.
  rewrite mul_Pol_spec.
  rewrite fold_spec.
  rewrite Feset.fold_spec.
  induction (Feset.elements _ (support Fm s')).
  inversion H0.
  rewrite fold_union_mem.
  rewrite union_supp.
  rewrite Feset.mem_union.
  destruct H0 as [H0|H0].
  rewrite H0.
  rewrite (transpo_supp_in_mem _ _ _ _ _ H H1); trivial.
  rewrite IHl; trivial. apply orb_true_r.
Qed.

Lemma coeff_empty : forall x,
    Oeset.eq_bool EltB (coeff Fm x (empty _)) zero = true.
Proof.
  intros.
  assert (x inSE? (support Fm (empty _)) = false).
  rewrite empty_supp.
  apply Feset.mem_empty.
  apply find_spec_alt in H.
  rewrite coeff_spec_2; trivial.
  apply Oeset.eq_bool_refl.  
Qed.



Lemma fold_left_equiv_1 : forall (s2 s3: pmap Fm) l f,
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f x a1) (f x a2) = true) ->
    equal _ s2 s3 = true ->
    forall x,
     Oeset.eq_bool EltB
    (coeff Fm x (fold_left (fun (a : pmap Fm) (e : A) => f e a) l s2))
    (coeff Fm x (fold_left (fun (a : pmap Fm) (e : A) => f e a) l s3)) = true.
Proof.
  intros.
  revert H0; revert s3; revert s2.
  induction l; intros.
  apply (proj1 (equal_spec _ _ _) H0); trivial.
  simpl. apply IHl.
  apply H. trivial.
Qed.
Lemma fold_equiv_1 : forall (s1 s2 s3: pmap Fm) s f,
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f x a1) (f x a2) = true) ->
    equal _ s2 s3 = true ->
    forall x,
     Oeset.eq_bool EltB
    (coeff Fm x (fold _ (fun (e : A) (a : pmap Fm) => f e a) s s2))
    (coeff Fm x (fold Fm (fun (e : A) (a : pmap Fm) => f e a) s s3)) = true.
Proof.
  intros.
  rewrite 2 fold_spec.
  rewrite 2 Feset.fold_spec.
  revert H0; revert s3; revert s2.
  induction (Feset.elements (FeElt1 Fm) (support Fm s)); intros.
  apply (proj1 (equal_spec _ _ _) H0); trivial.
  simpl. apply IHl.
  apply H. trivial.
Qed.

Lemma fold_left_equiv_2 : forall f1 f2 s1 s2 x',
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f1 x a1) (f2 x a2) = true) ->
    Oeset.eq_bool EltB (coeff Fm x' (fold_left (fun (a : pmap Fm) (e : A) => f1 e a) (Feset.elements (FeElt1 Fm) (support Fm s1)) s2))
    (coeff Fm x' (fold_left (fun (a : pmap Fm) (e : A) => f2 e a) (Feset.elements (FeElt1 Fm) (support Fm s1)) s2)) = true.
Proof.
  intros.
  revert s2.
  induction (Feset.elements (FeElt1 Fm) (support Fm s1)); intros.
  apply Oeset.eq_bool_refl.
  simpl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl (f1 a s2))).
  apply fold_left_equiv_1.
  intros.
  apply equal_spec; intros.
  assert (equal Fm (f1 x a1) (f2 x a1) = true).
  apply H.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
  apply (proj1 (equal_spec _ _ _)) with x0 in H1.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  assert (equal Fm (f1 x a1) (f2 x a2) = true).
  apply H; trivial.
  apply (proj1 (equal_spec _ _ _) H2).
  apply H; trivial.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
Qed.

Lemma fold_equiv_2 : forall f1 f2 s1 s2 x',
    (forall x a1 a2, equal _ a1 a2 = true -> equal _ (f1 x a1) (f2 x a2) = true) ->
    Oeset.eq_bool EltB (coeff _ x' (fold Fm (fun x a => f1 x a) s1 s2)) (coeff Fm x' (fold _ (fun x a => f2 x a) s1 s2)) = true.
Proof.
  intros.
  rewrite 2 fold_spec.
  rewrite 2 Feset.fold_spec.
  revert s2.
  induction (Feset.elements (FeElt1 Fm) (support Fm s1)); intros.
  apply Oeset.eq_bool_refl.
  simpl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl (f1 a s2))).
  apply fold_left_equiv_1.
  intros.
  apply equal_spec; intros.
  assert (equal Fm (f1 x a1) (f2 x a1) = true).
  apply H.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
  apply (proj1 (equal_spec _ _ _)) with x0 in H1.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  assert (equal Fm (f1 x a1) (f2 x a2) = true).
  apply H; trivial.
  apply (proj1 (equal_spec _ _ _) H2).
  apply H; trivial.
  apply equal_spec; intros.
  apply Oeset.eq_bool_refl.
Qed.







Lemma aux_4 : forall x b,
    Oeset.eq_bool EltB zero (FePmap.coeff Fm x b) = false ->
    x inSE (FePmap.support Fm b).
Proof.
  intros.
  apply not_false_iff_true.
  intro.
  apply not_true_iff_false in H.
  destruct H.
  apply find_spec_alt in H0.
  apply coeff_spec_2 in H0.
  rewrite H0;  
  apply Oeset.eq_bool_refl.
Qed.



Lemma eq_supp_suppr_zero : forall b1 b2 ,
    FePmap.equal Fm b1 b2 = true ->
    (forall x,
        Oeset.nb_occ EltA x (filter (fun x => negb (Oeset.eq_bool EltB zero (FePmap.coeff _ x b1))) (Feset.elements _ (FePmap.support _ b1))) =
        (Oeset.nb_occ EltA x (filter (fun x => negb (Oeset.eq_bool EltB zero (FePmap.coeff _ x b2))) (Feset.elements _ (FePmap.support _ b2))))).
Proof.
  intros.
  rewrite ! Oeset.nb_occ_filter; intros; try solve [apply Oeset.eq_bool_true_compare_eq in H1;
    now rewrite (coeff_eq _ _ _ H1)].
  rewrite <- ! Feset.nb_occ_unfold.
  apply (proj1 (FePmap.equal_spec _ _ _)) with x in H.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
  rewrite ! Feset.nb_occ_alt.
  case_eq (negb (Oeset.eq_bool EltB zero (FePmap.coeff Fm x b2))); intros; trivial.
  rewrite negb_true_iff in H0.
  rewrite (aux_4 _ _ H0).
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H) in H0.
  now rewrite (aux_4 _ _ H0).
Qed.


Section Sec2.
Hypothesis one : B.
Hypothesis EltB_is_CSR : CSR zero one plus mulB EltB.


Lemma coeff_union_eq : forall x s s',
    Oeset.eq_bool EltB (coeff Fm x (union _ s s')) (plus (coeff _ x s) (coeff _ x s')) = true.
Proof.
  intros.
  case_eq (x inSE? (support _ (union _ s s'))); intros.
  apply union_find in H. apply coeff_spec_1 in H.
  rewrite H. apply Oeset.eq_bool_refl.
  rewrite 3 coeff_spec_2.
  apply Oeset.eq_bool_true_compare_eq.
  apply Oeset.compare_eq_sym.
  apply EltB_is_CSR.
  rewrite union_supp in H.
  rewrite Feset.mem_union in H.
  apply orb_false_iff in H as [_ H].
  apply find_spec_alt; trivial.
   rewrite union_supp in H.
  rewrite Feset.mem_union in H.
  apply orb_false_iff in H as [H _].
  apply find_spec_alt; trivial.
  apply find_spec_alt; trivial.
Qed.

Lemma plus_aux : forall a b c,
    Oeset.eq_bool EltB (plus a (plus b c)) (plus b (plus a c)) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply Oeset.compare_eq_trans with (plus (plus a b) c).
  apply EltB_is_CSR.
  apply Oeset.compare_eq_trans with (plus (plus b a) c).
  repeat apply EltB_is_CSR.
  apply Oeset.compare_eq_refl.
  apply Oeset.compare_eq_sym.
  apply EltB_is_CSR.
Qed.

Lemma fold_union_coeff : forall x l b s s' s'',
    Oeset.eq_bool EltB
    (coeff Fm x (fold_left (fun (a : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a)
              (b::l) s''))
    (plus (coeff Fm x (transpo Fm b (coeff Fm b s') s)) (coeff _ x (fold_left (fun (a : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a)
                l s''))) = true.
Proof.
  intros x; induction l; intros.
  simpl.
  apply coeff_union_eq.
  rewrite fold_left_unfold.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl a s s' (union Fm (transpo Fm b (coeff Fm b s') s) s''))).
  assert ( Oeset.eq_bool EltB
     (plus (coeff Fm x (transpo Fm b (coeff Fm b s') s))
       (coeff Fm x
          (fold_left (fun (a0 : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a0)
             (a :: l) s'')))
    (plus (coeff Fm x (transpo Fm b (coeff Fm b s') s))
          (plus (coeff Fm x (transpo Fm a (coeff Fm a s') s))
                (coeff Fm x
          (fold_left (fun (a0 : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a0)
                     l s'')))) = true).
  apply Oeset.eq_bool_true_compare_eq.
  apply EltB_is_CSR. apply Oeset.compare_eq_refl.
  apply Oeset.eq_bool_true_compare_eq.
  apply IHl.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_aux  (coeff Fm x (transpo Fm b (coeff Fm b s') s))
       (coeff Fm x (transpo Fm a (coeff Fm a s') s))
          (coeff Fm x
             (fold_left
                (fun (a0 : pmap Fm) (e : A) => union Fm (transpo Fm e (coeff Fm e s') s) a0) l
                s'')))).
  apply Oeset.eq_bool_true_compare_eq.
  apply EltB_is_CSR. apply Oeset.compare_eq_refl.
  apply Oeset.eq_bool_true_compare_eq.
  specialize (IHl b s s' s'').
  simpl in IHl; trivial.
Qed.



Lemma transpo_singleton_0 : forall x x' x'' y',
    Oeset.eq_bool EltB (coeff Fm x (transpo Fm x' y' (singleton Fm x'' zero))) zero = true.
Proof.
  intros.
  case_eq (x inSE? support _ (transpo Fm x' y' (singleton Fm x'' zero))); intro.
  apply transpo_coeff in H as [x1 [H0 [H1 H2]]].
  rewrite H2.
  rewrite coeff_Pol_0.
  apply Oeset.eq_bool_true_compare_eq.
  apply Oeset.compare_eq_trans with (mulB y' zero);
  apply EltB_is_CSR.
  rewrite find_spec_alt in H.
  rewrite coeff_spec_2.
  apply Oeset.eq_bool_refl.
  trivial.
Qed.
  


Lemma mul_Pol_0 : forall x x' s,
    Oeset.eq_bool EltB (coeff Fm x (mul_Pol Fm (singleton _ x' zero) s)) zero = true.
Proof.
  intros.
  rewrite mul_Pol_spec.
  rewrite fold_spec.
  rewrite Feset.fold_spec.
  induction (Feset.elements (FeElt1 Fm) (support Fm s)).
  apply coeff_empty.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (fold_union_coeff _ _ _ _ _ _)).
  assert (Oeset.eq_bool EltB
    (plus (coeff Fm x (transpo Fm a (coeff Fm a s) (singleton Fm x' zero)))
       (coeff Fm x
          (fold_left
             (fun (a0 : pmap Fm) (e : A) =>
              union Fm (transpo Fm e (coeff Fm e s) (singleton Fm x' zero)) a0) l 
             (empty Fm))))
    (plus (coeff Fm x (transpo Fm a (coeff Fm a s) (singleton Fm x' zero)))
          zero) = true).
  apply Oeset.eq_bool_true_compare_eq.
  apply EltB_is_CSR. apply Oeset.compare_eq_refl.
  apply Oeset.eq_bool_true_compare_eq.
  apply IHl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
  apply Oeset.eq_bool_true_compare_eq.
  apply Oeset.compare_eq_trans with (plus zero zero).
  apply EltB_is_CSR.
  apply Oeset.eq_bool_true_compare_eq.
  apply transpo_singleton_0.
  apply Oeset.compare_eq_refl.
  apply EltB_is_CSR.
Qed.


Notation plus_assoc_eq_bool := (plus_assoc_eq_bool (isCM_plus_SR EltB_is_CSR)).
Notation plus_comm_eq_bool := (plus_comm_eq_bool (isCM_plus_SR EltB_is_CSR)).
Notation plus_zero_l_eq_bool := (plus_zero_l_eq_bool (isCM_plus_SR EltB_is_CSR)).
Notation plus_zero_r_eq_bool := (plus_zero_r_eq_bool (isCM_plus_SR EltB_is_CSR)).
Notation plus_compat_eq_bool := (plus_compat_eq_bool (isCM_plus_SR EltB_is_CSR)).
Notation plus_compat_l_eq_bool := (plus_compat_l_eq_bool (isCM_plus_SR EltB_is_CSR)).
Notation plus_compat_r_eq_bool := (plus_compat_r_eq_bool (isCM_plus_SR EltB_is_CSR)).

Notation mul_assoc_eq_bool := (SemiRing.plus_assoc_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_comm_eq_bool := (SemiRing.plus_comm_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_one_l_eq_bool := (SemiRing.plus_zero_l_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_one_r_eq_bool := (SemiRing.plus_zero_r_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_compat_eq_bool := (SemiRing.plus_compat_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_compat_l_eq_bool := (SemiRing.plus_compat_l_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_compat_r_eq_bool := (SemiRing.plus_compat_r_eq_bool (isCM_mul_SR EltB_is_CSR)).
Notation mul_zero_l_eq_bool := (SemiRing.mul_zero_l_eq_bool EltB_is_CSR).
Notation mul_zero_r_eq_bool := (SemiRing.mul_zero_r_eq_bool EltB_is_CSR).
Notation mul_distr_plus_r_eq_bool := (mul_distr_plus_r_eq_bool EltB_is_CSR).

Notation plus_aux_1 := (plus_aux_1 (isCM_plus_SR EltB_is_CSR)).
Notation plus_aux_2 := (plus_aux_2_d (isCM_plus_SR EltB_is_CSR)).
Notation mul_aux_1 := (plus_aux_2_g (isCM_mul_SR EltB_is_CSR)).
Notation plus_aux_3 := (plus_aux_3 (isCM_plus_SR EltB_is_CSR)).
Notation plus_aux_4 := (plus_aux_4 (isCM_plus_SR EltB_is_CSR)).
Notation plus_if_zero := (plus_if_zero (isCM_plus_SR EltB_is_CSR)).

Notation fold_sortir := (fold_sortir (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_l := (fold_sortir_l (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_r := (fold_sortir_r (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_l_bis := (fold_sortir_l_bis (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_r_bis := (fold_sortir_r_bis (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_l_plus_l := (fold_sortir_l_plus_l (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_l_plus_r := (fold_sortir_l_plus_r (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_r_plus_l := (fold_sortir_r_plus_l (isCM_plus_SR EltB_is_CSR)).
Notation fold_sortir_r_plus_r := (fold_sortir_r_plus_r (isCM_plus_SR EltB_is_CSR)).

Ltac fold_sortir_if := intros; apply fold_sortir; intros; apply plus_if_zero.
Ltac fold_sortir_2_if := intros; apply fold_sortir; intros; apply fold_sortir; intros; apply plus_if_zero.

(** coeff **)
Lemma fold_sortir_coeff : forall x l f b,
    (forall x a b, Oeset.eq_bool EltB (FePmap.coeff Fm x (f b a)) (plus (FePmap.coeff Fm x b) (FePmap.coeff Fm x (f (FePmap.empty Fm) a))) = true) ->
    Oeset.eq_bool EltB
    (FePmap.coeff _ x (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l b))
    (plus (FePmap.coeff _ x b) (FePmap.coeff Fm x (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l (FePmap.empty _)))) = true.
Proof.
  induction l; intros.
  - simpl.
  rewrite Oeset.eq_bool_sym.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (coeff_empty _))).
  apply plus_zero_r_eq_bool.
  - simpl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl _ _ H)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl _ _ H))).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  apply plus_compat_l_eq_bool.
  apply H.
Qed.


Lemma fold_sortir_coeff_r : forall x x' l f b,
    (forall x' a b, Oeset.eq_bool EltB (FePmap.coeff Fm x' (f b a)) (plus (FePmap.coeff Fm x' b) (FePmap.coeff Fm x' (f (FePmap.empty Fm) a))) = true) ->
    Oeset.eq_bool EltB x (plus (FePmap.coeff _ x' b) (FePmap.coeff Fm x' (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l (FePmap.empty _)))) = true ->
    Oeset.eq_bool EltB x (FePmap.coeff _ x' (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l b)) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  rewrite Oeset.eq_bool_sym.
  now apply fold_sortir_coeff.
Qed.


Lemma fold_sortir_coeff_l_plus_l : forall x x' a l f b,
    (forall x' a b, Oeset.eq_bool EltB (FePmap.coeff Fm x' (f b a)) (plus (FePmap.coeff Fm x' b) (FePmap.coeff Fm x' (f (FePmap.empty Fm) a))) = true) ->
    Oeset.eq_bool EltB (plus a (plus (FePmap.coeff _ x' b) (FePmap.coeff Fm x' (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l (FePmap.empty _))))) x = true ->
    Oeset.eq_bool EltB (plus a (FePmap.coeff _ x' (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l b))) x = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  apply plus_compat_r_eq_bool.
  now apply fold_sortir_coeff.
Qed.


Lemma fold_sortir_coeff_l_plus_r : forall x x' a l f b,
    (forall x' a b, Oeset.eq_bool EltB (FePmap.coeff Fm x' (f b a)) (plus (FePmap.coeff Fm x' b) (FePmap.coeff Fm x' (f (FePmap.empty Fm) a))) = true) ->
    Oeset.eq_bool EltB (plus (plus (FePmap.coeff _ x' b) (FePmap.coeff Fm x' (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l (FePmap.empty _)))) a) x = true ->
    Oeset.eq_bool EltB (plus (FePmap.coeff _ x' (fold_left
       (fun a1 (e1 : A) => (f a1 e1)) l b)) a) x = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  apply plus_compat_l_eq_bool.
  now apply fold_sortir_coeff.
Qed.


Notation add_Pol := (FePmap.union Fm). 

Lemma add_Pol_assoc :
  forall a1 a2 a3, FePmap.compare Fm (add_Pol a1 (add_Pol a2 a3)) (add_Pol (add_Pol a1 a2) a3) = Eq.
Proof.
  intros.
  apply FePmap.compare_spec.
  apply FePmap.equal_spec.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (coeff_union_eq _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (coeff_union_eq _ _ _)).
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (coeff_union_eq _ _ _))).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_l_eq_bool _ _ _ (coeff_union_eq _ _ _))).
  apply plus_assoc_eq_bool.
Qed.

Lemma add_Pol_comm :  forall a1 a2, FePmap.compare Fm (add_Pol a1 a2) (add_Pol a2 a1) = Eq.
Proof.
  intros.
  apply FePmap.compare_spec.
  apply FePmap.equal_spec.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (coeff_union_eq x a1 a2)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (coeff_union_eq x a2 a1)).
  apply plus_comm_eq_bool.
Qed.

Lemma add_Pol_mono :  forall a1 a2 b1 b2,
    FePmap.compare Fm a1 a2 = Eq -> FePmap.compare Fm b1 b2 = Eq ->
    FePmap.compare Fm (add_Pol a1 b1) (add_Pol a2 b2) = Eq.
Proof.
  intros.
  apply FePmap.compare_spec in H.
  apply FePmap.compare_spec in H0.
  apply FePmap.compare_spec.
  apply FePmap.equal_spec.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (coeff_union_eq x a1 b1)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (coeff_union_eq x a2 b2)).
  apply plus_compat_eq_bool; now apply FePmap.equal_spec.
Qed.


Lemma add_Pol_0 : forall a x', FePmap.compare Fm (add_Pol (singleton _ x' zero) a) a = Eq.
Proof.
  intros.
  apply FePmap.compare_spec.
  apply FePmap.equal_spec.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (coeff_union_eq x (singleton _ x' zero) a)).
  rewrite coeff_Pol_0.
  apply plus_zero_l_eq_bool.
Qed.



Lemma Pol0_Pol1_diff : forall x', FePmap.compare Fm (singleton _ x' zero) (singleton _ x' one) <> Eq.
Proof.
  intros x' H.
  apply FePmap.compare_spec in H.
  apply (proj1 (FePmap.equal_spec _ _ _)) with x' in H.
  rewrite 2 singleton_coeff_eq in H; try apply Oeset.eq_bool_refl.
  destruct (one_zero_diff_SR EltB_is_CSR);
  now apply Oeset.eq_bool_true_compare_eq.
Qed.



Notation mul_Pol := (FePmap.mul_Pol Fm).




Lemma eq_suppr_zero : forall (f: A -> bool) l a b1 b,
    Oeset.eq_bool EltB
    (fold_left
       (fun (a2 : B) (e : A) =>
          if f e then plus (mulB a (FePmap.coeff Fm e b1)) a2 else a2)
       l b)
    (fold_left
       (fun (a2 : B) (e : A) =>
          if f e then plus (mulB a (FePmap.coeff Fm e b1)) a2 else a2)
      (filter (fun x => negb (Oeset.eq_bool EltB zero (FePmap.coeff _ x b1))) l) b) = true.
Proof.
  induction l; intros; try apply Oeset.eq_bool_refl.
  simpl.
  case_eq (Oeset.eq_bool EltB zero (FePmap.coeff Fm a b1)); intros; try apply IHl.
  simpl; case (f a); try apply IHl.
  apply fold_sortir_l.
  intros; apply plus_if_zero.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (IHl _ _ _)).
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_zero_l_eq_bool _)).
  apply plus_compat_eq_bool.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (mul_compat_r_eq_bool _ _ _ H)).
  apply mul_zero_r_eq_bool.
  rewrite Oeset.eq_bool_sym.
  fold_sortir_if.
Qed.


Lemma mul_Pol_comm_aux : forall f f' a1 a2,
    (forall e (c0 : B) (e0 : A), Oeset.eq_bool EltB (f c0 e0 e) (plus c0 (f zero e0 e)) = true) ->
    (forall e (c0 : B) (e0 : A), Oeset.eq_bool EltB (f' c0 e0 e) (plus c0 (f' zero e0 e)) = true) ->
    (forall a a0, Oeset.eq_bool EltB (f zero a0 a) (f' zero a a0) = true) ->
   Oeset.eq_bool EltB
    (fold_left
       (fun (a0 : B) (e1 : A) =>
        fold_left
          (fun (a2 : B) (e2 : A) =>
             (f a2 e1 e2))
          (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a2)) a0)
       (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a1)) zero)
    (fold_left
       (fun (a0 : B) (e1 : A) =>
        fold_left
          (fun (a2 : B) (e2 : A) =>
             (f' a2 e1 e2))
          (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a1)) a0)
       (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a2)) zero) = true.
Proof.
  intros.
  induction (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a2)).
  - induction (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a1)); [>apply Oeset.eq_bool_refl|apply IHl].
  - simpl; apply fold_sortir_r.
    intros; now apply fold_sortir.
    rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ IHl)).
    clear IHl.
    induction (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm a1)).
  * rewrite Oeset.eq_bool_sym.
    apply plus_zero_l_eq_bool.
  * simpl.
    apply fold_sortir_l.
    -- intros.
      apply fold_sortir_l; trivial.
      apply fold_sortir_r_plus_l; trivial.
      rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
      now apply plus_compat_l_eq_bool.
    -- apply fold_sortir_l_plus_r; trivial.
      apply fold_sortir_r_plus_r; trivial.
      rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
      apply plus_compat_eq_bool; trivial.
      apply fold_sortir_r_plus_l.
      intros; apply fold_sortir; intros; apply H.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_comm_eq_bool _ _)).
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
      apply plus_compat_r_eq_bool.
      rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_comm_eq_bool _ _)).
      apply IHl0.
Qed.



Lemma fold_left_equiv_s : forall (f : A -> bool) a b1 b2,
    FePmap.equal Fm b1 b2 = true ->
    (forall e1 e2 : A, Oeset.eq_bool EltA e1 e2 = true -> f e1 = f e2) ->
    Oeset.eq_bool EltB
    (fold_left
       (fun (a2 : B) (e : A) =>
        if f e then plus (mulB a (FePmap.coeff Fm e b1)) a2 else a2)
       (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm b1)) zero)
    (fold_left
       (fun (a2 : B) (e : A) =>
        if f e then plus (mulB a (FePmap.coeff Fm e b2)) a2 else a2)
       (Feset.elements (FePmap.FeElt1 Fm) (FePmap.support Fm b2)) zero) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (eq_suppr_zero _ _ _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (eq_suppr_zero _ _ _ _ _)).
  apply (eq_fold_mem_eq EltB_is_CSR EltA); trivial; intros.
  - now apply eq_supp_suppr_zero.
  - rewrite <- (coeff_eq _ _ _ H1).
    now apply equal_spec.
Qed.



Lemma coeff_add_Pol_unfold : forall s b x',
  Oeset.eq_bool EltB
    (FePmap.coeff Fm x'
       (add_Pol s b))
    (plus (FePmap.coeff Fm x' b)
       (FePmap.coeff Fm x'
          (add_Pol s (FePmap.empty Fm)))) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (coeff_union_eq _ _ _)).
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_comm_eq_bool _ _)).
  apply plus_compat_r_eq_bool.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (coeff_union_eq _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (coeff_empty _))). 
  rewrite Oeset.eq_bool_sym.
  apply plus_zero_r_eq_bool.
Qed.

End Sec2.
End Sec.

Notation "s1 '=ME=' s2" := (FePmap.equal _ s1 s2 = true) (at level 70, no associativity).
Notation "s1 '=ME?=' s2" := (FePmap.equal _ s1 s2) (at level 70, no associativity).
Notation "s1 'unionME' s2" := (FePmap.union _ s1 s2) (at level 70, no associativity).
Require Import FiniteMapImpl.


Section Build.
Definition build (elt1 : Type) (elt2 : Type) (Elt1 : Oeset.Rcd elt1) (Elt2 : Oeset.Rcd elt2) (zero : elt2) (plus : elt2 -> elt2 -> elt2) (mul1 : elt1 -> elt1 -> elt1) (mul2 : elt2 -> elt2 -> elt2) (Elt1_is_CIO : CIO mul1 Elt1) :=
  mk_R Elt2 plus mul2 Elt1_is_CIO (Feset.build Elt1) fst (fun x m => if Feset.mem (Feset.build Elt1) x (fst m) then Some (f (snd m) x) else None)
       (@find_spec elt1 elt2 Elt1) (@equiv_eq elt1 elt2 Elt1)
       (@coeff elt1 Elt1 elt2 zero)
       (@coeff_spec1 elt1 Elt1 elt2 zero)
       (@coeff_spec2 elt1 Elt1 elt2 zero)
       (@equal elt1 Elt1 elt2 Elt2 zero)
       (@equal_spec elt1 Elt1 elt2 Elt2 zero)
       (fun e1 e2 => (Feset.singleton (Feset.build Elt1) e1, cst_qf Elt1 e2))
       (@singl_spec elt1 elt2 Elt1)
       (@support_singl elt1 elt2 Elt1)
              (fun e1 e2 m => (Feset.add (Feset.build Elt1) e1 (fst m), add_qf e1 e2 m))
       (@support_add elt1 elt2 Elt1)
       (@spec_add elt1 elt2 Elt1)
       (@spec_add_2 elt1 elt2 Elt1)

       (union_map plus zero)
       (@union_qf_supp elt1 Elt1 elt2 plus)
       (@union_qf_spec elt1 Elt1 elt2 plus zero)
       (Feset.empty (Feset.build Elt1), empty_qf Elt1 zero)
       (eq_refl _)
       (@fold_map elt1 Elt1 elt2)
       (@feset_fold_refl elt1 elt2 Elt1)
       (@transpo_map elt1 Elt1 elt2 zero mul1 mul2 Elt1_is_CIO)
       (fun _ _ _ => eq_refl _)
       (transpo_spec1 zero mul2 Elt1_is_CIO)
       (fun s s' => fold_map (fun x a => union_map plus zero (transpo_map zero mul2 Elt1_is_CIO x (coeff zero x s') s) a) s' (Feset.empty (Feset.build Elt1), empty_qf Elt1 zero))
       (fun _ _ => eq_refl _)
       (compare_fmap Elt2 zero)
       (compare_equal Elt2 zero)
       (compare_fmap_eq_trans Elt2 zero)
       (compare_fmap_eq_lt_trans Elt2 zero)
       (compare_fmap_lt_eq_trans Elt2 zero)
       (compare_fmap_lt_trans Elt2 zero)
       (compare_fmap_lt_gt Elt2 zero).
End Build.      
End FePmap.
