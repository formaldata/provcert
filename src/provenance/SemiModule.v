(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Require Import SemiRing.
Require Import OrderedSet.
Require Import Bool NArith.
Local Open Scope positive_scope.

Record SemiModule (K W : Type) (zeroK oneK : K) (plusK mulK : K -> K -> K) (zeroW : W) 
        (plusW :W -> W -> W) (tensW : K -> W -> W) (OK : Oeset.Rcd K) (OW : Oeset.Rcd W) : Type :=
mk_SM
   {
      isCSR : CSR zeroK oneK plusK mulK OK;
      isCM : CM zeroW plusW OW;
      plus_distr_w : forall k w1 w2,
       eq OW (tensW k (plusW w1 w2)) (plusW (tensW k w1) (tensW k w2));
      absorb_zero_W : forall k, eq OW (tensW k zeroW) zeroW;
      plus_distr_k : forall k1 k2 w,
       eq OW (tensW (plusK k1 k2) w) (plusW (tensW k1 w) (tensW k2 w));
      absorb_zero_K : forall w, eq OW (tensW zeroK w) zeroW;
      mul_assoc_K : forall k1 k2 w,
       eq OW (tensW (mulK k1 k2) w) (tensW k1 (tensW k2 w));
      one_neutral_K : forall w, eq OW (tensW oneK w) w
   }.

Section Nmodule.

Hypothesis M: Type.
Hypothesis zeroM : M.
Hypothesis plusM: M -> M -> M. 
Hypothesis OM : Oeset.Rcd M.
Hypothesis CM_M : CM zeroM plusM OM.

Fixpoint tensorN_aux (p : positive) (x : M) :=
match p with
  |p'~0 => plusM (tensorN_aux p' x) (tensorN_aux p' x)
  |p'~1 => plusM (plusM (tensorN_aux p' x) (tensorN_aux p' x)) x
  |1 => x
end.

Fixpoint tensorN (k : N) (x : M) :=
match k with
|N0 => zeroM
|Npos p => tensorN_aux p x
end.

Lemma distr_add_one_pos : forall p w,
    eq OM (tensorN_aux (Pos.succ p) w) (plusM (tensorN_aux p w) w).
Proof.
  intros.
  induction p; try solve[apply Oeset.compare_eq_refl].
  simpl.
  rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM CM_M _ _ _ _ IHp IHp)).
  rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_assocCM CM_M _ _ _)).
  apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
  rewrite <- (Oeset.compare_eq_2 _ _ _ _ (plus_assocCM CM_M _ _ _)).
  apply (plus_commCM CM_M).
Qed.



Lemma distr_add_carry : forall p0 p w,
(forall (p0 : positive) (w : M),
      eq OM (tensorN_aux (p0 + p) w) (plusM (tensorN_aux p0 w) (tensorN_aux p w))) ->
eq OM (tensorN_aux (Pos.add_carry p0 p) w) (plusM (plusM (tensorN_aux p0 w) (tensorN_aux p w)) w).
Proof.
  intros.
  rewrite Pos.add_carry_spec. 
  rewrite (Oeset.compare_eq_1 _ _ _ _ (distr_add_one_pos _ _)).
  apply (plus_compat_eq_CM CM_M _ _ _ _ (H _ _) (Oeset.compare_eq_refl _ _)).
Qed.


Lemma distr_add_pos : forall p p0 w,
eq OM (tensorN_aux (p0 + p) w) (plusM (tensorN_aux p0 w) (tensorN_aux p w)).
Proof.
  induction p; destruct p0; intros; try solve [apply Oeset.compare_eq_refl]; simpl.
  - apply (distr_add_carry p0 p w) in IHp.
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM CM_M _ _ _ _ IHp IHp)).
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_aux_1 CM_M _ _ _ _)).
    rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_aux_1 CM_M _ _ _ _)).
    apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
    apply (plus_aux_1 CM_M).
  - rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_assocCM CM_M _ _ _)).
    apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
    rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_aux_1 CM_M _ _ _ _)).
    apply (plus_compat_eq_CM CM_M); apply IHp.
  - rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM CM_M _ _ _ _ (distr_add_one_pos _ _) (distr_add_one_pos _ _))).
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_aux_1 CM_M _ _ _ _)).
    rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_commCM CM_M _ _)).
    apply (plus_assocCM CM_M).
  - rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_commCM CM_M _ _)).
    rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_assocCM CM_M _ _ _)).
    apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM CM_M _ _ _ _ (IHp _ _) (IHp _ _))).
    rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_commCM CM_M _ _)).
    apply (plus_aux_1 CM_M).
  - rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM CM_M _ _ _ _ (IHp _ _) (IHp _ _))).
    apply (plus_aux_1 CM_M).
  - apply (plus_commCM CM_M).
  - rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM CM_M _ _ _ _ (distr_add_one_pos _ _) (distr_add_one_pos _ _))).
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_assocCM CM_M _ _ _)).
    apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)).
    apply (plus_assocCM CM_M).
Qed.


Lemma distr_mul_pos : forall p p0 w,
eq OM (tensorN_aux (p * p0) w) (tensorN_aux p (tensorN_aux p0 w)).
Proof.
  induction p; intros.
  - simpl. rewrite (Oeset.compare_eq_1 _ _ _ _ (distr_add_pos _ _ _)).
    rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_commCM CM_M _ _)).
    apply (plus_compat_eq_CM CM_M _ _ _ _ (Oeset.compare_eq_refl _ _)).
    rewrite <- Pos.add_diag.
    rewrite (Oeset.compare_eq_1 _ _ _ _ (distr_add_pos _ _ _)).
    apply (plus_compat_eq_CM CM_M _ _ _ _ (IHp _ _) (IHp _ _)).
  - apply (plus_compat_eq_CM CM_M _ _ _ _ (IHp _ _) (IHp _ _)).
  - apply Oeset.compare_eq_refl.
Qed.

    

Lemma NArithSM :
  SemiModule N M N.zero N.one N.add N.mul zeroM plusM tensorN TN OM.
Proof.
  split; intros.
  - apply N_is_CSR.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_sym.
      apply CM_M.
    * induction p.
      -- simpl.
         rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_assocCM CM_M _ _ _)).
         rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_assocCM CM_M _ _ _)).
         apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
         rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_commCM CM_M _ _)).
         rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_assocCM CM_M _ _ _)).
         apply (plus_compat_eq_CM CM_M); try apply Oeset.compare_eq_refl.
         rewrite (Oeset.compare_eq_2 _ _ _ _ (plus_commCM CM_M _ _)).
         rewrite (Oeset.compare_eq_2 _ _ _ _ ((plus_aux_1 CM_M) _ _ _ _)).
         apply (plus_compat_eq_CM CM_M); apply IHp.
      -- simpl. rewrite (Oeset.compare_eq_2 _ _ _ _ ((plus_aux_1 CM_M) _ _ _ _)).
         apply (plus_compat_eq_CM CM_M); apply IHp.
      -- apply Oeset.compare_eq_refl.
  - case k.
    * apply Oeset.compare_eq_refl.
    * induction p.
      -- apply Oeset.compare_eq_trans with (plusM zeroM zeroM).
         apply (M_add_compat_r CM_M).
         apply Oeset.compare_eq_trans with (plusM zeroM zeroM); apply CM_M; trivial.
         apply CM_M.
      -- apply Oeset.compare_eq_trans with (plusM zeroM zeroM); apply CM_M; trivial.
      -- apply Oeset.compare_eq_refl.
  - case k1.
    * apply Oeset.compare_eq_sym.
      apply CM_M.
    * case k2.
      -- intros.
         apply Oeset.compare_eq_sym.
         simpl. 
         rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)).
         apply CM_M.
      -- intros. apply distr_add_pos.
  - apply Oeset.compare_eq_refl.
  - case k1. 
    * apply Oeset.compare_eq_refl.
    * intros. case k2; intros.
      -- simpl. apply Oeset.compare_eq_sym.
          induction p.
         ** simpl.
            rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_zero_rightCM CM_M _)).
            apply (plus_equal_zeroCM CM_M _ _ IHp IHp).
         ** apply (plus_equal_zeroCM CM_M _ _ IHp IHp).
         ** apply Oeset.compare_eq_refl.
      -- apply distr_mul_pos.
  - apply Oeset.compare_eq_refl.
Qed.

End Nmodule.


Section Bmodule.

Hypothesis M: Type.
Hypothesis zeroM : M.
Hypothesis plusM: M -> M -> M. 
Hypothesis OM : Oeset.Rcd M.
Hypothesis CM_M : CM zeroM plusM OM.

Hypothesis idempotent_plus : forall x,
  eq OM (plusM x x) x.


Definition tensorBool (b : bool) (x : M) :=
  match b with 
  |false => x
  |true => zeroM
  end.

Definition tensorBool2 (b : bool) (x : M) :=
  match b with 
  |true => x
  |false => zeroM
  end.


Lemma BoolSM1 :
  SemiModule bool M true false andb orb zeroM plusM tensorBool Tbool OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR1.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_sym; apply CM_M.
    * apply Oeset.compare_eq_refl.
  - case k; apply Oeset.compare_eq_refl.
  - case k1.
    * apply Oeset.compare_eq_sym. apply CM_M.
    * case k2.
      -- apply Oeset.compare_eq_sym.
         rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
      -- apply Oeset.compare_eq_sym. apply idempotent_plus.
  - case k1; apply Oeset.compare_eq_refl.
Qed.



Lemma BoolSM2 :
SemiModule bool M false true orb andb zeroM plusM tensorBool2 Tbool OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR2.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_refl.
    * apply Oeset.compare_eq_sym; apply CM_M.
  - case k; apply Oeset.compare_eq_refl.
  - case k1.
    * case k2.
      -- apply Oeset.compare_eq_sym. apply idempotent_plus.
      -- apply Oeset.compare_eq_sym.
         rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
    * apply Oeset.compare_eq_sym. apply CM_M.
  - case k1; apply Oeset.compare_eq_refl.
Qed.


Lemma BoolSM3 :
SemiModule bool M true false andb orb zeroM plusM tensorBool Tbool2 OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR1.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_sym; apply CM_M.
    * apply Oeset.compare_eq_refl.
  - case k; apply Oeset.compare_eq_refl.
  - case k1.
    * apply Oeset.compare_eq_sym. apply CM_M.
    * case k2.
      -- apply Oeset.compare_eq_sym.
         rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
      -- apply Oeset.compare_eq_sym. apply idempotent_plus.
  - case k1; apply Oeset.compare_eq_refl.
Qed.



Lemma BoolSM4 :
SemiModule bool M false true orb andb zeroM plusM tensorBool2 Tbool2 OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR2.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_refl.
    * apply Oeset.compare_eq_sym; apply CM_M.
  - case k; apply Oeset.compare_eq_refl.
  - case k1.
    * case k2.
      -- apply Oeset.compare_eq_sym. apply idempotent_plus.
      -- apply Oeset.compare_eq_sym.
         rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
    * apply Oeset.compare_eq_sym. apply CM_M.
  - case k1; apply Oeset.compare_eq_refl.
Qed.

End Bmodule.


Section Bmodule2.


Hypothesis M: Type.
Hypothesis zeroM : M.
Hypothesis plusM: M -> M -> M. 
Hypothesis OM : Oeset.Rcd M.
Hypothesis CM_M : CM zeroM plusM OM.

Hypothesis noyau_plus : forall x,
  eq OM (plusM x x) zeroM.

Notation tensorBool := (tensorBool M zeroM).
Notation tensorBool2 := (tensorBool2 M zeroM).

Lemma BoolSM5 :
SemiModule bool M true false eqb orb zeroM plusM tensorBool Tbool OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR3.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_sym; apply CM_M.
    * apply Oeset.compare_eq_refl.
  - case k; apply Oeset.compare_eq_refl.
  - case k1; case k2; apply Oeset.compare_eq_sym; try solve [apply CM_M].
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
    apply noyau_plus.
  - case k1; apply Oeset.compare_eq_refl.
Qed.



Lemma BoolSM6 :
SemiModule bool M false true xorb andb zeroM plusM tensorBool2 Tbool OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR4.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_refl.
    * apply Oeset.compare_eq_sym; apply CM_M.
  - case k; apply Oeset.compare_eq_refl.
  - case k1; case k2; apply Oeset.compare_eq_sym; try solve [apply CM_M].
    apply noyau_plus.
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
  - case k1; apply Oeset.compare_eq_refl.
Qed.


Lemma BoolSM7 :
SemiModule bool M true false eqb orb zeroM plusM tensorBool Tbool2 OM.
Proof.
  split; intros; try solve [apply Oeset.compare_eq_refl].
  - apply bool_is_CSR3.
  - apply CM_M.
  - case k.
    * apply Oeset.compare_eq_sym; apply CM_M.
    * apply Oeset.compare_eq_refl.
  - case k; apply Oeset.compare_eq_refl.
  - case k1; case k2; apply Oeset.compare_eq_sym; try solve [apply CM_M].
    rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM CM_M _ _)); apply CM_M.
    apply noyau_plus.
  - case k1; apply Oeset.compare_eq_refl.
Qed.



Lemma BoolSM8 :
SemiModule bool M false true xorb andb zeroM plusM tensorBool2 Tbool2 OM.
Proof.
split.
- apply bool_is_CSR4.
- apply CM_M.
- intros. case k.
  * apply Oeset.compare_eq_refl.
  * apply Oeset.compare_eq_sym.
  apply CM_M.
- intros. case k; apply Oeset.compare_eq_refl.
- intros. case k1.
  * case k2.
    -- apply Oeset.compare_eq_sym. apply noyau_plus.
    -- apply Oeset.compare_eq_sym.
    apply Oeset.compare_eq_trans with (plusM zeroM w); apply CM_M.
  * apply Oeset.compare_eq_sym. case k2; apply CM_M.
- intros. apply Oeset.compare_eq_refl.
- intros. case k1; apply Oeset.compare_eq_refl.
- intros. apply Oeset.compare_eq_refl.
Qed.

End Bmodule2.
