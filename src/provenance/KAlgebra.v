(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.
Require Import Bool List Arith NArith FlatData SemiRing SemiModule SqlAlgebra.

Require Import BasicTacs BasicFacts ListFacts Facts ListPermut ListSort OrderedSet
        FiniteSet FiniteBag FiniteCollection Data Join FlatData Tree Term Bool3 Formula 
        Partition Sql.
Import Tuple.

Section KAlgebra.
Hypothesis T : Tuple.Rcd.
Hypothesis relname : Type.
Hypothesis ORN : Oset.Rcd relname.
Notation predicate := (Tuple.predicate T).
Notation symb := (Tuple.symbol T).
Notation aggregate := (Tuple.aggregate T).
Notation OPredicate := (Tuple.OPred T).
Notation OSymb := (Tuple.OSymb T).
Notation OAggregate := (Tuple.OAgg T).
Notation BT := (B T).

Notation value := (value T).
Notation attribute := (attribute T).
Notation tuple := (tuple T).
Arguments funterm {T}.
Arguments aggterm {T}.
Arguments Select_Star {T}.
Arguments Select_As {T}.
Arguments Select_List {T}.
Arguments _Select_List {T}.
Arguments Group_Fine {T}.
Arguments Group_By {T}.
Arguments F_Dot {T}.
Arguments A_Expr {T}.

Hypothesis unknown : Bool.b (B T).
Hypothesis contains_nulls : tuple -> bool.
Hypothesis contains_nulls_eq : forall t1 t2, t1 =t= t2 -> contains_nulls t1 = contains_nulls t2.
Notation query := (query T relname).

Notation eval_sql_formula := (eval_sql_formula unknown contains_nulls).
Notation setA := (Fset.set (A T)).
Notation BTupleT := (Fecol.CBag (CTuple T)).
Notation bagT := (Febag.bag BTupleT).

Notation interp_funterm := (interp_funterm T).
Notation interp_aggterm := (interp_aggterm T).
Hypothesis basesort : relname -> Fset.set (Tuple.A T).
Notation sort := (sort basesort).
Hypothesis instance_rel : relname -> bagT.
Notation make_groups := 
  (fun env b => @make_groups T env (Febag.elements (Fecol.CBag (CTuple T)) b)).

Notation listT := (list (Tuple.tuple T)).

Notation tupleT := (Tuple.tuple T).
Notation FesetT := (Feset.build (OTuple T)).
Notation setT := (Feset.set FesetT).

Notation eval_query_rel := 
(eval_query basesort instance_rel unknown contains_nulls).
Notation sql_formula := (sql_formula T query). 

Ltac bool_dec_rewrite a H := destruct (bool_dec a true) as [H|H]; [> |apply not_true_is_false in H]; rewrite H.

Section Sec.
Hypothesis K : Type.
Hypothesis zeroK oneK : K.
Hypothesis plusK mulK : K -> K -> K.
Hypothesis OK : Oeset.Rcd K.
Hypothesis SR : SemiRing.CSR zeroK oneK plusK mulK OK.

Notation FesetK := (Feset.build OK).
Notation setK := (Feset.set FesetK).
Notation mul_one_l := (plus_zero_leftCM (isCM_mul_SR SR)).
Notation mul_one_r := (plus_zero_rightCM (isCM_mul_SR SR)).
Notation mul_compat_l := (M_add_compat_l (isCM_mul_SR SR)).
Notation mul_compat_r := (M_add_compat_r (isCM_mul_SR SR)).
Notation plus_compat_l := (M_add_compat_l (isCM_plus_SR SR)).
Notation plus_compat_r := (M_add_compat_r (isCM_plus_SR SR)).
Notation plus_compat := (plus_compat_eq_CM (isCM_plus_SR SR)).
Notation mul_0_r := (mul_zero_absorb_right_SR SR).
Notation mul_0_l := (mul_zero_absorb_left_SR SR).
Notation plus_0_l := (plus_zero_leftCM (isCM_plus_SR SR)).
Notation plus_0_r := (plus_zero_rightCM (isCM_plus_SR SR)).
Notation mul_one_l_eq_bool := (SemiRing.plus_zero_l_eq_bool (isCM_mul_SR SR)).
Notation mul_one_r_eq_bool := (SemiRing.plus_zero_r_eq_bool (isCM_mul_SR SR)).
Notation mul_zero_l_eq_bool := (SemiRing.mul_zero_l_eq_bool SR).
Notation mul_zero_r_eq_bool := (SemiRing.mul_zero_r_eq_bool SR).
Notation plus_compat_eq_bool := (plus_compat_eq_bool (isCM_plus_SR SR)).
Notation plus_compat_r_eq_bool := (plus_compat_r_eq_bool (isCM_plus_SR SR)).
Notation mul_compat_eq_bool := (SemiRing.plus_compat_eq_bool (isCM_mul_SR SR)).
Notation mul_compat_l_eq_bool := (SemiRing.plus_compat_l_eq_bool (isCM_mul_SR SR)).
Notation mul_compat_r_eq_bool := (SemiRing.plus_compat_r_eq_bool (isCM_mul_SR SR)).

Record Krel : Type :=
   mk_kr
    {
      f : tupleT -> K;
      support : setT;
      sort_krel : Fset.set (A T);
    }.

Record Krel_inv (kr:Krel) : Prop :=
   mk_kri
     {
      finite_support : forall t, Feset.mem FesetT t (support kr) = false -> eq OK (f kr t) zeroK;
      sort_labels_supp : forall t, t inSE (support kr) -> labels T t =S= sort_krel kr;
     }.

Hypothesis instance_prov : relname -> Krel.

Hypothesis instance_prov_inv : forall r, Krel_inv (instance_prov r).

Hypothesis instance_support : forall r,
    sort_krel (instance_prov r) = basesort r.

Hypothesis instance_prov_equiv : forall r x1 x2,
 eq (OTuple T) x1 x2 -> ((instance_prov r).(f) x1) = ((instance_prov r).(f) x2).

Definition env_slice_prov := (group_by T * Krel)%type.
Definition env_prov := list env_slice_prov.


Definition filter_zero kr :=
  Feset.filter FesetT (fun t => negb (Oeset.eq_bool OK (f kr t) zeroK)) (support kr).

Fixpoint interp_dot_prov (env : env_prov) (a : Tuple.attribute T) {struct env} : Tuple.value T :=
  match env with
  | nil => default_value T (type_of_attribute T a)
  |(_,kr) ::env0 =>
   match Feset.elements FesetT (filter_zero kr) with
       |nil => interp_dot_prov env0 a
       |t :: _ =>
          if a inS? labels T t
          then dot T t a
          else interp_dot_prov env0 a
      end
  end.
Lemma interp_dot_prov_unfold : forall env a,
    interp_dot_prov env a =
    match env with
    | nil => default_value T (type_of_attribute T a)
    |(_,kr) ::env0 =>
     match Feset.elements FesetT (filter_zero kr) with
     |nil => interp_dot_prov env0 a
     |t :: _ =>
      if a inS? labels T t
      then dot T t a
      else interp_dot_prov env0 a
     end
    end.
Proof.
  intros; destruct env; trivial.
Qed.


Definition groups_of_env_slice_prov (s : env_slice_prov) :=
  let (g0,kr) := s in
  match g0 with
  | Group_By g => g
  | Group_Fine => map (fun a : Tuple.attribute T => A_Expr (F_Dot a)) ({{{sort_krel kr}}})
  end.


Definition is_a_suitable_env_prov (kr : Krel) (env : env_prov) (f : funterm) :=
  is_built_upon_ft T (map (fun a : Tuple.attribute T => F_Dot a) ({{{sort_krel kr}}}) ++ extract_funterms T (flat_map groups_of_env_slice_prov env)) f.

Fixpoint find_eval_env_prov (env : env_prov) (f : funterm) {struct env} :=
  match env with
  | nil => if is_built_upon_ft T nil f then Some nil else None
  | (Group_By _, kr) :: env' =>
      match find_eval_env_prov env' f with
      | Some l1 => Some l1
      | None => if is_a_suitable_env_prov kr env' f then Some env else None
      end
  | (Group_Fine, kr) :: env' => find_eval_env_prov env' f
  end.

Fixpoint interp_funterm_prov (env : env_prov) (t : funterm) {struct t} : Tuple.value T :=
  match t with
  | F_Constant _ c => c
  | F_Dot a => interp_dot_prov env a
  | F_Expr _ f l => interp_symbol T f (map (fun x : funterm => interp_funterm_prov env x) l)
  end.

Lemma interp_funterm_prov_unfold : forall (env : env_prov) (t : funterm),
    interp_funterm_prov env t =
  match t with
  | F_Constant _ c => c
  | F_Dot a => interp_dot_prov env a
  | F_Expr _ f l => interp_symbol T f (map (fun x : funterm => interp_funterm_prov env x) l)
  end.
Proof.
  intros. destruct t; trivial.
Qed.

Definition unfold_env_slice_prov (s : env_slice_prov) :=
  let kr := snd s in
  map (fun t0 : Tuple.tuple T => ((@Group_Fine T, mk_kr (fun t => if Oeset.eq_bool (OTuple T) t t0 then oneK else zeroK) (Feset.add FesetT t0 (Feset.empty FesetT)) (sort_krel kr), f kr t0))) (Feset.elements FesetT (filter_zero kr)).

Hypothesis interp_aggregate_prov : Tuple.aggregate T -> list (Tuple.value T * K) -> Tuple.value T.



Fixpoint interp_aggterm_prov (env : env_prov) (agt : aggterm) {struct agt} : Tuple.value T :=
  match agt with
  | A_Expr ft => interp_funterm_prov env ft
  | A_agg _ ag ft =>
    let env' := if Fset.is_empty (A T) (variables_ft T ft) then Some env else find_eval_env_prov env ft in
    let lenv :=
        match env' with
        | Some (slc1 :: env'') => map (fun slc : (env_slice_prov * K) => (fst slc :: env'', snd slc)) (unfold_env_slice_prov slc1)
        | _ => nil
        end in
    interp_aggregate_prov ag (map (fun e : (env_prov * K) => (interp_funterm_prov (fst e) ft, snd e)) lenv)
  | A_fun _ f lag => interp_symbol T f (map (fun x : aggterm => interp_aggterm_prov env x) lag)
  end.

Lemma interp_aggterm_prov_unfold : forall (env : env_prov) (agt : aggterm),
    interp_aggterm_prov env agt =
  match agt with
  | A_Expr ft => interp_funterm_prov env ft
  | A_agg _ ag ft =>
    let env' := if Fset.is_empty (A T) (variables_ft T ft) then Some env else find_eval_env_prov env ft in 
    let lenv :=
        match env' with
        | Some (slc1 :: env'') => map (fun slc : (env_slice_prov * K) => (fst slc :: env'', snd slc)) (unfold_env_slice_prov slc1)
        | _ => nil
        end in
    interp_aggregate_prov ag (map (fun e : (env_prov * K) => (interp_funterm_prov (fst e) ft, snd e)) lenv)
  | A_fun _ f lag => interp_symbol T f (map (fun x : aggterm => interp_aggterm_prov env x) lag)
  end.
Proof.
  intros. induction agt; trivial.
Qed.

Definition projection_prov (env : env_prov) (las : select_item T) := 
match las with
| Select_Star =>
    match env with
    | nil => default_tuple T (emptysetS)
    | (_, kr) :: _ => match Feset.elements FesetT (filter_zero kr) with
                        | nil => default_tuple T (emptysetS)
                        | t :: _ => t
                        end
    end
| Select_List las0 =>
    mk_tuple T
      ((fun las1 : _select_list T =>
        Fset.mk_set (A T) match las1 with
                          | _Select_List las2 => map (fun x : select T => match x with
                                                                          | Select_As _ a => a
                                                                          end) las2
                          end) las0)
      (fun a : Tuple.attribute T =>
       match Oset.find (OAtt T) a las0 with
       | Some e => interp_aggterm_prov env e
       | None => dot T (default_tuple T (emptysetS)) a
       end)
end.

Lemma projection_prov_unfold :
  forall (env : env_prov) (las : select_item T),
    projection_prov env las =
    match las with
    | Select_Star =>
      match env with
      | nil => default_tuple T (emptysetS)
      | (_, kr) :: _ => match Feset.elements FesetT (filter_zero kr) with
                           | nil => default_tuple T (emptysetS)
                           | t :: _ => t
                           end
      end
    | Select_List las0 =>
      mk_tuple T
               ((fun las1 : _select_list T =>
                   Fset.mk_set (A T) match las1 with
                                     | _Select_List las2 => map (fun x : select T => match x with
                                                                                     | Select_As _ a => a
                                                                                     end) las2
                                     end) las0)
               (fun a : Tuple.attribute T =>
                  match Oset.find (OAtt T) a las0 with
                  | Some e => interp_aggterm_prov env e
                  | None => dot T (default_tuple T (emptysetS)) a
                  end)
    end.
Proof.
  intros.
  case las; case env; simpl; trivial.
Qed.

Section Formula.
  Hypothesis I : env_prov -> query -> Krel.

  Fixpoint eval_formula_prov (env: env_prov) (form: sql_formula) {struct form}: Bool.b BT :=
    match form with
    | Sql_Conj a f1 f2 => (interp_conj BT a) (eval_formula_prov env f1) (eval_formula_prov env f2)
    | Sql_Not f => Bool.negb BT (eval_formula_prov env f)
    | Sql_True _ _ => Bool.true BT
    | Sql_Exists _ sq =>
      let kr := (I env sq) in
      if Feset.exists_ _ (fun t => negb (Oeset.eq_bool OK (f kr t) (zeroK))) (support kr)
      then Bool.true BT
      else Bool.false BT
    | Sql_Pred _ p l => interp_predicate T p (map (interp_aggterm_prov env) l)
    | Sql_Quant qtf p l sq =>
      let kr := (I env sq) in
      let lt := map (interp_aggterm_prov env) l in
      interp_quant BT qtf
                   (fun x =>
                      let la := Fset.elements _ (labels T x) in
                      interp_predicate T p (lt ++ map (dot T x) la))
                   (Feset.elements FesetT (filter_zero kr))
    | Sql_In s sq =>
      let kr := (I env sq) in
      let p := (projection_prov env (Select_List (_Select_List s))) in
      interp_quant
        BT Exists_F
        (fun x => match Oeset.compare (OTuple T) p x with
                  | Eq => if contains_nulls p then unknown else Bool.true BT
                  | _ => if (contains_nulls p || contains_nulls x) then unknown else Bool.false BT
                  end)
        (Feset.elements FesetT (filter_zero kr))
  end.

Lemma eval_formula_prov_unfold : forall env (form: sql_formula),
    eval_formula_prov env form =
  match form with
  | Sql_Conj a f1 f2 => (interp_conj BT a) (eval_formula_prov env f1) (eval_formula_prov env f2)
  | Sql_Not f => Bool.negb BT (eval_formula_prov env f)
  | Sql_True _ _ => Bool.true BT
  | Sql_Exists _ sq =>
    if Feset.exists_ _ (fun t => negb (Oeset.eq_bool OK (f (I env sq) t) (zeroK))) (support (I env sq))
    then Bool.true BT
    else Bool.false BT
  | Sql_Pred _ p l => interp_predicate T p (map (interp_aggterm_prov env) l)
  | Sql_Quant qtf p l sq =>
    let kr := (I env sq) in
    let lt := map (interp_aggterm_prov env) l in
    interp_quant BT qtf
                 (fun x =>
                    let la := Fset.elements _ (labels T x) in
                    interp_predicate T p (lt ++ map (dot T x) la))
                 (Feset.elements FesetT (filter_zero kr))
  | Sql_In s sq =>
    let kr := (I env sq) in
    let p := (projection_prov env (Select_List (_Select_List s))) in
    interp_quant
      BT Exists_F
      (fun x => match Oeset.compare (OTuple T) p x with
                | Eq => if contains_nulls p then unknown else Bool.true BT
                | _ => if (contains_nulls p || contains_nulls x) then unknown else Bool.false BT
                end)
      (Feset.elements FesetT (filter_zero kr))
  end.
Proof.
intros; case form; intros; trivial.
Qed.


Definition env_pt (env : env_prov) (t : Tuple.tuple T) :=
  (Group_Fine, mk_kr (fun t0 => if Oeset.eq_bool (OTuple T) t0 t then oneK else zeroK) (Feset.add FesetT t (emptysetSE)) (labels T t)) :: env.

(*K-relation : selection*)
Notation f_sigma kr env form :=
(fun t => mulK (
      match (Bool.is_true BT (eval_formula_prov (env_pt env t) form)) with
      | true => oneK
      | false => zeroK
      end) (f kr t)).

Lemma finite_supp_sigma : forall (kr: Krel)  (env :env_prov) form,
    Krel_inv kr ->
    forall t, Feset.mem FesetT t (support kr) = false -> eq OK (f_sigma kr env form t) zeroK.
Proof.
  intros.
  apply (finite_support H) in H0.
  simpl. case (Bool.is_true BT (eval_formula_prov (env_pt env t) form)); simpl.
  apply (Oeset.compare_eq_trans _ _ _ _ (mul_compat_l _ _ _ H0)).
  apply mul_one_l.
  apply mul_0_r.
Qed.
    
Definition Krel_sigma kr (env : env_prov) form :=
  mk_kr (f_sigma kr env form) (support kr) (sort_krel kr).

Lemma Krel_sigma_Krel_inv : forall kr env form,
    Krel_inv kr -> Krel_inv (Krel_sigma kr env form).
Proof.
  intros. split; intros; [>apply (finite_supp_sigma _ _ H _ H0)|apply (sort_labels_supp H _ H0)].
Qed.

End Formula.


(*K-relation : empty tuple*)
Notation f_empty_tuple :=
  (fun t => if Oeset.eq_bool (OTuple T) t (empty_tuple T) then oneK else zeroK).

Notation support_empty_tuple := (Feset.singleton FesetT (empty_tuple T)).

Lemma finite_supp_empty_tuple : forall t,
    Feset.mem FesetT t support_empty_tuple  = false -> eq OK (f_empty_tuple t) zeroK.
Proof.
  intros. rewrite Feset.singleton_spec in H.
  simpl. rewrite H; apply Oeset.compare_eq_refl.
Qed.

Lemma labels_supp_empty_tuple :
  forall t : tupleT, t inSE support_empty_tuple -> labels T t =S= (emptysetS).
Proof.
  intros.
  rewrite Feset.singleton_spec in H.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply tuple_eq_labels in H.
  apply Fset_equal_trans with (labels T (empty_tuple T)); trivial.
  apply labels_empty_tuple.
 Qed.


Definition Krel_empty_tuple :=
  mk_kr f_empty_tuple support_empty_tuple (emptysetS).

Lemma Krel_empty_tuple_Krel_inv : Krel_inv Krel_empty_tuple.
Proof.
  split;[> apply finite_supp_empty_tuple | apply labels_supp_empty_tuple].
Qed.

(*K-relation : empty*)

Notation f_empty := (fun _: tupleT => zeroK).

Lemma finite_supp_empty : forall t,
    Feset.mem FesetT t (Feset.empty FesetT)  = false -> eq OK (f_empty t) zeroK.
Proof.
  intros. apply Oeset.compare_eq_refl.
Qed.

Lemma labels_supp_empty :
  forall t, t inSE (Feset.empty FesetT) -> labels T t =S= (Fset.empty (A T)).
Proof.
intros. rewrite Feset.mem_empty in H. inversion H.
Qed.

Definition Krel_empty :=
  mk_kr f_empty (Feset.empty FesetT) (Fset.empty (A T)).

Lemma Krel_empty_Krel_inv : Krel_inv Krel_empty.
Proof.
  split;[> apply finite_supp_empty | apply labels_supp_empty].
Qed.
  

Lemma labels_supp_empty_krel : forall kr,
  forall t, t inSE (Feset.empty FesetT) -> labels T t =S= (sort_krel kr).
Proof.
intros. rewrite Feset.mem_empty in H. inversion H.
Qed.

Definition Krel_empty_sort_krel s :=
  mk_kr f_empty (Feset.empty FesetT) s.

Lemma K_empty_sort_Krel_inv : forall s,
    Krel_inv (Krel_empty_sort_krel s).
Proof.
  intros.
  split; [> apply finite_supp_empty|apply labels_supp_empty_krel].
Qed.


(*K-relation : union*)

Notation f_set kr1 kr2 s :=
  (if sort_krel kr1 =S?= sort_krel kr2 then
     match s with
     |Union => fun t => plusK (kr1.(f) t) (kr2.(f) t)
     |_ => fun _ => zeroK
     end
   else fun _ => zeroK).


Notation support_set kr1 kr2 s :=
   (if sort_krel kr1 =S?= sort_krel kr2 then
     match s with
     |Union => Feset.union FesetT (kr1.(support)) (kr2.(support))
     |_ => Feset.empty FesetT
     end
   else Feset.empty FesetT).


Definition Krel_set kr1 kr2 s :=
  mk_kr (f_set kr1 kr2 s) (support_set kr1 kr2 s) (sort_krel kr1).


Lemma finite_supp_set : forall kr1 kr2 s,
    Krel_inv kr1 -> Krel_inv kr2 ->
    forall t, Feset.mem FesetT t (support_set kr1 kr2 s) = false ->
              eq OK ((f_set kr1 kr2 s) t) zeroK.
Proof.
  intros kr1 kr2 s Hk1 Hk2 t. case s; try solve [intros; rewrite if_same; apply Oeset.compare_eq_refl].
  case (sort_krel kr1 =S?= sort_krel kr2); intros.
  rewrite Feset.mem_union in H.
  apply orb_false_elim in H as [H1 H2].
  apply (Oeset.compare_eq_trans _ _ _ _ (plus_compat _ _ _ _ (finite_support Hk1 _ H1) (finite_support Hk2 _ H2))).
  apply plus_0_l.
  apply Oeset.compare_eq_refl.
Qed.




Lemma labels_supp_set : forall kr1 kr2 s,
    Krel_inv kr1 -> Krel_inv kr2 ->
    forall t, t inSE (support_set kr1 kr2 s) -> labels T t =S= (sort_krel kr1).
Proof.
  intros kr1 kr2 s Hk1 Hk2 t; case s; case_eq (sort_krel kr1 =S?= sort_krel kr2); intros; try solve [rewrite Feset.mem_empty in H0; inversion H0].
  rewrite Feset.mem_union in H0.
  apply orb_prop in H0 as [H1|H1].
  apply (sort_labels_supp Hk1) in H1; trivial.
  apply (sort_labels_supp Hk2) in H1.
  apply Fset.equal_true_sym in H.
  apply (Fset_equal_trans _ _ _ _ H1 H).
Qed.


Lemma Krel_set_Krel_inv : forall kr1 kr2 s,
    Krel_inv kr1 -> Krel_inv kr2 -> Krel_inv (Krel_set kr1 kr2 s).
Proof.
  intros; split; [> apply (finite_supp_set _ H H0)|apply (labels_supp_set _ H H0)].
Qed.




(*K-relation : natural join*)
Notation mul_Krel kr1 kr2:=
  (fun t =>
      mulK
        (mulK 
          (kr1.(f) (mk_tuple T (sort_krel kr1) (dot T t)))
          (kr2.(f) (mk_tuple T (sort_krel kr2) (dot T t))))
        (if labels T t =S?= (sort_krel kr1 unionS sort_krel kr2) then oneK else zeroK)).

  
Notation support_nj kr1 kr2 :=
  (Feset.mk_set _ (natural_join_list T (Feset.elements _ kr1.(support)) (Feset.elements _ kr2.(support)))).

Notation sort_nj kr1 kr2 := (sort_krel kr1 unionS sort_krel kr2).

Definition Krel_nj kr1 kr2 :=
  mk_kr (mul_Krel kr1 kr2) (support_nj kr1 kr2) (sort_nj kr1 kr2).


Lemma labels_supp_nj : forall kr1 kr2,
    Krel_inv kr1 -> Krel_inv kr2 ->
    forall t, Feset.mem FesetT t (support_nj kr1 kr2) = true -> labels T t =S= sort_nj kr1 kr2.
Proof.
  intros kr1 kr2 Hk1 Hk2 t H. rewrite Feset.mem_mk_set in H.
  apply Oeset.mem_bool_true_iff in H as [a [H0 H1]].
  apply in_natural_join_list in H1 as [x1[x2[H1[H2[H3 H4]]]]].
  apply Feset.in_elements_mem in H1. apply Feset.in_elements_mem in H2.
  apply (sort_labels_supp Hk1) in H1.
  apply (sort_labels_supp Hk2) in H2.
  apply (split_tuple _ (sort_krel kr1) (sort_krel kr2) t _ _ H1 H2) in H3.
  rewrite <- H4 in H3. apply H3 in H0. apply H0.
Qed.

Lemma finite_supp_nj : forall kr1 kr2,
    Krel_inv kr1 -> Krel_inv kr2 ->
    forall t, Feset.mem FesetT t (support_nj kr1 kr2) = false ->
              eq OK ((mul_Krel kr1 kr2) t) zeroK.
Proof.
  intros kr1 kr2 Hk1 Hk2. intros. rewrite Feset.mem_mk_set in H.
  apply Oeset.not_mem_nb_occ in H.
  rewrite (nb_occ_natural_join_list _ _ _ (sort_krel kr1) (sort_krel kr2)) in H.
  repeat (apply N.eq_mul_0 in H as [H|H]); try apply Oeset.nb_occ_not_mem in H;
    try rewrite <- Feset.mem_elements in H.
  - apply (finite_support Hk1) in H.
    apply (Oeset.compare_eq_trans _ _ _ _ (mul_compat_r _ _ _ (mul_compat_r _ _ _ H))).
    apply (Oeset.compare_eq_trans _ _ _ _ (mul_compat_r _ _ _ (mul_0_r _))).
    apply mul_0_r.
  - apply (finite_support Hk2) in H.
    apply (Oeset.compare_eq_trans _ _ _ _ (mul_compat_r _ _ _ (mul_compat_l _ _ _ H))).
    apply (Oeset.compare_eq_trans _ _ _ _ (mul_compat_r _ _ _ (mul_0_l _))).
    apply mul_0_r.
  - revert H; case (labels T t =S?= (sort_krel kr1 unionS sort_krel kr2)); intros. inversion H.
    apply mul_0_l.
  - intros. apply Feset.in_elements_mem in H0. apply (sort_labels_supp Hk1 _ H0).
  - intros. apply Feset.in_elements_mem in H0. apply (sort_labels_supp Hk2 _ H0).
Qed.


Lemma Krel_nj_Krel_inv : forall kr1 kr2,
    Krel_inv kr1 -> Krel_inv kr2 -> Krel_inv (Krel_nj kr1 kr2).
Proof.
intros; split; [> apply (finite_supp_nj H H0)|apply (labels_supp_nj H H0)]. 
Qed.

(*K-relation : projection*)

Definition f_pi kr (env:env_prov) s :=
  (fun t' =>
     (fold_left (fun x y => plusK ((f kr) y) x)
                 (filter (fun t => Oeset.eq_bool (OTuple T) t' (projection_prov (env_pt env t) (Select_List s))) (Feset.elements _ (support kr))) zeroK)).


Definition support_pi kr (env:env_prov) s:= (Feset.map FesetT FesetT (fun t => projection_prov (env_pt env t) (Select_List s)) kr.(support)).

Lemma fold_sum_init_eq : forall A (l : list A) f a1 a2,
    eq OK a1 a2 -> (forall a1 a2 a, eq OK a1 a2 -> eq OK (f a1 a) (f a2 a)) ->
    eq OK (fold_left f l a1) (fold_left f l a2).
Proof.
  intros A l f. induction l; intros; trivial.
  apply IHl; trivial.
  apply (H0 _ _ _ H).
Qed.

Lemma fold_sum : forall l f,
    (forall e, In e l -> eq OK (f e) zeroK) ->
    eq OK (fold_left (fun (a : K) (e : tupleT) => plusK (f e) a) l zeroK) zeroK.
Proof.
  intros l f.
  induction l; intros. apply Oeset.compare_eq_refl. simpl.
  apply Oeset.compare_eq_trans with (fold_left (fun (a0 : K) (e : tupleT) => plusK (f e) a0) l zeroK).
  - apply fold_sum_init_eq.
    * apply (Oeset.compare_eq_trans _ _ _ _ (plus_compat_r _ _ _ (H a (List.in_eq _ _)))).
      apply plus_0_l.
    * intros. now apply plus_compat_l.
  - apply IHl; intros. apply H; right; trivial.
Qed.

Lemma finite_supp_pi : forall kr env s,
    forall t, Feset.mem FesetT t (support_pi kr env s) = false -> eq OK (f_pi kr env s t) zeroK.
Proof.
  intros.  unfold support_pi in H. unfold f_pi.
  apply fold_sum. intros. apply filter_In in H0 as [H0 H1].
  apply not_true_iff_false in H.
  destruct H.  
  apply Feset.mem_map.
  exists e. split; trivial.
  apply Oeset.eq_bool_true_compare_eq; trivial.
Qed.


Definition sort_pi l := 
      (Fset.mk_set (A T) (map (fun x : select T => match x with
                                                  | Select_As _ a => a
                                                  end) l)).


Lemma labels_projection_prov : forall (env : env_prov) (l : list (select T)),
labels T (projection_prov env (Select_List (_Select_List l))) =S=
Fset.mk_set (A T) (map (fun x : select T => match x with
                                            | Select_As _ a => a
                                            end) l).
Proof.
  intros.
  rewrite projection_prov_unfold; case l;
  intros; apply labels_mk_tuple.
Qed.
  
Lemma labels_supp_pi : forall kr env l,
    forall t, t inSE (support_pi kr env (_Select_List l)) -> labels T t =S= sort_pi l.
Proof.
  intros.
  apply Feset.mem_map in H as [a [H1 H2]].
  apply tuple_eq_labels in H1. apply (Fset_equal_trans _ _ _ _ H1). apply labels_projection_prov.
Qed.


Definition Krel_pi kr (env:env_prov) l :=
  mk_kr (f_pi kr env (_Select_List l)) (support_pi kr env (_Select_List l)) (sort_pi l).

Fixpoint is_agregate (l : list (select T)) :=
  match l with
  |nil => false
  |(Select_As (A_agg _ _ _) _) :: tl =>
   true
  |_ :: tl => is_agregate tl
  end.

Lemma Krel_pi_Krel_inv_aux : forall kr env l, Krel_inv (Krel_pi kr env l).
Proof.
  intros; split; [> apply (finite_supp_pi)|apply (labels_supp_pi)].
Qed.

(*K-relation : aggregates*)

Definition env_gp (env : env_prov) (g : group_by T) kr :=
(g, kr) :: env.


Definition f_one_att_aggr (s:_select_list T) (env :env_prov) kr :=
  (fun t =>
     match s with
     |_Select_List ((Select_As (A_agg _ agg ft) att)::nil) =>
      let t' := projection_prov (env_gp env (Group_By nil) kr) (Select_List s) in
      if Oeset.eq_bool (OTuple T) t t' then oneK else zeroK
     |_ => zeroK
     end).

Definition Krel_one_att_aggr s env kr :=
  match s with
  |_Select_List ((Select_As (A_agg _ agg ft) att)::nil) =>
   let t' := projection_prov (env_gp env (Group_By nil) kr) (Select_List s) in
   mk_kr (fun t => if Oeset.eq_bool (OTuple T) t t' then oneK else zeroK) (Feset.singleton _ t')
         (att addS (emptysetS))
  |_Select_List l => Krel_empty_sort_krel (Fset.mk_set (A T)
    (map
       (fun x : select T => match x with
                            | Select_As _ a => a
                            end) l))
  end.


Lemma Krel_pi_Krel_inv :forall kr env l,
    Krel_inv kr ->
    Krel_inv (if is_agregate l then Krel_one_att_aggr (_Select_List l) env kr else Krel_pi kr env l). 
Proof.
  intros; destruct (is_agregate l); try apply Krel_pi_Krel_inv_aux.
  destruct l; try apply Krel_empty_Krel_inv.
  destruct l; destruct s; destruct a; try apply K_empty_sort_Krel_inv.
  split; intros; unfold Krel_one_att_aggr in H0; unfold support in H0. 
  - rewrite Feset.singleton_spec in H0. simpl. simpl in H0.
    rewrite H0. apply Oeset.compare_eq_refl.
  - rewrite Feset.singleton_spec in H0. apply Oeset.eq_bool_true_compare_eq in H0. apply tuple_eq_labels in H0. apply (Fset_equal_trans _ _ _ _ H0). apply labels_projection_prov.
Qed.


Fixpoint insert_in_partition_p (v : list value) (kr : Krel) (t : tuple) (p : list (list value * Krel)) {struct p} : list (list value * Krel) :=
  match p with
  | nil => (v, (mk_kr (fun t0 => if Oeset.eq_bool (OTuple T) t0 t then f kr t else zeroK) (Feset.singleton FesetT t) (sort_krel kr))) :: nil
  | (v1, p2) :: p1 =>
    if Oset.eq_bool (Oset.mk_list (OVal T)) v v1
    then (v1,
          (mk_kr (fun t0 => if Oeset.eq_bool (OTuple T) t0 t then f kr t else f p2 t0) (Feset.add FesetT t (support p2)) (sort_krel kr))) :: p1
    else (v1, p2) :: insert_in_partition_p v kr t p1
  end.


Lemma insert_in_partition_p_unfold : forall (v : list value) (kr : Krel) (t : tuple) (p : list (list value * Krel)),
    insert_in_partition_p v kr t p =
  match p with
  | nil => (v, (mk_kr (fun t0 => if Oeset.eq_bool (OTuple T) t0 t then f kr t else zeroK) (Feset.singleton FesetT t) (sort_krel kr))) :: nil
  | (v1, p2) :: p1 =>
    if Oset.eq_bool (Oset.mk_list (OVal T)) v v1
    then (v1,
          (mk_kr (fun t0 => if Oeset.eq_bool (OTuple T) t0 t then f kr t else f p2 t0) (Feset.add FesetT t (support p2)) (sort_krel kr))) :: p1
    else (v1, p2) :: insert_in_partition_p v kr t p1
  end.
Proof.
  intros. destruct p; trivial.
Qed.

Fixpoint partition_rec_krel (f : tuple -> list value) (kr : Krel) (p : list (list value * Krel)) (l : list tuple) {struct l} : list (list value * Krel) :=
  match l with
  | nil => p
  | a1 :: l0 => partition_rec_krel f kr (insert_in_partition_p (f a1) kr a1 p) l0
  end.

Definition partition (f : tuple -> list value) (kr : Krel) := partition_rec_krel f kr nil (Feset.elements FesetT (filter_zero kr)).


Definition make_groups_prov (env: env_prov) (kr : Krel) (gby : group_by T) :=
match gby with
| Group_By g =>
    map snd
        (partition
           (fun t : Tuple.tuple T => map (fun h : aggterm => interp_aggterm_prov (env_gp env (Group_By g) (mk_kr (fun t0 => if Oeset.eq_bool (OTuple T) t0 t then oneK  else zeroK) (Feset.singleton FesetT t) (sort_krel kr))) h) g) kr)
| Group_Fine => kr::nil
end.


Definition calcul_proj_grb env lf s kr :=
  map (fun l => (projection_prov (env_gp env (Group_By lf) l) (Select_List (_Select_List s)), l))
                     (make_groups_prov env kr (Group_By lf)).

Hypothesis delta_f : list K -> K.
  
Definition delta_it_f (kr : Krel) : K :=
  delta_f (map (f kr) (Feset.elements _ (support kr))).

Fixpoint f_gamma l :=
  match l with
  |nil => (fun t => zeroK)
  |(t0,kr) :: tl => let f1 := f_gamma tl in
   fun t => if Oeset.eq_bool (OTuple T) t0 t then plusK (delta_it_f kr) (f1 t) else f1 t
  end.

Definition Krel_gamma s env kr lf :=
  match s with
  |_Select_List ((Select_As (A_agg _ agg ft) att)::nil) =>
    let lproj := calcul_proj_grb env lf ((Select_As (A_agg _ agg ft) att)::nil) kr in
    let supp := Feset.mk_set FesetT (map fst lproj) in
    let attr := (att addS (emptysetS)) in
    mk_kr (f_gamma lproj) supp attr
  |_Select_List l => Krel_empty_sort_krel (Fset.mk_set (A T)
                                                       (map
       (fun x : select T => match x with
                            | Select_As _ a => a
                            end) l))
  end.

Lemma Krel_gamma_Krel_inv :forall kr env l lagg,
    Krel_inv (Krel_gamma (_Select_List l) env kr lagg).
Proof.
  intros.
  destruct l; try apply Krel_empty_Krel_inv.
  destruct l; destruct s; destruct a; try apply K_empty_sort_Krel_inv.
  split; intros; unfold Krel_gamma in H; unfold support in H.
    * rewrite Feset.mem_mk_set in H; simpl.
      induction (calcul_proj_grb env lagg                                 (Select_As (A_agg T a f0) a0 :: nil) kr); try solve [apply Oeset.compare_eq_refl].
      simpl.
      destruct a1.
      simpl in H.
      apply orb_false_iff in H as [H H1].
      rewrite Oeset.eq_bool_sym in H; rewrite H.
      now apply IHl.
    * rewrite Feset.mem_mk_set in H; simpl. unfold calcul_proj_grb in H.
      induction (make_groups_prov env kr (Group_By lagg)).
      inversion H.
      apply orb_prop in H as [H|H].
      apply Oeset.eq_bool_true_compare_eq in H.
      apply tuple_eq_labels in H.
      apply (Fset_equal_trans _ _ _ _ H).
      apply labels_mk_tuple.
      now apply IHl.
Qed.

(*Semantics*)
Fixpoint eval_query_prov_k (env : env_prov) (q : query) {struct q}: Krel :=
  match q with
  | Q_Empty_Tuple _ _=> Krel_empty_tuple
  | Q_Table _ r => instance_prov r
  | Q_Set o q1 q2 => Krel_set (eval_query_prov_k env q1) (eval_query_prov_k env q2) o
  | Q_NaturalJoin q1 q2 =>
    Krel_nj (eval_query_prov_k env q1) (eval_query_prov_k env q2)
  | Q_Pi (_Select_List l) q =>
    if is_agregate l then Krel_one_att_aggr (_Select_List l) env (eval_query_prov_k env q)
    else Krel_pi (eval_query_prov_k env q) env l 
  | Q_Sigma form q0 =>
    Krel_sigma eval_query_prov_k (eval_query_prov_k env q0) env form
  | Q_Gamma (_Select_List l) lagg f q => Krel_gamma (_Select_List l) env (eval_query_prov_k env q) lagg
  end.


Lemma eval_query_prov_k_unfold :
  forall env q, eval_query_prov_k env q =
  match q with
  | Q_Empty_Tuple _ _=> Krel_empty_tuple
  | Q_Table _ r => instance_prov r
  | Q_Set o q1 q2 => Krel_set (eval_query_prov_k env q1) (eval_query_prov_k env q2) o
  | Q_NaturalJoin q1 q2 =>
    Krel_nj (eval_query_prov_k env q1) (eval_query_prov_k env q2)
  | Q_Pi (_Select_List l) q =>
    if is_agregate l then Krel_one_att_aggr (_Select_List l) env (eval_query_prov_k env q)
    else Krel_pi (eval_query_prov_k env q) env l 
  | Q_Sigma form q0 =>
    let kr := eval_query_prov_k env q0 in
    Krel_sigma eval_query_prov_k kr env form
  | Q_Gamma (_Select_List l) lagg f q => Krel_gamma (_Select_List l) env (eval_query_prov_k env q) lagg
  end.
Proof.
intros env q; case q; intros; apply refl_equal.
Qed.


Lemma Krel_inv_eval_k : forall env q,
    Krel_inv (eval_query_prov_k env q).
Proof.
  intros. induction q.
  - apply Krel_empty_tuple_Krel_inv.
  - apply instance_prov_inv.
  - apply (Krel_set_Krel_inv _ IHq1 IHq2).
  - apply (Krel_nj_Krel_inv IHq1 IHq2).    
  - case _s; intros. now apply Krel_pi_Krel_inv.
  - apply (Krel_sigma_Krel_inv _ _ _ IHq).
  - case _s; intros. apply Krel_gamma_Krel_inv.
Qed.


Fixpoint well_formed env b q {struct q} :=
  match q with
  | Q_Empty_Tuple _ _ => true
  | Q_Table _ r => true
  | Q_Set o q1 q2 =>
    if sort q1 =S?= sort q2
    then
      match o with
      |Union => well_formed env false q1 && well_formed env false q2 
      |_ => false
      end
    else true
  | Q_NaturalJoin q1 q2 => well_formed env false q1 && well_formed env false q2
  | Q_Pi (_Select_List s) q => 
       well_formed env false q 
       && well_formed_s T (env_t T env (default_tuple T (sort q))) s
       && negb (is_agregate s)
  | Q_Sigma f q =>
    let well_formed_form :=
        (fix well_formed_form env f {struct f} :=
           match f with
           | Sql_Conj _ f1 f2 => well_formed_form env f1 && well_formed_form env f2
           | Sql_Not f => well_formed_form env f
           | Sql_True _ _ => true
           | Sql_Pred _ _ l => forallb (well_formed_ag T env) l
           | Sql_Quant _ _ l q => well_formed env false q && forallb (well_formed_ag T env) l
           | Sql_In s q => well_formed env false q && well_formed_s T env s
           | Sql_Exists _ q => well_formed env false q
           end) in
    well_formed env false q && well_formed_form env f
  | Q_Gamma (_Select_List s) g f q => 
    let well_formed_form :=
        (fix well_formed_form env f {struct f} :=
           match f with
           | Sql_Conj _ f1 f2 => well_formed_form env f1 && well_formed_form env f2
           | Sql_Not f => well_formed_form env f
           | Sql_True _ _ => true
           | Sql_Pred _ _ l => forallb (well_formed_ag T env) l
           | Sql_Quant _ _ l q => well_formed env false q && forallb (well_formed_ag T env) l
           | Sql_In s q => well_formed env false q && well_formed_s T env s
           | Sql_Exists _ q => well_formed env false q
           end) in
    b && well_formed env false q 
                && well_formed_s T (env_g T env (Group_By g) (default_tuple T (sort q) :: nil)) s
      && forallb (well_formed_ag T (env_t T env (default_tuple T (sort q)))) g
      && well_formed_form (env_g T env (Group_By g) (default_tuple T (sort q) :: nil)) f
      && (match s with |(Select_As (A_agg _ agg ft) att)::nil => b && (match f with |Sql_True _ _ => true |_ => false end) |_ => false end)

  end.

Lemma well_formed_unfold : forall env b q,
    well_formed env b q =
    match q with
    | Q_Empty_Tuple _ _ => true
    | Q_Table _ r => true
    | Q_Set o q1 q2 =>
      if sort q1 =S?= sort q2
      then
        match o with
        |Union => well_formed env false q1 && well_formed env false q2 
        |_ => false
        end
      else true
    | Q_NaturalJoin q1 q2 => well_formed env false q1 && well_formed env false q2
    | Q_Pi (_Select_List s) q => 
      well_formed env false q 
                  && well_formed_s T (env_t T env (default_tuple T (sort q))) s
                  && (if is_agregate s then false  else true)
    | Q_Sigma f q =>
      let well_formed_form :=
          (fix well_formed_form env f {struct f} :=
             match f with
             | Sql_Conj _ f1 f2 => well_formed_form env f1 && well_formed_form env f2
             | Sql_Not f => well_formed_form env f
             | Sql_True _ _ => true
             | Sql_Pred _ _ l => forallb (well_formed_ag T env) l
             | Sql_Quant _ _ l q => well_formed env false q && forallb (well_formed_ag T env) l
             | Sql_In s q => well_formed env false q && well_formed_s T env s
             | Sql_Exists _ q => well_formed env false q
             end) in
      well_formed env false q && well_formed_form env f
    | Q_Gamma (_Select_List s) g f q => 
      let well_formed_form :=
          (fix well_formed_form env f {struct f} :=
             match f with
             | Sql_Conj _ f1 f2 => well_formed_form env f1 && well_formed_form env f2
             | Sql_Not f => well_formed_form env f
             | Sql_True _ _ => true
             | Sql_Pred _ _ l => forallb (well_formed_ag T env) l
             | Sql_Quant _ _ l q => well_formed env false q && forallb (well_formed_ag T env) l
             | Sql_In s q => well_formed env false q && well_formed_s T env s
             | Sql_Exists _ q => well_formed env false q
             end) in
      b &&
      well_formed env false q 
                  && well_formed_s T (env_g T env (Group_By g) (default_tuple T (sort q) :: nil)) s
                  && forallb (well_formed_ag T (env_t T env (default_tuple T (sort q)))) g
                  && well_formed_form (env_g T env (Group_By g) (default_tuple T (sort q) :: nil)) f
                  && (match s with |(Select_As (A_agg _ agg ft) att)::nil => b && (match f with |Sql_True _ _ => true |_ => false end) |_ => false end)

    end.
Proof.
  intros; destruct q; trivial.
Qed.

Fixpoint well_formed_form env f {struct f} :=
  match f with
  | Sql_Conj _ f1 f2 => well_formed_form env f1 && well_formed_form env f2
  | Sql_Not f => well_formed_form env f
  | Sql_True _ _ => true
  | Sql_Pred _ _ l => forallb (well_formed_ag T env) l
  | Sql_Quant _ _ l q => well_formed env false q && forallb (well_formed_ag T env) l
  | Sql_In s q => well_formed env false q && well_formed_s T env s
  | Sql_Exists _ q => well_formed env false q
  end.


Lemma well_formed_form_unfold : forall env f,
    well_formed_form env f =
  match f with
  | Sql_Conj _ f1 f2 => well_formed_form env f1 && well_formed_form env f2
  | Sql_Not f => well_formed_form env f
  | Sql_True _ _ => true
  | Sql_Pred _ _ l => forallb (well_formed_ag T env) l
  | Sql_Quant _ _ l q => well_formed env false q && forallb (well_formed_ag T env) l
  | Sql_In s q => well_formed env false q && well_formed_s T env s
  | Sql_Exists _ q => well_formed env false q
  end.
Proof.
  intros; destruct f0; trivial.
Qed.


Lemma sort_krel_sort_N : forall (env :env_prov) q,
    sort q =S= sort_krel (eval_query_prov_k env q).
Proof.
  intros.
  induction q.
  - apply Fset.equal_refl.
  - simpl. rewrite instance_support. apply Fset.equal_refl.
  - apply IHq1. 
  - apply Fset.union_eq; [>apply IHq1|apply IHq2]; apply H.
  - case _s.
    intros. simpl.
    case (is_agregate l); try apply Fset.equal_refl.
    case l; try apply Fset.equal_refl.
    intros.
    case s; try apply Fset.equal_refl.
    intros; case a; intros; try solve [simpl; apply Fset.equal_refl].
    case l0; intros;
      apply Fset.equal_refl.
  - trivial.
  - case _s; intros; case l0; try apply Fset.equal_refl.
    intros; case s0; intros; case a; intros; case l0; intros; case l1; case l; intros; apply Fset.equal_refl.
Qed.
    
End Sec.


(*Instance : the annotations are the occurrences of the tuples*)

Hypothesis instance_prov : relname -> Krel N.

Hypothesis instance_support : forall r,
    sort_krel (instance_prov r) = basesort r.

Hypothesis instance_prov_inv : forall r, Krel_inv 0%N TN (instance_prov r).


Hypothesis instance_prov_nb_occ : forall r e,
 (instance_prov r).(f) e = Febag.nb_occ BTupleT e (instance_rel r).

Notation filter_zero_N := (filter_zero 0%N TN).


Fixpoint delta_f (l : list N) : N :=
  match l with
  |nil => 0%N
  |hd :: tl => if Oeset.eq_bool TN hd 0%N then delta_f tl
               else 1%N
  end.

Hypothesis interp_aggregate_prov : Tuple.aggregate T -> list (Tuple.value T * N) -> Tuple.value T.

Notation sort_krel_N_sort_N := (sort_krel_sort_N 0%N 1%N N.add N.mul TN instance_prov instance_support interp_aggregate_prov delta_f).

Notation eval_query_prov_N := (eval_query_prov_k 0%N 1%N N.add N.mul TN instance_prov interp_aggregate_prov delta_f).

Notation eval_formula_prov_N := (eval_formula_prov 0%N 1%N TN interp_aggregate_prov eval_query_prov_N).

Notation Krel_inv_eval_N := (Krel_inv_eval_k N_is_CSR instance_prov instance_prov_inv interp_aggregate_prov delta_f).

Notation finite_supp := (@finite_support N 0%N TN).

Notation sort_lab_supp := (@sort_labels_supp N 0%N TN).

Notation env_tp := (env_pt 0%N 1%N).

Notation env_pg := (env_gp 0%N 1%N).


Fixpoint create_krel (l : list tuple) s:=
  match l with
  |nil => mk_kr (fun t => 0%N) (Feset.empty FesetT) s
  |hd :: tl =>
   let kr := create_krel tl s in
   mk_kr (fun x => if Oeset.eq_bool (OTuple T) x hd then N.add (f kr x) 1%N
                   else f kr x) (Feset.add FesetT hd (support kr)) s  
  end.

Lemma f_create_krel : forall l s t,
    f (create_krel l s) t = Oeset.nb_occ (OTuple T) t l.
Proof.
  induction l; intros; trivial.
  simpl.
  unfold Oeset.eq_bool.
  case_eq (t_compare (OTuple T) t a); intros; rewrite IHl; trivial.
  apply N.add_comm.
Qed.

Lemma sort_krel_create_krel : forall s l,
    sort_krel (create_krel l s) = s.
Proof.
  intros.
  destruct l; simpl; trivial.
Qed.


Lemma support_create_krel : forall s l t,
    t inSE? support (create_krel l s) = Oeset.mem_bool (OTuple T) t  l.
Proof.
  intros.
  induction l; trivial.
  rewrite Oeset.mem_bool_unfold.
  unfold create_krel. unfold support.
  rewrite Feset.add_spec.
  rewrite <- IHl.
  f_equal.
Qed.

Fixpoint create_env (env : env T) :=
  match env with
  |nil => nil
  |(s,gb,l) :: tl => (gb,create_krel l s) :: (create_env tl)
  end.
(**************************************************)


Lemma Fset_equal_bool_sort_union : forall (env : env_prov N) q1 q2,
((sort q1) unionS (sort q2)) =S= ((sort_krel (eval_query_prov_N env q1)) unionS (sort_krel (eval_query_prov_N env q2))).
Proof.
  intros.
  apply Fset.union_eq; apply sort_krel_N_sort_N.
Qed.
  
Lemma not_zero_mem_support : forall kr t,
    Krel_inv 0%N TN kr ->
    f kr t <> 0%N -> t inSE support kr.
Proof.
  intros.
  apply not_false_iff_true; intro.
  destruct H0.
  destruct H. apply finite_support0 in H1.
  apply (N.compare_eq _ _ H1) .
Qed.

Lemma nb_occ_natural_join: forall (env : env T) (q1 q2 : query),
(forall (e : tuple), f (eval_query_prov_N (create_env env) q1) e = Febag.nb_occ (BTupleT) e (eval_query_rel env q1)) ->
(forall e, f (eval_query_prov_N (create_env env) q2) e = Febag.nb_occ (BTupleT) e (eval_query_rel env q2)) ->
(forall e,
 f (eval_query_prov_N (create_env env) (Q_NaturalJoin q1 q2)) e =
Febag.nb_occ (BTupleT) e (eval_query_rel env (Q_NaturalJoin q1 q2))).
Proof.
  intros.
  simpl. rewrite Febag.nb_occ_mk_bag.
  rewrite (nb_occ_natural_join_list _ _ _ (sort q1) (sort q2)).
  - rewrite H. rewrite H0.
    repeat rewrite Febag.nb_occ_elements.
    pose mk_tuple_eq_1.
    rewrite (Oeset.nb_occ_eq_1 _ _ (mk_tuple T (sort q1) (dot T e))); try solve [
    apply mk_tuple_eq_1; apply Fset.equal_true_sym;
    apply sort_krel_N_sort_N; trivial].
    repeat (rewrite <- N.mul_assoc); apply f_equal.
    rewrite (Oeset.nb_occ_eq_1 _ _ (mk_tuple T (sort q2) (dot T e))); try solve [
    apply mk_tuple_eq_1; apply Fset.equal_true_sym;
    apply sort_krel_N_sort_N; trivial].
    apply f_equal.
    rewrite (Fset.equal_eq_2 _ _ _ _ (Fset_equal_bool_sort_union (create_env env) q1 q2)).
    trivial.
  - intros; apply (Oeset.in_nb_occ (OTuple T)) in H1; rewrite <- Febag.nb_occ_elements in H1; rewrite <- H in H1; apply eq_bool_false_N in H1. apply eq_bool_false_N in H1.
    apply (not_zero_mem_support _ (Krel_inv_eval_N (create_env env) q1)) in H1.
    apply sort_lab_supp in H1.
    apply (Fset_equal_trans _ _ _ _ H1).
    apply Fset.equal_true_sym.
    apply sort_krel_N_sort_N. apply Krel_inv_eval_N.
  - intros; apply (Oeset.in_nb_occ (OTuple T)) in H1; rewrite <- Febag.nb_occ_elements in H1; rewrite <- H0 in H1; apply eq_bool_false_N in H1; apply eq_bool_false_N in H1.
    apply (not_zero_mem_support _ (Krel_inv_eval_N (create_env env) q2)) in H1.
    apply sort_lab_supp in H1.
    apply (Fset_equal_trans _ _ _ _ H1).
    apply Fset.equal_true_sym.
    apply sort_krel_N_sort_N. apply Krel_inv_eval_N.
Qed.


Lemma map_eq_s_elements : forall a b x,
  a =S= b ->
   map (dot T x) (Fset.elements _ a) = map (dot T x) (Fset.elements (Tuple.A T) b).
Proof.
  intros.
  apply Fset.elements_spec1 in H.
  rewrite H; trivial.
Qed.

Lemma Fset_equal_bool_sort : forall env q1 q2,
(sort q1 =S?= sort q2) = (sort_krel (eval_query_prov_N env q1) =S?= sort_krel (eval_query_prov_N env q2)).
Proof.
  intros.
  assert (sort q2 =S= sort_krel (eval_query_prov_N env q2)); try now apply sort_krel_N_sort_N.
  apply (Fset.equal_eq_2 _ (sort q1)) in H.
  rewrite H.
  apply Fset.equal_eq_1; now apply sort_krel_N_sort_N.
Qed.


(*******)
Lemma partition_fun_list : forall (A: Type) (OA : Oeset.Rcd A) x f0 l,
    (forall a b, Oeset.eq_bool OA a b = true -> Oeset.eq_bool OA (f0 a) (f0 b) = true) ->
    (exists la lb,
        Oeset.permut OA l (la++lb) /\
        (forall y, Oeset.mem_bool OA y lb = true -> Oeset.eq_bool OA x (f0 y) = false) /\
        (forall y, Oeset.eq_bool OA x (f0 y) = false -> Oeset.mem_bool OA y l = Oeset.mem_bool OA y lb) /\
        forall y, In y la -> Oeset.compare OA x (f0 y) = Eq).
Proof.
  intros A OA x f0 l Hf.
  induction l.
  - exists nil. exists nil. repeat split.
    * apply Pnil.
    * intros; inversion H.
    * intros. inversion H.
  - destruct IHl as [la [lb [H1 [H2 [H3 H4]]]]].
    case_eq (Oeset.eq_bool OA x (f0 a)); intros.
    * exists (a::la). exists lb. repeat split; trivial.
      -- rewrite <- app_nil_l.
         apply Pcons; trivial.
         apply Oeset.compare_eq_refl.
      -- intros.
         rewrite <- (H3 _ H0).
         simpl.
         case_eq (Oeset.eq_bool OA y a); intros; trivial.
         apply Hf in H5.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H5) in H0.
         rewrite H0 in H; inversion H.
      -- intros.
         inversion H0. rewrite H5 in H; apply Oeset.eq_bool_true_compare_eq; trivial.
         apply H4; trivial.
    * intros.
      exists la. exists (a::lb).
      repeat split; trivial.
      -- apply Pcons; trivial.
         apply Oeset.compare_eq_refl.
      -- intros.  apply orb_prop in H0 as [H0|H0].
         apply Hf in H0.
         rewrite (Oeset.eq_bool_eq_2 _ _  _ _ H0); trivial.
         apply (H2 _ H0).
      -- intros.
         simpl. rewrite H3; trivial.
Qed.


(*********************************)
(*fold_left*)

Lemma sum_fold_plus_acc : forall A f l a b,
    fold_left (fun (x : N) (y : A) => f y + x)%N l (a + b)%N = 
    N.add a (fold_left (fun (x : N) (y : A) => f y + x)%N l b).
Proof.
  induction l; intros; trivial.
  simpl. repeat rewrite IHl. do 2 rewrite N.add_assoc. rewrite (N.add_comm (f0 a) a0). trivial.
Qed.


Lemma fold_left_filter_add :  forall A (OA : Oeset.Rcd A) l1 e (f1 : A -> A) f2,
    fold_left (fun (x : N) y => f2 y + x)%N
              (filter (fun t => Oeset.eq_bool OA e (f1 t)) l1) 0%N =
    fold_left (fun (x : N) y => (if Oeset.eq_bool OA e (f1 y) then 1%N else 0%N) * f2 y + x)%N l1 0%N.
Proof.
  induction l1; intros; trivial.
  simpl.
  rewrite sum_fold_plus_acc.
  rewrite <- IHl1.
  case_eq (Oeset.eq_bool OA e (f1 a)); intros; trivial.
  rewrite fold_left_unfold. rewrite sum_fold_plus_acc.
  rewrite N.mul_1_l. trivial.
Qed.

Lemma permut_fold_left_nb_occ : forall A (OA : Oeset.Rcd A) l l1 l2 e f1,
    (forall a b, Oeset.eq_bool OA a b = true -> Oeset.eq_bool OA (f1 a) (f1 b) = true) ->
    Oeset.permut OA l1 l2 -> 
    fold_left (fun (x : N) y => ((if Oeset.eq_bool OA e (f1 y) then 1 else 0) * Oeset.nb_occ OA y l + x)%N) l1 0%N =
    fold_left (fun (x : N) y => ((if Oeset.eq_bool OA e (f1 y) then 1 else 0) * Oeset.nb_occ OA y l + x)%N) l2 0%N.
Proof.
  induction l1; intros.
  inversion H0 ; rewrite H2; trivial.
  inversion H0. 
  rewrite fold_left_app.
  rewrite <- (N.add_0_r (fold_left _ l3 0%N)).
  rewrite sum_fold_plus_acc.
  rewrite fold_left_unfold. rewrite sum_fold_plus_acc.
  rewrite (IHl1 (l3++l4)).
  rewrite fold_left_app.
  rewrite <- (N.add_0_r (fold_left _ l3 0%N)).
  rewrite sum_fold_plus_acc.
  rewrite N.add_comm. repeat rewrite <- N.add_assoc. apply N.add_cancel_l. rewrite N.add_0_l.
  rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H3).
  apply Oeset.eq_bool_true_compare_eq in H3.
  apply H in H3.
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H3).
  rewrite N.add_comm.
  rewrite <- sum_fold_plus_acc.
  symmetry; apply fold_left_unfold.
  apply H.
  trivial.
Qed.

Lemma fold_left_nb_occ : forall A (OA : Oeset.Rcd A) (FA:Feset.Rcd OA)l l2 (e: A) (f1 : A -> A),
    (forall a b, Oeset.eq_bool OA a b = true -> Oeset.eq_bool OA (f1 a) (f1 b) = true) ->
    (forall t : A, Oeset.mem_bool OA t l = true -> Oeset.mem_bool OA t (Feset.elements FA l2) = true) ->
    fold_left (fun (x : N) (y : A) => (if Oeset.eq_bool OA e (f1 y) then 1%N else 0%N)*(Oeset.nb_occ OA y l) + x)%N (Feset.elements FA l2) 0%N =
    Oeset.nb_occ OA e (map f1 l).
Proof.
  intros.
  destruct (partition_fun_list OA e f1 l H) as [l1 [l3 [H1 [H2 [_ H4]]]]].
  assert (Oeset.permut OA (map f1 l) (map f1 (l1++l3))) as Hperm.
  - revert H1. unfold Oeset.permut.
    apply _permut_map.
    intros. apply Oeset.eq_bool_true_compare_eq.
    apply Oeset.eq_bool_true_compare_eq in H5.
    apply (H _ _ H5).
  - rewrite (fold_left_eq _ (fun (x : N) y =>
                               ((if Oeset.eq_bool OA e (f1 y) then 1 else 0) * Oeset.nb_occ OA y l1 + x)%N) _ eq_refl).
    * rewrite (Oeset.permut_nb_occ _ Hperm).
      rewrite map_app.
      rewrite Oeset.nb_occ_app.
      assert (Oeset.nb_occ OA e (map f1 l3) = 0%N).
      -- apply Oeset.not_mem_nb_occ.
         clear H1. clear Hperm.
         induction l3; trivial. simpl.
         rewrite IHl3.
         rewrite (H2 a); trivial. simpl. rewrite Oeset.eq_bool_refl. trivial.
         intros; apply H2.
         simpl; rewrite H1; apply orb_true_r.
      -- rewrite H3; clear H3. rewrite N.add_0_r.
         destruct (partition_fun_list OA e f1 (Feset.elements FA l2) H) as [l1a [l3a [H1a [H2a [H3a H4a]]]]].
         assert (forall t, In t (l1a++l3a) -> Oeset.nb_occ OA t (l1a++l3a) = 0%N \/ Oeset.nb_occ OA t (l1a++l3a) = 1%N).
         intros. rewrite <- (Oeset.permut_nb_occ _ H1a).
         rewrite <- Feset.nb_occ_unfold.
         rewrite (Feset.nb_occ_alt FA t l2).
         case_eq (t inSE? l2); now [>right|left].
         assert ( fold_left (fun (x : N) y => ((if Oeset.eq_bool OA e (f1 y) then 1 else 0) * Oeset.nb_occ OA y l1 + x)%N)
    (Feset.elements FA l2) 0%N =  fold_left (fun (x : N) y => ((if Oeset.eq_bool OA e (f1 y) then 1 else 0) * Oeset.nb_occ OA y l1 + x)%N)
                                                (l1a++l3a) 0%N).
         apply permut_fold_left_nb_occ; trivial.
         rewrite H5; clear H5.
         rewrite fold_left_app.
         rewrite <- (N.add_0_r (fold_left _ l1a 0%N)).
         rewrite sum_fold_plus_acc.
         assert (fold_left (fun (x : N) y => (if Oeset.eq_bool OA e (f1 y) then 1 else 0) * Oeset.nb_occ OA y l1 + x) l3a 0 = 0)%N.
         clear H1a; clear H3a; clear H3.
         induction l3a; intros; trivial.
         rewrite fold_left_unfold.
         rewrite sum_fold_plus_acc. rewrite IHl3a.
         rewrite H2a; trivial.
         simpl; rewrite Oeset.eq_bool_refl; trivial.
         intros. apply H2a. simpl; rewrite H3; apply orb_true_r.
         rewrite H5; clear H5. rewrite N.add_0_r.
         rewrite (fold_left_eq _ (fun (x : N) y => Oeset.nb_occ OA y l1 + x)%N _ eq_refl); trivial.
         assert (forall t, Oeset.mem_bool OA t l1 = true -> Oeset.mem_bool OA t l1a = true).
         ** intros.
            assert (Oeset.mem_bool OA t l = true).
            rewrite (Oeset.permut_mem_bool_eq _ H1).
            rewrite Oeset.mem_bool_app; rewrite H5; trivial.
            apply H0 in H6.
            rewrite (Oeset.permut_mem_bool_eq _ H1a) in H6.
            rewrite Oeset.mem_bool_app in H6.
            rewrite Oeset.mem_bool_true_iff in H5.
            destruct H5 as [x [H6a H6b]].
            apply H4 in H6b. apply Oeset.eq_bool_true_compare_eq in H6a. apply H in H6a. apply Oeset.eq_bool_true_compare_eq in H6b.
            rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H6a) in H6b.
            apply orb_true_iff in H6 as [H6|H6]; trivial.            
            apply H2a in H6. rewrite H6b in H6; inversion H6.
         ** clear H0. clear H1. clear H2. clear Hperm. clear H1a. clear H2a. clear H3a.
         assert (forall t,
                    In t l1a -> Oeset.nb_occ OA t l1a = 0%N \/ Oeset.nb_occ OA t l1a = 1%N).
         intros. specialize (H3 t (in_or_app _ _ _ (or_introl H0))) as [H1|H1]; rewrite Oeset.nb_occ_app in H1.
         apply N.eq_add_0 in H1 as [H1 _]; left; trivial.
         apply N.eq_add_1 in H1 as [[H1 _]|[H1 _]]; [>right|left]; trivial.
         clear H3.
         revert H4; revert H5; revert l1.
         induction l1a; intros. revert H5. case l1; intros; trivial.
            --- specialize (H5 a). simpl in H5; rewrite Oeset.eq_bool_refl in H5.
                specialize (H5 (eq_refl _)).
                inversion H5.
            --- rewrite fold_left_unfold.
                rewrite sum_fold_plus_acc.
                destruct (Bool.essai1 OA a l1) as [l1b [l1c [H3 [H6 [H7 H8]]]]].
                generalize H3; intro Hperm.
                rewrite (fold_left_eq _ (fun (x : N) y => Oeset.nb_occ OA y (l1b++l1c) + x)%N _ eq_refl); trivial.
                rewrite (Oeset.permut_nb_occ _ H3).
                unfold Oeset.permut in H3.
                apply (_permut_map (fun x y => eq OA x y) f1 f1) in H3.
                rewrite (Oeset.permut_nb_occ _ H3).
                rewrite Oeset.nb_occ_app. rewrite map_app. rewrite Oeset.nb_occ_app.
                assert (Oeset.nb_occ OA a l1b = Oeset.nb_occ OA e (map f1 l1b)).
                ---- clear H3. clear Hperm. induction l1b; trivial.
                     simpl. assert (eq OA a a0); [>rewrite (H8 a0); [>| left]; trivial|]. rewrite H1. rewrite IHl1b.
                     apply Oeset.eq_bool_true_compare_eq in H1. apply H in H1.
                     apply Oeset.eq_bool_true_compare_eq in H1.
                     rewrite <- (Oeset.compare_eq_2 _ _ _ _ H1).
                     rewrite (H4a a); trivial.
                     left; trivial.
                     intros. apply H8. right; trivial.
                ---- rewrite H1. clear H1.
                     rewrite <- N.add_assoc.
                     apply N.add_cancel_l. apply Oeset.not_mem_nb_occ in H6.
                     rewrite H6. rewrite N.add_0_l.
                     rewrite <- IHl1a.
                         apply fold_left_eq; trivial.
                         intros. rewrite Oeset.nb_occ_app.
                         apply N.add_cancel_r. rewrite <- N.add_0_l.
                         apply N.add_cancel_r.
                         specialize (H0 a (List.in_eq _ _)) as [H2|H2]; simpl in H2; rewrite Oeset.compare_eq_refl in H2.
                         apply Nadd_1_diff_0 in H2; inversion H2.
                         rewrite <- N.add_0_r in H2. apply N.add_cancel_l in H2.
                         apply Oeset.nb_occ_not_mem in H2.
                         apply (Oeset.in_mem_bool OA) in H1.
                         apply Oeset.not_mem_nb_occ.
                         apply not_true_iff_false; intro.
                         apply Oeset.mem_bool_true_iff in H0 as [a' [H9 H10]].
                         apply H8 in H10. apply Oeset.compare_eq_sym in H10. apply (Oeset.compare_eq_trans _ _ _ _ H9) in H10.
                         rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H10) in H1. rewrite H1 in H2. inversion H2.
                         intros.
                         apply H4a. right; trivial.
                         intros.
                         apply (in_cons a) in H1. apply H0 in H1 as [H1|H1].
                         apply N.eq_add_0 in H1 as [_ H1]; left; trivial.
                         apply N.eq_add_1 in H1 as [[_ H1]|[_ H1]];[>left|right]; trivial.
                         intros. specialize (H5 t).
                         apply not_false_iff_true.
                         intro H10.
                         case_eq (Oeset.eq_bool OA a t); intros.
                         apply Oeset.nb_occ_not_mem in H6.
                         apply Oeset.eq_bool_true_compare_eq in H2.
                         rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H2) in H6.
                         rewrite H6 in H1; inversion H1.
                         rewrite <- (H7 _ H2) in H1.
                         apply H5 in H1.
                         simpl in H1. rewrite H10 in H1.
                         rewrite orb_false_r in H1.
                         rewrite Oeset.eq_bool_sym in H1.
                         rewrite H1 in H2; inversion H2.
                         intros. apply (Oeset.in_mem_bool OA) in H1.
                         assert (Oeset.mem_bool OA y l1 = true).
                         rewrite (Oeset.permut_mem_bool_eq _ Hperm). rewrite Oeset.mem_bool_app.
                         rewrite H1; apply orb_true_r.
                         apply Oeset.mem_bool_true_iff in H2 as [a' [H2 H9]].
                         apply Oeset.eq_bool_true_compare_eq in H2;
                           apply H in H2.
                         apply Oeset.eq_bool_true_compare_eq in H2;
                         rewrite (Oeset.compare_eq_2 _ _ _ _ H2).
                         apply (H4 _ H9).
                ---- intros. apply Oeset.eq_bool_true_compare_eq in H9; apply Oeset.eq_bool_true_compare_eq. apply (H _ _ H9).
                ---- intros. rewrite (Oeset.permut_nb_occ _ Hperm); trivial.
         ** intros. apply H4a in H5. apply Oeset.eq_bool_true_compare_eq in H5; rewrite H5. rewrite N.mul_1_l. trivial.
    * intros.
      rewrite (Oeset.permut_nb_occ _ H1).
      rewrite Oeset.nb_occ_app.
      apply N.add_cancel_r.
      case_eq (Oeset.eq_bool OA e (f1 b)); intros; trivial.
      apply f_equal.
      rewrite <- N.add_0_r.
      rewrite N.add_cancel_l.
      apply Oeset.not_mem_nb_occ.
      apply not_true_iff_false; intros H8.
      apply H2 in H8. rewrite H8 in H5. inversion H5.
Qed.

(*******)
Lemma create_env_prop_1 : forall env e,
    env_pt 0%N 1%N (create_env env) e = create_env ((labels T e, Group_Fine, e::nil)::env) .
Proof.
  intros.
  trivial. 
Qed.

Lemma support_create_krel_in : forall l s t,
    In t l ->
    t inSE (filter_zero_N (create_krel l s)).
Proof.
  intros.
  unfold filter_zero.
  rewrite Feset.filter_spec.
  rewrite support_create_krel.
  apply (Oeset.in_mem_bool (OTuple T)) in H.
  rewrite f_create_krel.
  rewrite H.
  apply Oeset.mem_nb_occ in H.
  apply negb_true_iff.
  apply not_true_iff_false; intro.
  destruct H.
  apply Oeset.eq_bool_true_compare_eq in H0.
  simpl in H0.
  now apply N.compare_eq in H0.
  intros.
  rewrite 2 f_create_krel.
  now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H1).
Qed.


Lemma support_create_krel_in_2 : forall l s t,
    t inSE (filter_zero_N (create_krel l s)) ->
    exists t0, eq (OTuple T) t t0 /\ In t0 l.
Proof.
  intros. unfold filter_zero_N in H.
  rewrite Feset.filter_spec in H.
  rewrite support_create_krel in H.
  apply andb_true_iff in H as [H H0].
  now apply Oeset.mem_bool_true_iff in H.
  intros.
  rewrite 2 f_create_krel.
  now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H1).
Qed.

Lemma support_create_krel_quicksort_nil : forall s l,
    quicksort (OTuple T) l = nil ->
    Feset.elements FesetT (filter_zero_N (create_krel l s)) = nil.
Proof.
  intros.
  apply quicksort_nil in H; rewrite H; trivial.
Qed.

Lemma support_create_krel_quicksort_app : forall s l l' t,
    quicksort (OTuple T) l = t :: l' ->
    exists t0 l0,
      Oeset.eq_bool (OTuple T) t t0 = true /\
      Feset.elements FesetT (filter_zero_N (create_krel l s)) = t0 :: l0.
Proof.
  intros.
  assert (t inSE (filter_zero_N (create_krel l s))).
  - apply support_create_krel_in.
    apply (In_quicksort (OTuple T)).
    rewrite H; now left.
  - rewrite Feset.mem_elements in H0.
    apply Oeset.mem_bool_true_iff in H0 as [t0 [H0 H1]].
    apply in_split in H1 as [l1 [l2 H1]].
    destruct l1.
    * apply Oeset.eq_bool_true_compare_eq in H0.
    exists t0; exists l2; now split.
    * pose (Feset.elements_spec3 FesetT (filter_zero_N (create_krel l s))).
    rewrite H1 in s0.
    rewrite <- app_comm_cons in s0.
    assert (Oeset.mem_bool (OTuple T) t0 (l1 ++ t0 :: l2) = true).
      -- rewrite Oeset.mem_bool_app.
         simpl.
         rewrite Oeset.eq_bool_refl.
         apply orb_true_r.
      -- apply (lt_feset_elt_aux _ s0) in H2.
         apply Oeset.compare_eq_sym in H0.
         apply (Oeset.compare_lt_eq_trans _ _ _ _ H2) in H0.
         clear H2. clear s0.
         assert (t1 inSE (filter_zero_N (create_krel l s))).
         rewrite Feset.mem_elements; rewrite H1.    
         rewrite Oeset.mem_bool_app.
         simpl; now rewrite Oeset.eq_bool_refl.
         clear H1.
         apply support_create_krel_in_2 in H2 as [t3 [H2 H3]].
         apply (In_quicksort (OTuple T)) in H3.
         apply in_split in H3 as [l3 [l4 H3]].
         rewrite H3 in H.
         apply Oeset.compare_eq_sym in H2.
         apply (Oeset.compare_eq_lt_trans _ _ _ _ H2) in H0.
         clear H2.
         destruct l3; inversion H.
         ** rewrite <- H2 in H0.
            rewrite Oeset.compare_eq_refl in H0; discriminate H0.
         ** set (quick_sorted (OTuple T) l).
            rewrite H3 in i.
            rewrite <- app_comm_cons in i.
            assert (In t3 (t2 :: l3 ++ t3 :: l4)).
            rewrite app_comm_cons.
            apply in_or_app; right; now left.
            apply (sorted_cons_min i) in H1.
            rewrite H2 in H1.
            destruct H1.
            rewrite Oeset.compare_lt_gt.
            now apply CompOpp_iff.
Qed.


Lemma nil_elements_feset : forall s1 s2,
    s1 =SE= s2 ->
    Feset.elements FesetT s1 = nil ->
    Feset.elements FesetT s2 = nil.
Proof.
  intros.
  apply Feset.elements_spec1 in H.
  rewrite H0 in H.
  destruct (Feset.elements FesetT s2); now inversion H.
Qed.

Lemma fst_elements_feset : forall s1 s2 x l,
    s1 =SE= s2 ->
    Feset.elements FesetT s1 = x::l ->
    exists x' l',
      Oeset.eq_bool (OTuple T) x x' = true /\ Feset.elements FesetT s2 = x'::l'.
Proof.
  intros.
  apply Feset.elements_spec1 in H.
  rewrite H0 in H.
  destruct (Feset.elements FesetT s2).
  inversion H.
  simpl in H.
  exists t; exists l0; split; trivial.
  rewrite Oeset.eq_bool_true_compare_eq.
  destruct (t_compare (OTuple T) x t); inversion H; trivial.
Qed.





(***********************************************)




Definition equiv_env_slicep (e1 e2 : group_by T * Krel N) :=
let (g1, kr1) := e1 in
let (g2, kr2) := e2 in
sort_krel kr1 =S= sort_krel kr2 /\ g1 = g2 /\
(filter_zero_N kr1 =SE= filter_zero_N kr2) /\
(forall t1 t2, Oeset.eq_bool (OTuple T) t1 t2 = true -> f kr1 t1 = f kr2 t2).

Definition equiv_envp (e1 e2 : env_prov N) := Forall2 equiv_env_slicep e1 e2.


Lemma interp_dotp_eq :
  forall a (e1 e2 : env_prov N), equiv_envp e1 e2 -> interp_dot_prov 0%N TN e1 a = interp_dot_prov 0%N TN e2 a.
Proof.
  induction e1; intros.
  - destruct e2; trivial.
    inversion H.
  - destruct e2;
      inversion H.
    rewrite interp_dot_prov_unfold.
    destruct a0; destruct e.
    destruct H3 as [_ [_ [H3 _]]].
    case_eq (Feset.elements FesetT (filter_zero 0%N TN k)); intros.
    rewrite (IHe1 e2 H5).
    symmetry.
    rewrite interp_dot_prov_unfold.
    apply (nil_elements_feset _ _ H3) in H6.
    now rewrite H6.
    symmetry.
    rewrite interp_dot_prov_unfold.
    apply (fst_elements_feset _ _ H3) in H6 as [x' [l1 [H6 H7]]].
    rewrite H7.
    apply Oeset.eq_bool_true_compare_eq in H6.
    apply tuple_eq in H6 as [H6a H6b].
    rewrite <- (Fset.mem_eq_2 _ _ _ H6a).
    case_eq (a inS? labels T t); intros.
    now rewrite H6b.
    symmetry.
    now apply IHe1.
Qed.


Lemma interp_funtermp_eq : forall (f : funterm) (e1 e2 : env_prov N),
    equiv_envp e1 e2 -> interp_funterm_prov 0%N TN e1 f = interp_funterm_prov 0%N TN e2 f.
Proof.
intro f.
set (n := size_funterm T f).
assert (Hn := le_n n).
unfold n at 1 in Hn; clearbody n.
revert f Hn.
induction n as [ | n];
  intros f Hn e1 e2 He. 
- destruct f; inversion Hn.
- destruct f as [c | a | f l].
  + apply refl_equal.
  + simpl; apply interp_dotp_eq; trivial.
  + simpl; apply f_equal.
    rewrite <- map_eq.
    intros a Ha.
    apply IHn; [ | assumption].
    simpl in Hn.
    refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply in_list_size; apply Ha.
Qed.

Definition equiv_envp_n (e1 e2: list (group_by T * Krel N * N)):= Forall2 (fun slcn1 slcn2 => equiv_env_slicep (fst slcn1) (fst slcn2) /\ snd slcn1 = snd slcn2) e1 e2.

Lemma unfold_env_slice_prov_eq :
  forall (slc1 slc2 : env_slice_prov N),
    equiv_env_slicep slc1 slc2 ->
    equiv_envp_n (unfold_env_slice_prov 0%N 1%N TN slc1)
               (unfold_env_slice_prov 0%N 1%N TN slc2).
Proof.
  intros [g1 l1] [g2 l2].
  intros [Hs [Hg [Hsup Hf]]].
  unfold unfold_env_slice_prov.
  unfold equiv_envp. 
  simpl snd.
  apply Feset.elements_spec1 in Hsup.
  set (q1 := Feset.elements FesetT (filter_zero_N l1)) in *.
  set (q2 := Feset.elements FesetT (filter_zero_N l2)) in *.
  clearbody q1 q2.
  revert Hsup. revert q2.
  induction q1; intros [| x2 q2]; intros; try solve [simpl; trivial|discriminate Hsup|apply Forall2_nil]. 
  apply Forall2_cons.
  unfold equiv_env_slicep; repeat split; try solve [simpl; trivial].
  - apply Feset.equal_spec.
  intros.
  unfold filter_zero. rewrite ! Feset.filter_spec; try solve [intros; f_equal; simpl; apply Oeset.eq_bool_true_compare_eq in H0; now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0)].
  unfold support.
  rewrite 2 Feset.add_spec.
  rewrite Feset.mem_empty, 2 orb_false_r.
  assert (Oeset.compare (OTuple T) a x2 = Eq).
    * simpl in Hsup; destruct (Oeset.compare (OTuple T) a x2); inversion Hsup; trivial.
    * apply Oeset.eq_bool_true_compare_eq in H.
      simpl. rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H); trivial.
  - intros.
    unfold f.
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H); trivial.
    simpl in Hsup.
    case_eq (t_compare (OTuple T) a x2); intros; try solve [rewrite H0 in Hsup; inversion Hsup].
    apply Oeset.eq_bool_true_compare_eq in H0.
    now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  - apply Hf.
    simpl in Hsup.
    case_eq (t_compare (OTuple T) a x2); intros; try solve [rewrite H in Hsup; inversion Hsup]; now apply Oeset.eq_bool_true_compare_eq.
  - apply IHq1.
    inversion Hsup.
    destruct (t_compare (OTuple T) a x2); try solve [inversion H0]; trivial.
Qed.


Lemma flat_map_group_of_env_slice : forall env,
    flat_map (groups_of_env_slice T) env =
    flat_map (groups_of_env_slice_prov (K:=N))
             (create_env env).
Proof.
  intros.
  unfold groups_of_env_slice.
  unfold groups_of_env_slice_prov.
  induction env; simpl; trivial.
  destruct a. destruct p.
  simpl.
  rewrite IHenv.
  f_equal.
  destruct g; trivial.
  now rewrite sort_krel_create_krel.
Qed.

Lemma find_eval_aux1 :  forall env f0,
    find_eval_env T env f0 = None -> find_eval_env_prov (create_env env) f0 = None.
Proof.
  induction env; simpl; intros.
  - destruct (is_built_upon_ft T nil f0); [>inversion H|trivial].
  - destruct a; destruct p.
    simpl; destruct g.
    * case_eq (find_eval_env T env f0); intros; rewrite H0 in H.
      inversion H.
      rewrite (IHenv _ H0).    
      unfold is_a_suitable_env in H.
      unfold is_a_suitable_env_prov.
      rewrite <- flat_map_group_of_env_slice.
      rewrite sort_krel_create_krel.
      destruct (is_built_upon_ft T(map (fun a : attribute => F_Dot a) ({{{s}}}) ++ extract_funterms T (flat_map (groups_of_env_slice T) env)) f0); [>inversion H|trivial].
    * now apply IHenv.
Qed.

Lemma find_eval_aux2 :  forall env slc f0,
    find_eval_env T env f0 = Some slc ->
    find_eval_env_prov (create_env env) f0 =
    Some (create_env slc).
Proof.
  induction env; simpl; intros.
  - destruct (is_built_upon_ft T nil f0); now inversion H.
  - destruct a; destruct p.
    simpl; destruct g.
    * case_eq (find_eval_env T env f0); intros; rewrite H0 in H.
      inversion H.
      apply IHenv in H0. rewrite H0. now rewrite H2.
      apply find_eval_aux1 in H0.
      rewrite H0.
      unfold is_a_suitable_env in H.
      unfold is_a_suitable_env_prov.
      rewrite <- flat_map_group_of_env_slice.
      rewrite sort_krel_create_krel.
      destruct (is_built_upon_ft T(map (fun a : attribute => F_Dot a) ({{{s}}}) ++ extract_funterms T (flat_map (groups_of_env_slice T) env)) f0); now inversion H.
    * now apply IHenv.
Qed.

Lemma funterm_compare_refl_aux : forall n f,
    size_funterm T f <= n ->
    funterm_compare T f f = Eq.
Proof.
  induction n; intros; destruct f0; simpl; try solve [apply Oset.compare_eq_refl].
  inversion H.
  rewrite Oset.compare_eq_refl.
  apply comparelA_eq_refl.
  intros.
  apply (in_list_size (size_funterm T)) in H0.
  apply IHn.
  simpl in H.
  apply le_S_n in H.
  apply (Nat.le_trans _ _ _ H0 H).
Qed.

Lemma funterm_compare_refl : forall f,
    funterm_compare T f f = Eq.
Proof.
  intros.
  apply funterm_compare_refl_aux with (size_funterm T f0); trivial.
Qed.

Lemma is_a_suitable_envp_aux : forall a kr1 kr2 env1 env2,
    sort_krel kr1 =S= sort_krel kr2 ->
    equiv_envp env1 env2 ->
    Oset.mem_bool (OFun T) a
    (map (fun a0 : attribute => F_Dot a0) ({{{@sort_krel N kr1}}}) ++
     extract_funterms T
       (flat_map (groups_of_env_slice_prov (K:=N)) env1)) =
  Oset.mem_bool (OFun T) a
    (map (fun a0 : attribute => F_Dot a0) ({{{@sort_krel N kr2}}}) ++
     extract_funterms T
     (flat_map (groups_of_env_slice_prov (K:=N)) env2)).
Proof.
  intros.
  simpl. apply Fset.elements_spec1 in H.
  rewrite H.
  rewrite ! Oset.mem_bool_app.
  f_equal.
  apply Oset.mem_bool_eq_2.
  unfold groups_of_env_slice_prov.
  clear H.
  revert H0; revert env2; induction env1; intros; inversion H0; trivial.
  simpl.
  rewrite 2 extract_funterms_app.
  apply comparelA_eq_app.
  destruct a0.
  destruct y.
  destruct H2 as [H2 [H5 [H6 H7]]].
  rewrite H5.
  destruct g0.
  apply comparelA_eq_refl.
  intros. apply funterm_compare_refl.
  apply Fset.elements_spec1 in H2.
  rewrite H2; apply comparelA_eq_refl.
  intros. apply funterm_compare_refl.
  now apply IHenv1.
Qed.
  
Lemma is_a_suitable_envp_eq_aux :
  forall n e kr1 env1 kr2 env2,
    size_funterm T e <= n ->
    sort_krel kr1 =S= sort_krel kr2 -> equiv_envp env1 env2 ->
    is_a_suitable_env_prov kr1 env1 e = is_a_suitable_env_prov kr2 env2 e.
Proof.
  induction n; intros; unfold is_a_suitable_env_prov.
  destruct e; inversion H.
  unfold is_a_suitable_env_prov in IHn.
  destruct e; simpl; trivial.
  - now apply is_a_suitable_envp_aux.
  - simpl.
   rewrite (is_a_suitable_envp_aux _ _ _ H0 H1).
   apply f_equal.
   apply forallb_eq.
   intros.
   apply IHn; trivial.
   apply (in_list_size (size_funterm T)) in H2.
   simpl in H.
   apply le_S_n in H.
   apply (Nat.le_trans _ _ _ H2 H).
Qed.

Lemma is_a_suitable_envp_eq :
  forall e kr1 env1 kr2 env2,
    sort_krel kr1 =S= sort_krel kr2 -> equiv_envp env1 env2 ->
    is_a_suitable_env_prov kr1 env1 e = is_a_suitable_env_prov kr2 env2 e.
Proof.
  intros.
  apply is_a_suitable_envp_eq_aux with (size_funterm T e); trivial.
Qed.

  
Lemma find_eval_envp_eq :
  forall e env1 env2, 
    equiv_envp env1 env2 -> 
    match (find_eval_env_prov env1 e), (find_eval_env_prov env2 e) with
      | None, None => True
      | Some e1, Some e2 => equiv_envp e1 e2
      | _, _ => False
    end.
Proof.
intros e env1; induction env1 as [ | [g1 kr1] env1]; intros [ | [g2 kr2] env2] He.
- simpl; case (is_built_upon_ft T nil e); trivial.
- inversion He.
- inversion He.
- inversion He; subst.
  assert (IH := IHenv1 _ H4).
  simpl in H2; destruct H2 as [H21 [H22 [H23 H24]]]; subst g2; simpl.
  destruct (find_eval_env_prov env1 e) as [_l1 | ];
    destruct (find_eval_env_prov env2 e) as [_l2 | ]; try contradiction IH.
  + destruct g1 as [g1 | ]; assumption.
  + destruct g1; [ | trivial].
    rewrite <- (is_a_suitable_envp_eq e _ _ H21 H4).
    case (is_a_suitable_env_prov kr1 env1 e); [ | trivial].
    constructor 2; trivial.
    repeat split; trivial.
Qed.

Lemma interp_aggtermp_eq : forall (e : aggterm) (env1 env2 : env_prov N),
    equiv_envp env1 env2 ->
    interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov env1 e =
    interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov env2 e.
Proof.
intro a.
set (n := size_aggterm T a).
assert (Hn := le_n n).
unfold n at 1 in Hn; clearbody n.
revert a Hn.
induction n as [ | n]; intros a Hn env1 env2 He.
- destruct a; inversion Hn.
- destruct a as [f | a f | f l]; simpl.
  + apply interp_funtermp_eq; trivial.
  + apply f_equal.
    case (Fset.is_empty (A T) (variables_ft T f)).
    * destruct env1 as [ | slc1 e1]; destruct env2 as [ | slc2 e2];
      try inversion He; [apply refl_equal | ].
      subst.
      rewrite !map_map.
      assert (H' := unfold_env_slice_prov_eq _ _ H2).
      set (l1 := unfold_env_slice_prov 0%N 1%N TN slc1) in *; clearbody l1.
      set (l2 := unfold_env_slice_prov 0%N 1%N TN slc2) in *; clearbody l2.
      {
        revert l2 H'; induction l1 as [ | t1 l1]; intros [ | t2 l2] H'.
        - apply refl_equal.
        - inversion H'.
        - inversion H'.
        - inversion H' as [ | _t1 _t2 _l1 _l2 Ht Hl K3 K4]; subst.
          simpl map; apply f_equal2.
          + destruct t1; destruct t2.
            simpl in Ht. simpl. rewrite (proj2 Ht). f_equal.
            apply interp_funtermp_eq; constructor 2; trivial.
            apply Ht.
          + apply IHl1; trivial.
      }
    * assert (H := find_eval_envp_eq f He).
      destruct (find_eval_env_prov env1 f) as [[ | slc1 e1] | ];
        destruct (find_eval_env_prov env2 f) as [[ | slc2 e2] | ];
        try inversion H; trivial.
      subst; rewrite !map_map.
      assert (H' := unfold_env_slice_prov_eq _ _ H3).
      set (l1 := unfold_env_slice_prov 0%N 1%N TN slc1) in *; clearbody l1.
      set (l2 := unfold_env_slice_prov 0%N 1%N TN slc2) in *; clearbody l2.
      revert l2 H'; induction l1 as [ | t1 l1]; intros [ | t2 l2] H';
      try (inversion H'; fail); trivial.
      {
        inversion H'; subst.
        simpl map; apply f_equal2.
        -  destruct t1; destruct t2.
           simpl in H4. simpl. rewrite (proj2 H4). f_equal.
           apply interp_funtermp_eq; constructor 2; trivial.
           apply H4.
        - apply IHl1; trivial.
      }
  + apply f_equal; rewrite <- map_eq.
    intros a Ha; apply IHn; trivial.
    simpl in Hn.
    refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply in_list_size; assumption.
Qed.

Lemma projection_prov_eq :
  forall s env1 env2, equiv_envp env1 env2 -> projection_prov 0%N 1%N TN interp_aggregate_prov env1 s =t= projection_prov 0%N 1%N TN interp_aggregate_prov env2 s.
Proof.
intros s env1 env2 He; unfold projection.
destruct s as [ | s].
- destruct env1 as [ | [gb1 kr1] env1]; destruct env2 as [ | [gb2 kr2] env2];
  try inversion He.
  + apply Oeset.compare_eq_refl.
  + subst.
    destruct H2 as [Hsa [Hg [Hsup Hf]]].
    unfold projection_prov.
    apply Feset.elements_spec1 in Hsup.
    destruct (Feset.elements FesetT (filter_zero_N kr1));
      destruct (Feset.elements FesetT (filter_zero_N kr2)); try apply Oeset.compare_eq_refl;
    try discriminate Hsup.
    simpl in Hsup.
    case_eq (Oeset.compare (OTuple T) t t0);
            intro Hu; rewrite Hu in Hsup; try discriminate Hsup; trivial.
- rewrite 2 projection_prov_unfold.
  apply mk_tuple_eq_2.
  intros a Ha.
  case_eq (Oset.find (OAtt T) a s); [ | intros; apply refl_equal].
  intros; apply interp_aggtermp_eq; trivial.
Qed.

Lemma equiv_env_eq_create_env : forall env,
    equiv_envp (create_env env) (create_env env).
Proof.
  induction env.
  apply Forall2_refl.
  simpl. destruct a; destruct p.
  apply Forall2_cons; trivial.
  repeat split.
  * apply Fset.equal_refl.
  * apply Feset.equal_refl.
  * intros. rewrite 2 f_create_krel.
    apply Oeset.eq_bool_true_compare_eq in H.
    now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H).
Qed.

Lemma equiv_env_eq_tuple : forall env a1 a2,
    Oeset.eq_bool (OTuple T) a1 a2 = true ->
    equiv_envp (env_pt 0%N 1%N (create_env env) a1)
               (env_pt 0%N 1%N (create_env env) a2).
Proof.
  intros.
  apply Forall2_cons.
  - repeat split.
    * apply Oeset.eq_bool_true_compare_eq in H.
      now apply tuple_eq_labels.
    * unfold filter_zero.
      apply Feset.equal_spec; intro.
      rewrite 2 Feset.filter_spec; try solve [intros; apply Oeset.eq_bool_true_compare_eq in H1; unfold f; now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1)].
      unfold support. rewrite 2 Feset.add_spec.
      rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
      destruct (Oeset.eq_bool (OTuple T) e a2 || (e inSE? (emptysetSE))); trivial.
      do 2 f_equal.
      unfold f.
      now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
    * intros; unfold f; rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0); now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
  - apply equiv_env_eq_create_env.
Qed.


Lemma interp_dot_equiv : forall env a,
    interp_dot T env a = interp_dot_prov 0%N TN (create_env env) a.
Proof.
  induction env; trivial.
  intros. simpl.
  rewrite IHenv.
  case a; intros.
  case p; intros.
  case_eq (quicksort (OTuple T) l); intros;
    unfold interp_dot_prov.
  - apply (support_create_krel_quicksort_nil s) in H; now rewrite H.
  - apply (support_create_krel_quicksort_app s) in H as [t1 [l1 [H H0]]]; rewrite H0.
    apply Oeset.eq_bool_true_compare_eq in H.
    apply tuple_eq in H as [Ha Hb].
    rewrite <- (Fset.mem_eq_2 _ _ _ Ha).
    case_eq (a0 inS? labels T t); intros; trivial.
    now apply Hb.
Qed.


Lemma interp_funterm_equiv_aux : forall n x env, size_funterm T x <= n ->
    interp_funterm env x =
    interp_funterm_prov 0%N TN (create_env env) x.
Proof.
  induction n; intros.
  destruct x; inversion H.
  destruct x; simpl; trivial.
  apply interp_dot_equiv.
  simpl in H.
  rewrite (proj1 (map_eq _ (fun x : funterm => interp_funterm_prov 0%N TN (create_env env) x) _)); trivial.
   intros.
   apply IHn.
   apply (in_list_size (size_funterm T)) in H0.
   apply le_S_n in H.
   apply (le_trans _ _ _ H0 H).
Qed.  

Lemma interp_funterm_equiv : forall x env,
    interp_funterm env x =
    interp_funterm_prov 0%N TN (create_env env) x.
Proof.
  intros.
  now apply interp_funterm_equiv_aux with (size_funterm T x).
Qed.

Lemma quicksort_create_krel : forall g l s,
  equiv_env_slicep (g, create_krel l s) (g, create_krel (quicksort (OTuple T) l) s).
Proof.
  intros; repeat split.
  - rewrite 2 sort_krel_create_krel. apply Fset.equal_refl.
  - apply Feset.equal_spec; intros.
    unfold filter_zero_N.
    rewrite 2 Feset.filter_spec.
    rewrite 2 support_create_krel.
    rewrite 2 f_create_krel.
    rewrite mem_quick_mem.
    now rewrite nb_occ_quicksort. 
    intros.
    rewrite 2 f_create_krel.
    now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H0).
    intros.
    rewrite 2 f_create_krel.
    now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H0).
  - intros. rewrite 2 f_create_krel.
    apply Oeset.eq_bool_true_compare_eq in H.
    rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H).
    now rewrite nb_occ_quicksort.
Qed.                         

Lemma interp_aggregate_aux : forall e1 e2 env f0,
    equiv_envp_n e1 e2 ->
   map (fun e0 : env_prov N * N => (interp_funterm_prov 0%N TN (fst e0) f0, snd e0))
       (map (fun slc : env_slice_prov N * N => (fst slc :: create_env env, snd slc)) e1) =
    map (fun e0 : env_prov N * N => (interp_funterm_prov 0%N TN (fst e0) f0, snd e0))
       (map (fun slc : env_slice_prov N * N => (fst slc :: create_env env, snd slc)) e2).
Proof.
  induction e1; intros; destruct e2; try solve [inversion H| trivial].
  simpl. 
  inversion H.
  destruct H3 as [H3a H3b].
  rewrite (IHe1 e2); trivial.
  f_equal.
  destruct a. destruct p. simpl.
  simpl in H3b. rewrite H3b.
  f_equal.
  apply interp_funtermp_eq.
  unfold equiv_envp.
  apply Forall2_cons; trivial.
  apply equiv_env_eq_create_env.
Qed.

Hypothesis interp_aggregate_prov_prop1 : forall a l,
    List.forallb (fun x => Oeset.eq_bool TN (snd x) 1%N) l = true -> 
    interp_aggregate_prov a l = interp_aggregate T a (fst(List.split l)).

Lemma interp_aggregate_prov_prop1_bis : forall a l, 
    interp_aggregate T a l = interp_aggregate_prov a (map (fun x => (x, 1%N)) l).
Proof.
  intros.
  symmetry.
  rewrite interp_aggregate_prov_prop1.
  f_equal.
  induction l; trivial.
  simpl.
  destruct (split (map (fun x : value => (x, 1%N)) l)).
  simpl.
  simpl in IHl. now rewrite IHl.
  apply forallb_forall.
  intros.
  apply in_map_iff in H as [a0 [H H0]].
  rewrite <- H.
  apply Oeset.eq_bool_refl.
Qed.
  
    
Lemma unfold_env_slice_prov_create_krel : forall l g s, 
    equiv_envp_n (unfold_env_slice_prov 0%N 1%N TN (g, create_krel l s)) (map
       (fun t0 : tupleT =>
        (Group_Fine,
        {|
        f := fun t : tupleT => if Oeset.eq_bool (OTuple T) t t0 then 1%N else 0%N;
        support := Feset.add FesetT t0 (Feset.empty FesetT);
        sort_krel := s |},
        Oeset.nb_occ (OTuple T) t0 l))
       (Feset.elements FesetT
          (filter_zero_N
             (create_krel l s)))).
Proof.
  intros.
  unfold unfold_env_slice_prov.
  unfold equiv_envp_n.
  unfold snd.
  induction (Feset.elements FesetT
          (filter_zero_N (create_krel l s))).
  simpl.
  apply Forall2_nil.
  rewrite 2 map_cons.
  apply Forall2_cons; trivial.
  repeat split; try rewrite sort_krel_create_krel.
  apply Fset.equal_refl.
  apply Feset.equal_refl.
  intros.
  unfold f.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H).
  now rewrite f_create_krel.
Qed.

Definition transfo A (f : A -> N) (l : list A) := flat_map (fun x => List.repeat x (N.to_nat (f x))) l.


Fixpoint filter_sum y l :=
  match l with
  |nil => 0%N
  |hd :: tl => N.add (if Oset.eq_bool (OVal T) (fst hd) y then snd hd else 0%N) (filter_sum y tl)
  end.

Hypothesis interp_aggregate_prov_prop3 : forall a l1 l2,
    (forall t, filter_sum t l1 = filter_sum t l2) ->
    interp_aggregate_prov a l1 = interp_aggregate_prov a l2.

Definition env_slicep_eq_bool y (e : env_slice_prov N) :=
  match e with
  |(Group_Fine,kr) =>
   support kr =SE?= Feset.singleton FesetT y    
    |_ => false
  end.

Fixpoint filter_sum2 y f0 env (l : list (env_slice_prov N * N)):=
  match l with
  |nil => 0%N
  |hd :: tl => N.add (if Oset.eq_bool (OVal T)
      (interp_funterm_prov 0%N TN (fst hd :: create_env env) f0) y then snd hd else 0%N) (filter_sum2 y f0 env tl)
  end.


Lemma filter_sum2_unfold : forall y f0 env l,
    filter_sum2 y f0 env l =
    match l with
    |nil => 0%N
    |hd :: tl => N.add (if Oset.eq_bool (OVal T)
                                        (interp_funterm_prov 0%N TN (fst hd :: create_env env) f0) y then snd hd else 0%N) (filter_sum2 y f0 env tl)
    end.
Proof.
  intros. destruct l; trivial.
Qed.

Lemma filter_sum2_app : forall y f0 env l1 l2,
    filter_sum2 y f0 env (l1++l2) =
    N.add (filter_sum2 y f0 env l1) (filter_sum2 y f0 env l2).
Proof.
  induction l1; intros; trivial.
  rewrite <- app_comm_cons.
  rewrite filter_sum2_unfold.
  rewrite IHl1.
  rewrite N.add_assoc.
  f_equal.
Qed.



Lemma filter_sum_aux : forall l env f0 t,
    filter_sum t
    (map
       (fun e : env_prov N * N =>
        (interp_funterm_prov 0%N TN (fst e) f0, snd e))
       (map
          (fun slc : env_slice_prov N * N =>
           (fst slc :: create_env env, snd slc))
          l)) = 
    filter_sum2 t f0 env l.
Proof.
  induction l; intros; trivial.
  simpl.
  f_equal.
  apply IHl.
Qed.




  
Lemma filter_sum2_nb_occ : forall l env f0 s t,
     filter_sum2 t f0 env
    (map
       (fun t0 : tupleT =>
        (Group_Fine, create_krel (t0 :: nil) s, 1%N))
       l) =
     Oset.nb_occ (OVal T) t (map (fun y => interp_funterm_prov 0%N TN (fst (Group_Fine, create_krel (y :: nil) s, 1%N)
           :: create_env env) f0) l).
Proof.
  intros.
  induction l; trivial.
  rewrite filter_sum2_unfold.
  rewrite 2 map_cons.
  rewrite Oset.nb_occ_unfold.
  rewrite IHl.
  f_equal.
  rewrite Oset.eq_bool_sym.
  unfold Oset.eq_bool.
  assert (forall a, (if (match a with |Eq => true|_=> false end) then 1%N else 0%N) = (match a with |Eq=>1%N |_ => 0%N end) ).
  intros. destruct a0; trivial.
  rewrite H.
  reflexivity.
Qed.

Lemma support_create_krel_filter_zero : forall l s,
    filter_zero_N (create_krel l s) =SE= support (create_krel l s).
Proof.
  intros.
  unfold filter_zero.
  apply Feset.equal_spec.
  intros.
  rewrite Feset.filter_spec.
  rewrite f_create_krel.
  rewrite support_create_krel.
  case_eq (Oeset.mem_bool (OTuple T) e l); intros; trivial.
  apply Oeset.mem_nb_occ in H.
  apply negb_true_iff.
  apply not_true_iff_false.
  intro.
  rewrite Oeset.eq_bool_true_compare_eq in H0.
  apply N.compare_eq_iff in H0.
  destruct H; trivial.
  intros.
  rewrite 2 f_create_krel.
  now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H0).
Qed.

Lemma map_env_nb_occ : forall x' a s l l' l1' l2',
    eq (OTuple T) x' a ->
    l' = l1' ++ x' :: l2' ->
    (forall x : tupleT, (Oeset.nb_occ (OTuple T) x l' <= 1)%N) ->
  map
    (fun t0 : tupleT =>
     (@Group_Fine T,
     {|
     f := fun t1 : tupleT =>
          if Oeset.eq_bool (OTuple T) t1 t0 then 1%N else 0%N;
     support := Feset.add FesetT t0 (Feset.empty FesetT);
     sort_krel := s |}, Oeset.nb_occ (OTuple T) t0 (a :: l)))
    (l2' ++ l1') =
  map
    (fun t0 : tupleT =>
     (Group_Fine,
     {|
     f := fun t1 : tupleT =>
          if Oeset.eq_bool (OTuple T) t1 t0 then 1%N else 0%N;
     support := Feset.add FesetT t0 (Feset.empty FesetT);
     sort_krel := s |}, Oeset.nb_occ (OTuple T) t0 l))
    (l2' ++ l1').
Proof.
  intros x' a s l l' l1' l2' H H0 Hocc.
  apply map_eq.
  intros.
  f_equal.
  specialize (Hocc x').
  rewrite H0 in Hocc.
  simpl; case_eq (t_compare (OTuple T) a0 a); intros; trivial.
  rewrite Oeset.nb_occ_app in Hocc.
  simpl in Hocc.
  rewrite Oeset.compare_eq_refl in Hocc.
  rewrite N.add_comm in Hocc.
  rewrite <- N.add_assoc in Hocc.
  rewrite N.add_1_l,N.one_succ in Hocc.
  apply N.succ_le_mono in Hocc.
  rewrite <- Oeset.nb_occ_app in Hocc.
  apply N.le_0_r in Hocc.
  apply Oeset.nb_occ_not_mem in Hocc.
  apply in_split in H1 as [l1 [l2 H1]].
  rewrite H1 in Hocc.
  rewrite Oeset.mem_bool_app in Hocc.
  simpl in Hocc.
  apply orb_false_iff in Hocc as [_ Hocc].
  apply orb_false_iff in Hocc as [Hocc _].
  apply Oeset.eq_bool_true_compare_eq in H.
  apply Oeset.eq_bool_true_compare_eq in H2.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H) in H2.
  rewrite Oeset.eq_bool_sym in H2.
  rewrite H2 in Hocc.
  inversion Hocc.
Qed.

Lemma le_add_N : forall (n m p : N),
    (n + m <= p)%N -> (n <= p)%N /\ (m <= p)%N.
Proof.
  split; [> apply (N.add_le_mono_r _ _ m)|apply (N.add_le_mono_r _ _ n);rewrite N.add_comm]; apply (N.le_trans _ _ _ H (N.le_add_r _ _)).
Qed.

Lemma filter_sum2_nb_occ2 : forall l l' env f0 s t, 
    (forall x, Oeset.mem_bool (OTuple T) x l = Oeset.mem_bool (OTuple T) x l') ->
    (forall x, (Oeset.nb_occ (OTuple T) x l' <= 1)%N) ->
    filter_sum2 t f0 env
    (map
       (fun t0 : tupleT =>
        (Group_Fine,
        {|
        f := fun t1 : tupleT => if Oeset.eq_bool (OTuple T) t1 t0 then 1%N else 0%N;
        support := Feset.add FesetT t0 (Feset.empty FesetT);
        sort_krel := s |},
        Oeset.nb_occ (OTuple T) t0 l))
       l')
    =
    Oset.nb_occ (OVal T) t (map (fun y => interp_funterm_prov 0%N TN
             ((Group_Fine,
             {|
             f := fun t1 : tupleT =>
                  if Oeset.eq_bool (OTuple T) t1 y then 1%N else 0%N;
             support := Feset.add FesetT y (Feset.empty FesetT);
             sort_krel := s |}) :: create_env env) f0) l).
Proof.
  induction l; intros l' env f0 s t H Hocc.
  rewrite (nb_occ_nil (OTuple T) l'); trivial.
  intros.
  apply Oeset.not_mem_nb_occ.
  now rewrite <- H.
  rewrite Oset.nb_occ_unfold.
  rewrite map_cons.
  assert (Oeset.mem_bool (OTuple T) a l' = true). 
  rewrite <- H.
  simpl.
  now rewrite Oeset.eq_bool_refl.
  apply Oeset.mem_bool_true_iff in H0 as [x' [H1 H2]].
  apply in_split in H2 as [l1' [l2' H2]].
  rewrite H2.
    rewrite map_app, map_cons, filter_sum2_app, N.add_comm, filter_sum2_unfold, <- N.add_assoc, <- filter_sum2_app, <- map_app.
    symmetry.
  case_eq (Oeset.mem_bool (OTuple T) x' l); intros.
  - rewrite <- (IHl (x'::l2'++l1')).
    rewrite map_cons, filter_sum2_unfold, N.add_assoc.
    unfold snd.
    apply Oeset.compare_eq_sym in H1.
    symmetry.
    rewrite (@map_env_nb_occ x' a s l l'); trivial.
    f_equal.
    unfold fst.
    rewrite (@interp_funtermp_eq _ _ ((Group_Fine,
        {|
          f := fun t1 : tupleT =>
                 if Oeset.eq_bool (OTuple T) t1 a then 1%N else 0%N;
          support := Feset.add FesetT a (Feset.empty FesetT);
          sort_krel := s |}) :: create_env env)).
    * rewrite Oset.eq_bool_sym.
     unfold Oset.eq_bool.
     case_eq ( Oset.compare (OVal T) t
         (interp_funterm_prov 0%N TN
            ((Group_Fine,
             {|
             f := fun t1 : tupleT =>
                  if Oeset.eq_bool (OTuple T) t1 a then 1%N else 0%N;
             support := Feset.add FesetT a (Feset.empty FesetT);
             sort_krel := s |}) :: create_env env) f0)); intros; trivial.
     rewrite Oeset.nb_occ_unfold.
     now rewrite H1.
    *  unfold equiv_envp.
       apply Forall2_cons.
       repeat split.
       -- apply Fset.equal_refl.
       -- unfold filter_zero.
          unfold support.
          apply Feset.equal_spec.
          intros.
          rewrite 2 Feset.filter_spec.
          unfold f.
          generalize H1; intros.
          apply Oeset.eq_bool_true_compare_eq in H1.
          rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H1).
          rewrite 2 Feset.add_spec.
          apply Oeset.eq_bool_true_compare_eq in H3.
          now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H3).
          intros.
          unfold f.
          do 2 f_equal.
          apply Oeset.eq_bool_true_compare_eq in H4.
          now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H4).
          intros.
          unfold f.
          apply Oeset.eq_bool_true_compare_eq in H4.
          now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H4).
       -- intros.
          unfold f.
          apply Oeset.eq_bool_true_compare_eq in H1.
          rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H1).
          now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H3).
       -- apply equiv_env_eq_create_env.
    * intros.
      specialize (H x).
      case_eq (Oeset.eq_bool (OTuple T) x x'); intros.
      apply Oeset.eq_bool_true_compare_eq in H3.
      rewrite 2 (Oeset.mem_bool_eq_1 _ _ _ _ H3).
      simpl.
      now rewrite Oeset.eq_bool_refl.
      rewrite H2 in H.
      simpl.
      rewrite Oeset.mem_bool_app in H.
      simpl in H.
      rewrite Oeset.mem_bool_app.
      rewrite orb_assoc.
      rewrite orb_comm.
      rewrite <- H.
      apply Oeset.compare_eq_sym in H1.
      apply Oeset.eq_bool_true_compare_eq in H1.
      rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H1).
      now rewrite H3.
    * intros.
      specialize (Hocc x).
      rewrite H2 in Hocc.
      simpl. rewrite Oeset.nb_occ_app.
      rewrite Oeset.nb_occ_app in Hocc.
      simpl in Hocc.
      rewrite N.add_assoc.
      now rewrite N.add_comm.
  - rewrite <- (IHl (l1'++l2')).
    unfold snd.
    apply Oeset.compare_eq_sym in H1.
    symmetry.
    rewrite (@map_env_nb_occ x' a s l l'); trivial.
    f_equal.
    unfold fst.
    rewrite (@interp_funtermp_eq _ _ ((Group_Fine,
        {|
          f := fun t1 : tupleT =>
                 if Oeset.eq_bool (OTuple T) t1 a then 1%N else 0%N;
          support := Feset.add FesetT a (Feset.empty FesetT);
          sort_krel := s |}) :: create_env env)).
    * rewrite Oset.eq_bool_sym.
     unfold Oset.eq_bool.
     case_eq ( Oset.compare (OVal T) t
         (interp_funterm_prov 0%N TN
            ((Group_Fine,
             {|
             f := fun t1 : tupleT =>
                  if Oeset.eq_bool (OTuple T) t1 a then 1%N else 0%N;
             support := Feset.add FesetT a (Feset.empty FesetT);
             sort_krel := s |}) :: create_env env) f0)); intros; trivial.
     rewrite Oeset.nb_occ_unfold.
     apply Oeset.not_mem_nb_occ in H0.
     rewrite H0.
     now rewrite H1.
    * unfold equiv_envp.
       apply Forall2_cons.
       repeat split.
       -- apply Fset.equal_refl.
       -- unfold filter_zero.
          unfold support.
          apply Feset.equal_spec.
          intros.
          rewrite 2 Feset.filter_spec.
          unfold f.
          generalize H1; intros.
          apply Oeset.eq_bool_true_compare_eq in H1.
          rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H1).
          rewrite 2 Feset.add_spec.
          apply Oeset.eq_bool_true_compare_eq in H3.
          now rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H3).
          intros.
          unfold f.
          do 2 f_equal.
          apply Oeset.eq_bool_true_compare_eq in H4.
          now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H4).
          intros.
          unfold f.
          apply Oeset.eq_bool_true_compare_eq in H4.
          now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H4).
       -- intros.
          unfold f.
          apply Oeset.eq_bool_true_compare_eq in H1.
          rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H1).
          now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H3).
       -- apply equiv_env_eq_create_env.
    * rewrite map_app, filter_sum2_app, N.add_comm, <- filter_sum2_app. now rewrite <- map_app.
    * intros.
      specialize (H x).
      case_eq (Oeset.eq_bool (OTuple T) x x'); intros.
      apply Oeset.eq_bool_true_compare_eq in H3.
      rewrite 2 (Oeset.mem_bool_eq_1 _ _ _ _ H3).
      specialize (Hocc x').
      rewrite H2 in Hocc.
      rewrite Oeset.nb_occ_app in Hocc.
      simpl in Hocc.
      rewrite Oeset.compare_eq_refl in Hocc.
      rewrite N.add_comm in Hocc.
      rewrite <- N.add_assoc in Hocc.
      rewrite N.add_1_l,N.one_succ in Hocc.
      apply N.succ_le_mono in Hocc.
      rewrite N.add_comm in Hocc.
      rewrite <- Oeset.nb_occ_app in Hocc.
      apply N.le_0_r in Hocc.
      apply Oeset.nb_occ_not_mem in Hocc.
      now rewrite Hocc.
      rewrite H2 in H.
      rewrite Oeset.mem_bool_app in H.
      simpl in H.
      apply Oeset.eq_bool_true_compare_eq in H1.
      rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H1) in H.
      rewrite H3 in H.
      simpl in H.
      now rewrite Oeset.mem_bool_app.
    * intros.
      specialize (Hocc x).
      rewrite H2 in Hocc.
      simpl. rewrite Oeset.nb_occ_app.
      rewrite Oeset.nb_occ_app in Hocc.
      simpl in Hocc.
      rewrite N.add_comm, <- N.add_assoc in Hocc.
      apply le_add_N in Hocc as [_ Hocc].
      now rewrite N.add_comm in Hocc.
Qed.

Lemma interp_aggterm_equiv_aux : forall n env agt,
    size_aggterm T agt <= n ->
    interp_aggterm env agt = interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov (create_env env) agt.
Proof.
  induction n; intros; try solve [destruct agt; inversion H].
  destruct agt; simpl.
  - apply interp_funterm_equiv.
  - destruct (Fset.is_empty (A T) (variables_ft T f0)).
     destruct env; intros; trivial.
     simpl.
     symmetry.
     now apply interp_aggregate_prov_prop1.
     destruct e; destruct p.
     simpl.
     pose (unfold_env_slice_prov_eq _ _ (quicksort_create_krel g l s)).
     rewrite (interp_aggregate_aux _ _ e).
     clear e.
     pose (unfold_env_slice_prov_create_krel (quicksort (OTuple T) l) g s).
     rewrite (interp_aggregate_aux _ _ e).
     clear e.

     rewrite interp_aggregate_prov_prop1_bis.
     assert (map (fun x : value => (x, 1%N))
    (map (fun e : Env.env T => interp_funterm e f0)
       (map (fun slc : env_slice T => slc :: env)
          (map (fun t : tupleT => (s, Group_Fine, t :: nil))
               (quicksort (OTuple T) l)))) =
    map (fun e => (interp_funterm_prov 0%N TN (fst e) f0, snd e))
       (map (fun slc : env_slice_prov N * N  => (fst slc :: create_env env, snd slc))
          (map (fun t : tupleT => ((Group_Fine, create_krel (t::nil) s),1%N))  (quicksort (OTuple T) l)))).
     induction (quicksort (OTuple T) l); simpl;
       trivial.
     rewrite interp_funterm_equiv.
     now rewrite IHl0.
     rewrite H0; clear H0.
     apply interp_aggregate_prov_prop3.
     intros.
     rewrite 2 filter_sum_aux.
     rewrite filter_sum2_nb_occ.
     rewrite filter_sum2_nb_occ2.
     reflexivity.
     intros.
     rewrite <- Feset.mem_elements.
     rewrite (Feset.mem_eq_2 _ _ _ (support_create_krel_filter_zero _ _)).
     now rewrite support_create_krel.
     intros.
     rewrite <- Feset.nb_occ_unfold, Feset.nb_occ_alt.
     destruct (x inSE? filter_zero_N (create_krel (quicksort (OTuple T) l) s)); [>apply N.le_refl|apply N.le_0_1].
     case_eq (find_eval_env T env f0); intros.
     apply find_eval_aux2 in H0.
     rewrite H0.
     induction l; trivial.
     rewrite interp_aggregate_prov_prop1_bis; trivial.
     simpl.
     destruct a0; destruct p.
     simpl.
     pose (unfold_env_slice_prov_eq _ _ (quicksort_create_krel g l0 s)).
     rewrite (interp_aggregate_aux _ _ e).
     clear e.
     pose (unfold_env_slice_prov_create_krel (quicksort (OTuple T) l0) g s).
     rewrite (interp_aggregate_aux _ _ e).
     clear e.

     rewrite interp_aggregate_prov_prop1_bis.
     assert (map (fun x : value => (x, 1%N))
    (map (fun e : Env.env T => interp_funterm e f0)
       (map (fun slc : env_slice T => slc :: l)
          (map (fun t : tupleT => (s, Group_Fine, t :: nil))
               (quicksort (OTuple T) l0)))) =
    map (fun e => (interp_funterm_prov 0%N TN (fst e) f0, snd e))
       (map (fun slc : env_slice_prov N * N  => (fst slc :: create_env l, snd slc))
          (map (fun t : tupleT => ((Group_Fine, create_krel (t::nil) s),1%N))  (quicksort (OTuple T) l0)))).
     induction (quicksort (OTuple T) l0); simpl;
       trivial.
     rewrite interp_funterm_equiv.
     now rewrite IHl1.
     rewrite H1; clear H1.
     apply interp_aggregate_prov_prop3.
     intros.
     rewrite 2 filter_sum_aux.
     rewrite filter_sum2_nb_occ.
     rewrite filter_sum2_nb_occ2.
     reflexivity.
     intros.
     rewrite <- Feset.mem_elements.
     rewrite (Feset.mem_eq_2 _ _ _ (support_create_krel_filter_zero _ _)).
     now rewrite support_create_krel.
     intros.
     rewrite <- Feset.nb_occ_unfold, Feset.nb_occ_alt.
     destruct (x inSE? filter_zero_N (create_krel (quicksort (OTuple T) l0) s)); [>apply N.le_refl|apply N.le_0_1].
     apply find_eval_aux1 in H0.
     rewrite H0; simpl.
          apply interp_aggregate_prov_prop1_bis.
  - simpl in H.
    apply le_S_n in H.
    f_equal.
    induction l; trivial.
    simpl.
    apply le_plus_n in H as [H H0].
    rewrite IHn; trivial.
    now rewrite IHl.
Qed.
  
Lemma interp_aggterm_equiv : forall env agt,
    interp_aggterm env agt = interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov (create_env env) agt.
Proof.
  intros. now apply (@interp_aggterm_equiv_aux (size_aggterm T agt)).
Qed.

Lemma projection_equiv : forall env l,
  eq (OTuple T)
    (projection_prov 0%N 1%N TN interp_aggregate_prov (create_env env) (Select_List (_Select_List l)))
    (projection T env (Select_List (_Select_List l))).
Proof.
  intros.
  simpl.
  apply tuple_eq; split.
  - apply (Fset_equal_trans _ _ _ _ (labels_mk_tuple _ _ _)).
    apply Fset.equal_true_sym.
    apply labels_mk_tuple.
  - intros. rewrite 2 dot_mk_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)) in H.
    rewrite H.
    case_eq (Oset.find (OAtt T) a (map (pair_of_select T) l)); trivial.
    intros. symmetry. apply interp_aggterm_equiv.
Qed.


Lemma well_formed_ft_env_t2 : forall ft e env1 env2,
    well_formed_ft T (env1++env2) ft = true ->
    well_formed_ft T (env1 ++ (env_t T env2 e)) ft = true.
Proof.
  induction ft; unfold well_formed_ft; simpl; intros; trivial; rewrite flat_map_app; rewrite ! extract_funterms_app; rewrite Oset.mem_bool_app; rewrite flat_map_app, extract_funterms_app, Oset.mem_bool_app in H; apply orb_true_iff in H as [H|H].
  - now rewrite H.
  - apply orb_true_iff; right.
  simpl. rewrite ! extract_funterms_app; rewrite Oset.mem_bool_app.
  rewrite H.
  apply orb_true_r.
  - apply orb_true_iff in H as [H|H].
    now rewrite H.
    simpl. rewrite ! extract_funterms_app; rewrite Oset.mem_bool_app.
  rewrite H.
  repeat rewrite orb_true_r. trivial.
  - apply orb_true_intro; right.
    rewrite <- extract_funterms_app. rewrite <- flat_map_app.
    rewrite <- extract_funterms_app in H. rewrite <- flat_map_app in H.  
  induction l; trivial.
  simpl.
  rewrite IHl. simpl in H.
  apply andb_true_iff in H as [Ha Hb].
  rewrite andb_true_r.
  rewrite (is_built_upon_ft_incl T a (extract_funterms T (flat_map (groups_of_env_slice T) (env1++env2)))); trivial.
  intros.
  rewrite flat_map_app; simpl; rewrite 2 extract_funterms_app.
  apply in_or_app.
  rewrite flat_map_app, extract_funterms_app in H. apply in_app_or in H as [H|H].
  now left.
  right; apply in_or_app; now right.
  simpl in H.
  now apply andb_true_iff in H as [_ H].
Qed.

Lemma well_formed_agg_env_t_n2 : forall n agg e env1 env2,
    size_aggterm T agg <= n ->
    well_formed_ag T (env1++env2) agg = true ->
    well_formed_ag T (env1++(env_t T env2 e)) agg = true.
Proof.
  induction n; destruct agg; simpl; intros; trivial; try solve [now apply well_formed_ft_env_t2 |inversion H].
  - apply orb_true_iff in H0 as [H0|H0].
    now rewrite H0.
    apply orb_true_iff; right.
    clear H; clear IHn.
    induction env1; intros; trivial.
    simpl in H0; simpl.
    case_eq (find_eval_env T (env1 ++ env2) f0); intros.
    * rewrite H in IHenv1.
    destruct (find_eval_env T (env1 ++ env_t T env2 e) f0).
    destruct a0; destruct p; destruct g; trivial.
    specialize (IHenv1 (eq_refl _)).
    inversion IHenv1.
    * destruct a0; destruct p; destruct g.
    rewrite H in H0.
    case_eq (find_eval_env T (env1 ++ env_t T env2 e) f0); intros; trivial.
    -- case_eq (is_a_suitable_env T s (env1 ++ env2) f0); intros.
    unfold is_a_suitable_env in H2. unfold is_a_suitable_env.
    rewrite (is_built_upon_ft_incl _ _ (map (fun a : attribute => F_Dot a) ({{{s}}}) ++
                                            extract_funterms T
                                            (flat_map (groups_of_env_slice T) (env1 ++ env2)))); trivial.
    intros.
    rewrite flat_map_app; simpl.
    rewrite 2 extract_funterms_app.
    rewrite flat_map_app, extract_funterms_app in H3.
    repeat apply in_app_or in H3 as [H3|H3].
    apply in_or_app; now left.
    apply in_or_app; right.
    apply in_or_app; now left.
    apply in_or_app; right.
    apply in_or_app; right.
    apply in_or_app; now right.
    rewrite H2 in H0.
    inversion H0.
    -- rewrite H in H0; inversion H0. 
  - apply forallb_forall.
    intros.
    apply IHn.
    apply Peano.le_S_n in H.
    apply (in_list_size (size_aggterm T)) in H1.
    apply (le_trans _ _ _ H1 H).
    revert x H1.
    now apply forallb_forall.
Qed.

Lemma well_formed_agg_env_t2 : forall agg e env1 env2,
    well_formed_ag T (env1++env2) agg = true ->
    well_formed_ag T (env1++(env_t T env2 e)) agg = true.
Proof.
  intros.
  now apply well_formed_agg_env_t_n2 with (size_aggterm T agg).
Qed.
  


Lemma well_formed_s_env_t2 : forall l e env1 env2,
    well_formed_s T (env1++env2) l = true ->
    well_formed_s T (env1++(env_t T env2 e)) l = true.
Proof.
  induction l; unfold well_formed_s; simpl; intros; trivial.
  apply andb_true_iff in H as [H H1]. apply andb_true_iff in H as [H H0].
  destruct a.
  rewrite well_formed_agg_env_t2; trivial.
  apply andb_true_iff; repeat split; simpl; trivial.
  apply forallb_forall.
  intros.
  apply (proj1 (forallb_forall _ _)) with x in H0; trivial.
  destruct x.
  now apply well_formed_agg_env_t2. 
Qed.



Lemma well_formed_env_tn :
  forall n,
  (forall q b e env1 env2,
      (tree_size (tree_of_query q)) <= n  -> 
      well_formed (env1++env2) b q = true ->
      well_formed (env1 ++ (labels T e, Group_Fine, e :: nil) :: env2) b q = true)
    /\
    forall f0 e env1 env2,
      tree_size (tree_of_sql_formula (fun t => tree_of_query t)
                                       f0) <= n -> 
    well_formed_form (env1 ++ env2) f0 = true ->
    well_formed_form (env1 ++ (labels T e, Group_Fine, e :: nil) :: env2) f0 = true.
Proof.
  induction n; split.
  - destruct q; intros; inversion H.
  - destruct f0; intros; inversion H.
  - destruct q; simpl; intros; trivial; try solve [apply andb_true_iff in H as [H H0]; rewrite IHq1; trivial; now rewrite IHq2].
    * apply Peano.le_S_n in H.
      apply le_plus_n in H as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      destruct (sort q1 =S?= sort q2); destruct s; trivial.
      apply andb_true_iff in H0 as [H0a H0b].
      now rewrite 2 (proj1 IHn).
    * apply Peano.le_S_n in H.
      apply le_plus_n in H as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      apply andb_true_iff in H0 as [H0a H0b].
      now rewrite 2 (proj1 IHn).
    * destruct _s.
      apply Peano.le_S_n in H.
      apply le_plus_n in H as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      apply andb_true_iff in H0 as [H0a H0c].
      apply andb_true_iff in H0a as [H0a H0b].
      rewrite (proj1 IHn); trivial.
      unfold env_t in H0b.
      unfold env_t.
      rewrite app_comm_cons.
      rewrite app_comm_cons in H0b. 
      apply (well_formed_s_env_t2 _ e) in H0b.
      unfold env_t in H0b.
      rewrite H0c.
      rewrite andb_true_r.
      trivial.
    * apply Peano.le_S_n in H.
      apply le_plus_n in H as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      apply andb_true_iff in H0 as [H0a H0b].
      rewrite (proj1 IHn); trivial.
      apply ((proj2 IHn) _ e); trivial.
    * destruct _s.
      destruct b; try solve [inversion H0]. rewrite andb_true_l. 
      apply Peano.le_S_n in H.
      apply le_plus_n in H as [Hn1 Hn2].
      apply le_plus_n in Hn2 as [_ Hn2].
      apply le_plus_n in Hn2 as [Hn2 Hn3].
      apply andb_true_iff in H0 as [H0a H0e].
      apply andb_true_iff in H0a as [H0a H0d].
      apply andb_true_iff in H0a as [H0a H0c].
      apply andb_true_iff in H0a as [H0a H0b].
      rewrite H0e.
      rewrite andb_true_r.
      apply andb_true_iff; split.
      apply andb_true_iff; split.
      apply andb_true_iff; split.
      now apply (proj1 IHn).
      unfold env_g.
      unfold env_g in H0b.
      rewrite app_comm_cons.
      rewrite app_comm_cons in H0b.
      now apply (well_formed_s_env_t2 _ e) in H0b.
      apply forallb_forall; intros.
      unfold env_t in H0c.
      unfold env_t.
      rewrite app_comm_cons.
      rewrite app_comm_cons in H0c.
      apply well_formed_agg_env_t2.
      revert x H.
      now apply forallb_forall.
      unfold env_g.
      unfold env_g in H0d.
      rewrite app_comm_cons.
      rewrite app_comm_cons in H0d.
      now apply (proj2 IHn).
  - destruct f0; simpl; intros; trivial.
    * apply Peano.le_S_n in H.
      apply le_plus_n in H as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      apply andb_true_iff in H0 as [H0a H0b].
      now rewrite 2 (proj2 IHn).
    * apply Peano.le_S_n in H.
      rewrite <- plus_n_O in H.
      now rewrite (proj2 IHn).
    * apply Peano.le_S_n in H.
      do 2 apply le_Sn_le in H.
      rewrite <- plus_n_O in H.
      apply forallb_forall; intros.
      apply well_formed_agg_env_t2.
      revert x H1.
      now apply forallb_forall.
    * apply Peano.le_S_n in H.
      do 2 apply le_Sn_le in H.
      rewrite <- plus_n_O in H.
      apply le_plus_n in H as [Hn1 Hn2].
      apply andb_true_iff in H0 as [H0a H0b].
      rewrite (proj1 IHn); trivial.
      apply forallb_forall; intros.
      apply well_formed_agg_env_t2.
      revert x H.
      now apply forallb_forall.
    * apply Peano.le_S_n in H.
      do 2 apply le_Sn_le in H.
      rewrite <- 2 plus_n_O in H.
      apply le_plus_n in H as [Hn1 Hn2].
      apply andb_true_iff in H0 as [H0a H0b].
      rewrite (proj1 IHn); trivial.
      now apply well_formed_s_env_t2.
    * apply Peano.le_S_n in H.
      apply le_Sn_le in H.
      rewrite <- 2 plus_n_O in H.
      now rewrite (proj1 IHn). 
Qed.

Lemma well_formed_env_t :
  (forall q b e env1 env2,
      well_formed (env1++env2) b q = true ->
      well_formed (env1 ++ (labels T e, Group_Fine, e :: nil) :: env2) b q = true)
    /\
    forall f0 e env1 env2,
    well_formed_form (env1 ++ env2) f0 = true ->
    well_formed_form (env1 ++ (labels T e, Group_Fine, e :: nil) :: env2) f0 = true.
Proof.
  split; intros.
  apply (well_formed_env_tn (tree_size (tree_of_query q))); trivial.
  apply (well_formed_env_tn (tree_size (tree_of_sql_formula (fun t => tree_of_query t) f0))); trivial.
 Qed.

Lemma support_mem_equiv_eval_query : forall q env,
    (forall e,
        f (eval_query_prov_N (create_env env) q) e = 
        Febag.nb_occ (BTupleT) e (eval_query_rel env q)) ->
    forall t, Febag.mem _ t (eval_query_rel env q) = true ->
              Feset.mem _ t (support (eval_query_prov_N (create_env env) q)) = true.
Proof.
  intros.
  rewrite Febag.mem_unfold in H0. apply Oeset.mem_nb_occ in H0.
  rewrite <- Febag.nb_occ_elements in H0.
  rewrite <- H in H0; trivial.
  apply not_zero_mem_support; trivial.
  apply Krel_inv_eval_N.
Qed.
Lemma support_not_mem_equiv_eval_query : forall q env,
    (forall e,
        f (eval_query_prov_N (create_env env) q) e = 
        Febag.nb_occ (BTupleT) e (eval_query_rel env q)) ->
    forall t, Febag.mem _ t (eval_query_rel env q) = false ->
              f (eval_query_prov_N (create_env env) q)t = 0%N.
Proof.
  intros.
  rewrite Febag.mem_unfold in H0.
  apply Oeset.not_mem_nb_occ in H0.
  rewrite <- Febag.nb_occ_elements in H0.
  now rewrite <- H in H0.
Qed.          



Lemma delta_it_f_1 : forall c,
    (exists x, In x (Feset.elements _ (support c)) /\ f c x <> 0%N) ->
              delta_it_f delta_f c = 1%N.
Proof.
  intros c [x [H H0]].
  unfold delta_it_f.
  unfold delta_f.
  apply in_split in H as [l1 [l2 H]].
  rewrite H.
  rewrite map_app. simpl.
  clear H.
  induction l1.
  simpl.
  case_eq (Oeset.eq_bool TN (f c x) 0%N); intros; trivial.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H.
  rewrite H in H0.
  now destruct H0.
  simpl.
  destruct (Oeset.eq_bool TN (f c a) 0%N); trivial.
Qed.

Lemma f_gamma_nb_occ : forall l t,
    (forall x, In x l ->  delta_it_f delta_f (snd x) = 1%N) ->
    f_gamma 0%N N.add delta_f l t = 
    Oeset.nb_occ (OTuple T) t (map fst l).
Proof.
  induction l; intros; trivial.
  destruct a.
  simpl.
  rewrite IHl.
  rewrite Oeset.eq_bool_sym.
  unfold Oeset.eq_bool.
  case_eq (Oeset.compare (OTuple T) t t0); intros; trivial.
  specialize (H (t0,k)); simpl in H.
  rewrite H; trivial.
  now left.
  intros.
  apply H.
  now right.
Qed.

Definition equiv_krel (kr1 kr2 : Krel N) :=
sort_krel kr1 =S= sort_krel kr2 /\
(filter_zero_N kr1 =SE= filter_zero_N kr2) /\
(forall t1 t2, Oeset.eq_bool (OTuple T) t1 t2 = true -> f kr1 t1 = f kr2 t2).

Definition equiv_part (e1 e2 : list (list value * Krel N)) := Forall2 (fun a1 a2 => equiv_krel (snd a1) (snd a2)) e1 e2.


Lemma equiv_part_partition : forall l1 l2,
    map fst l1 = map fst l2 ->
      all_diff (map fst l1) ->    
    (forall a, In a l1 ->
               exists b, In (fst a, b) l2 /\
                         equiv_krel b (create_krel (snd a)
                match quicksort (OTuple T) (snd a) with
                | nil => emptysetS
                | t :: _ => labels T t
                end)) ->
    equiv_part l2 (map
             (fun l : list value * listT =>
              (fst l,
              create_krel (snd l)
                match quicksort (OTuple T) (snd l) with
                | nil => emptysetS
                | t :: _ => labels T t
                end)) l1).
Proof.
  induction l1; intros; destruct l2; try solve [inversion H].
  apply Forall2_refl.
  simpl.
  unfold equiv_part.
  apply Forall2_cons.
  - specialize (H1 a (List.in_eq _ _)).
  destruct H1 as [b [H1 H2]].
  destruct H1.
  rewrite H1.
  trivial.
  rewrite H in H0.
  simpl in H.
  inversion H.
  apply in_split in H1 as [l3 [l4 H1]].
  rewrite H1 in H0.
  rewrite H4 in H0.
  rewrite map_cons in H0.
  rewrite map_app in H0.
  rewrite map_cons in H0.
  rewrite app_comm_cons in H0.
  apply (all_diff_app _ _) with (fst p) in H0; try solve [now left].
  inversion H0.
  - apply IHl1.
  now inversion H.
  now apply all_diff_tl in H0.
  intros.
  specialize (H1 a0).
  assert (In a0 (a::l1)).
  now right.
  apply H1 in H3.
  destruct H3 as [b [H3 H4]].
  exists b.
  split; trivial.
  destruct H3; trivial.
  apply in_split in H2 as [l3 [l4 H2]].
  rewrite H2 in H0.
  rewrite map_cons in H0.
  rewrite map_app in H0.
  rewrite map_cons in H0.
  inversion H.
  rewrite H6 in H0.
  rewrite H3 in H0.
  rewrite app_comm_cons in H0.
  apply (all_diff_app _ _) with (fst a0) in H0; try solve [now left]. inversion H0.
Qed.



Fixpoint fst_insert_in_partition v p {struct p} :=
  match p with
  | nil => v :: nil
  | v1 :: p1 =>
      if Oset.eq_bool (Oset.mk_list (OVal T)) v v1
      then v1 :: p1
      else v1 :: fst_insert_in_partition v p1
  end.

Definition fst_insert_in_partition' v p :=
  if Oset.mem_bool (Oset.mk_list (OVal T)) v p
  then p
  else p ++ v :: nil.

Lemma fst_insert_in_partition_eq : forall v p,
     fst_insert_in_partition v p =  fst_insert_in_partition' v p.
Proof.
  intros.
  induction p; trivial.
  unfold fst_insert_in_partition'.
  simpl.
  destruct (Oset.eq_bool (Oset.mk_list (OVal T)) v a); trivial.
  rewrite IHp.
  unfold fst_insert_in_partition'.
  now destruct (Oset.mem_bool (Oset.mk_list (OVal T)) v p).
Qed.


Fixpoint fst_partition_rec A (f : A -> list value) p l {struct l} :=
  match l with
  | nil => p
  | a1 :: l0 =>
      fst_partition_rec f (fst_insert_in_partition (f a1) p) l0
  end.

Fixpoint fst_partition_rec' A (f : A -> list value) p l {struct l} :=
  match l with
  | nil => p
  | a1 :: l0 =>
      fst_partition_rec' f (fst_insert_in_partition' (f a1) p) l0
  end.

Definition fst_partition A f l := @fst_partition_rec A f nil l.

Definition fst_partition' A f l := @fst_partition_rec' A f nil l.

Lemma fst_partition_eq : forall A (f: A -> list value) l,
    fst_partition f l = fst_partition' f l.
Proof.
  intros.
  unfold fst_partition.
  unfold fst_partition'.
  set (acc1 := nil) at 1.
  set (acc2 := nil).
  assert (H : acc1 = acc2); [apply refl_equal | ].
  clearbody acc1 acc2.
  revert acc1 acc2 H.
  induction l; intros; trivial.
  simpl.
  apply IHl.
  rewrite H.
  apply fst_insert_in_partition_eq.
Qed.


Lemma fst_partition_rec_unfold : forall A (f : A -> list value) p l,
    fst_partition_rec' f p l =
  match l with
  | nil => p
  | a1 :: l0 =>
      fst_partition_rec' f (fst_insert_in_partition' (f a1) p) l0
  end.
Proof.
  intros; destruct l; trivial.
Qed.


Lemma partition_rec_krel_app : forall f0 k l1 l2 b,
    partition_rec_krel 0%N f0 k b (l1 ++ l2) =
    partition_rec_krel 0%N f0 k (partition_rec_krel 0%N f0 k b l1) l2.
Proof.
  induction l1; intros; [ |simpl; rewrite IHl1]; trivial.
Qed.

Lemma fst_partition_app : forall A (f0 : A -> list value) l1 l2 b,
    fst_partition_rec' f0 b (l1 ++ l2) =
    fst_partition_rec' f0 (fst_partition_rec' f0 b l1) l2.
Proof.
  induction l1; intros; [ |simpl; rewrite IHl1]; trivial.
Qed.


Lemma fst_partition_rec_mem_eq' : forall f0 a b l,
    (forall t, In t l -> eq (OTuple T) t a) ->
    (forall t1 t2, Oeset.mem_bool (OTuple T) t1 l = true -> eq (OTuple T) t1 t2 ->  f0 t1 = f0 t2) ->
    Oset.mem_bool (Oset.mk_list (OVal T)) (f0 a) b = true ->
     fst_partition_rec' f0 b l = b.
Proof.
  induction l; intros; trivial.
  simpl. unfold fst_insert_in_partition'.
  simpl.
  rewrite <- IHl at 4; trivial.
  f_equal.
  apply if_eq_true.
  specialize (H a0 (List.in_eq _ _)).
  assert (Oeset.mem_bool (OTuple T) a0 (a0 :: l) = true).
  simpl; now rewrite Oeset.eq_bool_refl.
  now rewrite (H0 _ _ H2 H).
  intros. apply H.
  now right.
  intros.
  apply H0; trivial.
  simpl.
  rewrite H2.
  apply orb_true_r.
Qed.

Lemma sorted_lt_cons_lt_not_mem : forall a1 a2 l,
    Sorted.Sorted (fun x y : tupleT => lt (OTuple T) x y) (a2::l) ->
    lt (OTuple T) a1 a2 ->
    Oeset.mem_bool (OTuple T) a1 l = false.
Proof.
  induction l; intros; trivial.
  simpl.
  rewrite IHl; trivial.
  inversion H.
  inversion H4.
  unfold Oeset.eq_bool.
  now rewrite (Oeset.compare_lt_trans _ _ _ _ H0 H6).
  inversion H.
  inversion H3.
  apply Sorted.Sorted_cons; trivial.
  inversion H4.
  revert H8.
  apply SetoidList.InfA_ltA; trivial.
  apply Oeset.StrictOrder.
Qed.

Lemma sorted_lt_cons_not_mem : forall a l,
    Sorted.Sorted (fun x y : tupleT => lt (OTuple T) x y) (a::l) ->
    Oeset.mem_bool (OTuple T) a l = false.
Proof.
  induction l; intros; trivial.
  simpl.
  rewrite IHl; trivial.
  inversion H.
  inversion H3.
  unfold Oeset.eq_bool.
  now rewrite H5.
  inversion H.
  inversion H2.
  apply Sorted.Sorted_cons; trivial.
  inversion H3.
  revert H7.
  apply SetoidList.InfA_ltA; trivial.
  apply Oeset.StrictOrder.
Qed.

Lemma sorted_not_gt_lt_not_mem : forall a1 a2 l,
    Sorted.Sorted (fun x y : tupleT =>  t_compare (OTuple T) x y <> Gt) (a2::l) ->
    lt (OTuple T) a1 a2 ->
    Oeset.mem_bool (OTuple T) a1 l = false.
Proof.
  induction l; intros; trivial.
  simpl.
  rewrite IHl; trivial.
  inversion H.
  inversion H4.
  unfold Oeset.eq_bool.
  case_eq (t_compare (OTuple T) a2 a); intros.
  now rewrite (Oeset.compare_lt_eq_trans _ _ _ _ H0 H8).
  now rewrite (Oeset.compare_lt_trans _ _ _ _ H0 H8).
  now destruct H6.
  inversion H.
  inversion H3.
  apply Sorted.Sorted_cons; trivial.
  inversion H4.
  destruct l; trivial.
  apply Sorted.HdRel_cons.
  inversion H8.
  case_eq (t_compare (OTuple T) a2 a); case_eq (t_compare (OTuple T) a t); intros; try solve [now destruct H13|now destruct H10].
  now rewrite (Oeset.compare_eq_trans _ _ _ _ H16 H15).
  now rewrite (Oeset.compare_eq_lt_trans _ _ _ _ H16 H15).
  now rewrite (Oeset.compare_lt_eq_trans _ _ _ _ H16 H15).
  now rewrite (Oeset.compare_lt_trans _ _ _ _ H16 H15).
Qed.

Lemma not_gt_relation_trans : Relations_1.Transitive
    (fun x y : tupleT => t_compare (OTuple T) x y <> Gt).
Proof.
  unfold Relations_1.Transitive.
  intros.
  case_eq (t_compare (OTuple T) x y); case_eq (t_compare (OTuple T) y z); intros; try solve [now destruct H|now destruct H0].
  now rewrite (Oeset.compare_eq_trans _ _ _ _ H2 H1).
  now rewrite (Oeset.compare_eq_lt_trans _ _ _ _ H2 H1).
  now rewrite (Oeset.compare_lt_eq_trans _ _ _ _ H2 H1).
  now rewrite (Oeset.compare_lt_trans _ _ _ _ H2 H1).
Qed.
Lemma Sorted_sorted_gt_eq_decompose : forall a t l1,
    Sorted.Sorted
         (fun x y : tupleT => t_compare (OTuple T) x y <> Gt)
         (t :: l1) ->
    eq (OTuple T) t a ->
    exists l1a l1b : listT,
    l1 = l1a ++ l1b /\
    (forall t0 : tupleT, In t0 l1a -> eq (OTuple T) a t0) /\
    (forall t0 : tupleT, In t0 l1b -> lt (OTuple T) a t0).
Proof.
  induction l1; intros.
  exists nil; exists nil; repeat split; intros; inversion H1.
  case_eq (Oeset.compare (OTuple T) t a0); intros.
  - inversion H.
  destruct IHl1 as [l1a [l1b [H6 [H7 H8]]]]; trivial.
  inversion H4.
  apply Sorted.Sorted_cons; trivial.
  destruct l1; trivial.
  apply Sorted.HdRel_cons.
  inversion H9.
  intro.
  destruct H11.
  apply Oeset.compare_eq_sym in H1.
  apply (Oeset.compare_eq_gt_trans _ _ _ _ H1 H13).
  rewrite H6.
  exists (a0 :: l1a); exists l1b; repeat split.
  intros.
  destruct H9.
  rewrite <- H9.
  apply Oeset.compare_eq_sym in H0.
  apply (Oeset.compare_eq_trans _ _ _ _ H0 H1).
  now apply H7.
  now apply H8.
  - exists nil; exists (a0 :: l1); repeat split.
  intros.
  inversion H2.
  intros.
  inversion H.
  apply Sorted.Sorted_extends in H.
  apply Sorted.Sorted_extends in H5.
  inversion H2.
  rewrite H7 in H1.
  apply Oeset.compare_eq_sym in H0.
  apply (Oeset.compare_eq_lt_trans _ _ _ _ H0 H1).
  apply in_split in H7 as [l2 [l3 H7]].
  rewrite H7 in H5.
  apply (Forall_forall _ _) with t0 in H5.
  case_eq (t_compare (OTuple T) a0 t0); intros; apply Oeset.compare_eq_sym in H0; apply (Oeset.compare_eq_lt_trans _ _ _ _ H0) in H1.
  apply (Oeset.compare_lt_eq_trans _ _ _ _ H1 H8).
  apply (Oeset.compare_lt_trans _ _ _ _ H1 H8).
  now destruct H5.
  apply in_or_app.
  right; now left.
  apply not_gt_relation_trans.
  apply not_gt_relation_trans.
  - apply Sorted.Sorted_extends in H.
    apply (Forall_forall _ _) with a0 in H.
    now destruct H.
    now left.
  apply not_gt_relation_trans.
Qed.


Lemma sorted_sorted_elt_eq : forall a1 a2 l1 l2,
    (forall t, Oeset.mem_bool (OTuple T) t (a2 :: l2) = Oeset.mem_bool (OTuple T) t (a1 :: l1)) ->
    Sorted.Sorted
      (fun x y : tupleT => t_compare (OTuple T) x y <> Gt)
      (a1 :: l1) ->
    Sorted.Sorted (fun x y : tupleT => lt (OTuple T) x y)
                  (a2 :: l2) ->
    eq (OTuple T) a1 a2.
Proof.
  intros.
  case_eq (Oeset.compare (OTuple T) a1 a2); intros; trivial;[> specialize (H a1)|specialize (H a2)]; simpl in H; rewrite Oeset.eq_bool_refl in H; unfold Oeset.eq_bool in H.
  rewrite H2 in H.
  rewrite (sorted_lt_cons_lt_not_mem _  H1 H2) in H.
  inversion H.
  rewrite Oeset.compare_lt_gt in H2.
  rewrite CompOpp_iff in H2.
  rewrite H2 in H.
  simpl in H2.
  rewrite (sorted_not_gt_lt_not_mem _  H0 H2) in H.
  inversion H. 
Qed.

Lemma Sorted_Sorted_app_2: forall l1 l2,
     Sorted.Sorted
    (fun x y : tupleT => t_compare (OTuple T) x y <> Gt) (l1++l2) ->
    Sorted.Sorted
      (fun x y : tupleT => t_compare (OTuple T) x y <> Gt) l2.
Proof.
  induction l1; intros; trivial.
  inversion H.
  now apply IHl1.  
Qed.

Lemma not_mem_dev : forall a t0 l,
    Oeset.eq_bool (OTuple T) t0 a =false ->
    (forall t : tupleT, In t l -> eq (OTuple T) a t) ->
    Oeset.mem_bool (OTuple T) t0 l = false.
Proof.
  induction l; intros; trivial.
  simpl.
  rewrite IHl; trivial.
  specialize (H0 a0 (List.in_eq _ _)).
  apply Oeset.eq_bool_true_compare_eq in H0.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  now rewrite orb_false_r.
  intros.
  apply H0; now right.
Qed.

Lemma fst_partition_rec_eq' : forall env f0 s1 s2 l2 l1 b1 b2,
    (forall t, Oeset.mem_bool (OTuple T) t l2 = Oeset.mem_bool (OTuple T) t l1) ->
    (forall t, Oeset.mem_bool (OTuple T) t l1 = true -> s1 t =S= s2) ->
    Sorted.Sorted (fun x y  => t_compare (OTuple T) x y <> Gt) l1 ->
    Sorted.Sorted (fun x y => lt (OTuple T) x y) l2 ->
    b1 = b2 ->
    fst_partition_rec'
    (fun t : tupleT =>
     map
       (fun f2 : aggterm =>
        interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
          (env_gp (create_env env) (Group_By f0)
             {|
             f := fun t0 : tupleT =>
                  if Oeset.eq_bool (OTuple T) t0 t
                  then 1%N
                  else 0%N;
             support := Feset.add FesetT t (Feset.empty _);
             sort_krel := s1 t|})
          f2) f0) b1 l1 =
     fst_partition_rec'
    (fun t : tupleT =>
     map
       (fun f2 : aggterm =>
        interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
          (env_gp (create_env env) (Group_By f0)
             {|
             f := fun t0 : tupleT =>
                  if Oeset.eq_bool (OTuple T) t0 t
                  then 1%N
                  else 0%N;
             support := Feset.singleton FesetT t;
             sort_krel := s2|})
          f2) f0) b2 l2.
Proof.
  induction l2; intros.
  - simpl in H.
    symmetry in H.
    apply Bool.Oeset_mem_bool_eq_nil in H.
    rewrite H.
    trivial.
  - destruct l1.
    apply Bool.Oeset_mem_bool_eq_nil in H.
    inversion H.
    assert (eq (OTuple T) t a).
    apply (sorted_sorted_elt_eq H H1 H2).
    rewrite fst_partition_rec_unfold.
    symmetry.
    rewrite fst_partition_rec_unfold.
    case_eq (Oeset.mem_bool (OTuple T) a l1); intros.
    * assert (exists l1a l1b, l1 = l1a++l1b /\
          (forall t, In t l1a -> eq (OTuple T) a t)
          /\ forall t, In t l1b -> lt (OTuple T) a t).
      apply (Sorted_sorted_gt_eq_decompose _ H1); trivial.
      destruct H6 as [l1a [l1b [H4a [H4b H4c]]]].
      rewrite H4a.  
      rewrite fst_partition_app.
      rewrite (fst_partition_rec_mem_eq' _ t (fst_insert_in_partition'
          (map
             (fun f2 : aggterm =>
              interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
                (env_gp (create_env env) (Group_By f0)
                   {|
                   f := fun t0 : tupleT =>
                        if Oeset.eq_bool (OTuple T) t0 t
                        then 1%N
                        else 0%N;
                   support := t addSE (emptysetSE);
                   sort_krel := s1 t |}) f2) f0) b1) l1a).
      -- symmetry. rewrite H3. apply IHl2; trivial.
         ** intros.
            case_eq (Oeset.eq_bool (OTuple T) t0 t); intros.
            assert (Oeset.mem_bool (OTuple T) t0 l1b = false).
            apply not_true_iff_false.
            intro.
            apply Oeset.mem_bool_true_iff in H7 as [a' [H7 H8]].
            apply H4c in H8.
            apply Oeset.eq_bool_true_compare_eq in H6.
            apply (Oeset.compare_eq_trans _ _ _ _ H6) in H4.
            apply (Oeset.compare_eq_lt_trans _ _ _ _ H4) in H8.
            rewrite H7 in H8.
            inversion H8.
            rewrite H7.
            apply Oeset.eq_bool_true_compare_eq in H6.
            apply (Oeset.compare_eq_trans _ _ _ _ H6) in H4. 
            rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H4).
            now apply sorted_lt_cons_not_mem in H2.
            specialize (H t0).
            simpl in H.
            apply Oeset.eq_bool_true_compare_eq in H4.
            rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H4) in H. 
            rewrite H6 in H.
            rewrite H4a in H.
            rewrite Oeset.mem_bool_app in H.
            rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H4) in H6.
            now rewrite (not_mem_dev _ _ _ H6 H4b) in H.
      ** intros.
      apply H0.
      simpl.
      rewrite H4a.
      rewrite Oeset.mem_bool_app.
      rewrite H6.
      now repeat rewrite orb_true_r.
      ** rewrite H4a in H1.
         rewrite app_comm_cons in H1.
         apply (Sorted_Sorted_app_2 _ _ H1).
      ** now inversion H2.
      ** unfold fst_insert_in_partition'.
         assert (map
         (fun f2 : aggterm =>
          interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
            (env_gp (create_env env) (Group_By f0)
               {|
               f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 t
                    then 1%N
                    else 0%N;
               support := t addSE (emptysetSE);
               sort_krel := s1 t |}) f2) f0 =
                 map
         (fun f2 : aggterm =>
          interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
            (env_gp (create_env env) (Group_By f0)
               {|
               f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 a
                    then 1%N
                    else 0%N;
               support := Feset.singleton FesetT a;
               sort_krel := s2 |}) f2) f0).
         apply map_eq.
         intros.
         apply interp_aggtermp_eq.
         unfold equiv_envp.
         apply Forall2_cons.
         repeat split.
         apply H0.
         simpl; now rewrite Oeset.eq_bool_refl.
         apply Feset.equal_spec; intros.
         unfold filter_zero.
         rewrite ! Feset.filter_spec.
         unfold support.
         rewrite Feset.singleton_spec.
         rewrite Feset.add_spec.
         rewrite Feset.mem_empty.
         apply Oeset.eq_bool_true_compare_eq in H4.
         unfold f.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H4).
         now rewrite orb_false_r.
         intros.
         apply Oeset.eq_bool_true_compare_eq in H8.
         unfold f.
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H8).
         intros.
         apply Oeset.eq_bool_true_compare_eq in H8.
         unfold f.
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H8).
         intros. unfold f.
         apply Oeset.eq_bool_true_compare_eq in H4.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H4).
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H7).
         apply equiv_env_eq_create_env.
         now rewrite ! H6.
      -- intros.
         apply Oeset.compare_eq_sym.
         apply (Oeset.compare_eq_trans _ _ _ _ H4).
         now apply H4b.
      -- intros.
         apply map_eq.
         intros.
         apply interp_aggtermp_eq.
         unfold equiv_envp.
         apply Forall2_cons.
         repeat split.
         simpl.
         rewrite (Fset.equal_eq_2 _ (s1 t1) (s1 t2) s2).
         apply H0.
         rewrite H4a.
         simpl.
         rewrite Oeset.mem_bool_app.
         rewrite H6.
         apply orb_true_r.
         rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H7) in H6.
         apply H0. rewrite H4a.
         simpl.
         rewrite Oeset.mem_bool_app.
         rewrite H6.
         apply orb_true_r.
         apply Feset.equal_spec; intros.
         unfold filter_zero.
         rewrite ! Feset.filter_spec.
         unfold support.
         rewrite ! Feset.add_spec.
         rewrite ! Feset.mem_empty.
         apply Oeset.eq_bool_true_compare_eq in H7.
         unfold f.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H7).
         now rewrite orb_false_r.
         intros.
         apply Oeset.eq_bool_true_compare_eq in H10.
         unfold f.
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H10).
         intros.
         apply Oeset.eq_bool_true_compare_eq in H10.
         unfold f.
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H10).
         intros. unfold f.
         apply Oeset.eq_bool_true_compare_eq in H7.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H7).
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H9).
         apply equiv_env_eq_create_env.
      -- unfold fst_insert_in_partition'.
         case_eq (Oset.mem_bool (Oset.mk_list (OVal T))
        (map
           (fun f2 : aggterm =>
            interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
              (env_gp (create_env env) (Group_By f0)
                 {|
                 f := fun t0 : tupleT =>
                      if Oeset.eq_bool (OTuple T) t0 t
                      then 1%N
                      else 0%N;
                 support := t addSE (emptysetSE);
                 sort_krel := s1 t |}) f2) f0) b1); intros; trivial.
    
        intros.
        rewrite Oset.mem_bool_app.
        simpl.
        rewrite Oset.eq_bool_refl.
        apply orb_true_r.
    * symmetry. apply IHl2.
      --  intros.
          case_eq (Oeset.eq_bool (OTuple T) t0 t); intros.
          apply Oeset.eq_bool_true_compare_eq in H6.
          apply (Oeset.compare_eq_trans _ _ _ _ H6) in H4. 
          rewrite ! (Oeset.mem_bool_eq_1 _ _ _ _ H4).
          rewrite H5.
          now apply sorted_lt_cons_not_mem in H2.
          specialize (H t0).
          simpl in H.
          apply Oeset.eq_bool_true_compare_eq in H4.
          rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H4) in H. 
          now rewrite H6 in H.
      -- intros.
         apply H0.
         simpl. rewrite H6.
         apply orb_true_r.
      -- now inversion H1.
      -- now inversion H2.
      --unfold fst_insert_in_partition'.
         assert (map
         (fun f2 : aggterm =>
          interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
            (env_gp (create_env env) (Group_By f0)
               {|
               f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 t
                    then 1%N
                    else 0%N;
               support := t addSE (emptysetSE);
               sort_krel := s1 t |}) f2) f0 =
                 map
         (fun f2 : aggterm =>
          interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
            (env_gp (create_env env) (Group_By f0)
               {|
               f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 a
                    then 1%N
                    else 0%N;
               support := Feset.singleton FesetT a;
               sort_krel := s2 |}) f2) f0).
         apply map_eq.
         intros.
         apply interp_aggtermp_eq.
         unfold equiv_envp.
         apply Forall2_cons.
         repeat split.
         apply H0.
         simpl; now rewrite Oeset.eq_bool_refl.
         apply Feset.equal_spec; intros.
         unfold filter_zero.
         rewrite ! Feset.filter_spec.
         unfold support.
         rewrite Feset.singleton_spec.
         rewrite Feset.add_spec.
         rewrite Feset.mem_empty.
         apply Oeset.eq_bool_true_compare_eq in H4.
         unfold f.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H4).
         now rewrite orb_false_r.
         intros.
         apply Oeset.eq_bool_true_compare_eq in H8.
         unfold f.
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H8).
         intros.
         apply Oeset.eq_bool_true_compare_eq in H8.
         unfold f.
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H8).
         intros. unfold f.
         apply Oeset.eq_bool_true_compare_eq in H4.
         rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H4).
         now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H7).
         apply equiv_env_eq_create_env.
         rewrite H3.
         now rewrite ! H6.
Qed.

Lemma fst_partition_eq_aux : forall f0 env q,
    (forall t,  Oeset.mem_bool (OTuple T) t
    (Feset.elements FesetT
       (filter_zero_N (eval_query_prov_N (create_env env) q))) =
  Oeset.mem_bool (OTuple T) t
                 (Febag.elements BTupleT (eval_query_rel env q))) ->
    (forall e e' : tupleT,
  e inSE support (eval_query_prov_N (create_env env) q) ->
  eq (OTuple T) e e' ->
  negb
    (Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e)
       0%N) =
  negb
    (Oeset.eq_bool TN
       (f (eval_query_prov_N (create_env env) q) e') 0%N)) ->
      map fst
    (Partition.partition (Oset.mk_list (OVal T))
       (fun t : tupleT => map (fun f2 : aggterm => interp_aggterm (env_g T env (Group_By f0) (t :: nil)) f2) f0)
       (Febag.elements BTupleT (eval_query_rel env q))) =
  map fst
    (partition 0%N TN
       (fun t : tupleT =>
        map
          (fun h : aggterm =>
           interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
             (env_gp (create_env env) (Group_By f0)
                {|
                f := fun t0 : tupleT => if Oeset.eq_bool (OTuple T) t0 t then 1%N else 0%N;
                support := FiniteSetImpl.singleton_ (OTuple T) t;
                sort_krel := sort_krel (eval_query_prov_N (create_env env) q) |}) h) f0) (eval_query_prov_N (create_env env) q)).
Proof.
  intros f0 env q Hmem Heq.
  assert (map fst
    (Partition.partition (Oset.mk_list (OVal T))
       (fun t : tupleT =>
        map
          (fun f2 : aggterm =>
           interp_aggterm (env_g T env (Group_By f0) (t :: nil))
             f2) f0)
       (Febag.elements BTupleT (eval_query_rel env q))) =
            fst_partition
       (fun t : tupleT =>
        map
          (fun f2 : aggterm =>
                        interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
             (env_gp (create_env env) (Group_By f0)
                {|
                f := fun t0 : tupleT =>
                     if Oeset.eq_bool (OTuple T) t0 t
                     then 1%N
                     else 0%N;
                support := Feset.add _ t (Feset.empty _);
                sort_krel := labels T t |}) f2) f0)
       (Febag.elements BTupleT (eval_query_rel env q))).
  - clear Hmem Heq. unfold Partition.partition.
    unfold fst_partition.
    set (acc1:= nil) at 2.
    set (acc2:= nil) at 2.
    assert (H : map fst acc1 = acc2); [apply refl_equal | ].
    clearbody acc1 acc2.
    revert acc1 acc2 H.
    induction (Febag.elements BTupleT (eval_query_rel env q)); intros; trivial.
    simpl.
    apply IHl.
    clear IHl.
    revert acc2 H.
    induction acc1; intros; destruct acc2; try solve [inversion H]; trivial.
    simpl.
    f_equal.
    apply map_eq.
    intros.
    apply interp_aggterm_equiv.
    inversion H.
    destruct a0.
    simpl.
    case_eq (Oset.eq_bool (Oset.mk_list (OVal T))
                          (map
                             (fun f2 : aggterm =>
                                interp_aggterm (env_g T env (Group_By f0) (a :: nil))
                                               f2) f0) l1); intros; trivial.
    apply Oset.eq_bool_true_iff in H0.
    assert (Oset.eq_bool (Oset.mk_list (OVal T))
                         (map
                            (fun f2 : aggterm =>
                               interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
                                                   (env_gp (create_env env) (Group_By f0)
               {|
               f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 a
                    then 1%N
                    else 0%N;
               support := FiniteSetImpl.add_ a
                            (FiniteSetImpl.empty_ (OTuple T));
               sort_krel := labels T a |}) f2) f0) l1 = true).
  rewrite <- H0.
  apply Oset.eq_bool_true_iff.
  apply map_eq.
  intros.
  symmetry. apply interp_aggterm_equiv.
  clear H0. rewrite H3.
  trivial.
  assert (Oset.eq_bool (Oset.mk_list (OVal T))
      (map
         (fun f2 : aggterm =>
          interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
            (env_gp (create_env env) (Group_By f0)
               {|
               f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 a
                    then 1%N
                    else 0%N;
               support := FiniteSetImpl.add_ a
                            (FiniteSetImpl.empty_ (OTuple T));
               sort_krel := labels T a |}) f2) f0) l1 = false).
  apply not_true_iff_false.
  apply not_true_iff_false in H0.
  intro. destruct H0.
  apply Oset.eq_bool_true_iff in H3.
  rewrite <- H3.
  apply Oset.eq_bool_true_iff.
  apply map_eq.
  intros.
  apply interp_aggterm_equiv.
  rewrite H3.
  rewrite map_cons.
  f_equal.
  now apply IHacc1.
  - rewrite H; clear H.
    rewrite fst_partition_eq.
  assert (map fst
    (partition 0%N TN
       (fun t : tupleT =>
        map
          (fun h : aggterm =>
           interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
             (env_gp (create_env env) (Group_By f0)
                {|
                f := fun t0 : tupleT =>
                     if Oeset.eq_bool (OTuple T) t0 t
                     then 1%N
                     else 0%N;
                support := FiniteSetImpl.singleton_ (OTuple T) t;
                sort_krel := sort_krel
                               (eval_query_prov_N
                                  (create_env env) q) |}) h) f0)
       (eval_query_prov_N (create_env env) q)) =
            fst_partition
       (fun t : tupleT =>
        map
          (fun f2 : aggterm =>
           interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
             (env_gp (create_env env) (Group_By f0)
                {|
                f := fun t0 : tupleT =>
                     if Oeset.eq_bool (OTuple T) t0 t
                     then 1%N
                     else 0%N;
                support := FiniteSetImpl.singleton_ (OTuple T) t;
                sort_krel := sort_krel
                               (eval_query_prov_N
                                  (create_env env) q) |}) f2) f0)
       (Feset.elements FesetT (filter_zero_N (eval_query_prov_N (create_env env) q)))).
  * clear Hmem Heq. unfold partition.
  unfold fst_partition.
  set (acc1:= nil) at 1.
  set (acc2:= nil).
  assert (H : map fst acc1 = acc2); [apply refl_equal | ].
  clearbody acc1 acc2.
  revert acc1 acc2 H.
  induction (Feset.elements FesetT
       (filter_zero_N (eval_query_prov_N (create_env env) q))); intros; trivial.
  simpl.
  apply IHl.
  clear IHl.
  revert acc2 H.
  induction acc1; intros; destruct acc2; try solve [inversion H]; trivial.
  inversion H.
  destruct a0.
  simpl.
  destruct (Oset.eq_bool (Oset.mk_list (OVal T))
        (map
           (fun h : aggterm =>
            interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
              (env_gp (create_env env) (Group_By f0)
                 {|
                 f := fun t0 : tupleT =>
                      if Oeset.eq_bool (OTuple T) t0 a
                      then 1%N
                      else 0%N;
                 support := FiniteSetImpl.singleton_ (OTuple T) a;
                 sort_krel := sort_krel
                                (eval_query_prov_N
                                   (create_env env) q) |}) h) f0)
        l1); trivial.
  rewrite map_cons.
  f_equal.
  now apply IHacc1.
  * rewrite H. clear H.
    rewrite fst_partition_eq.
  apply fst_partition_rec_eq'.
  -- intros. apply Hmem.
  -- intros.
     apply sort_lab_supp.
     apply Krel_inv_eval_N.
     rewrite <- Hmem in H.
     rewrite <- Feset.mem_elements in H.
     unfold filter_zero in H.
     rewrite Feset.filter_spec in H.
     apply andb_true_iff in H as [H _]; trivial.
     apply Heq. 
  -- apply Febag.elements_spec3.
  -- apply Feset.elements_spec3.
  -- trivial.
Qed.




(*********)

Lemma insert_in_sort : forall f0 k l a b,
    (forall t, In t l -> sort_krel (snd t) =S= sort_krel k) ->
    In b (insert_in_partition_p 0%N (f0 a) k a l) ->
    sort_krel (snd b) =S= sort_krel k.
Proof.
  induction l; intros.
  destruct H0.
  rewrite <- H0.
  apply Fset.equal_refl.
  inversion H0.
  destruct a.
  simpl in H0.
  destruct (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l0).
  destruct H0.
  rewrite <- H0.
  apply Fset.equal_refl.
  apply H.
  now right.
  destruct H0.
  apply H.
  now left.
  apply (IHl a0); trivial.
  intros.
  apply H.
  now right.
Qed.


Lemma partition_krel_sort : forall f0 k l p a b,
    (forall t, In t p -> sort_krel (snd t) =S= sort_krel k) ->
    In (a, b) (partition_rec_krel 0%N f0 k p l) ->
    sort_krel b =S= sort_krel k.
Proof.
  induction l; intros.
  simpl in H0.
  apply (H _ H0).
  simpl in H0.
  revert H0.
  apply IHl.
  intro t.
  now apply insert_in_sort.
Qed.

(********************************************)



Lemma insert_in : forall f0 k a a0 b d,
  In d (insert_in_partition_p 0%N (f0 a) k a b) ->
  a0 inSE support (snd d) ->
  Oeset.eq_bool (OTuple T) a0 a = true \/
  (exists d0 : list value * Krel N,
     In d0 b /\ a0 inSE support (snd d0)).
Proof.
  induction b; intros; case_eq (Oeset.eq_bool (OTuple T) a0 a); intros; try solve [now left];right.
  destruct H.
  rewrite <- H in H0.
  unfold snd in H0.
  unfold support in H0.
  rewrite Feset.singleton_spec in H0.
  rewrite H1 in H0; inversion H0.
  inversion H.
  rewrite insert_in_partition_p_unfold in H.
  destruct a1.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a) l); intros; rewrite H2 in H; destruct H.
  rewrite <- H in H0.
  unfold snd in H0.
  unfold support in H0.
  rewrite Feset.add_spec in H0.
  apply orb_true_iff in H0 as [H0|H0].
  rewrite H1 in H0; inversion H0.
  exists (l,k0).
  split; trivial.
  now left.
  exists d.
  split; trivial.
  now right.
  exists d; split; trivial.
  now left.
  apply IHb in H as [H|[d0 [H Ha]]]; trivial.
  rewrite H in H1.
  inversion H1.
  exists d0; split; trivial.
  now right.
Qed.

Lemma partition_in : forall f0 k l a b c,
    a inSE (support (snd c)) ->
    In c (partition_rec_krel 0%N f0 k b l) ->
    Oeset.mem_bool (OTuple T) a l = true \/
    exists d, In d b /\ a inSE (support (snd d)).
Proof.
  induction l; intros.
  - simpl in H0. right.
    exists c; split; trivial. 
  - simpl in H0.
    apply (IHl _ _ _ H) in H0.
    destruct H0 as [H0|[d [H0 H1]]].
    left.
    simpl; rewrite H0; apply orb_true_r.
    apply (insert_in _ _ _ _ _ _ H0) in H1 as [H1|[d0 [H1 H2]]].
    left; simpl.
    now rewrite H1.
    right.
    exists d0; split; trivial.
Qed.


Lemma all_diff_in_fst : forall A B (c : A*B) (k0 : B) b,
    In c b -> all_diff (map fst ((fst c, k0) :: b)) -> False.
Proof.
  intros.
  assert (In (fst (fst c, k0)) (map fst b)).
  clear H0.
  induction b.
  inversion H.
  destruct H.
  rewrite H.
  now left.
  right.
  now apply IHb.
  rewrite cons_app_nil in H0.
  rewrite map_app in H0.
  apply (all_diff_app _ _ H0 (fst c) (List.in_eq _ _) H1).
Qed.


Lemma insert_in_part_in_3 : forall f0 k l a,
    exists y,
    In (f0 a, y) (insert_in_partition_p 0%N (f0 a) k a l).
Proof.
  induction l; intros.
  - exists ({|
    f := fun t0 : tupleT =>
         if Oeset.eq_bool (OTuple T) t0 a then f k a else 0%N;
    support := FiniteSetImpl.singleton_ (OTuple T) a;
    sort_krel := sort_krel k |}).
    now left.
  - simpl.
    destruct a.
    case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l0); intros.
    apply Oset.eq_bool_true_iff in H.
    rewrite H.
    exists ({|
       f := fun t0 : tupleT =>
            if Oeset.eq_bool (OTuple T) t0 a0
            then f k a0
            else f k0 t0;
       support := FiniteSetImpl.add_ a0 (support k0);
       sort_krel := sort_krel k |}).
    now left.
    destruct (IHl a0) as [y H0].
    exists y; now right.
Qed.

Lemma all_diff_insert_in_not_eq : forall k0 k f0 b a0 l,
    all_diff (map fst ((l, k0) :: b)) ->
    Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l = false ->
    forall a : list value,
      In a (map fst (insert_in_partition_p 0%N (f0 a0) k a0 b)) ->
      l <> a.
Proof.
  induction b; intros; simpl in H1.
  destruct H1.
  rewrite H1 in H0.
  intro.
  rewrite H2 in H0.
  rewrite Oset.eq_bool_refl in H0. inversion H0. 
  inversion H1.
  destruct a.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l0); intros; rewrite H2 in H1.
  destruct H1. simpl in H1.
  rewrite H1 in H2.
  intro.
  rewrite H3 in H0.
  rewrite H0 in H2; inversion H2.
  intro.
  rewrite <- H3 in H1.
  rewrite (cons_app_nil (l0, k1)) in H.
  rewrite app_comm_cons in H.
  rewrite map_app in H.
  apply (all_diff_app _ _ H l); trivial.
  now left.
  destruct H1.
  rewrite <- H1.
  intro. rewrite H3 in H.
  rewrite 2 map_cons in H.
  inversion H.
  destruct (H4 (fst (l0, k1)) (List.in_eq _ _)); trivial.
  apply (IHb a0); trivial.
  rewrite map_cons.
  apply (all_diff_app2 (fst (l0, k1) ::nil)).
  rewrite <- map_cons.
  rewrite <- cons_app_nil.
  rewrite <- map_cons.
  apply (@all_diff_permut (list value) (map fst ((l, k0) :: (l0, k1) :: b)) (map fst ((l0, k1) :: (l, k0) :: b))); trivial.
  rewrite ! map_cons.
  simpl.
  rewrite (cons_app_nil l0 (l :: map fst b)).
  apply Pcons; trivial.
  rewrite cons_app_nil.
  now apply _permut_refl.
Qed.


Lemma insert_all_diff : forall f0 k b a,
    all_diff (map fst b) ->
    all_diff (map fst (insert_in_partition_p 0%N (f0 a) k a b)).
Proof.
  induction b; intros; trivial.
  rewrite insert_in_partition_p_unfold.
  destruct a.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros.
  - rewrite map_cons in H.
  simpl. destruct (map fst b); intros; trivial.
  - simpl. case_eq (map fst (insert_in_partition_p 0%N (f0 a0) k a0 b)); intros; trivial.
  rewrite <- H1.
  split.
    * apply (all_diff_insert_in_not_eq _ _ _ _ _ H H0).
    * apply IHb.
      rewrite cons_app_nil in H.
      rewrite map_app in H.
      now apply all_diff_app2 in H.     
Qed.

Lemma partition_all_diff : forall f0 k l b ,
    all_diff (map fst b) ->
    all_diff (map fst (partition_rec_krel 0%N f0 k b l)).
Proof.
  induction l; intros; trivial.
  simpl.
  apply IHl.
  now apply insert_all_diff.
Qed.


Lemma insert_in_part_in : forall f0 k l a b,
    f0 a = fst b ->
    all_diff (map fst l) ->
    In b (insert_in_partition_p 0%N (f0 a) k a l) ->
    Oeset.eq_bool TN (f k a) 0%N = false ->
    (forall h, In h l ->  forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true ->
          Oeset.eq_bool TN (f (snd h) t1) (f (snd h) t2) = true) ->
    a inSE (filter_zero_N (snd b)).
Proof.
  induction l; intros.
  - destruct H1 as [H1|H1].
    rewrite <- H1.
    unfold filter_zero.
    unfold snd.
    unfold support.
    rewrite Feset.filter_spec.
    rewrite Feset.singleton_spec.
    unfold f.
    rewrite Oeset.eq_bool_refl.
    unfold f in H2. now rewrite H2.
    intros.
    unfold f.
    apply Oeset.eq_bool_true_compare_eq in H5.
    now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H5).
    destruct H1.
  - destruct a.
    case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l0); intros; rewrite insert_in_partition_p_unfold in H1; rewrite H4 in H1.
    * apply Oset.eq_bool_true_iff in H4.
    rewrite H4 in H.
    rewrite H in H0.
    destruct H1.
    rewrite <- H1.    
    unfold filter_zero.
    unfold snd.
    unfold support.
    rewrite Feset.filter_spec.
    rewrite Feset.add_spec.
    unfold f.
    rewrite Oeset.eq_bool_refl.
    unfold f in H2. now rewrite H2.
    intros.
    unfold f.
    apply Oeset.eq_bool_true_compare_eq in H6.
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H6).
    do 2 f_equal.
    apply (H3 (l0,k0) (List.in_eq _ _)) in H6.
    apply Oeset.eq_bool_true_compare_eq in H6.
    apply N.compare_eq_iff in H6.
    simpl in H6. unfold f in H6. now rewrite H6.
    rewrite cons_app_nil in H0.
    rewrite map_app in H0.
    assert (In (fst b) (map fst l)).
    clear IHl H H0 H2 H3.
    induction l.
    inversion H1.
    destruct H1.
    rewrite H.
    now left.
    right.
    now apply IHl.
    apply (all_diff_app _ _ H0 (fst b) (List.in_eq _ _)) in H5.
    destruct H5.
    *  destruct H1.
       apply not_true_iff_false in H4; destruct H4.
       apply Oset.eq_bool_true_iff.
       rewrite H.
       now rewrite <- H1.
       apply IHl; trivial.
       rewrite cons_app_nil in H0.
       rewrite map_app in H0.
       now apply all_diff_app2 in H0.
       intros.
       apply H3; trivial.
       now right.
Qed.

Lemma feset_equal_trans : forall A (OA : Oeset.Rcd A) (FA : Feset.Rcd OA) s1 s2 s3,
    Feset.equal FA s1 s2 = true ->
    s2 =SE= s3 ->
    s1 =SE= s3.
Proof.
  intros.
  apply Feset.equal_spec.
  intros.
  apply (Feset.equal_spec _ _ _) with e in H.
  apply (Feset.equal_spec _ _ _) with e in H0.
  now rewrite H.
Qed.


Lemma insert_in_part_in_10: forall (f0 : tupleT -> list value) (k : Krel N) (l : list (list value * Krel N)) (a : tupleT),
    all_diff (map fst l) ->
    Oeset.eq_bool TN (f k a) 0%N = false ->
    (forall h, In h l ->  forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true ->
          Oeset.eq_bool TN (f (snd h) t1) (f (snd h) t2) = true) ->
    exists y : Krel N,
      In (f0 a, y) (insert_in_partition_p 0%N (f0 a) k a l) /\
      a inSE (filter_zero_N y).
Proof.
  intros.
  destruct (insert_in_part_in_3 f0 k l a) as [y e0].
  exists y. split; trivial.
  pose (insert_in_part_in f0 k l a (f0 a, y)).
  simpl in e; apply e; trivial.
Qed.


Lemma mem_insert_prov_aux2 : forall e1 c a y k f0,
  In (f0 e1, y) c ->
  e1 inSE filter_zero_N y ->
  exists b,
  In (f0 e1, b) (insert_in_partition_p 0%N (f0 a) k a c).
Proof.
  induction c; intros; rewrite insert_in_partition_p_unfold.
  inversion H.
  destruct H; destruct a;
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros.
  exists ({|
       f := fun t0 : tupleT =>
            if Oeset.eq_bool (OTuple T) t0 a0
            then f k a0
            else f k0 t0;
       support := a0 addSE support k0;
       sort_krel := sort_krel k |}).
  left. now inversion H.
  exists k0.
  left.
  now inversion H.
  exists y; now right.
  apply (IHc a0 _ k _ H) in H0 as [b H0].
  exists b; now right.
Qed.















Lemma insert_in_krel_filter_zero : forall e1 c a y b k f0,
    In (f0 e1, y) c ->
    e1 inSE filter_zero_N y ->
    In (f0 e1, b) (insert_in_partition_p 0%N (f0 a) k a c) ->
    all_diff (map fst c) ->
    Oeset.eq_bool TN (f y e1) 0%N = false ->
    Oeset.eq_bool (OTuple T) e1 a = false ->
    (forall h, In h c ->  forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true ->
          Oeset.eq_bool TN (f (snd h) t1) (f (snd h) t2) = true) ->
    e1 inSE filter_zero_N b.
Proof.
  induction c; intros.
  inversion H.
  simpl in H.
  destruct H.
  - rewrite H in H1.
  rewrite H in H2.
  rewrite H in H5.
  clear H.
  rewrite insert_in_partition_p_unfold in H1.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) (f0 e1)); intros; rewrite H in H1.
  * unfold filter_zero in H0.
  rewrite Feset.filter_spec in H0.
  apply andb_true_iff in H0 as [H0a H0b].
  destruct H1.
  inversion H0.
  unfold filter_zero.
  unfold support.
  rewrite ! Feset.filter_spec; intros.
  unfold f.
  rewrite H4.
  simpl in H0a. unfold support in H0a. 
  simpl.
  rewrite FiniteSetImpl.add_spec_.
  simpl in H0a. rewrite H0a.
  rewrite if_same; simpl.
  now apply negb_true_iff.
  unfold f.
  do 2 f_equal.
  apply Oeset.eq_bool_true_compare_eq in H7.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H7).
  apply (H5 (f0 e1, y) (List.in_eq _ _)) in H7.
  apply Oeset.eq_bool_true_compare_eq in H7.
  apply N.compare_eq_iff in H7.
  simpl in H7. unfold f in H7. now rewrite H7.
  apply (all_diff_in_fst _ _ _ H0) in H2.
  inversion H2.
  intros.
  apply Oeset.eq_bool_true_compare_eq in H7.
  apply (H5 (f0 e1, y) (List.in_eq _ _)) in H7.
  apply Oeset.eq_bool_true_compare_eq in H7.
  apply N.compare_eq_iff in H7.
  simpl in H7. now rewrite H7.
  * destruct H1.
    inversion H1.
    now rewrite <- H7.
    apply (insert_all_diff f0 k _ a0) in H2.
    rewrite insert_in_partition_p_unfold in H2.
    rewrite H in H2.
    apply (all_diff_in_fst _ _ _ H1) in H2. 
    inversion H2.
  - rewrite insert_in_partition_p_unfold in H1.
    destruct a.
    case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros; rewrite H6 in H1.
    * destruct H1.
      inversion H1.
      rewrite <- H8 in H.
      apply (all_diff_in_fst _ _ _ H) in H2. 
      inversion H2.
      rewrite map_cons in H2.
      rewrite cons_app_nil in H2.
      apply all_diff_app2 in H2.
      apply (all_diff_fst _ H2 _ _ _ H1) in H.
      now rewrite H.
    * destruct H1.
      inversion H1.
      rewrite H8 in H2.
      apply (all_diff_in_fst _ _ _ H) in H2. 
      inversion H2.
      apply (IHc _ _ _ _ _ H H0 H1); trivial.
      rewrite map_cons in H2.
      rewrite cons_app_nil in H2.
      now apply all_diff_app2 in H2.
      intros.
      apply H5.
      now right. trivial.
Qed.

Lemma f_eq_insert : forall h1 f0 c a k,
    (forall h, In h c ->  forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true ->
          Oeset.eq_bool TN (f (snd h) t1) (f (snd h) t2) = true) ->
    In h1 (insert_in_partition_p 0%N (f0 a) k a c) ->
    forall t1 t2 : tupleT,
      Oeset.eq_bool (OTuple T) t1 t2 = true ->
      Oeset.eq_bool TN (f (snd h1) t1) (f (snd h1) t2) = true.
Proof.
  intros.
  induction c.
  destruct H0.
  rewrite <- H0.
  unfold snd. unfold f.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  apply Oeset.eq_bool_refl.
  inversion H0.
  rewrite insert_in_partition_p_unfold in H0.
  destruct a0.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a) l); intros; rewrite H2 in H0.
  destruct H0.
  rewrite <- H0.
  unfold snd. unfold f.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  apply (H (l, k0) (List.in_eq _ _)) in H1.
  apply Oeset.eq_bool_true_compare_eq in H1.
  apply N.compare_eq_iff in H1.
  simpl in H1.
  unfold f in H1. rewrite H1.
  apply Oeset.eq_bool_refl.
  apply (H h1); trivial.
  now right.
  destruct H0.
  apply (H h1); trivial.
  rewrite <- H0.
  now left.
  apply IHc; trivial.
  intros.
  apply H; trivial.
  now right.
Qed.

Lemma mem_partition_prov_aux : forall e1 l1 y c b k f0,
    In (f0 e1, y) c ->
    e1 inSE filter_zero_N y ->
    In (f0 e1, b) (partition_rec_krel 0%N f0 k c l1) ->
    all_diff (map fst c) ->
    Oeset.eq_bool TN (f y e1) 0%N = false ->
    Oeset.mem_bool (OTuple T) e1 l1 = false ->
    (forall h, In h c ->  forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true ->
          Oeset.eq_bool TN (f (snd h) t1) (f (snd h) t2) = true) ->
    e1 inSE filter_zero_N b.
Proof.
  induction l1; intros.
  simpl in H1.
  apply (all_diff_fst _ H2 _ _ _ H1) in H.
  now rewrite H.
  destruct (mem_insert_prov_aux2 _ _ a _ k _ H H0) as [b' e] .
  simpl in H4.
  apply orb_false_iff in H4 as [H4 H6].
  pose (insert_in_krel_filter_zero _ _ _ _ _ _ _ H H0 e H2 H3 H4 H5).
  apply (IHl1 _ _ _ _ _ e e0 H1); trivial.
  now apply insert_all_diff.
  unfold filter_zero in e0.
  rewrite Feset.filter_spec in e0.
  apply andb_true_iff in e0 as [_ e0].
  now apply negb_true_iff in e0.
  intros.
  apply Oeset.eq_bool_true_compare_eq in H8.
  apply (f_eq_insert _ _ _ _ _ H5 e) in H8.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H8).
  intro.
  apply f_eq_insert; trivial.
Qed.



Lemma mem_partition_prov : forall e1 l1 l c b k f0,
    In (l, b)
       (partition_rec_krel 0%N f0 k c l1) ->
    Oeset.mem_bool (OTuple T) e1 l1 = true ->
    f0 e1 = l ->
    Sorted.Sorted (fun x y => lt (OTuple T) x y) l1 ->
    all_diff (map fst c) ->
    (forall t, Oeset.mem_bool (OTuple T) t l1 = true -> Oeset.eq_bool TN (f k t) 0%N = false) ->
    (forall t1 t2, eq (OTuple T) t1 t2 -> f0 t1 = f0 t2) ->
    (forall h : list value * Krel N,
        In h c ->
        forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true ->
          Oeset.eq_bool TN (f (snd h) t1) (f (snd h) t2) = true) ->
    e1 inSE filter_zero_N b.
Proof.
  induction l1; intros.
  inversion H0.
  simpl in H0.
  simpl in H.
  apply orb_true_iff in H0 as [H0|H0].
  assert (Oeset.mem_bool (OTuple T) a l1 = false).
  clear H H0 H1 H3 H4 H5 H6 IHl1.
  induction l1; trivial.
  simpl.
  inversion H2.
  inversion H3.
  unfold Oeset.eq_bool. rewrite H5.
  apply IHl1.
  inversion H1.
  apply Sorted.Sorted_cons; trivial.
  destruct l1; trivial.
  apply Sorted.HdRel_cons.
  inversion H10.
  apply (Oeset.compare_lt_trans _ _ _ _ H5 H12).
  clear H2.
  destruct (insert_in_part_in_10 f0 k c a) as [y [H2 H8]]; trivial.
  apply H4.
  simpl. now rewrite Oeset.eq_bool_refl.
  apply Oeset.eq_bool_true_compare_eq in H0.
  rewrite (Feset.mem_eq_1 _ _ _ _ H0).
  rewrite <- H1 in H.
  assert (f0 e1 = f0 a).
  now apply H5 in H0.
  rewrite H9 in H.
  apply (mem_partition_prov_aux _ _ _ _ _ _ _ H2 H8 H); trivial.
  now apply insert_all_diff.
  unfold filter_zero in H8.
  rewrite Feset.filter_spec in H8.
  apply andb_true_iff in H8 as [_ H8].
  now apply negb_true_iff in H8.
  intros.
  apply Oeset.eq_bool_true_compare_eq in H11.
  apply (f_eq_insert _ _ _ _ _ H6 H2) in H11.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H11).
  intro.
  apply f_eq_insert; trivial.
  apply (IHl1 _ _ _ _ _ H H0 H1); trivial.
  now inversion H2.
  now apply insert_all_diff.
  intros.
  apply H4.
  simpl. rewrite H7. apply orb_true_r.
  intro.
  now apply f_eq_insert.
Qed.


(**************************************************)

Lemma insert_f : forall f0 k b t a c,
    In c (insert_in_partition_p 0%N (f0 a) k a b) ->
    all_diff (map fst b) ->
    Oeset.eq_bool (OTuple T) t a = true ->
    f0 a = fst c ->
    f (snd c) t = f k a.
Proof.
  induction b; intros.
  - simpl in H.
    destruct H.
    rewrite <- H.
    unfold snd.
    unfold f at 1.
    now rewrite H1.
    inversion H.
  - simpl in H. destruct a.
    case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros; rewrite H3 in H.
    * destruct H.
      rewrite <- H.
      unfold snd.
      unfold f at 1.
      now rewrite H1.
      apply Oset.eq_bool_true_iff in H3.
      rewrite H3 in H2.
      rewrite H2 in H0.
      apply (all_diff_in_fst _ _ _ H) in H0.
      inversion H0.
    * destruct H.
      rewrite <- H in H2.
      rewrite H2 in H3.
      rewrite Oset.eq_bool_refl in H3.
      inversion H3.
      apply IHb; trivial.
      rewrite cons_app_nil in H0.
      rewrite map_app in H0.
      now apply all_diff_app2 in H0.
Qed.


Lemma insert_f_2 : forall f0 k b t a c y,
    all_diff (map fst b) ->
    f0 t = c ->
    Oeset.eq_bool (OTuple T) t a = false ->
    In (c, y) b ->
    exists z, In (c, z) (insert_in_partition_p 0%N (f0 a) k a b)
    /\ f y t = f z t.
Proof.
  induction b; intros.
  inversion H2.
  destruct a.
  rewrite insert_in_partition_p_unfold.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros.
  - destruct H2.
    inversion H2.
    exists {|
       f := fun t0 : tupleT =>
            if Oeset.eq_bool (OTuple T) t0 a0
            then f k a0
            else f y t0;
       support := a0 addSE support y;
       sort_krel := sort_krel k |}.
    split.
    now left.
    unfold f. now rewrite H1.
    exists y. split; trivial.
    now right.
  - destruct H2.
    inversion H2.
    exists y; split; trivial. now left.
    rewrite cons_app_nil in H.
    rewrite map_app in H.
    apply all_diff_app2 in H.
    apply (IHb _ _ _ _ H H0 H1) in H2.
    destruct H2 as [z [H2a H2b]].
    exists z; split; trivial.
    now right.    
Qed.


Lemma partition_f : forall f0 k l b t c y,
    In c (partition_rec_krel 0%N f0 k b l) ->
    all_diff (map fst b) ->
    f0 t = fst c ->
    Oeset.mem_bool (OTuple T) t l = false ->
    In (fst c, y) b ->
    f (snd c) t = f y t.
Proof.
  induction l; intros.
  simpl in H.
  destruct c.
  apply (all_diff_fst _ H0 _ _ _ H) in H3.
  now rewrite H3.
  simpl in H.
  simpl in H2. apply orb_false_iff in H2 as [H2 H4].
  destruct (insert_f_2 f0 k _ _ _ _ H0 H1 H2 H3) as [z [H2a H2b]].

  apply (IHl _ t _ z) in H; trivial.
  now rewrite H2b.
  now apply (insert_all_diff f0 k _ a) in H0.
Qed.




Lemma partition_f_2 : forall f0 k l b t c,
    In c (partition_rec_krel 0%N f0 k b l) ->
    all_diff (map fst b) ->
    f0 t = fst c ->
    Oeset.mem_bool (OTuple T) t l = true ->
    (forall t1 t2 : tupleT,
        Oeset.eq_bool (OTuple T) t1 t2 = true ->
        f k t1 = f k t2) ->
    (forall t1 t2 : tupleT,
        Oeset.eq_bool (OTuple T) t1 t2 = true ->
        f0 t1 = f0 t2) ->
    Sorted.Sorted (fun x y => lt (OTuple T) x y) l ->
    f (snd c) t = f k t.
Proof.
  intros f0 k l b t c H H0 H1 H2 Hk Hf0 Hsort.
  apply Oeset.mem_bool_true_iff in H2 as [a [H2 H3]].
  apply in_split in H3 as [l1 [l2 H3]].
  rewrite H3 in H.
  rewrite partition_rec_krel_app in H.
  simpl in H.
  destruct (insert_in_part_in_3 f0 k (partition_rec_krel 0%N f0 k b l1) a) as [y H4].
  assert (all_diff
    (map fst
       (insert_in_partition_p 0%N (f0 a) k a
                              (partition_rec_krel 0%N f0 k b l1)))).
  apply insert_all_diff. now apply partition_all_diff.
  assert (Oeset.mem_bool (OTuple T) t l2 = false).
  rewrite H3 in Hsort.
  rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H2).
  clear H H0 H1 H2 H3 H4 H5 Hk.
  induction l1.
  now apply sorted_lt_cons_not_mem.
  apply IHl1.
  now inversion Hsort.
  apply Oeset.eq_bool_true_compare_eq in H2.
  assert (f0 a = f0 t).
  apply Hf0.
  now rewrite Oeset.eq_bool_sym.
  rewrite H7 in H4 at 1.
  rewrite H1 in H4.
  pose (partition_f _ _ _ _ _ _ y H H5 H1 H6 H4).
  rewrite e.
  assert (all_diff (map fst (partition_rec_krel 0%N f0 k b l1))).
  now apply partition_all_diff.
  assert ( f0 a = fst (fst c, y)).
  now rewrite H7.
  pose (insert_f _ _ _ _ _ _ H4 H8 H2 H9).
  simpl in e0.
  rewrite e0.
  apply Hk.
  now rewrite Oeset.eq_bool_sym.
Qed.





Lemma insert_f_3 : forall f0 k b t a c,
    all_diff (map fst b) ->
    f0 t <> fst c ->
    In c (insert_in_partition_p 0%N (f0 a) k a b) ->
    (forall t1 t2 : tupleT,
        Oeset.eq_bool (OTuple T) t1 t2 = true ->
        f0 t1 = f0 t2) ->
    f (snd c) t = 0%N \/ 
    (exists y, In (fst c, y) b /\ f (snd c) t = f y t).
Proof.
  induction b; intros t a' c H H0 H1 Hf0.
  simpl in H1.
  destruct H1.
  rewrite <- H1.
  unfold snd.
  simpl.
  left.
  rewrite <- H1 in H0.
  simpl in H0.
  assert (Oeset.eq_bool (OTuple T) t a' = false).
  apply not_true_iff_false.
  intro.
  destruct H0.
  now apply Hf0.
  now rewrite H2.
  inversion H1.
  destruct a.
  rewrite insert_in_partition_p_unfold in H1.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a') l); intros; rewrite H2 in H1.
  - destruct H1.
    rewrite <- H1.
    simpl.
    right.
    exists k0. split.
    now left. 
    rewrite <- H1 in H0.
    simpl in H0.
    assert (Oeset.eq_bool (OTuple T) t a' = false).
    apply not_true_iff_false.
    intro.
    destruct H0.
    apply Oset.eq_bool_true_iff in H2.
    rewrite <- H2.
    now apply Hf0.
    now rewrite H3.
    right.
    exists (snd c). split; trivial. destruct c. now right.
  - destruct H1.
    rewrite <- H1.
    right; exists k0; split; trivial. now left.
    rewrite cons_app_nil in H.
    rewrite map_app in H.
    apply all_diff_app2 in H.
    apply (IHb _ _ _ H H0) in H1; trivial.
    destruct H1 as [H1|[y [H1a H1b]]].
    now left.
    right; exists y; split; trivial. now right.
Qed.



Lemma partition_f_3 : forall f0 k l b t c,
    In c (partition_rec_krel 0%N f0 k b l) ->
    all_diff (map fst b) ->
    f0 t <> fst c ->
    (forall t1 t2 : tupleT, Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    f (snd c) t = 0%N \/ 
      (exists y : Krel N, In (fst c, y) b /\ f (snd c) t = f y t).
Proof.
  induction l; intros b t c H H0 H1 Hf0.
  simpl in H.
  destruct c. right; exists k0; split; trivial.
  simpl in H.
  apply (IHl _ t) in H; trivial.
  destruct H as [H|[x [Ha Hb]]]. 
  now left.
  assert (f0 t <> fst (fst c, x)); trivial.
  destruct (insert_f_3 _ _ _ _ _ _ H0 H Ha); trivial; rewrite Hb.
  now left.
  now right.
  now apply (insert_all_diff f0 k _ a) in H0.
Qed.

Lemma insert_f_4 : forall f0 k b t a c,
    all_diff (map fst b) ->
    In c (insert_in_partition_p 0%N (f0 a) k a b) ->
    Oeset.eq_bool (OTuple T) t a = false -> 
    f (snd c) t = 0%N \/ 
    (exists y, In (fst c, y) b /\ f (snd c) t = f y t).
Proof.
  induction b; intros.
  simpl in H0.
  destruct H0.
  rewrite <- H0.
  unfold snd.
  simpl.
  rewrite H1.
  now left.
  inversion H0.
  destruct a.
  rewrite insert_in_partition_p_unfold in H0.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros; rewrite H2 in H0.
  - destruct H0.
    rewrite <- H0.
    simpl.
    right.
    exists k0. split.
    now left.
    now rewrite H1.
    right.
    exists (snd c). split; trivial. destruct c. now right.
  - destruct H0.
    rewrite <- H0.
    right; exists k0; split; trivial. now left.
    rewrite cons_app_nil in H.
    rewrite map_app in H.
    apply all_diff_app2 in H.
    apply (IHb t _ _ H) in H0; trivial.
    destruct H0 as [H0|[y [H0a H0b]]].
    now left.
    right; exists y; split; trivial. now right.
Qed.


Lemma partition_f_4 : forall f0 k l b t c,
    In c (partition_rec_krel 0%N f0 k b l) ->
    all_diff (map fst b) ->
    Oeset.mem_bool (OTuple T) t l = false -> 
    f (snd c) t = 0%N \/
      (exists y : Krel N, In (fst c, y) b /\ f (snd c) t = f y t).
Proof.
  induction l; intros.
  simpl in H.
  right.
  exists (snd c); split; trivial.
  now destruct c.
  simpl in H.
  apply (IHl _ t) in H as [H|[y [Ha Hb]]]; trivial.
  now left.
  apply (insert_f_4 _ _ _ t _ _) in Ha; trivial. simpl in Ha.
  rewrite <- Hb in Ha.
  trivial.
  simpl in H1. now apply orb_false_iff in H1 as [H1 _].
  now apply insert_all_diff.
  simpl in H1. now apply orb_false_iff in H1 as [_ H1].
Qed.

Lemma partition_f_def : forall f0 k t c,
    In c (partition 0%N TN f0 k) ->
    f0 t = fst c ->
    t inSE (filter_zero_N k) ->
    (forall t1 t2 : tupleT, Oeset.eq_bool (OTuple T) t1 t2 = true -> f k t1 = f k t2) ->
    (forall t1 t2 : tupleT, Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Sorted.Sorted (fun x y : tupleT => lt (OTuple T) x y) (Feset.elements FesetT (filter_zero_N k))->
    f (snd c) t = f k t.
Proof.
  intros f0 k t c H H0 H1 Hk Hf0 Hsort.
  unfold partition in H.
  rewrite Feset.mem_elements in H1.
  apply (partition_f_2 _ _ _ _ _ H); trivial.
  simpl; trivial.
Qed.

Lemma partition_f_def_2 : forall f0 k t c,
    In c (partition 0%N TN f0 k) ->
    f0 t <> fst c ->
    (forall t1 t2 : tupleT, Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    f (snd c) t = 0%N.
Proof.
  intros.
  unfold partition in H.
  destruct (partition_f_3 _ _ _ _ t _ H) as [H2|[y [H2 H3]]]; trivial.
  simpl; trivial.
  inversion H2.
Qed.

Lemma partition_f_def_3 : forall f0 k t c,
    In c (partition 0%N TN f0 k) ->
    t inSE? filter_zero_N k = false -> 
    f (snd c) t = 0%N.
Proof.
  intros.
  unfold partition in H.
  rewrite Feset.mem_elements in H0.
  destruct (partition_f_4 _ _ _ _ t _ H) as [H1|[y [H1 H2]]]; trivial.
  simpl; trivial.
  inversion H1.
Qed.


Lemma insert_rel_nb_occ_4 : forall f0 b t a c,
    all_diff (map fst b) ->
    In c (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) ->
    Oeset.eq_bool (OTuple T) t a = false -> 
    Oeset.nb_occ (OTuple T) t (snd c) = 0%N \/ 
    (exists y, In (fst c, y) b /\ Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t y).
Proof.
  induction b; intros.
  simpl in H0.
  destruct H0.
  rewrite <- H0.
  unfold snd.
  simpl.
  unfold Oeset.eq_bool in H1.
  destruct (t_compare (OTuple T) t a); inversion H1; now left.
  inversion H0.
  destruct a.
  simpl in H0.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l); intros; rewrite H2 in H0.
  - destruct H0.
    rewrite <- H0.
    simpl.
    right.
    exists l0. split.
    now left.
    unfold Oeset.eq_bool in H1.
    destruct (t_compare (OTuple T) t a0); inversion H1; trivial.
    right.
    exists (snd c). split; trivial. destruct c. now right.
  - destruct H0.
    rewrite <- H0.
    right; exists l0; split; trivial. now left.
    rewrite cons_app_nil in H.
    rewrite map_app in H.
    apply all_diff_app2 in H.
    apply (IHb t _ _ H) in H0; trivial.
    destruct H0 as [H0|[y [H0a H0b]]].
    now left.
    right; exists y; split; trivial. now right.
Qed.



Lemma partition_partition_f_4 : forall f0 l b t c,
    In c (partition_rec (Oset.mk_list (OVal T)) f0 b l) ->
    all_diff (map fst b) ->
    Oeset.mem_bool (OTuple T) t l = false -> 
    Oeset.nb_occ (OTuple T) t (snd c) = 0%N \/
      (exists y, In (fst c, y) b /\ Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t y).
Proof.
  induction l; intros.
  simpl in H.
  right.
  exists (snd c); split; trivial.
  now destruct c.
  simpl in H.
  apply (IHl _ t) in H as [H|[y [Ha Hb]]]; trivial.
  now left.
  apply (insert_rel_nb_occ_4 _ _ t _ _) in Ha; trivial. simpl in Ha.
  rewrite <- Hb in Ha.
  trivial.
  simpl in H1. now apply orb_false_iff in H1 as [H1 _].
  now apply insert_in_partition_all_diff_values.
  simpl in H1. now apply orb_false_iff in H1 as [_ H1].
Qed.

  
Lemma partition_partition_f_def_3 : forall f0 l t c,
   In c (Partition.partition (Oset.mk_list (OVal T)) f0 l) ->
   Oeset.mem_bool (OTuple T) t l = false ->
   Oeset.nb_occ (OTuple T) t (snd c) = 0%N.
Proof.
  intros.
  unfold Partition.partition in H.
  destruct (partition_partition_f_4 _ _ _ t _ H) as [H1|[y [H1 H2]]]; trivial.
  simpl; trivial.
  inversion H1.
Qed.

Lemma insert_rel_in_3 : forall A f0 l (a : A),
    exists y,
    In (f0 a, y) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a l).
Proof.
  induction l; intros.
  - exists (a::nil).
    now left.
  - simpl.
    destruct a.
    case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a0) l0); intros.
    apply Oset.eq_bool_true_iff in H.
    rewrite H.
    exists (a0 :: l1).
    now left.
    destruct (IHl a0) as [y H0].
    exists y; now right.
Qed.




Lemma partition_partition_f_7 : forall f0 l b y t c,
    In c (partition_rec (Oset.mk_list (OVal T)) f0 b l) ->
    all_diff (map fst b) ->
    Oeset.mem_bool (OTuple T) t l = false -> 
    In (fst c, y) b ->
    Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t y.
Proof.
  induction l; intros.
  simpl in H.
  destruct c.
  apply (all_diff_fst _ H0 _ _ _ H) in H2.
  now rewrite H2.
  simpl in H.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a) (fst c)); intros.
  - apply Oset.eq_bool_true_iff in H3.
    destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a b) as [[l1 [l2 [l3 [H4 H5]]]]|H4].
    * assert (In (f0 a, a:: l1) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b)).
      rewrite H5.
      apply in_or_app; now right; left.
      apply (IHl _ (a::l1) t _) in H.
      rewrite H.
      simpl in H1.
      apply orb_false_iff in H1 as [H1 _].
      rewrite <- H3 in H2.
      assert (In (f0 a, l1) b).
      rewrite H4; apply in_or_app; right; now left.
      apply (all_diff_fst _ H0 _ _ _ H2) in H7.
      rewrite H7.
      simpl.
      unfold Oeset.eq_bool in H1.
      destruct (t_compare (OTuple T) t a); trivial; inversion H1.
      now apply insert_in_partition_all_diff_values.
      simpl in H1.
      now apply orb_false_iff in H1 as [_ H1].
      now rewrite <- H3.
    * apply (insert_in_partition_all_diff_values (Oset.mk_list (OVal T)) (f0 a) a) in H0.
      rewrite H4 in H0.
      rewrite map_app in H0.
      assert (In (f0 a) (map fst b)).
      rewrite <- H3 in H2.
      clear H0 H H1 H3 H4 IHl.
      induction b; simpl;  trivial.
      destruct H2.
      rewrite H; now left.
      right; now apply IHb.
      assert (In (f0 a) (map fst ((f0 a, a ::nil)::nil))).
      now left.
      apply (all_diff_app _ _ H0 _ H5) in H6.
      destruct H6.
  - assert (In (fst c, y) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b)).
    destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a b) as [[l1 [l2 [l3 [H4 H5]]]]|H4].
    * rewrite H5.
      rewrite H4 in H2.
      apply in_app_or in H2 as [H2|[H2|H2]]; apply in_or_app.
      now left.
      apply not_true_iff_false in H3.
      destruct H3.
      inversion H2.
      apply Oset.eq_bool_refl.
      now do 2 right.
    * rewrite H4.
      apply in_or_app. now left.
    * apply (IHl _ y t _) in H; trivial.
      now apply insert_in_partition_all_diff_values.
      simpl in H1.
      now apply orb_false_iff in H1 as [_ H1].
Qed.


Lemma in_fst : forall A B (a:A*B) l,
    In a l -> In (fst a) (map fst l).
Proof.
    induction l; intros.
    inversion H.
    destruct H; [> rewrite H; now left|right; now apply IHl].
Qed.


Lemma insert_rel_nb_occ_10 : forall f0 b a t c,
    f0 a = c ->
    all_diff (map fst b) ->
    Oeset.eq_bool (OTuple T) t a = true ->
    exists z, In (c, z) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) /\
    (forall y, In (c, y) b ->
               Oeset.nb_occ (OTuple T) t z = N.add 1%N (Oeset.nb_occ (OTuple T) t y)).
Proof.
  intros.
  destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a b) as [[l1 [l2 [l3 [H5 H6]]]]|H5].
  - rewrite H6.
    rewrite H5.
    rewrite <- H.
    exists (a :: l1); split.
    apply in_or_app.
    right; now left.
    intros.
    rewrite H5 in H0.
    apply (all_diff_fst _ H0 _ _ l1) in H2. 
    rewrite H2.
    apply Oeset.eq_bool_true_compare_eq in H1.
    simpl.
    now rewrite H1.
    apply in_or_app.
    right; now left.
  - rewrite H5.
    rewrite <- H.
    exists (a::nil); split.
    apply in_or_app.
    right; now left.
    intros.
    apply (insert_in_partition_all_diff_values (Oset.mk_list (OVal T)) (f0 a) a) in H0.
    rewrite H5 in H0.
    rewrite map_app in H0.
    apply in_fst in H2.
    apply (all_diff_app _ _ H0) in H2.
    destruct H2.
    rewrite map_cons.
    now left.
Qed.

Lemma insert_rel_nb_occ_9 : forall f0 b a t c,
    f0 a = c ->
    Oeset.eq_bool (OTuple T) t a = true ->
    exists z, In (c, z) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) /\
    ((exists y, In (c, y) b /\
               Oeset.nb_occ (OTuple T) t z = N.add 1%N (Oeset.nb_occ (OTuple T) t y)) \/ Oeset.nb_occ (OTuple T) t z = 1%N).
Proof.
  intros.
  destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a b) as [[l1 [l2 [l3 [H5 H6]]]]|H5].
  - rewrite H6.
    rewrite H5.
    rewrite <- H.
    exists (a :: l1); split.
    apply in_or_app.
    right; now left.
    left; exists l1; split.
    apply in_or_app.
    right; now left.
    apply Oeset.eq_bool_true_compare_eq in H0.
    simpl.
    now rewrite H0.
  - rewrite H5.
    rewrite <- H.
    exists (a::nil); split.
    apply in_or_app.
    right; now left.
    right.
    apply Oeset.eq_bool_true_compare_eq in H0.
    simpl.
    now rewrite H0.
Qed.


Lemma insert_in_nb : forall f0 b c t a,
    In c b ->
    all_diff (map fst b) ->
    f0 a <> fst c ->
    exists z, In (fst c, z) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) /\
    Oeset.nb_occ (OTuple T) t z  = Oeset.nb_occ (OTuple T) t (snd c).
Proof.
  intros.
  destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a b) as [[l1 [l2 [l3 [H5 H6]]]]|H5].
  rewrite H6.
  rewrite H5 in H.
  apply in_app_or in H.
  destruct H as [H|[H|H]].
  * exists (snd c); split; trivial.
    destruct c.
    apply in_or_app.
    now left.
  * rewrite <- H in H1.
    now destruct H1.
  * exists (snd c); split; trivial.
    destruct c.
    apply in_or_app.
    right; now right.
  * rewrite H5.
    exists (snd c); split; trivial.
    destruct c.
    apply in_or_app.
    now left.
 Qed.

Lemma insert_in_nb2 : forall f0 b c t a,
    In c b ->
    all_diff (map fst b) ->
    f0 a = fst c ->
    Oeset.eq_bool (OTuple T) t a = false ->
    exists z, In (fst c, z) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) /\
    Oeset.nb_occ (OTuple T) t z  = Oeset.nb_occ (OTuple T) t (snd c).
Proof.
  intros.
  destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a b) as [[l1 [l2 [l3 [H5 H6]]]]|H5].
  rewrite H6.
  rewrite H5 in H.
  apply in_app_or in H.
  destruct H as [H|[H|H]].
  * rewrite H5 in H0.
    apply in_fst in H.
    rewrite map_app in H0.
    apply (all_diff_app _ _ H0) in H.
    destruct H.
    rewrite H1.
    rewrite map_cons; now left.
  * rewrite <- H.
    exists (a::l1); split.
    apply in_or_app.
    right; now left.
    simpl (Oeset.nb_occ (OTuple T) t (a :: l1)).
    unfold Oeset.eq_bool in H2.
    destruct (t_compare (OTuple T) t a); trivial; inversion H2.
  * rewrite H5 in H0.
    apply in_fst in H.
    rewrite map_app in H0.
    rewrite map_cons in H0.
    rewrite cons_app_nil, app_assoc in H0.
    apply (all_diff_app _ _ H0) in H.
    destruct H.
    rewrite H1.
    apply in_or_app. right; now left.
  * exists (snd c); split; trivial.
    destruct c.
    rewrite H5.
    apply in_or_app.
    now left.
 Qed.

Lemma insert_in_nb3 : forall f0 b c t a,
    In c b ->
    all_diff (map fst b) ->
    Oeset.eq_bool (OTuple T) t a = false ->
    exists z, In (fst c, z) (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) /\
    Oeset.nb_occ (OTuple T) t z  = Oeset.nb_occ (OTuple T) t (snd c).
Proof.
  intros.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a) (fst c)); intros.
  apply Oset.eq_bool_true_iff in H2.
  now apply insert_in_nb2.
  apply insert_in_nb; trivial.
  intro.
  apply not_true_iff_false in H2.
  destruct H2.
  now apply Oset.eq_bool_true_iff.
Qed.
  
Lemma partition_partition_f_2_aux : forall f0 l b y t c,
    In c (Partition.partition_rec (Oset.mk_list (OVal T)) f0 b l) ->
    all_diff (map fst b) ->
    f0 t = fst c ->
    Oeset.mem_bool (OTuple T) t l = true ->
    In (fst c, y) b ->
    (forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Oeset.nb_occ (OTuple T) t (snd c) = N.add (Oeset.nb_occ (OTuple T) t l) (Oeset.nb_occ (OTuple T) t y).
Proof.
  induction l; intros b y t c H H0 H1 H2 H3 Hf0; try solve [inversion H2].
  simpl in H2.
  case_eq (Oeset.eq_bool (OTuple T) t a); intros.
  - simpl.
    generalize H4; intros.
    apply Oeset.eq_bool_true_compare_eq in H5.
    rewrite H5.
    clear H5.
    case_eq (Oeset.mem_bool (OTuple T) t l); intros Hmem.
    * assert (f0 a = fst c). rewrite <- H1. apply Hf0.
      now rewrite Oeset.eq_bool_sym.
      apply (insert_rel_nb_occ_10 f0 b a t) in H5 as [z [H6 H7]]; trivial.
      apply H7 in H3.
      rewrite (IHl _ z t _ H); trivial.
      rewrite H3.
      rewrite N.add_assoc.
      f_equal.
      apply N.add_comm.
     now apply insert_in_partition_all_diff_values.
    * assert (f0 a = fst c). rewrite <- H1. apply Hf0.
      now rewrite Oeset.eq_bool_sym.
      apply (insert_rel_nb_occ_10 f0 b a t) in H5 as [z [H6 H7]]; trivial.
      simpl in H.
      apply (partition_partition_f_7 _ _ _ _ t _ H) in H6; trivial.
      apply Oeset.not_mem_nb_occ in Hmem.
      rewrite Hmem.
      rewrite H6.
      apply H7 in H3.
      rewrite H3.
      trivial.
      now apply insert_in_partition_all_diff_values.
  - rewrite H4 in H2.
    apply (insert_in_nb3 f0 b (fst c,y) t a) in H3 as [y' [H3a H3b]]; trivial.
    simpl in H.
    rewrite (IHl _ y' t _ H); trivial.
    rewrite H3b.
    f_equal.
    simpl.
    unfold Oeset.eq_bool in H4.
    destruct (t_compare (OTuple T) t a); trivial; inversion H4.
    now apply insert_in_partition_all_diff_values.
Qed.

Lemma partition_partition_f_2 : forall f0 l b t c,
    In c (Partition.partition_rec (Oset.mk_list (OVal T)) f0 b l) ->
    all_diff (map fst b) ->
    f0 t = fst c ->
    Oeset.mem_bool (OTuple T) t l = true ->
    (forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    (exists y, In (fst c, y) b /\
               Oeset.nb_occ (OTuple T) t (snd c) = N.add (Oeset.nb_occ (OTuple T) t l) (Oeset.nb_occ (OTuple T) t y)) \/ Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t l.
Proof.
  induction l; intros b t c H H0 H1 H2 Hf0; try solve [inversion H2].
  simpl in H2.
  case_eq (Oeset.eq_bool (OTuple T) t a); intros.
    simpl.
    generalize H3; intros.
    apply Oeset.eq_bool_true_compare_eq in H4.
    rewrite H4.
    clear H4.
  - case_eq (Oeset.mem_bool (OTuple T) t l); intros.
    * simpl in H.
      assert (f0 a = fst c). rewrite <- H1. apply Hf0.
      now rewrite Oeset.eq_bool_sym.
      apply (insert_rel_nb_occ_9 f0 b a t) in H5 as [z [H6 H7]]; trivial.
      apply (partition_partition_f_2_aux _ _ _ _ t _ H) in H6; trivial.
      rewrite H6.
      destruct H7 as [[y [H7 H8]]|H7].
      rewrite H8.
      left; exists y; split; trivial.
      rewrite N.add_assoc.
      f_equal.
      apply N.add_comm.
      rewrite H7. right; apply N.add_comm.
      now apply insert_in_partition_all_diff_values.
    * assert (f0 a = fst c). rewrite <- H1. apply Hf0.
      now rewrite Oeset.eq_bool_sym.  
      apply (insert_rel_nb_occ_9 f0 b a t) in H5 as [z [H6 H7]]; trivial.
      simpl in H.
      apply (partition_partition_f_7 _ _ _ _ t _ H) in H6; trivial.
      apply Oeset.not_mem_nb_occ in H4.
      rewrite H4.
      rewrite H6.
      destruct H7 as [[y [H7 H8]]|H7].
      rewrite H8.
      left; exists y; split; trivial.
      rewrite H7.
      now right.
      now apply insert_in_partition_all_diff_values.
  - rewrite H3 in H2.
    destruct (IHl _ t _ H); trivial.
    now apply insert_in_partition_all_diff_values.
    destruct H4 as [y [H5 H6]].
    rewrite H6.
    destruct (insert_rel_nb_occ_4 _ _ _ _ _ H0 H5 H3) as [H7|[y' [H7 H8]]].
    simpl in H7.
    rewrite H7.
    right.
    simpl.
    unfold Oeset.eq_bool in H3.
    destruct (t_compare (OTuple T) t a); inversion H3; try apply N.add_comm.
    left; exists y'; split; trivial.
    rewrite <- H8.
    simpl.
    unfold Oeset.eq_bool in H3.
    destruct (t_compare (OTuple T) t a); inversion H3; trivial.
    right. rewrite H4.
    simpl.
    unfold Oeset.eq_bool in H3.
    destruct (t_compare (OTuple T) t a); trivial; inversion H3.
Qed.


Lemma partition_partition_f_def : forall f0 l t c,
    In c (Partition.partition (Oset.mk_list (OVal T)) f0 l) ->
    f0 t = fst c ->
    Oeset.mem_bool (OTuple T) t l = true ->
    (forall t1 t2 : tupleT, Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t l.
Proof.
  intros.
  unfold Partition.partition in H.
  apply (partition_partition_f_2 _ _ _ t) in H; trivial.
  destruct H as [[y [H _]]|H]; trivial.
  inversion H.
  simpl; trivial.
Qed.

Lemma insert_rel_nb_occ_3 : forall f0 b t a c,
    all_diff (map fst b) ->
    f0 t <> fst c ->
    In c (insert_in_partition (Oset.mk_list (OVal T)) (f0 a) a b) ->
    (forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Oeset.nb_occ (OTuple T) t (snd c) = 0%N \/ 
    (exists y, In (fst c, y) b /\ Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t y).
Proof.
  induction b; intros t a' c H H0 H1 Hf0.
  simpl in H1.
  destruct H1.
  rewrite <- H1.
  unfold snd.
  simpl.
  left.
  rewrite <- H1 in H0.
  simpl in H0.
  assert (Oeset.eq_bool (OTuple T) t a' = false).
  apply not_true_iff_false; intro; destruct H0.
  now apply Hf0.
  unfold Oeset.eq_bool in H2.
  destruct (t_compare (OTuple T) t a'); trivial; inversion H2.
  inversion H1.
  destruct a.
  simpl in H1.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a') l); intros; rewrite H2 in H1.
  - destruct H1.
    rewrite <- H1.
    simpl.
    right.
    exists l0. split.
    now left. 
    rewrite <- H1 in H0.
    simpl in H0.
    assert (Oeset.eq_bool (OTuple T) t a' = false).
    apply not_true_iff_false; intro; destruct H0.
    apply Oset.eq_bool_true_iff in H2.
    rewrite <- H2.
    now apply Hf0.
    unfold Oeset.eq_bool in H3.
    destruct (t_compare (OTuple T) t a'); trivial; inversion H3.
    right.
    exists (snd c). split; trivial. destruct c. now right.
  - destruct H1.
    rewrite <- H1.
    right; exists l0; split; trivial. now left.
    rewrite cons_app_nil in H.
    rewrite map_app in H.
    apply all_diff_app2 in H.
    apply (IHb _ _ _ H H0) in H1; trivial.
    destruct H1 as [H1|[y [H1a H1b]]].
    now left.
    right; exists y; split; trivial. now right.
Qed.



Lemma partition_partition_f_3 : forall f0 l b t c,
    In c (Partition.partition_rec (Oset.mk_list (OVal T)) f0 b l) ->
    all_diff (map fst b) ->
    f0 t <> fst c ->
    (forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Oeset.nb_occ (OTuple T) t (snd c) = 0%N \/
      (exists y, In (fst c, y) b /\ Oeset.nb_occ (OTuple T) t (snd c) = Oeset.nb_occ (OTuple T) t y).
Proof.
  induction l; intros  b t c H H0 H1 Hf0.
  simpl in H.
  destruct c. right; exists l0; split; trivial.
  simpl in H.
  apply (IHl _ t) in H; trivial.
  destruct H as [H|[x [Ha Hb]]]. 
  now left.
  assert (f0 t <> fst (fst c, x)); trivial.
  destruct (insert_rel_nb_occ_3 _ _ _ _ _ H0 H Ha); trivial;
  rewrite Hb.
  now left.
  now right.
  now apply insert_in_partition_all_diff_values.
Qed.


Lemma partition_partition_f_def_2 : forall f0 l t c,
    In c (Partition.partition (Oset.mk_list (OVal T)) f0 l) ->
    f0 t <> fst c ->
    (forall t1 t2 : tupleT,
          Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Oeset.nb_occ (OTuple T) t (snd c) = 0%N.
Proof.
  intros.
  unfold Partition.partition in H.
  destruct (partition_partition_f_3 _ _ _ t _ H) as [H2|[y [H2 H3]]]; trivial.
  simpl; trivial.
  inversion H2.
Qed.


Lemma insert_in_l : forall f0 k a b c,
    f k a <> 0%N ->
    In c (insert_in_partition_p 0%N (f0 a) k a b) ->
    In c b \/
    (exists x0 : tupleT, In x0 (Feset.elements FesetT (support (snd c))) /\ f (snd c) x0 <> 0%N).
Proof.
  induction b; intros.
  destruct H0.
  rewrite <- H0.
  right.
  simpl.
  exists a; split.
  now left.
  now rewrite Oeset.eq_bool_refl.
  inversion H0.
  rewrite insert_in_partition_p_unfold in H0.
  destruct a0.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a) l); intros; rewrite H1 in H0; destruct H0.
  rewrite <- H0.
  right.
  pose (Feset.add_spec FesetT (support k0) a a).
  rewrite Oeset.eq_bool_refl in e.
  rewrite Feset.mem_elements in e.
  apply Oeset.mem_bool_true_iff in e as [a0 [H2 H3]].
  exists a0; split; trivial.
  simpl.
  apply Oeset.eq_bool_true_compare_eq in H2.
  rewrite Oeset.eq_bool_sym. rewrite H2.
  trivial.
  left; now right.
  rewrite H0.
  left; now left.
  apply IHb in H0.
  destruct H0 as [H0|H0].
  left; now right.
  now right.
  trivial.
Qed.


Lemma partition_in_l : forall f0 k l b c,
    (forall a, In a l -> f k a <> 0%N) ->
    In c (partition_rec_krel 0%N f0 k b l) ->
    In c b \/ exists x0 : tupleT, In x0 (Feset.elements FesetT (support (snd c))) /\ f (snd c) x0 <> 0%N.
Proof.
  induction l; intros.
  - simpl in H0. now left.
  - simpl in H0.
    apply IHl in H0.
    destruct H0 as [H0|[x1 H0]].
    revert H0.
    apply insert_in_l.
    apply H. now left.
    right; now exists x1. intros. apply H. now right.
Qed.



Lemma equiv_envp_env_g : forall env f0 a1 a2,
    eq (OTuple T) a1 a2 ->
    equiv_envp (create_env (env_g T env (Group_By f0) (a1 :: nil)))
               (create_env (env_g T env (Group_By f0) (a2 :: nil))).
Proof.
  intros.
  unfold equiv_envp.
  apply Forall2_cons.
  repeat split.
  - apply tuple_eq in H.
    apply H.
  - apply Feset.equal_spec; intros.
    unfold filter_zero.
    rewrite ! Feset.filter_spec.
    rewrite ! support_create_krel.
    rewrite ! f_create_krel.
    simpl.
    unfold Oeset.eq_bool.
    case_eq (t_compare (OTuple T) e a1); intros.
    now rewrite (Oeset.compare_eq_trans _ _ _ _ H0 H).
    now rewrite (Oeset.compare_lt_eq_trans _ _ _ _ H0 H).
    now rewrite (Oeset.compare_gt_eq_trans _ _ _ _ H0 H).
    intros.
    rewrite ! f_create_krel.
    now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H1).
    intros.
    rewrite ! f_create_krel.
    now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H1).
  - intros.
    rewrite ! f_create_krel.
    simpl.
  case_eq (t_compare (OTuple T) t1 a1); intros; rewrite Oeset.eq_bool_sym in H0; apply Oeset.eq_bool_true_compare_eq in H0.
  apply (Oeset.compare_eq_trans _ _ _ _ H1) in H.
  now rewrite (Oeset.compare_eq_trans _ _ _ _ H0 H).
  apply (Oeset.compare_lt_eq_trans _ _ _ _ H1) in H.
  now rewrite (Oeset.compare_eq_lt_trans _ _ _ _ H0 H).
  apply (Oeset.compare_gt_eq_trans _ _ _ _ H1) in H.
  now rewrite (Oeset.compare_eq_gt_trans _ _ _ _ H0 H).
  - apply equiv_env_eq_create_env.
Qed.



Lemma equiv_envp_env_g_gp : forall env f1 a q,
    a inSE filter_zero_N (eval_query_prov_N (create_env env) q) ->
    (forall e e', eq (OTuple T) e e' ->
                  Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e) 0%N =
                  Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e') 0%N) ->
    equiv_envp (create_env (env_g T env (Group_By f1) (a :: nil)))
    (env_gp (create_env env) (Group_By f1)
       {|
       f := fun t0 : tupleT => if Oeset.eq_bool (OTuple T) t0 a then 1%N else 0%N;
       support := Feset.singleton FesetT a;
       sort_krel := sort_krel (eval_query_prov_N (create_env env) q) |}).
Proof. 
  intros.
  apply Forall2_cons.
  repeat split.
  - rewrite sort_krel_create_krel.
    rewrite quicksort_1.
    apply (sort_labels_supp  (Krel_inv_eval_N (create_env env) q)).
    unfold filter_zero in H.
    rewrite Feset.filter_spec in H.
    now apply andb_true_iff in H as [H _].
    intros.
    f_equal.
    now apply H0.
  - apply (feset_equal_trans _ _ _ _ (support_create_krel_filter_zero _ _)).
    apply Feset.equal_spec.
    intros. rewrite support_create_krel.
    unfold filter_zero. unfold f. unfold support.
    rewrite Feset.filter_spec.
    rewrite Feset.singleton_spec.
    simpl.
    destruct (Oeset.eq_bool (OTuple T) e a); trivial.
    intros.
    apply Oeset.eq_bool_true_compare_eq in H2.
    now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H2).
    - intros.
    rewrite f_create_krel.
    unfold f.
    rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H1).
    simpl.
    unfold Oeset.eq_bool.
    destruct (t_compare (OTuple T) t1 a); trivial.
    - apply equiv_env_eq_create_env.
Qed.

Lemma equiv_envp_env_gp : forall env f0 a1 a2 q,
    eq (OTuple T) a1 a2 ->
    equiv_envp
      (env_gp (create_env env) (Group_By f0)
       {|
       f := fun t0 : tupleT => if Oeset.eq_bool (OTuple T) t0 a1 then 1%N else 0%N;
       support := FiniteSetImpl.singleton_ (OTuple T) a1;
       sort_krel := sort_krel (eval_query_prov_N (create_env env) q) |})
      (env_gp (create_env env) (Group_By f0)
       {|
       f := fun t0 : tupleT => if Oeset.eq_bool (OTuple T) t0 a2 then 1%N else 0%N;
       support := FiniteSetImpl.singleton_ (OTuple T) a2;
       sort_krel := sort_krel (eval_query_prov_N (create_env env) q) |}).
Proof.
  intros.
  apply Forall2_cons.
  repeat split.
  - apply Fset.equal_refl.
  - unfold filter_zero.
    unfold f. unfold support.
    apply Feset.equal_spec.
    intro.
    rewrite ! Feset.filter_spec.
    simpl.
    rewrite ! FiniteSetImpl.singleton_spec_.
    unfold Oeset.eq_bool.
    case_eq (t_compare (OTuple T) e a1); intros.
    now rewrite (Oeset.compare_eq_trans _ _ _ _ H0 H).
    now rewrite (Oeset.compare_lt_eq_trans _ _ _ _ H0 H).
    now rewrite (Oeset.compare_gt_eq_trans _ _ _ _ H0 H).
    intros.
    apply Oeset.eq_bool_true_compare_eq in H1.
    now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).
    intros.
    apply Oeset.eq_bool_true_compare_eq in H1.
    now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H1).
  - intros.
    unfold f.
    apply Oeset.eq_bool_true_compare_eq in H.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ H).
    now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  - apply equiv_env_eq_create_env.
Qed.

Lemma interp_aggterm_eq_env_g : forall env f0 a2 a1 a,
    eq (OTuple T) a1 a2 ->
    interp_aggterm (env_g T env (Group_By f0) (a1 :: nil)) a =
    interp_aggterm (env_g T env (Group_By f0) (a2 :: nil)) a.
Proof.
  intros.  
  rewrite 2 interp_aggterm_equiv.
  apply interp_aggtermp_eq.
  now apply equiv_envp_env_g.
Qed.

Lemma map_interp_aggterm : forall env a f0 q,
    a inSE filter_zero_N (eval_query_prov_N (create_env env) q) ->
    (forall e e', eq (OTuple T) e e' ->
                  Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e) 0%N =
                  Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e') 0%N) ->
    map (fun f2 : aggterm => interp_aggterm (env_g T env (Group_By f0) (a :: nil)) f2) f0 =
    map (fun h : aggterm => interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov                             (env_gp (create_env env) (Group_By f0)
           {|
             f := fun t0 : tupleT =>
                    if Oeset.eq_bool (OTuple T) t0 a
                    then 1%N
                    else 0%N;
             support := Feset.singleton _ a;
             sort_krel := sort_krel (eval_query_prov_N (create_env env) q) |}) h) f0.
Proof.
  intros.
  apply map_eq.
  intros.
  rewrite interp_aggterm_equiv.
  apply interp_aggtermp_eq.
  now apply equiv_envp_env_g_gp.
Qed.


Lemma mem_partition : forall e1 l1 a c f0,
    In a (partition_rec (Oset.mk_list (OVal T)) f0 c l1) ->
    Oeset.mem_bool (OTuple T) e1 l1 = true ->
    f0 e1 = fst a ->
    all_diff (map fst c) ->
    (forall t1 t2 : tupleT, Oeset.eq_bool (OTuple T) t1 t2 = true -> f0 t1 = f0 t2) ->
    Oeset.mem_bool (OTuple T) e1 (snd a) = true.
Proof.
  induction l1; intros.
  inversion H0.
  case_eq (Oeset.mem_bool (OTuple T) e1 l1); intros.
  - apply (IHl1 _ _ _ H); trivial.
    now apply insert_in_partition_all_diff_values.
  - simpl in H0.
    simpl in H.
    rewrite H4 in H0.
    rewrite orb_false_r in H0.
    destruct (insert_rel_in_3 f0 c a).
    apply (partition_partition_f_7 _ _ _ x e1 _) in H; trivial.
    destruct (insert_in_partition_cases (Oset.mk_list (OVal T)) (f0 a) a c) as [[l2 [l3 [l4 [H6 H7]]]]|H6].
    rewrite H7 in H5.
    rewrite H6 in H2.
    apply in_app_or in H5.
    destruct H5 as [H5|[H5|H5]].
    rewrite map_app in H2.
    apply in_fst in H5.
    apply (all_diff_app _ _ H2) in H5.
    destruct H5.
    rewrite map_cons.
    now left.
    inversion H5.
    rewrite <- H9 in H.
    apply Oeset.eq_bool_true_compare_eq in H0.
    rewrite !(Oeset.nb_occ_eq_1 _ _ _ _ H0) in H.
    rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H0).
    simpl in H.
    rewrite Oeset.compare_eq_refl in H.
    apply Oeset.nb_occ_mem.
    rewrite H.
    apply Nadd_1_diff_0.
    rewrite map_app in H2.
    rewrite map_cons in H2.
    rewrite cons_app_nil in H2.
    rewrite app_assoc in H2.
    apply in_fst in H5.
    apply (all_diff_app _ _ H2) in H5.
    destruct H5.
    apply in_or_app.
    right.
    now left.
    rewrite H6 in H5.
    apply in_app_or in H5.
    destruct H5 as [H5|[H5|H5]].
    apply (insert_in_partition_all_diff_values (Oset.mk_list (OVal T)) (f0 a) a c) in H2.
    rewrite H6 in H2.
    rewrite map_app in H2.
    apply in_fst in H5.
    apply (all_diff_app _ _ H2) in H5.
    destruct H5.
    now left.
    inversion H5.
    rewrite <- H8 in H.
    apply Oeset.eq_bool_true_compare_eq in H0.
    rewrite !(Oeset.nb_occ_eq_1 _ _ _ _ H0) in H.
    rewrite (Oeset.mem_bool_eq_1 _ _ _ _ H0).
    simpl in H.
    rewrite Oeset.compare_eq_refl in H.
    apply Oeset.nb_occ_mem.
    rewrite H.
    apply Nadd_1_diff_0.
    inversion H5.
    now apply insert_in_partition_all_diff_values.
    rewrite <- H1.
    assert (f0 e1 = f0 a).
    now apply H3.
    now rewrite H6.
Qed.
  

Lemma homogeneous_values_partition_rec_krel : forall l f0 k b,
    (forall v1 l2, In (v1, l2) b -> forall x, x inSE support l2 -> f0 x = v1) ->
    (forall t1 t2, eq (OTuple T) t1 t2 -> f0 t1 = f0 t2) ->
    forall v l1, In (v,l1) (partition_rec_krel 0%N f0 k b l) ->
                 forall x, x inSE support l1 -> f0 x = v.
Proof.
  induction l as [ | a1 l]; intros.
  apply (H _ _ H1 _ H2).
  simpl in H1.
  revert H1 x H2. apply IHl; trivial.
  clear IHl.
  revert b H H0; induction b; intros.
  simpl in H1.
  destruct H1.
  inversion H1.
  rewrite <- H5 in H2.
  simpl in H2.
  rewrite FiniteSetImpl.singleton_spec_ in H2.
  apply H0.
  destruct (t_compare (OTuple T) x a1); intros; trivial; inversion H2.
  inversion H1.
  simpl in H1.
  destruct a.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a1) l0); intros; rewrite H3 in H1; destruct H1 as [H1|H1].
  inversion H1; subst v1 l2.
  simpl in H2.
  rewrite FiniteSetImpl.add_spec_ in H2.
  case_eq (t_compare (OTuple T) x a1); intros; try solve [rewrite H4 in H2; apply (H _ _ (List.in_eq _ _) _ H2)].
  apply H0 in H4.
  rewrite H4.
  now apply Oset.eq_bool_true_iff in H3.
  revert x H2.
  apply H.
  now right.
  revert x H2.
  apply H.
  now left.
  revert H1 x H2.
  apply IHb; trivial.
  intros until 1.
  apply H.
  now right.
Qed.

Lemma homogeneous_values_partition_krel : forall k f0,
    forall v l1, In (v,l1) (partition 0%N TN f0 k) ->
    (forall t1 t2, eq (OTuple T) t1 t2 -> f0 t1 = f0 t2) ->
    forall x, x inSE support l1 -> f0 x = v.
Proof.
  intros.
  revert H x H1.
  apply homogeneous_values_partition_rec_krel; trivial.
  intros.
  inversion H.
Qed.

Lemma partition_eq_krel : forall l k a b c f0,
    In (a, b) (partition_rec_krel 0%N f0 k c l) ->
    (forall d, In (a, d) c -> (forall e1 e' : tupleT,
      e1 inSE support d ->
      eq (OTuple T) e1 e' -> 
      negb (Oeset.eq_bool TN (f d e1) 0%N) = negb (Oeset.eq_bool TN (f d e') 0%N))) ->
    forall e1 e' : tupleT,
      e1 inSE support b ->
      eq (OTuple T) e1 e' ->
      negb (Oeset.eq_bool TN (f b e1) 0%N) = negb (Oeset.eq_bool TN (f b e') 0%N).
Proof.
  induction l; intros until 2.
  simpl in H.
  apply (H0 _ H).
  apply (IHl _ _ _ _ _ H).
  clear IHl H.
  induction c; intros until 1.
  destruct H.
  inversion H. subst a0 d.
  intros.
  unfold f.
  apply Oeset.eq_bool_true_compare_eq in H2.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H2).
  inversion H.
  simpl in H.
  destruct a1.
  case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (f0 a) l0); intro H1; rewrite H1 in H; destruct H as [H|H].
  inversion H; subst a0 d.
  intros.
  unfold f.
  apply Oeset.eq_bool_true_compare_eq in H3.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ H3).
  simpl in H2.
  rewrite FiniteSetImpl.add_spec_ in H2.
  unfold Oeset.eq_bool.
  apply Oeset.eq_bool_true_compare_eq in H3.
  destruct (t_compare (OTuple T) e1 a); intros; trivial; apply (H0 k0 (List.in_eq _ _) _ e') in H2; trivial; unfold f in H2; trivial.
  apply H0.
  now right.
  apply H0.
  now left.
  revert H.
  apply IHc.
  intros until 1.
  apply H0.
  now right.  
Qed.
(***********)
Lemma equiv_part_partition_aux : forall q f0 env,
    (forall t : tupleT,
  Oeset.mem_bool (OTuple T) t
    (Feset.elements FesetT
       (filter_zero_N (eval_query_prov_N (create_env env) q))) =
  Oeset.mem_bool (OTuple T) t
    (Febag.elements BTupleT (eval_query_rel env q))) -> (forall e e' : tupleT,
 e inSE support (eval_query_prov_N (create_env env) q) ->
 eq (OTuple T) e e' ->
 negb
   (Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e)
      0%N) =
 negb
   (Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) e')
                  0%N)) ->
    (forall t1, Oeset.nb_occ (OTuple T) t1 (Febag.elements BTupleT (eval_query_rel env q)) =
                f (eval_query_prov_N (create_env env) q) t1) ->
    equiv_part (partition 0%N TN
            (fun t : tupleT =>
               map
                 (fun h : aggterm =>
                    interp_aggterm_prov 0%N 1%N TN
                      interp_aggregate_prov
                      (env_gp (create_env env) 
                         (Group_By f0)
                         {|
                         f := fun t0 : tupleT =>
                              if Oeset.eq_bool (OTuple T) t0 t
                              then 1%N
                              else 0%N;
                         support := FiniteSetImpl.singleton_
                                      (OTuple T) t;
                         sort_krel := sort_krel
                                        (eval_query_prov_N
                                          (create_env env) q) |})
                      h) f0)
                (eval_query_prov_N (create_env env) q)) (map
             (fun l : list value * listT =>
              (fst l,
              create_krel (snd l)
                match quicksort (OTuple T) (snd l) with
                | nil => emptysetS
                | t :: _ => labels T t
                end))
             (Partition.partition (Oset.mk_list (OVal T))
                (fun t : tupleT =>
                 map
                   (fun f2 : aggterm =>
                    interp_aggterm
                      (env_g T env (Group_By f0) (t :: nil)) f2)
                   f0)
                (Febag.elements BTupleT (eval_query_rel env q)))).
Proof.
  intros q f0 env H H0 Hf.
  set (fst_partition_eq_aux f0 env q H H0).
  apply equiv_part_partition; trivial.
  - apply partition_all_diff_values.
  - intros.
  assert (exists b : Krel N,
    In (fst a, b)
      (partition 0%N TN
         (fun t : tupleT =>
          map
            (fun h : aggterm =>
             interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
               (env_gp (create_env env) (Group_By f0)
                  {|
                  f := fun t0 : tupleT =>
                       if Oeset.eq_bool (OTuple T) t0 t
                       then 1%N
                       else 0%N;
                  support := FiniteSetImpl.singleton_ (OTuple T) t;
                  sort_krel := sort_krel
                                 (eval_query_prov_N
                                    (create_env env) q) |}) h) f0)
         (eval_query_prov_N (create_env env) q))).
    * apply (in_map fst) in H1.
      rewrite e in H1.
      apply in_map_iff in H1.
      destruct H1 as [b [H1 H2]].
      destruct b.
      exists k.
      simpl in H1. now rewrite H1 in H2.
    * destruct H2 as [b H2].
      exists b.
      split; trivial.
      repeat split.
      -- rewrite sort_krel_create_krel. destruct a.
         simpl.
         case_eq (quicksort (OTuple T) l0); intros.
         apply quicksort_nil in H3.
         rewrite H3 in H1.
         apply in_partition_diff_nil in H1.
         now destruct H1.
         apply (in_partition _ _ _ _ _ t) in H1.
         assert (t inSE support (eval_query_prov_N (create_env env) q)).
         apply (Oeset.in_mem_bool (OTuple T)) in H1.
         rewrite <- H in H1.
         rewrite <- Feset.mem_elements in H1.
         unfold filter_zero in H1.
         rewrite Feset.filter_spec in H1; trivial.
         now apply andb_true_iff in H1.
         apply sort_lab_supp in H4.
         rewrite (Fset.equal_eq_2 _ _ _ _ H4).
         revert H2.
         apply partition_krel_sort.
         intros.
         inversion H2.
         apply Krel_inv_eval_N.
         apply (In_quicksort (OTuple T)).
         rewrite H3.
         now left.
      --  apply Feset.equal_true_sym.
          apply (feset_equal_trans _ _ _ _ (support_create_krel_filter_zero _ (match quicksort (OTuple T) (snd a) with | nil => emptysetS | t :: _ => labels T t end))).
          apply Feset.equal_true_sym.
          apply Feset.equal_spec; intros.
          rewrite support_create_krel.
          case_eq (Oeset.mem_bool (OTuple T) e0 (snd a)); intros.
          ** apply Oeset.mem_bool_true_iff in H3 as [a' [H3 H4]].
             destruct a.
             generalize H4; intros.
             apply (in_partition _ _ _ _ _ _ H1) in H4.
             apply (Oeset.in_mem_bool (OTuple T)) in H4.
             rewrite <- H in H4.
             apply (partition_homogeneous_values _ _ _ _ _ H1) in H5.
             rewrite <- (Oeset.mem_bool_eq_1 _ _ _ _ H3) in H4.
             assert (map
                       (fun f2 : aggterm =>
                          interp_aggterm (env_g T env (Group_By f0) (e0 :: nil))
                                         f2) f0 = l).
             rewrite <- H5.
             apply map_eq.
             intros. now apply interp_aggterm_eq_env_g.
             clear H5.
             rewrite (map_interp_aggterm _ _ _ q) in H6; try now rewrite <- Feset.mem_elements in H4.
             apply (mem_partition_prov _ _ _ _ _ H2 H4 H6); try solve [apply Feset.elements_spec3|
             simpl; trivial|intros; inversion H5].
             --- intros.
                 rewrite <- Feset.mem_elements in H5.
                 unfold filter_zero in H5. rewrite Feset.filter_spec in H5.
                 apply andb_true_iff in H5 as [_ H5].
                 now apply negb_true_iff in H5.
                 intros.
                 rewrite <- ! Hf.
                 now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H8).
             --- intros.
                 apply map_eq; intros.
                 apply interp_aggtermp_eq.
                 now apply equiv_envp_env_gp.
             --- intros.
                 rewrite <- ! Hf.
                 now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H5).
          ** apply not_true_iff_false.
             apply not_true_iff_false in H3.
             intro.
             destruct H3.
             unfold filter_zero in H4.
             rewrite Feset.filter_spec in H4.
             apply andb_true_iff in H4 as [H3 H4].
             assert (b = (snd (fst a, b))); trivial.
             rewrite H5 in H3. clear H5.
             generalize H2; intros Hpart.
             apply (partition_in _ _ _ _ _ _ H3) in H2.
             destruct H2 as [H2|[x [H2a H2b]]].
             rewrite H in H2.
             apply (mem_partition _ _ _ _ _ H1); trivial.
             rewrite (map_interp_aggterm _ _ _ q).
             destruct a.
             apply (homogeneous_values_partition_krel _ _ _ _ Hpart); trivial.
             intros.
             apply map_eq; intros.
             apply interp_aggtermp_eq.
             now apply equiv_envp_env_gp.
             rewrite <- H in H2.
             now rewrite <- Feset.mem_elements in H2.
             intros; rewrite <- ! Hf.
             now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H5).
             simpl; trivial.
             intros.
             apply map_eq; intros.
             apply interp_aggterm_eq_env_g.
             now apply Oeset.eq_bool_true_compare_eq.
             inversion H2a.
             apply (partition_eq_krel _ _ _ _ _ _ H2).
             intros.
             inversion H3.
      -- intros.
         rewrite f_create_krel.
         apply Oeset.eq_bool_true_compare_eq in H3.
         rewrite <- (Oeset.nb_occ_eq_1 _ _ _ _ H3).
         clear H3.
         case_eq (t1 inSE? filter_zero_N (eval_query_prov_N (create_env env) q)); intros.
         case_eq (Oset.eq_bool (Oset.mk_list (OVal T)) (map
               (fun h : aggterm =>
                interp_aggterm_prov 0%N 1%N TN
                  interp_aggregate_prov
                  (env_gp (create_env env) 
                     (Group_By f0)
                     {|
                     f := fun t0 : tupleT =>
                          if Oeset.eq_bool (OTuple T) t0 t1
                          then 1%N
                          else 0%N;
                     support := Feset.singleton _ t1;
                     sort_krel := sort_krel
                                    (eval_query_prov_N
                                       (create_env env) q) |}) h)
               f0) (fst (fst a, b))); intros.
         ** rewrite Oset.eq_bool_true_iff in H4.
            pose (partition_f_def _ _ _ _ H2 H4 H3).
            simpl in e0.
            rewrite e0.
            clear e0.
            symmetry.
            rewrite (partition_partition_f_def _ _ _ _ H1); trivial.
            --- simpl in H4.
                rewrite <- H4.
                apply map_interp_aggterm; trivial.
                intros.
                rewrite <- ! Hf.
                now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H5).
            --- rewrite <- H.
                now rewrite <- Feset.mem_elements.
            --- intros.
                apply map_eq.
                intros.
                rewrite ! interp_aggterm_equiv.
                apply interp_aggtermp_eq.
                apply equiv_envp_env_g.
                now apply Oeset.eq_bool_true_compare_eq in H5.
            --- intros.
                rewrite <- ! Hf.
                apply Oeset.eq_bool_true_compare_eq in H5.
                apply (Oeset.nb_occ_eq_1 _ _ _ _ H5).
            --- intros. apply map_eq.
                intros.
                apply interp_aggtermp_eq.
                apply equiv_envp_env_gp.
                now apply Oeset.eq_bool_true_compare_eq in H5.
            --- apply FiniteSetImpl.elements_spec3_.
         ** assert ((map
            (fun h : aggterm =>
             interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
               (env_gp (create_env env) (Group_By f0)
                  {|
                  f := fun t0 : tupleT =>
                       if Oeset.eq_bool (OTuple T) t0 t1
                       then 1%N
                       else 0%N;
                  support := Feset.singleton FesetT t1;
                  sort_krel := sort_krel
                                 (eval_query_prov_N
                                    (create_env env) q) |}) h) f0)
               <> (fst (fst a, b))).
            intro.
            apply not_true_iff_false in H4.
            destruct H4.
            now rewrite Oset.eq_bool_true_iff.
            clear H4.
            pose (partition_f_def_2 _ _ _ _ H2 H5).
            simpl in e0.
            rewrite e0.
            clear e0.
            symmetry.
            apply (partition_partition_f_def_2 _ _ _ _ H1).
            --- intro.
                destruct H5.
                rewrite <- H4.
                apply map_eq.
                intros.
                rewrite interp_aggterm_equiv.
                symmetry.
                apply interp_aggtermp_eq.
                apply equiv_envp_env_g_gp; trivial.
                intros.
                rewrite <- ! Hf.
                now rewrite (Oeset.nb_occ_eq_1 _ _ _ _ H6).
            --- intros.
                apply map_eq.
                intros.
                rewrite ! interp_aggterm_equiv.
                apply interp_aggtermp_eq.
                apply equiv_envp_env_g.
                now apply Oeset.eq_bool_true_compare_eq in H4.
            --- intros.
                apply map_eq.
                intros.
                apply interp_aggtermp_eq.
                apply equiv_envp_env_gp.
                now apply Oeset.eq_bool_true_compare_eq in H4.
         ** pose (partition_f_def_3 _ _ _ _ H2 H3).
            simpl in e0.
            rewrite e0.
            symmetry.
            apply (partition_partition_f_def_3 _ _ _ _ H1).
            rewrite <- H.
            now rewrite <- Feset.mem_elements.
Qed.

Lemma equiv_part_nb_occ_map_partition : forall t f1 f0 a a0 env (e1 e2 : list (list value * Krel N)),
   equiv_part e1 e2 -> 
    Oeset.nb_occ (OTuple T) t
    (map
       (fun x : Krel N =>
        mk_tuple T (a0 addS (emptysetS))
          (fun a1 : attribute =>
           match
             (if Oset.eq_bool (OAtt T) a1 a0
              then Some (A_agg T a f1)
              else None)
           with
           | Some e0 =>
               interp_aggterm_prov 0%N 1%N TN
                 interp_aggregate_prov
                 (env_gp (create_env env) (Group_By f0) x) e0
           | None => dot T (default_tuple T (emptysetS)) a1
           end))
       (map snd e1)) =
      Oeset.nb_occ (OTuple T) t
    (map
       (fun x : Krel N =>
        mk_tuple T (a0 addS (emptysetS))
          (fun a1 : attribute =>
           match
             (if Oset.eq_bool (OAtt T) a1 a0
              then Some (A_agg T a f1)
              else None)
           with
           | Some e0 =>
               interp_aggterm_prov 0%N 1%N TN
                 interp_aggregate_prov
                 (env_gp (create_env env) (Group_By f0) x) e0
           | None => dot T (default_tuple T (emptysetS)) a1
           end))
       (map snd e2)).
Proof.
  intros.
  rewrite 2 map_map.
  revert e2 H; induction e1; intros.
  now inversion H.
  inversion H.
  simpl. rewrite (IHe1 l'); trivial.
  f_equal.
  assert (Oeset.compare (OTuple T) (mk_tuple T (a0 addS (emptysetS))
         (fun a2 : attribute =>
          match
            (if Oset.eq_bool (OAtt T) a2 a0
             then Some (A_agg T a f1)
             else None)
          with
          | Some e0 =>
              interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
                (env_gp (create_env env) (Group_By f0) (snd a1))
                e0
          | None => dot T (default_tuple T (emptysetS)) a2
          end))
      (mk_tuple T (a0 addS (emptysetS))
         (fun a2 : attribute =>
          match
            (if Oset.eq_bool (OAtt T) a2 a0
             then Some (A_agg T a f1)
             else None)
          with
          | Some e0 =>
              interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
                (env_gp (create_env env) (Group_By f0) (snd y)) e0
          | None => dot T (default_tuple T (emptysetS)) a2
          end)) = Eq).
  apply mk_tuple_eq.
  apply Fset.equal_refl.
  intros.
  destruct (Oset.eq_bool (OAtt T) a2 a0); trivial.
  apply interp_aggtermp_eq.
  unfold env_gp.
  apply Forall2_cons.
  destruct H2 as [H2a [H2b H2c]].
  repeat split; trivial.
  apply equiv_env_eq_create_env.
  case_eq (t_compare (OTuple T) t
      (mk_tuple T (a0 addS (emptysetS))
         (fun a2 : attribute =>
          match
            (if Oset.eq_bool (OAtt T) a2 a0
             then Some (A_agg T a f1)
             else None)
          with
          | Some e0 =>
              interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov
                (env_gp (create_env env) (Group_By f0) (snd a1)) e0
          | None => dot T (default_tuple T (emptysetS)) a2
          end))); intros.  
  now rewrite (Oeset.compare_eq_trans _ _ _ _ H6 H5).
  now rewrite (Oeset.compare_lt_eq_trans _ _ _ _ H6 H5).
  now rewrite (Oeset.compare_gt_eq_trans _ _ _ _ H6 H5).
Qed.


Lemma eval_query_prov_k_n : forall n,
    (forall q b env, well_formed env b q = true ->
                   (tree_size (tree_of_query q)) <= n  -> 
                   forall e,
                     f (eval_query_prov_N (create_env env) q) e = 
                     Febag.nb_occ (BTupleT) e (eval_query_rel env q)) /\

    (forall form env,
        well_formed_form env form = true ->
        tree_size (tree_of_sql_formula (fun t => tree_of_query t)
                                       form) <= n -> (eval_sql_formula eval_query_rel env form) =
                                                     (eval_formula_prov_N (create_env env) form)).
Proof.
  intro n; induction n as [ | n]; repeat split.
  - intros q b env Hwf Hn; destruct q; inversion Hn.
  - intros f env Hwf Hn; destruct f; inversion Hn; try solve [destruct q; inversion Hn].
  - intros q b env Hwf Hn e; destruct q as [| r | o q1 q2 | q1 q2 | s q | f q | g f0 s q].
    * rewrite eval_query_unfold; rewrite Febag.nb_occ_singleton; trivial.
    * rewrite eval_query_unfold; rewrite eval_query_prov_k_unfold.
        apply instance_prov_nb_occ.
    * case_eq (sort q1 =S?= sort q2); intro Hsort.
      -- revert Hwf. case o; try solve[intros; simpl in Hwf; rewrite Hsort in Hwf; inversion Hwf]. 
         intros. rewrite well_formed_unfold in Hwf. rewrite Hsort in Hwf.
         apply andb_true_iff in Hwf as [Hwf1 Hwf2].
         rewrite tree_of_query_unfold in Hn; simpl in Hn. 
         apply le_S_n in Hn.
         apply le_plus_n in Hn as [Hn1 Hn2].
         rewrite <- plus_n_O in Hn2.
         rewrite eval_query_unfold. rewrite eval_query_prov_k_unfold.
         rewrite Hsort. simpl.
         rewrite (Fset_equal_bool_sort (create_env env)) in Hsort; rewrite Hsort.
         rewrite Febag.nb_occ_union.
         repeat rewrite ((proj1 IHn) _ false); trivial.
      -- rewrite eval_query_unfold.
         rewrite eval_query_prov_k_unfold.
         rewrite Hsort. simpl. rewrite (Fset_equal_bool_sort (create_env env)) in Hsort; rewrite Hsort.
         rewrite Febag.nb_occ_empty; trivial.
    * rewrite well_formed_unfold in Hwf.
      apply andb_true_iff in Hwf as [Hwf1 Hwf2].
      rewrite tree_of_query_unfold in Hn; simpl in Hn.
      apply Le.le_S_n in Hn.
      apply le_plus_n in Hn as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      apply nb_occ_natural_join; trivial; apply ((proj1 IHn) _ false); trivial.
    * rewrite well_formed_unfold in Hwf.
      rewrite tree_of_query_unfold in Hn; simpl in Hn.
      apply Le.le_S_n in Hn.
      apply le_plus_n in Hn as [_ Hn]. rewrite <- plus_n_O in Hn.
      rewrite eval_query_unfold. rewrite eval_query_prov_k_unfold.
      destruct s.
      apply andb_true_iff in Hwf as [Hwf1 Hwf2].
      apply andb_true_iff in Hwf1 as [Hwf1 _].
      destruct (is_agregate l); try solve [inversion Hwf2].
      simpl.
      unfold f_pi.
        unfold Febag.map.
        rewrite Febag.nb_occ_mk_bag.
        rewrite (fold_left_eq _ (fun (x : N) (y : tupleT) => Oeset.nb_occ (OTuple T) y (Febag.elements BTupleT (eval_query_rel env q)) + x)%N _ eq_refl).
        rewrite fold_left_filter_add.
      ** assert (forall t, Febag.mem _ t (eval_query_rel env q) = true -> Feset.mem _ t (support (eval_query_prov_N (create_env env) q)) = true).
         --- apply (support_mem_equiv_eval_query q env).
         intros; apply (proj1 IHn _ false); trivial.
         --- assert (forall t, Febag.mem _ t (eval_query_rel env q) = false -> f (eval_query_prov_N (create_env env) q)t = 0%N).
            *** apply (support_not_mem_equiv_eval_query q env).
         intros; apply (proj1 IHn _ false); trivial.
            *** rewrite fold_left_nb_occ.
                ---- apply Oeset.nb_occ_map_eq_2_alt.
                    intros. rewrite create_env_prop_1. apply projection_equiv.
                ---- intros.
                apply Oeset.eq_bool_true_compare_eq.
                apply Oeset.eq_bool_true_compare_eq in H1.
                apply projection_prov_eq.
                apply equiv_env_eq_tuple.
                now apply Oeset.eq_bool_true_compare_eq.
                ----               
                intros. apply Oeset.mem_nb_occ in H1. rewrite <- Febag.nb_occ_elements in H1.
                rewrite <- ((proj1 IHn) _ false) in H1; trivial.
                rewrite <- Feset.mem_elements.
      apply (not_zero_mem_support _ (Krel_inv_eval_N (create_env env) q) H1).
      ** intros.
         rewrite ((proj1 IHn) _ false); trivial.
         rewrite Febag.nb_occ_elements; trivial.
    * rewrite well_formed_unfold in Hwf.
      apply andb_true_iff in Hwf as [Hwf1 Hwf2].
      rewrite tree_of_query_unfold in Hn; simpl in Hn.
      apply Le.le_S_n in Hn. apply le_plus_n in Hn as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      rewrite eval_query_unfold. rewrite eval_query_prov_k_unfold.
      simpl. rewrite create_env_prop_1.
      rewrite Febag.nb_occ_filter.
      rewrite ((proj1 IHn) _ false); trivial.
      rewrite N.mul_comm.
      f_equal.
      rewrite (proj2 IHn); trivial.
      rewrite <- (app_nil (env_t T env e)).
      unfold env_t.
      apply (proj2 well_formed_env_t); simpl; trivial.
      intros. apply is_true_eval_f_eq; [>apply contains_nulls_eq|trivial].
    * rewrite tree_of_query_unfold in Hn; simpl in Hn. 
      apply le_S_n in Hn.
      apply le_plus_n in Hn as [Hn1 Hn2].
      apply le_plus_n in Hn2 as [_ Hn2].
      apply le_plus_n in Hn2 as [Hn2 Hn3].
      destruct b; simpl in Hwf; destruct g; try solve [inversion Hwf].
      apply andb_true_iff in Hwf as [Hwf1 Hwf5].
      apply andb_true_iff in Hwf1 as [Hwf1 Hwf4].
      apply andb_true_iff in Hwf1 as [Hwf1 Hwf3].
      apply andb_true_iff in Hwf1 as [Hwf1 Hwf2].
      destruct l; try solve [inversion Hwf5].
      destruct l; try solve [destruct s0; destruct a; inversion Hwf5].
      destruct s0; try solve [inversion Hwf5].
      destruct a; try solve [inversion Hwf5].
      destruct s; try solve [inversion Hwf5].
      rewrite eval_query_unfold.
      rewrite eval_query_prov_k_unfold.
      unfold Krel_gamma.
      unfold f.
      rewrite Febag.nb_occ_mk_bag.
      unfold calcul_proj_grb.
      simpl (fun l : listT =>
           Bool.is_true BT
             (eval_sql_formula eval_query_rel
                (env_g T env (Group_By f0) l) 
                (Sql_True T query))).
      rewrite Bool.true_is_true_alt.
      rewrite filter_true; trivial.
      unfold f_gamma.
      rewrite f_gamma_nb_occ.
      -- rewrite map_map.
      unfold fst.
      assert ( Oeset.nb_occ (OTuple T) e
    (map
       (fun l : listT =>
        projection T (env_g T env (Group_By f0) l)
          (Select_List
             (_Select_List (Select_As (A_agg T a f1) a0 :: nil))))
       (FlatData.make_groups T env
          (Febag.elements BTupleT (eval_query_rel env q))
          (Group_By f0)))= Oeset.nb_occ (OTuple T) e
    (map
       (fun l : listT =>
        projection_prov  0%N 1%N TN interp_aggregate_prov
          (create_env (env_g T env (Group_By f0) l))
          (Select_List
             (_Select_List (Select_As (A_agg T a f1) a0 :: nil))))
       (FlatData.make_groups T env
          (Febag.elements BTupleT (eval_query_rel env q))
          (Group_By f0)))).
      apply Oeset.nb_occ_eq_2.
      apply Oeset.comparelA_map_eq_1.
      intros. apply Oeset.compare_eq_sym.
      apply projection_equiv.
      rewrite H; clear H.
      assert (  Oeset.nb_occ (OTuple T) e
    (map
       (fun l : listT =>
        projection_prov 0%N 1%N TN interp_aggregate_prov
          (create_env (env_g T env (Group_By f0) l))
          (Select_List
             (_Select_List (Select_As (A_agg T a f1) a0 :: nil))))
       (FlatData.make_groups T env
          (Febag.elements BTupleT (eval_query_rel env q))
          (Group_By f0)))
                =
                  Oeset.nb_occ (OTuple T) e
    (map
       (fun x : Krel N =>
        projection_prov 0%N 1%N TN interp_aggregate_prov
          (env_gp (create_env env) (Group_By f0) x)
          (Select_List
             (_Select_List (Select_As (A_agg T a f1) a0 :: nil))))
       (map (fun l => create_krel l
             match quicksort (OTuple T) l with
             | nil => emptysetS
             | t :: _ => labels T t
             end) (FlatData.make_groups T env
          (Febag.elements BTupleT (eval_query_rel env q))
          (Group_By f0))))).
      
      simpl (create_env _).
      now rewrite map_map.
      rewrite H. clear H.
      simpl.
      assert (map
          (fun l : listT =>
           create_krel l
             match quicksort (OTuple T) l with
             | nil => emptysetS
             | t :: _ => labels T t
             end)
          (map snd
             (Partition.partition (Oset.mk_list (OVal T))
                (fun t : tupleT =>
                 map
                   (fun f2 : aggterm =>
                    interp_aggterm
                      (env_g T env (Group_By f0) (t :: nil)) f2)
                   f0)
                (Febag.elements BTupleT (eval_query_rel env q))))
              = map snd (map
          (fun l => (fst l,
           create_krel (snd l)
             match quicksort (OTuple T) (snd l) with
             | nil => emptysetS
             | t :: _ => labels T t
             end))
          
             (Partition.partition (Oset.mk_list (OVal T))
                (fun t : tupleT =>
                 map
                   (fun f2 : aggterm =>
                    interp_aggterm
                      (env_g T env (Group_By f0) (t :: nil)) f2)
                   f0)
                (Febag.elements BTupleT (eval_query_rel env q))))).
      repeat rewrite map_map; trivial.
      rewrite H; clear H.
      apply equiv_part_nb_occ_map_partition.
      apply equiv_part_partition_aux.
      intros.
      rewrite <- Feset.mem_elements.
      unfold filter_zero.
      rewrite Feset.filter_spec.
      case_eq (Oeset.mem_bool (OTuple T) t (Febag.elements BTupleT (eval_query_rel env q))); intros.
      apply Oeset.mem_nb_occ in H.
      rewrite <- Febag.nb_occ_elements in H.
      rewrite <- (proj1 IHn _ false) in H; trivial.
      rewrite (not_zero_mem_support _ (Krel_inv_eval_N (create_env env) q)); trivial.
      apply eq_true_not_negb.
      intro. destruct H.
      apply Oeset.eq_bool_true_compare_eq in H0.
      now apply N.compare_eq_iff.
      apply Oeset.not_mem_nb_occ in H.
      rewrite <- Febag.nb_occ_elements in H.
      rewrite <- (proj1 IHn _ false) in H; trivial.
      rewrite H.
      apply andb_false_r.
      intros.
      rewrite ! (proj1 IHn _ false); trivial.
      now rewrite (Febag.nb_occ_eq_1 _ _ _ _ H0).
      intros.
      rewrite ! (proj1 IHn _ false); trivial.
      now rewrite (Febag.nb_occ_eq_1 _ _ _ _ H0).
      intros.
      rewrite <- Febag.nb_occ_elements.
      now rewrite ((proj1 IHn) _ false).
      -- intros.
      apply delta_it_f_1.
      apply in_map_iff in H as [x0 [H H0]].
      simpl in H0.
      rewrite <- H.
      unfold snd.
      apply in_map_iff in H0 as [x1 [H0 H1]].
      rewrite <- H0.
      unfold partition in H1.
      destruct x1.
      apply partition_in_l in H1 as [H1|H1]; trivial.
      inversion H1.
      intros.
      apply (Oeset.in_mem_bool (OTuple T)) in H2.
      rewrite <- Feset.mem_elements in H2.
      unfold filter_zero in H2.
      rewrite Feset.filter_spec in H2.
      apply andb_true_iff in H2 as [_ H2].
      intro.
      rewrite H3 in H2.
      rewrite Oeset.eq_bool_refl in H2.
      inversion H2.
      intros.
      rewrite ! (proj1 IHn _ false); trivial.
      now rewrite (Febag.nb_occ_eq_1 _ _ _ _ H4).
    - intros f env Hwf Hn; destruct f; trivial.
    * rewrite well_formed_form_unfold in Hwf.
      apply andb_true_iff in Hwf as [Hwf1 Hwf2].
      rewrite tree_of_sql_formula_unfold in Hn; simpl in Hn.
      apply le_S_n in Hn.
      apply le_plus_n in Hn as [Hn1 Hn2].
      rewrite <- plus_n_O in Hn2.
      rewrite eval_sql_formula_unfold. rewrite eval_formula_prov_unfold.
      revert Hn1; revert Hn2; case a; intros; repeat (rewrite (proj2 IHn)); trivial.
    * rewrite well_formed_form_unfold in Hwf.
      rewrite tree_of_sql_formula_unfold in Hn; simpl in Hn.
      apply le_S_n in Hn.
      rewrite <- plus_n_O in Hn.
      rewrite eval_sql_formula_unfold. rewrite eval_formula_prov_unfold.
      rewrite (proj2 IHn); trivial.
    * simpl. clear Hwf. clear Hn.
      f_equal.
      apply map_eq.
      intros.
      apply interp_aggterm_equiv.
     * induction q; rewrite tree_of_sql_formula_unfold in Hn; rewrite well_formed_form_unfold in Hwf; rewrite eval_sql_formula_unfold; rewrite eval_formula_prov_unfold.
      simpl in Hn; trivial.
      -- apply Le.le_S_n in Hn;
         repeat apply Le.le_Sn_le in Hn;
         apply le_plus_n in Hn as [Hn _];
         apply le_plus_n in Hn as [Hn1 Hn2].
         apply andb_prop in Hwf as [Hwf1 Hwf2].
         simpl.
         assert (Bool.forallb BT
    (fun x : tupleT =>
     interp_predicate T p
       (map (interp_aggterm env) l ++
        map (dot T x) ({{{labels T x}}})))
    (Febag.elements BTupleT (eval_query_rel env q0)) =
                 Bool.forallb BT
    (fun x : tupleT =>
     interp_predicate T p
       (map (interp_aggterm env) l ++
        map (dot T x) ({{{labels T x}}})))
         (Feset.elements FesetT (filter_zero 0%N TN (eval_query_prov_N (create_env env) q0)))).
         apply (Bool.forallb_forall_eq _ (OTuple T)).
         ** intros. do 2 apply f_equal. apply tuple_eq in H as [H1 H2].
         rewrite <- (map_eq_s_elements _ _ _ H1).
         assert (forall a : attribute, In a ({{{labels T x1}}}) -> dot T x1 a = dot T x2 a).
         intros. apply H2. apply (Fset.in_elements_mem _ _ _ H).
         induction ({{{labels T x1}}}); intros; trivial. simpl.
         rewrite IHl0. rewrite H; trivial. left; trivial.
         intros. apply H. right; trivial.
         ** intros.
            unfold filter_zero.
            rewrite <- Feset.mem_elements.
            rewrite Feset.filter_spec.
            case_eq (Oeset.mem_bool (OTuple T) x (Febag.elements BTupleT (eval_query_rel env q0))); intros.
            --- apply Oeset.mem_nb_occ in H.
                rewrite <- Febag.nb_occ_elements in H.
                rewrite <- ((proj1 IHn) _ false) in H; trivial.
                generalize H; intros.
                apply eq_bool_false_N in H.
                rewrite H.
                apply (not_zero_mem_support _ (Krel_inv_eval_N _ q0)) in H0.
                rewrite <- H0. now rewrite andb_true_r.
            --- apply Oeset.not_mem_nb_occ in H.
                rewrite <- Febag.nb_occ_elements in H.
                rewrite <- ((proj1 IHn) _ false) in H; trivial.                
                rewrite H. rewrite Oeset.eq_bool_refl; trivial.
                rewrite andb_false_r. trivial.
            --- intros. repeat rewrite ((proj1 IHn) _ false); trivial.
                now rewrite (Febag.nb_occ_eq_1 _ _ _ _ H0).
         ** rewrite H; simpl.
            induction (FiniteSetImpl.elements_
                         (filter_zero_N (eval_query_prov_N (create_env env) q0))); trivial.
            simpl.
            rewrite IHl0.
            do 3 f_equal.
            clear IHl0. clear H.
            clear Hwf2.
            clear Hn2.
            apply map_eq.
            intros.
            apply interp_aggterm_equiv.
      -- simpl in Hn.
         apply Le.le_S_n in Hn.
         do 2 apply le_Sn_le in Hn.
         do 2 apply le_plus_n in Hn as [Hn _].
         apply andb_prop in Hwf as [Hwf1 Hwf2].
         simpl.
         rewrite 2 Bool.existsb_not_forallb_not.
         f_equal.
         assert (Bool.forallb BT (fun x : tupleT =>
     Bool.negb BT
       (interp_predicate T p
          (map (interp_aggterm env) l ++
           map (dot T x) ({{{labels T x}}}))))
    (Febag.elements BTupleT (eval_query_rel env q0)) =
                 Bool.forallb BT(fun x : tupleT =>
     Bool.negb BT
       (interp_predicate T p
          (map (interp_aggterm env) l ++
           map (dot T x) ({{{labels T x}}}))))
    (Feset.elements FesetT (filter_zero 0%N TN (eval_query_prov_N (create_env env) q0)))).
         apply (Bool.forallb_forall_eq _ (OTuple T)).
** intros. do 3 apply f_equal. apply tuple_eq in H as [H1 H2].
         rewrite <- (map_eq_s_elements _ _ _ H1).
         assert (forall a : attribute, In a ({{{labels T x1}}}) -> dot T x1 a = dot T x2 a).
         intros. apply H2. apply (Fset.in_elements_mem _ _ _ H).
         induction ({{{labels T x1}}}); intros; trivial. simpl.
         rewrite IHl0. rewrite H; trivial. left; trivial.
         intros. apply H. right; trivial.
         ** intros.
            unfold filter_zero.
            rewrite <- Feset.mem_elements.
            rewrite Feset.filter_spec.
         case_eq (Oeset.mem_bool (OTuple T) x (Febag.elements BTupleT (eval_query_rel env q0))); intros.
            --- apply Oeset.mem_nb_occ in H.
                rewrite <- Febag.nb_occ_elements in H.
                rewrite <- ((proj1 IHn) _ false) in H; trivial.
                generalize H; intros.
                apply eq_bool_false_N in H.
                rewrite H.
                apply (not_zero_mem_support _ (Krel_inv_eval_N _ q0)) in H0.
                rewrite <- H0. now rewrite andb_true_r.
            --- apply Oeset.not_mem_nb_occ in H.
                rewrite <- Febag.nb_occ_elements in H.
                rewrite <- ((proj1 IHn) _ false) in H; trivial.                
                rewrite H. rewrite Oeset.eq_bool_refl; trivial.
                rewrite andb_false_r. trivial.
            --- intros. repeat rewrite ((proj1 IHn) _ false); trivial.
                now rewrite (Febag.nb_occ_eq_1 _ _ _ _ H0).
         ** rewrite H; simpl.
            induction (FiniteSetImpl.elements_
                         (filter_zero_N (eval_query_prov_N (create_env env) q0))); trivial.
            simpl.
            rewrite IHl0.
            do 3 f_equal.
            clear IHl0. clear H.
            clear Hwf2.
            induction l; trivial.
            simpl. rewrite IHl.
            now rewrite interp_aggterm_equiv.         
    * simpl in Hn.
      apply Le.le_S_n in Hn.
      do 2 apply le_Sn_le in Hn.
      apply le_plus_n in Hn as [Hn _].
      apply le_plus_n in Hn as [_ Hn].
      apply le_plus_n in Hn as [Hn _].
      apply andb_prop in Hwf as [Hwf1 Hwf2].
      simpl.
      rewrite 2 Bool.existsb_not_forallb_not.
      f_equal.
      assert (Bool.forallb BT
    (fun x : tupleT =>
     Bool.negb BT
       match
         t_compare (OTuple T)
           (mk_tuple T
              (Fset.mk_set (A T)
                 (map (fun x0 : select T => match x0 with
                                            | Select_As _ a => a
                                            end) l))
              (fun a : attribute =>
               match Oset.find (OAtt T) a (map (pair_of_select T) l) with
               | Some e => interp_aggterm env e
               | None => dot T (default_tuple T (emptysetS)) a
               end)) x
       with
       | Eq =>
           if
            contains_nulls
              (mk_tuple T
                 (Fset.mk_set (A T)
                    (map (fun x0 : select T => match x0 with
                                               | Select_As _ a => a
                                               end) l))
                 (fun a : attribute =>
                  match Oset.find (OAtt T) a (map (pair_of_select T) l) with
                  | Some e => interp_aggterm env e
                  | None => dot T (default_tuple T (emptysetS)) a
                  end))
           then unknown
           else Bool.true BT
       | _ =>
           if
            contains_nulls
              (mk_tuple T
                 (Fset.mk_set (A T)
                    (map (fun x0 : select T => match x0 with
                                               | Select_As _ a => a
                                               end) l))
                 (fun a : attribute =>
                  match Oset.find (OAtt T) a (map (pair_of_select T) l) with
                  | Some e => interp_aggterm env e
                  | None => dot T (default_tuple T (emptysetS)) a
                  end)) || contains_nulls x
           then unknown
           else Bool.false BT
       end) (Febag.elements BTupleT (eval_query_rel env q)) =
  Bool.forallb BT
    (fun x : tupleT =>
     Bool.negb BT
       match
         t_compare (OTuple T)
           (mk_tuple T
              (Fset.mk_set (A T)
                 (map (fun x0 : select T => match x0 with
                                            | Select_As _ a => a
                                            end) l))
              (fun a : attribute =>
               match Oset.find (OAtt T) a (map (pair_of_select T) l) with
               | Some e =>
                   interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov (create_env env) e
               | None => dot T (default_tuple T (emptysetS)) a
               end)) x
       with
       | Eq =>
           if
            contains_nulls
              (mk_tuple T
                 (Fset.mk_set (A T)
                    (map (fun x0 : select T => match x0 with
                                               | Select_As _ a => a
                                               end) l))
                 (fun a : attribute =>
                  match Oset.find (OAtt T) a (map (pair_of_select T) l) with
                  | Some e =>
                      interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov (create_env env) e
                  | None => dot T (default_tuple T (emptysetS)) a
                  end))
           then unknown
           else Bool.true BT
       | _ =>
           if
            contains_nulls
              (mk_tuple T
                 (Fset.mk_set (A T)
                    (map (fun x0 : select T => match x0 with
                                               | Select_As _ a => a
                                               end) l))
                 (fun a : attribute =>
                  match Oset.find (OAtt T) a (map (pair_of_select T) l) with
                  | Some e =>
                      interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov (create_env env) e
                  | None => dot T (default_tuple T (emptysetS)) a
                  end)) || contains_nulls x
           then unknown
           else Bool.false BT
       end) (Febag.elements BTupleT (eval_query_rel env q))).
      apply (Bool.forallb_eq _ (OTuple T)).
      apply _permut_refl.
      intros.
      apply Oeset.compare_eq_refl.
      intros.
      intros.
      rewrite (contains_nulls_eq _ _ H0).
      apply f_equal.
      rewrite (Oeset.compare_eq_2 _ _ _ _ H0).
      assert (forall a : attribute,
      a
      inS Fset.mk_set (A T)
            (map (fun x0 : select T => match x0 with
                                       | Select_As _ a1 => a1
                                       end) l) ->
      (fun a0 : attribute =>
       match Oset.find (OAtt T) a0 (map (pair_of_select T) l) with
       | Some e0 => interp_aggterm env e0
       | None => dot T (default_tuple T (emptysetS)) a0
       end) a =
      (fun a0 : attribute =>
       match Oset.find (OAtt T) a0 (map (pair_of_select T) l) with
       | Some e0 => interp_aggterm_prov 0%N 1%N TN interp_aggregate_prov (create_env env) e0
       | None => dot T (default_tuple T (emptysetS)) a0
       end) a); intros.
      destruct (Oset.find (OAtt T) a (map (pair_of_select T) l)); trivial.
      apply interp_aggterm_equiv.
      apply mk_tuple_eq_2 in H1.
      rewrite (Oeset.compare_eq_1 _ _ _ _ H1).
      now rewrite (contains_nulls_eq _ _ H1).
      rewrite H. clear H.
      apply (Bool.forallb_forall_eq _ (OTuple T)).
      ** intros.
         rewrite (contains_nulls_eq _ _ H).
         apply f_equal.
         now rewrite (Oeset.compare_eq_2 _ _ _ _ H).
      ** intros.
         unfold filter_zero.
         simpl.
         rewrite <- FiniteSetImpl.elements_spec2_.
         rewrite FiniteSetImpl.filter_spec_.
         case_eq (Oeset.mem_bool (OTuple T) x (Febag.elements BTupleT (eval_query_rel env q))); intros.
            --- apply Oeset.mem_nb_occ in H.
                rewrite <- Febag.nb_occ_elements in H.
                rewrite <- ((proj1 IHn) _ false) in H; trivial.
                generalize H; intros.
                apply eq_bool_false_N in H.
                rewrite H.
                apply (not_zero_mem_support _ (Krel_inv_eval_N _ q)) in H0.
                rewrite <- H0. simpl.
                assert (forall a : bool, (if a then true else false) = a).
                destruct a; trivial.
                rewrite H1; trivial.
            --- apply Oeset.not_mem_nb_occ in H.
                rewrite <- Febag.nb_occ_elements in H.
                rewrite <- ((proj1 IHn) _ false) in H; trivial.                
                rewrite H. rewrite Oeset.eq_bool_refl; trivial.
                destruct (FiniteSetImpl.mem_ x (support (eval_query_prov_N (create_env env) q))); trivial.
            --- intros. repeat rewrite ((proj1 IHn) _ false); trivial.
                now rewrite (Febag.nb_occ_eq_1 _ _ _ _ H0).
    * simpl in Hn.
      apply Le.le_S_n in Hn.
      apply le_Sn_le in Hn.
      apply le_plus_n in Hn as [Hn _].
      apply le_plus_n in Hn as [Hn _].
      rewrite eval_sql_formula_unfold. rewrite eval_formula_prov_unfold.
      case_eq ( Feset.exists_ FesetT (fun t : tupleT => negb (Oeset.eq_bool TN (f (eval_query_prov_N (create_env env) q) t) 0%N)) (support (eval_query_prov_N (create_env env) q))); intros.
      -- rewrite Feset.exists_spec in H.
         apply existsb_exists in H as [t [_ H]].
         rewrite ((proj1 IHn) _ false) in H; trivial.
         rewrite Febag.is_empty_spec.
         apply negb_true_iff in H.
         apply eq_bool_false_N in H.
         apply if_eq_false.
         apply not_true_iff_false; intro.
         destruct H.
         rewrite (Febag.nb_occ_eq_2 _ _ _ H0).
         apply Febag.nb_occ_empty.
      -- rewrite Feset.exists_spec in H.
          rewrite <- (if_same (Febag.is_empty BTupleT (eval_query_rel env q))).
          apply if_eq; intros; trivial.
          apply not_empty_exists in H0 as [t H0].
          case_eq (t inSE? (support (eval_query_prov_N (create_env env) q))); intros.
          rewrite Feset.mem_elements in H1.
          apply Oeset.mem_bool_true_iff in H1 as [t' [H1 H2]].
          rewrite (Febag.nb_occ_eq_1 _ _ _ _ H1) in H0.
          rewrite <- ((proj1 IHn) _ false) in H0; trivial.
          apply not_true_iff_false in H.
          destruct H.
          apply existsb_exists.
          exists t'. split; trivial.
          apply negb_true_iff.
          apply eq_bool_false_N; trivial.
          apply finite_supp in H1.
          rewrite <- ((proj1 IHn) _ false) in H0; trivial.
          apply N.compare_eq in H1.
          rewrite H1 in H0.
          destruct H0; reflexivity.
          apply Krel_inv_eval_N.
Qed.

Theorem K_relations_extend_relational_algebra :
  forall env (q:query) b,
    well_formed env b q = true ->
    forall (t:tuple),
      f (eval_query_prov_N (create_env env) q) t = 
                     Febag.nb_occ (BTupleT) t (eval_query_rel env q).
Proof.
  intros.
  apply (proj1 (eval_query_prov_k_n (tree_size (tree_of_query q))) q b); trivial.
Qed.



End KAlgebra.
