(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Relations.

Require Import Bool NArith.

Require Import FlatData OrderedSet.
Require Import Bool3.


Notation t_compare := (Oeset.compare).
Notation eq TA A B := (((t_compare TA) A B) = Eq).
Notation lt TA A B := (((t_compare TA) A B) = Lt).
Notation gt TA A B := (((t_compare TA) A B) = Gt).
Notation total_order := (Oeset.Rcd).

Record CM (M : Type) (zero : M) (plus : M -> M -> M) (TM : total_order M) : Type :=
  mk_CM
    {
      plus_assocCM : forall a1 a2 a3, eq TM (plus a1 (plus a2 a3)) (plus (plus a1 a2) a3);
      plus_zero_leftCM : forall a, eq TM (plus zero a) a;
      plus_commCM : forall a1 a2, eq TM (plus a1 a2) (plus a2 a1);
      plus_compat_eq_CM :  forall a1 a2 b1 b2, eq TM a1 a2-> eq TM b1 b2 -> eq TM (plus a1 b1) (plus a2 b2);
    }.

Record CSR (R : Type) (zero : R) (one : R) (plus : R -> R -> R) (mul : R -> R -> R) (TR : total_order R) : Type :=
  mk_SC
    {
      one_zero_diff_SR : not (eq TR zero one);
      isCM_plus_SR : CM zero plus TR;
      isCM_mul_SR : CM one mul TR;
      mul_distr_plus_SR : forall a1 a2 b, eq TR (mul b (plus a1 a2)) (plus (mul b a1) (mul b a2));
      mul_zero_absorb_left_SR : forall a, eq TR (mul a zero) zero
    }.


(** natural integers**)

Definition TN := Oset.oeset_of_oset ON.

Notation eq_n a b := (eq TN a b).
Notation lt_n a b := (lt TN a b).


Ltac compat_eq :=
  intros a1 a2 b1 b2 [ | | ] H1 H2;
  [ >apply N.compare_eq_iff in H1; apply N.compare_eq_iff in H2; apply N.compare_eq_iff; rewrite H1,H2; trivial| | ].



Lemma N_comp_compat_add :
  forall (a1 a2 b1 b2 : N) x,
    N.compare a1 a2 = x ->
    N.compare b1 b2 = x -> N.compare (a1 + b1)%N (a2 + b2)%N = x.
Proof.
  compat_eq.
  - apply N.add_lt_mono; assumption.
  - apply N.gt_lt_iff.
    apply N.add_lt_mono; apply N.gt_lt_iff; assumption.
Qed.


Lemma N_comp_compat_mul :
  forall (a1 a2 b1 b2 : N) x,
    N.compare a1 a2 = x ->
    N.compare b1 b2 = x -> N.compare (a1 * b1)%N (a2 * b2)%N = x.
Proof.
  compat_eq.
  - apply N.mul_lt_mono; assumption.
  - apply N.gt_lt_iff.
    apply N.mul_lt_mono; apply N.gt_lt_iff; assumption.
Qed.

Lemma N_is_CM_add : CM N.zero N.add TN.
Proof.
  split; intros;
    [>rewrite N.add_assoc|rewrite N.add_0_l |rewrite N.add_comm| ];
    try solve[apply Oeset.compare_eq_refl|apply N_comp_compat_add; trivial].
Qed.

Lemma N_is_CM_mul : CM N.one N.mul TN.
Proof.
    split; intros; [>rewrite N.mul_assoc|rewrite N.mul_1_l |rewrite N.mul_comm| ]; try solve[apply Oeset.compare_eq_refl|apply N_comp_compat_mul; trivial].
Qed.


Lemma N_is_CSR : CSR N.zero N.one N.add N.mul TN.
Proof.
  split.
  - discriminate.
  - apply N_is_CM_add.
  - apply N_is_CM_mul.
  - intros a1 a2 b.
    rewrite N.mul_add_distr_l.
    apply Oeset.compare_eq_refl.
  - intros.
    rewrite N.mul_0_r.
    apply Oeset.compare_eq_refl.
Qed.





(*Booleans*)

Definition Tbool := Oset.oeset_of_oset Obool.


Definition bool_2_compare (a1 a2 : bool) :=
  bool_compare a2 a1.

Definition Obool2 : Oset.Rcd bool.
  split with bool_2_compare.
  - intros [ | ] [ | ]; simpl; trivial; discriminate.
  - intros [ | ] [ | ] [ | ]; simpl; trivial; discriminate.
  - intros [ | ] [ | ]; simpl; trivial.
Defined.

Definition Tbool2 := Oset.oeset_of_oset Obool2.


Notation eq_b a b:= (eq Tbool a b).
Notation lt_b a b:= (lt Tbool a b).

Notation eq_b2 a b:= (eq Tbool2 a b).
Notation lt_b2 a b:= (lt Tbool2 a b).


Ltac bool_compat_eq := intros [|] [|] a3 a4 H0 H1; try solve[assumption|apply Oeset.compare_eq_refl|inversion H0].

Ltac CM_andb := split; [> intros; rewrite andb_assoc| | intros; rewrite andb_comm| ]; try solve [apply Oeset.compare_eq_refl|bool_compat_eq].

Ltac CM_orb := split; [> intros; rewrite orb_assoc| | intros; rewrite orb_comm| ]; try solve [apply Oeset.compare_eq_refl|bool_compat_eq].

Ltac CM_eqb := split; [> intros [|] [|] [|]| intros [|] |intros [|] [|]| intros [|] [|] [|] [|] H0 H1]; try solve[trivial | inversion H0| inversion H1].

Ltac CM_xorb := split; [> intros; rewrite xorb_assoc|intros [|]|intros; rewrite xorb_comm|intros [|] [|] [|] [|] H0 H1]; try solve[apply Oeset.compare_eq_refl|trivial|inversion H0|inversion H1].

Lemma boolCM : CM true andb Tbool /\ CM true andb Tbool2.
Proof.
split; try solve [CM_andb].
Qed.

Lemma boolCM2 : CM false orb Tbool /\ CM false orb Tbool2.
Proof.
  split;  try solve [CM_orb].
Qed.

Lemma boolCM3 : CM true eqb Tbool /\ CM true eqb Tbool2.
Proof.
  split;  try solve [CM_eqb].
Qed.

Lemma boolCM4 : CM false xorb Tbool /\ CM false xorb Tbool2.
Proof.
  split; try solve [CM_xorb].
Qed.

Ltac CSR_andb_orb :=
  split; intros; [> | | |rewrite orb_andb_distrib_r|rewrite orb_true_r]; try solve[discriminate|apply boolCM|apply boolCM2|apply Oeset.compare_eq_refl].
Ltac CSR_orb_andb := 
   split; intros; [> | | |rewrite andb_orb_distrib_r|rewrite andb_false_r]; try solve[discriminate|apply boolCM|apply boolCM2|apply Oeset.compare_eq_refl].
Ltac CSR_eqb_orb :=
  split; [> | | |intros a1 a2 b; case b|intros];try solve[discriminate|apply boolCM3|apply boolCM2|apply Oeset.compare_eq_refl].
Ltac CSR_xorb_andb := 
  split; [> | | |intros a1 a2 b; case b|intros]; try solve[discriminate|apply boolCM|apply boolCM4|apply Oeset.compare_eq_refl].





Lemma bool_is_CSR1 : CSR true false andb orb Tbool /\ CSR true false andb orb Tbool2.
Proof.
  split; try solve [CSR_andb_orb].
Qed.

Lemma bool_is_CSR2 : CSR false true orb andb Tbool /\ CSR false true orb andb Tbool2.
Proof.
  split; try solve [CSR_orb_andb].
Qed.

Lemma bool_is_CSR3 : CSR true false eqb orb Tbool /\ CSR true false eqb orb Tbool2.
Proof.
  split; try solve [CSR_eqb_orb].
Qed.

Lemma bool_is_CSR4 : CSR false true xorb andb Tbool /\ CSR false true xorb andb Tbool2.
Proof.
    split; try solve [CSR_xorb_andb].
Qed.

Record CMcompat  (M : Type) (zero : M) (plus : M -> M -> M) (TM : total_order M) : Type :=
  mk_CMc
    {
      isCM : CM zero plus TM;
      equiv_plus_compat_CM_distr_lt :  forall a1 a2 b, 
          lt TM a1 a2 -> (eq TM (plus b a1) (plus b a2) \/ lt TM (plus b a1) (plus b a2));
    }.


Section CMc_SRI.
  Hypothesis M : Type.
  Hypothesis zero : M.
  Hypothesis plus : M -> M -> M.
  Hypothesis TM : total_order M.
  Hypothesis CMc_M : CMcompat zero plus TM.

  Inductive MI :=
  | Infinity : MI
  | EltM : M -> MI.

  Definition compMI a1 a2 :=
    match a1 with
    | Infinity => 
      match a2 with
      | Infinity => Eq
      | EltM b2 => Gt
      end
    | EltM b1 =>
      match a2 with
      | Infinity => Lt
      | EltM b2 => (t_compare TM) b1 b2
      end
    end.

  Ltac compMI_eq_trans_aux b :=
    intros [ |a1] [ |a2] [ |a3] H1 H2;
    try solve[inversion H1 | inversion H2 | trivial|apply b with a2; trivial].
  
  Definition compMI_eq_trans :
    forall a1 a2 a3, compMI a1 a2 = Eq -> compMI a2 a3 = Eq -> compMI a1 a3 = Eq.
  Proof.
     compMI_eq_trans_aux Oeset.compare_eq_trans.
  Qed.

  Definition compMI_eq_lt_trans :
    forall a1 a2 a3, compMI a1 a2 = Eq -> compMI a2 a3 = Lt -> compMI a1 a3 = Lt.
  Proof.
     compMI_eq_trans_aux Oeset.compare_eq_lt_trans.
  Qed.

  Definition compMI_lt_eq_trans :
    forall a1 a2 a3, compMI a1 a2 = Lt -> compMI a2 a3 = Eq -> compMI a1 a3 = Lt.
  Proof.
     compMI_eq_trans_aux Oeset.compare_lt_eq_trans.
  Qed.

  Definition compMI_lt_trans :
    forall a1 a2 a3, compMI a1 a2 = Lt -> compMI a2 a3 = Lt -> compMI a1 a3 = Lt.
  Proof.
    compMI_eq_trans_aux Oeset.compare_lt_trans.
  Qed.

  Definition compMI_lt_gt : forall a1 a2, compMI a1 a2 = CompOpp (compMI a2 a1).
  Proof.
    intros [ |a1] [ |a2]; try solve[inversion H1 | inversion H2 | trivial | apply Oeset.compare_lt_gt].
  Qed.

  Definition OTMI := 
    Oeset.mk_R compMI compMI_eq_trans compMI_eq_lt_trans 
               compMI_lt_eq_trans compMI_lt_trans compMI_lt_gt.

  Definition oneMI := EltM zero.

  Definition mulMI a1 a2 :=
    match a1 with
    | Infinity => Infinity
    | EltM b1 =>
      match a2 with
      | Infinity => Infinity
      | EltM b2 => EltM (plus b1 b2)
      end
    end.


  Definition minMI a1 a2 :=
    match compMI a1 a2 with
    |Gt => a2
    |_ => a1
    end.

  Lemma minMI_inf_r : forall a, minMI a Infinity = a.
  Proof.
    intros [|a]; trivial.
  Qed.

  Lemma minMI_inf_l : forall a, minMI Infinity a = a.
  Proof.
    intros [|a]; trivial.
  Qed.

  Ltac minMI_assoc_aux b H0 H H1 H2 := simpl; rewrite H0; simpl; rewrite H; apply b in H1; simpl; rewrite H2 ; trivial. 
  
  Lemma minMI_assoc : forall a1 a2 a3 ,
      minMI a1 (minMI a2 a3) = minMI (minMI a1 a2) a3.
  Proof.
    intros [|a1] [|a2] [|a3]; repeat rewrite minMI_inf_l + rewrite minMI_inf_r; trivial.
    - unfold minMI. destruct (Dcompare (Oeset.compare TM a1 a2)) as [H|[H|H]]; destruct (Dcompare (Oeset.compare TM a2 a3)) as [H0|[H0|H0]].
      * minMI_assoc_aux (Oeset.compare_eq_trans TM a1 a2 a3 H) H0 H H0 H0.
      * minMI_assoc_aux (Oeset.compare_eq_lt_trans TM a1 a2 a3 H) H0 H H0 H0.
      * minMI_assoc_aux (Oeset.compare_eq_gt_trans TM a1 a2 a3 H) H0 H H0 H0.
      * minMI_assoc_aux (Oeset.compare_lt_eq_trans TM a1 a2 a3 H) H0 H H0 H0.
      * minMI_assoc_aux (Oeset.compare_lt_trans TM a1 a2 a3 H) H0 H H0 H0.
      * simpl. rewrite H. rewrite H0; trivial.
      * simpl. rewrite H; simpl. rewrite H0; rewrite H; trivial. 
      * simpl. rewrite H; simpl. rewrite H0; rewrite H; trivial. 
      * minMI_assoc_aux (Oeset.compare_gt_trans TM a1 a2 a3 H) H H0 H0 H0.
Qed.        

  Lemma minMI_compat : forall a1 a2 b1 b2,
       eq OTMI a1 a2 -> eq OTMI b1 b2 -> eq OTMI (minMI a1 b1) (minMI a2 b2).
  Proof.
     intros [|a1] [|a2] [|b1] [|b2] H1 H2;
     try solve[apply Oeset.compare_eq_refl|inversion H1|inversion H2|assumption].
     unfold minMI. simpl. simpl in H1,H2.
     destruct (Dcompare (Oeset.compare TM a1 b1)) as [H|[H|H]]; rewrite H.
     * rewrite <- H1. apply Oeset.compare_eq_sym in H1. apply (Oeset.compare_eq_trans TM a2 a1 b1 H1) in H. simpl in H2. apply (Oeset.compare_eq_trans TM a2 b1 b2 H) in H2. 
       rewrite H2; trivial. 
     * rewrite <- H1. apply Oeset.compare_eq_sym in H1. apply (Oeset.compare_eq_lt_trans TM a2 a1 b1 H1) in H. simpl in H2. apply (Oeset.compare_lt_eq_trans TM a2 b1 b2 H) in H2. 
       rewrite H2; trivial.
     * rewrite <- H2. apply Oeset.compare_eq_sym in H1. apply (Oeset.compare_eq_gt_trans TM a2 a1 b1 H1) in H. simpl in H2. apply (Oeset.compare_gt_eq_trans TM a2 b1 b2 H) in H2. 
       rewrite H2; trivial.
  Qed.

  Lemma CM_minMI : CM Infinity minMI OTMI.
  Proof.
    split.
    - intros; rewrite minMI_assoc; apply Oeset.compare_eq_refl.
    - intros; rewrite minMI_inf_l; apply Oeset.compare_eq_refl.
    - intros [|a1] [|a2]; try solve[apply Oeset.compare_eq_refl].
      unfold minMI; simpl; rewrite (Oeset.compare_lt_gt TM a1 a2); destruct (Dcompare (Oeset.compare TM a2 a1)) as [H|[H|H]]; rewrite H; simpl; try solve [apply Oeset.compare_eq_refl|apply Oeset.compare_eq_sym; assumption].
    - apply minMI_compat.
  Qed.

  Lemma CM_mulMI : CM oneMI mulMI OTMI.
  Proof.
    split.
    - intros [|a1] [|a2] [|a3]; try solve[apply Oeset.compare_eq_refl | apply CMc_M].
    - intros [|a]; try solve[apply Oeset.compare_eq_refl | apply CMc_M].
    - intros [|a1] [|a2]; try solve[apply Oeset.compare_eq_refl | apply CMc_M].
    - intros [|a1] [|a2] [|b1] [|b2] H1 H2;
        try solve[apply Oeset.compare_eq_refl|inversion H1|inversion H2|assumption|apply CMc_M; trivial].
  Qed.


  Lemma MI_is_CSR : CSR Infinity oneMI minMI mulMI OTMI.
  Proof.
    split.
    - discriminate.
    - apply CM_minMI.
    - apply CM_mulMI.
    - intros [|a1] [|a2] [|b]; try solve[apply Oeset.compare_eq_refl].
      unfold minMI; simpl.
      destruct (Dcompare (Oeset.compare TM a1 a2)) as [H|[H|H]]; rewrite H.
      ** assert (eq TM (plus b a1) (plus b a2)); [>apply CMc_M;
           [>apply Oeset.compare_eq_refl|trivial]| rewrite H0; apply Oeset.compare_eq_refl].
      ** destruct (equiv_plus_compat_CM_distr_lt CMc_M a1 a2 b H); rewrite H0; apply Oeset.compare_eq_refl.
      ** rewrite Oeset.compare_lt_gt in H; apply CompOpp_iff in H; destruct (equiv_plus_compat_CM_distr_lt CMc_M a2 a1 b H) in H.
      -- rewrite <- H0. apply Oeset.compare_eq_sym in H0. rewrite H0; trivial.
      -- rewrite Oeset.compare_lt_gt in H0. apply CompOpp_iff in H0.
         rewrite H0. apply Oeset.compare_eq_refl.
    - intros [|a]; try solve[apply Oeset.compare_eq_refl | apply CMc_M].
  Qed.

End CMc_SRI.


(*Tropical integers*)

Lemma N_is_CMc : CMcompat N.zero N.add TN.
Proof.
  split. 
  - apply N_is_CM_add.
  - intros a1 a2 b H; right; apply N.add_lt_mono_l; assumption.
Qed.


Definition inf_ni := Infinity N.
Definition min_ni := minMI TN.
Definition comp_ni := compMI TN.
Definition mul_ni := mulMI N.add.
Definition one_ni := oneMI N.zero.
Definition OT_ni := OTMI TN.

Lemma NI_is_CSR : CSR inf_ni  one_ni min_ni mul_ni OT_ni.
Proof.
  apply MI_is_CSR. apply N_is_CMc.
Qed.


Section CM_aux.
  
Hypothesis M: Type.
Hypothesis zeroM : M.
Hypothesis plusM: M -> M -> M. 
Hypothesis TM : Oeset.Rcd M.
Hypothesis M_is_CM : CM zeroM plusM TM.

Lemma M_add_compat_r : forall m1 m2 m3,
eq TM m1 m2 -> eq TM (plusM m1 m3) (plusM m2 m3).
Proof.
intros. apply (plus_compat_eq_CM M_is_CM _ _ _ _ H (Oeset.compare_eq_refl _ _)); trivial.
Qed.

Lemma M_add_compat_l : forall m1 m2 m3,
eq TM m1 m2 -> eq TM (plusM m3 m1) (plusM m3 m2).
Proof.
intros. apply (plus_compat_eq_CM M_is_CM _ _ _ _ (Oeset.compare_eq_refl _ _) H); trivial.
Qed.


Lemma plus_aux_1 : forall a b c d,
eq TM
   (plusM (plusM a b) (plusM c d))
   (plusM (plusM a c) (plusM b d)).
Proof.
  intros.
  rewrite <- (Oeset.compare_eq_1 _ _ _ _ (plus_assocCM M_is_CM _ _ _)). 
  rewrite <- (Oeset.compare_eq_2 _ _ _ _ (plus_assocCM M_is_CM _ _ _)).
  apply (plus_compat_eq_CM M_is_CM _ _ _ _ (Oeset.compare_eq_refl _ _)).
  rewrite <- (Oeset.compare_eq_1 _ _ _ _ (plus_commCM M_is_CM _ _)). 
  rewrite <- (Oeset.compare_eq_1 _ _ _ _ (plus_assocCM M_is_CM _ _ _)).
  apply (plus_compat_eq_CM M_is_CM _ _ _ _ (Oeset.compare_eq_refl _ _)).
  apply (plus_commCM M_is_CM).
Qed.

Lemma plus_zero_rightCM : forall a, eq TM (plusM a zeroM) a.
Proof.
  intros.
  rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_commCM M_is_CM _ _)). 
  apply M_is_CM.
Qed.

Lemma plus_equal_zeroCM : forall a b, eq TM a zeroM -> eq TM b zeroM -> eq TM (plusM a b) zeroM.
Proof.
  intros.
  rewrite (Oeset.compare_eq_1 _ _ _ _ (plus_compat_eq_CM M_is_CM _ _ _ _ H H0)). 
  apply M_is_CM.
Qed.

(************ Oeset.eq_bool *************)

(** plus **)

Lemma plus_assoc_eq_bool : forall a1 a2 a3,
    Oeset.eq_bool TM (plusM a1 (plusM a2 a3)) (plusM (plusM a1 a2) a3) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply M_is_CM; now apply Oeset.eq_bool_true_compare_eq.
Qed.

Lemma plus_comm_eq_bool : forall a1 a2,
    Oeset.eq_bool TM (plusM a1 a2) (plusM a2 a1) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply M_is_CM; now apply Oeset.eq_bool_true_compare_eq.
Qed.

Lemma plus_zero_l_eq_bool : forall a,
    Oeset.eq_bool TM (plusM zeroM a) a = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply M_is_CM; now apply Oeset.eq_bool_true_compare_eq.
Qed.

Lemma plus_zero_r_eq_bool : forall a,
    Oeset.eq_bool TM (plusM a zeroM) a = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_comm_eq_bool _ _)).
  apply plus_zero_l_eq_bool.
Qed.

Lemma plus_compat_eq_bool : forall a1 a2 b1 b2,
    Oeset.eq_bool TM a1 a2 = true ->
    Oeset.eq_bool TM b1 b2 = true ->
    Oeset.eq_bool TM (plusM a1 b1) (plusM a2 b2) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply M_is_CM; now apply Oeset.eq_bool_true_compare_eq.
Qed.

Lemma plus_compat_l_eq_bool : forall a1 a2 b,
    Oeset.eq_bool TM a1 a2 = true ->
    Oeset.eq_bool TM (plusM a1 b) (plusM a2 b) = true.
Proof.
  intros.
  apply plus_compat_eq_bool; trivial.
  apply Oeset.eq_bool_refl.
Qed.

Lemma plus_compat_r_eq_bool : forall a b1 b2,
    Oeset.eq_bool TM b1 b2 = true ->
    Oeset.eq_bool TM (plusM a b1) (plusM a b2) = true.
Proof.
  intros.
  apply plus_compat_eq_bool; trivial.
  apply Oeset.eq_bool_refl.
Qed.

(************ fonction auxiliaire plus ************)

Lemma plus_aux_2_d : forall a b c,
    Oeset.eq_bool TM (plusM a (plusM b c)) (plusM b (plusM a c)) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  apply plus_compat_l_eq_bool.
  apply plus_comm_eq_bool.
Qed.


Lemma plus_aux_2_g : forall a1 a2 a3,
    Oeset.eq_bool TM (plusM (plusM a1 a2) a3) (plusM (plusM a1 a3) a2) = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  apply plus_compat_r_eq_bool.
  apply plus_comm_eq_bool.
Qed.


Lemma plus_aux_3 : forall a b c d,
    Oeset.eq_bool TM (plusM (plusM a c) (plusM b d)) (plusM (plusM a b) (plusM c d)) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply plus_aux_1.
Qed.

Lemma plus_aux_4 : forall a b c,
    Oeset.eq_bool TM (plusM a (plusM (plusM b zeroM) c)) (plusM b (plusM a c)) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_comm_eq_bool _ _)).
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  apply plus_compat_r_eq_bool.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_zero_l_eq_bool _)).
  apply plus_comm_eq_bool.
Qed.

Lemma plus_if_zero : forall (a : bool) b c,
    Oeset.eq_bool TM (if a then plusM b c else c) (plusM c (if a then plusM b zeroM else zeroM)) = true.
Proof.
  intros.
  destruct a; [>rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_comm_eq_bool _ _));apply plus_compat_r_eq_bool|];
  rewrite Oeset.eq_bool_sym;
  apply plus_zero_r_eq_bool.
Qed.

Require Import List.

(******* Fold ******)
Lemma fold_sortir : forall A l f b,
    (forall c e, (Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true)) ->
    Oeset.eq_bool TM
    (fold_left
       (fun (a1 : M) (e1 : A) => (f a1 e1)) l b)
    (plusM b (fold_left
       (fun (a1 : M) (e1 : A) => (f a1 e1)) l zeroM)) = true.
Proof.
  induction l; intros.
  - rewrite Oeset.eq_bool_sym.
    apply plus_zero_r_eq_bool.
  - simpl.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl _ _ H)).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl _ _ H))).
  rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
  now apply plus_compat_l_eq_bool.
Qed.


Lemma fold_sortir_l : forall A x (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM)) x = true ->
    Oeset.eq_bool TM (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b) x = true.
Proof.
  intros.
  now rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (fold_sortir _ _ _ H)).
Qed.

Lemma fold_sortir_r : forall A x (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM x (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM)) = true ->
    Oeset.eq_bool TM x (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  rewrite Oeset.eq_bool_sym. now apply fold_sortir.
Qed.

Lemma fold_sortir_l_bis : forall A x (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b) x = true ->
    Oeset.eq_bool TM (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM)) x = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  rewrite Oeset.eq_bool_sym.
  now apply fold_sortir.
Qed.

Lemma fold_sortir_r_bis : forall A x (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM x (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b) = true ->
    Oeset.eq_bool TM x (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM)) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  now apply fold_sortir.
Qed.


Lemma fold_sortir_l_plus_l : forall A x a (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM (plusM a (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM))) x = true ->
    Oeset.eq_bool TM (plusM a (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b)) x = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  apply plus_compat_r_eq_bool. now apply fold_sortir.
Qed.

Lemma fold_sortir_l_plus_r : forall A a x (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM (plusM (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM)) a) x = true ->
    Oeset.eq_bool TM (plusM (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b) a) x = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ H0).
  apply plus_compat_l_eq_bool.
  now apply fold_sortir.
Qed.

Lemma fold_sortir_r_plus_l : forall A x a (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM x (plusM a (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM))) = true ->
    Oeset.eq_bool TM x (plusM a (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b)) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  apply plus_compat_r_eq_bool.
  rewrite Oeset.eq_bool_sym.
  now apply fold_sortir.
Qed.

Lemma fold_sortir_r_plus_r : forall A x a (l : list A) (f : M -> A -> M) (b : M),
    (forall (c : M) (e : A), Oeset.eq_bool TM (f c e) (plusM c (f zeroM e)) = true) ->
    Oeset.eq_bool TM x (plusM (plusM b (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l zeroM)) a) = true ->
    Oeset.eq_bool TM x (plusM (fold_left (fun (a1 : M) (e1 : A) => f a1 e1) l b) a) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  apply plus_compat_l_eq_bool.
  rewrite Oeset.eq_bool_sym. now apply fold_sortir.
Qed.


End CM_aux.


Section CSR_aux.
  Hypothesis R: Type.
  Hypothesis zeroR : R.
  Hypothesis oneR : R.
  Hypothesis plusR: R -> R -> R. 
  Hypothesis mulR: R -> R -> R. 
  Hypothesis TR : Oeset.Rcd R.
  Hypothesis R_is_CSR : CSR zeroR oneR plusR mulR TR.
  
  Notation mul_assoc_eq_bool := (plus_assoc_eq_bool (isCM_mul_SR R_is_CSR)).
  Notation mul_comm_eq_bool := (plus_comm_eq_bool (isCM_mul_SR R_is_CSR)).
  Notation mul_one_l_eq_bool := (plus_zero_l_eq_bool (isCM_mul_SR R_is_CSR)).
  Notation mul_one_r_eq_bool := (plus_zero_r_eq_bool (isCM_mul_SR R_is_CSR)).
  Notation mul_compat_eq_bool := (plus_compat_eq_bool (isCM_mul_SR R_is_CSR)).
  Notation mul_compat_l_eq_bool := (plus_compat_l_eq_bool (isCM_mul_SR R_is_CSR)).
  Notation mul_compat_r_eq_bool := (plus_compat_r_eq_bool (isCM_mul_SR R_is_CSR)).

  Notation plus_assoc_eq_bool := (plus_assoc_eq_bool (isCM_plus_SR R_is_CSR)).
  Notation plus_comm_eq_bool := (plus_comm_eq_bool (isCM_plus_SR R_is_CSR)).
  Notation plus_zero_l_eq_bool := (plus_zero_l_eq_bool (isCM_plus_SR R_is_CSR)).
  Notation plus_zero_r_eq_bool := (plus_zero_r_eq_bool (isCM_plus_SR R_is_CSR)).
  Notation plus_compat_eq_bool := (plus_compat_eq_bool (isCM_plus_SR R_is_CSR)).
  Notation plus_compat_l_eq_bool := (plus_compat_l_eq_bool (isCM_plus_SR R_is_CSR)).
  Notation plus_compat_r_eq_bool := (plus_compat_r_eq_bool (isCM_plus_SR R_is_CSR)).

  Notation plus_aux_1 := (plus_aux_1 (isCM_plus_SR R_is_CSR)).
  Notation plus_aux_2 := (plus_aux_2_d (isCM_plus_SR R_is_CSR)).
  Notation mul_aux_1 := (plus_aux_2_g (isCM_mul_SR R_is_CSR)).
  Notation plus_aux_3 := (plus_aux_3 (isCM_plus_SR R_is_CSR)).
  Notation plus_aux_4 := (plus_aux_4 (isCM_plus_SR R_is_CSR)).
  Notation plus_if_zero := (plus_if_zero (isCM_plus_SR R_is_CSR)).

  Notation fold_sortir := (fold_sortir (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_l := (fold_sortir_l (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_r := (fold_sortir_r (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_l_bis := (fold_sortir_l_bis (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_r_bis := (fold_sortir_r_bis (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_l_plus_l := (fold_sortir_l_plus_l (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_l_plus_r := (fold_sortir_l_plus_r (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_r_plus_l := (fold_sortir_r_plus_l (isCM_plus_SR R_is_CSR)).
  Notation fold_sortir_r_plus_r := (fold_sortir_r_plus_r (isCM_plus_SR R_is_CSR)).

  Ltac fold_sortir_if := intros; apply fold_sortir; intros; apply plus_if_zero.
  Ltac fold_sortir_2_if := intros; apply fold_sortir; intros; apply fold_sortir; intros; apply plus_if_zero.


Lemma mul_zero_absorb_right_SR : forall a, eq TR (mulR zeroR a) zeroR.
Proof.
  intros.
  apply (Oeset.compare_eq_trans _ _ _ _ (plus_commCM (isCM_mul_SR R_is_CSR) _ _)).
  apply (mul_zero_absorb_left_SR R_is_CSR).
Qed.
  
Lemma mul_zero_r_eq_bool : forall a,
      Oeset.eq_bool TR (mulR a zeroR) zeroR = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply R_is_CSR; now apply Oeset.eq_bool_true_compare_eq.
Qed.

Lemma mul_zero_l_eq_bool : forall a,
    Oeset.eq_bool TR (mulR zeroR a) zeroR = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (mul_zero_r_eq_bool a)).
  apply Oeset.eq_bool_true_compare_eq.
  apply R_is_CSR.
Qed.

(** Distr **)
Lemma mul_distr_plus_l_eq_bool : forall a1 a2 b,
    Oeset.eq_bool TR (mulR b (plusR a1 a2)) (plusR (mulR b a1) (mulR b a2)) = true.
Proof.
  intros.
  apply Oeset.eq_bool_true_compare_eq.
  apply R_is_CSR; now apply Oeset.eq_bool_true_compare_eq.
Qed.

Lemma mul_distr_plus_r_eq_bool : forall a1 a2 b,
    Oeset.eq_bool TR (mulR (plusR a1 a2) b) (plusR (mulR a1 b) (mulR a2 b)) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_comm_eq_bool _ _)).
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (mul_distr_plus_l_eq_bool _ _ _)).
  apply plus_compat_eq_bool; apply mul_comm_eq_bool.
Qed.

Require Import List.


Lemma fold_2_sortir : forall A l1 l (f : A -> A -> bool) f1 (a: A) b,
    Oeset.eq_bool TR
    (fold_left
       (fun (a1 : R) (e1 : A) =>
        fold_left
          (fun (a2 : R) (e2 : A) =>
           if f e1 e2 then plusR (f1 e1 e2) a2 else a2) l
          (if f e1 a then plusR (f1 e1 a) a1 else a1))
       l1 b)
    (plusR (fold_left
       (fun (a1 : R) (e1 : A) =>
          if f e1 a then plusR (f1 e1 a) a1
          else a1) l1 b)
     (fold_left
       (fun (a1 : R) (e1 : A) =>
           fold_left
              (fun (a2 : R) (e2 : A) =>
               if f e1 e2 then plusR (f1 e1 e2) a2 else a2) l
              a1)
       l1 zeroR)) = true.
Proof.
  induction l1; intros.
  - rewrite Oeset.eq_bool_sym;
    apply plus_zero_r_eq_bool.
  - simpl; rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (IHl1 _ _ _ _ _)).
    apply fold_sortir_r_plus_l.
    intros; apply fold_sortir; intros; apply plus_if_zero.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
    apply plus_compat_l_eq_bool.
    apply fold_sortir_l.
    intros; apply plus_if_zero.
    apply fold_sortir_r_plus_r.
    intros; apply plus_if_zero.
    apply fold_sortir_l_plus_r.
    intros; apply plus_if_zero.
    rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
    rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
    apply plus_compat_r_eq_bool.
    apply plus_comm_eq_bool.
Qed.

(**fold_sortir mul**)
Lemma fold_sortir_r_mul_r : forall A a x (l : list A) (f : R -> A -> R) (b : R),
    (forall (c : R) (e : A), Oeset.eq_bool TR (f c e) (plusR c (f zeroR e)) = true) ->
    Oeset.eq_bool TR x (mulR (plusR b (fold_left (fun (a1 : R) (e1 : A) => f a1 e1) l zeroR)) a) = true ->
    Oeset.eq_bool TR x (mulR (fold_left (fun (a1 : R) (e1 : A) => f a1 e1) l b) a) = true.
Proof.
  intros.
  rewrite (Oeset.eq_bool_eq_1 _ _ _ _ H0).
  apply mul_compat_l_eq_bool.
  rewrite Oeset.eq_bool_sym. now apply fold_sortir.
Qed.

Lemma fold_sortir_mul : forall A (f : A -> bool) f' l b,
    Oeset.eq_bool TR
    (fold_left
       (fun (a2 : R) (e2 : A) =>
          if f e2
          then plusR (mulR (f' e2) b) a2
          else a2) l zeroR)
      (mulR (fold_left (fun (a2 : R) (e2 : A) =>
           if f e2
           then plusR (f' e2) a2
           else a2) l zeroR) b) = true.
Proof.
  induction l; intros; simpl.
  - rewrite Oeset.eq_bool_sym.
    apply mul_zero_l_eq_bool.
  - apply fold_sortir_l; try solve [intros; apply plus_if_zero].
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl b))).
    destruct (f a).
    * rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_l_eq_bool _ _ _ (plus_zero_r_eq_bool _))).
      rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (mul_distr_plus_r_eq_bool _ _ _)).
      apply mul_compat_l_eq_bool.
      rewrite <- (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_l_eq_bool _ _ _ (plus_zero_r_eq_bool _))).
      rewrite Oeset.eq_bool_sym.
      intros; apply fold_sortir; intros; apply plus_if_zero.
    * apply plus_zero_l_eq_bool.
Qed.


Lemma equiv_fold: forall A l f1 f2 b,
    (forall e (c0 : R), Oeset.eq_bool TR (f1 c0 e) (plusR c0 (f1 zeroR e)) = true) ->
    (forall e (c0 : R), Oeset.eq_bool TR (f2 c0 e) (plusR c0 (f2 zeroR e)) = true) ->
    (forall e (c0 : R), Oeset.eq_bool TR (f1 c0 e) (f2 c0 e) = true) ->
    Oeset.eq_bool TR
       (fold_left (fun (a1 : R) (e1 : A) => f1 a1 e1) l b) 
       (fold_left (fun (a1 : R) (e1 : A) => f2 a1 e1) l b) = true. 
Proof.
  intros; revert b; induction l; intros; try apply Oeset.eq_bool_refl.
  simpl.
  apply fold_sortir_l; trivial. 
  apply fold_sortir_r; trivial.
  now apply plus_compat_eq_bool.
Qed.


Lemma equiv_fold_fold : forall A l1 l2 f1 f2 b,
    (forall e (c0 : R) (e0 : A), Oeset.eq_bool TR (f1 c0 e e0) (plusR c0 (f1 zeroR e e0)) = true) ->
    (forall e (c0 : R) (e0 : A), Oeset.eq_bool TR (f2 c0 e e0) (plusR c0 (f2 zeroR e e0)) = true) ->
    (forall e (c0 : R) (e0 : A), Oeset.eq_bool TR (f1 c0 e e0) (f2 c0 e e0) = true) ->
    Oeset.eq_bool TR
       (fold_left (fun (a1 : R) (e1 : A) =>
       (fold_left (fun (a2 : R) (e2 : A) => f1 a2 e1 e2) l2 a1)) l1 b) 
       (fold_left (fun (a1 : R) (e1 : A) =>
       (fold_left (fun (a2 : R) (e2 : A) => f2 a2 e1 e2) l2 a1)) l1 b) = true. 
Proof.
  intros.
  apply equiv_fold; try solve [intros; now apply fold_sortir].
  intros; now apply equiv_fold.
Qed.

Lemma essaiaux : forall A f1 f2 l b,
    (forall (c : R) (e : A), Oeset.eq_bool TR (f1 c e) (plusR c (f1 zeroR e)) = true) ->
    (forall (c : R) (e : A), Oeset.eq_bool TR (f2 c e) (plusR c (f2 zeroR e)) = true) ->
    Oeset.eq_bool TR (fold_left (fun (a : R) (e : A) => plusR (f1 a e) (f2 zeroR e)) l b)
      (plusR (fold_left (fun (a : R) (e : A) => (f1 a e)) l b)
            (fold_left (fun (a : R) (e : A) => f2 a e) l zeroR)) = true.
Proof.
  intros; revert b; induction l; intros.
  - rewrite Oeset.eq_bool_sym.
    apply plus_zero_r_eq_bool.
  - simpl. apply fold_sortir_l.
    intros.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
    now apply plus_compat_l_eq_bool.
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_compat_r_eq_bool _ _ _ (IHl _))).
    rewrite (Oeset.eq_bool_eq_1 _ _ _ _ (plus_aux_3 _ _ _ _)).
    apply plus_compat_eq_bool; rewrite Oeset.eq_bool_sym; now apply fold_sortir.
Qed.

Lemma essai1 : forall A l1 l2 (f: A -> A -> A -> bool) f1 (c : R) (e : A),
 Oeset.eq_bool TR
   (fold_left
       (fun (a2 : R) (e2 : A) =>
        fold_left
          (fun (a3 : R) (e3 : A) =>
           if f e e2 e3 then plusR (f1 e e2 e3) a3 else a3) l1 a2)
       l2 c)
   (plusR c
      (fold_left
          (fun (a2 : R) (e2 : A) =>
           fold_left
             (fun (a3 : R) (e3 : A) =>
              if f e e2 e3 then plusR (f1 e e2 e3) a3 else a3) l1
             a2) l2 zeroR)) = true.
Proof.
  fold_sortir_2_if.
Qed.

Lemma essai2 : forall A l (f: A -> A -> A -> bool) f1 a (c : R) (e : A),
 Oeset.eq_bool TR
   (fold_left
       (fun (a2 : R) (e2 : A) =>
        if f e e2 a then plusR (f1 e e2 a) a2 else a2) l c)
   (plusR c
      (fold_left
          (fun (a2 : R) (e2 : A) =>
           if f e e2 a then plusR (f1 e e2 a) a2 else a2) l zeroR)) = true.
Proof.
  fold_sortir_if.
Qed.

Lemma fold_3_sortir : forall A l1 l2 l (f : A -> A -> A -> bool) f1 (a: A) b,
    Oeset.eq_bool TR
    (fold_left
       (fun (a1 : R) (e1 : A) =>
        fold_left
          (fun (a2 : R) (e2 : A) =>
          fold_left
          (fun (a3 : R) (e3 : A) =>
           if f e1 e2 e3 then plusR (f1 e1 e2 e3) a3 else a3) l
          (if f e1 e2 a then plusR (f1 e1 e2 a) a2 else a2))
       l2 a1) l1 b)
    (plusR (fold_left
         (fun (a1 : R) (e1 : A) =>
          fold_left
          (fun (a2 : R) (e2 : A) =>
          if f e1 e2 a then plusR (f1 e1 e2 a) a2
          else a2) l2 a1) l1 b)
     (fold_left
       (fun (a1 : R) (e1 : A) =>
           fold_left
             (fun (a2 : R) (e2 : A) =>
                 fold_left
          (fun (a3 : R) (e3 : A) =>
               if f e1 e2 e3 then plusR (f1 e1 e2 e3) a3 else a3) l
              a2) l2 a1)
       l1 zeroR)) = true.
Proof.
  intros.
  rewrite <- (Oeset.eq_bool_eq_2 _ _ _ _ (essaiaux _ _ _ _ (essai2 _ _ _ _) (essai1 _ _ _ _))).
  apply equiv_fold.
  * intros. apply fold_sortir; intros.
    apply fold_sortir_l. intros; apply plus_if_zero.
    apply fold_sortir_r_plus_l. intros; apply plus_if_zero.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
    apply plus_compat_l_eq_bool.
    apply plus_if_zero.
  * intros.
    rewrite (Oeset.eq_bool_eq_2 _ _ _ _ (plus_assoc_eq_bool _ _ _)).
    apply plus_compat_l_eq_bool.
    apply essai2.
  * intros; apply fold_2_sortir.
Qed.

End CSR_aux.



Section compat_inv_op.
  Record CIO (A : Type) (plus : A -> A -> A) (TA : total_order A) : Type :=
  mk_CIO
    {
      compat_g : forall a1 a2 a3, eq TA a1 a2 <-> eq TA (plus a1 a3) (plus a2 a3);
      compat_d : forall a1 a2 a3, eq TA a1 a2 <-> eq TA (plus a3 a1) (plus a3 a2)
    }.
  
End compat_inv_op.
    
