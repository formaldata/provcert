(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(* Miscellanous useful types and functions *)

type ('a, 'b) sum =
  | Left : 'a -> ('a, 'b) sum
  | Right : 'b -> ('a, 'b) sum

let rec partition = function
  | [] -> ([], [])
  | (Left a)::l -> let (l1, l2) = partition l in (a::l1, l2)
  | (Right b)::l -> let (l1, l2) = partition l in (l1, b::l2)


let fold_and_add_comma f l = String.concat ", " (List.map f l)


(* Collecting Coq terms *)

let gen_constant_in_modules s m n = Universes.constr_of_global @@ Coqlib.gen_reference_in_modules s m n
(* let gen_constant_in_modules s m n = EConstr.of_constr (Universes.constr_of_global @@ Coqlib.gen_reference_in_modules s m n) *)
let gen_constant modules constant = lazy (gen_constant_in_modules "Datacert" modules constant)


(* Positive *)
let positive_modules = [["Coq";"Numbers";"BinNums"]]
let cpositive = gen_constant positive_modules "positive"
let cxI = gen_constant positive_modules "xI"
let cxO = gen_constant positive_modules "xO"
let cxH = gen_constant positive_modules "xH"

(* N *)
let n_modules = [["Coq";"Numbers";"BinNums"]]
let cN = gen_constant n_modules "N"
let cN0 = gen_constant n_modules "N0"
let cNpos = gen_constant n_modules "Npos"

(* Z *)
let z_modules = [["Coq";"Numbers";"BinNums"]]
let cZ = gen_constant z_modules "Z"
let cZ0 = gen_constant z_modules "Z0"
let cZpos = gen_constant z_modules "Zpos"
let cZneg = gen_constant z_modules "Zneg"

(* Booleans *)
let bool_modules = [["Coq";"Bool";"Bool"]]
let cbool = gen_constant Coqlib.init_modules "bool"
let ctrue = gen_constant Coqlib.init_modules "true"
let cfalse = gen_constant Coqlib.init_modules "false"

(* Lists *)
let clist = gen_constant Coqlib.init_modules "list"
let cnil = gen_constant Coqlib.init_modules "nil"
let ccons = gen_constant Coqlib.init_modules "cons"

(* Option *)
let coption = gen_constant Coqlib.init_modules "option"
let cSome = gen_constant Coqlib.init_modules "Some"
let cNone = gen_constant Coqlib.init_modules "None"

(* Pairs *)
let cprod = gen_constant Coqlib.init_modules "prod"
let cpair = gen_constant Coqlib.init_modules "pair"

(* Strings *)
let string_modules = [["Coq";"Strings";"Ascii"];["Coq";"Strings";"String"]]
let cAscii = gen_constant string_modules "Ascii"
let cString = gen_constant string_modules "String"
let cstring = gen_constant string_modules "string"
let cEmptyString = gen_constant string_modules "EmptyString"


(* Generic functions to construct Coq terms *)

let mkName s =
  let id = Names.Id.of_string s in
  Names.Name id
let mklApp f args = Constr.mkApp (Lazy.force f, args)

let mkList ty elem l =
  List.fold_right (fun c acc -> mklApp ccons [|ty; elem c; acc|]) l (mklApp cnil [|ty|])

let mkBool = function
  | true -> Lazy.force ctrue
  | false -> Lazy.force cfalse

let mkPositive i =
  if i <= 0 then assert false else
  let rec mkPos i =
    if i = 1 then Lazy.force cxH
    else if i mod 2 = 0 then mklApp cxO [|mkPos (i/2)|]
    else mklApp cxI [|mkPos (i/2)|] in
  mkPos i

let mkN i =
  if i = 0 then Lazy.force cN0
  else if i > 0 then mklApp cNpos [|mkPositive i|]
  else failwith "Sqlparser.mkN: not a positive integer"

let mkZ i =
  if i = 0 then Lazy.force cZ0
  else if i > 0 then mklApp cZpos [|mkPositive i|]
  else mklApp cZneg [|mkPositive (-i)|]


let mkUConst : Constr.t -> Safe_typing.private_constants Entries.definition_entry = fun c ->
  let env = Global.env () in
  let evd = Evd.from_env env in
  let evd, ty = Typing.type_of env evd (EConstr.of_constr c) in
  { Entries.const_entry_body        = Future.from_val ((c, Univ.ContextSet.empty),
                                               Safe_typing.empty_private_constants);
    const_entry_secctx      = None;
    const_entry_feedback    = None;
    const_entry_type        = Some (EConstr.Unsafe.to_constr ty);
    const_entry_universes   = Evd.const_univ_entry ~poly:false evd;
    const_entry_opaque      = false;
    const_entry_inline_code = false }


(* Functions to construct Coq strings *)

let mkAscii c =
  let a = int_of_char c in
  let t = Array.make 8 (Lazy.force cfalse) in
  let power = ref 1 in
  for i = 0 to 7 do
    if (a / !power) mod 2 = 1 then t.(i) <- Lazy.force ctrue;
    power := 2 * !power
  done;
  mklApp cAscii t

let mkString s =
  let rec mkString i acc =
    if i = -1 then acc else
      mkString (i-1) (mklApp cString [|mkAscii s.[i]; acc|]) in
  mkString ((String.length s) - 1) (Lazy.force cEmptyString)


(* Common Datacert terms *)
let datacert_common_modules = [["Datacert";"terms";"DExpressions"];
                               ["Datacert";"data";"sql";"SqlCommon"];
                               ["Datacert";"data";"sql";"Sql"];
                               ["Datacert";"logic";"Formula"];
                               ["Datacert";"data";"proof_of_concept";"Values";"NullValues"];
                               ["Datacert";"data";"proof_of_concept";"GenericInstance"];
                               ["Datacert";"data";"proof_of_concept";"SqlSyntax"]]
let cattribute = gen_constant datacert_common_modules "attribute"
let cAttr_string = gen_constant datacert_common_modules "Attr_string"
let cAttr_Z = gen_constant datacert_common_modules "Attr_Z"
let cAttr_bool = gen_constant datacert_common_modules "Attr_bool"

let cvalue = gen_constant [["Datacert";"data";"proof_of_concept";"Values";"NullValues"]] "value"
let cValue_string = gen_constant [["Datacert";"data";"proof_of_concept";"Values";"NullValues"]] "Value_string"
let cValue_Z = gen_constant [["Datacert";"data";"proof_of_concept";"Values";"NullValues"]] "Value_Z"
let cValue_bool = gen_constant [["Datacert";"data";"proof_of_concept";"Values";"NullValues"]] "Value_bool"

let crelname = gen_constant datacert_common_modules "relname"
let cRel = gen_constant datacert_common_modules "Rel"

let cAnd_F = gen_constant datacert_common_modules "And_F"
let cOr_F = gen_constant datacert_common_modules "Or_F"

let cpredicate = gen_constant datacert_common_modules "predicate"
let cPredicate = gen_constant datacert_common_modules "Predicate"

let c_funterm = gen_constant datacert_common_modules "_funterm"
let c_F_Constant = gen_constant datacert_common_modules "_F_Constant"
let c__Sql_Dot = gen_constant datacert_common_modules "__Sql_Dot"
let c_F_Expr = gen_constant datacert_common_modules "_F_Expr"

let c_aggterm = gen_constant datacert_common_modules "_aggterm"
let c_A_Expr = gen_constant datacert_common_modules "_A_Expr"
let c_A_agg = gen_constant datacert_common_modules "_A_agg"
let c_A_fun = gen_constant datacert_common_modules "_A_fun"

let csymbol = gen_constant [["Datacert";"data";"proof_of_concept";"SqlSyntax"]] "symbol"
let cSymbol = gen_constant [["Datacert";"data";"proof_of_concept";"SqlSyntax"]] "Symbol"
let cAggregate = gen_constant [["Datacert";"data";"proof_of_concept";"SqlSyntax"]] "Aggregate"

let c_select = gen_constant datacert_common_modules "_select"
let c_Select_As = gen_constant datacert_common_modules "_Select_As"
