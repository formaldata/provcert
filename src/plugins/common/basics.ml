(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(* Basic types *)

open Utils


(* Names of relations *)
type relname = string
let string_of_relname (r:relname) : string = r
let mk_relname = Utils.mkString
let mk_tablename t = mklApp cRel [|mk_relname t|]

(* Types *)
type coltype = TString | TInt | TBool
let string_of_coltype = function
  | TInt -> "int"
  | TString -> "text"
  | TBool -> "boolean"


(* Names of attributes *)
type aname = string
let string_of_aname (a:aname) : string = a
let mk_aname = mkString


type attribute_name = relname option * aname
let string_of_attribute_name sep : attribute_name -> string = function
  | (Some t, a) -> (string_of_relname t)^sep^(string_of_aname a)
  | (None, a) -> string_of_aname a
let string_of_attribute_name_wrn ((_,a):attribute_name) : string =
  string_of_aname a


(* Typed attributes *)
type typed_aname = aname * coltype
let string_of_typed_aname ((a,t):typed_aname) =
  (string_of_aname a) ^ " " ^ (string_of_coltype t)
let string_of_typed_aname_wt ((a,_):typed_aname) = string_of_aname a
type typed_attribute_name = attribute_name * coltype
let string_of_typed_attribute_name_wt sep ((a,_):typed_attribute_name) =
  string_of_attribute_name sep a
let string_of_typed_attribute_name_wt_wrn ((a,_):typed_attribute_name) =
  string_of_attribute_name_wrn a
let mk_typed_attribute_name rn (an, t) =
  let c =
    if rn then
      string_of_attribute_name "_" an
    else
      string_of_attribute_name_wrn an
  in
  let attr =
    match t with
      | TString -> cAttr_string
      | TInt -> cAttr_Z
      | TBool -> cAttr_bool
  in
  mklApp attr [|Lazy.force cN0; mk_aname c|]

(* Values, e.g. [3], [2017-03-28] *)
(* TODO: add more types *)
type value =
  | VNull
  | VString of string
  | VInt of int
  | VBool of bool
let string_of_value = function
  | VNull -> "NULL"
  | VString s -> "'"^s^"'"
  | VInt i -> string_of_int i
  | VBool b -> string_of_bool b
let mk_value = function
  (* We consider NULL values as untyped, and put them into bool values *)
  | VNull -> mklApp cValue_bool [|mklApp cNone [|Lazy.force cbool|]|]
  | VString s -> mklApp cValue_string [|mklApp cSome [|Lazy.force cstring; mkString s|]|]
  | VInt i -> mklApp cValue_Z [|mklApp cSome [|Lazy.force cZ; mkZ i|]|]
  | VBool b -> mklApp cValue_bool [|mklApp cSome [|Lazy.force cbool; mkBool b|]|]


(* Function symbols, e.g. [+], [-], ... *)
type symb = string
let string_of_symb (s:symb) : string = s
let mk_symb s a =
  let s_string =
    match s with
      | "+" -> "plus"
      | "*" -> "mult"
      | "-" -> if a = 2 then "minus" else "opp"
      | _ -> s
  in
  mklApp cSymbol [|mkString s_string|]

(* Predicate symbols, e.g. [<=], [>], ... *)
type predicate = string
let string_of_predicate (s:predicate) : string = s
let mk_predicate p = mklApp cPredicate [|mkString p|]
let comm_pred : predicate -> predicate = function
  | "<" -> ">"
  | "<=" -> ">="
  | ">" -> "<"
  | ">=" -> "<="
  | "=" -> "="
  | "<>" -> "<>"
  | p -> p

(* Aggregate symbols, e.g. [count], [avg], ... *)
type aggregate = string
let string_of_aggregate (s:aggregate) : string = s
let mk_aggregate a =
  let a_string =
    match a with
      | "COUNT" -> "count"
      | "SUM" -> "sum"
      | "AVG" -> "avg"
      | s -> s
  in
  mklApp cAggregate [|mkString a_string|]


(* Set operations between queries *)
type set_op =
  | Union
  | Intersect
  | Except
let string_of_set_op = function
  | Union -> "UNION"
  | Intersect -> "INTERSECT"
  | Except -> "EXCEPT"

(* Logical connectives *)
type and_or =
  | And_F
  | Or_F
let string_of_and_or = function
  | And_F -> "AND"
  | Or_F -> "OR"
let mk_and_or = function
  | And_F -> Lazy.force cAnd_F
  | Or_F -> Lazy.force cOr_F

(* Quantifiers *)
type quantifier =
  | Forall_F
  | Exists_F
let string_of_quantifier = function
  | Forall_F -> "ALL"
  | Exists_F -> "ANY"
