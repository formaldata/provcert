(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(* Managing SQL context *)

open Basics


(* Name and type handling *)
type context_table = relname * (typed_aname list)
type context_tables = context_table list

let context_tables_get_type (ctxt:context_tables) (an:attribute_name) =
  match an with
    | (Some rn, a) -> Some (List.assoc a (List.assoc rn ctxt))
    | _ -> None


let pair_find_first f g p =
  let (a,b) = p in
  match f a with
    | Some x -> Some x
    | None -> g b

let rec list_find_first f l =
  match l with
    | [] -> None
    | y::ys ->
       match f y with
         | Some z -> Some z
         | None -> list_find_first f ys

let find_or_fail e = function
  | Some t -> t
  | None -> failwith(e)

let context_table_find (an : aname) (ctxtt : context_table) =
  let (rn, l) = ctxtt in
  if List.exists (fun (an',_) -> an = an') l then Some rn else None
let context_tables_find (an : aname) = list_find_first (context_table_find an)

(* let context_table_get_type (an : aname) (ct : context_table) =
 *   let (_, l) = ct in
 *   try Some (List.assoc an l)
 *   with Not_found -> None
 * let context_tables_get_type (an : aname) = list_find_first (context_table_get_type an)
 * let context_tables_get_type_fail an ctxt =
 *   find_or_fail ("Type of attribute "^an^" is unknown") (context_tables_get_type an ctxt) *)

(* let context_table_to_coq ((rn, al) : context_table) : context_table =
 *   (rn, List.map (fun (a,t) -> (string_of_attribute_name "_" (Some rn, a), t)) al)
 * 
 * let context_tables_to_coq (ctxt : context_tables) : context_tables =
 *   List.map context_table_to_coq ctxt *)


let (fresh_att, clear_fresh_att) =
  let f = ref (-1) in
  (fun () -> incr f; "_c"^(string_of_int !f)), (fun () -> f := -1)

let clear () = clear_fresh_att ()


