(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(** Common structures between SQLCoq and SQLAlgebra **)

open Utils
open Basics


(* Expressions *)
type funterm =
  (* A constant, e.g. [3] *)
  | F_Constant of value
  (* Some column, eg [table.col] *)
  | F_Dot of typed_attribute_name
  (* Fully applied function symbol, eg [t1.c1 + t2.c2] *)
  | F_Expr of symb * funterm list

let rec string_of_funterm = function
  | F_Constant v -> string_of_value v
  | F_Dot d -> string_of_typed_attribute_name_wt "." d
  (* | F_Dot d -> string_of_typed_attribute_name_wt "_" d *)
  | F_Expr (s, [f1; f2]) -> "("^(string_of_funterm f1)^") "^(string_of_symb s)^" ("^(string_of_funterm f2)^")"
  | F_Expr (s, [f]) -> (string_of_symb s)^" ("^(string_of_funterm f)^")"
  | F_Expr _ -> failwith "Wrong number of arguments in funterm"

let type_of_funterm = function
  | F_Constant v ->
     (match v with
        (* We consider NULL values as untyped, and put them into bool values *)
        | VNull -> TBool
        | VString _ -> TString
        | VInt _ -> TInt
        | VBool _ -> TBool)
  | F_Dot (_, t) -> t
  | F_Expr (s, _) ->
     if s = "+" || s = "-" || s = "*" || s = "/"
     then TInt
     else failwith("Unknown return type of symbol "^s)


(* Expressions and aggregates *)
type aggterm =
  (* Some expression *)
  | A_Expr of funterm
  (* An aggregate applied to an expression, e.g. [max(t1.c1+t2.c2)] *)
  | A_agg of aggregate * funterm
  (* A function applied to aggregates and expressions,
       e.g. [count( * )+4] *)
  | A_fun of symb * aggterm list

let rec string_of_aggterm = function
  | A_Expr f -> string_of_funterm f
  | A_agg (a, f) -> (string_of_aggregate a)^"("^(string_of_funterm f)^")"
  | A_fun (s, [a1; a2]) -> "("^(string_of_aggterm a1)^") "^(string_of_symb s)^" ("^(string_of_aggterm a2)^")"
  | A_fun (s, [a]) -> (string_of_symb s)^" ("^(string_of_aggterm a)^")"
  | A_fun _ -> failwith "Wrong number of arguments in aggterm"

let type_of_aggterm = function
  | A_Expr e -> type_of_funterm e
  | A_agg (agg, _) ->
     if agg = "COUNT" || agg = "AVG" || agg = "MIN" || agg = "MAX" || agg = "SUM"
     then TInt
     else failwith("Unknown return type of aggregate symbol "^agg)
  | A_fun (s, _) ->
     if s = "+" || s = "-" || s = "*" || s = "/"
     then TInt
     else failwith("Unknown return type of symbol "^s)


(* SELECT clause with attributes *)
type select =
  (* Renamed select clause, e.g. [t.c as c'], [sum(t.c) as sum] *)
  | Select_As of aggterm * typed_aname

let string_of_select = function
  | Select_As (a, ta) -> (string_of_aggterm a)^" AS "^(string_of_typed_aname_wt ta)

let string_of_select_wr = function
  | Select_As (a, _) -> string_of_aggterm a


(* SELECT clause *)
type select_item =
  (* All the columns, [*] *)
  | Select_Star
  (* List of columns, eg [t1.c1 as c1', t2.c2 as c2', ...] *)
  | Select_List of select list

let string_of_select_item = function
  | Select_Star -> "*"
  | Select_List sl -> fold_and_add_comma string_of_select sl

let string_of_select_item_wr = function
  | Select_Star -> "*"
  | Select_List sl -> fold_and_add_comma string_of_select_wr sl


(* WHERE and HAVING clauses *)
type 'query sql_formula =
  (* Connective between two formulae *)
  | Sql_Conj of and_or * 'query sql_formula * 'query sql_formula
  (* Negation of a formula *)
  | Sql_Not of 'query sql_formula
  (* The true formula *)
  | Sql_True
  (* Some predicate applied to expressions,
       e.g. [t1.c1 <= (t2.c2+t3.c3)] *)
  | Sql_Pred of predicate * aggterm list
  (* Some quantified predicate,
       e.g. [t1.c1-t2.c2 >= ALL (SELECT c3 FROM t3)] *)
      (* Sql_Quant, ForAll, >=, [.c1 - .c2] , (SELECT c3 FROM t3)
         fun x => \forall t \in t3, x.c1 - x.c2 >= t.c3 *)
  | Sql_Quant of quantifier * predicate * aggterm list * 'query
  (* IN clause, e.g. [t1.c1, t1.c2 IN (SELECT c3 FROM t2)] *)
  | Sql_In of select_item * 'query
  (* EXISTS clause, e.g. [EXISTS (SELECT c3 FROM t2)] *)
  | Sql_Exists of 'query

let rec string_of_sql_formula string_of_query = function
  | Sql_Conj (c, f1, f2) -> "("^(string_of_sql_formula string_of_query f1)^") "^(string_of_and_or c)^" ("^(string_of_sql_formula string_of_query f2)^")"
  | Sql_Not f -> "NOT ("^(string_of_sql_formula string_of_query f)^")"
  | Sql_True -> "TRUE"
  | Sql_Pred (p, [a1; a2]) -> "("^(string_of_aggterm a1)^") "^(string_of_predicate p)^" ("^(string_of_aggterm a2)^")"
  | Sql_Pred _ -> failwith "Wrong number of arguments in formula"
  | Sql_Quant (qu, p, [a], q) -> "("^(string_of_aggterm a)^") "^(string_of_predicate p)^" "^(string_of_quantifier qu)^" ("^(string_of_query q)^")"
  | Sql_Quant _ -> failwith "Wrong number of arguments in formula"
  | Sql_In (s, q) -> (string_of_select_item_wr s)^" IN ("^(string_of_query q)^")"
  | Sql_Exists q -> "EXISTS ("^(string_of_query q)^")"
