(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

open Utils
open Basics


(* ren  x (as y(a1,a2,...)) *)
(* the list may be empty *)
type var_ren = aname * (relname * (aname list)) option

(* attribute_name_ren (t.) x (as y) *)
type attribute_name_ren = attribute_name * aname option

(* Expressions *)
(* We have symbols only in aggterms to avoid grammar conflicts *)
type funterm =
  (* A constant, e.g. [3] *)
  | F_Constant of value
  (* Some column, eg [table.col] *)
  | F_Dot of attribute_name

type aggregate_term =
    (* Some expression *)
    | A_Fun of funterm
    (* *)
    | A_Exp of symb * (aggregate_term list)
    (* An aggregate applied to an expression, e.g. [max(t1.c1+t2.c2)]
       The expression is optional (e.g. for count( * )) *)
    | A_Agg of aggregate * (aggregate_term option)

type aggregate_ren = aggregate_term * aname option

type select_type =
  | Distinct
  | All

type select_item =
  | Select_star
  | Select_list of aggregate_ren list

type order_type =
  | Asc
  | Desc

type sql_query =
  | Sql_select of
      select_type
      * select_item                 (* SELECT *)
      * from_item                   (* FROM *)
      * sql_formula option          (* WHERE *)
      * grpby_item option           (* GROUP BY + HAVING *)
  | Sql_table of relname
  | Sql_set of set_op * sql_query * sql_query
and sql_formula =
  (* Connective between two formulae *)
  | Sql_Conj of and_or * sql_formula * sql_formula
  (* Negation of a formula *)
  | Sql_Not of sql_formula
  (* Other formulae *)
(*  | Sql_Atom of sql_atom
and sql_atom = *)
  (* The true formula *)
  | Sql_True
  (* Some predicate applied to expressions,
       e.g. [t1.c1 <= (t2.c2+t3.c3)] *)
  | Sql_Pred of predicate * aggregate_term * aggregate_term
  (* Some predicate applied to queries,
       e.g. [t1.c1 <= (select max(a) from t)] *)
  | Sql_PredQ of predicate * aggregate_term * sql_query
  (* Some quantified predicate,
       e.g. [t1.c1-t2.c2 >= ALL (SELECT c3 FROM t3)] *)
  (* Sql_Quant, ForAll, >=, [.c1 - .c2] , (SELECT c3 FROM t3)
     fun x => \forall t \in t3, x.c1 - x.c2 >= t.c3 *)
  | Sql_Quant of quantifier * predicate * aggregate_term list * sql_query
  (* IN clause, e.g. [t1.c1, t1.c2 IN (SELECT c3 FROM t2)] *)
  | Sql_In of select_item * sql_query
  (* EXISTS clause, e.g. [EXISTS (select * from t where a = b)] *)
  | Sql_Nonempty of sql_query
and from_item =
  from_atom list
and from_atom =
  | Basic of var_ren
  | Subquery of sql_query
  | Renquery of sql_query * aname * (aname list)
and having_item = sql_formula
and grpby_item = (aggregate_term list) * (having_item option) * (order_by option)
and order_by = (((attribute_name list) * (order_type option)) list)

type ren_query =  sql_query * aname

let string_of_order_type = function
  | Asc -> "ASC"
  | Desc -> "DESC"

let string_of_select_type = function
  | Distinct -> "DISTINCT "
  | All -> ""

let string_of_ren = function
  | v, None -> v
  | v1, Some(v2,[]) -> v1 ^ " AS " ^ v2
  | v1, Some(v2,l) -> v1 ^ " AS " ^ v2 ^ "(" ^ (fold_and_add_comma (fun x -> x) l) ^ ")"

let string_of_attribute_name_ren = function
  | v, None -> string_of_attribute_name "." v
  | v1, Some(v2) -> (string_of_attribute_name "." v1) ^ " AS " ^ v2

let string_of_funterm = function
  | F_Constant(c) ->  string_of_value c
  | F_Dot(v) -> string_of_attribute_name "." v

let string_of_funterm_ren = function
  | ft, None -> string_of_funterm ft
  | ft, Some(v) -> (string_of_funterm ft) ^ " AS " ^ v

let rec string_of_aggregate_term_par = function
  | A_Fun(ft) -> string_of_funterm ft
  | A_Exp(s, [at]) -> "("^(string_of_symb s)^" "^(string_of_aggregate_term_par at)^")"
  | A_Exp(s, [at1; at2]) -> "(" ^ (string_of_aggregate_term_par at1)^ " " ^ (string_of_symb s) ^ " " ^ (string_of_aggregate_term_par at2) ^ ")"
  | A_Exp _ -> failwith "Only unary and binary symbols are supported"
  | A_Agg(aggf, Some at) -> "(" ^ aggf ^ "(" ^ (string_of_aggregate_term_par at) ^ ")" ^ ")"
  | A_Agg(aggf, None) -> "(" ^ aggf ^ "(*)" ^ ")"

let rec string_of_aggregate_term = function
  | A_Fun(ft) -> string_of_funterm ft
  | A_Exp(s, [at]) -> (string_of_symb s)^" "^(string_of_aggregate_term at)
  | A_Exp(s, [at1; at2]) -> (string_of_aggregate_term at1)^ " " ^ (string_of_symb s) ^ " " ^ (string_of_aggregate_term at2)
  | A_Exp _ -> failwith "Only unary and binary symbols are supported"
  | A_Agg(aggf, Some at) -> aggf ^ "(" ^ (string_of_aggregate_term at) ^ ")"
  | A_Agg(aggf, None) -> aggf ^ "(*)"

let string_of_aggregate_ren = function
  | at, None -> string_of_aggregate_term at
  | at, Some(v) -> (string_of_aggregate_term at) ^ " AS " ^ v

let string_of_funterm_par = function
  | F_Constant(c) ->  "(" ^ (string_of_value c) ^ ")"
  | F_Dot(v) -> "(" ^ (string_of_attribute_name "." v) ^ ")"

let string_of_sItem = function
  | Select_star -> "*"
  | Select_list(l) -> fold_and_add_comma (string_of_aggregate_ren) l

let rec string_of_where_item = function
  | None -> ""
  | Some (f) -> " WHERE " ^ (string_of_sqlFormula f)

and string_of_sqlFormula = function
| Sql_True -> "TRUE"
| Sql_Pred(p, t1, t2) -> (string_of_aggregate_term t1) ^ " " ^ p ^ " " ^ (string_of_aggregate_term t2)
| Sql_PredQ(p, t1, e2) -> (string_of_aggregate_term t1) ^ " " ^ p ^ " (" ^ (string_of_sql_query e2) ^ ")"
| Sql_Quant(quant, pred, aggL, query) -> begin match aggL with
    | [x] -> (string_of_aggregate_term x) ^ " " ^ pred ^ " " ^ (string_of_quantifier quant) ^ " (" ^ string_of_sql_query query ^")"
    | _ -> assert false (* Non-binary pred. not supported *)
  end
| Sql_In(i, q) -> (string_of_sItem i) ^ " IN (" ^ (string_of_sql_query q) ^ ")"
| Sql_Nonempty q -> "EXISTS (" ^ (string_of_sql_query q) ^ ")"

(*and string_of_sqlFormula = function*)

  | Sql_Conj(conj, lf, rf) ->
    let op = match conj with | And_F -> " AND " | Or_F -> " OR " in
    "(" ^ (string_of_sqlFormula lf) ^ ")" ^ op ^ "(" ^ (string_of_sqlFormula rf) ^ ")"
  | Sql_Not(f) -> "NOT (" ^ (string_of_sqlFormula f) ^ ")"
(*  | Sql_Atom(a) -> string_of_sql_atom a *)

and string_of_from_atom = function
  | Basic(el) -> string_of_ren el
  | Subquery(q) -> "(" ^ (string_of_sql_query q)  ^ ")"
  | Renquery(q,v,[]) -> "(" ^ (string_of_sql_query q) ^ ") AS " ^ v
  | Renquery(q,v,l) -> "(" ^ (string_of_sql_query q) ^ ") AS " ^ v ^ "(" ^ (fold_and_add_comma (fun x -> x) l) ^ ")"
and string_of_from_item fitem =
  " FROM " ^ (fold_and_add_comma (string_of_from_atom) fitem)
and string_of_grpby = function
  | None -> ""
  | Some(col, having, ord) -> " GROUP BY " ^ (fold_and_add_comma (string_of_aggregate_term) col) ^ (string_of_having having) ^ (string_of_order_by ord)
and string_of_having = function
  | None -> ""
  | Some(f) -> " HAVING " ^ (string_of_sqlFormula f)
and string_of_order_by = function
  | None -> ""
  | Some(l) -> " ORDER BY " ^ (fold_and_add_comma (string_of_order_by_atom) l)
and string_of_order_by_atom = function
  | l, None -> (fold_and_add_comma (string_of_attribute_name ".") l)
  | l, Some(ord) -> (fold_and_add_comma (string_of_attribute_name ".") l) ^ " " ^ (string_of_order_type ord)
and string_of_sql_query = function
  | Sql_select (sType, sItem, fItem, wItem, gItem)
    ->
    "SELECT " ^ (string_of_select_type sType) ^ (string_of_sItem sItem) ^
    (string_of_from_item fItem) ^ (string_of_where_item wItem) ^ (string_of_grpby gItem)
  | Sql_table t -> "TABLE " ^ (string_of_relname t)
  | Sql_set(op, q1, q2) -> "(" ^ (string_of_sql_query q1) ^ ") " ^ (string_of_set_op op) ^ " (" ^ (string_of_sql_query q2) ^ ")"


(* CREATE TABLE queries *)

type sql_create =
  relname *                     (* name of the table *)
    (
      (typed_aname list) *       (* list of columns *)
      (
        (aname list) *       (* list of primary keys *)
        ((relname * ((aname list) * (aname list))) list) (* list of foreign keys *)
      )
    )

let string_of_sql_create (a:sql_create) =
  let (t, (cols, (lpk, lfk))) = a in
  "CREATE TABLE "^
    (string_of_relname t)^
    " ("^
    (fold_and_add_comma string_of_typed_aname cols)^
    (match lpk with
       | [] -> ""
       | _ -> ", PRIMARY KEY ("^(fold_and_add_comma string_of_aname lpk)^")"
    )^
    (if List.length lfk > 0 then ", " else "")^
    (fold_and_add_comma (
         fun (r, (k1, k2)) -> "FOREIGN KEY ("^(fold_and_add_comma string_of_aname k1)^") REFERENCES "^(string_of_relname r)^" ("^(fold_and_add_comma string_of_aname k2)^")"
       ) lfk
    )^
    ");"


(* INSERT queries *)

type sql_insert = relname * ((aname list) option) * ((value list) list)

let string_of_sql_insert (i:sql_insert) =
  let (t, cols, vals) = i in
  let t' = string_of_relname t in
  let cols' = match cols with
      | Some cols -> " (" ^ (fold_and_add_comma string_of_aname cols) ^ ")"
      | None -> ""
  in
  let vals' = fold_and_add_comma (fun vs -> "(" ^ (fold_and_add_comma string_of_value vs) ^ ")") vals in
  "INSERT INTO " ^ t' ^ cols' ^ " VALUES " ^ vals' ^ ";"


(* A file contains CREATE TABLE, INSERT or SELECT queries *)
type sql_line =
  | SQL_Query of sql_query
  | SQL_Create of sql_create
  | SQL_Insert of sql_insert

let string_of_sql_line = function
  | SQL_Query q -> string_of_sql_query q
  | SQL_Create c -> string_of_sql_create c
  | SQL_Insert i -> string_of_sql_insert i

let empty_query = Sql_select (All, Select_list ([]), [], None, None)

let agg_term_of_col_name colName = A_Fun(F_Dot(None, colName))

let agg_term_of_attribute_name (tblName, colName) = A_Fun(F_Dot(tblName, colName))

let agg_term_of_agg_ren (agg, name) =
  match name with
    None -> agg
  | Some colName -> (agg_term_of_col_name colName)
