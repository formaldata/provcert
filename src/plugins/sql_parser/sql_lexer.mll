(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

{
  open Sql_parser

  exception Error of string
}

let alpha   = ['a'-'z' 'A'-'Z']
let blank   = [' ' '\t']
let digit   = ['0'-'9']
let int     = '-'? digit+
let newline = ['\n' '\r']
let var     = alpha (alpha|digit|'_')*

(* Rule for a single line, terminated with '\n' or eof.
   Returns an optional string (the line that was found) and a boolean (false if eof was reached). *)

rule line = parse
  | ([^'\n']* '\n') as line
      (* Normal case: one line, no eof. *)
      { Some line, true }
  | eof
      (* Normal case: no data, eof. *)
      { None, false }
  | ([^'\n']+ as line) eof
      (* Special case: some data but missing '\n', then eof.
         Consider this as the last line, and add the missing '\n'. *)
      { Some (line ^ "\n"), false }

(* This rule analyzes a single line and turns it into a stream of
   tokens. *)
and token = parse
  | blank   { token lexbuf }
  | newline { EOL }

  | '^'   { CFLEX }
  | ','   { COMMA }
  | "."   { DOT }
  | '='   { EQ }
  | ">="  { GEQ }
  | '>'   { GT }
  | "<="  { LEQ }
  | '('   { LPAR }
  | '<'   { LT }
  | '-'   { MINUS }
  | "<>"  { NEQ }
  | '%'   { PERCENT }
  | '+'   { PLUS }
  | ')'   { RPAR }
  | ';'   { SEMICOLON }
  | '/'   { SLASH }
  | '*'   { STAR }

  | "all"          { ALL }
  | "and"          { AND }
  | "any"          { ANY }
  | "as"           { AS }
  | "asc"          { ASC }
  | "avg"          { AVG }
  | "count"        { COUNT }
  | "create"       { CREATE }
  | "desc"         { DESC }
  | "distinct"     { DISTINCT }
  | "exists"       { EXISTS }
  | "from"         { FROM }
  | "group by"     { GRPBY }
  | "having"       { HAVING }
  | "in"           { IN }
  | "like"         { LIKE }
  | "max"          { MAX }
  | "min"          { MIN }
  | "not"          { NOT }
  | "or"           { OR }
  | "order by"     { ORDERBY }
  | "select"       { SELECT }
  | "sum"          { SUM }
  | "table"        { TABLE }
  | "true"         { TRUE }
  | "false"        { FALSE }
  | "where"        { WHERE }
  | "union"        { UNION }
  | "intersect"    { INTERSECT }
  | "except"       { EXCEPT }
  | "null"         { NULL }
  | "insert"       { INSERT }
  | "into"         { INTO }
  | "values"       { VALUES }
  | "integer"
  | "int"          { INTEGER }
  | "text"         { TEXT }
  | "varchar"      { VARCHAR }
  | "boolean"
  | "bool"         { BOOLEAN }
  | "primary"      { PRIMARY }
  | "foreign"      { FOREIGN }
  | "key"          { KEY }
  | "references"   { REFERENCES }

  | "--"                    { comment lexbuf }

  | "'"                     { let buffer = Buffer.create 10 in STRING (string buffer lexbuf) }

  | int as i    { INT (int_of_string i) }
  | var as v    { VAR v }


and comment = parse
| newline  { EOL }
| _        { comment lexbuf }


and string buffer = parse
| "'"       { Buffer.contents buffer }
| "\\t"     { Buffer.add_char buffer '\t'; string buffer lexbuf }
| "\\n"     { Buffer.add_char buffer '\n'; string buffer lexbuf }
| "\\" "'"  { Buffer.add_char buffer '\''; string buffer lexbuf }
| "\\" "\\" { Buffer.add_char buffer '\\'; string buffer lexbuf }
| "\n"      { raise (Error "String non terminated") }
| _ as char { Buffer.add_char buffer char; string buffer lexbuf }
