(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

DECLARE PLUGIN "Plugins_datacert"

open Stdarg

VERNAC COMMAND EXTEND Vernac_sqlparser CLASSIFIED AS QUERY
| [ "Parse_sql" string(query) ident(res) ] -> [ Sqlparser.parse_query_to_coq query res ]
END
