(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(** SQLCoq AST **)

open Utils
open Basics
open Sqlcontext
open Coq_sql_algebra


let string_of_attribute_name = string_of_attribute_name "_"


(* (Possibly empty) GROUP BY clause *)
type group_by =
  (* A list of grouping expressions, eg [t1.c1, t2.c2 + t3.c3] *)
  | Group_By of funterm list
  (* Empty *)
  | Group_Fine

let string_of_group_by = function
  | Group_By [] -> ""
  | Group_By fl -> " GROUP BY "^(fold_and_add_comma string_of_funterm fl)
  | Group_Fine -> ""


(* The renaming of an attribute *)
type att_renaming =
  (* Renaming, eg [table.col as col'] *)
  | Att_As of typed_attribute_name * typed_attribute_name

let string_of_att_renaming = function
  | Att_As (ta1, ta2) -> (string_of_typed_attribute_name_wt_wrn ta1)^" AS "^(string_of_typed_attribute_name_wt_wrn ta2)


(* (Possibly empty) list of renamed attributes *)
type att_renaming_item =
  (* No renaming *)
  | Att_Ren_Star
  (* A list of renamings *)
  | Att_Ren_List of att_renaming list


(* Queries *)
type sql_query =
  (* The name of a table *)
  | Sql_Table of relname
  (* Set operation between two queries *)
  | Sql_Set of set_op * sql_query * sql_query
  (* SELECT FROM WHERE GROUP-BY HAVING *)
  | Sql_Select of
      select_item               (* SELECT *)
      * from_item list          (* FROM *)
      * sql_query sql_formula   (* WHERE *)
      * group_by                (* GROUP BY *)
      * sql_query sql_formula   (* HAVING *)

(* FROM clause : query + renaming of the attributes (either no renaming
   or renaming everything) *)
and from_item =
  | From_Item of sql_query * att_renaming_item


(** Translation from our AST to SQLCoq **)

(* Original schema:
   - to each name of table is associated the list of attribute names
   - indices are stored
*)
let tables : (relname, (typed_aname list)) Hashtbl.t = Hashtbl.create 17
type index_name = string
let pk_to_index_name (r:relname) : index_name = r^"_pkey"
let indices : (index_name, (aname list)) Hashtbl.t = Hashtbl.create 17


(* In addition to a standard SQL context (see common/sqlcontext.ml), we
   need a context of aggterms *)
type context_aggterm = aname * (aggterm * coltype)
type context_aggterms = context_aggterm list
type context = context_tables * context_aggterms
let empty_context : context = ([], [])
let init_context : unit -> context =
  fun () -> (Hashtbl.fold (fun t atts acc -> (t, atts)::acc) tables [], [])

let context_aggterm_get_type (an : aname) (ca : context_aggterm) =
  let (an', (_, t)) = ca in
  if an' = an then Some t else None
let context_aggterms_get_type (an : aname) = list_find_first (context_aggterm_get_type an)

let context_get_type (an : attribute_name) =
  pair_find_first (fun ctxt -> context_tables_get_type ctxt an) (context_aggterms_get_type (snd an))
let context_get_type_fail an ctxt =
  find_or_fail ("Type of attribute "^(snd an)^" is unknown") (context_get_type an ctxt)


(* Sort of a SQLCoq query *)
let rec sql_sort (ctxt : context_tables) : sql_query -> typed_attribute_name list = function
  | Sql_Table tbl -> List.map (fun (a,t) -> ((Some tbl, a), t)) (List.assoc tbl ctxt)
  | Sql_Set (_, sq1, _) -> sql_sort ctxt sq1
  | Sql_Select (s, f, _, _, _) ->
     match s with
       | Select_Star -> List.flatten (List.map (from_item_sort ctxt) f)
       | Select_List la ->
          List.map (function Select_As (_, (a, t)) -> ((None, a), t)) la

and from_item_sort ctxt = function
  | From_Item (sq, Att_Ren_Star) -> sql_sort ctxt sq
  | From_Item (_, Att_Ren_List la) ->
     List.map (function Att_As (_, a) -> a) la


(* Top projection of a SQLCoq query *)
type top_proj = (typed_attribute_name * Sql_ast.aggregate_term) list

let rec merge_proj (l1 : top_proj) (l2 : top_proj) : top_proj =
  match l1 with
    | [] -> l2
    | an::l1 ->
       if List.mem an l2 then
         merge_proj l1 l2
       else if List.mem (snd an) (List.map snd l2) then
         failwith "Impossible to find a coherent top-level renaming"
       else
         an::(merge_proj l1 l2)

let apply_proj_typed_attribute_name proj ((an, t) as ant) =
  match List.assoc_opt ant proj with
    | Some agg -> agg
    | None -> Sql_ast.A_Fun (Sql_ast.F_Dot an)

let rec apply_proj_funterm proj = function
  | F_Constant v -> Sql_ast.A_Fun (Sql_ast.F_Constant v)
  | F_Dot an -> apply_proj_typed_attribute_name proj an
  | F_Expr (s, l) -> Sql_ast.A_Exp (s, List.map (apply_proj_funterm proj) l)

let rec apply_proj_aggterm proj = function
  | A_Expr f -> apply_proj_funterm proj f
  | A_agg (a, f) -> Sql_ast.A_Agg (a, Some (apply_proj_funterm proj f))
  | A_fun (s, l) -> Sql_ast.A_Exp (s, List.map (apply_proj_aggterm proj) l)

let rec sql_top_proj (ctxt : context_tables) : sql_query -> top_proj = function
  | Sql_Table tbl -> List.map (fun (a,t) -> (((Some tbl, a), t), Sql_ast.A_Fun (Sql_ast.F_Dot (Some tbl, a)))) (List.assoc tbl ctxt)
  | Sql_Set (_, sq1, sq2) -> merge_proj (sql_top_proj ctxt sq1) (sql_top_proj ctxt sq2)
  | Sql_Select (s, f, _, _, _) ->
     let proj_from = List.flatten (List.map (from_item_top_proj ctxt) f) in
     match s with
       | Select_Star -> proj_from
       | Select_List la ->
          List.map (function Select_As (agg, (an, t)) -> (((None, an), t), apply_proj_aggterm proj_from agg)) la

and from_item_top_proj ctxt = function
  | From_Item (sq, Att_Ren_Star) -> sql_top_proj ctxt sq
  | From_Item (sq, Att_Ren_List la) ->
     let proj_sq = sql_top_proj ctxt sq in
     List.map (function Att_As (a1, a2) -> (a2, apply_proj_typed_attribute_name proj_sq a1)) la


(* Translation *)
let value_to_coq : value -> value = fun v -> v

let funterm_to_coq ctxt : Sql_ast.funterm -> funterm = function
  | Sql_ast.F_Constant v -> F_Constant (value_to_coq v)
  | Sql_ast.F_Dot (rn, an) ->
     let rn =
       match rn with
         | Some rn -> Some rn
         | None -> context_tables_find an (fst ctxt)
     in
     let an = (rn, an) in
     let t = context_get_type_fail an ctxt in
     F_Dot (an, t)

let rec funterm_of_aggterm : aggterm -> funterm = function
  | A_Expr e -> e
  | A_fun (s,l) -> F_Expr (s, List.map funterm_of_aggterm l)
  | _ -> failwith "Aggregates cannot be nested"

(* aggterm_renamings used to associate a name to an aggterm; it is now
   the contrary to match SQLCoq well-formed queries *)
let rec aggregate_term_to_coq (ctxt : context) (att : typed_attribute_name) : Sql_ast.aggregate_term -> aggterm =
  fun a ->
  match a with
    (* Particular case: GBY and HAVING clauses allow names given in SELECT, but not in SQLCoq: we have to find the aggregate *)
    | Sql_ast.A_Fun (Sql_ast.F_Dot (None, an)) when List.mem_assoc an (snd ctxt) ->
       fst (List.assoc an (snd ctxt))
    (* Standard cases *)
    | Sql_ast.A_Fun f -> A_Expr (funterm_to_coq ctxt f)
    | Sql_ast.A_Exp (s, l) ->
       (try
          let l = List.map (fun a -> funterm_of_aggterm (aggregate_term_to_coq ctxt att a)) l in
          A_Expr (F_Expr (s, l))
        with
          | _ ->
             let l = List.map (aggregate_term_to_coq ctxt att) l in
             A_fun (s, l)
       )
    | Sql_ast.A_Agg (agg, Some t) -> A_agg (agg, funterm_of_aggterm (aggregate_term_to_coq ctxt att t))
    (* When there is no expression (e.g. for count( * )),
       we put one of the attributes of the sort of the "from" part *)
    | Sql_ast.A_Agg (agg, None) -> A_agg (agg, F_Dot att)

let aggregate_ren_to_coq ctxt (att : typed_attribute_name) : Sql_ast.aggregate_ren -> select * context_aggterm =
  fun (agg,n) ->
  let agg' = aggregate_term_to_coq ctxt att agg in
  let n' = match n with
      | Some n -> n
      | None ->
         (match agg' with
            | A_Expr (F_Dot (an, _)) -> string_of_attribute_name an
            | _ -> fresh_att ()
         )
  in
  let t' = type_of_aggterm agg' in
  (Select_As (agg', (n', t')), (n', (agg', t')))

let select_item_to_coq ctxt (att : typed_attribute_name) : Sql_ast.select_item -> select_item * context_aggterms = function
  | Sql_ast.Select_star -> (Select_Star, [])
  | Sql_ast.Select_list l ->
     let l' = List.map (aggregate_ren_to_coq ctxt att) l in
     let ressel = List.map fst l' in
     let resctxt = List.map snd l' in
     (Select_List ressel, resctxt)

let var_ren_to_coq ctxt : Sql_ast.var_ren -> (relname * att_renaming_item * context_table) =
  fun (v, n) ->
  let atts = List.assoc v ctxt in
  match n with
    | Some (v', []) ->
       (v, Att_Ren_List (List.map (fun (x, ty) -> Att_As (((Some v, x), ty), ((Some v', x), ty))) atts), (v', atts))
    | Some (v', l) ->
       (v, Att_Ren_List (List.map2 (fun (x, ty) x' -> Att_As (((Some v, x), ty), ((Some v', x'), ty))) atts l), (v', List.map2 (fun (_, t) x' -> (x', t)) atts l))
    | None ->
       (v, Att_Ren_Star, (v, atts))

(* Checks if an aggterm actually contains an aggregate *)
let rec contains_agg : aggterm -> bool = function
  | A_Expr _ -> false
  | A_agg _ -> true
  | A_fun (_,l) -> List.exists contains_agg l

(* ctxt: the table and attribute names that can be used
   - for SELECT and WHERE: comes from FROM
   - for GBY and HAVING: comes from FROM or SELECT
   Starts with the schema
*)
let rec sql_query_to_coq (ctxt : context) : Sql_ast.sql_query -> sql_query =
  fun qu ->
  match qu with
  | Sql_ast.Sql_select (ty, select, from, where, group_by_having) ->
     (match ty with
        | Sql_ast.Distinct -> failwith "SQLCoq does not handle DISTINCT"
        | Sql_ast.All ->
           let (grps, hav) =
             match group_by_having with
               | Some (_, _, Some _) -> failwith "SQLCoq does not handle ORDER BY"
               | Some (g, h, None) -> (Some g, h)
               | None -> (None, None) in
           let (from', ctxt') = from_item_to_coq ctxt from in
           let ctxt = (ctxt'@(fst ctxt), snd ctxt) in
           let att = List.nth (from_item_sort (fst ctxt) (List.nth from' 0)) 0 in
           let where' = match where with Some w -> sql_formula_to_coq ctxt att w | None -> Sql_True in
           let (select', ctxt') = select_item_to_coq ctxt att select in
           let ctxt = (fst ctxt, ctxt'@(snd ctxt)) in
           let group_by' =
             match grps with
               | Some g -> Group_By (List.map (fun a -> funterm_of_aggterm (aggregate_term_to_coq ctxt att a)) g)
               | None ->
                  (* If the SELECT clause contains an aggterm, return the
                     partition that contains everything, otherwise the
                     fine partition *)
                  match select' with
                    | Select_List l when List.exists (fun (Select_As (agg, _)) -> contains_agg agg) l -> Group_By []
                    | _ -> Group_Fine
           in
           let having' = match hav with Some h -> sql_formula_to_coq ctxt att h | None -> Sql_True in
           Sql_Select (select', from', where', group_by', having')
     )
  | Sql_ast.Sql_table t -> Sql_Table t
  | Sql_ast.Sql_set(op, q1, q2) ->
     let q1' = sql_query_to_coq ctxt q1 in
     let q2' = sql_query_to_coq ctxt q2 in
     let s1 = sql_sort (fst ctxt) q1' in
     let s2 = sql_sort (fst ctxt) q2' in
     let q2'' = Sql_Select(
                    Select_List (List.map2 (fun (a1,t1) at2 -> Select_As (A_Expr (F_Dot at2), (string_of_attribute_name a1, t1))) s1 s2),
                    [From_Item (q2', Att_Ren_Star)],
                    Sql_True, Group_Fine, Sql_True)
     in
     Sql_Set(op, q1', q2'')

and sql_formula_to_coq ctxt (att : typed_attribute_name) : Sql_ast.sql_formula -> sql_query sql_formula = function
  | Sql_ast.Sql_Conj (ao, f1, f2) -> Sql_Conj (ao, sql_formula_to_coq ctxt att f1, sql_formula_to_coq ctxt att f2)
  | Sql_ast.Sql_Not f -> Sql_Not (sql_formula_to_coq ctxt att f)
  | Sql_ast.Sql_True -> Sql_True
  | Sql_ast.Sql_Pred (p, a1, a2) ->
     Sql_Pred (p, [aggregate_term_to_coq ctxt att a1; aggregate_term_to_coq ctxt att a2])
  | Sql_ast.Sql_PredQ _ ->
     failwith "SQLCoq does not handle predicates applied to queries"
  | Sql_ast.Sql_Quant (q, p, l, qu) ->
     Sql_Quant (q, p, List.map (aggregate_term_to_coq ctxt att) l, sql_query_to_coq ctxt qu)
  | Sql_ast.Sql_In (s,qu) ->
     (let (sel, _) = select_item_to_coq ctxt att s in
      match sel with
        | Select_Star -> failwith "IN cannot be preceded by *"
        | Select_List l ->
           let q = sql_query_to_coq ctxt qu in
           let l' = sql_sort (fst ctxt) q in
           Sql_In (Select_List (List.map2 (fun (Select_As (a,_)) (n,t) -> Select_As (a, (string_of_attribute_name n, t))) l l'), q)
     )
  | Sql_ast.Sql_Nonempty qu ->
     Sql_Exists (sql_query_to_coq ctxt qu)

and from_item_to_coq ctxt : Sql_ast.from_item -> (from_item list * context_tables) =
  fun l ->
  let l' = List.map (from_atom_to_coq ctxt) l in
  let resl = List.map fst l' in
  let resctxt = List.map snd l' in
  (resl, resctxt)

and from_atom_to_coq ctxt : Sql_ast.from_atom -> (from_item * context_table) = function
  | Sql_ast.Basic v ->
     let (v, n, ctxtt) = var_ren_to_coq (fst ctxt) v in
     (From_Item (Sql_Table v, n), ctxtt)
  | Sql_ast.Subquery q ->
     failwith "Nested queries in FROM must be named"
     (* (From_Item (sql_query_to_coq ctxt q, Att_Ren_Star), None) *)
  | Sql_ast.Renquery (q,n,l) ->
     let q' = sql_query_to_coq ctxt q in
     let s = sql_sort (fst ctxt) q' in
     (match l with
        | [] ->
           let ren' = Att_Ren_List (List.map (fun ((n', a), ty) -> Att_As (((n', a), ty), ((Some n, a), ty))) s) in
           (From_Item (q', ren'), (n, (List.map (fun ((_, an), t) -> (an, t)) s)))
        | _ ->
           let ren' = Att_Ren_List (List.map2 (fun (x, ty) x' -> Att_As ((x, ty), ((Some n, x'), ty))) s l) in
           (From_Item (q', ren'), (n, List.map2 (fun (_, t) x' -> (x', t)) s l))
     )


let select context q =
  clear ();
  try
    sql_query_to_coq context q
  with
    | Failure s -> CErrors.user_err (Pp.str ("This query is not supported for the following reason: "^s))


(** Pretty-printing of SQLCoq queries *)

let rec string_of_sql_query context = function
  | Sql_Table t -> "TABLE " ^ (string_of_relname t)
  | Sql_Set(op, q1, q2) -> "(" ^ (string_of_sql_query context q1) ^ ") " ^ (string_of_set_op op) ^ " (" ^ (string_of_sql_query context q2) ^ ")"
  | Sql_Select (sItem, fItems, wItem, gItem, hItem)
    ->
     let h =
       match hItem with
         | Sql_True -> ""
         | _ -> " HAVING "^(string_of_sql_formula (string_of_sql_query context) hItem)
     in
     "SELECT " ^ (string_of_select_item sItem) ^
       (string_of_from_items context fItems) ^ " WHERE " ^ (string_of_sql_formula (string_of_sql_query context) wItem) ^ (string_of_group_by gItem) ^ h

and string_of_from_items context fItems =
  " FROM " ^ (fold_and_add_comma (string_of_from_item context) fItems)

and string_of_from_item context = function
  | From_Item (q, r) -> "("^(string_of_sql_query context q)^") "^(string_of_att_renaming_item context q r)

(* WARNING: it assumes that renamings are in the correct order,
   nonempty, and fully characterized *)
and string_of_att_renaming_item context q arn =
  let find_relname = function
    | [] -> failwith "Unable to guess the new relation name"
    | (Att_As (((o, _), _), ((n, _), _)))::_ ->
       match (n, o) with
         | Some n, _ -> n
         | None, Some o -> o
         | None, None -> failwith "Unable to guess the new relation name"
  in
  let find_relname_query = function
    | Sql_Table t -> t
    | _ -> failwith "Unable to guess the new relation name"
  in
  match arn with
  | Att_Ren_Star ->
     let t = find_relname_query q in
     let s = sql_sort context q in
     t^"("^(fold_and_add_comma (string_of_typed_attribute_name_wt_wrn) s)^")"
  | Att_Ren_List l ->
     let n = find_relname l in
     n^"("^(fold_and_add_comma (fun (Att_As (_, (a, _))) -> string_of_attribute_name_wrn a) l)^")"


(** Translation of CREATE queries *)

let create_table ((t,(cols, (pkl, _))):Sql_ast.sql_create) =
  Hashtbl.add tables t cols;
  (match pkl with
     | [] -> ()
     | _ -> Hashtbl.add indices (pk_to_index_name t) pkl
  );
  let cols = List.map (fun (c,ty) -> ((Some t, c), ty)) cols in
  (t, cols)

let string_of_create_table (t,cols) =
  let atts = fold_and_add_comma (fun (d, ty) ->
                 (string_of_attribute_name_wrn d)^" "^(string_of_coltype ty)
               ) cols in
  "CREATE TABLE "^(string_of_relname t)^" ("^atts^");"


(** Translation of INSERT queries *)

let insert (t, cols, valss) =
  let full_cols = Hashtbl.find tables t in
  let cols = match cols with
      | Some cs -> List.map (fun c -> (c, List.assoc c full_cols)) cs
      | None -> full_cols
  in
  let cols = List.map (fun (a, ty) -> ((Some t, a), ty)) cols in
  (t, cols, valss)

let string_of_insert (t, cols, vals) =
  let t' = string_of_relname t in
  let cols' = " (" ^ (fold_and_add_comma (fun (d, _) -> string_of_attribute_name_wrn d) cols) ^ ")"in
  let vals' = fold_and_add_comma (fun vs -> "(" ^ (fold_and_add_comma string_of_value vs) ^ ")") vals in
  "INSERT INTO " ^ t' ^ cols' ^ " VALUES " ^ vals' ^ ";"
