(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(* Collecting Coq terms *)

open Utils
open Basics

(* Datacert terms *)
let datacert_sql_modules = [["Datacert";"data";"sql";"Sql"];
                        ["Datacert";"logic";"Formula"];
                        ["Datacert";"data";"proof_of_concept";"Values"];
                        ["Datacert";"data";"proof_of_concept";"GenericInstance"];
                        ["Datacert";"data";"proof_of_concept";"SqlSyntax"]]
let c_Group_By = gen_constant datacert_sql_modules "_Group_By"
let cAtt_Ren_List = gen_constant datacert_sql_modules "Att_Ren_List"
let catt_renaming = gen_constant datacert_sql_modules "att_renaming"
let cForall_F = gen_constant datacert_sql_modules "Forall_F"
let cExists_F = gen_constant datacert_sql_modules "Exists_F"
let c_Union = gen_constant datacert_sql_modules "_Union"
let c_Inter = gen_constant datacert_sql_modules "_Inter"
let c_Diff = gen_constant datacert_sql_modules "_Diff"
let cdb_state = gen_constant datacert_sql_modules "db_state"
let ccreate_table = gen_constant datacert_sql_modules "create_table"
let c_Select_Star = gen_constant datacert_sql_modules "_Select_Star"
let c_Select_List = gen_constant datacert_sql_modules "__Select_List"
let c_Group_Fine = gen_constant datacert_sql_modules "_Group_Fine"
let c_Att_As = gen_constant datacert_sql_modules "_Att_As"
let c_Att_Ren_Star = gen_constant datacert_sql_modules "_Att_Ren_Star"
let c_Sql_Table = gen_constant datacert_sql_modules "_Sql_Table"
let c_Sql_Select = gen_constant datacert_sql_modules "_Sql_Select"
let c_Sql_Set = gen_constant datacert_sql_modules "_Sql_Set"
let cMyDBS = gen_constant datacert_sql_modules "MyDBS"
let c_From_Item = gen_constant datacert_sql_modules "_From_Item"
let c_sql_from_item = gen_constant datacert_sql_modules "_sql_from_item"
let c_Sql_Conj = gen_constant datacert_sql_modules "_Sql_Conj"
let c_Sql_Not = gen_constant datacert_sql_modules "_Sql_Not"
(*let c_Sql_Atom = gen_constant datacert_sql_modules "_Sql_Atom"*)
let c_Sql_True = gen_constant datacert_sql_modules "_Sql_True"
let c_Sql_Pred = gen_constant datacert_sql_modules "_Sql_Pred"
let c_Sql_Quant = gen_constant datacert_sql_modules "_Sql_Quant"
let c_Sql_In = gen_constant datacert_sql_modules "_Sql_In"
let c_Sql_Exists = gen_constant datacert_sql_modules "_Sql_Exists"
let cSortedTuplesT = gen_constant datacert_sql_modules "SortedTuplesT"
let cTNull = gen_constant datacert_sql_modules "TNull"
let cmk_insert = gen_constant datacert_sql_modules "mk_insert"

let caggregate = gen_constant [["Datacert";"data";"proof_of_concept";"SqlSyntax"]] "aggregate"
let cOSymbol = gen_constant [["Datacert";"data";"proof_of_concept";"SqlSyntax"]] "OSymbol"
let cOAgg = gen_constant [["Datacert";"data";"proof_of_concept";"SqlSyntax"]] "OAgg"


let mk_typed_attribute_name = mk_typed_attribute_name true


(* CREATE TABLE queries *)

let rec mk_funterm = function
  | Coq_sql_algebra.F_Constant v -> mklApp c_F_Constant [|mk_value v|]
  | Coq_sql_algebra.F_Dot an -> mklApp c__Sql_Dot [|mk_typed_attribute_name an|]
  | Coq_sql_algebra.F_Expr (s,l) -> mklApp c_F_Expr [|mk_symb s (List.length l); mkList (Lazy.force c_funterm) mk_funterm l|]


let rec mk_aggterm = function
  | Coq_sql_algebra.A_Expr f -> mklApp c_A_Expr [|mk_funterm f|]
  | Coq_sql_algebra.A_agg (agg,f) -> mklApp c_A_agg [|mk_aggregate agg; mk_funterm f|]
  | Coq_sql_algebra.A_fun (s,l) -> mklApp c_A_fun [|mk_symb s (List.length l); mkList (Lazy.force c_aggterm) mk_aggterm l|]


let mk_select = function
  | Coq_sql_algebra.Select_As (agg, (an, ty)) -> mklApp c_Select_As [|mk_aggterm agg; mk_typed_attribute_name ((None, an), ty)|]

let mk_select_list l = mkList (Lazy.force c_select) mk_select l


let mk_select_item = function
  | Coq_sql_algebra.Select_Star -> Lazy.force c_Select_Star
  | Coq_sql_algebra.Select_List l -> mklApp c_Select_List [|mk_select_list l|]


let mk_create_table_query (t, cols) =
  let tablename = mk_tablename t in
  let attributes = mkList (Lazy.force cattribute) mk_typed_attribute_name cols in
  let body = mklApp ccreate_table [|Constr.mkRel 1; tablename; attributes|] in
  Constr.mkLambda (mkName "db", Lazy.force cdb_state, body)


(* INSERT queries *)

let mk_insert_query (t, cols, valss) =
  let atts = mkList (Lazy.force cattribute) mk_typed_attribute_name cols in
  let valss = mkList (mklApp clist [|Lazy.force cvalue|])
               (mkList (Lazy.force cvalue) mk_value)
               valss
  in
  let body = mklApp cmk_insert [|Constr.mkRel 1 (*db*); mk_tablename t; atts; valss|] in
  Constr.mkLambda (mkName "db", Lazy.force cdb_state, body)


(* SELECT queries *)

let mk_group_by = function
  | ToCoq.Group_By l -> mklApp c_Group_By [|mkList (Lazy.force c_funterm) mk_funterm l|]
  | ToCoq.Group_Fine -> Lazy.force c_Group_Fine

let mk_att_renaming = function
  | ToCoq.Att_As (a1,a2) -> mklApp c_Att_As [|mk_typed_attribute_name a1; mk_typed_attribute_name a2|]

let mk_att_renaming_item = function
  | ToCoq.Att_Ren_Star -> Lazy.force c_Att_Ren_Star
  | ToCoq.Att_Ren_List l -> mklApp cAtt_Ren_List [|Lazy.force cTNull; mkList (mklApp catt_renaming [|Lazy.force cTNull|]) mk_att_renaming l|]

let mk_quantifier = function
  | Forall_F -> Lazy.force cForall_F
  | Exists_F -> Lazy.force cExists_F

let mk_set_op = function
  | Union -> Lazy.force c_Union
  | Intersect -> Lazy.force c_Inter
  | Except -> Lazy.force c_Diff

let rec mk_sql_query = function
  | ToCoq.Sql_Table name -> mklApp c_Sql_Table [|mk_tablename name|]
  | ToCoq.Sql_Set (op, q1, q2) ->
     mklApp c_Sql_Set [|mk_set_op op; mk_sql_query q1; mk_sql_query q2|]
  | ToCoq.Sql_Select (select, from, where, group_by, having) ->
     mklApp c_Sql_Select [|mk_select_item select; mkList (Lazy.force c_sql_from_item) mk_from_item from; mk_sql_formula where; mk_group_by group_by; mk_sql_formula having|]

and mk_from_item = function
  | ToCoq.From_Item (q, an) -> mklApp c_From_Item [|mk_sql_query q; mk_att_renaming_item an|]

and mk_sql_formula = function
  | Coq_sql_algebra.Sql_Conj (ao, f1, f2) -> mklApp c_Sql_Conj [|mk_and_or ao; mk_sql_formula f1; mk_sql_formula f2|]
  | Coq_sql_algebra.Sql_Not f -> mklApp c_Sql_Not [|mk_sql_formula f|]
(*  | ToCoq.Sql_Atom at -> mklApp c_Sql_Atom [|mk_sql_atom at|]

and mk_sql_atom = function *)
  | Coq_sql_algebra.Sql_True -> Lazy.force c_Sql_True
  | Coq_sql_algebra.Sql_Pred (p, l) -> mklApp c_Sql_Pred [|mk_predicate p; mkList (Lazy.force c_aggterm) mk_aggterm l|]
  | Coq_sql_algebra.Sql_Quant (q, p, l, qu) -> mklApp c_Sql_Quant [|mk_quantifier q; mk_predicate p; mkList (Lazy.force c_aggterm) mk_aggterm l; mk_sql_query qu|]
  | Coq_sql_algebra.Sql_In (Coq_sql_algebra.Select_List l, qu) -> mklApp c_Sql_In [|mk_select_list l; mk_sql_query qu|]
  | Coq_sql_algebra.Sql_In (Coq_sql_algebra.Select_Star, _) -> failwith "Sqlparser.mk_sql_atom: in IN the select must be a list"
  | Coq_sql_algebra.Sql_Exists qu -> mklApp c_Sql_Exists [|mk_sql_query qu|]


(* Parsing a SQL query *)

let lowercase query =
  let quote = ref false in
  String.map (fun c ->
      if c = '\'' then (
        quote := not !quote;
        c
      ) else if not !quote then
        Char.lowercase_ascii c
      else
        c
    ) query

let parse_query squery =
  let squery = lowercase squery in
  let linebuf = Lexing.from_string squery in
  try
    Sql_parser.line Sql_lexer.token linebuf
  with
    | Sql_lexer.Error msg -> failwith msg
    | Sql_parser.Error -> failwith ("At offset "^(string_of_int (Lexing.lexeme_start linebuf))^": syntax error.\n")

let parse_query_to_coq squery res =
  try
    let oquery = parse_query squery in
    let cquery =
      match oquery with
        | Sql_ast.SQL_Query q -> mk_sql_query (ToCoq.select (ToCoq.init_context ()) q)
        | Sql_ast.SQL_Create c -> mk_create_table_query (ToCoq.create_table c)
        | Sql_ast.SQL_Insert i -> mk_insert_query (ToCoq.insert i) in
    let ce1 = mkUConst cquery in
    let _ = Declare.declare_constant res (Entries.DefinitionEntry ce1, Decl_kinds.IsDefinition Decl_kinds.Definition) in
    ()
  with
    | Failure msg -> CErrors.user_err (Pp.str msg)
