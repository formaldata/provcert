(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

%{
  open Utils
  open Basics
  open Sql_ast
%}

%token EOL
%token CFLEX COMMA DOT EQ GEQ GT LEQ LIKE LPAR LT MINUS NEQ PERCENT PLUS RPAR SEMICOLON SLASH STAR
%token ALL AND ANY AS ASC AVG COUNT CREATE DESC DISTINCT EXISTS FROM GRPBY IN HAVING MAX MIN NOT OR ORDERBY SELECT SUM TABLE TRUE FALSE WHERE UNION INTERSECT EXCEPT NULL INSERT INTO VALUES INTEGER TEXT VARCHAR BOOLEAN PRIMARY FOREIGN KEY REFERENCES
%token <int> INT
%token <string> VAR STRING

(* See: https://www.postgresql.org/docs/9.6/static/sql-syntax-lexical.html#SQL-PRECEDENCE-TABLE *)
%left OR
%left AND
%right NOT
%left MINUS PLUS
%left SLASH STAR PERCENT
%left CFLEX

%start <Sql_ast.sql_line> line
%type <(typed_aname * (bool * (relname * (aname option)) option), (aname list, relname * ((aname list) * (aname list))) sum) sum> col_or_keys

%%

line:
| m = main    { SQL_Query m }
| c = create  { SQL_Create c }
| i = insert  { SQL_Insert i }

main:
| e = expr SEMICOLON  { e }

expr:
| s = select f = from w = where g = group_by { Sql_select(fst(s), snd(s), f, w, g) }
| TABLE t = VAR                              { Sql_table(t) }
| q1 = expr op = setop q2 = expr             { Sql_set(op, q1, q2) }
| LPAR e = expr RPAR                         { e }

select:
| SELECT t = select_type i = select_item {t, i}

select_type:
| ALL? {All}
| DISTINCT {Distinct}

select_item:
| STAR {Select_star}
| l = separated_nonempty_list(COMMA, aggregate_ren) {Select_list(l)}

aggregate_ren:
| t = aggregate_term {(t, None)}
| t = aggregate_term AS? v = VAR {(t, Some(v))}

aggregate_term:
| LPAR t = aggregate_term RPAR {t}
| MINUS t = aggregate_term {A_Exp("-", [t])}
| t1 = aggregate_term s = symb t2 = aggregate_term {A_Exp(s, [t1; t2])}
| f = aggr_fun LPAR t = aggregate_term RPAR {A_Agg(f, Some t)}
| f = aggr_fun LPAR STAR RPAR {A_Agg(f, None)}
| t = funterm {A_Fun(t)}

%inline symb:
| PLUS    { "+" }
| MINUS   { "-" }
| STAR    { "*" }
| SLASH   { "/" }
| PERCENT { "%" }
| CFLEX   { "^" }

%inline aggr_fun:
| COUNT { "COUNT" }
| AVG   { "AVG" }
| MIN   { "MIN" }
| MAX   { "MAX" }
| SUM   { "SUM" }

funterm:
| v = value   { F_Constant(v) }
| d = dot     { F_Dot(d) }

value:
| NULL        { VNull }
| s = STRING  { VString s }
| i = INT     { VInt i }
| b = bool    { VBool b }

bool:
| TRUE   { true }
| FALSE  { false }

dot:
| v = VAR              { (None, v) }
| t = VAR DOT v = VAR  { (Some(t), v) }

from:
| FROM l = separated_nonempty_list(COMMA, from_item)  { l }

from_item:
| r = ren                         { Basic(r) }
| LPAR q = expr RPAR              { Subquery(q) }
| LPAR e = expr RPAR AS? v = VAR  { Renquery(e, v, []) }
| LPAR e = expr RPAR AS? v = VAR LPAR l = separated_nonempty_list(COMMA, VAR) RPAR {Renquery(e, v, l)}

ren:
| v = VAR                { (v, None) }
| v1 = VAR AS? v2 = VAR  { (v1, Some(v2,[])) }
| v1 = VAR AS? v2 = VAR LPAR l = separated_nonempty_list(COMMA, VAR) RPAR { (v1, Some(v2,l)) }

where:
| WHERE f = sql_formula  { Some(f) }
|                        { None }

sql_formula:
| c = sql_conj               { c }
| n = sql_neg                { n }
| a = sql_atom               { a }
| LPAR f = sql_formula RPAR  { f }

sql_conj:
| LPAR f = sql_conj RPAR                                  { f }
| f1 = sql_formula o = and_or f2 = sql_formula            { Sql_Conj(o, f1, f2) }

%inline and_or:
| OR   { Or_F }
| AND  { And_F }

sql_neg:
| LPAR f = sql_neg RPAR          { f }
| NOT f = sql_formula            { Sql_Not(f) }

sql_atom:
| TRUE { Sql_True }
| t1 = aggregate_term p = predicate t2 = aggregate_term { Sql_Pred(p, t1, t2)}
| t1 = aggregate_term p = predicate LPAR e2 = expr RPAR { Sql_PredQ(p, t1, e2)}
/* IN */
| i = select_item IN LPAR e = expr RPAR {Sql_In(i, e)}
| LPAR i = select_item RPAR IN LPAR e = expr RPAR {Sql_In(i, e)}
/* NOT IN has a particular status since NOT is not at the beginning of the sentence */
| i = select_item NOT IN LPAR e = expr RPAR {Sql_Not(Sql_In(i, e))}
| LPAR i = select_item RPAR NOT IN LPAR e = expr RPAR {Sql_Not(Sql_In(i, e))}
| t = aggregate_term p = predicate q = quantifier LPAR e = expr RPAR { Sql_Quant(q, p, [t], e) }
| EXISTS LPAR e = expr RPAR { Sql_Nonempty e }

%inline predicate:
| EQ { "=" }
| LT { "<" }
| GT { ">" }
| NEQ { "<>" }
| GEQ { ">=" }
| LEQ { "<=" }
| LIKE { "like" }

quantifier:
| ALL { Forall_F }
| ANY { Exists_F }

group_by:
| GRPBY l = separated_nonempty_list(COMMA, aggregate_term) h = having o = order_by{ Some(l, h, o) }
| { None }

having:
| HAVING f = sql_formula {Some(f)}
| { None }

order_type:
| ASC {Asc}
| DESC {Desc}

order_by_atoms:
| d = dot COMMA a = order_by_atoms { fun acc res -> a (d::acc) res }
| d = dot o = order_type COMMA a = order_by_atoms { fun acc res -> a [] ((List.rev (d::acc), Some o)::res) }
| d = dot { fun acc res -> (List.rev (d::acc), None)::res }
| d = dot o = order_type { fun acc res -> (List.rev (d::acc), Some o)::res }

order_by:
| ORDERBY l = order_by_atoms { Some (List.rev (l [] [])) }
|                            { None }

create:
| CREATE TABLE v = VAR LPAR l = separated_nonempty_list(COMMA, col_or_keys) RPAR SEMICOLON
     {
       let rec find_fk r = function
         | [] -> raise Not_found
         | (r1, ks)::l -> if r = r1 then (ks, l) else let (ks', l') = find_fk r l in (ks', (r1, ks)::l')
       in

       (v,
        let l : ((typed_aname * (bool * (relname * (aname option)) option), (aname list, relname * ((aname list) * (aname list))) sum) sum) list = l in
        let (l, lk) = Utils.partition l in
        let (lpk, lfk) = Utils.partition lk in
        let lpk = List.flatten lpk in
        let (cols, lk) =
          List.fold_left (fun (cols, (lpk, lfk)) ((a, t), (b, fk)) ->
              let lpk = if b then (a::lpk) else lpk in
              let lfk =
                match fk with
                  | Some (r, fk) ->
                     (try let ((k1, k2), lfk) = find_fk r lfk in
                          (r, (a::k1, (match fk with Some fk -> fk::k2 | None -> k2)))::lfk
                      with | Not_found -> (r, ([a], (match fk with Some fk -> [fk] | None -> [])))::lfk
                     )
                  | None -> lfk
              in
              let cols = (a, t)::cols in
              (cols, (lpk, lfk))
            ) ([], (lpk, lfk)) l
        in
        (List.rev cols, lk)
     ) }

col:
| v = VAR t = sqltype not_null?        {((v, t), (false, None))}
| v = VAR t = sqltype not_null? REFERENCES r = VAR
                                       {((v, t), (false, Some (r, None)))}
| v = VAR t = sqltype not_null? REFERENCES r = VAR LPAR fk = VAR RPAR
                                       {((v, t), (false, Some (r, Some fk)))}
| v = VAR t = sqltype not_null? PRIMARY KEY
                                       {((v, t), (true, None))}
| v = VAR t = sqltype not_null? PRIMARY KEY REFERENCES r = VAR
                                       {((v, t), (true, Some (r, None)))}
| v = VAR t = sqltype not_null? PRIMARY KEY REFERENCES r = VAR LPAR fk = VAR RPAR
                                       {((v, t), (true, Some (r, Some fk)))}

not_null:
| NOT NULL  { CWarnings.create ~name:"" ~category:"Datacert plugin" Pp.str "NOT NULL is currently ignored, which is fine since it has no influence on the validity of plans" }

col_or_keys:
| c = col                { Left c }
| PRIMARY KEY LPAR k = separated_nonempty_list(COMMA, VAR) RPAR
                         { Right (Left k) }
| FOREIGN KEY LPAR k1 = separated_nonempty_list(COMMA, VAR) RPAR REFERENCES r = VAR
                         { Right (Right (r, (k1, []))) }
| FOREIGN KEY LPAR k1 = separated_nonempty_list(COMMA, VAR) RPAR REFERENCES r = VAR LPAR k2 = separated_nonempty_list(COMMA, VAR) RPAR
                         { Right (Right (r, (k1, k2))) }

sqltype:
| text     { TString }
| INTEGER  { TInt }
| BOOLEAN  { TBool }

text:
| TEXT                   { () }
| VARCHAR LPAR INT RPAR  { CWarnings.create ~name:"" ~category:"Datacert plugin" Pp.str "VARCHAR is currently treated as arbitrary length text, which is fine since it has no influence on the validity of plans" }

setop:
| UNION      { Union }
| INTERSECT  { Intersect }
| EXCEPT     { Except }

insert:
| INSERT INTO t = VAR cols = cols? VALUES vals = separated_nonempty_list(COMMA, values) SEMICOLON  { (t,cols,vals) }

cols:
| LPAR cs = separated_nonempty_list(COMMA, VAR) RPAR  { cs }

values:
| LPAR l = separated_nonempty_list(COMMA, value) RPAR  { l }
