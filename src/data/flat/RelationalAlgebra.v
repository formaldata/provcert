(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Set Implicit Arguments.
Require Import Bool List Arith NArith.

Require Import BasicTacs BasicFacts ListFacts ListPermut ListSort OrderedSet
        FiniteSet FiniteBag FiniteCollection Data Join FlatData Tree Term Bool3 Formula.

(*
Section Logic.

Hypothesis dom : Type.
Hypothesis predicate : Type.
Hypothesis term : Type.

Inductive formula : Type :=
  | TTrue 
  | Atom : predicate -> list term -> formula
  | Conj : and_or -> formula -> formula -> formula
  | Not : formula -> formula
  | Quant : quantifier -> variable dom -> formula -> formula.


Hypothesis ODom : Oset.Rcd dom.
Hypothesis OP : Oset.Rcd predicate.

SearchAbout Vrbl.
Hypothesis OVR : Oset.Rcd 
Hypothesis variables_t : term -> 

End Logic.

Section DomCal.

Print sql_formula.
Print term.
End DomCal.

Section TupleCal.

End TupleCal.

Section RelCal.

End RelCal.
*)

Section Sec.

Hypothesis T : Tuple.Rcd.

Hypothesis relname : Type.
(* Hypothesis ORN : Oset.Rcd relname. *)

Import Tuple.

Notation predicate := (predicate T).
Notation OPredicate := (OPred T).
Notation value := (value T).
Notation attribute := (attribute T).
Notation tuple := (tuple T).

Notation setA := (Fset.set (A T)).
Notation FTupleT := (Fecol.CSet (CTuple T)).
Notation setT := (Feset.set FTupleT).

Inductive term : Type :=
  | Constant : value -> term
  | Dot : attribute -> term.

Inductive formula : Type :=
  | Conj : and_or -> formula -> formula -> formula
  | Not : formula -> formula
  | Pred : predicate -> list term -> formula.

Inductive query : Type := 
  | Q_Table : relname -> query
  | Q_Sigma : formula -> query -> query
  | Q_Pi : setA -> query -> query
  | Q_NaturalJoin : query -> query -> query
  | Q_Rename : (list (attribute * attribute)) -> query -> query
  | Q_Set : set_op -> query -> query -> query.

Fixpoint attributes_l l :=
  match l with
  | nil => Fset.empty (A T)
  | Constant _ :: l => attributes_l l
  | Dot a :: l => Fset.add (A T) a (attributes_l l)
  end.

Fixpoint attributes_f f :=
  match f with
  | Conj _ f1 f2 => (attributes_f f1) unionS (attributes_f f2)
  | Not f => attributes_f f
  | Pred p l => attributes_l l
  end.
    
Hypothesis basesort : relname -> setA.
Hypothesis instance : relname -> setT.

Fixpoint eval_formula f (t : tuple) :=
  match f with
  | Conj o f1 f2 => interp_conj (B T) o (eval_formula f1 t) (eval_formula f2 t)
  | Not f => Bool.negb (B T) (eval_formula f t)
  | Pred p l => 
    interp_predicate 
      T p (List.map 
             (fun x => match x with 
                       | Constant v => v 
                       | Dot a => if a inS? labels T t 
                                  then dot T t a 
                                  else default_value T (type_of_attribute T a)
                       end) 
             l)
  end.

Fixpoint sort (q : query) : setA :=
  match q with
    | Q_Table r1 => basesort r1
    | Q_Sigma _ q1 => sort q1
    | Q_Pi s q1 => s
    | Q_NaturalJoin q1 q2 => (sort q1) unionS (sort q2)
    | Q_Rename rho q1 => Fset.map (A T) (A T) (apply_renaming T rho) (sort q1)
    | Q_Set _ q1 _ => sort q1
  end.

Fixpoint eval_query q {struct q} : setT :=
  match q with
  | Q_Table r => instance r
  | Q_Sigma f q => Feset.filter _ (fun t => Bool.is_true (B T) (eval_formula f t)) (eval_query q)
  | Q_Pi s q => 
      Feset.map 
        _ _ 
        (fun t => mk_tuple 
                    T s 
                    (fun a => if a inS? labels T t 
                              then dot T t a 
                              else default_value T (type_of_attribute T a))) 
                (eval_query q) 
  | Q_NaturalJoin q1 q2 => 
      Feset.mk_set _ (natural_join_list T (Feset.elements _ (eval_query q1)) 
                                      (Feset.elements _ (eval_query q2)))
  | Q_Rename rho q => 
      Feset.map _ _ (fun t => rename_tuple T (apply_renaming T rho) t) (eval_query q)
  | Q_Set o q1 q2 =>
    if sort q1 =S?= sort q2 
    then Feset.interp_set_op _ o (eval_query q1) (eval_query q2)
    else Feset.empty _
  end.
    
Lemma eval_formula_unfold :
  forall f (t : tuple), eval_formula f t =
  match f with
  | Conj o f1 f2 => interp_conj (B T) o (eval_formula f1 t) (eval_formula f2 t)
  | Not f => Bool.negb (B T) (eval_formula f t)
  | Pred p l => 
    interp_predicate 
      T p (List.map 
             (fun x => match x with 
                       | Constant v => v 
                       | Dot a => if a inS? labels T t 
                                  then dot T t a 
                                  else default_value T (type_of_attribute T a)
                       end) 
             l)
  end.
Proof.
intros f t; case f; intros; apply refl_equal.
Qed.

Lemma sort_unfold : 
  forall q, sort q =
  match q with
    | Q_Table r1 => basesort r1
    | Q_Sigma _ q1 => sort q1
    | Q_Pi s q1 => s
    | Q_NaturalJoin q1 q2 => (sort q1) unionS (sort q2)
    | Q_Rename rho q1 => Fset.map (A T) (A T) (apply_renaming T rho) (sort q1)
    | Q_Set _ q1 _ => sort q1
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Lemma eval_query_unfold : 
  forall q, eval_query q = 
  match q with
  | Q_Table r => instance r
  | Q_Sigma f q => Feset.filter _ (fun t => Bool.is_true (B T) (eval_formula f t)) (eval_query q)
  | Q_Pi s q => 
      Feset.map 
        _ _ 
        (fun t => mk_tuple 
                    T s 
                    (fun a => if a inS? labels T t 
                              then dot T t a 
                              else default_value T (type_of_attribute T a))) 
                (eval_query q) 
  | Q_NaturalJoin q1 q2 => 
      Feset.mk_set _ (natural_join_list T (Feset.elements _ (eval_query q1)) 
                                      (Feset.elements _ (eval_query q2)))
  | Q_Rename rho q => 
      Feset.map _ _ (fun t => rename_tuple T (apply_renaming T rho) t) (eval_query q)
  | Q_Set o q1 q2 =>
    if sort q1 =S?= sort q2 
    then Feset.interp_set_op _ o (eval_query q1) (eval_query q2)
    else Feset.empty _
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Notation query_eq q1 q2 := (eval_query q1 =SE= eval_query q2).

Infix "=I=" := query_eq (at level 70, no associativity).

Lemma eval_formula_eq :
  forall f t1 t2, t1 =t= t2 -> eval_formula f t1 = eval_formula f t2.
Proof.
intro f; induction f as [o f1 IH1 f2 IH2 | f IH | p l]; intros t1 t2 Ht.
- rewrite 2 (eval_formula_unfold (Conj _ _ _) _); apply f_equal2.
  * apply IH1; assumption.
  * apply IH2; assumption.
- rewrite 2 (eval_formula_unfold (Not _) _); apply f_equal.
  apply IH; assumption.
- rewrite 2 (eval_formula_unfold (Pred _ _) _); apply f_equal.
  rewrite <- map_eq; intros [c | a] Ha; [apply refl_equal | ].
  rewrite tuple_eq in Ht.
  rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 Ht)).
  case_eq (a inS? labels T t1); intro Ka; [ | apply refl_equal].
  apply (proj2 Ht _ Ka).
Qed.

Lemma eval_formula_eq_strong :
  forall f u1 u2, (forall a, a inS attributes_f f -> 
                             (if a inS? labels T u1 
                              then dot T u1 a else default_value T (type_of_attribute T a)) =
                             if a inS? labels T u2 
                             then dot T u2 a else default_value T (type_of_attribute T a)) ->
                  eval_formula f u1 = eval_formula f u2.
Proof.
intros f u1 u2; 
induction f as [o f1 IH1 f2 IH2 | f IH | p l]; intro Hf.
- rewrite !(eval_formula_unfold (Conj _ _ _)); apply f_equal2.
  + apply IH1.
    intros a Ha; apply Hf; simpl; rewrite Fset.mem_union, Ha; apply refl_equal.
  + apply IH2.
    intros a Ha; apply Hf; simpl; rewrite Fset.mem_union, Ha; apply Bool.Bool.orb_true_r.
- rewrite !(eval_formula_unfold (Not _)); apply f_equal.
  apply IH; apply Hf.
- rewrite !(eval_formula_unfold (Pred _ _)); apply f_equal.
  simpl in Hf.
  rewrite <- map_eq; intros [c | a] Ha; [apply refl_equal | ].
  apply Hf; clear Hf.
  induction l as [ | [c1 | a1] l]; [contradiction Ha | | ].
  + simpl in Ha; destruct Ha as [Ha | Ha]; [discriminate Ha | ].
      simpl; apply IHl; assumption.
  + simpl in Ha; destruct Ha as [Ha | Ha].
    * injection Ha; clear Ha; intro; subst a1; simpl.
      rewrite Fset.add_spec, Oset.eq_bool_refl; apply refl_equal.
    * simpl; rewrite Fset.add_spec, (IHl Ha); apply Bool.Bool.orb_true_r.
Qed.

Lemma eval_formula_mk_tuple :
  forall s f u, attributes_f f subS s ->
  eval_formula f
    (mk_tuple T s
       (fun a : attribute =>
        if a inS? labels T u then dot T u a else default_value T (type_of_attribute T a))) =
  eval_formula f u.
Proof.
intros s f u Hf; rewrite Fset.subset_spec in Hf.
apply eval_formula_eq_strong.
intros a Ha; rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), (Hf _ Ha).
rewrite dot_mk_tuple, (Hf _ Ha); apply refl_equal.
Qed.

(** Well-sortedness is inherited by queries from base relations ([relname]). *)
(** An instance is well-sorted whenever it associates to each relname a set of tuples whose labels is _exactly_ the basesort of _that_ relname. *)
Definition well_sorted_instance (I : relname -> setT) :=
 forall (r : relname) (t : tuple), t inSE (I r) -> labels T t =S= basesort r.

Lemma well_sorted_query :
  well_sorted_instance instance -> 
  forall (q : query) (t : tuple), t inSE (eval_query q) -> labels T t =S= sort q.
Proof.
intros W q; induction q as [q | f q | s q | q1 IHq1 q2 IHq2 | rho q | o q1 IHq1 q2 IHq2]; 
  intros t Ht; rewrite eval_query_unfold in Ht; rewrite sort_unfold.
- apply W; apply Ht.
- rewrite Feset.filter_spec in Ht; [ | intros; apply f_equal; apply eval_formula_eq; assumption].
  apply IHq; rewrite Bool.Bool.andb_true_iff in Ht; apply (proj1 Ht).
- unfold Feset.map in Ht; rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff in Ht.
  destruct Ht as [t' [Ht Ht']]; rewrite in_map_iff in Ht'.
  destruct Ht' as [u [Ht' Hu]].
  rewrite tuple_eq in Ht.
  rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)), <- Ht'.
  rewrite (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)); apply Fset.equal_refl.
- rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff in Ht.
  destruct Ht as [t' [Ht Ht']]; rewrite in_natural_join_list in Ht'.
  destruct Ht' as [t1 [t2 [Ht1 [Ht2 [Ht12 Ht']]]]].
  rewrite tuple_eq in Ht.
  rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)), Ht'.
  unfold join_tuple; rewrite (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
  apply Fset.union_eq.
  + apply IHq1; rewrite Feset.mem_elements; apply Oeset.in_mem_bool; assumption.
  + apply IHq2; rewrite Feset.mem_elements; apply Oeset.in_mem_bool; assumption.
- unfold Feset.map in Ht; rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff in Ht.
  destruct Ht as [t' [Ht Ht']]; rewrite in_map_iff in Ht'.
  destruct Ht' as [u [Ht' Hu]].
  rewrite tuple_eq in Ht.
  rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)), <- Ht'.
  rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 (rename_tuple_ok _ _ _))).
  unfold Fset.map; apply Fset.equal_refl_alt; do 2 apply f_equal.
  apply Fset.elements_spec1; apply IHq; rewrite Feset.mem_elements.
  apply Oeset.in_mem_bool; assumption.
- case_eq (sort q1 =S?= sort q2); intro Hs; rewrite Hs in Ht.
  + destruct o; simpl in Ht.
    * rewrite Feset.mem_union, Bool.Bool.orb_true_iff in Ht.
      destruct Ht as [Ht | Ht]; [apply IHq1; assumption | ].
      rewrite (Fset.equal_eq_2 _ _ _ _ Hs); apply IHq2; assumption.
    * rewrite Feset.mem_union, Bool.Bool.orb_true_iff in Ht.
      destruct Ht as [Ht | Ht]; [apply IHq1; assumption | ].
      rewrite (Fset.equal_eq_2 _ _ _ _ Hs); apply IHq2; assumption.
    * rewrite Feset.mem_inter, Bool.Bool.andb_true_iff in Ht.
      apply IHq1; apply (proj1 Ht).
    * rewrite Feset.mem_diff, Bool.Bool.andb_true_iff in Ht.
      apply IHq1; apply (proj1 Ht).
  + apply False_rec.
    rewrite Feset.mem_empty in Ht; discriminate Ht.
Qed.

Lemma mem_Q_Sigma :
  forall f q t, t inSE (eval_query (Q_Sigma f q)) <->
                t inSE eval_query q /\ Bool.is_true _ (eval_formula f t) = true.
Proof.
intros f q t; 
  rewrite (eval_query_unfold (Q_Sigma _ _)), Feset.filter_spec, Bool.Bool.andb_true_iff;
  [ | intros; apply f_equal; apply eval_formula_eq; assumption].
split; exact (fun h => h).
Qed.

Lemma mem_Q_Pi :
  forall s q t, t inSE eval_query q -> 
                mk_tuple T s (fun a => if a inS? labels T t 
                                       then dot T t a 
                                       else default_value T (type_of_attribute T a)) 
                         inSE eval_query (Q_Pi s q).
Proof.
intros s q t H; rewrite eval_query_unfold.
rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in H.
destruct H as [t' [H Ht']].
unfold Feset.map.
rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff.
eexists; split; [ | rewrite in_map_iff; eexists; split; [ | apply Ht']; apply refl_equal].
apply mk_tuple_eq_2.
intros a Ha.
rewrite tuple_eq in H.
rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 H)).
case_eq (a inS? labels T t); intro Ka; [ | apply refl_equal].
apply (proj2 H); assumption.
Qed.

Lemma mem_Q_Pi_rev :
  forall s q t, t inSE eval_query (Q_Pi s q) ->
                exists u, u inSE eval_query q /\ 
                          t =t=  mk_tuple T s (fun a => if a inS? labels T u 
                                       then dot T u a 
                                       else default_value T (type_of_attribute T a)).
Proof.
intros s q t Ht; rewrite eval_query_unfold in Ht; unfold Feset.map in Ht.
rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff in Ht.
destruct Ht as [t' [Ht Ht']]; rewrite in_map_iff in Ht'.
destruct Ht' as [u [Ht' Hu]].
exists u; split.
- rewrite Feset.mem_elements; apply Oeset.in_mem_bool; assumption.
- rewrite (Oeset.compare_eq_1 _ _ _ _ Ht), <- Ht'.
  apply Oeset.compare_eq_refl.
Qed.

Lemma mem_Q_NaturalJoin :
  forall q1 q2 t1 t2, join_compatible_tuple T t1 t2 = true -> 
                      t1 inSE eval_query q1 -> t2 inSE eval_query q2 -> 
                      join_tuple T t1 t2 inSE eval_query (Q_NaturalJoin q1 q2).
Proof.
intros q1 q2 t1 t2 Hj Ht1 Ht2.
rewrite eval_query_unfold, Feset.mem_mk_set.
rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ht1, Ht2.
destruct Ht1 as [t1' [Ht1 Ht1']]; destruct Ht2 as [t2' [Ht2 Ht2']].
rewrite Oeset.mem_bool_true_iff.
exists (join_tuple T t1' t2'); split.
- apply join_tuple_eq; assumption.
- rewrite in_natural_join_list.
  exists t1'; exists t2'; repeat split; trivial.
  rewrite <- Hj; apply sym_eq; apply join_compatible_tuple_eq; assumption.
Qed.

Lemma mem_Q_NaturalJoin_rev :
  forall q1 q2 t, t inSE eval_query (Q_NaturalJoin q1 q2) ->
                  exists t1, exists t2,
                      t1 inSE eval_query q1 /\
                      t2 inSE eval_query q2 /\
                      join_compatible_tuple T t1 t2 = true /\
                      t =t= join_tuple T t1 t2.
                      Proof.
intros q1 q2 t Ht; rewrite eval_query_unfold, Feset.mem_mk_set, Oeset.mem_bool_true_iff in Ht.
destruct Ht as [t' [Ht Ht']].
rewrite in_natural_join_list in Ht'.
destruct Ht' as [t1 [t2 [Ht1 [Ht2 [Hj Ht']]]]]; subst t'.
exists t1; exists t2; repeat split; trivial.
- rewrite Feset.mem_elements.
  apply Oeset.in_mem_bool; assumption.
- rewrite Feset.mem_elements.
  apply Oeset.in_mem_bool; assumption.
Qed.

Lemma mem_Q_Rename :
  forall rho q t, t inSE eval_query q -> 
                  rename_tuple T (apply_renaming T rho) t inSE eval_query (Q_Rename rho q).
Proof.
intros rho q t Ht; rewrite eval_query_unfold.
rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ht.
destruct Ht as [t' [Ht Ht']].
unfold Feset.map.
rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff.
eexists; split; [ | rewrite in_map_iff; eexists; split; [ | apply Ht']; apply refl_equal].
apply rename_tuple_eq; assumption.
Qed.

Lemma mem_Q_Set_Union :
  forall q1 q2 t, sort q1 =S= sort q2 ->
                  t inSE eval_query q1 \/ t inSE eval_query q2 <-> 
                  t inSE eval_query (Q_Set Union q1 q2).
Proof.
intros q1 q2 t Hs.
rewrite (eval_query_unfold (Q_Set _ _ _)), Hs; simpl.
rewrite Feset.mem_union, Bool.Bool.orb_true_iff.
split; exact (fun h => h).
Qed.

Lemma mem_Q_Set_Inter :
  forall q1 q2 t, sort q1 =S= sort q2 ->
                  t inSE eval_query q1 /\ t inSE eval_query q2 <-> 
                  t inSE eval_query (Q_Set Inter q1 q2).
Proof.
intros q1 q2 t Hs.
rewrite (eval_query_unfold (Q_Set _ _ _)), Hs; simpl.
rewrite Feset.mem_inter, Bool.Bool.andb_true_iff.
split; exact (fun h => h).
Qed.

Lemma mem_Q_Set_Diff :
  forall q1 q2 t, sort q1 =S= sort q2 ->
                  t inSE eval_query q1 /\ t inSE? eval_query q2 = false <-> 
                  t inSE eval_query (Q_Set Diff q1 q2).
Proof.
intros q1 q2 t Hs.
rewrite (eval_query_unfold (Q_Set _ _ _)), Hs; simpl.
rewrite Feset.mem_diff, Bool.Bool.andb_true_iff, negb_true_iff.
split; exact (fun h => h).
Qed.

(** Formula (1) *)
Lemma Sigma_Sigma : 
  forall f1 f2 q, Q_Sigma f1 (Q_Sigma f2 q) =I= Q_Sigma (Conj And_F f1 f2) q.
Proof.
intros f1 f2 q.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff.
rewrite !mem_Q_Sigma, !(eval_formula_unfold (Conj _ _ _)), !Bool.true_is_true; simpl.
rewrite !Bool.andb_true_iff.
intuition.
Qed.

Lemma Inter_Sigma : 
  forall f1 f2 q,
    Q_Set Inter (Q_Sigma f1 q) (Q_Sigma f2 q) =I= Q_Sigma (Conj And_F f1 f2) q.
Proof.
intros f1 f2 q.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff.
assert (H : sort (Q_Sigma f1 q) =S= sort (Q_Sigma f2 q)); [apply Fset.equal_refl | ].
repeat (rewrite mem_Q_Sigma || 
        rewrite (eval_formula_unfold (Conj _ _ _)) || 
        rewrite Bool.true_is_true ||
        rewrite <- (mem_Q_Set_Inter _ _ _ H)); simpl.
rewrite !Bool.andb_true_iff.
intuition.
Qed.

(** Formula (2) *)
Lemma Sigma_comm : 
 forall f1 f2 q, Q_Sigma f1 (Q_Sigma f2 q) =I= Q_Sigma f2 (Q_Sigma f1 q).
Proof.
intros f1 f2 q.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff.
rewrite !mem_Q_Sigma.
intuition.
Qed.

(** Formula (3) *)
Lemma NaturalJoin_assoc :
  forall q1 q2 q3, 
    Q_NaturalJoin (Q_NaturalJoin q1 q2) q3 =I= Q_NaturalJoin q1 (Q_NaturalJoin q2 q3).
Proof.
intros q1 q2 q3.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- destruct (mem_Q_NaturalJoin_rev _ _ _ H) as [t12 [t3 [H12 [Ht3 [Hj Ht]]]]].
  destruct (mem_Q_NaturalJoin_rev _ _ _ H12) as [t1 [t2 [Ht1 [Ht2 [Hj' Ht']]]]].
  rewrite (Oeset.compare_eq_2 _ _ _ _ (join_tuple_eq_1 _ _ _ _ Ht')) in Ht.
  rewrite <- (Oeset.compare_eq_2 _ _ _ _ (join_tuple_assoc _ _ _ _)) in Ht.
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht).
  apply mem_Q_NaturalJoin; [ | | apply mem_Q_NaturalJoin]; trivial.
  + rewrite tuple_eq in *; rewrite join_compatible_tuple_alt in *.
    unfold join_tuple; intros a Ha1 Ha23.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)) in Ha23.
    rewrite dot_mk_tuple, Ha23.
    case_eq (a inS? labels T t2); intro Ha2.
    * apply Hj'; trivial.
    * rewrite Fset.mem_union, Ha2, Bool.Bool.orb_false_l in Ha23.
      rewrite <- Hj, (proj2 Ht').
      -- unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union, Ha1; apply refl_equal.
      -- rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
         rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha1.
         apply refl_equal.
      -- rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
         rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha1.
         apply refl_equal.
      -- assumption.
  + rewrite tuple_eq in *; rewrite join_compatible_tuple_alt in *.
    unfold join_tuple; intros a Ha2 Ha3.
    rewrite <- Hj, (proj2 Ht').
    * unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union.
      case_eq (a inS? labels T t1); intro Ha1.
      -- apply sym_eq; apply Hj'; trivial.
      -- rewrite Ha2; apply refl_equal.
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha2.
      apply Bool.Bool.orb_true_r.
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha2.
      apply Bool.Bool.orb_true_r.
    * assumption.
- destruct (mem_Q_NaturalJoin_rev _ _ _ H) as [t1 [t23 [Ht1 [H23 [Hj Ht]]]]].
  destruct (mem_Q_NaturalJoin_rev _ _ _ H23) as [t2 [t3 [Ht2 [Ht3 [Hj' Ht']]]]].
  rewrite (Oeset.compare_eq_2 _ _ _ _ (join_tuple_eq_2 _ _ _ _ Ht')) in Ht.
  rewrite (Oeset.compare_eq_2 _ _ _ _ (join_tuple_assoc _ _ _ _)) in Ht.
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht).
  apply mem_Q_NaturalJoin; [ | apply mem_Q_NaturalJoin | ]; trivial.
  + rewrite tuple_eq in *; rewrite join_compatible_tuple_alt in *.
    unfold join_tuple; intros a Ha12 Ha3.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)) in Ha12.
    rewrite dot_mk_tuple, Ha12.
    case_eq (a inS? labels T t1); intro Ha1.
    * rewrite Hj, (proj2 Ht').
      -- unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union, Ha3, Bool.Bool.orb_true_r.
         case_eq (a inS? labels T t2); intro Ha2; [ | apply refl_equal].
         apply Hj'; trivial.
      -- rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
         rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha3.
         apply Bool.Bool.orb_true_r.
      -- assumption.
      -- rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
         rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha3.
         apply Bool.Bool.orb_true_r.
    * apply Hj'; trivial.
      rewrite Fset.mem_union, Ha1 in Ha12; apply Ha12.
  + rewrite tuple_eq in *; rewrite join_compatible_tuple_alt in *.
    unfold join_tuple; intros a Ha1 Ha2.
    rewrite Hj, (proj2 Ht').
    * unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union, Ha2; apply refl_equal.
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha2.
      apply refl_equal.
    * assumption.
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha2.
      apply refl_equal.
Qed.
 
(** Formula (4) *)
Lemma NaturalJoin_comm :
  forall q1 q2, Q_NaturalJoin q1 q2 =I= Q_NaturalJoin q2 q1.
Proof.
intros q1 q2.
rewrite Feset.equal_spec; intro t.
rewrite !(eval_query_unfold (Q_NaturalJoin _ _)).
rewrite 2 Feset.mem_mk_set.
apply Oeset.permut_mem_bool_eq; apply Oeset.nb_occ_permut.
clear t; intro t; apply Oeset.permut_nb_occ; apply theta_join_list_comm.
- intros t1 t2; rewrite eq_bool_iff; rewrite !join_compatible_tuple_alt; 
    split; intros H a Ha1 Ha2; apply sym_eq; apply H; assumption.
- intros t1 t2; apply join_tuple_comm.
Qed.

(** Formula (5) *)
Lemma Pi_idempotent :
  forall s1 s2 q, s1 subS s2 -> Q_Pi s1 (Q_Pi s2 q) =I= Q_Pi s1 q.
Proof.
intros s1 s2 q Hs; rewrite Fset.subset_spec in Hs.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- destruct (mem_Q_Pi_rev _ _ _ H) as [u1 [H1 Ht]].
  destruct (mem_Q_Pi_rev _ _ _ H1) as [u2 [Hu2 Hu1]].
  assert (Ht' : t =t=  
                mk_tuple T s1
                         (fun a : attribute =>
                            if a inS? labels T u2 
                            then dot T u2 a else default_value T (type_of_attribute T a))).
  {
    rewrite (Oeset.compare_eq_1 _ _ _ _ Ht).
    apply mk_tuple_eq_2.
    intros a Ha1.
    rewrite tuple_eq in Hu1.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu1)), (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
    rewrite (Hs _ Ha1), (proj2 Hu1), dot_mk_tuple, (Hs _ Ha1); [apply refl_equal | ].
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu1)), (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
    apply Hs; assumption.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht').
  apply mem_Q_Pi; assumption.
- destruct (mem_Q_Pi_rev _ _ _ H) as [u [H1 Ht]].
  set (u2 :=  mk_tuple T s2
         (fun a : attribute =>
          if a inS? labels T u then dot T u a else default_value T (type_of_attribute T a))).
  assert (Ht' : t =t=  
                mk_tuple T s1
                         (fun a : attribute =>
                            if a inS? labels T u2 
                            then dot T u2 a else default_value T (type_of_attribute T a))).
  {
    rewrite (Oeset.compare_eq_1 _ _ _ _ Ht).
    apply mk_tuple_eq_2.
    intros a Ha1; unfold u2.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), dot_mk_tuple, (Hs _ Ha1).
    apply refl_equal.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht').
  apply mem_Q_Pi.
  apply mem_Q_Pi; assumption.
Qed.  

(** Formula (6) *)
Lemma Pi_Sigma_comm :
  forall s f q, attributes_f f subS s -> Q_Pi s (Q_Sigma f q) =I= Q_Sigma f (Q_Pi s q).
Proof.
intros s f q Hf; rewrite Fset.subset_spec in Hf.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- destruct (mem_Q_Pi_rev _ _ _ H) as [u [Hu Ht]].
  rewrite mem_Q_Sigma in Hu.
  rewrite mem_Q_Sigma; split.
  + rewrite (Feset.mem_eq_1 _ _ _ _ Ht).
    apply mem_Q_Pi; apply (proj1 Hu).
  + rewrite <- (proj2 Hu); apply f_equal; apply eval_formula_eq_strong.
    intros a Ha.
    rewrite tuple_eq in Ht.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)), (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
    rewrite (Hf _ Ha), (proj2 Ht), dot_mk_tuple, (Hf _ Ha); [apply refl_equal | ].
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)), (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
    apply (Hf _ Ha).
- rewrite mem_Q_Sigma in H.
  destruct (mem_Q_Pi_rev _ _ _ (proj1 H)) as [u [Hu Ht]].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht).
  apply mem_Q_Pi; rewrite mem_Q_Sigma; split; [assumption | ].
  rewrite <- (proj2 H); apply f_equal; apply eval_formula_eq_strong.
  intros a Ha.
  rewrite tuple_eq in Ht.
  rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)), (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
  rewrite (Hf _ Ha), (proj2 Ht), dot_mk_tuple, (Hf _ Ha); [apply refl_equal | ].
  rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)), (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
  apply (Hf _ Ha).
Qed.

(** Formula (7) *)
Lemma Sigma_NaturalJoin_comm : 
 well_sorted_instance instance ->
 forall f q1 q2, attributes_f f subS sort q1 ->
   Q_Sigma f (Q_NaturalJoin q1 q2) =I= Q_NaturalJoin (Q_Sigma f q1) q2.
Proof.
intros Wi f q1 q2 Hf; rewrite Fset.subset_spec in Hf.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- rewrite mem_Q_Sigma in H.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 H)) as [t1 [t2 [Ht1 [Ht2 [Hj Ht]]]]].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_NaturalJoin; trivial.
  rewrite mem_Q_Sigma; split; [assumption | ].
  rewrite <- (proj2 H); apply f_equal; apply eval_formula_eq_strong.
  assert (Wt1 : labels T t1 =S= sort q1).
  {
    apply well_sorted_query; assumption.
  }
  intros a Ha.
  rewrite tuple_eq in Ht; rewrite (proj2 Ht).
  + rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)); unfold join_tuple; rewrite dot_mk_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), !Fset.mem_union.
    rewrite !(Fset.mem_eq_2 _ _ _ Wt1), (Hf _ Ha); apply refl_equal.
  + rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), !Fset.mem_union.
    rewrite !(Fset.mem_eq_2 _ _ _ Wt1), (Hf _ Ha); apply refl_equal.
- destruct (mem_Q_NaturalJoin_rev _ _ _ H) as [t1 [t2 [Ht1 [Ht2 [Hj Ht]]]]].
  rewrite mem_Q_Sigma in Ht1.
  rewrite mem_Q_Sigma; split.
  + rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_NaturalJoin; trivial.
    apply (proj1 Ht1).
  + rewrite <- (proj2 Ht1); apply f_equal; apply eval_formula_eq_strong.
  assert (Wt1 : labels T t1 =S= sort q1).
  {
    apply well_sorted_query; [assumption | apply (proj1 Ht1)].
  }
  intros a Ha.
  rewrite tuple_eq in Ht; rewrite (proj2 Ht).
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)); unfold join_tuple; rewrite dot_mk_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), !Fset.mem_union.
      rewrite !(Fset.mem_eq_2 _ _ _ Wt1), (Hf _ Ha); apply refl_equal.
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), !Fset.mem_union.
      rewrite !(Fset.mem_eq_2 _ _ _ Wt1), (Hf _ Ha); apply refl_equal.
Qed.

Lemma Sigma_Union_distr :
  forall f q1 q2, Q_Sigma f (Q_Set Union q1 q2) =I= Q_Set Union (Q_Sigma f q1) (Q_Sigma f q2).
Proof.
intros f q1 q2.
rewrite Feset.equal_spec; intro t.
repeat (rewrite (eval_query_unfold (Q_Sigma _ _)) || 
        rewrite (eval_query_unfold (Q_Set _ _ _))).
rewrite !(sort_unfold (Q_Sigma _ _)).
case_eq (sort q1 =S?= sort q2); intro Hs; 
  [ | rewrite Feset.filter_spec, Feset.mem_empty; 
      [apply refl_equal | intros; apply f_equal; apply eval_formula_eq; assumption]].
simpl; rewrite Feset.mem_union.
do 3 (rewrite Feset.filter_spec; [ | intros; apply f_equal; apply eval_formula_eq; assumption]).
case (Bool.is_true (B T) (eval_formula f t)).
- rewrite !Bool.Bool.andb_true_r, Feset.mem_union; apply refl_equal.
- rewrite !Bool.Bool.andb_false_r; apply refl_equal.
Qed.

Lemma Sigma_Inter_distr :
  forall f q1 q2, Q_Sigma f (Q_Set Inter q1 q2) =I= Q_Set Inter (Q_Sigma f q1) (Q_Sigma f q2).
Proof.
intros f q1 q2.
rewrite Feset.equal_spec; intro t.
repeat (rewrite (eval_query_unfold (Q_Sigma _ _)) || 
        rewrite (eval_query_unfold (Q_Set _ _ _))).
rewrite !(sort_unfold (Q_Sigma _ _)).
case_eq (sort q1 =S?= sort q2); intro Hs; 
  [ | rewrite Feset.filter_spec, Feset.mem_empty; 
      [apply refl_equal | intros; apply f_equal; apply eval_formula_eq; assumption]].
simpl; rewrite Feset.mem_inter.
do 3 (rewrite Feset.filter_spec; [ | intros; apply f_equal; apply eval_formula_eq; assumption]).
case (Bool.is_true (B T) (eval_formula f t)).
- rewrite !Bool.Bool.andb_true_r, Feset.mem_inter; apply refl_equal.
- rewrite !Bool.Bool.andb_false_r; apply refl_equal.
Qed.

Lemma Sigma_Diff_distr :
  forall f q1 q2, Q_Sigma f (Q_Set Diff q1 q2) =I= Q_Set Diff (Q_Sigma f q1) (Q_Sigma f q2).
Proof.
intros f q1 q2.
rewrite Feset.equal_spec; intro t.
repeat (rewrite (eval_query_unfold (Q_Sigma _ _)) || 
        rewrite (eval_query_unfold (Q_Set _ _ _))).
rewrite !(sort_unfold (Q_Sigma _ _)).
case_eq (sort q1 =S?= sort q2); intro Hs; 
  [ | rewrite Feset.filter_spec, Feset.mem_empty; 
      [apply refl_equal | intros; apply f_equal; apply eval_formula_eq; assumption]].
simpl; rewrite Feset.mem_diff.
do 3 (rewrite Feset.filter_spec; [ | intros; apply f_equal; apply eval_formula_eq; assumption]).
case (Bool.is_true (B T) (eval_formula f t)).
- rewrite !Bool.Bool.andb_true_r, Feset.mem_diff; apply refl_equal.
- rewrite !Bool.Bool.andb_false_r; apply refl_equal.
Qed.

Definition Q_SemiJoin f q1 q2 := (Q_Pi (sort q1) (Q_Sigma f (Q_NaturalJoin q1 q2))).

Lemma sort_SemiJoin :
  forall f q1 q2, sort (Q_SemiJoin f q1 q2) =S= sort q1.
Proof.
intros f q1 q2; unfold Q_SemiJoin; 
  rewrite (sort_unfold (Q_Pi _ _)); apply Fset.equal_refl.
Qed.

Lemma mem_Q_SemiJoin :
  well_sorted_instance instance ->
  forall f q1 q2 t1,
    t1 inSE? eval_query (Q_SemiJoin f q1 q2) =
    (t1 inSE? (eval_query q1)) && 
       (Feset.exists_ _ (fun t2 => join_compatible_tuple T t1 t2 && 
                                      Bool.is_true _ (eval_formula f (join_tuple T t1 t2)))
                      (eval_query q2)).
Proof.
intros Wi f q1 q2 t1; unfold Q_SemiJoin; rewrite eq_bool_iff; split; intro H.
- rewrite Bool.Bool.andb_true_iff.
  destruct (mem_Q_Pi_rev _ _ _ H) as [t [Ht Ht1]].
  rewrite mem_Q_Sigma in Ht.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Ht)) as [u1 [u2 [Hu1 [Hu2 [Hj Hu]]]]].
  assert (Wu1 : labels T u1 =S= sort q1).
  {
    apply well_sorted_query; assumption.
  }
  assert (Kt1 : u1 =t= t1).
  {
    rewrite (Oeset.compare_eq_2 _ _ _ _ Ht1).
    rewrite tuple_eq, (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)), 
       (Fset.equal_eq_1 _ _ _ _ Wu1); split; [apply Fset.equal_refl | ].
    intros a Ha; rewrite (Fset.mem_eq_2 _ _ _ Wu1) in Ha.
    rewrite dot_mk_tuple, Ha.
    rewrite tuple_eq in Hu.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu)); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
    rewrite (Fset.mem_eq_2 _ _ _ Wu1), Ha; simpl.
    rewrite (proj2 Hu).
    - unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union, (Fset.mem_eq_2 _ _ _ Wu1), Ha.
      apply refl_equal.
    - rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu)); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
    rewrite (Fset.mem_eq_2 _ _ _ Wu1), Ha; apply refl_equal.
  }
  rewrite <- (Feset.mem_eq_1 _ _ _ _ Kt1); split; [assumption | ].
  rewrite Feset.exists_spec, existsb_exists.
  rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Hu2.
  destruct Hu2 as [t2 [Hu2 Ht2]].
  exists t2; rewrite Bool.Bool.andb_true_iff; repeat split; [assumption | | ].
  + rewrite <- Hj; apply sym_eq; apply join_compatible_tuple_eq; assumption.
  + rewrite <- (proj2 Ht); apply f_equal; apply sym_eq.
    apply eval_formula_eq; rewrite (Oeset.compare_eq_1 _ _ _ _ Hu).
    apply join_tuple_eq; assumption.
- rewrite Bool.Bool.andb_true_iff, Feset.exists_spec, existsb_exists in H.
  destruct H as [Ht1 [t2 [Ht2 H]]].
  rewrite Bool.Bool.andb_true_iff in H; destruct H as [Hj H].
  assert (Wt1 : labels T t1 =S= sort q1).
  {
    apply well_sorted_query; assumption.
  }
  assert (Kt1 : t1 =t= 
                mk_tuple 
                  T (sort q1) 
                  (fun a => if a inS? labels T (join_tuple T t1 t2)
                            then dot T (join_tuple T t1 t2) a 
                            else default_value T (type_of_attribute T a))).
  {
    rewrite tuple_eq, (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _));
      split; [assumption | ].
    intros a Ha; rewrite dot_mk_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ Wt1) in Ha; rewrite Ha; unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), dot_mk_tuple, Fset.mem_union.
    rewrite (Fset.mem_eq_2 _ _ _ Wt1), Ha; apply refl_equal.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Kt1); apply mem_Q_Pi.
  rewrite mem_Q_Sigma; split; [ | assumption].
  apply mem_Q_NaturalJoin; trivial.
  rewrite Feset.mem_elements; apply Oeset.in_mem_bool; assumption.
Qed.

(** Formula (9) *)
Lemma Join_SemiJoin :
  well_sorted_instance instance ->
  forall f q1 q2, 
    Q_Sigma f (Q_NaturalJoin q1 q2) =I=  Q_Sigma f (Q_NaturalJoin q1 (Q_SemiJoin f q2 q1)).
Proof.
intros Wi f q1 q2.
rewrite Feset.equal_spec; intro t; unfold Q_SemiJoin; rewrite eq_bool_iff; split; intro H.
- rewrite mem_Q_Sigma in H.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 H)) as [t1 [t2 [Ht1 [Ht2 [Hj Ht]]]]].
  rewrite mem_Q_Sigma; split; [ | apply (proj2 H)].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_NaturalJoin; trivial.
  assert (Wt2 : labels T t2 =S= sort q2).
  {
    apply well_sorted_query; assumption.
  }
  assert (Kt2 : t2 =t= 
                mk_tuple T (sort q2)
                         (fun a : attribute =>
                            if a inS? labels T t 
                            then dot T t a else default_value T (type_of_attribute T a))).
  {
    rewrite tuple_eq, (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)); split; [assumption | ].
    intros a Ha; rewrite dot_mk_tuple, <- (Fset.mem_eq_2 _ _ _ Wt2), Ha.
    rewrite tuple_eq in Ht.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha, 
       Bool.Bool.orb_true_r, (proj2 Ht).
    - unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union, Ha, Bool.orb_true_r.
      case_eq (a inS? labels T t1); intro Ha1; [ | apply refl_equal].
      apply sym_eq; rewrite join_compatible_tuple_alt in Hj; apply Hj; trivial.
    - rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ht)); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha.
      apply Bool.Bool.orb_true_r.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Kt2); apply mem_Q_Pi.
  rewrite mem_Q_Sigma; split; [ | apply (proj2 H)].
  rewrite (Feset.mem_eq_2 _ _ _ (NaturalJoin_comm q2 q1)); apply (proj1 H).
- rewrite mem_Q_Sigma in H.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 H)) as [t1 [t2 [Ht1 [Ht2 [Hj Ht]]]]].
  rewrite mem_Q_Sigma; split; [ | apply (proj2 H)].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_NaturalJoin; trivial.
  destruct (mem_Q_Pi_rev _ _ _ Ht2) as [u [Hu Kt2]].
  rewrite mem_Q_Sigma in Hu.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Hu)) as [u2 [u1 [Hu2 [Hu1 [Hj' Hu']]]]].
  assert (Jt2 : t2 =t= u2).
  {
    assert (Wu2 : labels T u2 =S= sort q2).
    {
      apply well_sorted_query; assumption.
    }
    rewrite (Oeset.compare_eq_1 _ _ _ _ Kt2), tuple_eq, 
       (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)), (Fset.equal_eq_2 _ _ _ _ Wu2).
    split; [apply Fset.equal_refl | ].
    intros a Ha; rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)) in Ha.
    rewrite dot_mk_tuple, Ha.
    rewrite tuple_eq in Hu'; rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu')), (proj2 Hu').
    - unfold join_tuple; rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), dot_mk_tuple.
      rewrite Fset.mem_union, (Fset.mem_eq_2 _ _ _ Wu2), Ha; apply refl_equal.
    - rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
      rewrite Fset.mem_union, (Fset.mem_eq_2 _ _ _ Wu2), Ha; apply refl_equal.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Jt2); assumption.
Qed.

(** Formula (10) *)
Lemma Join_SemiJoin2 :
  well_sorted_instance instance ->
  forall f1 f2 q1 q2 q3, 
    attributes_f f1 subS (sort q1 unionS sort q2) ->
    let q2' := Q_SemiJoin (Conj And_F f1 f2) q2 (Q_NaturalJoin q1 q3) in
    Q_SemiJoin f2 (Q_Sigma f1 (Q_NaturalJoin q1 q2)) q3 =I=
    Q_SemiJoin f2 (Q_Sigma f1 (Q_NaturalJoin q1 q2')) q3.
Proof.
intros Wi f1 f2 q1 q2 q3 Hf1 q2'; rewrite Fset.subset_spec in Hf1.
rewrite Feset.equal_spec; intro t; unfold q2', Q_SemiJoin; rewrite eq_bool_iff; split; intro H.
- rewrite (sort_unfold (Q_Sigma _ _)), (sort_unfold (Q_NaturalJoin _ _)), (sort_unfold (Q_Pi _ _)).
  rewrite (sort_unfold (Q_Sigma _ _)), (sort_unfold (Q_NaturalJoin _ _)) in H.
  destruct (mem_Q_Pi_rev _ _ _ H) as [u [Hu Ht]].
  rewrite mem_Q_Sigma in Hu.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Hu)) as [u12 [u3 [Hu12 [Hu3 [Hj Hu']]]]].
  rewrite mem_Q_Sigma in Hu12.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Hu12)) as [u1 [u2 [Hu1 [Hu2 [Hj' Hu12']]]]].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_Pi.
  rewrite mem_Q_Sigma; split; [ | apply (proj2 Hu)].
  rewrite (Feset.mem_eq_1 _ _ _ _ Hu'); apply mem_Q_NaturalJoin; trivial.
  rewrite mem_Q_Sigma; split; [ | apply (proj2 Hu12)].
  rewrite (Feset.mem_eq_1 _ _ _ _ Hu12'); apply mem_Q_NaturalJoin; trivial.
  assert (Wu1 : labels T u1 =S= sort q1).
  {
    apply well_sorted_query; assumption.
  }
  assert (Wu2 : labels T u2 =S= sort q2).
  {
    apply well_sorted_query; assumption.
  }
  assert (Ku2 : u2 =t= 
                mk_tuple T (sort q2)
                         (fun a : attribute =>
                            if a inS? labels T u 
                            then dot T u a else default_value T (type_of_attribute T a))).
  {
    rewrite tuple_eq, (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)); split; [assumption | ].
    intros a Ha; rewrite dot_mk_tuple, <- (Fset.mem_eq_2 _ _ _ Wu2), Ha.
    rewrite tuple_eq in Hu', Hu12'.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu')); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha.
    rewrite Bool.Bool.orb_true_r, Bool.Bool.orb_true_l.
    rewrite (proj2 Hu').
    - unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union.
      rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha, Bool.orb_true_r.
      rewrite (proj2 Hu12'); unfold join_tuple.
      + rewrite dot_mk_tuple, Fset.mem_union, Ha, Bool.Bool.orb_true_r.
        case_eq (a inS? labels T u1); intro Ha1; [ | apply refl_equal].
        apply sym_eq; rewrite join_compatible_tuple_alt in Hj'; apply Hj'; trivial.
      + rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
        rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha.
        apply Bool.Bool.orb_true_r.
    - rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
      rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union, Ha.
      rewrite Bool.Bool.orb_true_r; apply refl_equal.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Ku2).
  apply mem_Q_Pi; rewrite mem_Q_Sigma; split.
  + rewrite (Feset.mem_eq_1 _ _ _ _ Hu').
    rewrite (Feset.mem_eq_1 _ _ _ _ (join_tuple_eq_1 _ _ _ _ Hu12')).
    rewrite (Feset.mem_eq_1 _ _ _ _ (join_tuple_eq_1 _ _ _ _ (join_tuple_comm _ _ _ Hj'))).
    rewrite <- (Feset.mem_eq_1 _ _ _ _ (join_tuple_assoc _ _ _ _)).
    apply mem_Q_NaturalJoin; [ | | apply mem_Q_NaturalJoin]; trivial.
    * rewrite join_compatible_tuple_alt in *.
      intros a Ha2 Ha13; unfold join_tuple.
      rewrite dot_mk_tuple, Fset.mem_union.
      case_eq (a inS? labels T u1); intro Ha1.
      -- apply sym_eq; apply Hj'; trivial.
      -- rewrite tuple_eq in Hu12'.
         unfold join_tuple in Ha13; rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)) in Ha13.
         rewrite Fset.mem_union, Ha1, Bool.Bool.orb_false_l in Ha13.
         rewrite Ha13; rewrite <- Hj.
         ++ rewrite (proj2 Hu12').
            ** unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union.
               rewrite Ha1, Ha2; apply refl_equal.
            ** rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
               rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
               rewrite Ha1, Ha2; apply refl_equal.
         ++ rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
            rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
               rewrite Ha1, Ha2; apply refl_equal.
         ++ assumption.
    * rewrite join_compatible_tuple_alt in *.
      rewrite tuple_eq in Hu12'.
      intros a Ha1 Ha3; unfold join_tuple; rewrite <- Hj.
      -- rewrite (proj2 Hu12').
         ++ unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union, Ha1.
            apply refl_equal.
         ++ rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
            rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
            rewrite Ha1; apply refl_equal.
      -- rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
         rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
         rewrite Ha1; apply refl_equal.
      -- assumption.
  + rewrite Bool.true_is_true; simpl.
    rewrite Bool.andb_true_iff; split;
      [ | rewrite <- Bool.true_is_true; apply (proj2 Hu)].
    rewrite Bool.true_is_true in Hu12; rewrite <- (proj2 Hu12); apply eval_formula_eq_strong.
    intros a Ha.
    rewrite tuple_eq in *.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu')); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
    rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_1 _ _ _ _ Wu1)).
    rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_2 _ _ _ _ Wu2)).
    rewrite (Hf1 _ Ha); simpl.
    rewrite (proj2 Hu').
    * unfold join_tuple; rewrite dot_mk_tuple, Fset.mem_union.
      rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
      rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_1 _ _ _ _ Wu1)).
      rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_2 _ _ _ _ Wu2)).
      rewrite (Hf1 _ Ha); simpl; apply refl_equal.
    * rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
      rewrite (Fset.mem_eq_2 _ _ _ (proj1 Hu12')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
      rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_1 _ _ _ _ Wu1)).
      rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_2 _ _ _ _ Wu2)).
      rewrite (Hf1 _ Ha); apply refl_equal.
- destruct (mem_Q_Pi_rev _ _ _ H) as [u [Hu Ht]].
  rewrite 2 sort_unfold in Ht.
  rewrite (sort_unfold (Q_Pi _ _)) in Ht.
  rewrite mem_Q_Sigma in Hu.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Hu)) as [u12 [u3 [Hu12 [Hu3 [Hj Ku]]]]].
  rewrite mem_Q_Sigma in Hu12.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Hu12)) as [u1 [u2 [Hu1 [Hu2 [Hj' Ku12]]]]].
  destruct (mem_Q_Pi_rev _ _ _ Hu2) as [u' [Hu' Ku2]].
  rewrite mem_Q_Sigma in Hu'.
  destruct (mem_Q_NaturalJoin_rev _ _ _ (proj1 Hu')) as [u2' [u13' [Hu2' [Hu13' [Hj'' Ku']]]]].
  destruct (mem_Q_NaturalJoin_rev _ _ _ Hu13') as [u1' [u3' [Hu1' [Hu3' [Hj''' Ku13']]]]].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_Pi.
  rewrite mem_Q_Sigma; split; [ | apply (proj2 Hu)].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ku); apply mem_Q_NaturalJoin; trivial.
  rewrite mem_Q_Sigma; split; [ | apply (proj2 Hu12)].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ku12); apply mem_Q_NaturalJoin; trivial.
  assert (Ju2 : u2 =t= u2').
  {
    rewrite (Oeset.compare_eq_1 _ _ _ _ Ku2).
    rewrite tuple_eq in *.
    rewrite (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
    rewrite (Fset.equal_eq_2 _ _ _ _ (well_sorted_query Wi _ _ Hu2')).
    split; [apply Fset.equal_refl | ].
    intros a Ha; rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)) in Ha.
    rewrite dot_mk_tuple, Ha.
    rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ku')); unfold join_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
    rewrite (Fset.mem_eq_2 _ _ _ (well_sorted_query Wi _ _ Hu2')), Ha.
    rewrite (proj2 Ku'); unfold join_tuple.
    - rewrite dot_mk_tuple, Fset.mem_union.
      rewrite (Fset.mem_eq_2 _ _ _ (well_sorted_query Wi _ _ Hu2')), Ha; apply refl_equal.
    - rewrite (Fset.mem_eq_2 _ _ _ (proj1 Ku')); unfold join_tuple.
      rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Fset.mem_union.
      rewrite (Fset.mem_eq_2 _ _ _ (well_sorted_query Wi _ _ Hu2')), Ha.
      apply refl_equal.
  }
  rewrite (Feset.mem_eq_1 _ _ _ _ Ju2); assumption.
Qed.  
 
Lemma Sigma_Or :
  forall f1 f2 q, Q_Set Union (Q_Sigma f1 q) (Q_Sigma f2 q) =I= Q_Sigma (Conj Or_F f1 f2) q.
Proof.
intros f1 f2 q.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- rewrite <- mem_Q_Set_Union, !mem_Q_Sigma in H.
  rewrite mem_Q_Sigma, eval_formula_unfold; simpl.
  destruct H as [[H1 H2] | [H1 H2]]; (split; [assumption | ]).
  + rewrite Bool.true_is_true in H2; rewrite Bool.true_is_true, Bool.orb_true_iff.
    left; assumption.
  + rewrite Bool.true_is_true in H2; rewrite Bool.true_is_true, Bool.orb_true_iff.
    right; assumption.
  + apply Fset.equal_refl.
- rewrite mem_Q_Sigma in H; simpl in H.
  rewrite Bool.true_is_true, Bool.orb_true_iff in H.
  rewrite <- mem_Q_Set_Union, !mem_Q_Sigma, !Bool.true_is_true; [ | simpl; apply Fset.equal_refl].
  destruct H as [H1 [H2 | H2]]; [left | right]; split; trivial.
Qed.

Lemma Pi_Union_distr :
  forall s q1 q2, sort q1 =S= sort q2 ->
                  Q_Pi s (Q_Set Union q1 q2) =I= Q_Set Union (Q_Pi s q1) (Q_Pi s q2).
Proof.
intros s q1 q2 Hq.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- destruct (mem_Q_Pi_rev _ _ _ H) as [u [Hu Ht]].
  rewrite <- mem_Q_Set_Union in Hu; [ | assumption].
  rewrite (Feset.mem_eq_1 _ _ _ _ Ht), <- mem_Q_Set_Union; [ | simpl; apply Fset.equal_refl].
  destruct Hu as [Hu | Hu].
  + left; apply mem_Q_Pi; assumption.
  + right; apply mem_Q_Pi; assumption.
- rewrite <- mem_Q_Set_Union in H; [ | simpl; apply Fset.equal_refl].
  destruct H as [H | H]; destruct (mem_Q_Pi_rev _ _ _ H) as [u [Hu Ht]];
    rewrite (Feset.mem_eq_1 _ _ _ _ Ht); apply mem_Q_Pi;
      rewrite <- mem_Q_Set_Union; trivial.
  + left; assumption.
  + right; assumption.
Qed.

Lemma NaturalJoin_Inter :
  well_sorted_instance instance ->
  forall q1 q2, sort q1 =S= sort q2 -> Q_NaturalJoin q1 q2 =I= Q_Set Inter q1 q2.
Proof.
intros Wi q1 q2 Hq.
rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff; split; intro H.
- destruct (mem_Q_NaturalJoin_rev _ _ _ H) as [u1 [u2 [Hu1 [Hu2 [Hj Ht]]]]].
  assert (Wu1 : labels T u1 =S= sort q1).
  {
    apply well_sorted_query; assumption.
  }
  assert (Wu2 : labels T u2 =S= sort q2).
  {
    apply well_sorted_query; assumption.
  }
  assert (Kt : u1 =t= t).
  {
    rewrite (Oeset.compare_eq_2 _ _ _ _ Ht), tuple_eq; unfold join_tuple.
    rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
    split.
    - rewrite (Fset.equal_eq_2 _ _ _ _ (Fset.union_eq_2 _ _ _ _ Wu2)).
      rewrite <- (Fset.equal_eq_2 _ _ _ _ (Fset.union_eq_2 _ _ _ _ Hq)).
      rewrite <- (Fset.equal_eq_2 _ _ _ _ (Fset.union_eq_2 _ _ _ _ Wu1)).
      rewrite Fset.equal_spec; intro a; rewrite Fset.mem_union.
      rewrite Bool.orb_diag; apply refl_equal.
    - intros a Ha; rewrite dot_mk_tuple, Fset.mem_union, Ha.
      apply refl_equal.
  }
  assert (Kt' : u2 =t= t).
  {
    rewrite (Oeset.compare_eq_2 _ _ _ _ Ht), tuple_eq; unfold join_tuple.
    rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
    split.
    - rewrite (Fset.equal_eq_2 _ _ _ _ (Fset.union_eq_2 _ _ _ _ Wu2)).
      rewrite (Fset.equal_eq_2 _ _ _ _ (Fset.union_eq_1 _ _ _ _ Wu1)).
      rewrite (Fset.equal_eq_2 _ _ _ _ (Fset.union_eq_1 _ _ _ _ Hq)).
      rewrite (Fset.equal_eq_1 _ _ _ _ Wu2).
      rewrite Fset.equal_spec; intro a; rewrite Fset.mem_union.
      rewrite Bool.orb_diag; apply refl_equal.
    - intros a Ha; rewrite dot_mk_tuple, Fset.mem_union, Ha.
      rewrite (Fset.mem_eq_2 _ _ _ Wu1), Bool.orb_true_r.
      rewrite (Fset.mem_eq_2 _ _ _ Hq), <- (Fset.mem_eq_2 _ _ _ Wu2), Ha.
      apply sym_eq.
      rewrite join_compatible_tuple_alt in Hj; apply Hj; trivial.
      rewrite (Fset.mem_eq_2 _ _ _ Wu1), (Fset.mem_eq_2 _ _ _ Hq), <- (Fset.mem_eq_2 _ _ _ Wu2).
      assumption.
  }
  rewrite <- mem_Q_Set_Inter; [ | assumption].
  split.
  + rewrite <- (Feset.mem_eq_1 _ _ _ _ Kt); assumption.
  + rewrite <- (Feset.mem_eq_1 _ _ _ _ Kt'); assumption.
- rewrite <- mem_Q_Set_Inter in H; [ | assumption].
  assert (Kt : t =t= join_tuple T t t).
  {
    rewrite tuple_eq; unfold join_tuple.
    rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)); split.
    - rewrite Fset.equal_spec; intro a; rewrite Fset.mem_union, Bool.Bool.orb_diag.
      apply refl_equal.
    - intros a Ha; rewrite dot_mk_tuple, Fset.mem_union, Ha; apply refl_equal.
  }
  destruct H as [H1 H2]; rewrite (Feset.mem_eq_1 _ _ _ _ Kt); apply mem_Q_NaturalJoin; trivial.
  rewrite join_compatible_tuple_alt; intros; apply refl_equal.
Qed.  

(*



Lemma fold_left_NaturalJoin : 
  forall q1 q2 lq,
    fold_left (@Query_NaturalJoin _ _ _) (q2 :: lq) q1 =I=
    Query_NaturalJoin q1 (fold_left (@Query_NaturalJoin _ _ _) lq q2).
Proof.
intros q1 q2 lq.
revert q1 q2.
induction lq as [ | q lq]; intros q1 q2.
- rewrite Fset.equal_spec; intro; apply refl_equal.
- rewrite fold_left_unfold.
  rewrite (fset_equal_trans _ _ _ _ (IHlq _ _)).
  rewrite fold_left_unfold.
  rewrite (fset_equal_trans _ _ _ _ (NaturalJoin_assoc _ _ _)).
  apply NaturalJoin_compat_right.  
  rewrite fset_equal_sym.  
  apply (IHlq q2 q).
Qed.

Lemma mem_N_ary_Query_NaturalJoin : 
  forall q1 lq,
    N_ary_Query_NaturalJoin (q1 :: lq) =I=
    Query_NaturalJoin q1 (N_ary_Query_NaturalJoin lq).
Proof.
intros q1 lq; rewrite Fset.equal_spec; intro t.
unfold N_ary_Query_NaturalJoin.
set (q0 := Query_Empty_Tuple T DBS predicate) in *.
clearbody q0.
revert t q0 q1; induction lq as [ | q lq]; intros t q0 q1; simpl.
- assert (Aux := NaturalJoin_comm q0 q1).
  rewrite Fset.equal_spec in Aux; apply Aux.
- rewrite <- IHlq.
  simpl.
  assert (Aux : (Query_NaturalJoin (Query_NaturalJoin q0 q1) q) =I=
                (Query_NaturalJoin (Query_NaturalJoin q0 q) q1)).
  {
    rewrite (fset_equal_trans _ _ _ _ (NaturalJoin_assoc _ _ _)).
    rewrite fset_equal_sym.
    rewrite (fset_equal_trans _ _ _ _ (NaturalJoin_assoc _ _ _)).
    apply NaturalJoin_compat_right.
    apply NaturalJoin_comm.
  }
  set (q2 := Query_NaturalJoin (Query_NaturalJoin q0 q1) q) in *.
  clearbody q2.
  set (q3 := Query_NaturalJoin (Query_NaturalJoin q0 q) q1) in *.
  clearbody q3.
  clear q0 q1 q IHlq.
  revert t q2 q3 Aux; induction lq as [ | q1 lq]; intros t q2 q3 Aux; simpl.
  + rewrite Fset.equal_spec in Aux; apply Aux.
  + apply IHlq.
    rewrite (fset_equal_trans _ _ _ _ (NaturalJoin_comm _ _)).
    rewrite fset_equal_sym.
    rewrite (fset_equal_trans _ _ _ _ (NaturalJoin_comm _ _)).
    apply NaturalJoin_compat_right.
    rewrite fset_equal_sym; apply Aux.
Qed.    




(** The formula 2.12 p.18 of the book "Building Query Compilers" by Guido 
      Moerkotte becomes false when the plain jointure (i.e. parameterized by a logical 
      formula) is replaced by the natural jointure. Hence the following lemma DOES NOT 
      hold :#<br>#
<<
[Lemma Pi_NaturalJoin_distr :
  forall fa fa1 fa2 r1 r2, 
  Fset.equal (A T) fa (Fset.union (A T) fa1 fa2) = true ->
  Fset.subset (A T) fa1 (sort r1) = true ->
  Fset.subset (A T) fa2 (sort r2) = true ->
  Fset.subset (A T) (Fset.inter (A T) (sort r1) (sort r2)) fa = true ->
  Pi fa (NaturalJoin r1 r2) === NaturalJoin (Pi fa1 r1) (Pi fa2 r2).]
>>
#<br>#
The reason is that when the natural join is expressed by the plain join, 
the logical formulas are not the same for each side of the identity. 
#<br>#
The counter-example is the following : #<br>#
<<
[sort (r1) = {A, B, C, D}
sort (r2) =     {B, C, D, E}
fa1 =               {B, C}
fa2 =                   {C, D}
r1 =          {(1, 2, 3, 4)}
r2 =              {(1, 3, 5, 6)]
>>
[(Pi fa (NaturalJoin r1 r2))] is empty, since so is [(NaturalJoin r1 r2)]. #<br>#
[(Pi fa1 r1)] is equal to [{(B = 2, C= 3)}],#<br>#
[(Pi fa2 r2)] is equal to            [{(C=3, D=5)}]#<br>#
and [NaturalJoin (Pi fa1 r1) (Pi fa2 r2)] is equal to [{(B= 2, C= 3, D = 5)}].#<br>#
#<br>#
The lemma is nevertheless true when the natural jointure is actually a cartesian product,
i.e. when both queryations do not share common attributes.
*)

Lemma Pi_NaturalJoin_distr :
  well_sorted_instance T DBS I ->
  forall W W1 W2 q1 q2, 
  W =S= (W1 unionS W2) ->
  W1 subS (sort q1) ->
  W2 subS (sort q2) ->
  (sort q1 interS sort q2) subS (W1 interS W2) ->
  Query_Pi W (Query_NaturalJoin q1 q2) =I= Query_NaturalJoin (Query_Pi W1 q1) (Query_Pi W2 q2).
Proof.
intros Ws fa fa1 fa2 r1 r2 Hf Hf1 Hf2 Kf.
rewrite Fset.equal_spec; intro e.
rewrite equiv_bool, mem_NaturalJoin, mem_Pi; split.
- (* 1/2 *)
  intros [t1 [t2 [Ht1 [Ht2 [H K]]]]]; 
  rewrite tuple_eq in K; destruct K as [K J].
  rewrite mem_Pi in Ht1; 
    destruct Ht1 as [s1 [Hs1 Ks1]]; 
    rewrite tuple_eq in Ks1; destruct Ks1 as [Ks1 Js1].
  rewrite mem_Pi in Ht2; destruct Ht2 as [s2 [Hs2 Ks2]]; 
  rewrite tuple_eq in Ks2; destruct Ks2 as [Ks2 Js2].
  exists (join_tuple T s1 s2).
  rewrite Fset.equal_spec in Ks1, Ks2, K.
  split.
  + (* 1/3 *)
    rewrite mem_NaturalJoin.
    exists s1; exists s2.
    split; [assumption | ].
    split; [assumption | ].
    split; [ | unfold Fset.elt_compare; apply compare_eq_refl].
    unfold join_compatible_tuple.
    rewrite Fset.for_all_spec, forallb_forall.
    intros a Ha.
    assert (Ha1 : a inS support T t1).
    {
      rewrite Ks1, mem_support_mk_tuple.
      generalize (in_impl_mem_bool (OAttr T) _ _ Ha).
      rewrite in_listA, <- Fset.mem_elements.
      intro Ka.
      rewrite Fset.mem_inter, Bool.andb_true_iff; split.
      - assert (W1 := well_sorted_query OP interp_pred Ws _ _ Hs1).
        assert (W2 := well_sorted_query OP interp_pred Ws _ _ Hs2).
        rewrite Fset.equal_spec in W1, W2.
        rewrite Fset.mem_inter, W1, W2, <- Fset.mem_inter in Ka.
        rewrite Fset.subset_spec in Kf.
        generalize (Kf _ Ka).
        rewrite Fset.mem_inter, Bool.andb_true_iff.
        apply proj1.
      - rewrite Fset.mem_inter, Bool.andb_true_iff in Ka.
        apply (proj1 Ka).
    }
    assert (Ha2 : a inS support T t2).
    {
      rewrite Ks2, mem_support_mk_tuple.
      generalize (in_impl_mem_bool (OAttr T) _ _ Ha).
      rewrite in_listA, <- Fset.mem_elements.
      intro Ka.
      rewrite Fset.mem_inter, Bool.andb_true_iff; split.
      - assert (W1 := well_sorted_query OP interp_pred Ws _ _ Hs1).
        assert (W2 := well_sorted_query OP interp_pred Ws _ _ Hs2).
        rewrite Fset.equal_spec in W1, W2.
        rewrite Fset.mem_inter, W1, W2, <- Fset.mem_inter in Ka.
        rewrite Fset.subset_spec in Kf.
        generalize (Kf _ Ka).
        rewrite Fset.mem_inter, Bool.andb_true_iff.
        apply proj2.
      - rewrite Fset.mem_inter, Bool.andb_true_iff in Ka.
        apply (proj2 Ka).
    }
    assert (Aux : dot T s1 a = dot T s2 a).
    {
      apply trans_eq with (dot T t1 a).
      - rewrite Js1, dot_mk_tuple.
        + apply refl_equal.
        + rewrite Ks1, mem_support_mk_tuple in Ha1. 
          apply Ha1.
        + assumption.
      - apply trans_eq with (dot T t2 a).
        + unfold join_compatible_tuple in H.
          rewrite Fset.for_all_spec, forallb_forall in H.
          assert (Ka : List.In a ({{{support T t1 interS support T t2}}})).
          {
            apply (mem_bool_impl_in (OAttr T)).
            rewrite in_listA, <- Fset.mem_elements.
            rewrite Fset.mem_inter, Ha1, Ha2.
            apply refl_equal.
          }
          generalize (Oset.eq_bool_ok (OVal T) (dot T t1 a) (dot T t2 a)).
          rewrite (remove_match _ (H _ Ka)).
          exact (fun h => h).
        + rewrite Js2, dot_mk_tuple.
          * apply refl_equal.
          * rewrite Ks2, mem_support_mk_tuple in Ha2. 
            apply Ha2.
          * assumption.
    }
    rewrite Aux, Oset.compare_eq_refl.
    apply refl_equal.
  + (* 1/2 *)
    rewrite tuple_eq; split.
    * (* 1/3 *)
      rewrite Fset.equal_spec; intro x.
      rewrite mem_support_mk_tuple, K.
      unfold join_tuple.
      rewrite Fset.mem_inter, 2 mem_support_mk_tuple.
      rewrite 2 Fset.mem_union.
      rewrite Ks1, Ks2.
      rewrite 2 mem_support_mk_tuple.
      rewrite Fset.equal_spec in Hf; rewrite Hf.
      rewrite 2 Fset.mem_inter.
      rewrite Fset.mem_union.
      {
        case_eq (x inS? fa1); intro Hx.
        - simpl.
          rewrite Fset.subset_spec in Hf1.
          assert (Aux := well_sorted_query OP interp_pred Ws _ _ Hs1).
          rewrite Fset.equal_spec in Aux.
          rewrite Aux, (Hf1 _ Hx); apply refl_equal.
        - case_eq (x inS? fa2); intro Kx; [ | apply refl_equal] .
          simpl.
          rewrite Fset.subset_spec in Hf2.
          assert (Aux := well_sorted_query OP interp_pred Ws _ _ Hs2).
          rewrite Fset.equal_spec in Aux.
          rewrite Aux, (Hf2 _ Kx), Bool.orb_true_r; apply refl_equal.
      }
    * (* 1/2 *)
      intros a Ha.
      rewrite (J _ Ha).
      unfold join_tuple.
      {
        rewrite 3 dot_mk_tuple.
        - rewrite K in Ha.
          unfold join_tuple in Ha.
          rewrite mem_support_mk_tuple, Fset.mem_union, Ks1, Ks2 in Ha. 
          rewrite 2 mem_support_mk_tuple, 2 Fset.mem_inter in Ha.
          case_eq (a inS? support T t1); intro Ka.
          + rewrite (Js1 _ Ka), dot_mk_tuple.
            * rewrite Ks1, mem_support_mk_tuple, Fset.mem_inter, Bool.andb_true_iff in Ka.
              rewrite (proj2 Ka); apply refl_equal.
            * rewrite Ks1, mem_support_mk_tuple in Ka.
              assumption.
          + rewrite Ks1, mem_support_mk_tuple, Fset.mem_inter in Ka.
            case_eq (a inS? fa1); intro Ja; rewrite Ja in Ha, Ka.
            * simpl in Ka.
              assert (Aux := well_sorted_query OP interp_pred Ws _ _ Hs1).
              rewrite Fset.equal_spec in Aux; rewrite Aux in Ka.
              rewrite Fset.subset_spec in Hf1; rewrite (Hf1 _ Ja) in Ka.
              discriminate Ka.
            * simpl in Ha.
              rewrite Bool.andb_true_iff in Ha.
              destruct Ha as [Ha1 Ha2].
              {
                case_eq (a inS? support T s1); intro La.
                - assert (Aux1 := well_sorted_query OP interp_pred Ws _ _ Hs1).
                  assert (Aux2 := well_sorted_query OP interp_pred Ws _ _ Hs2).
                  rewrite Fset.equal_spec in Aux1, Aux2.
                  rewrite Aux1 in La; rewrite Aux2 in Ha2.
                  rewrite Fset.subset_spec in Kf.
                  generalize (Kf a); rewrite 2 Fset.mem_inter, Ha2, La.
                  intro Aux; generalize (Aux (refl_equal _)); clear Aux; intro Aux.
                  rewrite Ja in Aux; discriminate Aux.
                - rewrite Js2, dot_mk_tuple; 
                  [apply refl_equal | | rewrite Ks2, mem_support_mk_tuple];
                  rewrite Fset.mem_inter, Ha1, Ha2; apply refl_equal.
              }
        - rewrite K in Ha.
          unfold join_tuple in Ha.
          rewrite mem_support_mk_tuple, Fset.mem_union, Ks1, Ks2 in Ha.
          rewrite 2 mem_support_mk_tuple, Bool.orb_true_iff in Ha.
          destruct Ha as [Ha | Ha].
          + rewrite Fset.mem_inter, Bool.andb_true_iff in Ha.
            rewrite Fset.mem_union, (proj2 Ha).
            apply refl_equal.
          + rewrite Fset.mem_inter, Bool.andb_true_iff in Ha.
            rewrite Fset.mem_union, (proj2 Ha), Bool.orb_true_r.
            apply refl_equal.
        - rewrite K in Ha.
          unfold join_tuple in Ha.
          rewrite mem_support_mk_tuple, Fset.mem_union, Ks1, Ks2 in Ha.
          rewrite 2 mem_support_mk_tuple, Bool.orb_true_iff in Ha.
          rewrite Fset.equal_spec in Hf.
          rewrite Fset.mem_inter, Hf, mem_support_mk_tuple, 2 Fset.mem_union.
          destruct Ha as [Ha | Ha];
            rewrite Fset.mem_inter, Bool.andb_true_iff in Ha;
            rewrite (proj1 Ha), (proj2 Ha).
          + apply refl_equal.
          + rewrite 2 Bool.orb_true_r.
            apply refl_equal.
        - rewrite K in Ha.
          unfold join_tuple in Ha.
          rewrite mem_support_mk_tuple, Fset.mem_union, Ks1, Ks2 in Ha.
          rewrite 2 mem_support_mk_tuple in Ha.
          rewrite Fset.mem_union, Ks1, Ks2, 2 mem_support_mk_tuple.
          apply Ha.
      }
- (* 1/1 *)
  intros [t [Ht Kt]].
  rewrite mem_NaturalJoin in Ht.
  destruct Ht as [t1 [t2 [Ht1 [Ht2 [H K]]]]].
  exists (mk_tuple T (Fset.inter (A T) fa1 (sort r1)) (dot T t1)).
  exists (mk_tuple T (Fset.inter (A T) fa2 (sort r2)) (dot T t2)).
  repeat split.
  + rewrite mem_Pi.
    exists t1; split; [assumption | ].
    rewrite mk_tuple_eq; 
      split; [ | intros; apply refl_equal].
    rewrite Fset.equal_spec; intro x; rewrite 2 Fset.mem_inter.
    apply f_equal.
    assert (W := well_sorted_query OP interp_pred Ws _ _ Ht1).
    rewrite Fset.equal_spec in W; rewrite W.
    apply refl_equal.
  + rewrite mem_Pi.
    exists t2; split; [assumption | ].
    rewrite mk_tuple_eq; 
      split; [ | intros; apply refl_equal].
    rewrite Fset.equal_spec; intro x; rewrite 2 Fset.mem_inter.
    apply f_equal.
    assert (W := well_sorted_query OP interp_pred Ws _ _ Ht2).
    rewrite Fset.equal_spec in W; rewrite W.
    apply refl_equal.
  + revert H.
    unfold join_compatible_tuple.
    rewrite 2 Fset.for_all_spec, 2 forallb_forall.
    intros H x Hx.
    generalize (in_impl_mem_bool (Fset.OElt (A T)) _ _ Hx).
    clear Hx; intro Hx.
    rewrite <- Fset.mem_elements, Fset.mem_inter, 2 mem_support_mk_tuple in Hx.
    rewrite 2 dot_mk_tuple.
    * apply H.
      apply (mem_bool_impl_in (OAttr T)).
      rewrite in_listA, <- Fset.mem_elements, Fset.mem_inter.
      rewrite 2 Fset.mem_inter, 3 Bool.andb_true_iff in Hx.
      assert (Aux1 := well_sorted_query OP interp_pred Ws _ _ Ht1).
      assert (Aux2 := well_sorted_query OP interp_pred Ws _ _ Ht2).
      rewrite Fset.equal_spec in Aux1, Aux2.
      rewrite Aux1, Aux2, (proj2 (proj1 Hx)), (proj2 (proj2 Hx)).
      apply refl_equal.
    * rewrite Bool.andb_true_iff in Hx.
      apply (proj2 Hx).
    * rewrite Bool.andb_true_iff in Hx.
      apply (proj1 Hx).
  + apply (Eoset.compare_eq_trans _ _ _ _ Kt).
    unfold join_tuple in *; rewrite tuple_eq_, Fset.equal_spec; split.
    * intro a; rewrite mem_support_mk_tuple, Fset.mem_inter.
      rewrite mem_support_mk_tuple, Fset.mem_union, 2 mem_support_mk_tuple.
      rewrite 2 Fset.mem_inter.
      rewrite tuple_eq, Fset.equal_spec in K; rewrite (proj1 K).
      rewrite Fset.equal_spec in Hf; rewrite Hf.
      rewrite mem_support_mk_tuple, 2 Fset.mem_union.
      assert (Aux1 := well_sorted_query OP interp_pred Ws _ _ Ht1).
      assert (Aux2 := well_sorted_query OP interp_pred Ws _ _ Ht2).
      rewrite Fset.equal_spec in Aux1, Aux2.
      rewrite Aux1, Aux2.
      {
        case_eq (a inS? fa1); intro Ha; simpl.    
        - rewrite Fset.subset_spec in Hf1.
          rewrite (Hf1 _ Ha); apply refl_equal.
        - case_eq (a inS? fa2); intro Ja; simpl.    
          + rewrite Fset.subset_spec in Hf2.
            rewrite (Hf2 _ Ja), Bool.orb_true_r; apply refl_equal.
          + apply refl_equal.
      } 
    * intros a Ha.
      rewrite mem_support_mk_tuple in Ha.
      rewrite (dot_mk_tuple _ _ _ _ Ha).
      rewrite tuple_eq, Fset.equal_spec in K.
      rewrite Fset.mem_inter, Bool.andb_true_iff in Ha.
      rewrite ((proj2 K) _ (proj2 Ha)).
      assert (Aux1 := well_sorted_query OP interp_pred Ws _ _ Ht1).
      assert (Aux2 := well_sorted_query OP interp_pred Ws _ _ Ht2).
      rewrite Fset.equal_spec in Aux1, Aux2.
      {
        rewrite 2 dot_mk_tuple, mem_support_mk_tuple, Fset.mem_inter.
        - rewrite (proj1 K), mem_support_mk_tuple, Fset.mem_union in Ha. 
          rewrite Fset.equal_spec in Hf; rewrite Hf, Fset.mem_union in Ha.
          case_eq (a inS? support T t1); 
            intro Ka; rewrite Ka in Ha; simpl in Ha.
          + rewrite <- Aux1, Ka, Bool.andb_true_r.
            case_eq (a inS? fa1); intro Ja.
            * rewrite dot_mk_tuple; [apply refl_equal | ].
              rewrite Fset.mem_inter, <- Aux1, Ka, Ja.
              apply refl_equal.
            * rewrite Ja in Ha; simpl in Ha.
              rewrite Fset.subset_spec in Hf2, Kf.
              assert (La := Hf2 _ (proj1 Ha)).
              rewrite Aux1 in Ka.
              assert (Abs := Kf a).
              rewrite 2 Fset.mem_inter, Ka, La in Abs.
              generalize (Abs (refl_equal _)); clear Abs; intro Abs.
              rewrite Ja in Abs; discriminate Abs.
          + rewrite <- Aux1, Ka, Bool.andb_false_r.
            case_eq (a inS? fa1); intro Ja.
            * rewrite Fset.subset_spec in Hf1.
              rewrite Aux1, (Hf1 _ Ja) in Ka.
              discriminate Ka.
            * rewrite dot_mk_tuple; [apply refl_equal | ].
              rewrite Fset.mem_inter, <- Aux2, (proj2 Ha).
              rewrite Ja, Bool.orb_false_l in Ha.
              rewrite (proj1 Ha); apply refl_equal.
        - rewrite Fset.mem_union, 2 mem_support_mk_tuple.
          rewrite Fset.equal_spec in Hf; rewrite Hf, Fset.mem_union in Ha.
          rewrite 2 Fset.mem_inter.
          case_eq (a inS? fa1); intro Ja; rewrite Ja in Ha.
          + rewrite Fset.subset_spec in Hf1.
            rewrite (Hf1 _ Ja); apply refl_equal.
          + simpl in Ha; rewrite (proj1 Ha).
            rewrite Fset.subset_spec in Hf2.
            rewrite (Hf2 _ (proj1 Ha)); apply refl_equal.
        - rewrite (proj1 K), mem_support_mk_tuple in Ha.
          apply (proj2 Ha).
      }
Qed.

Lemma mem_cartesian_product :
 well_sorted_instance T DBS I ->
 forall (W1 W2 : setA) (q : query T DBS predicate), sort q =S= (W1 unionS W2) ->
   (forall x y : tuple T,
    x inI q ->
    y inI q ->
    exists u : attribute T -> value T,
      exists v : attribute T -> value T,
        (forall a : attribute T, a inS W1 -> u a = dot T x a) /\
        (forall a : attribute T, a inS W2 -> u a = dot T y a) /\
        (forall a : attribute T, a inS W1 -> v a = dot T y a) /\
        (forall a : attribute T, a inS W2 -> v a = dot T x a) /\
        mk_tuple T (sort q) u inI q /\ mk_tuple T (sort q) v inI q) ->
   q =I= Query_NaturalJoin (Query_Pi W1 q) (Query_Pi W2 q).
Proof.
intros Ws fa1 fa2 r Hr H; rewrite Fset.equal_spec; intro x; rewrite equiv_bool.
assert (Hs1 : Fset.subset (A T) fa1 (sort r) = true).
{
  rewrite Fset.equal_spec in Hr.
  rewrite Fset.subset_spec; 
    intros a Ha; 
    rewrite Hr, Fset.mem_union, Ha; apply refl_equal.
}
assert (Hs2 : Fset.subset (A T) fa2 (sort r) = true).
{
  rewrite Fset.equal_spec in Hr.
  rewrite Fset.subset_spec; 
    intros a Ha; 
    rewrite Hr, Fset.mem_union, Ha, Bool.orb_true_r; apply refl_equal.
}
split; intro Hx.
- (* 1/2 *)
  rewrite mem_NaturalJoin in Hx.
  destruct Hx as [a1 [a2 [Ha1 [Ha2 [Hx J]]]]].
  rewrite mem_Pi in Ha1, Ha2.
  destruct Ha1 as [x1 [Ha1 Ka1]]; destruct Ha2 as [x2 [Ha2 Ka2]].
  destruct (H _ _ Ha1 Ha2) as [u [v [H1 [H2 [H3 [H4 [H5 H6]]]]]]].
  rewrite (mem_compat (FTuple T) x (mk_tuple T (sort r) u) (eval_query _ _ I r)); [assumption | ].
  refine (Eoset.compare_eq_trans (Fset.OElt (FTuple T)) _ _ _ J _).
  rewrite tuple_eq_; split.
  + unfold join_tuple.
    rewrite Fset.equal_spec in Hr.
    rewrite Fset.equal_spec.
    intros a; rewrite 2 mem_support_mk_tuple, Hr, 2 Fset.mem_union.
    apply f_equal2.
    * rewrite tuple_eq, Fset.equal_spec in Ka1.
      rewrite (proj1 Ka1), mem_support_mk_tuple, Fset.mem_inter.
      case_eq (a inS? fa1); intro Ha; [ | apply refl_equal].
      rewrite Fset.subset_spec in Hs1.
      assert (Aux := well_sorted_query OP interp_pred Ws _ _ Ha1).
      rewrite Fset.equal_spec in Aux.
      rewrite Aux, (Hs1 _ Ha); apply refl_equal.
    * rewrite tuple_eq, Fset.equal_spec in Ka2.
      rewrite (proj1 Ka2), mem_support_mk_tuple, Fset.mem_inter.
      case_eq (a inS? fa2); intro Ha; [ | apply refl_equal].
      rewrite Fset.subset_spec in Hs2.
      assert (Aux := well_sorted_query OP interp_pred Ws _ _ Ha2).
      rewrite Fset.equal_spec in Aux.
      rewrite Aux, (Hs2 _ Ha); apply refl_equal.
  + unfold join_tuple.
    intros a Ha; rewrite mem_support_mk_tuple, Fset.mem_union in Ha.
    case_eq (a inS? support T a1); intro Ka; rewrite Ka in Ha.
    * {
        rewrite tuple_eq, Fset.equal_spec in Ka1.
        rewrite 2 dot_mk_tuple, Ka.
        - rewrite ((proj2 Ka1) _ Ka), dot_mk_tuple.
          + apply sym_eq; apply H1.      
            rewrite (proj1 Ka1), mem_support_mk_tuple, Fset.mem_inter, Bool.andb_true_iff in Ka.
            apply (proj1 Ka).
          + rewrite (proj1 Ka1), mem_support_mk_tuple in Ka.
            apply Ka.
        - rewrite (proj1 Ka1), mem_support_mk_tuple, Fset.mem_inter, Bool.andb_true_iff in Ka.
          rewrite Fset.subset_spec in Hs1.
          apply Hs1; apply (proj1 Ka).
        - rewrite Fset.mem_union, Ka; apply refl_equal.
      }
    * simpl in Ha.
      {
        rewrite tuple_eq, Fset.equal_spec in Ka2.
        rewrite 2 dot_mk_tuple, Ka.
        - rewrite ((proj2 Ka2) _ Ha), dot_mk_tuple.
          + apply sym_eq; apply H2.      
            rewrite (proj1 Ka2), mem_support_mk_tuple, Fset.mem_inter, Bool.andb_true_iff in Ha.
            apply (proj1 Ha).
          + rewrite (proj1 Ka2), mem_support_mk_tuple in Ha.
            apply Ha.
        - rewrite (proj1 Ka2), mem_support_mk_tuple, Fset.mem_inter, Bool.andb_true_iff in Ha.
          rewrite Fset.subset_spec in Hs2.
          apply Hs2; apply (proj1 Ha).
        - rewrite Fset.mem_union, Ha, Bool.orb_true_r; apply refl_equal.
      }
- rewrite mem_NaturalJoin.
  assert (Aux := well_sorted_query OP interp_pred Ws _ _ Hx).
  rewrite Fset.equal_spec in Aux.
  exists (mk_tuple T fa1 (dot T x)); exists (mk_tuple T fa2 (dot T x)); repeat split.
  + rewrite mem_Pi.
    exists x; split; [assumption | ].
    rewrite mk_tuple_eq; split.
    * rewrite Fset.equal_spec.
      rewrite Fset.subset_spec in Hs1.
      intro a; rewrite Fset.mem_inter.
      case_eq (a inS? fa1); [ | intros _; apply refl_equal].
      intro Ha; rewrite Aux, (Hs1 _ Ha).
      apply refl_equal.
    * exact (fun _ _ => refl_equal _).
  + rewrite mem_Pi.
    exists x; split; [assumption | ].
    rewrite mk_tuple_eq; split.
    * rewrite Fset.equal_spec.
      rewrite Fset.subset_spec in Hs2.
      intro a; rewrite Fset.mem_inter.
      case_eq (a inS? fa2); [ | intros _; apply refl_equal].
      intro Ha; rewrite Aux, (Hs2 _ Ha).
      apply refl_equal.
    * exact (fun _ _ => refl_equal _).
  +  unfold join_compatible_tuple.
     rewrite Fset.for_all_spec, forallb_forall.
     intros a Ha.
     generalize (in_impl_mem_bool (Fset.OElt (A T)) _ _ Ha).
     rewrite <- Fset.mem_elements.
     rewrite Fset.mem_inter, 2 mem_support_mk_tuple, Bool.andb_true_iff.
     clear Ha; intros [Ha1 Ha2].
     rewrite 2 dot_mk_tuple, Oset.compare_eq_refl; trivial.
  + unfold join_tuple.
    rewrite tuple_eq, Fset.equal_spec; split.
    * intros a; rewrite mem_support_mk_tuple, Fset.mem_union, 2 mem_support_mk_tuple, Aux.
      rewrite Fset.equal_spec in Hr; rewrite Hr.
      rewrite Fset.mem_union; apply refl_equal.
    * rewrite Fset.equal_spec in Hr.
      intros a Ha.
      {
        rewrite dot_mk_tuple.
        - rewrite mem_support_mk_tuple.
          case_eq (a inS? fa1); intro Ka.
          + rewrite dot_mk_tuple.
            * apply refl_equal.
            * assumption.
          + rewrite Aux, Hr, Fset.mem_union, Ka in Ha.
            simpl in Ha.
            rewrite dot_mk_tuple.
            * apply refl_equal.
            * assumption.
        - rewrite Aux, Hr, Fset.mem_union in Ha.
          rewrite Fset.mem_union, 2 mem_support_mk_tuple.
          apply Ha.
      } 
Qed.
*)
End Sec.


(*
=====

Lemma 
(* begin hide *)
Section Compare.

Inductive All : Type :=
  | All_relname : relname  -> All
  | All_renaming : list (attribute * attribute) -> All
  | All_N : N -> All
  | All_value : value -> All
  | All_attribute : attribute -> All
  | All_predicate : predicate -> All.

Definition tree_of_attribute (a : attribute) : tree All := Leaf (All_attribute a).

Definition tree_of_symbol (f : symbol) : tree All :=
  match f with
    | Constant c => Leaf (All_value c)
    | Dot a => Leaf (All_attribute a)
  end.

Fixpoint tree_of_query (r1 : query) : tree All :=
  match r1 with
  | Query_Empty s => Node 0 (map tree_of_attribute s)
  | Query_Empty_Tuple => Node 1 nil
  | Query_Basename r1 => Node 2 (Leaf (All_relname r1) :: nil)
  | Query_Sigma f1 r1 => 
    let tree_of_formula :=
        (fix tree_of_formula (f1 : formula query predicate symbol) : tree All :=
           let tree_of_var x :=
               match x with
                 | Vrbl q n => Node 3 (tree_of_query q :: Leaf (All_N n) :: nil) 
               end in
           let tree_of_term :=
               (fix tree_of_term (t1 : term query symbol) : tree All :=
                  match t1 with
                    | Var x => Node 4 (tree_of_var x :: nil)
                    | Term f l => Node 5 (tree_of_symbol f :: map tree_of_term l)
                  end) in
           match f1 with
             | TTrue => Node 6 (Leaf (All_N 0):: nil) 
             | Atom p l => Node 7 (Leaf (All_predicate p) :: map tree_of_term l)
             | Conj c f1 g1 => 
               Node (match c with And_F => 8 | Or_F => 9 end)
                    (tree_of_formula f1 :: tree_of_formula g1 :: nil)
             | Not f1 => Node 10 (tree_of_formula f1 :: nil)
             | Quant q x1 f1 => 
               Node (match q with Forall_F => 11 | Exists_F => 12 end)
                    (tree_of_var x1 :: tree_of_formula f1 :: nil)
           end) in
    Node 13 (tree_of_formula f1 :: tree_of_query r1 :: nil)
  | Query_Pi fa1 r1 => Node 14 (tree_of_query r1 :: map tree_of_attribute fa1)
  | Query_NaturalJoin s1 r1 => Node 15 (tree_of_query s1 :: tree_of_query r1 :: nil)
  | Query_Set o s1 r1 => 
    Node (match o with
            | Union => 16
            | Inter => 17
            | Diff => 18
            | UnionMax => 20
          end) (tree_of_query s1 :: tree_of_query r1 :: nil)
  | Query_Rename rho r1 => 
    Node 19 ((Leaf (All_renaming rho)) :: tree_of_query r1 :: nil)
  end.

Definition tree_of_var (v : variable query) : tree All :=
  match v with
    | Vrbl q n => Node 3 (tree_of_query q :: Leaf (All_N n) :: nil)
  end.

Fixpoint tree_of_term (t : term query symbol) : tree All :=
  match t with
    | Var v => Node 4 (tree_of_var v :: nil)
    | Term f l => Node 5 (tree_of_symbol f :: map tree_of_term l)
  end.

Fixpoint tree_of_formula (f : formula query predicate symbol) : tree All :=
  match f with
    | TTrue => Node 6 (Leaf (All_N 0):: nil) 
    | Atom p l => Node 7 (Leaf (All_predicate p) :: map tree_of_term l)
    | Conj c f1 g1 => 
      Node (match c with And_F => 8 | Or_F => 9 end)
           (tree_of_formula f1 :: tree_of_formula g1 :: nil)
    | Not f1 => Node 10 (tree_of_formula f1 :: nil)
    | Quant q x1 f1 => 
      Node (match q with Forall_F => 11 | Exists_F => 12 end)
           (tree_of_var x1 :: tree_of_formula f1 :: nil)
  end.

Lemma tree_of_var_unfold :
  forall v, tree_of_var v =
            match v with
              | Vrbl q n => Node 3 (tree_of_query q :: Leaf (All_N n) :: nil)
            end.
Proof.
intro v; case v; intros; apply refl_equal.
Qed.

Lemma tree_of_term_unfold :
  forall t, tree_of_term t = 
            match t with
              | Var x => Node 4 (tree_of_var x :: nil)
              | Term f l => Node 5 (tree_of_symbol f :: map tree_of_term l)
            end.
Proof.
intro t; case t; intros; apply refl_equal.
Qed.

Lemma tree_of_formula_unfold :
  forall f, tree_of_formula f = 
  match f with
    | TTrue => Node 6 (Leaf (All_N 0):: nil) 
    | Atom p l => Node 7 (Leaf (All_predicate p) :: (map tree_of_term l))
    | Conj c f1 g1 => 
      Node (match c with And_F => 8 | Or_F => 9 end)
           (tree_of_formula f1 :: tree_of_formula g1 :: nil)
    | Not f1 => Node 10 (tree_of_formula f1 :: nil)
    | Quant q x1 f1 => 
      Node (match q with Forall_F => 11 | Exists_F => 12 end)
           (tree_of_var x1 :: tree_of_formula f1 :: nil)
  end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma tree_of_query_unfold :
  forall q, tree_of_query q = 
  match q with
  | Query_Empty s => Node 0 (map tree_of_attribute s)
  | Query_Empty_Tuple => Node 1 nil
  | Query_Basename r1 => Node 2 (Leaf (All_relname r1) :: nil)
  | Query_Sigma f1 r1 => 
    Node 13 (tree_of_formula f1 :: tree_of_query r1 :: nil)
  | Query_Pi fa1 r1 => Node 14 (tree_of_query r1 :: map tree_of_attribute fa1)
  | Query_NaturalJoin s1 r1 => Node 15 (tree_of_query s1 :: tree_of_query r1 :: nil)
  | Query_Set o s1 r1 => 
    Node (match o with
            | Union => 16
            | Inter => 17
            | Diff => 18
            | UnionMax => 20
          end) (tree_of_query s1 :: tree_of_query r1 :: nil)
  | Query_Rename rho r1 => 
    Node 19 (Leaf (All_renaming rho) :: tree_of_query r1 :: nil)
  end.
Proof.
intros q; case q; intros; trivial.
Qed.

Open Scope N_scope.

Definition N_of_All (a : All) : N :=
  match a with
  | All_relname _ => 0
  | All_renaming _ => 1
  | All_N _ => 2
  | All_value _ => 3
  | All_attribute _ => 4
  | All_predicate _ => 5
  end.

Close Scope N_scope.

Definition all_compare a1 a2 :=
   match N.compare (N_of_All a1) (N_of_All a2) with
   | Lt => Lt
   | Gt => Gt
   | Eq =>
       match a1, a2 with
       | All_relname r1, All_relname r2 => Oset.compare (ORN DBS) r1 r2
       | All_renaming la1, All_renaming la2 => 
         Oset.compare (mk_olists (mk_opairs (OAtt T) (OAtt T))) la1 la2
       | All_N n1, All_N n2 => Oset.compare ON n1 n2
       | All_value v1, All_value v2 => Oset.compare (OVal T) v1 v2
       | All_attribute a1, All_attribute a2 => Oset.compare (OAtt T) a1 a2
       | All_predicate p1, All_predicate p2 => Oset.compare OP p1 p2
       | _, _ => Eq
       end
   end.

Definition OAll : Oset.Rcd All.
split with all_compare; unfold all_compare.
(* 1/3 *)
- intros a1 a2.
  case a1; clear a1;
  (intro x1; case a2; clear a2; intro x2; simpl; try discriminate);
  try oset_compare_tac.
  + comparelA_eq_bool_ok_tac.
    compareAB_eq_bool_ok_tac; oset_compare_tac.
  + replace (N.compare x1 x2) with (Oset.compare ON x1 x2) by reflexivity.
    oset_compare_tac.
- intros a1 a2 a3.
  case a1; clear a1;
  (intro x1; case a2; clear a2; try discriminate; 
   (intro x2; case a3; clear a3; intro x3; simpl; 
    try (discriminate || intros; apply refl_equal)));
  try oset_compare_tac.
  + comparelA_tac; compareAB_tac; oset_compare_tac.
  + apply (Oset.compare_lt_trans ON).
- (* 1/1 *)
  intros a1 a2.
  case a1; clear a1; 
  (intro x1; case a2; clear a2; intro x2; simpl;
   try (discriminate || intros; apply refl_equal));
  try oset_compare_tac.
  + comparelA_tac; compareAB_tac; oset_compare_tac.
  + apply (Oset.compare_lt_gt ON).
Defined.

Definition query_size q := tree_size (tree_of_query q).

Lemma query_size_unfold :
  forall q, 
    query_size q =  
    match q with
      | Query_Empty l => S (list_size (@tree_size _) (map tree_of_attribute l))
      | Query_Empty_Tuple => 1 
      | Query_Basename _ => 2 
      | Query_Sigma f q => S (tree_size (tree_of_formula f) + (query_size q)) 
      | Query_Pi l q => 
          S (tree_size (tree_of_query q) + list_size (@tree_size _) (map tree_of_attribute l))
      | Query_NaturalJoin q1 q2 => S (query_size q1 + query_size q2) 
      | Query_Rename _ q => S (S (query_size q)) 
      | Query_Set _ q1 q2 => S (query_size q1 + query_size q2) 
     end.
Proof.
intro q; unfold query_size.
rewrite tree_of_query_unfold, tree_size_unfold.
case q; clear q.
- intro l; apply refl_equal.
- apply refl_equal.
- intro r; apply refl_equal.
- intros f q; simpl.
  do 2 apply f_equal; rewrite <- plus_n_O.
  apply refl_equal.
- intros l q; simpl.
  apply refl_equal. 
- intros q1 q2; simpl.
  do 2 apply f_equal; rewrite <- plus_n_O.
  apply refl_equal.
- intros r q; simpl.
  do 2 apply f_equal; rewrite <- plus_n_O.
  apply refl_equal.
- intros _ q1 q2; simpl.
  do 2 apply f_equal; rewrite <- plus_n_O.
  apply refl_equal.
Qed.

Lemma query_size_sigma_q :
  forall n f q, query_size (Query_Sigma f q) <= S n -> query_size q <= n.
Proof.
intros n f q Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_r.
Qed.

Lemma query_size_sigma_f :
  forall n f q, query_size (Query_Sigma f q) <= S n -> tree_size (tree_of_formula f) <= n.
Proof.
intros n f q Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma query_size_pi_q :
  forall n l q, query_size (Query_Pi l q) <= S n -> query_size q <= n.
Proof.
intros n l q Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma query_size_natural_join_1 :
  forall n q1 q2, query_size (Query_NaturalJoin q1 q2) <= S n -> query_size q1 <= n.
Proof.
intros n q1 q2 Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma query_size_natural_join_2 :
  forall n q1 q2, query_size (Query_NaturalJoin q1 q2) <= S n -> query_size q2 <= n.
Proof.
intros n q1 q2 Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_r.
Qed.

Lemma query_size_rename_q :
  forall n r q, query_size (Query_Rename r q) <= S n -> query_size q <= n.
Proof.
intros n r q Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_S; apply le_n.
Qed.

Lemma query_size_set_1 :
  forall n o q1 q2, query_size (Query_Set o q1 q2) <= S n -> query_size q1 <= n.
Proof.
intros n o q1 q2 Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma query_size_set_2 :
  forall n o q1 q2, query_size (Query_Set o q1 q2) <= S n -> query_size q2 <= n.
Proof.
intros n o q1 q2 Hn; rewrite query_size_unfold in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_r.
Qed.

Lemma formula_size_atom_t:
  forall n p l t, tree_size (tree_of_formula (Atom p l)) <= S n -> In t l -> 
                  tree_size (tree_of_term t) <= n.
Proof.
intros n p l t Hn Ht; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_S; apply in_list_size.
rewrite in_map_iff; exists t; split; trivial.
Qed.

Lemma formula_size_conj_1 :
  forall n a f1 f2, tree_size (tree_of_formula (Conj a f1 f2)) <= S n ->
   tree_size (tree_of_formula f1) <= n.
Proof.
intros n a f1 f2 Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma formula_size_conj_2 :
  forall n a f1 f2, tree_size (tree_of_formula (Conj a f1 f2)) <= S n ->
   tree_size (tree_of_formula f2) <= n.
Proof.
intros n a f1 f2 Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
refine (le_trans _ _ _ _ (le_plus_r _ _)).
apply le_plus_l.
Qed.

Lemma formula_size_not_f :
  forall n f, tree_size (tree_of_formula (Not f)) <= S n -> tree_size (tree_of_formula f) <= n.
Proof.
intros n f Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma formula_size_quant_v :
  forall n q v f, tree_size (tree_of_formula (Quant q v f)) <= S n ->
                  tree_size (tree_of_var v) <= n.
Proof.
intros n q v f Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma formula_size_quant_f :
  forall n q v f, tree_size (tree_of_formula (Quant q v f)) <= S n ->
   tree_size (tree_of_formula f) <= n.
Proof.
intros n q v f Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
refine (le_trans _ _ _ _ (le_plus_r _ _)).
apply le_plus_l.
Qed.

Lemma term_size_v :
  forall n v, tree_size (tree_of_term (Var v)) <= S n -> tree_size (tree_of_var v) <= n.
Proof.
intros n v Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma term_size_t :
  forall n f l t, tree_size (tree_of_term (Term f l)) <= S n -> In t l -> 
                tree_size (tree_of_term t) <= n.
Proof.
intros n f l t Hn Ht; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
refine (le_trans _ _ _ _ (le_plus_r _ _)).
apply in_list_size.
rewrite in_map_iff; exists t; split; trivial.
Qed.

Lemma var_size_q :
  forall n q m, tree_size (tree_of_var (Vrbl q m)) <= S n -> query_size q <= n.
Proof.
intros n q m Hn; simpl in Hn.
refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
apply le_plus_l.
Qed.

Lemma tree_of_query_eq_etc :
  forall n,
  (forall q1, query_size q1 <= n ->
   forall q2, tree_of_query q1 = tree_of_query q2 -> q1 = q2) /\
  (forall f1, tree_size (tree_of_formula f1) <= n ->
   forall f2, tree_of_formula f1 = tree_of_formula f2 -> f1 = f2) /\
  (forall t1, tree_size (tree_of_term t1) <= n ->
   forall t2, tree_of_term t1 = tree_of_term t2 -> t1 = t2) /\
  (forall v1, tree_size (tree_of_var v1) <= n ->
   forall v2, tree_of_var v1 = tree_of_var v2 -> v1 = v2).
Proof.
intro n; induction n as [ | n];
[ split; 
  [ intros q1 Hn; destruct q1; inversion Hn
  | split ; 
    [ intros f1 Hn; destruct f1; inversion Hn
    | split; 
      [ intros t1 Hn; destruct t1; inversion Hn 
      | intros v1 Hn; destruct v1; inversion Hn]]]
| ].
split; [ | split; [ | split]].
- intros q1 Hn q2 H; 
  rewrite (tree_of_query_unfold q1), (tree_of_query_unfold q2) in H;
  destruct q1; destruct q2; 
  try (trivial || discriminate H);
  injection H; clear H; try apply f_equal.
  + intros Hl; apply f_equal.
    clear Hn; revert l0 Hl; induction l as [ | a1 l]; 
    intros [ | b1 k] H; try discriminate H.
    * apply refl_equal.
    * simpl in H; injection H; clear H;
      intros Hl Ha.
      apply f_equal2; [ | apply IHl]; assumption.
  + intros _ Abs; destruct s; discriminate Abs.
  + intros Hq Hf; apply f_equal2.
    * apply (proj1 (proj2 IHn) _ (query_size_sigma_f Hn) _ Hf).
    * apply (proj1 IHn _ (query_size_sigma_q Hn) _ Hq).
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros Hl Hq; apply f_equal2.
    * {
        clear Hn; revert l0 Hl; induction l as [ | a1 l]; 
        intros [ | b1 k] H; try discriminate H.
        - apply refl_equal.
        - simpl in H; injection H; clear H;
          intros Hl Ha.
          apply f_equal2; [ | apply IHl]; assumption.
      }  
    * apply (proj1 IHn _ (query_size_pi_q Hn) _ Hq).
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros Hq1 Hq2; apply f_equal2.
    * apply (proj1 IHn _ (query_size_natural_join_1 Hn) _ Hq2).
    * apply (proj1 IHn _ (query_size_natural_join_2 Hn) _ Hq1).
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros Hq Hr; apply f_equal2; [assumption | ].
    apply (proj1 IHn _ (query_size_rename_q Hn) _ Hq).
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros _ Abs; destruct s; discriminate Abs.
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros _ _ Abs; destruct s; discriminate Abs.
  + intros Hq1 Hq2 Hs; apply f_equal3.
    * destruct s; destruct s0; try discriminate Hs; trivial.
    * apply (proj1 IHn _ (query_size_set_1 Hn) _ Hq2).
    * apply (proj1 IHn _ (query_size_set_2 Hn) _ Hq1).
- intros f1 Hn f2 H; destruct f1; destruct f2; 
  (discriminate H || 
   (injection H; clear H; try apply f_equal) ||
   (destruct v; discriminate H) ||
   trivial).
  + intros Hl Hp; apply f_equal2; [assumption | ].
    assert (IH : forall t, List.In t l -> forall t', tree_of_term t = tree_of_term t' -> t = t').
    {
      intros t Ht; apply (proj1 (proj2 (proj2 IHn)) _ (formula_size_atom_t _ Hn Ht)).
    }
    clear Hn; revert l0 Hl; 
      induction l as [ | t l];
      intros [ | t' l'] Hl;
      (discriminate Hl || trivial).
    simpl in Hl; injection Hl; clear Hl; intros Hl Ht.
    apply f_equal2.
    * apply IH; [left; apply refl_equal | assumption].
    * apply IHl; [intros; apply IH; [right | ]; trivial | assumption].
  + intros _ _ Abs; destruct a; discriminate Abs.
  + intros _ _ Abs; destruct q; discriminate Abs.
  + intros _ _ Abs; destruct a; discriminate Abs.
  + intros Hf1 Hf2 Ha.
    rewrite (proj1 (proj2 IHn) _ (formula_size_conj_1 Hn) _ Hf2).
    rewrite (proj1 (proj2 IHn) _ (formula_size_conj_2 Hn) _ Hf1).
    destruct a; destruct a0; try discriminate; trivial.
  + intros _ _ Abs; destruct a; destruct q; discriminate Abs.
  + intros Hf; apply f_equal.
    apply (proj1 (proj2 IHn) _ (formula_size_not_f Hn) _ Hf).
  + intros _ _ Abs; destruct q; discriminate Abs.
  + intros _ _ Abs; destruct q; destruct a; discriminate Abs.
  + intros Hf Hv Hq; apply f_equal3.
    * destruct q; destruct q0; trivial; discriminate Hq.
    * apply (proj2 (proj2 (proj2 IHn)) _ (formula_size_quant_v Hn) _ Hv).
    * apply (proj1 (proj2 IHn) _ (formula_size_quant_f Hn) _ Hf).
- intros t1 Hn t2 H; destruct t1; destruct t2.
  + apply f_equal.
    simpl in H; injection H; clear H.
    apply (proj2 (proj2 (proj2 IHn)) _ (term_size_v Hn)).
  + discriminate H.
  + discriminate H.
  + simpl in H; injection H.
    intros Hl Hs.
    apply f_equal2;
      [destruct s; destruct s0; try discriminate Hs;
       injection Hs; apply f_equal
      | ].
    assert (IH : forall t, List.In t l -> forall t', tree_of_term t = tree_of_term t' -> t = t').
    {
      intros t Ht; apply (proj1 (proj2 (proj2 IHn)) _ (term_size_t _ Hn Ht)).
    }
    clear Hn H; revert l0 Hl; 
    induction l as [ | t l];
    intros [ | t' l'] Hl;
    (discriminate Hl || trivial).
    simpl in Hl; injection Hl; clear Hl; intros Hl Ht.
    apply f_equal2.
    * apply IH; [left; apply refl_equal | assumption].
    * apply IHl; [intros; apply IH; [right | ]; trivial | assumption].
- intros [q1 n1] Hn [q2 n2] H; simpl in H.
  injection H; clear H.
  intros Kn Hq; apply f_equal2; [ | assumption].
  apply (proj1 IHn _ (var_size_q Hn) _ Hq).
Qed.

Lemma tree_of_query_eq :
  forall q1 q2, tree_of_query q1 = tree_of_query q2 -> q1 = q2.
Proof.
intros q1 q2 H.  
apply (proj1 (tree_of_query_eq_etc _) q1 (le_n _) q2 H).
Qed.

Definition OSymbol : Oset.Rcd symbol.
split with (fun f1 f2 => Oset.compare (OTree OAll) (tree_of_symbol f1) (tree_of_symbol f2)).
- intros a1 a2.
  generalize (Oset.eq_bool_ok (OTree OAll) (tree_of_symbol a1) (tree_of_symbol a2)).
  case (Oset.compare (OTree OAll) (tree_of_symbol a1) (tree_of_symbol a2)).
  + destruct a1 as [c1 | a1]; destruct a2 as [c2 | a2]; simpl.
    * intro H; injection H; clear H.
      apply f_equal.
    * intro H; discriminate H.
    * intro H; discriminate H.
    * intro H; injection H; clear H.
      apply f_equal.
  + intros H1 H2; apply H1; apply f_equal; apply H2.      
  + intros H1 H2; apply H1; apply f_equal; apply H2.      
- do 3 intro; oset_compare_tac.
- do 2 intro; oset_compare_tac.
Defined.

Definition OV : Oset.Rcd (variable query).
split with (fun v1 v2 => Oset.compare (OTree OAll) (tree_of_var v1) (tree_of_var v2)).
- intros a1 a2.
  generalize (Oset.eq_bool_ok (OTree OAll) (tree_of_var a1) (tree_of_var a2)).
  case (Oset.compare (OTree OAll) (tree_of_var a1) (tree_of_var a2)).
  + destruct a1 as [q1 n1]; destruct a2 as [q2 n2]; simpl.
    intro H; injection H; clear H.
    intros Hn Hq1; apply f_equal2; [ | assumption].
    generalize (tree_of_query_eq _ _ Hq1); clear Hq1; intro; subst q2.
    apply refl_equal.
  + destruct a1 as [q1 n1]; destruct a2 as [q2 n2]; simpl.
    intros H K; apply H; injection K; clear K; intros K1 K2.
    subst; apply refl_equal.
  + destruct a1 as [q1 n1]; destruct a2 as [q2 n2]; simpl.
    intros H K; apply H; injection K; clear K; intros K1 K2.
    subst; apply refl_equal.
- do 3 intro; oset_compare_tac.
- do 2 intro; oset_compare_tac.
Defined.

Definition OQ : Oset.Rcd query.
split with (fun q1 q2 => Oset.compare (OTree OAll) (tree_of_query q1) (tree_of_query q2)).
- intros a1 a2.
  generalize (Oset.eq_bool_ok (OTree OAll) (tree_of_query a1) (tree_of_query a2)).
  case (Oset.compare (OTree OAll) (tree_of_query a1) (tree_of_query a2)).
  + apply tree_of_query_eq.
  + intros H K; apply H; subst; apply refl_equal.
  + intros H K; apply H; subst; apply refl_equal.
- do 3 intro; oset_compare_tac.
- do 2 intro; oset_compare_tac.
Defined.

End Compare.

(** Individual lemmata for [sort]. *)

Lemma sort_Rename :
  forall rho q a', 
    a' inS (sort (Query_Rename rho q)) <-> 
    exists a, a inS sort q /\ a' = apply_renaming T rho a.
Proof.
intros rho r a'.
rewrite sort_unfold.
rewrite Fset.mem_map; split.
- intros [a [Ha Ka]]; exists a; split; [ | apply sym_eq]; assumption.
- intros [a [Ha Ka]]; exists a; split; [apply sym_eq | ]; assumption.
Qed.

Lemma sort_N_ary_Query_NaturalJoin :
  forall a lq, a inS? (sort (N_ary_Query_NaturalJoin lq)) = existsb (fun q => a inS? sort q) lq.
Proof.
intros a lq; unfold N_ary_Query_NaturalJoin.
assert (Aux : forall q, (a inS? sort (fold_left Query_NaturalJoin lq q)) =
                        existsb (fun q : query => a inS? sort q) (q :: lq)).
{
  induction lq as [ | q1 lq]; intro q; simpl.
  - rewrite Bool.orb_false_r; apply refl_equal.
  - rewrite IHlq; simpl.
    rewrite Fset.mem_union.
    rewrite Bool.Bool.orb_assoc.
    apply refl_equal.
}
rewrite Aux; simpl.
rewrite Fset.empty_spec.
apply refl_equal.
Qed.


(* end hide *)
Definition FV := Fset.build OV.

(** Well-formedness of queries *)

(** Well-formedness of queries *)

Fixpoint well_formed_q q :=
  match q with
    | Query_Empty _ | Query_Empty_Tuple | Query_Basename _ => true
    | Query_Sigma f q => 
      let well_formed_f :=
          (fix well_formed_f (f : formula query predicate symbol) :=   
             match f with
               | TTrue => true
               | Atom _ l => 
                   let well_formed_t :=
                       (fix well_formed_t (t : term query symbol) :=
                          match t with
                            | Var (Vrbl q _) => well_formed_q q
                            | Term _ l => forallb well_formed_t l
                          end) in forallb well_formed_t l
               | Conj _ f1 f2 => well_formed_f f1 && well_formed_f f2
               | Not f => well_formed_f f
               | Quant qtf (Vrbl q _) f => well_formed_q q && well_formed_f f
             end) in
      match {{{free_variables_f OQ OP OSymbol f}}} with 
          | nil => true 
          | Vrbl q' _ :: nil => Oset.eq_bool OQ q q'
          | _ => false end && 
      well_formed_f f && well_formed_q q
    | Query_Pi s q => (forallb (fun x => Fset.mem _ x (sort q)) s) && well_formed_q q
    | Query_NaturalJoin q1 q2 => well_formed_q q1 && well_formed_q q2
    | Query_Rename rho q => one_to_one_renaming_bool ({{{sort q}}}) rho && well_formed_q q
    | Query_Set _ q1 q2 => (sort q1 =S?= sort q2) && well_formed_q q1 && well_formed_q q2
  end.

Fixpoint well_formed_t (t : term query symbol) :=
  match t with
    | Var (Vrbl q _) => well_formed_q q
    | Term _ l => forallb well_formed_t l
  end.

Fixpoint well_formed_f (f : formula query predicate symbol) :=
  match f with
    | TTrue => true
    | Atom _ l => forallb well_formed_t l
    | Conj _ f1 f2 => well_formed_f f1 && well_formed_f f2
    | Not f => well_formed_f f
    | Quant qtf (Vrbl q _) f => well_formed_q q && well_formed_f f
  end.

Lemma well_formed_q_unfold :
  forall q, well_formed_q q =
  match q with
    | Query_Empty _ | Query_Empty_Tuple | Query_Basename _ => true
    | Query_Sigma f q => 
      match {{{free_variables_f OQ OP OSymbol f}}} with 
          | nil => true 
          | Vrbl q' _ :: nil => Oset.eq_bool OQ q q'
          | _ => false end && well_formed_f f && well_formed_q q
    | Query_Pi s q => (forallb (fun x => Fset.mem _ x (sort q)) s) && well_formed_q q
    | Query_NaturalJoin q1 q2 => well_formed_q q1 && well_formed_q q2
    | Query_Rename rho q => one_to_one_renaming_bool ({{{sort q}}}) rho && well_formed_q q
    | Query_Set _ q1 q2 => (sort q1 =S?= sort q2) && well_formed_q q1 && well_formed_q q2
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Lemma well_formed_f_unfold :
  forall f, well_formed_f f =
            match f with
              | TTrue => true
              | Atom _ l => forallb well_formed_t l
              | Conj _ f1 f2 => well_formed_f f1 && well_formed_f f2
              | Not f => well_formed_f f
              | Quant qtf (Vrbl q _) f => well_formed_q q && well_formed_f f
            end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

(** Semantics of query evaluation *)

Section Interp.

Hypothesis interp_pred : predicate -> list value -> bool.

Section Local.
(** [ivar] a valuation locally defined in the section [Local]. *)
Hypothesis ivar : variable query -> tuple T.

(** Interpretation of terms and atoms w.r.t valuation [ivar]. Formulae are interpreted out of the section, since the valuation changes in the recursive calls for quantified formulae. *)
Definition interp_term (t : term query symbol) : value :=
  match t with
    | Term (Constant c) nil => c
    | Term (Dot a) (Var x :: nil) => dot T (ivar x) a
    | _ => default_value T default_type
  end.

End Local.

Notation interp_formula := (interp_formula OQ OP OSymbol Bool2 interp_pred interp_term).
Notation natural_join_set := (Data.natural_join_set _ _ (build_data T) FTupleT).

Section Global.

(** [I] is an "instance", that is a function which associates a set of tuples to each [relname]. *)

Hypothesis I : relname -> setT.

Fixpoint eval_query q {struct q} : setT :=
  match q with
  | Query_Empty _ => emptysetSE
  | Query_Empty_Tuple => Feset.singleton _ (default_tuple T (emptysetS))
  | Query_Basename r => I r
  | Query_Sigma f q1 => 
    let eval_f := 
        fun t => 
          interp_formula 
            (fun _q => Feset.elements _ (eval_query _q)) (fun _ => t) f in 
    Feset.filter FTupleT eval_f (eval_query q1)
  | Query_Pi la q1 => 
      let sa := Fset.mk_set (A T) la in
      Feset.map FTupleT FTupleT (fun t => mk_tuple T sa (dot T t)) (eval_query q1)
  | Query_NaturalJoin q1 q2 => natural_join_set (eval_query q1) (eval_query q2)
  | Query_Rename rho q1 => 
    Feset.map FTupleT FTupleT (rename_tuple T (apply_renaming T rho)) (eval_query q1)
  | Query_Set o q1 q2 => 
    if sort q1 =S?= sort q2 
    then Feset.interp_set_op _ o (eval_query q1) (eval_query q2)
    else emptysetSE
  end.

Definition eval_f f t :=
  interp_formula (fun q => Feset.elements _ (eval_query q)) (fun _ => t) f.

Lemma eval_query_unfold :
 forall q, eval_query q =
  match q with
  | Query_Empty _ => emptysetSE
  | Query_Empty_Tuple => Feset.singleton _ (default_tuple T (emptysetS))
  | Query_Basename r => I r
  | Query_Sigma f q1 => Feset.filter FTupleT (eval_f f) (eval_query q1)
  | Query_Pi la q1 => 
    let sa := Fset.mk_set (A T) la in
    Feset.map FTupleT FTupleT (fun t => mk_tuple T sa (dot T t)) (eval_query q1)
  | Query_NaturalJoin q1 q2 => natural_join_set (eval_query q1) (eval_query q2)
  | Query_Rename rho q1 => 
    Feset.map FTupleT FTupleT (rename_tuple T (apply_renaming T rho)) (eval_query q1)
  | Query_Set o q1 q2 => 
    if sort q1 =S?= sort q2 
    then Feset.interp_set_op _ o (eval_query q1) (eval_query q2)
    else emptysetSE
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.


Notation "t 'inI' q" := (Feset.mem FTupleT t (eval_query q) = true) (at level 70, no associativity).

Lemma interp_term_eq :
  forall i1 i2 t, 
    (forall x, x inS variables_t (FVR OQ OP OSymbol) t -> i1 x =t= i2 x) ->
    interp_term i1 t = interp_term i2 t.
Proof.
intros i1 i2 [y | [c | a] l] H; [apply refl_equal | | ].
- destruct l; trivial.
- destruct l as [ | [x1 | ] [ | ]]; trivial.
  simpl; unfold Formula.ivar_xt.
  apply tuple_eq_dot; apply H.
  rewrite variables_t_unfold, 2 ListFacts.map_unfold.
  rewrite 2 Fset.Union_unfold, Fset.mem_union, Fset.empty_spec.
  rewrite Bool.orb_false_r, variables_t_unfold, Fset.singleton_spec.
  rewrite Oset.eq_bool_true_iff; apply refl_equal.
Qed.

Lemma interp_term_eq_rec :
  forall i1 i2 v x1 x2 s,
    Oeset.compare (OTuple T) x1 x2 = Eq ->
    (forall t, variables_t (FVR OQ OP OSymbol) t
                           subS Fset.filter (FVR OQ OP OSymbol)
                           (fun y  => negb (Oset.eq_bool (OVR OQ OP OSymbol) v y)) s -> 
                                      interp_term i1 t = interp_term i2 t) ->
    forall t, variables_t (FVR OQ OP OSymbol) t subS s -> 
              interp_term (ivar_xt OQ OP OSymbol i1 v x1) t = 
              interp_term (ivar_xt OQ OP OSymbol i2 v x2) t.
Proof.
intros i1 i2 v _x1 _x2 s _Hx H [y | [c | a] l] K; [apply refl_equal | | ].
- destruct l; trivial.
- destruct l as [ | [x1 | ] [ | ]]; trivial.
  unfold interp_term, ivar_xt, Formula.ivar_xt.
  case_eq (Oset.compare (OVR OQ OP OSymbol) v x1); intro Hv.
  + apply tuple_eq_dot; assumption.
  + apply (H (Term (Dot a) (Var x1 :: nil))).
    rewrite Fset.subset_spec; intros y Hy.
    rewrite Fset.mem_filter, Bool.Bool.andb_true_iff; split.
    * rewrite Fset.subset_spec in K; apply K; apply Hy.
    * rewrite variables_t_unfold, 2 ListFacts.map_unfold, 2 Fset.Union_unfold in Hy.
      rewrite Fset.mem_union, Fset.empty_spec, Bool.orb_false_r in Hy.
      rewrite variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff in Hy; subst y.
      unfold Oset.eq_bool; case_eq (Oset.compare (OVR OQ OP OSymbol) v x1); trivial.
      intro Kv; rewrite Oset.compare_eq_iff in Kv; subst v.
      rewrite Oset.compare_eq_refl in Hv; discriminate Hv.
  + apply (H (Term (Dot a) (Var x1 :: nil))).
    rewrite Fset.subset_spec; intros y Hy.
    rewrite Fset.mem_filter, Bool.Bool.andb_true_iff; split.
    * rewrite Fset.subset_spec in K; apply K; apply Hy.
    * rewrite variables_t_unfold, 2 ListFacts.map_unfold, 2 Fset.Union_unfold in Hy.
      rewrite Fset.mem_union, Fset.empty_spec, Bool.orb_false_r in Hy.
      rewrite variables_t_unfold, Fset.singleton_spec, Oset.eq_bool_true_iff in Hy; subst y.
      unfold Oset.eq_bool; case_eq (Oset.compare (OVR OQ OP OSymbol) v x1); trivial.
      intro Kv; rewrite Oset.compare_eq_iff in Kv; subst v.
      rewrite Oset.compare_eq_refl in Hv; discriminate Hv.
Qed.

Lemma interp_formula_eq :
  forall f i1 i2, (forall v, i1 v =t= i2 v) ->
   interp_formula (fun q => Feset.elements _ (eval_query q)) i1 f = 
   interp_formula (fun q => Feset.elements _ (eval_query q)) i2 f.
Proof.
intros f i1 i2 H.
apply (interp_formula_eq Bool2 (OTuple T)).
- apply interp_term_eq_rec.
- intros t Ht; apply interp_term_eq.
  intros x Hx; apply H.
Qed.    

Lemma eval_f_eq :
  forall f t1 t2, t1 =t= t2 -> eval_f f t1 = eval_f f t2.
Proof.
  intros f t1 t2 Ht; unfold eval_f.
  apply interp_formula_eq.
  intro x; assumption.
Qed.


(*
Definition tuples_of_pairs (s : Fset.set (FPTuple T)) :=
  Feset.mk_set FTupleT (List.map (pairs_as_tuple T) (Fset.elements (FPTuple T) s)).

Definition pairs_of_tuples (s : Feset.set FTupleT) :=
  Fset.mk_set (FPTuple T) (List.map (tuple_as_pairs T) (Feset.elements FTupleT s)).

Fixpoint can_eval_query q {struct q} : Fset.set (FPTuple T) :=
  match q with
  | Query_Empty _ => emptysetS
  | Query_Empty_Tuple => Fset.singleton _ nil
  | Query_Basename r => pairs_of_tuples (I r)
  | Query_Sigma f q1 => 
    let interp_dom x := Feset.elements _ (tuples_of_pairs (can_eval_query x)) in
    let interp_can_f := 
        fun t => interp_formula interp_dom (fun _ => (pairs_as_tuple _ t)) f in
    Fset.filter (FPTuple T) interp_can_f (can_eval_query q1)
  | Query_Pi la q1 => 
    let sa := Fset.mk_set (A T) la in
    Fset.map 
      (FPTuple T) (FPTuple T) 
      (fun t => tuple_as_pairs T (mk_tuple T sa (dot T (pairs_as_tuple T t))))
      (can_eval_query q1)
  | Query_NaturalJoin q1 q2 => pnatural_join (can_eval_query q1) (can_eval_query q2)
  | Query_Rename rho q1 => 
    Fset.map 
      (FPTuple T) (FPTuple T) 
      (fun t => tuple_as_pairs T (rename_tuple T (apply_renaming T rho) (pairs_as_tuple T t)))
      (can_eval_query q1)
  | Query_Set o q1 q2 => 
    if sort q1 =S?= sort q2 
    then Fset.interp_set_op _ o (can_eval_query q1) (can_eval_query q2)
    else emptysetS
  end.

Lemma can_eval_query_unfold :
 forall q, can_eval_query q =
  match q with
  | Query_Empty _ => emptysetS
  | Query_Empty_Tuple => Fset.singleton _ nil
  | Query_Basename r => pairs_of_tuples (I r)
  | Query_Sigma f q1 => 
    let interp_dom x := Feset.elements _ (tuples_of_pairs (can_eval_query x)) in
    let interp_can_f := 
        fun t => interp_formula interp_dom (fun _ => pairs_as_tuple _ t) f in
    Fset.filter (FPTuple T) interp_can_f (can_eval_query q1)
  | Query_Pi la q1 => 
    let sa := Fset.mk_set (A T) la in
    Fset.map 
      (FPTuple T) (FPTuple T) 
      (fun t => tuple_as_pairs T (mk_tuple T sa (_dot T (pairs_as_tuple T t))))
      (can_eval_query q1)
  | Query_NaturalJoin q1 q2 => pnatural_join (can_eval_query q1) (can_eval_query q2)
  | Query_Rename rho q1 => 
    Fset.map 
      (FPTuple T) (FPTuple T) 
      (fun t => tuple_as_pairs T (rename_tuple T (apply_renaming T rho) (pairs_as_tuple T t)))
      (can_eval_query q1)
  | Query_Set o q1 q2 => 
    if sort q1 =S?= sort q2 
    then Fset.interp_set_op _ o (can_eval_query q1) (can_eval_query q2)
    else emptysetS
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Lemma pairs_of_tuples_ok :
  forall s t, (t inSE? s) = (tuple_as_pairs T t inS? pairs_of_tuples s).
Proof.
intros s t.
unfold pairs_of_tuples; rewrite eq_bool_iff; split; intro Ht.
- rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ht.
  destruct Ht as [t' [Ht H]].
  rewrite tuple_as_pairs_canonizes in Ht; rewrite Ht.
  rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff.
  exists t'; split; trivial.
- rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff in Ht.
  destruct Ht as [t' [Ht H]].
  rewrite Feset.mem_elements, Oeset.mem_bool_true_iff.
  exists t'; split; [ | assumption].
  rewrite tuple_as_pairs_canonizes; apply sym_eq; apply Ht.
Qed.
*)

Lemma in_variables_t_size :
  forall x t, x inS variables_t (FVR OQ OP OSymbol) t -> 
              tree_size (tree_of_var x) < tree_size (tree_of_term t).
Proof.
intros x t; pattern t; apply term_rec3; clear t.
- intros v H; rewrite variables_t_unfold in H.
  rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in H.
  subst v; simpl.
  rewrite <- plus_n_O; apply le_n.
- intros f l IH H; rewrite variables_t_unfold in H.
  rewrite Fset.mem_Union in H.
  destruct H as [s [Hs H]].
  rewrite in_map_iff in Hs.
  destruct Hs as [t [Ht Hs]]; subst s.
  refine (Le.le_trans _ _ _ (IH t Hs H) _).
  simpl; apply le_S; refine (Le.le_trans _ _ _ _ (Plus.le_plus_r _ _)).
  apply in_list_size; rewrite in_map_iff.
  exists t; split; trivial.
Qed.

Lemma in_variables_f_size :
  forall x f, x inS variables_f OQ OP OSymbol f -> 
              tree_size (tree_of_var x) <= tree_size (tree_of_formula f).
Proof.
intros x f H; induction f; rewrite variables_f_unfold in H.
- rewrite Fset.empty_spec in H; discriminate H.
- rewrite Fset.mem_Union in H.
  destruct H as [s [Hs H]].
  rewrite in_map_iff in Hs.
  destruct Hs as [t [Ht Hs]]; subst s.
  apply le_S_n; refine (Le.le_trans _ _ _ (in_variables_t_size x t H) _).
  apply Lt.lt_le_weak; apply Le.le_n_S; simpl.
  do 2 apply le_S; apply in_list_size; rewrite in_map_iff; exists t; split; trivial.
- simpl; apply le_S.
  rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H.
  destruct H as [H | H].
  + refine (Le.le_trans _ _ _ (IHf1 H) _).
    apply Plus.le_plus_l.
  + refine (Le.le_trans _ _ _ (IHf2 H) _).
    rewrite <- plus_n_O; apply Plus.le_plus_r.
- simpl; apply le_S.
  rewrite <- plus_n_O; apply (IHf H).
- simpl; rewrite <- plus_n_O; apply le_S.
  rewrite Fset.add_spec, Bool.Bool.orb_true_iff in H.
  destruct H as [H | H].
  + rewrite Oset.eq_bool_true_iff in H; subst v.
    apply Plus.le_plus_l.
  + refine (Le.le_trans _ _ _ (IHf H) _).
    apply Plus.le_plus_r.
Qed.

(*
Lemma in_can_eval_query_tuple_as_pairs :
  forall q t, t inS (can_eval_query q) -> exists t', t = tuple_as_pairs T t'.
Proof.
intro q; set (n := query_size q).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert q Hn; induction n as [ | n]; 
intros q Hn; [destruct q; inversion Hn | ].
destruct q as [ | | r | f q | la q | q1 q2 | rho q | o q1 q2];
  rewrite can_eval_query_unfold; intros t Ht.
- rewrite Fset.empty_spec in Ht; discriminate Ht.
- rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Ht; subst t.
  exists (default_tuple T (emptysetS)).
  unfold tuple_as_pairs, default_tuple.
  rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple T _ _)), Fset.elements_empty, map_unfold.
  apply refl_equal.
- unfold pairs_of_tuples in Ht.
  rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff in Ht.
  destruct Ht as [t' [Ht' Ht]]; subst t.
  exists t'; apply refl_equal.
- cbv zeta in Ht; rewrite Fset.mem_filter, Bool.andb_true_iff in Ht.
  destruct Ht as [Ht _].
  apply (IHn q (query_size_sigma_q Hn) _ Ht).
- cbv zeta in Ht.
  rewrite Fset.mem_map in Ht.
  destruct Ht as [t' [Ht' Ht]]; subst t.
  destruct (IHn q (query_size_pi_q Hn) _ Ht) as [t'' Ht'']. 
  eexists; apply refl_equal.
- rewrite mem_pnatural_join in Ht.
  destruct Ht as [t1 [t2 [Ht1 [Ht2 [H Ht]]]]].
  unfold pjoin_tuple in Ht; rewrite Ht.
  eexists; apply refl_equal.
- rewrite Fset.mem_map in Ht.
  destruct Ht as [t' [Ht Ht']].
  rewrite <- Ht; eexists; apply refl_equal.
- destruct (sort q1 =S?= sort q2).
  + destruct o; unfold Fset.interp_set_op in Ht.
    * rewrite Fset.mem_union, Bool.orb_true_iff in Ht.
      destruct Ht as [Ht | Ht]; 
        [apply (IHn q1 (query_size_set_1 Hn) _ Ht) | apply (IHn q2 (query_size_set_2 Hn) _ Ht)].
    * rewrite Fset.mem_union, Bool.orb_true_iff in Ht.
      destruct Ht as [Ht | Ht]; 
        [apply (IHn q1 (query_size_set_1 Hn) _ Ht) | apply (IHn q2 (query_size_set_2 Hn) _ Ht)].
    * rewrite Fset.mem_inter, Bool.andb_true_iff in Ht.
      destruct Ht as [Ht _]; apply (IHn q1 (query_size_set_1 Hn) _ Ht).
    * rewrite Fset.mem_diff, Bool.andb_true_iff in Ht.
      destruct Ht as [Ht _]; apply (IHn q1 (query_size_set_1 Hn) _ Ht).
  + rewrite Fset.empty_spec in Ht; discriminate Ht.
Qed.

Lemma can_eval_query_ok :
  forall q t, t inSE? (eval_query q) = (tuple_as_pairs T t inS? (can_eval_query q)).
Proof.
intro q; set (n := query_size q).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert q Hn; induction n as [ | n]; 
intros q Hn; [destruct q; inversion Hn | ].
destruct q as [ | | r | f q | la q | q1 q2 | rho q | o q1 q2];
  rewrite can_eval_query_unfold; intro t.
- rewrite Feset.empty_spec, Fset.empty_spec; apply refl_equal.
- rewrite eval_query_unfold, eq_bool_iff; split; intro Ht.
  + rewrite Feset.singleton_spec in Ht; unfold Oeset.eq_bool in Ht.
    rewrite compare_eq_true in Ht.
    rewrite tuple_as_pairs_canonizes in Ht; rewrite Ht.
    unfold tuple_as_pairs, default_tuple at 2.
    rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple T _ _)), Fset.elements_empty, map_unfold.
    rewrite Fset.singleton_spec, Oset.eq_bool_true_iff.
    apply refl_equal.
  + rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Ht.
    rewrite Feset.singleton_spec; unfold Oeset.eq_bool.
    rewrite compare_eq_true.
    rewrite tuple_as_pairs_canonizes, Ht.
    unfold tuple_as_pairs, default_tuple.
    rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple T _ _)), Fset.elements_empty, map_unfold.
    apply refl_equal.
- apply pairs_of_tuples_ok. 
- rewrite eval_query_unfold.
  cbv zeta; rewrite Fset.mem_filter, Feset.filter_spec.
  + apply f_equal2.
    * apply (IHn _ (query_size_sigma_q Hn)).
    * assert (IHv : forall (d : query) (m : N),
                      Vrbl d m inS variables_f OQ OP OSymbol f ->
                      forall t : tuple T,
                        (t inSE? eval_query d) = (tuple_as_pairs T t inS? can_eval_query d)).
      {
        intros d m H; apply IHn.
        refine (le_trans _ _ _ _ (query_size_sigma_f Hn)).
        refine (le_trans _ _ _ _ (in_variables_f_size _ _ H)).
        simpl; apply le_S; apply Plus.le_plus_l.
      }
      assert (IHv' : forall (d : query) (m : N),
                       Vrbl d m inS variables_f OQ OP OSymbol f ->
                       eval_query d =SE= tuples_of_pairs (can_eval_query d)).
      {
        intros d m Hdm.
        rewrite Feset.equal_spec; intro u.
        rewrite (IHv _ _ Hdm), pairs_of_tuples_ok.
        unfold pairs_of_tuples, tuples_of_pairs.
        rewrite eq_bool_iff, Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff; split.
        - intro Hu.
          assert (Ku : (pairs_as_tuple T (tuple_as_pairs T u)) inSE
                         (Feset.mk_set 
                            FTupleT
                            (map (pairs_as_tuple T) ({{{can_eval_query d}}})))).
          {
            rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff.
            exists (pairs_as_tuple T (tuple_as_pairs T u)); split.
            - apply Oeset.compare_eq_refl.
            - rewrite in_map_iff.
              exists (tuple_as_pairs T u); split; [trivial | ].
              rewrite Fset.mem_elements, Oset.mem_bool_true_iff in Hu; apply Hu.
          }
          rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ku.
          destruct Ku as [u' [Ku Hu']].
          exists u'; split; [ | assumption].
          apply sym_eq; rewrite <- tuple_as_pairs_canonizes.
          refine (Oeset.compare_eq_trans _ _ _ _ _ Ku).
          apply canonized_tuple_eq.
        - intros [x [Hu Hx]]. 
          assert (Kx := Feset.in_elements_mem _ _ _ Hx).
          rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff in Kx.
          destruct Kx as [_x' [Kx Hx']]; rewrite in_map_iff in Hx'.
          destruct Hx' as [x' [Hx' Kx']]; subst _x'.
          destruct (in_can_eval_query_tuple_as_pairs _ _ (Fset.in_elements_mem _ _ _ Kx')) 
            as [y Hy].
          rewrite <- Hu, (Fset.mem_eq_mem _ _ x'); [apply Fset.in_elements_mem; assumption | ].
          rewrite Oset.eq_bool_true_iff, Hy, <- tuple_as_pairs_canonizes.
          refine (Oeset.compare_eq_trans _ _ _ _ Kx _); subst x'.
          apply Oeset.compare_eq_sym; apply canonized_tuple_eq.
      }
      {
        unfold eval_f.
        refine (eq_trans (interp_formula_interp_dom_eq 
                            _ _ _ interp_term_eq_rec _ _ _ _ _ _) _).
        - intros i1 i2 Hi u.
          apply interp_term_eq; intros; apply Hi.
        - intros d m Hdm x; rewrite <- 2 Feset.mem_elements.
          assert (IH := IHv' _ _ Hdm); rewrite Feset.equal_spec in IH; apply IH.
        - apply Formula.interp_formula_eq.
          + apply interp_term_eq_rec.
          + intros u Hu; apply interp_term_eq.
            intros x Hx; apply canonized_tuple_eq.
      }
  + intros t1 t2 _; apply eval_f_eq.
- rewrite eval_query_unfold; cbv zeta.
  rewrite eq_bool_iff, Feset.mem_map, Fset.mem_map; 
    split; intros [u [Hu H]].
  + exists (tuple_as_pairs T u); split.
    * rewrite <- tuple_as_pairs_canonizes.
      apply Oeset.compare_eq_sym; refine (Oeset.compare_eq_trans _ _ _ _ Hu _).
      apply mk_tuple_eq; [apply Fset.equal_refl | ].
      intros a Ha _; apply tuple_eq_dot; apply canonized_tuple_eq.
    * rewrite <- IHn; [apply Feset.in_elements_mem; assumption | ].
      apply (query_size_pi_q Hn).
  + destruct (in_can_eval_query_tuple_as_pairs _ _ H) as [u' Hu']; subst u.
    rewrite <- IHn in H.
    * rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in H.
      destruct H as [u [Hu' H]].
      exists u; split; [ | assumption].
      rewrite tuple_as_pairs_canonizes, <- Hu, <- tuple_as_pairs_canonizes.
      apply mk_tuple_eq; [apply Fset.equal_refl | ].
      intros a Ha _; apply tuple_eq_dot.
      refine (Oeset.compare_eq_trans _ _ _ _ _ Hu').
      apply Oeset.compare_eq_sym; apply canonized_tuple_eq.
    * apply (query_size_pi_q Hn).
- rewrite eval_query_unfold, eq_bool_iff, mem_natural_join_set, mem_pnatural_join; 
  split; intros [t1 [t2 [Ht1 [Ht2 [H Ht]]]]].
  + exists (tuple_as_pairs T t1); exists (tuple_as_pairs T t2); repeat split.
    * rewrite <- (IHn _ (query_size_natural_join_1 Hn)); apply Ht1.
    * rewrite <- (IHn _ (query_size_natural_join_2 Hn)); apply Ht2.
    * unfold pjoin_compatible_tuple; rewrite forallb_forall.
      intros [a v1] K1.
      case_eq (Oset.find (OAtt T) a (tuple_as_pairs T t2));
        [ | intros; apply refl_equal].
      intros v2 K2; rewrite Oset.eq_bool_true_iff.
      unfold tuple_as_pairs in K1, K2; rewrite in_map_iff in K1.
      destruct K1 as [_a [K1 Ha]]; injection K1; clear K1; intros K1 _Ha; subst _a v1.
      assert (H2 := Oset.find_some _ _ _ K2).
      rewrite in_map_iff in H2.
      destruct H2 as [_a [H2 Ha']]; injection H2; clear H2; intros H2 _Ha; subst _a v2.
      unfold join_compatible_tuple in H; rewrite Fset.for_all_spec, forallb_forall in H.
      rewrite <- (Oset.eq_bool_true_iff (OVal T)); apply H.
      rewrite Fset.elements_inter; split; assumption.
    * unfold pjoin_tuple; rewrite <- tuple_as_pairs_canonizes.
      refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
      apply join_tuple_eq; apply canonized_tuple_eq.
  + destruct (in_can_eval_query_tuple_as_pairs _ _ Ht1) as [u1 Hu1].
    destruct (in_can_eval_query_tuple_as_pairs _ _ Ht2) as [u2 Hu2].
    exists u1; exists u2; repeat split.
    * rewrite IHn, <- Hu1; [assumption | apply (query_size_natural_join_1 Hn)].
    * rewrite IHn, <- Hu2; [assumption | apply (query_size_natural_join_2 Hn)].
    * unfold pjoin_compatible_tuple in H; rewrite forallb_forall in H.
      unfold join_compatible_tuple; rewrite Fset.for_all_spec, forallb_forall.
      intros a Ha; rewrite compare_eq_true, Oset.compare_eq_iff.
      rewrite Fset.elements_inter in Ha; destruct Ha as [Ha Ha'].
      assert (Ka : In (a, dot T u1 a) t1).
      {
        rewrite Hu1; unfold tuple_as_pairs.
        rewrite in_map_iff; exists a; split; trivial.
      }
      generalize (H _ Ka); rewrite Hu2; unfold tuple_as_pairs.
      rewrite Oset.find_map; [ | assumption].
      rewrite Oset.eq_bool_true_iff; exact (fun h => h).
    * rewrite tuple_as_pairs_canonizes, Ht; unfold pjoin_tuple.
      rewrite <- tuple_as_pairs_canonizes, Hu1, Hu2.
      apply Oeset.compare_eq_sym; apply join_tuple_eq; apply canonized_tuple_eq.
- rewrite eval_query_unfold.
  rewrite eq_bool_iff, Feset.mem_map, Fset.mem_map; split; intro Ht;
    destruct Ht as [t' [Ht Ht']].
  + generalize (Feset.in_elements_mem _ _ _ Ht'); clear Ht'; intro Ht'.
    exists (tuple_as_pairs T t'); split.
    * apply sym_eq; rewrite <- tuple_as_pairs_canonizes.
      refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
      apply rename_tuple_eq; apply canonized_tuple_eq.
    * rewrite <- IHn; [assumption | apply (query_size_rename_q Hn)].
  + destruct (in_can_eval_query_tuple_as_pairs _ _ Ht') as [u Hu]; subst t'.
    rewrite <- IHn, Feset.mem_elements, Oeset.mem_bool_true_iff in Ht'.
    * destruct Ht' as [u' [Hu' Ht']].
      exists u'; split; [ | assumption].
      rewrite tuple_as_pairs_canonizes, <- Ht, <- tuple_as_pairs_canonizes.
      apply rename_tuple_eq; refine (Oeset.compare_eq_trans _ _ _ _ _ Hu').
      apply Oeset.compare_eq_sym; apply canonized_tuple_eq.
    * apply (query_size_rename_q Hn).
- rewrite eval_query_unfold; destruct (sort q1 =S?= sort q2).
  + destruct o; unfold Fset.interp_set_op; unfold Feset.interp_set_op.
    * rewrite Feset.mem_union, Fset.mem_union, 2 IHn; 
      [apply refl_equal | apply (query_size_set_2 Hn) | apply (query_size_set_1 Hn)].
    * rewrite Feset.mem_union, Fset.mem_union, 2 IHn; 
      [apply refl_equal | apply (query_size_set_2 Hn) | apply (query_size_set_1 Hn)].
    * rewrite Feset.mem_inter, Fset.mem_inter, 2 IHn; 
      [apply refl_equal | apply (query_size_set_2 Hn) | apply (query_size_set_1 Hn)].
    * rewrite Feset.mem_diff, Fset.mem_diff, 2 IHn; 
      [apply refl_equal | apply (query_size_set_2 Hn) | apply (query_size_set_1 Hn)].
  + rewrite Feset.empty_spec, Fset.empty_spec; apply refl_equal.
Qed.

Lemma eval_query_eq_can_eval_query :
  forall q1 q2, eval_query q1 =SE= eval_query q2 <-> can_eval_query q1 =S= can_eval_query q2.
Proof.
intros q1 q2; rewrite Feset.equal_spec, Fset.equal_spec; split; intros H t.
- rewrite eq_bool_iff; split; intro Ht;
  destruct (in_can_eval_query_tuple_as_pairs _ _ Ht) as [t' Ht']; subst t.
  + rewrite <- can_eval_query_ok, <- H, can_eval_query_ok; apply Ht.
  + rewrite <- can_eval_query_ok, H, can_eval_query_ok; apply Ht.
- rewrite 2 can_eval_query_ok; apply H.
Qed.

Lemma interp_formula_can_eval_query :
  forall f i, 
    interp_formula (fun x : query => Feset.elements _ (tuples_of_pairs (can_eval_query x))) i f = 
    interp_formula (fun x => Feset.elements _ (eval_query x)) i f.
Proof.
intros f i; apply (interp_formula_interp_dom_eq (OTuple T)).
- apply interp_term_eq_rec.
- intros i1 i2 H t; apply interp_term_eq; do 2 intro; apply H.
- intros d n _ t; rewrite <- 2 Feset.mem_elements.
  unfold tuples_of_pairs.
  rewrite eq_bool_iff, Feset.mem_mk_set, Oeset.mem_bool_true_iff; split.
  + intros [t' [Ht Ht']].
    rewrite in_map_iff in Ht'.
    destruct Ht' as [u [Ht' Hu]]; subst t'.
    generalize (Fset.in_elements_mem _ _ _ Hu); clear Hu; intro Hu.
    destruct (in_can_eval_query_tuple_as_pairs _ _ Hu) as [t' Ht']; subst u.
    rewrite <- can_eval_query_ok in Hu.
    rewrite (Feset.mem_eq_mem _ _ _ _ Ht).
    rewrite <- (Feset.mem_eq_mem _ _ _ _ (canonized_tuple_eq _ t')); assumption.
  + intro Ht.
    rewrite can_eval_query_ok, Fset.mem_elements, Oset.mem_bool_true_iff in Ht.
    exists (pairs_as_tuple T (tuple_as_pairs T t)); split.
    * apply canonized_tuple_eq.
    * rewrite in_map_iff; exists (tuple_as_pairs T t); split; trivial.
Qed.
*)

Lemma mem_Sigma :
  forall f q t,
    t inI Query_Sigma f q <-> (t inI q /\ eval_f f t = true).
Proof.
intros f q t.
rewrite (eval_query_unfold (Query_Sigma _ _)), Feset.filter_spec, Bool.Bool.andb_true_iff; 
  [ | intros t1 t2 _; apply eval_f_eq].
split; exact (fun h => h).
Qed.

Lemma mem_NaturalJoin : 
  forall q1 q2 t,
    t inI Query_NaturalJoin q1 q2 <->
    exists t1, 
      exists t2, 
        (t1 inI q1 /\ t2 inI q2 /\ join_compatible_tuple _ t1 t2 = true /\ 
         t =t= join_tuple T t1 t2).
Proof.
intros q1 q2 t; rewrite eval_query_unfold; unfold Data.natural_join_set.
rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff.
unfold Data.theta_join_list, Data.d_join_list; simpl; split.
- intros [t' [Ht Ht']]; rewrite in_flat_map in Ht'.
  destruct Ht' as [x1 [Hx1 Ht']].
  rewrite in_map_iff in Ht'.
  destruct Ht' as [x2 [Ht' Hx2]]; subst t'.
  rewrite filter_In in Hx2.
  destruct Hx2 as [Hx2 H].
  exists x1; exists x2; split; [ | split].
  + rewrite Feset.mem_elements; apply Oeset.in_mem_bool; assumption.
  + rewrite Feset.mem_elements; apply Oeset.in_mem_bool; assumption.
  + split; assumption.
- intros [x1 [x2 [Hx1 [Hx2 [H Ht]]]]].
  rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Hx1, Hx2.
  destruct Hx1 as [t1 [Hx1 Ht1]].
  destruct Hx2 as [t2 [Hx2 Ht2]].
  exists (join_tuple T t1 t2); split.
  + rewrite (Oeset.compare_eq_1 _ _ _ _ Ht); apply join_tuple_eq; assumption.
  + rewrite in_flat_map; exists t1; split; [assumption | ].
    rewrite in_map_iff; exists t2; split; [apply refl_equal | ].
    rewrite filter_In; split; [assumption | ].
    rewrite <- H; apply sym_eq.
    apply join_compatible_tuple_eq; assumption.
Qed.

Lemma mem_Pi : 
  forall l q t, 
    t inI (Query_Pi l q) <-> 
    (exists t', t' inI q /\ t =t= mk_tuple T (Fset.mk_set (A T) l) (dot T t')).
Proof.
intros l q t; rewrite eval_query_unfold; cbv zeta.
rewrite Feset.mem_map; split.
- intros [a [Ht Ha]]; exists a; split; trivial.
  apply Feset.in_elements_mem; assumption.
- intros [t' [Ht' Ht]].
  rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ht'.
  destruct Ht' as [t'' [Ht' Ht'']].
  exists t''; split; trivial.
  refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
  apply mk_tuple_eq.
  + apply Fset.equal_refl.
  + intros a _ _; apply tuple_eq_dot; assumption.
Qed.

Lemma mem_Rename :
  forall rho q t, 
    t inI (Query_Rename rho q) <-> 
    (exists t', t' inI q /\ t =t= rename_tuple T (apply_renaming T rho) t').
Proof.
intros rho q t.
rewrite eval_query_unfold, Feset.mem_map; split.
- intros [t' [Ht Ht']].
  exists t'; split; [apply Feset.in_elements_mem | ]; trivial.
- intros [t' [Ht Ht']].
  rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ht.
  destruct Ht as [t'' [Ht Ht'']].
  exists t''; split; [ | assumption].
  refine (Oeset.compare_eq_trans _ _ _ _ Ht' _).
  apply rename_tuple_eq_2; assumption.
Qed. 

End Global.

Section Linearize.

Fixpoint deep_variables_q q :=
  match q with
    | Query_Empty _ 
    | Query_Empty_Tuple 
    | Query_Basename _ => Fset.empty _
    | Query_Sigma f q1 => 
      let deep_variables_f :=
          (fix deep_variables_f f := 
             match f with
               | TTrue => Fset.empty (FVR OQ OP OSymbol)
               | Atom _ l => 
                 let deep_variables_t :=
                     (fix deep_variables_t t :=
                        match t with
                          | Var (Vrbl q n) => Fset.add _ (Vrbl q n) (deep_variables_q q)
                          | Term _ l => Fset.Union _ (map deep_variables_t l)
                        end) in                            
                 Fset.Union _ (map deep_variables_t l)
               | Conj _ f1 f2 => (deep_variables_f f1) unionS (deep_variables_f f2)
               | Not f => deep_variables_f f
               | Quant _ (Vrbl q n) f => 
                 Fset.add _ (Vrbl q n) (deep_variables_q q unionS deep_variables_f f) 
             end) in
      deep_variables_f f unionS deep_variables_q q1
    | Query_Pi _ q1 => deep_variables_q q1
    | Query_NaturalJoin q1 q2 => deep_variables_q q1 unionS deep_variables_q q2
    | Query_Rename _ q1 => deep_variables_q q1
    | Query_Set _ q1 q2 => deep_variables_q q1 unionS deep_variables_q q2
  end.

Fixpoint deep_variables_t (t : term _ symbol) :=
  match t with
    | Var (Vrbl q n) => Fset.add (FVR OQ OP OSymbol) (Vrbl q n) (deep_variables_q q)
    | Term _ l => Fset.Union _ (map deep_variables_t l)
  end.

Fixpoint deep_variables_f (f : formula query predicate symbol) := 
  match f with
    | TTrue => Fset.empty (FVR OQ OP OSymbol)
    | Atom _ l => Fset.Union _ (map deep_variables_t l)
    | Conj _ f1 f2 => (deep_variables_f f1) unionS (deep_variables_f f2)
    | Not f => deep_variables_f f
    | Quant _ (Vrbl q n) f => 
      Fset.add _ (Vrbl q n) (deep_variables_q q unionS deep_variables_f f) 
  end.

Lemma deep_variables_q_unfold :
  forall q, deep_variables_q q =
            match q with
              | Query_Empty _ 
              | Query_Empty_Tuple 
              | Query_Basename _ => Fset.empty _
              | Query_Sigma f q1 => deep_variables_f f unionS deep_variables_q q1
              | Query_Pi _ q1 => deep_variables_q q1
              | Query_NaturalJoin q1 q2 => deep_variables_q q1 unionS deep_variables_q q2
              | Query_Rename _ q1 => deep_variables_q q1
              | Query_Set _ q1 q2 => deep_variables_q q1 unionS deep_variables_q q2
            end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Lemma deep_variables_t_unfold :
  forall t, deep_variables_t t = 
            match t with
              | Var (Vrbl q n) => Fset.add (FVR OQ OP OSymbol) (Vrbl q n) (deep_variables_q q)
              | Term _ l => Fset.Union _ (map deep_variables_t l)
            end.
Proof.
intro t; case t; intros; apply refl_equal.
Qed.

Lemma deep_variables_f_unfold :
  forall f, deep_variables_f f =
  match f with
    | TTrue => Fset.empty (FVR OQ OP OSymbol)
    | Atom _ l => Fset.Union _ (map deep_variables_t l)
    | Conj _ f1 f2 => (deep_variables_f f1) unionS (deep_variables_f f2)
    | Not f => deep_variables_f f
    | Quant _ (Vrbl q n) f => 
      Fset.add _ (Vrbl q n) (deep_variables_q q unionS deep_variables_f f) 
  end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Fixpoint max_q q : N :=
  match q with
    | Query_Empty _ 
    | Query_Empty_Tuple
    | Query_Basename _ => 0%N
    | Query_Sigma f q1 => 
      let max_f :=
          (fix max_f f := 
             match f with
               | TTrue => 0%N
               | Atom _ l => 
                 let max_t :=
                     (fix max_t t := 
                        match t with
                          | Var (Vrbl q n) => N.max (max_q q) n
                          | Term _ l3 => maxN (List.map max_t l3)
                        end) in
                 maxN (List.map max_t l)
               | Conj _ f1 f2 => N.max (max_f f1) (max_f f2)
               | Not f => max_f f
               | Quant _ (Vrbl q n) F => N.max (N.max (max_q q) n) (max_f F)
             end) in
            N.max (max_f f) (max_q q1)
    | Query_Pi _ q1 => max_q q1
    | Query_NaturalJoin q1 q2 => N.max (max_q q1) (max_q q2)
    | Query_Rename _ q1 => max_q q1
    | Query_Set _ q1 q2 => N.max (max_q q1) (max_q q2)
  end.

Fixpoint max_t (t : term query symbol) : N :=
  match t with
    | Var (Vrbl q n) => N.max (max_q q) n
    | Term _ l => maxN (List.map max_t l)
  end.

Fixpoint max_f (f : formula query predicate symbol) : N :=    
  match f with
    | TTrue => 0%N
    | Atom _ l =>  maxN (List.map max_t l)
    | Conj _ f1 f2 => N.max (max_f f1) (max_f f2)
    | Not f => max_f f
    | Quant _ (Vrbl q n) F => N.max (N.max (max_q q) n) (max_f F)
  end.

Lemma max_q_unfold :
  forall q, max_q q =
  match q with
    | Query_Empty _ 
    | Query_Empty_Tuple
    | Query_Basename _ => 0%N
    | Query_Sigma f q1 => N.max (max_f f) (max_q q1)
    | Query_Pi _ q1 => max_q q1
    | Query_NaturalJoin q1 q2 => N.max (max_q q1) (max_q q2)
    | Query_Rename _ q1 => max_q q1
    | Query_Set _ q1 q2 => N.max (max_q q1) (max_q q2)
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Lemma max_f_unfold :
  forall f, max_f f =
            match f with
              | TTrue => 0%N
              | Atom _ l =>  maxN (List.map max_t l)
              | Conj _ f1 f2 => N.max (max_f f1) (max_f f2)
              | Not f => max_f f
              | Quant _ (Vrbl q n) F => N.max (N.max (max_q q) n) (max_f F)
            end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma max_t_unfold :
  forall t, max_t t =
            match t with
              | Var (Vrbl q n) => N.max (max_q q) n
              | Term _ l => maxN (List.map max_t l)
            end.
Proof.
intro t; case t; intros; apply refl_equal.
Qed.

Lemma deep_variables_q_max_q_etc :
  forall n, 
  (forall Q, query_size Q <= n ->
             forall q m, Vrbl q m inS deep_variables_q Q -> (m <= max_q Q)%N) /\
  (forall f, tree_size (tree_of_formula f) <= n ->
             forall q m, Vrbl q m inS deep_variables_f f -> (m <= max_f f)%N) /\
  (forall t, tree_size (tree_of_term t) <= n -> 
              forall q m, Vrbl q m inS deep_variables_t t -> (m <= max_t t)%N).
Proof.
intro n; induction n as [ | n];
[ split; 
  [ intros q1 Hn; destruct q1; inversion Hn
  | split ; 
    [ intros f1 Hn; destruct f1; inversion Hn
    | intros t1 Hn; destruct t1; inversion Hn]]
| ].
split; [ | split].
- intros Q Hn q m H; destruct Q; rewrite deep_variables_q_unfold in H; rewrite max_q_unfold.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * refine (N.le_trans _ (max_f f) _ _ (N.le_max_l _ _)).
      refine ((proj1 (proj2 IHn) _ (query_size_sigma_f Hn) q m H)).
    * refine (N.le_trans _ (max_q Q) _ _ (N.le_max_r _ _)).
      refine ((proj1 IHn) _ (query_size_sigma_q Hn) q m H).
  + apply (proj1 IHn _ (query_size_pi_q Hn) _ _ H).
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * refine (N.le_trans _ (max_q Q1) _ _ (N.le_max_l _ _)).
      refine ((proj1 IHn) _ (query_size_natural_join_1 Hn) q m H).
    * refine (N.le_trans _ (max_q Q2) _ _ (N.le_max_r _ _)).
      refine ((proj1 IHn) _ (query_size_natural_join_2 Hn) q m H).
  + apply (proj1 IHn _ (query_size_rename_q Hn) _ _ H).
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * refine (N.le_trans _ (max_q Q1) _ _ (N.le_max_l _ _)).
      refine ((proj1 IHn) _ (query_size_set_1 Hn) q m H).
    * refine (N.le_trans _ (max_q Q2) _ _ (N.le_max_r _ _)).
      refine ((proj1 IHn) _ (query_size_set_2 Hn) q m H).
- intros f Hn q m H; destruct f; rewrite deep_variables_f_unfold in H; rewrite max_f_unfold.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.mem_Union in H; destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst s.
    refine (N.le_trans _ _ _ _ (le_maxN (max_t t) _ _)).
    * apply (proj2 (proj2 IHn) t (formula_size_atom_t _ Hn Ht) q m H).
    * rewrite in_map_iff; exists t; split; trivial.
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * refine (N.le_trans _ _ _ _ (N.le_max_l _ _)). 
      apply (proj1 (proj2 IHn) _ (formula_size_conj_1 Hn) q m H).
    * refine (N.le_trans _ _ _ _ (N.le_max_r _ _)). 
      apply (proj1 (proj2 IHn) _ (formula_size_conj_2 Hn) q m H).
  + apply (proj1 (proj2 IHn) _ (formula_size_not_f Hn) q m H).
  + destruct v as [qv nv].
    rewrite Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
    destruct H as [H | [H | H]].
    * injection H; clear H; do 2 intro; subst qv nv.
      refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
      apply N.le_max_r.
    * refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
      refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
      refine (proj1 IHn qv _ q m H).
      refine (le_trans _ _ _ _ (formula_size_quant_v Hn)).
      simpl; apply le_S; apply le_plus_l.
    * refine (N.le_trans _ _ _ _ (N.le_max_r _ _)).
      refine (proj1 (proj2 IHn) _ (formula_size_quant_f Hn) q m H).
- intros t Hn q m H; destruct t; rewrite deep_variables_t_unfold in H; rewrite max_t_unfold.
  + destruct v as [qv nv].
    rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
    destruct H as [H | H].
    * injection H; clear H; do 2 intro; subst qv nv.
      apply N.le_max_r.
    * refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
      refine (proj1 IHn qv _ q m H).
      refine (le_trans _ _ _ _ (term_size_v Hn)).
      simpl; apply le_S; apply le_plus_l.
  + rewrite Fset.mem_Union in H.
    destruct H as [_s [Hs H]].
    rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst _s.
    refine (N.le_trans _ _ _ _ (le_maxN (max_t t) _ _)).
    * refine (proj2 (proj2 IHn) t _ q m H).
      apply (term_size_t _ Hn Ht).
    * rewrite in_map_iff; exists t; split; trivial.
Qed.    

Lemma deep_variables_q_max_q :
  forall n q Q, Vrbl q n inS deep_variables_q Q -> (n <= max_q Q)%N.
Proof.
intros n q Q.
refine (proj1 (deep_variables_q_max_q_etc (query_size Q)) Q _ _ _); apply le_refl.
Qed.

Lemma deep_variables_f_max_f :
  forall n q f, Vrbl q n inS deep_variables_f f -> (n <= max_f f)%N.
Proof.
intros n q f.
refine (proj1 (proj2 (deep_variables_q_max_q_etc (tree_size (tree_of_formula f)))) f _ _ _); 
  apply le_refl.
Qed.

Definition FQ := Fset.build OQ.

Fixpoint all_queries_q q :=
  match q with
    | Query_Empty _ 
    | Query_Empty_Tuple 
    | Query_Basename _ => Fset.singleton FQ q
    | Query_Sigma f q1 => 
      let all_queries_f :=
          (fix all_queries_f f' := 
             match f' with
               | TTrue => Fset.empty FQ
               | Atom _ l =>
                 let all_queries_t := 
                     (fix all_queries_t t :=
                        match t with
                          | Var (Vrbl q _) => all_queries_q q
                          | Term _ l => Fset.Union FQ (List.map all_queries_t l)
                        end
                     ) in
                 Fset.Union FQ (List.map all_queries_t l)
               | Conj _ f1 f2 => Fset.union _ (all_queries_f f1) (all_queries_f f2)
               | Not f1 => all_queries_f f1
               | Quant _ (Vrbl q n) f1 => Fset.union _ (all_queries_q q) (all_queries_f f1)
             end) in
      Fset.add _ q (Fset.union _ (all_queries_f f) (all_queries_q q1))
    | Query_Pi _ q1 => Fset.add _ q (all_queries_q q1)
    | Query_NaturalJoin q1 q2 => Fset.add _ q (Fset.union _ (all_queries_q q1) (all_queries_q q2))
    | Query_Rename _ q1 => Fset.add _ q (all_queries_q q1)
    | Query_Set _ q1 q2 => Fset.add _ q (Fset.union _ (all_queries_q q1) (all_queries_q q2))
  end.

Fixpoint all_queries_t (t : term query symbol) := 
  match t with
    | Var (Vrbl q _) => all_queries_q q
    | Term _ l => Fset.Union FQ (List.map all_queries_t l)
  end.

Fixpoint all_queries_f (f' : formula query predicate symbol) := 
  match f' with
    | TTrue => Fset.empty FQ
    | Atom _ l => Fset.Union FQ (List.map all_queries_t l)
    | Conj _ f1 f2 => Fset.union _ (all_queries_f f1) (all_queries_f f2)
    | Not f1 => all_queries_f f1
    | Quant _ (Vrbl q n) f1 => Fset.union _ (all_queries_q q) (all_queries_f f1)
  end.

Lemma all_queries_q_unfold :
  forall q, all_queries_q q =
  match q with
    | Query_Empty _ 
    | Query_Empty_Tuple 
    | Query_Basename _ => Fset.singleton FQ q
    | Query_Sigma f q1 => Fset.add _ q (Fset.union _ (all_queries_f f) (all_queries_q q1))
    | Query_Pi _ q1 => Fset.add _ q (all_queries_q q1)
    | Query_NaturalJoin q1 q2 => Fset.add _ q (Fset.union _ (all_queries_q q1) (all_queries_q q2))
    | Query_Rename _ q1 => Fset.add _ q (all_queries_q q1)
    | Query_Set _ q1 q2 => Fset.add _ q (Fset.union _ (all_queries_q q1) (all_queries_q q2))
  end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.

Lemma all_queries_f_unfold :
  forall f, all_queries_f f =
            match f with
              | TTrue => Fset.empty FQ
              | Atom _ l => Fset.Union FQ (List.map all_queries_t l)
              | Conj _ f1 f2 => Fset.union _ (all_queries_f f1) (all_queries_f f2)
              | Not f1 => all_queries_f f1
              | Quant _ (Vrbl q n) f1 => Fset.union _ (all_queries_q q) (all_queries_f f1)
            end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma all_queries_t_unfold :
  forall t, all_queries_t t = 
            match t with
              | Var (Vrbl q _) => all_queries_q q
              | Term _ l => Fset.Union FQ (List.map all_queries_t l)
            end.
Proof.
intro t; case t; intros; apply refl_equal.
Qed.

Lemma q_in_all_queries_q :
  forall q, q inS (all_queries_q q).
Proof.
intro q; rewrite all_queries_q_unfold; destruct q.
- rewrite Fset.singleton_spec, Oset.eq_bool_true_iff; apply refl_equal.
- rewrite Fset.singleton_spec, Oset.eq_bool_true_iff; apply refl_equal.
- rewrite Fset.singleton_spec, Oset.eq_bool_true_iff; apply refl_equal.
- rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
  left; apply refl_equal.
- rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
  left; apply refl_equal.
- rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
  left; apply refl_equal.
- rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
  left; apply refl_equal.
- rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
  left; apply refl_equal.
Qed.

Lemma deep_variables_q_all_queries_q_etc :
  forall n, 
  (forall Q, query_size Q <= n ->
             forall q m, Vrbl q m inS deep_variables_q Q -> q inS all_queries_q Q) /\
  (forall f, tree_size (tree_of_formula f) <= n ->
             forall q m, Vrbl q m inS deep_variables_f f -> q inS all_queries_f f) /\
  (forall t, tree_size (tree_of_term t) <= n -> 
              forall q m, Vrbl q m inS deep_variables_t t -> q inS all_queries_t t).
Proof.
intro n; induction n as [ | n];
[ split; 
  [ intros q1 Hn; destruct q1; inversion Hn
  | split ; 
    [ intros f1 Hn; destruct f1; inversion Hn
    | intros t1 Hn; destruct t1; inversion Hn]]
| ].
split; [ | split].
- intros Q Hn q m H; destruct Q; 
  rewrite deep_variables_q_unfold in H; rewrite all_queries_q_unfold.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      refine ((proj1 (proj2 IHn) _ (query_size_sigma_f Hn) q m H)).
    * rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
      refine ((proj1 IHn) _ (query_size_sigma_q Hn) q m H).
  + rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
    apply (proj1 IHn _ (query_size_pi_q Hn) _ _ H).
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      refine ((proj1 IHn) _ (query_size_natural_join_1 Hn) q m H).
    * rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
      refine ((proj1 IHn) _ (query_size_natural_join_2 Hn) q m H).
  + rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
    apply (proj1 IHn _ (query_size_rename_q Hn) _ _ H).
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      refine ((proj1 IHn) _ (query_size_set_1 Hn) q m H).
    * rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
      refine ((proj1 IHn) _ (query_size_set_2 Hn) q m H).
- intros f Hn q m H; destruct f; 
  rewrite deep_variables_f_unfold in H; rewrite all_queries_f_unfold.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.mem_Union in H; destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst s.
    rewrite Fset.mem_Union.
    exists (all_queries_t t); split.
    * rewrite in_map_iff; exists t; split; trivial.
    * apply (proj2 (proj2 IHn) t (formula_size_atom_t _ Hn Ht) q m H).
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      apply (proj1 (proj2 IHn) _ (formula_size_conj_1 Hn) q m H).
    * rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
      apply (proj1 (proj2 IHn) _ (formula_size_conj_2 Hn) q m H).
  + apply (proj1 (proj2 IHn) _ (formula_size_not_f Hn) q m H).
  + destruct v as [qv nv].
    rewrite Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
    destruct H as [H | [H | H]].
    * injection H; clear H; do 2 intro; subst qv nv.
      rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      apply q_in_all_queries_q.
    * rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      refine (proj1 IHn qv _ q m H).
      refine (le_trans _ _ _ _ (formula_size_quant_v Hn)).
      simpl; apply le_S; apply le_plus_l.
    * rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
      refine (proj1 (proj2 IHn) _ (formula_size_quant_f Hn) q m H).
- intros t Hn q m H; destruct t; 
  rewrite deep_variables_t_unfold in H; rewrite all_queries_t_unfold.
  + destruct v as [qv nv].
    rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
    destruct H as [H | H].
    * injection H; clear H; do 2 intro; subst qv nv.
      apply q_in_all_queries_q.
    * refine (proj1 IHn qv _ q m H).
      refine (le_trans _ _ _ _ (term_size_v Hn)).
      simpl; apply le_S; apply le_plus_l.
  + rewrite Fset.mem_Union in H.
    destruct H as [_s [Hs H]].
    rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst _s.
    rewrite Fset.mem_Union.
    exists (all_queries_t t); split.
    * rewrite in_map_iff; exists t; split; trivial.
    * refine (proj2 (proj2 IHn) t _ q m H).
      apply (term_size_t _ Hn Ht).
Qed.    

Lemma deep_variables_q_all_queries_q :
  forall n q Q, Vrbl q n inS deep_variables_q Q -> q inS (all_queries_q Q).
Proof.
intros n q Q.
refine (proj1 (deep_variables_q_all_queries_q_etc (query_size Q)) Q _ _ _); apply le_refl.
Qed.

Lemma deep_variables_f_all_queries_f :
  forall n q f, Vrbl q n inS deep_variables_f f -> q inS (all_queries_f f).
Proof.
intros n q f.
refine (proj1 (proj2 (deep_variables_q_all_queries_q_etc 
                        (tree_size (tree_of_formula f)))) f _ _ _); 
  apply le_refl.
Qed.

Fixpoint linearize_q M n_q q :=
  match q with
    | Query_Empty _
    | Query_Empty_Tuple
    | Query_Basename  _ => q
    | Query_Sigma f q =>
      let linearize_f :=
          (fix linearize_f M n_q (f1 : formula query predicate symbol) :=
             match f1 with
               | TTrue => f1
               | Atom p l => 
                 let linearize_t :=
                     (fix linearize_t M n_q t :=
                        match t with
                          | Var (Vrbl q n) => 
                            Var (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                          | Term g l => Term g (List.map (linearize_t M n_q) l)
                        end) in
                 Atom p (List.map (linearize_t M n_q) l)
               | Conj o f1 f2 => Conj o (linearize_f M n_q f1) (linearize_f M n_q f2)
               | Not f1 => Not (linearize_f M n_q f1)
               | Quant qft (Vrbl q1 n1) f1 => 
                 Quant qft (Vrbl (linearize_q M n_q q1) ((M * (n_q q1)) + n1)%N) 
                       (linearize_f M n_q f1)
          end) in
      Query_Sigma (linearize_f M n_q f) (linearize_q M n_q q)
    | Query_Pi s q => Query_Pi s (linearize_q M n_q q)
    | Query_NaturalJoin q1 q2 => Query_NaturalJoin (linearize_q M n_q q1) (linearize_q M n_q q2)
    | Query_Rename rho q => Query_Rename rho (linearize_q M n_q q)
    | Query_Set o q1 q2 => Query_Set o (linearize_q M n_q q1) (linearize_q M n_q q2)
  end.

Fixpoint linearize_t M n_q (t : term query symbol) :=
  match t with
    | Var (Vrbl q n) => 
      Var (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
    | Term g l => Term g (List.map (linearize_t M n_q) l)
  end.

Fixpoint linearize_f M n_q (f1 : formula query predicate symbol) :=
  match f1 with
    | TTrue => f1
    | Atom p l => Atom p (List.map (linearize_t M n_q) l)
    | Conj o f1 f2 => Conj o (linearize_f M n_q f1) (linearize_f M n_q f2)
    | Not f1 => Not (linearize_f M n_q f1)
    | Quant qft (Vrbl q1 n1) f1 => 
      Quant qft (Vrbl (linearize_q M n_q q1) ((M * (n_q q1)) + n1)%N) (linearize_f M n_q f1)
  end.

Lemma linearize_q_unfold :
  forall M n_q q, 
    linearize_q M n_q q =
    match q with
      | Query_Empty _
      | Query_Empty_Tuple
      | Query_Basename  _ => q
      | Query_Sigma f q => Query_Sigma (linearize_f M n_q f) (linearize_q M n_q q)
      | Query_Pi s q => Query_Pi s (linearize_q M n_q q)
      | Query_NaturalJoin q1 q2 => Query_NaturalJoin (linearize_q M n_q q1) (linearize_q M n_q q2)
      | Query_Rename rho q => Query_Rename rho (linearize_q M n_q q)
      | Query_Set o q1 q2 => Query_Set o (linearize_q M n_q q1) (linearize_q M n_q q2)
  end.
Proof.
intros M n_q q; case q; intros; apply refl_equal.
Qed.

Lemma linearize_f_unfold :
forall M n_q f1, 
  linearize_f M n_q f1 =
  match f1 with
    | TTrue => f1
    | Atom p l => Atom p (List.map (linearize_t M n_q) l)
    | Conj o f1 f2 => Conj o (linearize_f M n_q f1) (linearize_f M n_q f2)
    | Not f1 => Not (linearize_f M n_q f1)
    | Quant qft (Vrbl q1 n1) f1 => 
      Quant qft (Vrbl (linearize_q M n_q q1) ((M * (n_q q1)) + n1)%N) (linearize_f M n_q f1)
  end.
Proof.
intros M n_q f1; case f1; intros; apply refl_equal.
Qed.

Lemma linearize_t_unfold :
  forall M n_q t,
    linearize_t M n_q t =
    match t with
      | Var (Vrbl q n) => 
        Var (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
      | Term g l => Term g (List.map (linearize_t M n_q) l)
    end.
Proof.
intros M n_q t; case t; intros; apply refl_equal.
Qed.

Lemma sort_linearize_q :
  forall M n_q q, sort (linearize_q M n_q q) =S= sort q.
Proof.
intros M n_q q; rewrite Fset.equal_spec; induction q; simpl; trivial.
- intro a; rewrite 2 Fset.mem_union, IHq1, IHq2; apply refl_equal.
- rewrite <- Fset.equal_spec; apply Fset.map_eq_s; rewrite Fset.equal_spec; intro a.
  apply IHq.
Qed.

Lemma variables_t_linearize_t :
  forall M n_q t,
    variables_t (FVR OQ OP OSymbol) (linearize_t M n_q t) =S=
    Fset.map _ (FVR OQ OP OSymbol) 
             (fun x => 
                match x with 
                  | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                end) 
             (variables_t (FVR OQ OP OSymbol) t).
Proof.
intros M n_q t; pattern t; apply term_rec3; clear t.
- intros [q m]; rewrite linearize_t_unfold, 2 variables_t_unfold.
  unfold Fset.map; rewrite Fset.elements_singleton, 2 map_unfold.
  unfold Fset.mk_set; rewrite Fset.equal_spec.
  intro x; rewrite Fset.singleton_spec, Fset.add_spec, Fset.empty_spec, Bool.orb_false_r.
  apply refl_equal.
- intros f l IHl.
  rewrite linearize_t_unfold, 2 (variables_t_unfold _ (Term _ _)).
  rewrite Fset.equal_spec; intro x.
  rewrite eq_bool_iff, Fset.mem_map, Fset.mem_Union; split.
  + intros [s [Hs Hx]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    rewrite in_map_iff in Ht.
    destruct Ht as [u [Ht Hu]]; subst t.
    assert (IHu := IHl _ Hu); rewrite Fset.equal_spec in IHu.
    rewrite IHu, Fset.mem_map in Hx.
    destruct Hx as [[q n] [Hx Ku]]; subst x.
    exists (Vrbl q n); split; [apply refl_equal | ].
    rewrite Fset.mem_Union.
    exists (variables_t (FVR OQ OP OSymbol) u); split; [ | assumption].
    rewrite in_map_iff; exists u; split; trivial.
  + intros [[q n] [Hx H]]; subst x.
    rewrite Fset.mem_Union in H.
    destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    assert (IHt := IHl _ Ht).
    rewrite Fset.equal_spec in IHt.
    exists (variables_t (FVR OQ OP OSymbol) (linearize_t M n_q t)); split.
    * rewrite map_map, in_map_iff; exists t; split; trivial.
    * rewrite IHt, Fset.mem_map.
    exists (Vrbl q n); split; trivial.
Qed.

Lemma variables_f_deep_variables_f_etc :
  forall n, 
  (forall f, tree_size (tree_of_formula f) <= n -> 
             variables_f OQ OP OSymbol f subS deep_variables_f f) /\
  (forall t, tree_size (tree_of_term t) <= n -> 
             variables_t (FVR OQ OP OSymbol) t subS deep_variables_t t).
Proof.
intro n; induction n as [ | n];
[ split ; 
  [ intros f1 Hn; destruct f1; inversion Hn
  | intros t1 Hn; destruct t1; inversion Hn]
| ].
split.
- intros f Hn; rewrite Fset.subset_spec; intros x H; destruct f; 
  rewrite variables_f_unfold in H; rewrite deep_variables_f_unfold.
  + rewrite Fset.empty_spec in H; discriminate H.
  + rewrite Fset.mem_Union in H; destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst s.
    rewrite Fset.mem_Union.
    exists (deep_variables_t t); split.
    * rewrite in_map_iff; exists t; split; trivial.
    * revert x H; rewrite <- Fset.subset_spec; 
      apply (proj2 IHn t (formula_size_atom_t _ Hn Ht)).
  + rewrite Fset.mem_union, Bool.Bool.orb_true_iff in H; destruct H as [H | H].
    * rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
      revert x H; rewrite <- Fset.subset_spec; 
      apply (proj1 IHn _ (formula_size_conj_1 Hn)).
    * rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
      revert x H; rewrite <- Fset.subset_spec; 
      apply (proj1 IHn _ (formula_size_conj_2 Hn)).
  + revert x H; rewrite <- Fset.subset_spec; 
    apply (proj1 IHn _ (formula_size_not_f Hn)).
  + destruct v as [qv nv].
    rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
    rewrite Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
    destruct H as [H | H].
    * left; assumption.
    * right; right.
      revert x H; rewrite <- Fset.subset_spec; 
      refine (proj1 IHn f (formula_size_quant_f Hn)).
- intros t Hn; rewrite Fset.subset_spec; intros x H; destruct t; 
  rewrite variables_t_unfold in H; rewrite deep_variables_t_unfold.
  + destruct v as [qv nv].
    rewrite Fset.singleton_spec in H.
    rewrite Fset.add_spec, H; apply refl_equal.
  + rewrite Fset.mem_Union in H.
    destruct H as [_s [Hs H]].
    rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst _s.
    rewrite Fset.mem_Union.
    exists (deep_variables_t t); split.
    * rewrite in_map_iff; exists t; split; trivial.
    * revert x H; rewrite <- Fset.subset_spec; 
      refine (proj2 IHn t _).
      apply (term_size_t _ Hn Ht).
Qed.    

Lemma variables_f_deep_variables_f :
  forall f, variables_f OQ OP OSymbol f subS deep_variables_f f.
Proof.
intro f.
refine (proj1 (variables_f_deep_variables_f_etc (tree_size (tree_of_formula f))) f _).
apply le_refl.
Qed.

Lemma variables_f_linearize_f :
  forall M n_q f, 
    (max_f f < M)%N ->
    (forall q1 q2, q1 inS all_queries_f f -> q2 inS all_queries_f f -> 
                   n_q q1 = n_q q2 -> q1 = q2) -> 
    variables_f OQ OP OSymbol (linearize_f M n_q f) =S=
    Fset.map _ (FVR OQ OP OSymbol) 
             (fun x => 
                match x with 
                  | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                end) 
             (variables_f OQ OP OSymbol f).
Proof.
intros M n_q f; rewrite Fset.equal_spec; induction f; intros HM Hn_q x.
- rewrite linearize_f_unfold, variables_f_unfold.
  unfold Fset.map; rewrite Fset.elements_empty, map_unfold; apply refl_equal.
- rewrite linearize_f_unfold, 2 variables_f_unfold, map_map.
  rewrite eq_bool_iff, Fset.mem_Union, Fset.mem_map; split.
  + intros [s [Hs Hx]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    assert (IHt := variables_t_linearize_t M n_q t).
    rewrite Fset.equal_spec in IHt; rewrite IHt in Hx.
    rewrite Fset.mem_map in Hx.
    destruct Hx as [[q n] [Hx H]]; subst x.
    exists (Vrbl q n); split; [apply refl_equal | ].
    rewrite Fset.mem_Union.
    exists (variables_t (FVR OQ OP OSymbol) t); split; [ | assumption].
    rewrite in_map_iff; exists t; split; trivial.
  + intros [[q n] [Hx H]]; subst x.
    rewrite Fset.mem_Union in H.
    destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    exists (variables_t (FVR OQ OP OSymbol) (linearize_t M n_q t)); split.
    * rewrite in_map_iff; exists t; split; trivial.
    * assert (IHt := variables_t_linearize_t M n_q t).
      rewrite Fset.equal_spec in IHt; rewrite IHt.
      rewrite Fset.mem_map.
      exists (Vrbl q n); split; trivial.
- rewrite linearize_f_unfold, variables_f_unfold, Fset.mem_union, IHf1, IHf2.
  + rewrite (variables_f_unfold _ _ _ (Conj _ _ _)), <- Fset.mem_union; apply sym_eq.
    clear IHf1 IHf2; revert x; rewrite <- Fset.equal_spec; apply Fset.map_union.
  + refine (N.le_lt_trans _ _ _ _ HM).
    rewrite (max_f_unfold (Conj _ _ _)).
    apply N.le_max_r.
  + intros q1 q2 Hq1 Hq2; apply Hn_q.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq1, Bool.orb_true_r; apply refl_equal.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq2, Bool.orb_true_r; apply refl_equal.
  + refine (N.le_lt_trans _ _ _ _ HM).
    rewrite (max_f_unfold (Conj _ _ _)).
    apply N.le_max_l.
  + intros q1 q2 Hq1 Hq2; apply Hn_q.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq1; apply refl_equal.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq2; apply refl_equal.
- apply IHf; assumption.
- destruct v as [qv nv].
  rewrite linearize_f_unfold, 2 (variables_f_unfold _ _ _ (Quant _ _ _)).
  assert (HM' : (max_f f < M)%N).
  {
    refine (N.le_lt_trans _ _ _ _ HM).
    rewrite (max_f_unfold (Quant _ _ _)).
    apply N.le_max_r.
  }
  rewrite Fset.add_spec, (IHf HM'), eq_bool_iff, Bool.Bool.orb_true_iff, 2 Fset.mem_map; 
    [split | ].
  + intros [H | [[q1 n1] [Hx H1]]].
    * rewrite Oset.eq_bool_true_iff in H; subst x.
      exists (Vrbl qv nv); split; trivial.
      rewrite Fset.add_spec, Bool.Bool.orb_true_iff; left.
      rewrite Oset.eq_bool_true_iff; apply refl_equal.
    * exists (Vrbl q1 n1); subst x; split; [apply refl_equal | ].
      rewrite Fset.add_spec, Bool.Bool.orb_true_iff; right; assumption.
  + intros [[q1 n1] [Hx H1]]; subst x.
    rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H1.
    destruct H1 as [H1 | H1].
    * injection H1; clear H1; do 2 intro; subst.
      rewrite Oset.eq_bool_true_iff; left; apply refl_equal.
    * right; exists (Vrbl q1 n1); split; trivial.
  + intros q1 q2 Hq1 Hq2; apply Hn_q.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq1, Bool.orb_true_r; apply refl_equal.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq2, Bool.orb_true_r; apply refl_equal.
Qed.

Lemma free_variables_f_linearize_f :
  forall M n_q f, 
    (max_f f < M)%N ->
    (forall q1 q2, q1 inS all_queries_f f -> q2 inS all_queries_f f -> 
                   n_q q1 = n_q q2 -> q1 = q2) -> 
    free_variables_f OQ OP OSymbol (linearize_f M n_q f) =S=
    Fset.map _ (FVR OQ OP OSymbol) 
             (fun x => 
                match x with 
                  | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                end) 
             (free_variables_f OQ OP OSymbol f).
Proof.
intros M n_q f; rewrite Fset.equal_spec; induction f; intros HM Hn_q x.
- rewrite linearize_f_unfold, free_variables_f_unfold.
  unfold Fset.map; rewrite Fset.elements_empty, map_unfold; apply refl_equal.
- rewrite linearize_f_unfold, 2 free_variables_f_unfold, map_map.
  rewrite eq_bool_iff, Fset.mem_Union, Fset.mem_map; split.
  + intros [s [Hs Hx]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    assert (IHt := variables_t_linearize_t M n_q t).
    rewrite Fset.equal_spec in IHt; rewrite IHt in Hx.
    rewrite Fset.mem_map in Hx.
    destruct Hx as [[q n] [Hx H]]; subst x.
    exists (Vrbl q n); split; [apply refl_equal | ].
    rewrite Fset.mem_Union.
    exists (variables_t (FVR OQ OP OSymbol) t); split; [ | assumption].
    rewrite in_map_iff; exists t; split; trivial.
  + intros [[q n] [Hx H]]; subst x.
    rewrite Fset.mem_Union in H.
    destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    exists (variables_t (FVR OQ OP OSymbol) (linearize_t M n_q t)); split.
    * rewrite in_map_iff; exists t; split; trivial.
    * assert (IHt := variables_t_linearize_t M n_q t).
      rewrite Fset.equal_spec in IHt; rewrite IHt.
      rewrite Fset.mem_map.
      exists (Vrbl q n); split; trivial.
- rewrite linearize_f_unfold, free_variables_f_unfold, Fset.mem_union, IHf1, IHf2.
  + rewrite (free_variables_f_unfold _ _ _ (Conj _ _ _)), <- Fset.mem_union; apply sym_eq.
    clear IHf1 IHf2; revert x; rewrite <- Fset.equal_spec; apply Fset.map_union.
  + refine (N.le_lt_trans _ _ _ _ HM).
    rewrite (max_f_unfold (Conj _ _ _)).
    apply N.le_max_r.
  + intros q1 q2 Hq1 Hq2; apply Hn_q.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq1, Bool.orb_true_r; apply refl_equal.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq2, Bool.orb_true_r; apply refl_equal.
  + refine (N.le_lt_trans _ _ _ _ HM).
    rewrite (max_f_unfold (Conj _ _ _)).
    apply N.le_max_l.
  + intros q1 q2 Hq1 Hq2; apply Hn_q.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq1; apply refl_equal.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq2; apply refl_equal.
- apply IHf; assumption.
- destruct v as [qv nv].
  rewrite linearize_f_unfold, free_variables_f_unfold.
  assert (HM' : (max_f f < M)%N).
  {
    refine (N.le_lt_trans _ _ _ _ HM).
    rewrite (max_f_unfold (Quant _ _ _)).
    apply N.le_max_r.
  }
  rewrite Fset.mem_filter, (IHf HM'), eq_bool_iff, Bool.Bool.andb_true_iff, 2 Fset.mem_map; 
    [split | ].
  + intros [[[q1 n1] [Hx H1]] H]; subst x.
    exists (Vrbl q1 n1); split; [apply refl_equal | ].
    rewrite free_variables_f_unfold, Fset.mem_filter, H1, Bool.Bool.andb_true_l.
    case_eq (Oset.eq_bool (OVR OQ OP OSymbol) (Vrbl qv nv) (Vrbl q1 n1)); 
      [ | intros _; apply refl_equal].
    rewrite Oset.eq_bool_true_iff; intro H2; injection H2; clear H2; do 2 intro; subst.
    unfold Oset.eq_bool in H; rewrite Oset.compare_eq_refl in H.
    discriminate H.
  + intros [[q1 n1] [Hx H1]]; subst x.
    rewrite free_variables_f_unfold, Fset.mem_filter, Bool.Bool.andb_true_iff in H1.
    destruct H1 as [H1 H2]; split.
    exists (Vrbl q1 n1); split; trivial.
    case_eq (Oset.eq_bool (OVR OQ OP OSymbol)
                          (Vrbl (linearize_q M n_q qv) (M * n_q qv + nv))
                          (Vrbl (linearize_q M n_q q1) (M * n_q q1 + n1)));
      [ | intros _; apply refl_equal].
    rewrite Oset.eq_bool_true_iff; intro H3; injection H3; clear H3.
    intros Hn Hq.
    assert (Hnv : (nv < M)%N).
    {
      refine (N.le_lt_trans _ _ _ _ HM).
      rewrite (max_f_unfold (Quant _ _ _)).
      refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
      apply N.le_max_r.
    }
    assert (Hn1 : (n1 < M) %N).
    {
      refine (N.le_lt_trans _ _ _ _ HM').
      apply (deep_variables_f_max_f n1 q1).
      assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
      assert (Aux2 := variables_f_deep_variables_f f).
      rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1; assumption.
    }
    destruct (N.div_mod_unique M (n_q qv) (n_q q1) nv n1 Hnv Hn1 Hn) as [H3 H4]; subst n1.
    assert (Aux : qv = q1).
    {
      refine (Hn_q _ _ _ _ H3).
      - rewrite all_queries_f_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; left.
        apply q_in_all_queries_q.
      - rewrite all_queries_f_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; right.
        apply (deep_variables_f_all_queries_f nv q1).
        assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
        assert (Aux2 := variables_f_deep_variables_f f).
        rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1; assumption.
    } 
    subst q1; unfold Oset.eq_bool in H2; rewrite Oset.compare_eq_refl in H2.
    discriminate H2.
  + intros q1 q2 Hq1 Hq2; apply Hn_q.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq1, Bool.orb_true_r; apply refl_equal.
    * rewrite all_queries_f_unfold, Fset.mem_union, Hq2, Bool.orb_true_r; apply refl_equal.
Qed.

Lemma free_variables_f_linearize_f_alt :
  forall v M n_q f, 
    (max_f f < M)%N ->
    (forall q1 q2 : query,
       q1 inS all_queries_f f ->
       q2 inS all_queries_f f -> n_q q1 = n_q q2 -> q1 = q2) ->
    v inS (free_variables_f OQ OP OSymbol (linearize_f M n_q f)) ->
    exists q, exists m, 
                (Vrbl q m) inS (free_variables_f OQ OP OSymbol f) /\ 
                v = Vrbl (linearize_q M n_q q) (M * n_q q + m).
Proof.
intros v M n_q f HM Hn_q H.
assert (Aux := free_variables_f_linearize_f n_q f HM Hn_q).
rewrite Fset.equal_spec in Aux; rewrite Aux, Fset.mem_map in H.
destruct H as [[q m] [H1 H2]].
exists q; exists m; split; subst v; trivial.
Qed.

Lemma find_linearize_q_map_linearize_q :
  forall (A : Type) (i : _ -> A) q m M n_q f, 
    (max_f f < M)%N ->
    (forall q1 q2, q1 inS all_queries_f f -> q2 inS all_queries_f f -> 
                   n_q q1 = n_q q2 -> q1 = q2) -> 
       Vrbl q m inS free_variables_f OQ OP OSymbol f ->
       Oset.find 
         (OVR OQ OP OSymbol)
         (Vrbl (linearize_q M n_q q) (M * n_q q + m))
         (map
            (fun v : variable query =>
               match v with
                 | Vrbl q0 m0 =>
                   (Vrbl (linearize_q M n_q q0) (M * n_q q0 + m0), i v)
               end) ({{{free_variables_f OQ OP OSymbol f}}})) = Some (i (Vrbl q m)).
Proof.
intros A i q m M n_q f HM Hn_q H.
case_eq (Oset.find (OVR OQ OP OSymbol) (Vrbl (linearize_q M n_q q) (M * n_q q + m))
                   (map
                      (fun v : variable query =>
                         match v with
                           | Vrbl q0 m0 => (Vrbl (linearize_q M n_q q0) (M * n_q q0 + m0), i v)
                         end) ({{{free_variables_f OQ OP OSymbol f}}}))).
- intros t Ht.
  assert (Kt := Oset.find_some _ _ _ Ht).
  rewrite in_map_iff in Kt.
  destruct Kt as [[q1 n1] [K1 K2]].
  injection K1; clear K1; intros J1 J2 J3; rewrite <- J1.
  assert (Hn1 : (n1 < M)%N).
  {
    refine (N.le_lt_trans _ _ _ _ HM).
    apply (deep_variables_f_max_f n1 q1).
    assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
    assert (Aux2 := variables_f_deep_variables_f f).
    rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1.
    rewrite <- (Oset.mem_bool_true_iff (OVR OQ OP OSymbol)), <- Fset.mem_elements in K2.
    apply K2.
  }
  assert (Hm : (m < M) %N).
  {
    refine (N.le_lt_trans _ _ _ _ HM).
    apply (deep_variables_f_max_f m q).
    assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
    assert (Aux2 := variables_f_deep_variables_f f).
    rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1; assumption.
  }
  destruct (N.div_mod_unique M (n_q q1) (n_q q) n1 m Hn1 Hm J2) as [J4 J5]; subst n1.
  assert (Aux : q1 = q).
  {
    refine (Hn_q _ _ _ _ J4).
    - apply (deep_variables_f_all_queries_f m q1).
      assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
      assert (Aux2 := variables_f_deep_variables_f f).
      rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1.
      rewrite <- (Oset.mem_bool_true_iff (OVR OQ OP OSymbol)), <- Fset.mem_elements in K2.
      apply K2.
    - apply deep_variables_f_all_queries_f with m.
      assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
      assert (Aux2 := variables_f_deep_variables_f f).
      rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1; assumption.
  } 
  subst q1; apply refl_equal.
- intro Ht; assert (Kt := Oset.find_none _ _ _ Ht (i (Vrbl q m))).
  apply False_rec; apply Kt.
  rewrite in_map_iff.
  exists (Vrbl q m); split; trivial.
  rewrite <- (Oset.mem_bool_true_iff (OVR OQ OP OSymbol)), <- Fset.mem_elements.
  assumption.
Qed.

Lemma linearize_q_div_mod_unique :
  forall M n_q f,
    (max_f f < M)%N ->
    (forall q1 q2 : query,
       q1 inS all_queries_f f ->
       q2 inS all_queries_f f -> n_q q1 = n_q q2 -> q1 = q2) ->
    forall q1 m1 q2 m2, Vrbl q1 m1 inS deep_variables_f f -> Vrbl q2 m2 inS deep_variables_f f ->
      (M * n_q q1 + m1 = M * n_q q2 + m2)%N -> (m1 = m2 /\ q1 = q2).
Proof.
intros M n_q f HM Hn_q q1 m1 q2 m2 Hq1 Hq2 H.
assert (Hm1 : (m1 < M)%N).
{
  refine (N.le_lt_trans _ _ _ _ HM).
  apply (deep_variables_f_max_f m1 q1); assumption.
}
assert (Hm2 : (m2 < M) %N).
{
  refine (N.le_lt_trans _ _ _ _ HM).
  apply (deep_variables_f_max_f m2 q2); assumption.
}
destruct (N.div_mod_unique M (n_q q1) (n_q q2) m1 m2 Hm1 Hm2 H) as [J4 J5]; 
  split; [assumption | ].
refine (Hn_q _ _ _ _ J4).
- apply (deep_variables_f_all_queries_f m1 q1); assumption.
- apply (deep_variables_f_all_queries_f m2 q2); assumption.
Qed.

Lemma linearize_q_is_ok_etc :
  forall I n,
  (forall Q, 
     query_size Q <= n -> 
     forall M n_q, 
       (max_q Q < M)%N ->
       (forall q1 q2, q1 inS all_queries_q Q -> q2 inS all_queries_q Q -> 
                      n_q q1 = n_q q2 -> q1 = q2) ->
       eval_query I (linearize_q M n_q Q) =SE= eval_query I Q)  /\
  (forall f, 
     tree_size (tree_of_formula f) <= n ->
     forall M n_q,
       (max_f f < M)%N ->
       (forall q1 q2, q1 inS all_queries_f f -> q2 inS all_queries_f f -> 
                      n_q q1 = n_q q2 -> q1 = q2) -> 
       forall ivar, 
         interp_formula (fun q => Feset.elements _ (eval_query I q)) ivar f =
         interp_formula 
           (fun q => Feset.elements _ (eval_query I q)) 
           (fun x => match Oset.find 
                             (OVR OQ OP OSymbol) x
                             (List.map 
                                (fun v => 
                                   match v with 
                                     | Vrbl q m => 
                                       (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + m)%N,
                                        ivar v)
                                   end) 
                                (Fset.elements _ (free_variables_f OQ OP OSymbol f))) with
                       | Some t => t
                       | _ => empty_ivar (fun q => default_tuple T (sort q)) x 
                     end) (linearize_f M n_q f)).
Proof.
intros I; induction n as [ | n]; split.
- intros Q Hn; destruct Q; inversion Hn.
- intros f Hn; destruct f; inversion Hn.
- intros Q Hn M n_q HM Hn_q; destruct Q.
  + rewrite Feset.equal_spec; intro; apply refl_equal.
  + rewrite Feset.equal_spec; intro; apply refl_equal.
  + rewrite Feset.equal_spec; intro; apply refl_equal.
  + rewrite linearize_q_unfold, 2 (eval_query_unfold _ (Query_Sigma _ _)).
    rewrite Feset.equal_spec; intro t.
    do 2 (rewrite Feset.filter_spec; [ | intros; apply eval_f_eq; assumption]).
    assert (HM' : (max_f f < M)%N).
    {
      refine (N.le_lt_trans _ _ _ _ HM).
      rewrite max_q_unfold; apply N.le_max_l.
    }
    assert (Hn_q' : forall q1 q2 : query,
                      q1 inS all_queries_f f ->
                      q2 inS all_queries_f f -> n_q q1 = n_q q2 -> q1 = q2).
    {
      intros q1 q2 Hq1 Hq2.
      apply Hn_q.
      + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq1, Bool.orb_true_r.
        apply refl_equal.
      + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq2, Bool.orb_true_r.
        apply refl_equal.
    }
    apply f_equal2.
    * revert t; rewrite <- Feset.equal_spec.
      {
        apply (proj1 IHn).
        - apply (query_size_sigma_q Hn).
        - rewrite max_q_unfold in HM.
          refine (N.le_lt_trans _ _ _ _ HM).
          apply N.le_max_r.
        - intros q1 q2 Hq1 Hq2.
          apply Hn_q.
          + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq1, 2 Bool.orb_true_r.
            apply refl_equal.
          + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq2, 2 Bool.orb_true_r.
            apply refl_equal.
      }
    * {
        unfold eval_f.
        rewrite ((proj2 IHn) f (query_size_sigma_f Hn) M n_q).
        - apply (Formula.interp_formula_eq Bool2 (OTuple T)).
          + apply interp_term_eq_rec.
          + intros u Hu; apply interp_term_eq.
            intros x Hx.
            rewrite Fset.subset_spec in Hu.
            assert (Kx := Hu _ Hx).
            destruct (free_variables_f_linearize_f_alt _ _ _ HM' Hn_q' Kx) as [q1 [n1 [H1 H2]]];
              subst x.
            rewrite find_linearize_q_map_linearize_q; trivial.
            apply Oeset.compare_eq_refl.
        - assumption.
        - assumption.
      } 
  + rewrite linearize_q_unfold, 2 (eval_query_unfold _ (Query_Pi _ _)).
    cbv zeta.
    apply Feset.map_eq_s.
    * intros t1 t2 _ Ht; apply mk_tuple_eq; [apply Fset.equal_refl | ].
      intros a Ha _; apply tuple_eq_dot; assumption.
    * {
        apply (proj1 IHn).
        - apply (query_size_pi_q Hn).
        - rewrite max_q_unfold in HM; apply HM.
        - intros q1 q2 Hq1 Hq2.
          apply Hn_q.
          + rewrite all_queries_q_unfold, Fset.add_spec, Hq1, Bool.orb_true_r.
            apply refl_equal.
          + rewrite all_queries_q_unfold, Fset.add_spec, Hq2, Bool.orb_true_r.
            apply refl_equal.
      }
  + assert (IHQ1 : eval_query I (linearize_q M n_q Q1) =SE= eval_query I Q1).
    {
      apply (proj1 IHn).
      - apply (query_size_natural_join_1 Hn).
      - rewrite max_q_unfold in HM; refine (N.le_lt_trans _ _ _ _ HM).
        apply N.le_max_l.
      - intros q1 q2 Hq1 Hq2.
        apply Hn_q.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq1, Bool.orb_true_r.
          apply refl_equal.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq2, Bool.orb_true_r.
          apply refl_equal.
    }
    assert (IHQ2 :  eval_query I (linearize_q M n_q Q2) =SE= eval_query I Q2).
    {
      apply (proj1 IHn).
      - apply (query_size_natural_join_2 Hn).
      - rewrite max_q_unfold in HM; refine (N.le_lt_trans _ _ _ _ HM).
        apply N.le_max_r.
      - intros q1 q2 Hq1 Hq2.
        apply Hn_q.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq1, 2 Bool.orb_true_r.
          apply refl_equal.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq2, 2 Bool.orb_true_r.
          apply refl_equal.
    }
    rewrite linearize_q_unfold.
    rewrite Feset.equal_spec; intro t; rewrite eq_bool_iff, 2 mem_NaturalJoin; split.
    * intros [t1 [t2 [Ht1 [Ht2 [H Ht]]]]].
      rewrite (Feset.mem_eq_2 _ _ _ IHQ1) in Ht1.
      rewrite (Feset.mem_eq_2 _ _ _ IHQ2) in Ht2.
      exists t1; exists t2; repeat split; assumption.
    * intros [t1 [t2 [Ht1 [Ht2 [H Ht]]]]].
      rewrite <- (Feset.mem_eq_2 _ _ _ IHQ1) in Ht1.
      rewrite <- (Feset.mem_eq_2 _ _ _ IHQ2) in Ht2.
      exists t1; exists t2; repeat split; assumption.
  + rewrite linearize_q_unfold.
    assert (IHQ : eval_query I (linearize_q M n_q Q) =SE= eval_query I Q).
    {
      apply (proj1 IHn).
      - apply (query_size_rename_q Hn).
      - rewrite max_q_unfold in HM; apply HM.
      - intros q1 q2 Hq1 Hq2.
        apply Hn_q.
        + rewrite all_queries_q_unfold, Fset.add_spec, Hq1, Bool.orb_true_r.
          apply refl_equal.
        + rewrite all_queries_q_unfold, Fset.add_spec, Hq2, Bool.orb_true_r.
          apply refl_equal.
    } 
    apply Feset.map_eq_s; [ | assumption].
    intros t1 t2 _ Ht; apply rename_tuple_eq_2; assumption.
  + rewrite linearize_q_unfold.
    assert (IHQ1 : eval_query I (linearize_q M n_q Q1) =SE= eval_query I Q1).
    {
      apply (proj1 IHn).
      - apply (query_size_natural_join_1 Hn).
      - rewrite max_q_unfold in HM; refine (N.le_lt_trans _ _ _ _ HM).
        apply N.le_max_l.
      - intros q1 q2 Hq1 Hq2.
        apply Hn_q.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq1, Bool.orb_true_r.
          apply refl_equal.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq2, Bool.orb_true_r.
          apply refl_equal.
    }
    assert (IHQ2 : eval_query I (linearize_q M n_q Q2) =SE= eval_query I Q2).
    {
      apply (proj1 IHn).
      - apply (query_size_natural_join_2 Hn).
      - rewrite max_q_unfold in HM; refine (N.le_lt_trans _ _ _ _ HM).
        apply N.le_max_r.
      - intros q1 q2 Hq1 Hq2.
        apply Hn_q.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq1, 2 Bool.orb_true_r.
          apply refl_equal.
        + rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, Hq2, 2 Bool.orb_true_r.
          apply refl_equal.
    }
    rewrite Feset.equal_spec in IHQ1, IHQ2.
    rewrite Feset.equal_spec; intro t.
    rewrite 2 (eval_query_unfold _ (Query_Set _ _ _)).
    rewrite (Fset.equal_eq_1 _ _ _ _ (sort_linearize_q _ _ _)).
    rewrite (Fset.equal_eq_2 _ _ _ _ (sort_linearize_q _ _ _)).
    case (sort Q1 =S?= sort Q2); [ | apply refl_equal].
    destruct s; simpl.
    * simpl; rewrite 2 Feset.mem_union, IHQ1, IHQ2; apply refl_equal.
    * rewrite 2 Feset.mem_union, IHQ1, IHQ2; apply refl_equal.
    * simpl; rewrite 2 Feset.mem_inter, IHQ1, IHQ2; apply refl_equal.
    * rewrite 2 Feset.mem_diff, IHQ1, IHQ2; apply refl_equal.
- intros f Hn M n_q HM Hn_q i; destruct f; trivial.
  + rewrite linearize_f_unfold; unfold eval_f, interp_formula.
    apply f_equal; rewrite map_map, <- map_eq.
    intros u _Hu.
    destruct u as [[q m] | f k]; [apply refl_equal | ].
    destruct f as [c | a]; [destruct k; trivial | ].
    destruct k as [ | t1 k]; [apply refl_equal | ].
    destruct k as [ | t2 k]; [ | destruct t1 as [[q m] | ]; apply refl_equal].
    destruct t1 as [[q m] | ]; [ | apply refl_equal].
    rewrite linearize_t_unfold; unfold interp_term.
    rewrite 2 map_unfold, linearize_t_unfold; apply tuple_eq_dot.
    rewrite find_linearize_q_map_linearize_q; trivial.
    * apply Oeset.compare_eq_refl.
    * rewrite free_variables_f_unfold, Fset.mem_Union.
      exists (variables_t (FVR OQ OP OSymbol) (Term (Dot a) (Var (Vrbl q m) :: nil))).
      {
        split.
        - rewrite in_map_iff.
          exists (Term (Dot a) (Var (Vrbl q m) :: nil)); split; trivial.
        - rewrite variables_t_unfold, 2 map_unfold; unfold Fset.Union.
          rewrite Fset.mem_union, variables_t_unfold, Fset.singleton_spec.
          rewrite Bool.Bool.orb_true_iff; left.
          rewrite Oset.eq_bool_true_iff; apply refl_equal.
      }
  + rewrite linearize_f_unfold, 2 (interp_formula_unfold _ _ _ _ _ _ _ _ (Conj _ _ _)).
    apply f_equal2.
    * assert (IH1 := proj2 IHn f1).
      {
        assert (HM' : (max_f f1 < M)%N).
        {
          refine (N.le_lt_trans _ _ _ _ HM).
          rewrite (max_f_unfold (Conj _ _ _)); apply N.le_max_l.
        }
        assert (Hn_q' : forall q1 q2 : query,
                          q1 inS all_queries_f f1 ->
                          q2 inS all_queries_f f1 -> n_q q1 = n_q q2 -> q1 = q2).
        {
          intros q1 q2 Hq1 Hq2; apply Hn_q.
          - rewrite all_queries_f_unfold, Fset.mem_union, Hq1; apply refl_equal.
          - rewrite all_queries_f_unfold, Fset.mem_union, Hq2; apply refl_equal.
        }
        rewrite IH1 with M n_q i.
        - apply (Formula.interp_formula_eq Bool2 (OTuple T)).
          + apply interp_term_eq_rec.
          + intros u Hu; apply interp_term_eq.
            intros x Hx.
            rewrite Fset.subset_spec in Hu.
            assert (Kx := Hu _ Hx).
            destruct (free_variables_f_linearize_f_alt _ _ _ HM' Hn_q' Kx) as [q1 [n1 [H1 H2]]];
              subst x.
            rewrite 2 find_linearize_q_map_linearize_q; trivial.
            * apply Oeset.compare_eq_refl.
            * rewrite free_variables_f_unfold, Fset.mem_union, H1.
              apply refl_equal.
        - apply (formula_size_conj_1 Hn).
        - assumption.
        - assumption.
      } 
    * assert (IH2 := proj2 IHn f2).
      {
        assert (HM' : (max_f f2 < M)%N).
        {
          refine (N.le_lt_trans _ _ _ _ HM).
          rewrite (max_f_unfold (Conj _ _ _)); apply N.le_max_r.
        }
        assert (Hn_q' : forall q1 q2 : query,
                          q1 inS all_queries_f f2 ->
                          q2 inS all_queries_f f2 -> n_q q1 = n_q q2 -> q1 = q2).
        {
          intros q1 q2 Hq1 Hq2; apply Hn_q.
          - rewrite all_queries_f_unfold, Fset.mem_union, Hq1, Bool.orb_true_r; apply refl_equal.
          - rewrite all_queries_f_unfold, Fset.mem_union, Hq2, Bool.orb_true_r; apply refl_equal.
        }
        rewrite IH2 with M n_q i.
        - apply (Formula.interp_formula_eq Bool2 (OTuple T)).
          + apply interp_term_eq_rec.
          + intros u Hu; apply interp_term_eq.
            intros x Hx.
            rewrite Fset.subset_spec in Hu.
            assert (Kx := Hu _ Hx).
            destruct (free_variables_f_linearize_f_alt _ _ _ HM' Hn_q' Kx) as [q1 [n1 [H1 H2]]];
              subst x.
            rewrite 2 find_linearize_q_map_linearize_q; trivial.
            * apply Oeset.compare_eq_refl.
            * rewrite free_variables_f_unfold, Fset.mem_union, H1, Bool.orb_true_r.
              apply refl_equal.
        - apply (formula_size_conj_2 Hn).
        - assumption.
        - assumption.
      } 
  + rewrite linearize_f_unfold, 2 (interp_formula_unfold _ _ _ _ _ _ _ _ (Not _)); 
      apply f_equal.
    assert (IHf := proj2 IHn f).
    unfold interp_formula in IHf; apply IHf; trivial.
    apply (formula_size_not_f Hn).
  + destruct v as [qv nv].
    rewrite linearize_f_unfold, 2 (interp_formula_unfold _ _ _ _ _ _ _ _ (Quant _ _ _)).
    apply (interp_quant_eq Bool2 (OTuple T)).
    * apply Oeset.nb_occ_permut.
      intro x; rewrite <- 2 Feset.nb_occ_unfold, 2 Feset.nb_occ_alt.
      apply if_eq; [ | intros; apply refl_equal | intros; apply refl_equal].
      apply sym_eq; apply Feset.mem_eq_2.
      {
        apply (proj1 IHn).
        - refine (le_trans _ _ _ _ (formula_size_quant_v Hn)).
          rewrite tree_of_var_unfold, tree_size_unfold; simpl.
          apply le_S; apply le_plus_l.
        - refine (N.le_lt_trans _ _ _ _ HM).
          rewrite max_f_unfold.
          refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
          apply N.le_max_l.
        - intros q1 q2 Hq1 Hq2; apply Hn_q.
          + rewrite all_queries_f_unfold, Fset.mem_union, Hq1; apply refl_equal.
          + rewrite all_queries_f_unfold, Fset.mem_union, Hq2; apply refl_equal.
      } 
    * intros t1 t2 Ht1 Ht.
      rewrite <- Feset.mem_elements in Ht1.
      assert (HM' : (max_f f < M)%N).
      {
        refine (N.le_lt_trans _ _ _ _ HM).
        rewrite (max_f_unfold (Quant _ _ _)); apply N.le_max_r.
      }
      assert (Hn_q' : forall q1 q2 : query,
                        q1 inS all_queries_f f ->
                        q2 inS all_queries_f f -> n_q q1 = n_q q2 -> q1 = q2).
      {
        intros q1 q2 Hq1 Hq2; apply Hn_q.
        - rewrite all_queries_f_unfold, Fset.mem_union, Hq1, Bool.orb_true_r; 
          apply refl_equal.
        - rewrite all_queries_f_unfold, Fset.mem_union, Hq2, Bool.orb_true_r; 
          apply refl_equal.
      }
      assert (IHf := proj2 IHn f).
      {
        rewrite (IHf (formula_size_quant_f Hn) M n_q).
        - apply (Formula.interp_formula_eq Bool2 (OTuple T)).
          + apply interp_term_eq_rec.
          + intros u Hu; apply interp_term_eq.
            intros x Hx.
            rewrite Fset.subset_spec in Hu.
            assert (Kx := Hu _ Hx).
            destruct (free_variables_f_linearize_f_alt _ _ _ HM' Hn_q' Kx) as [q1 [n1 [H1 H2]]];
              subst x.
            rewrite find_linearize_q_map_linearize_q; trivial.
            unfold Formula.ivar_xt.
            case_eq (Oset.compare (OVR OQ OP OSymbol) (Vrbl qv nv) (Vrbl q1 n1)).
            * intro H; rewrite Oset.compare_eq_iff in H.
              injection H; clear H; intros K1 K2; subst q1 n1.
              rewrite Oset.compare_eq_refl; assumption.
            * intro H.
              {
                case_eq (Oset.compare (OVR OQ OP OSymbol)
                                      (Vrbl (linearize_q M n_q qv) (M * n_q qv + nv))
                                      (Vrbl (linearize_q M n_q q1) (M * n_q q1 + n1))).
                - intro K; rewrite Oset.compare_eq_iff in K.
                  injection K; clear K; intros K1 K2.
                  destruct (linearize_q_div_mod_unique 
                                   n_q (Quant q (Vrbl qv nv) f) HM Hn_q qv nv q1 n1) as [J1 J2].
                  + rewrite deep_variables_f_unfold, Fset.add_spec, Bool.Bool.orb_true_iff; left.
                    rewrite Oset.eq_bool_true_iff; apply refl_equal.
                  + rewrite deep_variables_f_unfold, Fset.add_spec, Bool.Bool.orb_true_iff; right.
                    rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
                    assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
                    assert (Aux2 := variables_f_deep_variables_f f).
                    rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1; assumption.
                  + assumption.
                  + subst nv qv; rewrite Oset.compare_eq_refl in H; discriminate H.
                - intro K; rewrite find_linearize_q_map_linearize_q; trivial.
                  + apply Oeset.compare_eq_refl.
                  + rewrite free_variables_f_unfold, Fset.mem_filter, H1.
                    unfold Oset.eq_bool; rewrite H; apply refl_equal.
                - intro K; rewrite find_linearize_q_map_linearize_q; trivial.
                  + apply Oeset.compare_eq_refl.
                  + rewrite free_variables_f_unfold, Fset.mem_filter, H1.
                    unfold Oset.eq_bool; rewrite H; apply refl_equal.
              }
            * intro H.
              {
                case_eq (Oset.compare (OVR OQ OP OSymbol)
                                      (Vrbl (linearize_q M n_q qv) (M * n_q qv + nv))
                                      (Vrbl (linearize_q M n_q q1) (M * n_q q1 + n1))).
                - intro K; rewrite Oset.compare_eq_iff in K.
                  injection K; clear K; intros K1 K2.
                  destruct (linearize_q_div_mod_unique 
                                   n_q (Quant q (Vrbl qv nv) f) HM Hn_q qv nv q1 n1) as [J1 J2].
                  + rewrite deep_variables_f_unfold, Fset.add_spec, Bool.Bool.orb_true_iff; left.
                    rewrite Oset.eq_bool_true_iff; apply refl_equal.
                  + rewrite deep_variables_f_unfold, Fset.add_spec, Bool.Bool.orb_true_iff; right.
                    rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
                    assert (Aux1 := free_variables_f_variables_f OQ OP OSymbol f).
                    assert (Aux2 := variables_f_deep_variables_f f).
                    rewrite Fset.subset_spec in Aux1, Aux2; apply Aux2; apply Aux1; assumption.
                  + assumption.
                  + subst nv qv; rewrite Oset.compare_eq_refl in H; discriminate H.
                - intro K; rewrite find_linearize_q_map_linearize_q; trivial.
                  + apply Oeset.compare_eq_refl.
                  + rewrite free_variables_f_unfold, Fset.mem_filter, H1.
                    unfold Oset.eq_bool; rewrite H; apply refl_equal.
                - intro K; rewrite find_linearize_q_map_linearize_q; trivial.
                  + apply Oeset.compare_eq_refl.
                  + rewrite free_variables_f_unfold, Fset.mem_filter, H1.
                    unfold Oset.eq_bool; rewrite H; apply refl_equal.
              }
        - assumption.
        - assumption.
      } 
Qed.

Lemma deep_variables_q_linearize_q_etc :
  forall n,
  (forall Q, 
     query_size Q <= n -> 
     forall M n_q, 
       (max_q Q < M)%N ->
       (forall q1 q2 : query,
          q1 inS all_queries_q Q ->
          q2 inS all_queries_q Q -> n_q q1 = n_q q2 -> q1 = q2) ->
       deep_variables_q (linearize_q M n_q Q) =S=
       Fset.map _ (FVR OQ OP OSymbol) 
                (fun x => 
                   match x with 
                     | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                   end) 
                (deep_variables_q Q)) /\
  (forall f, 
     tree_size (tree_of_formula f) <= n ->
     forall M n_q, 
       (max_f f < M)%N ->
       (forall q1 q2, q1 inS all_queries_f f -> q2 inS all_queries_f f -> 
                      n_q q1 = n_q q2 -> q1 = q2) -> 
       deep_variables_f (linearize_f M n_q f) =S=
       Fset.map _ (FVR OQ OP OSymbol) 
                (fun x => 
                   match x with 
                     | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                   end) 
                (deep_variables_f f)) /\
  (forall t, 
     tree_size (tree_of_term t) <= n ->
     forall M n_q, 
       (max_t t < M)%N ->
       (forall q1 q2, q1 inS all_queries_t t -> q2 inS all_queries_t t -> 
                      n_q q1 = n_q q2 -> q1 = q2) -> 
       deep_variables_t (linearize_t M n_q t) =S=
       Fset.map _ (FVR OQ OP OSymbol) 
                (fun x => 
                   match x with 
                     | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                   end) 
                (deep_variables_t t)).
Proof.
intro n; induction n as [ | n];
[ split; 
  [ intros Q Hn; destruct Q; inversion Hn
  | split ; 
    [ intros f Hn; destruct f; inversion Hn
    | intros t Hn; destruct t; inversion Hn]]
| ].
split; [ | split].
- intros Q Hn M n_q HM Hn_q; destruct Q; rewrite linearize_q_unfold, deep_variables_q_unfold.
  + unfold Fset.map; rewrite Fset.elements_empty, map_unfold; apply Fset.equal_refl.
  + unfold Fset.map; rewrite Fset.elements_empty, map_unfold; apply Fset.equal_refl.
  + unfold Fset.map; rewrite Fset.elements_empty, map_unfold; apply Fset.equal_refl.
  + rewrite (deep_variables_q_unfold (Query_Sigma _ _)).
    rewrite Fset.equal_spec; intro x.
    rewrite Fset.mem_union.
    assert (IHf := proj1 (proj2 IHn) f (query_size_sigma_f Hn) M n_q).
    assert (IHQ := proj1 IHn Q (query_size_sigma_q Hn) M n_q).
    rewrite Fset.equal_spec in IHf, IHQ.
    rewrite IHf, IHQ, <- Fset.mem_union.
    * apply sym_eq; revert x; rewrite <- Fset.equal_spec.
      apply Fset.map_union.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_q_unfold (Query_Sigma _ _)).
      apply N.le_max_r.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff;
      do 2 right; assumption.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_q_unfold (Query_Sigma _ _)).
      apply N.le_max_l.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff;
      right; left; assumption.
  + apply (proj1 IHn Q (query_size_pi_q Hn)).
    * rewrite (max_q_unfold (Query_Pi _ _)) in HM; apply HM.
    * do 4 intro; apply Hn_q; rewrite all_queries_q_unfold, Fset.add_spec, Bool.Bool.orb_true_iff;
      right; assumption.
  + rewrite (deep_variables_q_unfold (Query_NaturalJoin _ _)).
    rewrite Fset.equal_spec; intro x.
    rewrite Fset.mem_union.
    assert (IHQ1 := proj1 IHn Q1 (query_size_natural_join_1 Hn) M n_q).
    assert (IHQ2 := proj1 IHn Q2 (query_size_natural_join_2 Hn) M n_q).
    rewrite Fset.equal_spec in IHQ1, IHQ2.
    rewrite IHQ1, IHQ2, <- Fset.mem_union.
    * apply sym_eq; revert x; rewrite <- Fset.equal_spec.
      apply Fset.map_union.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_q_unfold (Query_NaturalJoin _ _)).
      apply N.le_max_r.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff;
      do 2 right; assumption.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_q_unfold (Query_NaturalJoin _ _)).
      apply N.le_max_l.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff;
      right; left; assumption.
  + rewrite (deep_variables_q_unfold (Query_Rename _ _)).
    apply (proj1 IHn Q (query_size_rename_q Hn)).
    * rewrite (max_q_unfold (Query_Rename _ _)) in HM; apply HM.
    * do 4 intro; apply Hn_q; rewrite all_queries_q_unfold, Fset.add_spec, Bool.Bool.orb_true_iff;
      right; assumption.
  + rewrite (deep_variables_q_unfold (Query_Set _ _ _)).
    rewrite Fset.equal_spec; intro x.
    rewrite Fset.mem_union.
    assert (IHQ1 := proj1 IHn Q1 (query_size_set_1 Hn) M n_q).
    assert (IHQ2 := proj1 IHn Q2 (query_size_set_2 Hn) M n_q).
    rewrite Fset.equal_spec in IHQ1, IHQ2.
    rewrite IHQ1, IHQ2, <- Fset.mem_union.
    * apply sym_eq; revert x; rewrite <- Fset.equal_spec.
      apply Fset.map_union.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_q_unfold (Query_Set _ _ _)).
      apply N.le_max_r.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff;
      do 2 right; assumption.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_q_unfold (Query_Set _ _ _)).
      apply N.le_max_l.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_q_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff;
      right; left; assumption.
- intros f Hn M n_q HM Hn_q; destruct f; rewrite linearize_f_unfold, deep_variables_f_unfold.
  + unfold Fset.map; rewrite Fset.elements_empty; apply Fset.equal_refl.
  + assert (IHt : forall t, 
                    In t l -> 
                    deep_variables_t (linearize_t M n_q t) =S=
                    Fset.map (FVR OQ OP OSymbol) (FVR OQ OP OSymbol)
                             (fun x : variable query =>
                                match x with
                                  | Vrbl q n => Vrbl (linearize_q M n_q q) (M * n_q q + n)
                                end) (deep_variables_t t)).
    {
      intros t Ht; apply (proj2 (proj2 IHn)).
      - apply (formula_size_atom_t _ Hn Ht).
      - refine (N.le_lt_trans _ _ _ _ HM).
        rewrite max_f_unfold; apply le_maxN.
        rewrite in_map_iff; exists t; split; trivial.
      - do 4 intro; apply Hn_q; rewrite all_queries_f_unfold, Fset.mem_Union;
        exists (all_queries_t t); split; trivial; rewrite in_map_iff; exists t; split; trivial.
    }
    rewrite deep_variables_f_unfold, Fset.equal_spec; intro x.
    rewrite eq_bool_iff, Fset.mem_Union, Fset.mem_map; split.
    * intros [s [Hs H]]; rewrite map_map, in_map_iff in Hs.
      destruct Hs as [t [Hs Ht]]; subst s.
      assert (IH := IHt t Ht).
      rewrite Fset.equal_spec in IH; rewrite IH, Fset.mem_map in H.
      destruct H as [[qv nv] [Hx H]]; subst x.
      exists (Vrbl qv nv); split; trivial.
      rewrite Fset.mem_Union; exists (deep_variables_t t); split; trivial.
      rewrite in_map_iff; exists t; split; trivial.
    * intros [[qv nv] [Hx H]]; subst x.
      rewrite Fset.mem_Union in H; destruct H as [s [Hs H]].
      rewrite in_map_iff in Hs; destruct Hs as [t [Hs Ht]]; subst s.
      rewrite map_map.
      exists (deep_variables_t (linearize_t M n_q t)).
      {
        split.
        - rewrite in_map_iff; exists t; split; trivial.
        - assert (IH := IHt t Ht); rewrite Fset.equal_spec in IH.
          rewrite IH, Fset.mem_map.
          exists (Vrbl qv nv); split; trivial.
      }
  + rewrite (deep_variables_f_unfold (Conj _ _ _)).
    rewrite Fset.equal_spec; intro x.
    rewrite Fset.mem_union.
    assert (IHf1 := proj1 (proj2 IHn) f1 (formula_size_conj_1 Hn) M n_q).
    assert (IHf2 := proj1 (proj2 IHn) f2 (formula_size_conj_2 Hn) M n_q).
    rewrite Fset.equal_spec in IHf1, IHf2.
    rewrite IHf1, IHf2, <- Fset.mem_union.
    * apply sym_eq; revert x; rewrite <- Fset.equal_spec.
      apply Fset.map_union.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_f_unfold (Conj _ _ _)).
      apply N.le_max_r.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_f_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; right; assumption.
    * refine (N.le_lt_trans _ _ _ _ HM); rewrite (max_f_unfold (Conj _ _ _)).
      apply N.le_max_l.
    * do 4 intro; apply Hn_q; 
      rewrite all_queries_f_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; left; assumption.
  + rewrite (deep_variables_f_unfold (Not _)).
    apply (proj1 (proj2 IHn)).
    * apply (formula_size_not_f Hn).
    * apply HM.
    * apply Hn_q.
  + destruct v as [qv nv].
    rewrite (deep_variables_f_unfold (Quant _ _ _)).
    rewrite Fset.equal_spec; intro x.
    rewrite Fset.add_spec, Fset.mem_union, eq_bool_iff, 2 Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff.
    rewrite Fset.mem_map.
    assert (Hn' : query_size qv <= n).
    {
      refine (le_trans _ _ _ _ (formula_size_quant_v Hn)).
      simpl; apply le_S.
      apply le_plus_l.
    }
    assert (IHqv := proj1 IHn qv Hn' M n_q).
    assert (IHf := proj1 (proj2 IHn) f (formula_size_quant_f Hn) M n_q).
    rewrite Fset.equal_spec in IHqv, IHf; rewrite IHqv, IHf; [split | | | |].
    * {
        intros [Hx | [Hx | Hx]].
        - subst x; exists (Vrbl qv nv); split; trivial.
          rewrite Fset.add_spec, Bool.Bool.orb_true_iff; left.
          rewrite Oset.eq_bool_true_iff; apply refl_equal.
        - rewrite Fset.mem_map in Hx; destruct Hx as [[q1 n1] [Hx H]]; subst x.
          exists (Vrbl q1 n1); split; trivial.
          rewrite Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff; right; left; assumption.
        - rewrite Fset.mem_map in Hx; destruct Hx as [[q1 n1] [Hx H]]; subst x.
          exists (Vrbl q1 n1); split; trivial.
          rewrite Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff; do 2 right; assumption.
      }
    * intros [[q1 n1] [Hx H]]; subst x.
      rewrite Fset.add_spec, Fset.mem_union, 2 Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
      {
        destruct H as [H | [H | H]].
        - injection H; clear H; do 2 intro; subst q1 n1.
          left; trivial.
        - right; left; rewrite Fset.mem_map.
          exists (Vrbl q1 n1); split; trivial.
        - do 2 right; rewrite Fset.mem_map.
          exists (Vrbl q1 n1); split; trivial.
      } 
    * refine (N.le_lt_trans _ _ _ _ HM); simpl.
      apply N.le_max_r.
    * do 4 intro; apply Hn_q;
      rewrite all_queries_f_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; right; assumption.
    * refine (N.le_lt_trans _ _ _ _ HM); simpl.
      refine (N.le_trans _ _ _ _ (N.le_max_l _ _)).
      apply N.le_max_l.
    * do 4 intro; apply Hn_q;
      rewrite all_queries_f_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; left; assumption.
- intros t Hn M n_q HM Hn_q; destruct t; rewrite linearize_t_unfold, deep_variables_t_unfold.
  + rewrite deep_variables_t_unfold; destruct v as [qv nv].
    rewrite Fset.equal_spec; intro x.
    rewrite eq_bool_iff, Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff, Fset.mem_map.
    assert (Hn' : query_size qv <= n).
    {
      simpl in Hn.
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
      apply le_S.
      rewrite <- plus_assoc; apply le_plus_l.
    }
    assert (IHqv := proj1 IHn qv Hn' M n_q).
    rewrite Fset.equal_spec in IHqv; rewrite IHqv; [split | | ].
    * {
        intros [H | H].
        - subst x.
          exists (Vrbl qv nv); split; trivial.
          rewrite Fset.add_spec, Bool.Bool.orb_true_iff; left.
          rewrite Oset.eq_bool_true_iff; apply refl_equal.
        - rewrite Fset.mem_map in H.
          destruct H as [[q1 n1] [Hx H]]; subst x.
          exists (Vrbl q1 n1); split; trivial.
          rewrite Fset.add_spec, H, Bool.orb_true_r; apply refl_equal.
      } 
    * intros [[q1 n1] [Hx H]]; subst x.
      rewrite Fset.add_spec, Bool.Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
      {
        destruct H as [H | H].
        - injection H; clear H; do 2 intro; subst q1 n1.
          left; trivial.
        - right; rewrite Fset.mem_map.
          exists (Vrbl q1 n1); split; trivial.
      } 
    * refine (N.le_lt_trans _ _ _ _ HM).
      rewrite max_t_unfold; apply N.le_max_l.
    * do 4 intro; apply Hn_q; rewrite all_queries_t_unfold; assumption.
  + rewrite map_map, (deep_variables_t_unfold (Term _ _)), Fset.equal_spec; intro x.
    rewrite eq_bool_iff, Fset.mem_Union, Fset.mem_map.
    assert (IHl : forall t, In t l ->
                            deep_variables_t (linearize_t M n_q t) =S=
                            Fset.map (FVR OQ OP OSymbol) (FVR OQ OP OSymbol)
                                     (fun x : variable query =>
                                        match x with
                                          | Vrbl q n => Vrbl (linearize_q M n_q q) (M * n_q q + n)
                                        end) (deep_variables_t t)).
    {
      intros t Ht; apply (proj2 (proj2 IHn)).
      - apply (term_size_t _ Hn Ht).
      - refine (N.le_lt_trans _ _ _ _ HM).
        rewrite (max_t_unfold (Term _ _)).
        apply le_maxN.
        rewrite in_map_iff; exists t; split; trivial.
      - do 4 intro; apply Hn_q; rewrite all_queries_t_unfold, Fset.mem_Union;
        exists (all_queries_t t); split; trivial; rewrite in_map_iff; exists t; split; trivial.
    } 
    split.
    * intros [_s [Hs H]]; rewrite in_map_iff in Hs.
      destruct Hs as [t [Hs Ht]]; subst _s.
      assert (IHt := IHl t Ht).
      rewrite Fset.equal_spec in IHt; rewrite IHt, Fset.mem_map in H.
      destruct H as [[q1 n1] [Hx H]]; subst x.
      exists (Vrbl q1 n1); split; trivial.
      rewrite Fset.mem_Union.
      exists (deep_variables_t t); split; trivial.
      rewrite in_map_iff; exists t; split; trivial.
    * intros [[q1 n1] [Hx H]]; subst x.
      rewrite Fset.mem_Union in H.
      destruct H as [_s [Hs H]]; rewrite in_map_iff in Hs.
      destruct Hs as [t [Hs Ht]]; subst _s.
      {
        exists (deep_variables_t (linearize_t M n_q t)); split.
        - rewrite in_map_iff; exists t; split; trivial.
        - assert (IHt := IHl t Ht).
          rewrite Fset.equal_spec in IHt; rewrite IHt, Fset.mem_map.
          exists (Vrbl q1 n1); split; trivial.
      }
Qed.

Lemma deep_variables_q_linearize_q :
  forall Q M n_q, 
    (max_q Q < M)%N ->
    (forall q1 q2 : query,
       q1 inS all_queries_q Q ->
       q2 inS all_queries_q Q -> n_q q1 = n_q q2 -> q1 = q2) ->
    deep_variables_q (linearize_q M n_q Q) =S=
    Fset.map _ (FVR OQ OP OSymbol) 
             (fun x => 
                match x with 
                  | Vrbl q n => (Vrbl (linearize_q M n_q q) ((M * (n_q q)) + n)%N)
                end) 
             (deep_variables_q Q).
Proof.
intros Q N n_q HM Hn_q.
apply (proj1 (deep_variables_q_linearize_q_etc (query_size Q))); trivial.
Qed.

Lemma linearize_q_linearize :
  forall Q M n_q, 
    (max_q Q < M)%N ->
    (forall q1 q2, q1 inS all_queries_q Q -> q2 inS all_queries_q Q -> 
                   n_q q1 = n_q q2 -> q1 = q2) ->
    forall q1 m1 q2 m2, 
      Vrbl q1 m1 inS deep_variables_q (linearize_q M n_q Q) ->
      Vrbl q2 m2 inS deep_variables_q (linearize_q M n_q Q) -> m1 = m2 -> q1 = q2.
Proof.
intros Q M n_q HM Hn_q q1 m1 q2 m2 H1 H2 Hm.
assert (H := deep_variables_q_linearize_q Q _ HM Hn_q).
rewrite Fset.equal_spec in H; rewrite H, Fset.mem_map in H1, H2.
destruct H1 as [[q1' m1'] [K1 J1]].
destruct H2 as [[q2' m2'] [K2 J2]].
injection K1; injection K2; intros L1 L2 L3 L4; subst.
apply f_equal.
assert (Hm1 : (m1' < M)%N).
{
  refine (N.le_lt_trans _ _ _ _ HM).
  apply (deep_variables_q_max_q _ _ _ J1).
}
assert (Hm2 : (m2' < M) %N).
{
  refine (N.le_lt_trans _ _ _ _ HM).
  apply (deep_variables_q_max_q _ _ _ J2).
}
destruct (N.div_mod_unique M (n_q q1') (n_q q2') m1' m2' Hm1 Hm2 L3) as [J4 J5].
apply Hn_q; trivial.
-  apply (deep_variables_q_all_queries_q _ _ _ J1).
-  apply (deep_variables_q_all_queries_q _ _ _ J2).
Qed.

Fixpoint n_q_rec n l q :=
  match l with
    | nil => None
    | q1 :: lq => match Oset.compare OQ q q1 with 
                    | Eq => Some n 
                    | _ => n_q_rec (n+1)%N lq q
                  end
  end.

Lemma n_q_rec_unfold :
  forall n l q, n_q_rec n l q =
  match l with
    | nil => None
    | q1 :: lq => match Oset.compare OQ q q1 with 
                    | Eq => Some n 
                    | _ => n_q_rec (n+1)%N lq q
                  end
  end.
Proof.
intros n l q; case l; intros; apply refl_equal.
Qed.

Definition n_q Q q :=
  let lq := Fset.elements _ (all_queries_q Q) in
  match n_q_rec 0 lq q with
    | Some n => (n+1)%N
    | None => 0%N
  end.

Lemma n_q_rec_is_increasing :
  forall n l q n1, n_q_rec n l q = Some n1 -> (n <= n1)%N.
Proof.
intros n l; revert n; induction l as [ | q1 lq]; intros n q n1 H.
- discriminate H.
- rewrite n_q_rec_unfold in H.
  destruct (Oset.compare OQ q q1).
  + injection H; clear H; intro H; subst n1.
    apply N.le_refl.
  + assert (IH := IHlq _ _ _ H).
    refine (N.le_trans _ _ _ _ IH).
    apply N.le_add_r.
  + assert (IH := IHlq _ _ _ H).
    refine (N.le_trans _ _ _ _ IH).
    apply N.le_add_r.
Qed.

Lemma n_q_rec_in :
  forall n l q, In q l -> n_q_rec n l q <> None.
Proof.
intros n l; revert n; induction l as [ | q1 lq]; intros n q H.
- contradiction H.
- rewrite n_q_rec_unfold.
  case_eq (Oset.compare OQ q q1); intro Hq1.
  + discriminate.
  + apply IHlq.
    simpl in H; destruct H as [H | H]; [ | assumption].
    subst q1; rewrite Oset.compare_eq_refl in Hq1; discriminate Hq1.
  + apply IHlq.
    simpl in H; destruct H as [H | H]; [ | assumption].
    subst q1; rewrite Oset.compare_eq_refl in Hq1; discriminate Hq1.
Qed.

Lemma n_q_is_one_to_one :
  forall Q q1 q2, q1 inS all_queries_q Q -> q2 inS all_queries_q Q -> n_q Q q1 = n_q Q q2 -> 
                  q1 = q2.
Proof.
intros Q q1 q2 Hq1 Hq2 H.
unfold n_q in H.
rewrite Fset.mem_elements, Oset.mem_bool_true_iff in Hq1, Hq2.
set (l := Fset.elements _ (all_queries_q Q)) in *; clearbody l.
assert (Aux : n_q_rec 0 l q1 = n_q_rec 0 l q2).
{
  case_eq (n_q_rec 0 l q1).
  - intros n1 Hn1; case_eq (n_q_rec 0 l q2).
    + intros n2 Hn2.
      rewrite Hn1, Hn2 in H.
      apply f_equal.
      rewrite 2 (N.add_comm _ 1) in H.
      apply (Nplus_reg_l _ _ _ H).
    + intro Hn2; rewrite Hn1, Hn2 in H.
      destruct n1; discriminate H.
  -  intros Hn1; case_eq (n_q_rec 0 l q2).
     + intros n2 Hn2; rewrite Hn1, Hn2 in H.
       destruct n2; discriminate H.
     + intros _; apply refl_equal.
} 
clear H.
set (n0 := 0%N) in *.
 clearbody n0.
revert n0 Aux; induction l as [ | q l]; intros n0 H.
- contradiction Hq1.
- rewrite 2 (n_q_rec_unfold _ (_ :: _)) in H.
  case_eq (Oset.compare OQ q1 q); intro Kq1; rewrite Kq1 in H.
  + rewrite Oset.compare_eq_iff in Kq1; subst q1.
    case_eq (Oset.compare OQ q2 q); intro Kq2; rewrite Kq2 in H.
    * rewrite Oset.compare_eq_iff in Kq2; subst q2; apply refl_equal.
    * simpl in Hq2; destruct Hq2 as [Hq2 | Hq2];
      [subst q2; rewrite Oset.compare_eq_refl in Kq2; discriminate Kq2 | ].
      assert (Aux := n_q_rec_is_increasing _ _ _ (sym_eq H)).
      apply False_rec; apply (N.lt_irrefl n0).
      rewrite N.add_comm, <- N.lt_succ_r, <- N.add_1_l in Aux.
      rewrite <- N.add_lt_mono_l in Aux; apply Aux.
    * simpl in Hq2; destruct Hq2 as [Hq2 | Hq2];
      [subst q2; rewrite Oset.compare_eq_refl in Kq2; discriminate Kq2 | ].
      assert (Aux := n_q_rec_is_increasing _ _ _ (sym_eq H)).
      apply False_rec; apply (N.lt_irrefl n0).
      rewrite N.add_comm, <- N.lt_succ_r, <- N.add_1_l in Aux.
      rewrite <- N.add_lt_mono_l in Aux; apply Aux.
  + simpl in Hq1; destruct Hq1 as [Hq1 | Hq1];
    [subst q1; rewrite Oset.compare_eq_refl in Kq1; discriminate Kq1 | ].
    case_eq (Oset.compare OQ q2 q); intro Kq2; rewrite Kq2 in H.
    * rewrite Oset.compare_eq_iff in Kq2; subst q2.
      assert (Aux := n_q_rec_is_increasing _ _ _ H).
      apply False_rec; apply (N.lt_irrefl n0).
      rewrite N.add_comm, <- N.lt_succ_r, <- N.add_1_l in Aux.
      rewrite <- N.add_lt_mono_l in Aux; apply Aux.
    * simpl in Hq2; destruct Hq2 as [Hq2 | Hq2];
      [subst q2; rewrite Oset.compare_eq_refl in Kq2; discriminate Kq2 | ].
      revert H; apply IHl; trivial.
    * simpl in Hq2; destruct Hq2 as [Hq2 | Hq2];
      [subst q2; rewrite Oset.compare_eq_refl in Kq2; discriminate Kq2 | ].
      revert H; apply IHl; trivial.
  + simpl in Hq1; destruct Hq1 as [Hq1 | Hq1];
    [subst q1; rewrite Oset.compare_eq_refl in Kq1; discriminate Kq1 | ].
    case_eq (Oset.compare OQ q2 q); intro Kq2; rewrite Kq2 in H.
    * rewrite Oset.compare_eq_iff in Kq2; subst q2.
      assert (Aux := n_q_rec_is_increasing _ _ _ H).
      apply False_rec; apply (N.lt_irrefl n0).
      rewrite N.add_comm, <- N.lt_succ_r, <- N.add_1_l in Aux.
      rewrite <- N.add_lt_mono_l in Aux; apply Aux.
    * simpl in Hq2; destruct Hq2 as [Hq2 | Hq2];
      [subst q2; rewrite Oset.compare_eq_refl in Kq2; discriminate Kq2 | ].
      revert H; apply IHl; trivial.
    * simpl in Hq2; destruct Hq2 as [Hq2 | Hq2];
      [subst q2; rewrite Oset.compare_eq_refl in Kq2; discriminate Kq2 | ].
      revert H; apply IHl; trivial.
Qed.

End Linearize.

End Interp.

End Sec.

Arguments Constant {RA} v.
Arguments Dot {RA} a.
Arguments Query_Empty {RA} l.
Arguments Query_Empty_Tuple {RA}.
Arguments Query_Basename {RA} r.

End RA.

*)
