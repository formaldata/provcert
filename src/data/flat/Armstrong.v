(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)


Set Implicit Arguments.

(** printing inS? $\in_?$ #∈<SUB><I>?</I></SUB># *)
(** printing inS $\in$ #∈# *)
(** printing subS? $\subseteq_?$ #⊆<SUB><I>?</I></SUB># *)
(** printing subS $\subseteq$ #⊆# *)
(** printing unionS $\cup$ #⋃# *)
(** printing interS $\cap$ #⋂# *)
(** printing inI $\in_I$ #∈<SUB><I>I</I></SUB># *)
(** printing theta $\theta$ #θ# *)
(** printing nu1 $\nu_1$ #ν<SUB><I>1</I></SUB># *)
(** printing nu $\nu$ #ν# *)
(** printing mu $\mu$ #μ# *)
(** printing sigma $\sigma$ #σ# *)
(** printing -> #⟶# *)
(** printing <-> #⟷# *)
(** printing => #⟹# *)
(** printing (emptysetS) $\emptyset$ #Ø# *)
(** printing emptysetS $\emptyset$ #Ø# *)
(** printing {{ $\{$ #{# *)
(** printing }} $\}$ #}# *)

Require Import List ZArith FSets Orders NArith String.
Require Import BasicFacts ListPermut ListFacts OrderedSet FiniteSet FiniteCollection Tree 
        FlatData RelationalAlgebra.

Section Sec.

Import Tuple.

Hypothesis T : Tuple.Rcd.

Notation setA := (Fset.set (A T)).
Notation FTupleT := (Fecol.CSet (CTuple T)).
Notation setT := (Feset.set FTupleT).

(** Syntactic definition of functional dependencies, simply two sets of attributes. *)
Inductive fd := FD : setA -> setA -> fd.

Notation "V '-->' W" := (FD V W).

Import Tuple.

(** Semantic of functional depencies, [fd_sem ST d] holds whenever the instance (set of tuples) [I] satisfies the functional dependency [d]. *)
Definition fd_sem (I : setT) (d : fd) := 
  match d with 
    | V --> W => 
      forall t1 t2, t1 inSE I -> t2 inSE I ->
                    (forall x, x inS V -> dot T t1 x = dot T t2 x) ->
                    forall y, y inS W -> dot T t1 y = dot T t2 y
  end.

(** Orderings and finite sets of functional dependencies. *)
Definition OFAttr : Oeset.Rcd setA.
  split with (Fset.compare (A T)).
(* 1/5 *)
apply Fset.compare_eq_trans.
(* 1/4 *)
apply Fset.compare_eq_lt_trans.
(* 1/3 *)
apply Fset.compare_lt_eq_trans.
(* 1/2 *)
apply Fset.compare_lt_trans.
(* 1/1 *)
apply Fset.compare_lt_gt.
Defined.

Definition OFD : Oeset.Rcd fd.
apply Oeset.mk_R with 
  (fun d1 d2 => 
     match d1, d2 with 
       | FD fx1 fy1, FD fx2 fy2 => 
         compareAB (Oeset.compare OFAttr) (Oeset.compare OFAttr) (fx1, fy1) (fx2, fy2)
     end).
intros [fx1 fy1] [fx2 fy2] [fx3 fy3]; compareAB_tac; oeset_compare_tac.
intros [fx1 fy1] [fx2 fy2] [fx3 fy3]; compareAB_tac; oeset_compare_tac.
intros [fx1 fy1] [fx2 fy2] [fx3 fy3]; compareAB_tac; oeset_compare_tac.
intros [fx1 fy1] [fx2 fy2] [fx3 fy3]; compareAB_tac; oeset_compare_tac.
intros [fx1 fy1] [fx2 fy2]; compareAB_tac; oeset_compare_tac.
Defined.

Definition FFD := Feset.build OFD.

Section Sec1.

(** A set [F] of functional dependencies is fixed troughout this section. *)
Hypothesis F : Feset.set FFD.

(** Derivation trees for Armstrong system. All dependencies in [F] are assumed to hold, by the case [D_Ax]. *)
Inductive dtree : fd -> Type :=
  | D_Ax : forall X Y, (X --> Y) inSE F -> dtree (X --> Y)
  | D_Refl : forall X Y, Y subS X -> dtree (X --> Y)
  | D_Aug : forall X Y Z XZ YZ, XZ =S= (X unionS Z) -> YZ =S= (Y unionS Z) ->
      dtree (X --> Y) -> dtree (XZ --> YZ)
  | D_Trans : forall X Y Y' Z, Y =S= Y' -> 
      dtree (X --> Y) -> dtree (Y' --> Z) -> dtree (X --> Z).

Definition dtree_size (d : fd) (t : dtree d) : nat.
induction t.
exact 0%nat.
exact 0%nat.
exact (S IHt).
exact (S (plus IHt1 IHt2)).
Defined. 

Lemma dtree_size_unfold :
  forall (d : fd) (t : dtree d),
    dtree_size t =
    match t with
        | D_Ax _ _ _ 
        | D_Refl _ _ _ => 0%nat
        | D_Aug _ _ _ _ _ t1 => S (dtree_size t1)
        | D_Trans _ t1 t2 => S (dtree_size t1 + dtree_size t2)
    end.
Proof.
intros d t; case t; intros; apply refl_equal.
Qed.

Lemma dtree_rec3 :
  forall (P : forall (d : fd), dtree d -> Type),
    (forall X Y H1, P _ (D_Ax X Y H1)) ->
    (forall X Y H1, P _ (D_Refl X Y H1)) ->
    (forall X Y Z XZ YZ H1 H2 H3, P _ H1 -> P _ (@D_Aug X Y Z XZ YZ H2 H3 H1)) ->
    (forall X Y Y' Z H1 H2 H3, P _ H2 -> P _ H3 -> P _ (@D_Trans X Y Y' Z H1 H2 H3)) ->
    forall d (t : dtree d), (P _ t).
Proof.
intros P H_Ax H_Refl H_Aug H_Trans d t.
set (n := dtree_size t).
assert (Hn := le_refl n); unfold n at 1 in Hn; clearbody n.
revert d t Hn.
induction n as [ | n]; intros d t; rewrite dtree_size_unfold.
(* 1/2 *)
destruct t as [_X _Y H1 | _X _Y H1 | _X _Y Z Xz Yz H1 H2 H3 | _X _Y _Y' Z H1 H2 H3].
intros _; apply H_Ax.
intros _; apply H_Refl.
intro Hn; apply False_rect; inversion Hn.
intro Hn; apply False_rect; inversion Hn.
(* 1/1 *)
destruct t as [_X _Y H1 | _X _Y H1 | _X _Y Z Xz Yz H1 H2 H3 | _X _Y _Y' Z H1 H2 H3].
intros _; apply H_Ax.
intros _; apply H_Refl.
intro Hn; apply H_Aug; apply IHn.
apply le_S_n; assumption.
intro Hn; apply H_Trans; apply IHn;
apply le_S_n; refine (le_trans _ _ _ _ Hn); apply le_n_S.
apply le_plus_l.
apply le_plus_r.
Qed.

End Sec1.

(** A derivation tree is invariant under sets' equivalence. *)
Lemma set_dtree :
  forall F X Y F' X' Y', F =SE= F' -> X =S= X' -> Y =S= Y' -> 
      dtree F (X --> Y) -> dtree F' (X' --> Y').
Proof.
intros F X Y  F' X' Y' HF Hx Hy HT.
set (d := X --> Y) in *.
assert (Hd := refl_equal d); unfold d at 1 in Hd; clearbody d.
revert X Y F' X' Y' HF Hx Hy Hd.
apply (dtree_rec3 (fun d (HT : dtree F d) =>
    forall (X Y : setA) (F' : Feset.set FFD) (X' Y' : setA),
    F =SE= F' ->
    X =S= X' -> Y =S= Y' -> X --> Y = d -> dtree F' (X' --> Y'))).
- (* 1/5 *)
  intros X Y H _X _Y F' X' Y' HF Hx Hy Hd.
  injection Hd; clear Hd; do 2 intro; subst _X _Y.
  constructor 1.
  rewrite <- (Feset.mem_eq_1 FFD (X --> Y) (X' --> Y') F').
  + rewrite <- (Feset.mem_eq_2 _ _ _ HF); assumption.
  + rewrite <- Fset.compare_spec in Hx, Hy.
    simpl; rewrite Hx, Hy; apply refl_equal.
- (* 1/4 *)
  intros X Y H _X _Y F' X' Y' HF Hx Hy Hd.
  injection Hd; clear Hd; do 2 intro; subst _X _Y.
  constructor 2.
  rewrite Fset.subset_spec in H; rewrite Fset.subset_spec; intro e.
  rewrite Fset.equal_spec in Hx, Hy; rewrite <- Hx, <- Hy.
  apply H.
- (* 1/3 *)
  intros X Y Z Xz Yz H HXz HYz IH _X _Y F' X' Y' HF Hx Hy Hd.
  injection Hd; clear Hd; do 2 intro; subst _X _Y.
  constructor 3 with X Y Z.
  + rewrite Fset.equal_spec in HXz, Hx; rewrite Fset.equal_spec; intro e;
    rewrite <- Hx, HXz; apply refl_equal.
  + rewrite Fset.equal_spec in HYz, Hy; rewrite Fset.equal_spec; intro e;
    rewrite <- Hy, HYz; apply refl_equal.
  + apply IH with X Y.
    * assumption.
    * rewrite Fset.equal_spec; intro; apply refl_equal.
    * rewrite Fset.equal_spec; intro; apply refl_equal.
    * apply refl_equal.
- (* 1/2 *)
  intros X Y Y' Z H1 HY H2 IH1 IH2 _X _Z F' X' Z' HF Hx Hz Hd.
  injection Hd; clear Hd; do 2 intro; subst _X _Z.
  constructor 4 with Y Y'.
  + assumption.
  + apply IH1 with X Y.
    * assumption.
    * assumption.
    * rewrite Fset.equal_spec; intro; apply refl_equal.
    * apply refl_equal.
  + apply IH2 with Y' Z.
    * assumption.
    * rewrite Fset.equal_spec; intro; apply refl_equal.
    * assumption.
    * apply refl_equal.
- assumption.
Qed.

(** When [F] is a subset of [F'], all that can be derived from [F] can also be derived from [F']. *)
Lemma dtree_is_monotonic :
  forall F F' X Y, Feset.subset _ F F' = true -> dtree F (X --> Y) -> dtree F' (X --> Y).
Proof.
intros F F' X Y H t.
set (d := X --> Y) in *; clearbody d; clear X Y.
revert F' H.
apply (dtree_rec3 (fun d (HT : dtree F d) =>
                     forall (F' : Feset.set FFD), Feset.subset _ F F' = true -> dtree F' d)).
- (* 1/5 *)
  intros X Y H F' HF'; constructor 1.
  rewrite Feset.subset_spec in HF'; apply HF'; assumption.
- (* 1/4 *)
  intros X Y H F' _; constructor 2; assumption.
- (* 1/3 *)
  intros X Y Z Xz Yz txy Hx Hy IH F' HF'; constructor 3 with X Y Z.
  + assumption.
  + assumption.
  + apply IH; assumption.
- (* 1/2 *)
  intros X Y Y' Z tx Hy tz IHx IHz F' HF'; constructor 4 with Y Y'. 
  + assumption.
  + apply IHx; assumption.
  + apply IHz; assumption.
- (* 1/1 *)
  assumption.
Qed.

(* Soundness *)

(** Armstrong's axioms are sound: when a dependency [d] can be derived
  from [F], any set of tuples [ST] which satisfies [F] also satisfies
  [d]. The proof simply proceeds by induction over the derivation
  tree. *)

Lemma Armstrong_soundness :
  forall F d (t : dtree F d) ST, (forall f, f inSE F -> fd_sem ST f) -> fd_sem ST d.
Proof.
intros F d t.
apply (dtree_rec3 (fun d (t : dtree F d) =>  
   forall ft : setT,
    (forall f : fd, f inSE F -> fd_sem ft f) -> fd_sem ft d)).
- (* 1/5 *)
  intros X Y H ft Hf; apply Hf; assumption.
- (* 1/4 *)
  intros X Y H ft Hf.
  intros t1 t2 Ht1 Ht2 KX a Ha; apply KX.
  rewrite Fset.subset_spec in H; apply H; assumption.
- (* 1/3 *)
  intros X Y Z Xz Yz dt1 HXz HYz IH ft Hf.
  intros t1 t2 Ht1 Ht2 KX a Ha.
  rewrite Fset.equal_spec in HXz, HYz.
  rewrite HYz, Fset.mem_union in Ha.
  case_eq (Fset.mem (A T) a Y); intro Ka; rewrite Ka in Ha.
  + apply (IH _ Hf t1 t2 Ht1 Ht2).
    * intros b Hb; apply KX.
      rewrite HXz, Fset.mem_union, Hb; apply refl_equal.
    * assumption.
  + apply KX.
    simpl in Ha; rewrite HXz, Fset.mem_union, Ha, Bool.orb_true_r; apply refl_equal.
- (* 1/2 *)
  intros X Y Y' Z Hy dt1 dt2 IH1 IH2 ft Hf.
  intros t1 t2 Ht1 Ht2 KX a Ha.
  assert (IHH1 : forall y : attribute T, y inS Y' -> dot T t1 y = dot T t2 y).
  {
    intros b Hb; apply (IH1 _ Hf t1 t2 Ht1 Ht2 KX).
    rewrite Fset.equal_spec in Hy; rewrite Hy; assumption.
  }
  apply (IH2 _ Hf t1 t2 Ht1 Ht2 IHH1 _ Ha).
- (* 1/1 *)
  assumption.
Qed.

(* Completeness *)

Lemma union_is_derivable :
  forall F X Y Z, dtree F (X --> Y) -> dtree F (X --> Z) -> dtree F (X --> (Y unionS Z)).
Proof.
intros F X Y Z Hxy Hxz.
constructor 4 with (X unionS Y) (X unionS Y).
rewrite Fset.equal_spec; intro x; rewrite Fset.mem_union; case (Fset.mem (A T) x X); apply refl_equal.
constructor 3 with X Y X.
rewrite Fset.equal_spec; intro x; rewrite Fset.mem_union; case (x inS?X); apply refl_equal.
rewrite Fset.equal_spec; intro x; rewrite 2 Fset.mem_union, Bool.orb_comm; apply refl_equal.
assumption.
constructor 3 with X Z Y.
rewrite Fset.equal_spec; intro x; apply refl_equal.
rewrite Fset.equal_spec; intro x; rewrite 2 Fset.mem_union, Bool.orb_comm; apply refl_equal.
assumption.
Qed.

Lemma pseudo_transitivity_is_derivable :
  forall F X Y Z W, 
    dtree F (X --> Y) -> dtree F ((W unionS Y) --> Z) -> dtree F ((X unionS W) --> Z).
Proof.
intros F X Y Z fw Hxy Hwyz.
constructor 4 with (fw unionS Y) (fw unionS Y).                        
rewrite Fset.equal_spec; intro; apply refl_equal.
constructor 3 with X Y fw.
rewrite Fset.equal_spec; intro; apply refl_equal.
rewrite Fset.equal_spec; intro x; rewrite 2 Fset.mem_union, Bool.orb_comm; apply refl_equal.
assumption.
assumption.
Qed.

Lemma decomposition_is_derivable :
  forall F X Y Z, dtree F (X --> Y) -> Z subS Y -> dtree F (X --> Z).
Proof.
intros F X Y Z Hxy H.
constructor 4 with Y Y.
rewrite Fset.equal_spec; intro; apply refl_equal.
assumption.
constructor 2; assumption.
Qed.

(** closure of a set of attributes [Z] w.r.t. a set of functional
dependencies [F], defined according to Ullman. One has to take care to
stop the computation only when no dependency in [F] can augment the
result. Otherwise, if a dependency [V --> W] is not immediately useful
(i.e. [V] is not a subset of [Z]), it kept for a later application in
the set [F2]. *)

Function closure_list (F : list fd) (Z : setA) {measure List.length F} : setA :=
  match partition (fun d => match d with V --> W => Fset.subset (A T) V Z end) F with
    | (nil, _) => Z
    | (F1, F2) => 
      let Z' := 
          Z unionS (Fset.Union (A T) (map (fun d => match d with _ --> W => W end) F1)) in
        closure_list F2 Z'
  end.
intros F Z _ F2 [X Y] F1 _ H2.
set (ff := (fun d : fd =>
          match d with
          | X --> _ => Fset.subset (A T) X Z
          end)) in *.
rewrite (ListPermut._permut_length (Oeset.partition_spec3 OFD ff _ H2)), app_length; simpl.
apply le_n_S; apply le_plus_r.
Defined.

Definition closure (F : Feset.set FFD) (Z : setA) : setA := closure_list (Feset.elements FFD F) Z.

Lemma closure_deriv :
   forall F X Y, Y subS (closure F X) -> dtree F (X --> Y).
Proof.
intros F X Y H.
apply dtree_is_monotonic with (Feset.mk_set FFD (Feset.elements FFD F));
  [rewrite Feset.subset_spec; intros f Hf; 
   rewrite Feset.mem_mk_set, <- Feset.mem_elements in Hf; assumption | ].
unfold closure in H.
set (l := Feset.elements FFD F) in *; clearbody l; clear F.
set (n := List.length l).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert l Hn X Y H; induction n as [ | n]; intros F Hn X Y H.
- (* 1/2 *)
  destruct F; [ | apply False_rect; inversion Hn].
  rewrite closure_list_equation in H; simpl in H.
  constructor 2; assumption.
- (* 1/1 *)
  case_eq (partition 
             (fun d : fd => match d with | V --> _ => V subS? X end) F); intros F1 F2 HF.
  rewrite closure_list_equation, HF in H.
  destruct F1 as [ | [X1 Y1] F1]; [constructor 2; assumption | ].
  assert (L : List.length F2 <= n).
  {
    apply le_S_n; refine (le_trans _ _ _ _ Hn).
    apply (closure_list_tcc F X (F1 := X1 --> Y1 :: F1) (refl_equal _) HF).
  }
  assert (H1 : dtree (Feset.mk_set FFD F)
                       ((X
                           unionS Fset.Union (A T)
                           (map (fun d : fd => match d with
                                                 | _ --> W => W
                                               end) (X1 --> Y1 :: F1))) --> Y)).
    {
      refine (dtree_is_monotonic _ _ (IHn _ L _ _ H)).
      rewrite Feset.subset_spec; intros e He.
      rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff in He.
      destruct He as [e' [He He']].
      rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff; exists e'; split; trivial.
      assert (HF2 : F2 = snd (X1 --> Y1 :: F1, F2)); [apply refl_equal | ].
      rewrite <- HF, partition_spec2 in HF2.
      rewrite HF2, filter_In in He'.
      apply (proj1 He').
    }
    refine (D_Trans (Fset.equal_refl _ _) _ H1).
    apply union_is_derivable; [constructor 2; apply Fset.subset_refl | ].
    assert (KF1 : forall V W, List.In (V --> W) (X1 --> Y1 :: F1) -> (In (V --> W) F /\ V subS X)).
    {
      intros V W K.
      assert (HF1 : X1 --> Y1 :: F1 = fst (X1 --> Y1 :: F1, F2)); [apply refl_equal | ].
      rewrite <- HF, partition_spec1 in HF1.
      rewrite HF1, filter_In in K; apply K.
    }
    set (_F1 := X1 --> Y1 :: F1) in *; clearbody _F1; clear X1 Y1 F1.
    clear H HF H1 Hn Y F2 L; revert X F KF1.
    induction _F1 as [ | [V W] F1]; intros X F HF1.
  + constructor 2; simpl.
    rewrite Fset.subset_spec; intros x Hx; rewrite Fset.mem_empty in Hx; discriminate Hx.
  + rewrite map_unfold, Fset.Union_unfold.
    apply union_is_derivable.
    * assert (H := HF1 _ _ (or_introl _ (refl_equal _))).
      {
        constructor 4 with V V. 
        - apply Fset.equal_refl.
        - constructor 2; apply (proj2 H).
        - constructor 1; rewrite Feset.mem_mk_set, Oeset.mem_bool_true_iff.
          exists (V --> W); split; [ | apply (proj1 H)].
          apply Oeset.compare_eq_refl.
      }
    * apply IHF1.
      do 3 intro; apply HF1; right; assumption.
Qed.

(** A set of attributes is a subset of its closure. *)
Lemma closure_list_is_increasing :
  forall l X, X subS (closure_list l X).
Proof.
intro l.
set (n := List.length l).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert l Hn; induction n as [ | n]; intros F Hn X.
- (* 1/2 *)
  destruct F; [ | inversion Hn].
  rewrite closure_list_equation; simpl; apply Fset.subset_refl.
- (* 1/1 *)
  destruct F as [ | [X1 Y1] F]; rewrite closure_list_equation; [simpl; apply Fset.subset_refl | ].
  simpl partition.
  case_eq (partition
             (fun d : fd =>
                match d with
                  | X0 --> _ => Fset.subset (A T) X0 X
                end) F); intros F1 F2 HF.
  assert (Hn2 : List.length F2 <= n).
  {
    set (ff := (fun d : fd =>
                  match d with
                    | V --> _ => Fset.subset (A T) V X
                  end)) in *.
    apply le_trans with (List.length F).
    - rewrite (ListPermut._permut_length (Oeset.partition_spec3 OFD ff _ HF)), app_length; 
      simpl.
      apply le_plus_r.
    - simpl in Hn; apply le_S_n; apply Hn.
  }
  case_eq (X1 subS? X); intro H1.
  + refine (Fset.subset_trans _ _ _ _ _ (IHn _ Hn2 _)).
    rewrite Fset.subset_spec; intros x Hx; rewrite Fset.mem_union, Hx.
    apply refl_equal.
  + destruct F1 as [ | [X2 Y2] F1]; [apply Fset.subset_refl | ].
    refine (Fset.subset_trans _ _ _ _ _ (IHn _ _ _)).
    * rewrite Fset.subset_spec; intros x Hx; rewrite Fset.mem_union, Hx.
      apply refl_equal.
    * apply le_S_n; refine (le_trans _ _ _ _ Hn).
      apply (le_n_S _ _ (closure_list_tcc _ X (refl_equal _) HF)).
Qed.

(** The closure is monotonic w.r.t. the relation subset. *)

(** The closure computation is invariant w.r.t. sets' equivalence. *)
Lemma closure_list_eq :
  forall F F' X X', _permut (fun x y => Oeset.compare OFD x y = Eq) F F' -> X =S= X' -> 
                    (closure_list F X) =S= (closure_list F' X').
Proof.
intros F; set (n := List.length F); assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert F Hn; induction n as [ | n]; intros F Hn F' X X' PF HX.
- destruct F as [ | f1 F]; [ | inversion Hn].
  inversion PF; subst.
  rewrite 2 closure_list_equation; simpl; assumption.
- rewrite (closure_list_equation F X).
  case_eq (partition (fun d : fd => match d with
                                 | V --> _ => V subS? X
                                 end) F); intros F1 F2 KF.
  case_eq F1.
  + intro HF1; subst F1.
    rewrite closure_list_equation.
    case_eq (partition (fun d : fd => match d with
                                 | V --> _ => V subS? X'
                                 end) F'); intros F1' F2' KF'.
    assert (HF1' : F1' = nil).
    {
      destruct F1' as [ | f1' F1']; [apply refl_equal | ].
      assert (Hf1' : In f1' (fst (f1' :: F1', F2'))); [left; apply refl_equal | ].
      rewrite <- KF', partition_spec1, filter_In in Hf1'.
      assert (Kf1' : Oeset.mem_bool OFD f1' F = true).
      {
        rewrite (Oeset.permut_mem_bool_eq _ PF), Oeset.mem_bool_true_iff.
        exists f1'; split; [ | apply (proj1 Hf1')].
        apply Oeset.compare_eq_refl.
      }
      rewrite Oeset.mem_bool_true_iff in Kf1'; destruct Kf1' as [f1 [Kf1' Hf1]].
      assert (Kf1 : In f1 (fst (nil, F2))).
      {
        rewrite <- KF, partition_spec1, filter_In.
        split; [assumption | ].
        rewrite <- (proj2 Hf1').
        destruct f1 as [V1 W1]; destruct f1' as [V1' W1']; simpl in Kf1'.
        case_eq (Fset.compare (A T) V1' V1); 
          intro HV1; rewrite HV1 in Kf1'; try discriminate Kf1'.
        rewrite (Fset.subset_eq_2 _ _ _ _ HX).
        apply sym_eq; apply Fset.subset_eq_1.
        rewrite Fset.compare_spec in HV1; apply HV1.
      }
      contradiction Kf1.
    }
    rewrite HF1'; assumption.
  + intros f1 _F1 HF1.
    rewrite (closure_list_equation F' X').
    case_eq (partition (fun d : fd => match d with
                                 | V --> _ => V subS? X'
                                 end) F'); intros F1' F2' HF'.
    assert (HF1' : F1' <> nil).
    {
      destruct F1'; [ | discriminate].
      assert (Hf1 : In f1 (fst (f1 :: _F1, F2))); [left; apply refl_equal | ].
      rewrite <- HF1, <- KF, partition_spec1, filter_In in Hf1.
      assert (Kf1 : Oeset.mem_bool OFD f1 F' = true).
      {
        rewrite <- (Oeset.permut_mem_bool_eq _ PF), Oeset.mem_bool_true_iff.
        exists f1; split; [ | apply (proj1 Hf1)].
        apply Oeset.compare_eq_refl.
      }
      rewrite Oeset.mem_bool_true_iff in Kf1; destruct Kf1 as [f1' [Kf1 Hf1']].
      assert (Kf1' : In f1' (fst (nil, F2'))).
      {
        rewrite <- HF', partition_spec1, filter_In.
        split; [assumption | ].
        rewrite <- (proj2 Hf1).
        destruct f1 as [V1 W1]; destruct f1' as [V1' W1']; simpl in Kf1.
        case_eq (Fset.compare (A T) V1 V1'); 
          intro HV1; rewrite HV1 in Kf1; try discriminate Kf1.
        rewrite (Fset.subset_eq_2 _ _ _ _ HX).
        apply sym_eq; apply Fset.subset_eq_1.
        rewrite Fset.compare_spec in HV1; apply HV1.
      }
      contradiction Kf1'.
    }
    case_eq F1'; [intro KF1'; apply False_rec; apply HF1'; apply KF1' | ].
    intros f1' _F1' _HF1'.
    apply IHn.
    * apply le_S_n; refine (le_trans _ _ _ _ Hn).
      rewrite HF1 in KF; apply (closure_list_tcc _ _ HF1 KF).
    * apply Oeset.nb_occ_permut.
      intros [V W]; replace F2 with (snd (F1, F2)); replace F2' with (snd (F1',F2')); trivial.
      {
        rewrite <- KF, <- HF', 2 partition_spec2, 2 Oeset.nb_occ_filter.
        - rewrite (Fset.subset_eq_2 _ _ _ _ HX), (Oeset.permut_nb_occ _ PF).
          apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H.
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H.
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV).
          apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H.
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H.
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV).
          apply refl_equal.
      }
    * apply Fset.union_eq; [assumption | ].
      assert (H : _permut (fun x y : fd => Oeset.compare OFD x y = Eq) (f1 :: _F1) (f1' :: _F1')).
      {
        apply Oeset.nb_occ_permut.
        intros [V W]; replace (f1 :: _F1) with (fst (f1 :: _F1, F2)); 
        replace (f1' :: _F1') with (fst (f1' :: _F1',F2')); trivial.
        rewrite <- HF1, <- _HF1', <- KF, <- HF', 2 partition_spec1, 2 Oeset.nb_occ_filter.
        - rewrite (Fset.subset_eq_2 _ _ _ _ HX), (Oeset.permut_nb_occ _ PF).
          apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H.
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H.
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV).
          apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H.
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H.
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV).
          apply refl_equal.
      }
      rewrite Fset.equal_spec; intro x.
      {
        rewrite eq_bool_iff, 2 Fset.mem_Union; split; 
        intros [s [Hs Ks]]; rewrite in_map_iff in Hs;
        destruct Hs as [[V W] [_Hs Hs]]; subst s. 
        - assert (Js : Oeset.mem_bool OFD (V --> W) (f1' :: _F1') = true).
          {
            rewrite <- (Oeset.permut_mem_bool_eq _ H), Oeset.mem_bool_true_iff.
            exists (V --> W); split; [apply Oeset.compare_eq_refl | trivial].
          }
          rewrite Oeset.mem_bool_true_iff in Js.
          destruct Js as [[V' W'] [Js Hs']].
          exists W'; split.
          + rewrite in_map_iff; exists (V' --> W'); split; trivial.
          + simpl in Js.
            case_eq (Fset.compare (A T) V V'); intro HV; rewrite HV in Js; try discriminate Js.
            rewrite Fset.compare_spec in Js.
            rewrite <- (Fset.mem_eq_2 _ _ _ Js); assumption.
        - assert (Js : Oeset.mem_bool OFD (V --> W) (f1 :: _F1) = true).
          {
            rewrite (Oeset.permut_mem_bool_eq _ H), Oeset.mem_bool_true_iff.
            exists (V --> W); split; [apply Oeset.compare_eq_refl | trivial].
          }
          rewrite Oeset.mem_bool_true_iff in Js.
          destruct Js as [[V' W'] [Js Hs']].
          exists W'; split.
          + rewrite in_map_iff; exists (V' --> W'); split; trivial.
          + simpl in Js.
            case_eq (Fset.compare (A T) V V'); intro HV; rewrite HV in Js; try discriminate Js.
            rewrite Fset.compare_spec in Js.
            rewrite <- (Fset.mem_eq_2 _ _ _ Js); assumption.
      }
Qed.

Lemma closure_list_subs_eq_1 :
 forall F Fi X, (forall V W, In (V --> W) Fi -> W subS X) ->
  closure_list F X =S= closure_list (Fi ++ F) X.
Proof.
assert (H : forall F V W X, W subS X -> closure_list F X =S= closure_list (V --> W :: F) X).
{
  intros F; set (n := List.length F).
  assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
  revert F Hn; induction n as [ | n]; intros F Hn V W X H;
  rewrite (closure_list_equation F), (closure_list_equation (V --> W :: F)).
  - destruct F; [ | inversion Hn]; simpl.
    case_eq (V subS? X); intro HV.
    + rewrite closure_list_equation; simpl.
      rewrite Fset.equal_spec; intro x.
      rewrite 2 Fset.mem_union, Fset.mem_empty, Bool.orb_false_r.
      case_eq (x inS? W); intro Hx; [ | rewrite Bool.orb_false_r; apply refl_equal].
      rewrite Fset.subset_spec in H; rewrite (H _ Hx); apply refl_equal.
    + apply Fset.equal_refl.
  - simpl partition; case_eq (partition (fun d : fd => match d with
                                 | V0 --> _ => V0 subS? X
                                 end) F); intros F1 F2 HF.
    case_eq (V subS? X); intro HV.
    + rewrite (map_unfold _ (_ :: _)); simpl Fset.Union.
      rewrite (Fset.equal_eq_2
                 _ _ _ _ (closure_list_eq _ _ (Oeset.permut_refl _ _) (Fset.union_assoc _ _ _ _))).
      assert (H' : X unionS W =S= X).
      {
        rewrite Fset.subset_spec in H.
        rewrite Fset.equal_spec; intro x; rewrite Fset.mem_union.
        case_eq (x inS? W); intro Hx; [ | rewrite Bool.orb_false_r; apply refl_equal].
        rewrite (H _ Hx); apply refl_equal.
      }
      rewrite (Fset.equal_eq_2
                 _ _ _ _ (closure_list_eq _ _ (Oeset.permut_refl _ _) (Fset.union_eq_1 _ _ _ _ H'))).
      destruct F1; [simpl | apply Fset.equal_refl].
      assert (H'' : X unionS (emptysetS) =S= X).
      {
        rewrite Fset.subset_spec in H.
        rewrite Fset.equal_spec; intro x; 
        rewrite Fset.mem_union, Fset.mem_empty, Bool.orb_false_r.
        apply refl_equal.
      }
      rewrite (Fset.equal_eq_2
                 _ _ _ _ (closure_list_eq _ _ (Oeset.permut_refl _ _) H'')).
      rewrite closure_list_equation.
      case_eq (partition (fun d : fd => match d with
                                          | V0 --> _ => V0 subS? X
                                        end) F2); intros F1 F22 HF2.
      assert (HF1 : F1 = nil).
      {
        destruct F1 as [ | [V1 W1] F1]; [apply refl_equal | ].
        assert (H1 : In (V1 --> W1) (fst (V1 --> W1 :: F1, F22))); [left; apply refl_equal | ].
        rewrite <- HF2, partition_spec1, filter_In in H1.
        assert (H2 : In (V1 --> W1) (snd (@nil fd, F2))); [apply (proj1 H1) | ].
        rewrite <- HF, partition_spec2, filter_In in H2.
        rewrite (proj2 H1) in H2; destruct H2 as [_ H2]; discriminate H2.
      }
      rewrite HF1; apply Fset.equal_refl.
    + destruct F1 as [ | f1 F1]; [apply Fset.equal_refl | ].
      apply IHn.
      * apply le_S_n; refine (le_trans _ _ _ _ Hn).
        apply (closure_list_tcc _ _ (refl_equal _) HF).
      * rewrite Fset.subset_spec in H.
        rewrite Fset.subset_spec; intros x Hx.
        rewrite Fset.mem_union, (H _ Hx); apply refl_equal. 
}
intros F Fi X; induction Fi as [ | [Vi Wi] Fi]; intro Hi; [apply Fset.equal_refl | simpl].
rewrite (Fset.equal_eq_1 _ _ _ _ (IHFi (fun V W h => Hi V W (or_intror _ h)))).
apply H; apply (Hi _ _ (or_introl _ (refl_equal _))).
Qed.

Lemma closure_list_is_monotonic :
  forall F F' X X', 
    (forall f, Oeset.nb_occ OFD f F = 0 \/ Oeset.nb_occ OFD f F = Oeset.nb_occ OFD f F')%N -> 
    X subS X' ->
    (closure_list F X) subS (closure_list F' X').
Proof.
intros F F'.
set (n := List.length F').
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert F F' Hn; induction n as [ | n]; intros F F' Hn X X' HFF' HXX'.
- (* 1/2 *)
  destruct F'; [ | inversion Hn].
  destruct F as [ | f _F]; [rewrite closure_list_equation; apply HXX' | ].
  apply False_rec.
  assert (Hf := HFF' f); destruct f as [V W]; simpl in Hf.
  rewrite 2 Fset.compare_eq_refl in Hf.
  destruct (Oeset.nb_occ OFD (V --> W) _F); destruct Hf as [Hf | Hf]; 
  discriminate Hf.
- (* 1/1 *)
  rewrite (closure_list_equation F X).
  case_eq (partition (fun d : fd => match d with
                                 | V --> _ => V subS? X
                                 end) F); intros F1 F2 HF.
  destruct F1 as [ | f1 _F1].
  + refine (Fset.subset_trans _ _ _ _ HXX' _).
    apply closure_list_is_increasing.
  + rewrite (closure_list_equation F' X').
    case_eq (partition (fun d : fd => match d with
                                 | V --> _ => V subS? X'
                                 end) F'); intros F1' F2' HF'.
    destruct F1' as [ | f1' _F1'].
    * apply False_rec.
      assert (Hf1 : In f1 (@fst _ _ (f1 :: _F1, F2))); [left; apply refl_equal | ].
      rewrite <- HF, partition_spec1, filter_In in Hf1.
      assert (Abs : (Oeset.nb_occ OFD f1 F = 0)%N).
      {
        destruct (HFF' f1) as [Kf1 | Kf1]; [assumption | ].
        rewrite Kf1.
        rewrite (Oeset.not_mem_nb_occ OFD f1 F'); [apply refl_equal | ].
        rewrite <- not_true_iff_false, Oeset.mem_bool_true_iff.
        intros [f1' [Hf1' Kf1']].
        assert (Abs : In f1' (@fst _ _ (nil, F2'))).
        {
          rewrite <- HF', partition_spec1, filter_In; split; [assumption | ].
          destruct f1 as [V W]; destruct f1' as [V' W']; simpl in Hf1'.
          case_eq (Fset.compare (A T) V V'); intro HV; rewrite HV in Hf1'; try discriminate Hf1'.
          rewrite Fset.compare_spec in HV.
          rewrite <- (Fset.subset_eq_1 _ _ _ _ HV).
          refine (Fset.subset_trans _ _ _ _ _ HXX').
          apply (proj2 Hf1).
        }
        contradiction Abs.
      }
      apply (Oeset.in_nb_occ OFD _ _ (proj1 Hf1) Abs).
    * set (Y := X unionS Fset.Union (A T)
                   (map (fun d : fd => match d with
                                         | _ --> W => W
                                       end) (f1 :: _F1))).
      set (Y' := X' unionS Fset.Union (A T)
                   (map (fun d : fd => match d with
                                         | _ --> W => W
                                       end) (f1' :: _F1'))).
      assert (HYY' : Y subS Y').
      {
        unfold Y, Y'.
        rewrite Fset.subset_spec; intro a; rewrite 2 Fset.mem_union, 2 Bool.orb_true_iff.
        intros [Ha | Ha]; [left; rewrite Fset.subset_spec in HXX'; apply HXX'; assumption | right].
        rewrite Fset.mem_Union in Ha.
        destruct Ha as [s [Hs Ha]]; rewrite in_map_iff in Hs.
        destruct Hs as [f [Hs Hf]]; subst s.
        replace (f1 :: _F1) with (fst (f1 :: _F1, F2)) in Hf by reflexivity.
        rewrite <- HF in Hf.
        assert (Kf := Oeset.in_nb_occ OFD _ _ Hf).
        rewrite Oeset.nb_occ_partition_1 in Kf;
          [ | intros [V1 W1] [V2 W2] _ H; simpl in H; apply Fset.subset_eq_1;
              case_eq (Fset.compare (A T) V1 V2); intro HV1; rewrite HV1 in H; 
              try discriminate H; rewrite Fset.compare_spec in HV1; assumption].
        destruct f as [V W].
        case_eq (V subS? X); intro HV; rewrite HV in Kf.
        - assert (Jf := Oeset.not_mem_nb_occ OFD (V --> W) F').
          case_eq (Oeset.mem_bool OFD (V --> W) F'); intro Lf.
          + rewrite Oeset.mem_bool_true_iff in Lf.
            destruct Lf as [[V' W'] [Lf Mf]]; simpl in Lf.
            rewrite Fset.mem_Union; exists W'; split.
            * rewrite in_map_iff; exists (V' --> W'); split; trivial.
              replace (f1' :: _F1') with (fst (f1' :: _F1', F2')) by reflexivity.
              rewrite <- HF', partition_spec1, filter_In.
              split; [assumption | ].
              case_eq (Fset.compare (A T) V V'); intro KV; rewrite KV in Lf; try discriminate Lf.
              rewrite Fset.compare_spec in KV.
              rewrite <- (Fset.subset_eq_1 _ _ _ _ KV).
              refine (Fset.subset_trans _ _ _ _ HV HXX').
            * case_eq (Fset.compare (A T) V V'); intro KV; rewrite KV in Lf; try discriminate Lf.
              rewrite Fset.compare_spec in Lf.
              rewrite <- (Fset.mem_eq_2 _ _ _ Lf); assumption.
          + assert (Mf := HFF' (V --> W)).
            rewrite (Jf Lf) in Mf; apply False_rec;
            destruct Mf as [Mf | Mf]; apply Kf; apply Mf.
        - apply False_rec; apply Kf; apply refl_equal.
      } 
      assert (IH : closure_list F2 Y subS closure_list F2 Y').
      {
        apply IHn; [ | | assumption].
        - apply le_S_n; refine (le_trans _ _ _ _ Hn).
          apply le_trans with (List.length F).
          + apply (closure_list_tcc _ _ (refl_equal _) HF).
          + clear Hn HF HF' f1 Y HYY'; revert F' HFF'; 
              induction F as [ | f1 F]; intros F' HFF'; [apply le_O_n | ].
            assert (Hf1 := HFF' f1).
            destruct Hf1 as [Hf1 | Hf1];
              [rewrite Oeset.nb_occ_unfold, Oeset.compare_eq_refl in Hf1;
               rewrite N.eq_add_0 in Hf1; destruct Hf1 as [Hf1 _]; discriminate Hf1 | ].
            assert (Kf1 : Oeset.mem_bool OFD f1 F' = true).
            {
              apply Oeset.nb_occ_mem; rewrite <- Hf1, Oeset.nb_occ_unfold, Oeset.compare_eq_refl.
              intro Abs; rewrite N.eq_add_0 in Abs; destruct Abs as [Abs _].
              discriminate Abs.
            }
            rewrite Oeset.mem_bool_true_iff in Kf1.
            destruct Kf1 as [_f1 [Kf1 Jf1]].
            destruct (in_split _ _ Jf1) as [l1 [l2 H]].
            rewrite H, app_length, 2 (length_unfold (_ :: _)).
            rewrite plus_comm; simpl; apply le_n_S; rewrite plus_comm, <- app_length.
            apply IHF.
            intro f; case_eq (Oeset.compare OFD f f1); intro Hf.
            * right.
              rewrite <- 2 (Oeset.nb_occ_eq_1 _ _ _ _ Hf), H, Oeset.nb_occ_app in Hf1.
              rewrite 2 (Oeset.nb_occ_unfold _ _ (_ :: _)), Hf in Hf1.
              rewrite (Oeset.compare_eq_1 _ _ _ _ Hf), Kf1, (N.add_comm _ (1 + _)) in Hf1.
              rewrite <- N.add_assoc in Hf1.
              assert (Lf1 := Nplus_reg_l _ _ _ Hf1); rewrite N.add_comm in Lf1.
              rewrite Oeset.nb_occ_app; apply Lf1.
            * {
                destruct (HFF' f) as [Kf | Kf].
                - rewrite Oeset.nb_occ_unfold, Hf in Kf; left; apply Kf.
                - right.
                  rewrite Oeset.nb_occ_unfold, Hf, H, Oeset.nb_occ_app in Kf.
                  rewrite (Oeset.nb_occ_unfold _ _ (_ :: _)), 
                    <- (Oeset.compare_eq_2 _ _ _ _ Kf1), Hf in Kf; simpl in Kf.
                  rewrite Kf, Oeset.nb_occ_app; trivial.
              } 
            * {
                destruct (HFF' f) as [Kf | Kf].
                - rewrite Oeset.nb_occ_unfold, Hf in Kf; left; apply Kf.
                - right.
                  rewrite Oeset.nb_occ_unfold, Hf, H, Oeset.nb_occ_app in Kf.
                  rewrite (Oeset.nb_occ_unfold _ _ (_ :: _)), 
                    <- (Oeset.compare_eq_2 _ _ _ _ Kf1), Hf in Kf; simpl in Kf.
                  rewrite Kf, Oeset.nb_occ_app; trivial.
              } 
        - intro; right; apply refl_equal.
      }
      refine (Fset.subset_trans _ _ _ _ IH _).
      set (FF2' := filter (fun x => Oeset.mem_bool OFD x F2') F2).
      set (K2 := filter (fun x => negb (Oeset.mem_bool OFD x FF2')) F2).
      assert (HK2 : _permut (fun x y : fd => Oeset.compare OFD x y = Eq) F2 (K2 ++ FF2')).
      {
        subst K2 FF2'; apply Oeset.nb_occ_permut.
        intros x; rewrite Oeset.nb_occ_app, 2 Oeset.nb_occ_filter, Oeset.mem_bool_filter.
        - case_eq (Oeset.mem_bool OFD x F2'); intro Hx.
          + case_eq (Oeset.mem_bool OFD x F2); trivial.
            intro Kx; rewrite (Oeset.not_mem_nb_occ _ _ _ Kx); trivial.
          + rewrite N.add_0_r; trivial.
        - do 3 intro; apply Oeset.mem_bool_eq_1.
        - do 3 intro; apply Oeset.mem_bool_eq_1.
        - intros x1 x2 _ Hx; apply f_equal; apply Oeset.mem_bool_eq_1; assumption.
      } 
      set (K2' := filter (fun x => negb (Oeset.mem_bool OFD x FF2')) F2').
      assert (HK2' : _permut (fun x y : fd => Oeset.compare OFD x y = Eq) F2' (K2' ++ FF2')).
      {
        subst K2' FF2'; apply Oeset.nb_occ_permut.
        intros [V W]; rewrite Oeset.nb_occ_app.
        replace F2 with (snd (f1 :: _F1, F2)) by reflexivity.
        replace F2' with (snd (f1' :: _F1', F2')) by reflexivity.
        rewrite <- HF, <- HF', 2 partition_spec2, 3 Oeset.nb_occ_filter, 3 Oeset.mem_bool_filter, 
        2 Oeset.nb_occ_filter, Oeset.mem_bool_filter.
        - case_eq (V subS? X'); intro HV'; simpl; trivial.
          case_eq (Oeset.mem_bool OFD (V --> W) F'); 
            intro H'; [ | simpl; rewrite N.add_0_r; apply refl_equal].
          case_eq (V subS? X); 
            intro HV; simpl; [rewrite N.add_0_r; apply refl_equal | ].
          case_eq (Oeset.mem_bool OFD (V --> W) F); intro H; simpl.
          + destruct (HFF' (V --> W)) as [K | K].
            * rewrite (Oeset.nb_occ_not_mem _ _ _ K) in H; discriminate H.
            * apply sym_eq; assumption.
          + rewrite (Oeset.not_mem_nb_occ _ _ _ H), N.add_0_r; apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H;
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H;
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV); apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H;
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H;
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV); apply refl_equal.
        - do 3 intro; apply Oeset.mem_bool_eq_1.
        - intros [V1 W1] [V2 W2] _ H; simpl in H;
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H;
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV); apply refl_equal.
        - intros [V1 W1] [V2 W2] _ H; simpl in H;
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H;
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV); apply refl_equal.
        - do 3 intro; apply Oeset.mem_bool_eq_1.
        - intros [V1 W1] [V2 W2] _ H; simpl in H;
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H;
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV); apply refl_equal.
        - do 4 intro; apply f_equal; apply Oeset.mem_bool_eq_1; assumption.
        - intros [V1 W1] [V2 W2] _ H; simpl in H;
          case_eq (Fset.compare (A T) V1 V2); intro HV; rewrite HV in H; try discriminate H;
          rewrite Fset.compare_spec in HV; rewrite (Fset.subset_eq_1 _ _ _ _ HV); apply refl_equal.
      }
      rewrite (Fset.subset_eq_2 _ _ _ _ (closure_list_eq _ _ HK2' (Fset.equal_refl _ _))).
      rewrite (Fset.subset_eq_1 _ _ _ _ (closure_list_eq _ _ HK2 (Fset.equal_refl _ _))).
      assert (H : closure_list FF2' Y' =S= closure_list (K2 ++ FF2') Y').
      {
        apply closure_list_subs_eq_1.
        intros V W H; unfold K2, FF2' in H.
        rewrite filter_In, negb_true_iff, <- not_true_iff_false in H.
        destruct H as [H1 H2].
        case_eq (Oeset.mem_bool OFD (V --> W) F2'); intro H3.
        - apply False_rec; apply H2.
          rewrite Oeset.mem_bool_true_iff; exists (V --> W); split.
          + apply Oeset.compare_eq_refl.
          + rewrite filter_In; split; trivial.
        - replace F2 with (snd (f1 :: _F1, F2)) in H1 by reflexivity.
          rewrite <- HF, partition_spec2, filter_In in H1.
          assert (H4 : Oeset.mem_bool OFD (V --> W) F' = true).
          {
            apply Oeset.nb_occ_mem.
            destruct (HFF' (V --> W)) as [H4 | H4].
            - apply False_rec; apply (Oeset.in_nb_occ OFD _ _ (proj1 H1) H4).
            - rewrite <- H4; apply (Oeset.in_nb_occ OFD _ _ (proj1 H1)).
          }
          rewrite Oeset.mem_bool_true_iff in H4.
          destruct H4 as [[V' W'] [H4 H5]].
          case_eq (V' subS? X'); intro HV'.
          + assert (H6 : In (V' --> W') (f1' :: _F1')).
            {
              replace (f1' :: _F1') with (fst (f1' :: _F1', F2')) by reflexivity.
              rewrite <- HF', partition_spec1, filter_In; split; trivial.
            }
            simpl in H4.
            case_eq (Fset.compare (A T) V V'); intro HV; rewrite HV in H4; try discriminate H4.
            rewrite Fset.compare_spec in H4.
            rewrite (Fset.subset_eq_1 _ _ _ _ H4).
            unfold Y'; rewrite Fset.subset_spec; intros x Hx.
            rewrite Fset.mem_union, Bool.orb_true_iff; right.
            rewrite Fset.mem_Union; exists W'; split; [ | assumption].
            rewrite in_map_iff; exists (V' --> W'); split; trivial.
          + rewrite <- not_true_iff_false in H3; apply False_rec; apply H3.
            rewrite Oeset.mem_bool_true_iff; exists (V' --> W'); split; [assumption | ].
            replace F2' with (snd (f1' :: _F1', F2')) by reflexivity.
            rewrite <- HF', partition_spec2, filter_In, HV'; split; trivial.
      }
      rewrite <- (Fset.subset_eq_1 _ _ _ _ H).
      {
        apply IHn.
        - rewrite <- (_permut_length HK2').
          apply le_S_n; refine (le_trans _ _ _ _ Hn).
          apply le_trans with (List.length F').
          + apply (closure_list_tcc _ _ (refl_equal _) HF').
          + apply le_n.
        - intro; rewrite Oeset.nb_occ_app.
          unfold K2'; rewrite Oeset.nb_occ_filter.
          + case_eq (Oeset.mem_bool OFD f FF2'); intro Hf.
            * right; apply refl_equal.
            * left; apply Oeset.not_mem_nb_occ; assumption.
          + do 4 intro; apply f_equal; apply Oeset.mem_bool_eq_1; assumption.
        - apply Fset.subset_refl.
      } 
Qed.

(** The main property of the closure. *)
Lemma closure_list_is_closed :
  forall F X V W, In (V --> W) F -> V subS closure_list F X -> 
     W subS closure_list F X.
intros F.
set (n := List.length F).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert F Hn; induction n as [ | n]; 
intros F Hn X V W H HV; [destruct F; [contradiction H | inversion Hn] | ].
rewrite closure_list_equation in HV; rewrite closure_list_equation.
case_eq (partition
               (fun d : fd => match d with
                              | V0 --> _ => V0 subS? X
                              end) F); intros F1 F2 HF; rewrite HF in HV.
destruct F1 as [ | f1 _F1].
- assert (KV : In (V --> W) (fst (nil, F2))).
  {
    rewrite <- HF, partition_spec1, filter_In; split; assumption.
  }
  contradiction KV.
- case_eq (V subS? X); intro KV.
  + refine (Fset.subset_trans _ _ _ _ _ (closure_list_is_increasing _ _)).
    rewrite Fset.subset_spec; intros x Hx; rewrite Fset.mem_union, Bool.orb_true_iff; right.
    rewrite Fset.mem_Union.
    exists W; split; [ | assumption].
    rewrite in_map_iff; exists (V --> W); split; [trivial | ].
    replace (f1 :: _F1) with (fst (f1 :: _F1, F2)) by reflexivity.
    rewrite <- HF, partition_spec1, filter_In; split; trivial.
  + apply IHn with V.  
    * apply le_S_n; refine (le_trans _ _ _ _ Hn).
      apply (closure_list_tcc _ _ (refl_equal _) HF).
    * replace F2 with (snd (f1 :: _F1, F2)) by reflexivity.
      rewrite <- HF, partition_spec2, filter_In, KV; split; trivial.
    * assumption.
Qed.

(** The closure is idempotent. *)
Lemma closure_list_is_idem_aux :
  forall F' F X, 
    (forall V W, In (V --> W) F' -> (W subS closure_list F X \/ In (V --> W) F)) ->
    closure_list F' (closure_list F X) subS closure_list F X.
Proof.
intros F'.
set (n := List.length F').
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert F' Hn; induction n as [ | n]; intros F' Hn F X HHF'.
- (* 1/2 *)
  destruct F'; [ | inversion Hn].
  rewrite closure_list_equation; simpl partition; cbv beta zeta iota.
  apply Fset.subset_refl.
- (* 1/1 *)
  set (X' := closure_list F X) in *.
  rewrite closure_list_equation.
  case_eq (partition (fun d : fd => match d with
                                 | V --> _ => V subS? X'
                                    end) F'); intros F1' F2' HF'.
  destruct F1' as [ | f1' _F1']; [apply Fset.subset_refl | ].
  assert (L : (List.length F2' <= n)%nat).
  {
    apply le_S_n; refine (le_trans _ _ _ _ Hn).
    apply (closure_list_tcc _ _ (refl_equal _) HF').
  }
  refine (Fset.subset_trans _ _ _ _ _ (IHn _ L F X _)).
  + apply closure_list_is_monotonic; [intro; right; trivial | ].
    rewrite Fset.subset_spec; intros x Hx; rewrite Fset.mem_union, Bool.orb_true_iff in Hx.
    destruct Hx as [Hx | Hx]; [apply Hx | ].
    rewrite Fset.mem_Union in Hx; destruct Hx as [s [Hs Hx]]; rewrite in_map_iff in Hs.
    destruct Hs as [[V W] [_Hs Hs]]; subst s.
    replace (f1' :: _F1') with (fst (f1' :: _F1', F2')) in Hs by reflexivity.
    rewrite <- HF', partition_spec1, filter_In in Hs.
    destruct (HHF' _ _ (proj1 Hs)) as [H1 | H1].
    * rewrite Fset.subset_spec in H1; apply H1; assumption.
    * revert x Hx; rewrite <- Fset.subset_spec.
      apply closure_list_is_closed with V; [assumption | ].
      apply (proj2 Hs).
  + intros V W H.
    replace F2' with (snd (f1' :: _F1', F2')) in H by reflexivity.
    rewrite <- HF', partition_spec2, filter_In in H.
    apply (HHF' _ _ (proj1 H)).
Qed.

Lemma closure_list_is_idem : forall F X, closure_list F (closure_list F X) =S= closure_list F X.
Proof.
intros F X.
rewrite Fset.equal_spec; intro e; rewrite eq_bool_iff; split;
revert e; rewrite <- Fset.subset_spec; 
[ apply closure_list_is_idem_aux | apply closure_list_is_increasing].
intros V W H; right; assumption.
Qed.



Section Sec2.

(** There are at least two distinct values. *)
Hypothesis zero : value T.
Hypothesis one : value T.
Hypothesis zero_diff_one : zero <> one.

(** 
- [U] is the universe of attributes. It is needed to build a
model, that is a set of tuples. One has to know the labels of these
tuples. 
- [X --> Y] is a dependency which is assumed to be a semantic consequence of 
 the set of functional dependencies [F] in the main completeness theorem. *)

Hypothesis U X Y : setA.
Hypothesis F : Feset.set FFD.
Hypothesis HF_left : forall V W, (V --> W) inSE F -> V subS U.
Hypothesis HF_right : forall V W, (V --> W) inSE F -> W subS U.

(** Definition of the first tuple in the model. *)
Definition t0 := 
  mk_tuple T U (fun a => if a inS? (closure F X) then one else zero).

(** Definition of the second tuple in the model. *)
Definition t1 := mk_tuple T U (fun a => one).

(** Definition of the model. *)
Definition M := (Feset.singleton FTupleT t0) unionSE (Feset.singleton FTupleT t1).

Lemma fd_t0_t1 :
  forall V W, V --> W inSE F ->
              (forall a, a inS V -> dot T t0 a = dot T t1 a) ->
              forall a, a inS W -> dot T t0 a = dot T t1 a.
Proof.
intros V W H K.
unfold t0, t1 in *.
assert (HV : V subS closure F X).
{
  rewrite Fset.subset_spec; intros e He.
  generalize (K e He).
  assert (J := HF_left _ _ H); rewrite Fset.subset_spec in J.
  assert (Je := J _ He); clear J.
  rewrite !dot_mk_tuple, Je.
  case_eq (e inS? closure F X); [exact (fun _ _ => refl_equal _) | ].
  intros _ Abs; apply False_rec; apply zero_diff_one; assumption.
}
intros a Ha.
assert (Ka : a inS U).
  {
    assert (J := HF_right _ _ H); rewrite Fset.subset_spec in J; apply J; assumption.
  }
  rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in H.
  destruct H as [[V' W'] [H H']].
  simpl in H; case_eq (Fset.compare (A T) V V'); intro KV; rewrite KV in H; try discriminate H.
  rewrite Fset.compare_spec in H, KV; rewrite (Fset.mem_eq_2 _ _ _ H) in Ha.
  rewrite (Fset.subset_eq_1 _ _ _ _ KV) in HV.
  unfold t0, t1; rewrite 2 dot_mk_tuple; trivial.
  assert (Ja : a inS (closure F X)).
  {
    unfold closure.
    assert (J := closure_list_is_closed _ _ _ _ H' HV); rewrite Fset.subset_spec in J.
    apply J; assumption.
  }    
  rewrite Ja; apply refl_equal.
Qed.

(** [M] is indeed a model of [F]. *)
Lemma M_is_a_model : forall f, f inSE F -> fd_sem M f.
Proof.
intros [V W] H.
intros s1 s2 Hs1 Hs2 K a Ha.
assert (Jl := HF_left _ _ H); rewrite Fset.subset_spec in Jl.
assert (Jr := HF_right _ _ H); rewrite Fset.subset_spec in Jr.
assert (Ja := Jr a Ha).
assert (Aux : forall s s', s =t= s' -> (s' = t0 \/ s' = t1) -> 
                           forall a, a inS U -> dot T s a = dot T s' a).
{
  intros s s' H1 Hs; clear a Ha Ja; intros a Ha.
  rewrite tuple_eq in H1; apply (proj2 H1).
  unfold M in Hs.
  destruct Hs as [Hs | Hs]; unfold t0, t1 in Hs;
   rewrite (Fset.mem_eq_2 _ _ _ (proj1 H1)), Hs, (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _));
   assumption.
}
unfold M in Hs1, Hs2.
rewrite Feset.mem_union, 2 Feset.singleton_spec, Bool.orb_true_iff in Hs1, Hs2;
destruct Hs1 as [Hs1 | Hs1]; destruct Hs2 as [Hs2 | Hs2];
  rewrite Oeset.eq_bool_true_compare_eq in Hs1, Hs2.
- rewrite (Aux _ _ Hs1 (or_introl _ (refl_equal _)) _ Ja).
  rewrite (Aux _ _ Hs2 (or_introl _ (refl_equal _)) _ Ja); apply refl_equal.
- rewrite (Aux _ _ Hs1 (or_introl _ (refl_equal _)) _ Ja).
  rewrite (Aux _ _ Hs2 (or_intror _ (refl_equal _)) _ Ja).
  apply fd_t0_t1 with V W; trivial.
  intros a' Ha'; 
    assert (Ja' := Jl a' Ha').
  rewrite <- (Aux _ _ Hs1 (or_introl _ (refl_equal _)) _ Ja'), 
                 <- (Aux _ _ Hs2 (or_intror _ (refl_equal _)) _ Ja'), K; trivial.
- rewrite (Aux _ _ Hs1 (or_intror _ (refl_equal _)) _ Ja).
  rewrite (Aux _ _ Hs2 (or_introl _ (refl_equal _)) _ Ja).
  apply sym_eq; apply fd_t0_t1 with V W; trivial.
  intros a' Ha'; 
    assert (Ja' := Jl a' Ha').
  rewrite <- (Aux _ _ Hs1 (or_intror _ (refl_equal _)) _ Ja'), 
                 <- (Aux _ _ Hs2 (or_introl _ (refl_equal _)) _ Ja'), K; trivial.
- rewrite (Aux _ _ Hs1 (or_intror _ (refl_equal _)) _ Ja).
  rewrite (Aux _ _ Hs2 (or_intror _ (refl_equal _)) _ Ja); apply refl_equal.
Qed.

(** Main theorem of completeness. *)
Lemma Armstrong_completeness :
  X subS U -> Y subS U ->
  (forall ST, (forall t, t inSE ST -> labels T t =S= U) -> 
              (forall f, f inSE F -> fd_sem ST f) -> fd_sem ST (X --> Y)) ->
  (dtree F (X --> Y)).
Proof.
intros HX HY H.
assert (HM : fd_sem M (X --> Y)).
{
  apply H.
  - intros t Ht; unfold M in Ht.
    rewrite Feset.mem_union, 2 Feset.singleton_spec, Bool.orb_true_iff in Ht.
    destruct Ht as [Ht | Ht]; rewrite Oeset.eq_bool_true_compare_eq, tuple_eq in Ht.
    + rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)); unfold t0.
      apply labels_mk_tuple.
    + rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)); unfold t1.
      apply labels_mk_tuple.
  - apply M_is_a_model.
}
apply closure_deriv.
rewrite Fset.subset_spec; intros e He.
assert (Ht0 : t0 inSE M).
{
  unfold M; rewrite Feset.mem_union, Feset.singleton_spec; unfold Oeset.eq_bool.
  rewrite Oeset.compare_eq_refl; apply refl_equal.
}
assert (Ht1 : t1 inSE M).
{
  unfold M; rewrite Feset.mem_union, 2 Feset.singleton_spec; unfold Oeset.eq_bool.
  rewrite Oeset.compare_eq_refl, Bool.orb_true_r; apply refl_equal.
}
assert (KX : forall x : attribute T, x inS X -> dot T t0 x = dot T t1 x).
{
  intros x Hx; unfold t0, t1.
  rewrite 2 dot_mk_tuple.
  - assert (K := closure_list_is_increasing (Feset.elements _ F) X).
    rewrite Fset.subset_spec in K; unfold closure; rewrite (K _ Hx); apply refl_equal.
}
rewrite Fset.subset_spec in HY.
generalize (HM t0 t1 Ht0 Ht1 KX e He).
unfold t0, t1; rewrite 2 dot_mk_tuple, (HY _ He).
case_eq (e inS? (closure F X)); [intros; apply refl_equal | ].
intros _ Abs; apply False_rec; apply zero_diff_one; assumption.
Qed.

End Sec2.

End Sec.
