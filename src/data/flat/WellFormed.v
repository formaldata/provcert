(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Require Import List Bool Arith NArith.
Require Import BasicTacs BasicFacts Bool3 ListFacts ListSort ListPermut
        OrderedSet Partition FiniteSet FiniteBag FiniteCollection Join.

Require Import FTuples NJoinTuples FTerms ATerms Env Interp Projection.
Import Tuple.

Section Sec.

Hypothesis T : Rcd.

Definition well_formed_ft env f := 
  is_built_upon_ft T (extract_funterms T (flat_map (groups_of_env_slice T) env)) f.

Fixpoint well_formed_ag env a :=
  match a with
  | A_Expr _ f => well_formed_ft env f
  | A_agg _ _ f => 
    Fset.is_empty (A T) (variables_ft _ f) ||
       match find_eval_env T env f with Some _ => true | None => false end
  | A_fun _ _ la => forallb (well_formed_ag env) la
  end.

Definition well_formed_s env s :=
  forallb (fun x => match x with Select_As _ e _ => well_formed_ag env e end) s &&
  Oset.all_diff_bool (OAtt T) (map (fun x => fst match x with Select_As _ e a => (a, e) end) s) 
  (* &&
  Fset.is_empty 
  (A T) 
  (Fset.inter 
     _ (Fset.mk_set _ (map (fun x => fst match x with Select_As _ e a => (a, e) end) s))
     (Fset.Union _ (map (fun x => match x with (s, _, _) => s end) env))) *).

Fixpoint well_formed_e env:=
  match env with
  | nil => true
  | (sa, g, l) :: env =>
    match g with
      | Group_By _ g => 
        forallb (fun x => 
                   well_formed_ag 
                     ((sa, Group_By T (map (fun a => A_Expr T (F_Dot T a)) ({{{sa}}})), 
                       (default_tuple T sa :: nil)) :: env) x) g &&
        (match l with
         | nil => false
         | t1 :: _ => 
           forallb 
             (fun t => 
                forallb 
                  (fun x => 
                     Oset.eq_bool 
                       (OVal T) (interp_aggterm _ (env_t _ env t) x) 
                       (interp_aggterm _ (env_t _ env t1) x)) g) l
         end)
      | _ => match l with _ :: nil => true | _ => false end
    end 
      && forallb (fun x => labels T x =S?= sa) l 
      && fresh_att_in_env T sa env && well_formed_e env
  end.

Lemma well_formed_ag_unfold :
  forall env a, well_formed_ag env a =
  match a with
  | A_Expr _ f => well_formed_ft env f
  | A_agg _ _ f => 
    Fset.is_empty (A T) (variables_ft _ f) ||
       match find_eval_env T env f with Some _ => true | None => false end
  | A_fun _ _ la => forallb (well_formed_ag env) la
  end.
Proof.
intros env a; case a; intros; apply refl_equal.
Qed.

Lemma well_formed_ft_eq :
  forall f env1 env2, weak_equiv_env T env1 env2 -> well_formed_ft env1 f = well_formed_ft env2 f.
Proof.
intros f.
set (n := size_funterm T f).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert f Hn; induction n as [ | n]; intros f Hn env1 env2 He; [destruct f; inversion Hn | ].
destruct f as [c | a | fc lf]; [apply refl_equal | | ]; simpl.
- revert env2 He; induction env1 as [ | [[sa1 g1] l1] env1]; intros env2 He.
  + inversion He; subst; apply refl_equal.
  + destruct env2 as [ | [[sa2 g2] l2] env2]; [inversion He | ].
    inversion He; subst; simpl.
    unfold well_formed_ft.
    unfold weak_equiv_env_slice in H2; destruct H2 as [K1 K2]; subst g2.
    unfold well_formed_ft; simpl.
    rewrite !extract_funterms_app, !Oset.mem_bool_app, <- (Fset.elements_spec1 _ _ _ K1); 
      apply f_equal.
    apply IHenv1; trivial.
- unfold well_formed_ft; simpl; apply f_equal2.
  + revert env2 He; induction env1 as [ | [[sa1 g1] l1] env1]; intros env2 He.
    * inversion He; subst; apply refl_equal.
    * destruct env2 as [ | [[sa2 g2] l2] env2]; [inversion He | ].
      inversion He; subst; simpl.
      unfold weak_equiv_env_slice in H2; destruct H2 as [K1 K2]; subst g2.
      unfold well_formed_ft; simpl.
      rewrite !extract_funterms_app, !Oset.mem_bool_app, <- (Fset.elements_spec1 _ _ _ K1); 
        apply f_equal.
      apply IHenv1; trivial.
  + apply forallb_eq; intros x Hx.
    apply IHn; trivial.
    simpl in Hn.
    refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply in_list_size; assumption.
Qed.

Lemma well_formed_ag_eq :
  forall a env1 env2, weak_equiv_env T env1 env2 -> well_formed_ag env1 a = well_formed_ag env2 a.
Proof.
intro e; 
set (n := size_aggterm T e).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert e Hn; induction n as [ | n]; 
  intros e Hn env1 env2 He; [destruct e; inversion Hn | ].
destruct e as [f | a f | f la]; simpl.
- apply well_formed_ft_eq; trivial.
- apply f_equal.
  revert env2 He;  induction env1 as [ | [[sa1 g1] l1] env1]; intros env2 He.
  + inversion He; subst; apply refl_equal.
  + destruct env2 as [ | [[sa2 g2] l2] env2]; [inversion He | ].
    inversion He; subst; simpl.
    assert (IH := IHenv1 env2 H4).
    unfold equiv_env_slice in H2; destruct H2 as [K1 K2]; subst g2.
    destruct (find_eval_env T env1 f); destruct (find_eval_env T env2 f);
      try discriminate IH; trivial.
    * destruct g1 as [g1 | ]; apply refl_equal.
    * destruct g1 as [g1 | ]; [ | apply refl_equal].
      rewrite <- (is_a_suitable_env_eq _ _ _ _ _ _ K1 H4).
    case (is_a_suitable_env T sa1 env1 f); apply refl_equal.
- apply forallb_eq; intros x Hx.
  apply IHn; trivial.
  simpl in Hn.
  refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
  apply in_list_size; assumption.
Qed.

Lemma find_eval_env_weak_eq :
  forall e env1 env2, 
    weak_equiv_env T env1 env2 -> 
    match (find_eval_env T env1 e), (find_eval_env T env2 e) with
      | None, None => True
      | Some e1, Some e2 => weak_equiv_env T e1 e2
      | _, _ => False
    end.
Proof.
intros e env1; induction env1 as [ | [[sa1 g1] l1] env1]; intros [ | [[sa2 g2] l2] env2] He.
- simpl; case (is_built_upon_ft T nil e); trivial.
- inversion He.
- inversion He.
- inversion He; subst.
  unfold equiv_env_slice in H2; destruct H2 as [K1 K2]; subst g2.
  assert (IH := IHenv1 _ H4).
  simpl.
  destruct (find_eval_env T env1 e) as [_l1 | ];
    destruct (find_eval_env T env2 e) as [_l2 | ]; try contradiction IH.
  + destruct g1 as [g1 | ]; assumption.
  + destruct g1 as [g1 | ]; [ | trivial].
    rewrite <- (is_a_suitable_env_eq T e _ _ _ _ K1 H4).
    case (is_a_suitable_env T sa1 env1 e); [ | trivial].
    constructor 2; trivial.
    simpl; split; trivial.
Qed.

(*
Lemma find_eval_env_weak_weak_eq :
  forall e env1 env2 s g l,
    (forall x, In x g ->
              is_built_upon_ag 
                T (flat_map (groups_of_env_slice T)
                            ((labels T (default_tuple T s), Group_Fine T, 
                              default_tuple T s :: nil) :: env2)) x = true) ->
    match find_eval_env T (env1 ++ (s, Group_By T g, l) :: env2) e with
    | Some _ => match find_eval_env 
                        T (env1 ++
                                (labels T (default_tuple T s), Group_Fine T, 
                                 default_tuple T s :: nil) :: env2) e with
                | Some _ => True
                | _ => False
                end
    | None => True
    end.
Proof.
intros e env1; induction env1 as [ | [[sa1 g1] l1] env1]; intros env2 s g l Hg; simpl.
- case_eq (find_eval_env T env2 e); [intros; trivial | ].
  intro H; simpl.
  assert (Aux : is_a_suitable_env T (labels T (default_tuple T s)) env2 e =
                is_a_suitable_env T s env2 e).
  {
    simpl.
    apply is_a_suitable_env_eq; [ | apply equiv_env_weak_equiv_env; apply equiv_env_refl].
    apply labels_mk_tuple.
  }
  rewrite Aux.
  case (is_a_suitable_env T s env2 e); trivial.
- assert (IH := IHenv1 env2 s g l).
  case_eq (find_eval_env T (env1 ++ (s, Group_By T g, l) :: env2) e);
    [intros ee Hee; rewrite Hee in IH;
     case_eq 
       (find_eval_env 
          T (env1 ++
            (labels T (default_tuple T s), Group_Fine T, default_tuple T s :: nil) :: env2) e);
    [intros ee' Hee' | intro Hee']; rewrite Hee' in IH; [trivial | contradiction IH] | ].
  case_eq (is_a_suitable_env T sa1 (env1 ++ (s, Group_By T g, l) :: env2) e); [ | intros; trivial].
  intros H1 H2.
  case_eq 
       (find_eval_env 
          T (env1 ++
            (labels T (default_tuple T s), Group_Fine T, default_tuple T s :: nil) :: env2) e);
  [intros; trivial | ].
  intro H4.
  assert (Aux : is_a_suitable_env 
                  T sa1
                  (env1 ++ (labels T (default_tuple T s), Group_Fine T, 
                            default_tuple T s :: nil) :: env2)
                  e = true).
  {
    revert H1; unfold is_a_suitable_env; apply is_built_upon_ag_trans.
    intros x Hx; destruct (in_app_or _ _ _ Hx) as [Kx | Kx].
    - apply in_is_built_upon_ag; apply in_or_app; left; assumption.
    - rewrite flat_map_app in Kx; destruct (in_app_or _ _ _ Kx) as [Jx | Jx].
      + apply in_is_built_upon_ag; apply in_or_app; right; rewrite flat_map_app.
        apply in_or_app; left; assumption.
      + simpl in Jx; destruct (in_app_or _ _ _ Jx) as [Lx | Lx]. 
        * generalize (Hg _ Lx); apply is_built_upon_ag_incl.
          intros y Hy; apply in_or_app; right.
          rewrite flat_map_app; apply in_or_app; right; assumption.
        * apply in_is_built_upon_ag; apply in_or_app; right; rewrite flat_map_app.
          apply in_or_app; right; simpl.
          apply in_or_app; right; assumption.
  }
  rewrite Aux; trivial.
Qed.
*)

Lemma well_formed_e_unfold :
  forall env,  well_formed_e env =
  match env with
  | nil => true
  | (sa, g, l) :: env =>
    match g with
      | Group_By _ g => 
        forallb (fun x => 
                   well_formed_ag 
                     ((sa, Group_By T (map (fun a => A_Expr T (F_Dot T a)) ({{{sa}}})), 
                       (default_tuple T sa :: nil)) :: env) x) g &&
        (match l with
         | nil => false
         | t1 :: _ => 
           forallb 
             (fun t => 
                forallb 
                  (fun x => 
                     Oset.eq_bool 
                       (OVal T) (interp_aggterm _ (env_t _ env t) x) 
                       (interp_aggterm _ (env_t _ env t1) x)) g) l
         end)
      | _ => match l with _ :: nil => true | _ => false end
    end 
      && forallb (fun x => labels T x =S?= sa) l 
      && fresh_att_in_env T sa env 
      && well_formed_e env
  end.
Proof.
intro env; case env; intros; apply refl_equal.
Qed.

Lemma well_formed_e_tail :
  forall e1 e2, well_formed_e (e1 ++ e2) = true -> well_formed_e e2 = true.
Proof.
intro e1; induction e1 as [ | [[s1 g1] lt1] e1]; intros e2 W; simpl.
- apply W.
- apply IHe1.
  simpl in W; rewrite !Bool.Bool.andb_true_iff in W.
  apply (proj2 W).
Qed.

Lemma well_formed_e_disj_att_of_env :
  forall env1 env2, well_formed_e (env1 ++ env2) = true ->
                    ((att_of_env T env1) interS (att_of_env T env2)) =S= (emptysetS).
Proof.
intro env1; induction env1 as [ | [[sa1 g1] l1] env1];
  intros env2 We.
- unfold att_of_env; simpl; rewrite Fset.equal_spec; intro a.
  rewrite Fset.mem_inter, Fset.mem_empty; apply refl_equal.
- simpl app in We; rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff in We.
  assert (IH := IHenv1 env2 (proj2 We)).
  assert (H := proj2 (proj1 We)); unfold fresh_att_in_env in H.
  rewrite Fset.is_empty_spec in H.
  rewrite (Fset.equal_eq_1 _ _ _ _ (Fset.inter_eq_2 _ _ _ _ (att_of_env_app T _ _))) in H.
  unfold att_of_env in *.
  rewrite map_unfold.
  rewrite Fset.equal_spec; intro a.
  rewrite Fset.equal_spec in IH; assert (IHa := IH a).
  rewrite Fset.mem_inter, Fset.mem_empty, Bool.Bool.andb_false_iff in IHa.
  rewrite Fset.equal_spec in H; assert (Ha := H a).
  rewrite Fset.mem_inter, Fset.mem_union, Fset.mem_empty in Ha.
  rewrite Fset.Union_unfold, Fset.mem_inter, Fset.mem_union, Fset.mem_empty.
  destruct (a inS? sa1); simpl in IHa, Ha; simpl.
  + destruct IHa as [IHa | IHa].
    * rewrite IHa in Ha; simpl in Ha; apply Ha.
    * apply IHa.
  + destruct IHa as [IHa | IHa].
    * rewrite IHa; apply refl_equal.
    * rewrite IHa; apply Bool.Bool.andb_false_r.
Qed.

Lemma well_formed_e_disj_att :
  forall env1 sa g l env2, well_formed_e (env1 ++ (sa, g, l) :: env2) = true ->
                           fresh_att_in_env T sa (env1 ++ env2) = true.
Proof.
intros; unfold fresh_att_in_env.
rewrite (Fset.is_empty_eq _ _ _ (Fset.inter_eq_2 _ _ _ _ (att_of_env_app T _ _))).
rewrite (Fset.is_empty_eq _ _ _ (Fset.inter_distr_union_1 _ _ _ _)).
rewrite Fset.is_empty_union, !Fset.is_empty_spec, Bool.Bool.andb_true_iff.
assert (H1 := well_formed_e_disj_att_of_env _ _ H).
assert (H2 := well_formed_e_tail _ _ H); 
  rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff in H2; destruct H2 as [[_ H2] _].
split; [ | rewrite <- Fset.is_empty_spec; apply H2].
unfold att_of_env in *; simpl in H1; rewrite Fset.equal_spec in H1.
rewrite Fset.equal_spec; intro a; assert (H1a := H1 a).
rewrite Fset.mem_inter, Fset.mem_union, Fset.mem_empty, Bool.Bool.andb_false_iff, 
   Bool.Bool.orb_false_iff in H1a.
rewrite Fset.mem_inter, Fset.mem_empty.
destruct H1a as [H1a | H1a].
- rewrite H1a; apply Bool.Bool.andb_false_r.
- destruct H1a as [H1a _]; rewrite H1a; apply refl_equal.
Qed.

Lemma well_formed_env_t :
  forall env t, 
    well_formed_e env = true -> fresh_att_in_env T (labels T t) env = true -> 
                           well_formed_e (env_t _ env t) = true.
Proof.
intros env t We H; simpl.
rewrite Fset.equal_refl, We, H; apply refl_equal.
Qed.


Lemma well_formed_ft_tail :
  forall e1 e2 f, well_formed_ft e2 f = true -> well_formed_ft (e1 ++ e2) f = true.
Proof.
intro e1; induction e1 as [ | x1 e1]; intros e2 f Hf; [apply Hf | ].
simpl; destruct f as [c | a | f l]; [apply refl_equal | | ]; unfold well_formed_ft in *; 
  simpl; simpl in Hf.
- rewrite Oset.mem_bool_true_iff, in_extract_funterms in Hf.
  rewrite Oset.mem_bool_true_iff, in_extract_funterms, flat_map_app; 
    do 2 (apply in_or_app; right); assumption.
- rewrite Bool.Bool.orb_true_iff, Oset.mem_bool_true_iff, in_extract_funterms in Hf.
  rewrite Bool.Bool.orb_true_iff, Oset.mem_bool_true_iff, in_extract_funterms, flat_map_app.
  destruct Hf as [Hf | Hf].
  + left; do 2 (apply in_or_app; right); assumption.
  + right; rewrite !extract_funterms_app.
    rewrite forallb_forall in Hf; rewrite forallb_forall; intros a Ha.
    generalize (Hf a Ha); apply is_built_upon_ft_incl.
    intros x Hx; do 2 (apply in_or_app; right); assumption.
Qed.

Lemma well_formed_e_variables_ft_in_att_of_env :
  forall env, 
    well_formed_e env = true ->
    Fset.Union 
      (A T) (map (variables_ft T) (extract_funterms T (flat_map (groups_of_env_slice T) env)))
      subS att_of_env T env.
Proof.
induction env as [ | [[sa g] l] env]; intro We; simpl; [apply Fset.subset_refl | ].
rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff in We.
assert (IH := IHenv (proj2 We)); rewrite Fset.subset_spec in IH.
rewrite Fset.subset_spec; intros b Hb.
rewrite extract_funterms_app, map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff in Hb.
destruct Hb as [Hb | Hb].
- destruct g as [g | ].
  + rewrite Fset.mem_Union in Hb.
    destruct Hb as [s [Hs Hb]]; rewrite in_map_iff in Hs.
    destruct Hs as [f [Hs Hf]]; subst s; rewrite in_extract_funterms in Hf.
    rewrite Bool.Bool.andb_true_iff, forallb_forall in We.
    destruct We as [[[[W1 W2] W3] W4] W5].
    assert (Wf := W1 _ Hf); simpl in Wf; unfold well_formed_ft in Wf.
    assert (Kf := is_built_upon_ft_variables_ft_sub _ _ _ Wf); rewrite Fset.subset_spec in Kf.
    assert (Kb := Kf _ Hb); rewrite Fset.mem_Union in Kb. 
    destruct Kb as [s [Hs Kb]]; rewrite in_map_iff in Hs.
    destruct Hs as [f1 [Hs Hf1]]; subst s; simpl in Hf1.
    rewrite in_extract_funterms in Hf1; destruct (in_app_or _ _ _ Hf1) as [Kf1 | Kf1].
    * rewrite in_map_iff in Kf1.
      destruct Kf1 as [c [Kf1 Hc]]; injection Kf1; intro; subst f1; simpl in Kb.
      rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Kb; subst c.
      rewrite att_of_env_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; left.
      apply Fset.in_elements_mem; assumption.
    * rewrite att_of_env_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; right.
      apply IH; rewrite Fset.mem_Union.
      rewrite <- in_extract_funterms in Kf1.
      eexists; split; [rewrite in_map_iff; eexists; split; [ | apply Kf1 ] | ].
      -- apply refl_equal.
      -- assumption.
  + rewrite Fset.mem_Union in Hb.
    destruct Hb as [s [Hs Hb]]; rewrite in_map_iff in Hs.
    destruct Hs as [f1 [Hs Hf1]]; subst s.
    rewrite in_extract_funterms, in_map_iff in Hf1.
    destruct Hf1 as [a [Hf1 Ha]].
    injection Hf1; intro; subst f1.
    simpl in Hb; rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Hb; subst b.
    rewrite att_of_env_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; left.
    apply Fset.in_elements_mem; assumption.
- rewrite att_of_env_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; right.
  revert b Hb; rewrite <- Fset.subset_spec.
  apply IHenv; apply (proj2 We).
Qed.

Lemma well_formed_ft_variables_ft_env :
  forall env f, well_formed_e env = true -> well_formed_ft env f = true -> 
    variables_ft T f subS (att_of_env T env).
Proof.
intros env f We Wf.
unfold well_formed_ft in Wf.
apply (Fset.subset_trans _ _ _ _ (is_built_upon_ft_variables_ft_sub _ _ _ Wf)).
clear f Wf; induction env as [ | [[s1 g1] l1] env]; simpl; [apply Fset.subset_refl | ].
rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff in We.
assert (IH := IHenv (proj2 We)); clear IHenv; rewrite Fset.subset_spec in IH.
rewrite extract_funterms_app, map_app, Fset.subset_spec; intro a.
rewrite Fset.mem_Union_app, Bool.Bool.orb_true_iff, att_of_env_unfold, Fset.mem_union.
intros [Ha | Ha]; [ | rewrite (IH a Ha); apply Bool.Bool.orb_true_r].
rewrite Fset.mem_Union in Ha; destruct Ha as [s [Hs Ha]].
rewrite in_map_iff in Hs; destruct Hs as [f [Hs Hf]]; subst s.
rewrite in_extract_funterms in Hf.
destruct g1 as [g1 | ].
- rewrite !Bool.Bool.andb_true_iff, !forallb_forall in We.
  destruct We as [[[[W1 W2] W3] W4] W5].
  assert (Wf := W1 _ Hf).
  unfold well_formed_ag, well_formed_ft in Wf; simpl in Wf.
  assert (Aux := is_built_upon_ft_variables_ft_sub _ _ _ Wf); rewrite Fset.subset_spec in Aux.
  assert (Ka := Aux _ Ha); 
    rewrite extract_funterms_app, map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff in Ka.
  destruct Ka as [Ka | Ka].
  + rewrite Bool.Bool.orb_true_iff; left.
    rewrite <- Ka; rewrite extract_funterms_A_Expr, map_map; simpl; apply sym_eq.
    apply Fset.mem_eq_2; apply Fset.Union_map_singleton.
  + rewrite (IH _ Ka); apply Bool.Bool.orb_true_r.
- rewrite in_map_iff in Hf; destruct Hf as [_a [_Ha Hf]].
  injection _Ha; intro; subst f; simpl in Ha.
  rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Ha; subst _a.
  rewrite Bool.Bool.orb_true_iff; left.
  apply Fset.in_elements_mem; assumption.
Qed.

Lemma well_formed_ag_variables_ag_env :
  forall env f, well_formed_e env = true -> well_formed_ag env f = true -> 
    variables_ag T f subS (att_of_env T env).
Proof.
intros env f.
set (n := size_aggterm T f); assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert env f Hn; induction n as [ | n]; intros env f Hn Wenv Wf;
  [destruct f; inversion Hn | ].
destruct f as [f | a f | a l]; simpl.
- apply (well_formed_ft_variables_ft_env _ _ Wenv Wf).
- simpl in Wf.
  case_eq (Fset.is_empty (A T) (variables_ft T f)); intro Hf; rewrite Hf in Wf.
  + rewrite Fset.is_empty_spec in Hf. 
    rewrite (Fset.subset_eq_1 _ _ _ _ Hf), Fset.subset_spec; intros b Hb.
    rewrite Fset.mem_empty in Hb; discriminate Hb.
  + rewrite Bool.Bool.orb_false_l in Wf.
    case_eq (find_eval_env T env f); [ | intro H; rewrite H in Wf; discriminate Wf].
    clear Wf; intros env2 Henv2. 
    destruct (find_eval_env_is_tail _ _ _ _ Henv2) as [env1 Henv]; rewrite Henv.
    apply Fset.subset_trans with (att_of_env T env2);
      [ | rewrite Fset.subset_spec; intros b Hb; 
          rewrite (Fset.mem_eq_2 _ _ _ (att_of_env_app _ _ _)), Fset.mem_union, Hb;
          apply Bool.Bool.orb_true_r].
    destruct env2 as [ | [[sa2 g2] l2] env2];
      [rewrite Fset.is_empty_spec, (find_eval_env_some_nil _ _ _ Henv2) in Hf; discriminate Hf | ].
    assert (H := find_eval_env_some _ _ _ _ _ _ _ Henv2); unfold is_a_suitable_env in H.
    assert (K := is_built_upon_ft_variables_ft_sub _ _ _ H).
    apply (Fset.subset_trans _ _ _ _ K).    
    rewrite Fset.subset_spec; intros b Hb.
    rewrite map_app, map_map, Fset.mem_Union_app, Bool.Bool.orb_true_iff in Hb.
    destruct Hb as [Hb | Hb].
    * simpl in Hb; rewrite (Fset.mem_eq_2 _ _ _ (Fset.Union_map_singleton _ _)) in Hb.
      rewrite att_of_env_unfold, Fset.mem_union, Hb; apply refl_equal.
    * rewrite att_of_env_unfold, Fset.mem_union, Bool.Bool.orb_true_iff; right.
      revert b Hb.
      rewrite <- Fset.subset_spec; apply well_formed_e_variables_ft_in_att_of_env.
      rewrite Henv in Wenv; assert (Wenv' := well_formed_e_tail _ _ Wenv).
      rewrite well_formed_e_unfold, Bool.Bool.andb_true_iff in Wenv'; apply (proj2 Wenv').
- rewrite Fset.subset_spec; intros b Hb.
  simpl in Hb; rewrite Fset.mem_Union in Hb.
  destruct Hb as [s [Hs Hb]]; rewrite in_map_iff in Hs.
  destruct Hs as [x [Hs Hx]]; subst s.
  revert b Hb; rewrite <- Fset.subset_spec; apply IHn.
  + simpl in Hn; refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply in_list_size; assumption.
  + assumption.
  + simpl in Wf; rewrite forallb_forall in Wf; apply Wf; assumption.
Qed.

Lemma well_formed_e_well_formed_e :
  forall s g l env t, 
    labels T t =S= s ->
    well_formed_e ((s, Group_By T g, l) :: env) = true ->
    well_formed_e
      ((s, Group_By T (map (fun a => A_Expr T (F_Dot T a)) ({{{s}}})),
        t :: nil) :: env) = true.
Proof.
intros s g l env t Ht We.
rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff, !forallb_forall in We.
rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff, !forallb_forall; repeat split.
- intros x Hx; rewrite in_map_iff in Hx.
  destruct Hx as [a [Hx Ha]]; subst x; simpl.
  unfold well_formed_ft; simpl; rewrite extract_funterms_app, Oset.mem_bool_app.
  rewrite Bool.Bool.orb_true_iff; left.
  rewrite extract_funterms_A_Expr, Oset.mem_bool_true_iff, in_map_iff.
  eexists; split; [ | apply Ha]; apply refl_equal.
- intros x [Hx | Hx]; [ | contradiction Hx].
  subst x; rewrite forallb_forall; intros.
  apply Oset.eq_bool_refl.
- intros x [Hx | Hx]; [ | contradiction Hx].
  subst x; assumption.
- apply (proj2 (proj1 We)).
- apply (proj2 We).
Qed.

Lemma well_formed_e_well_formed_e_default :
  forall s g l env, 
  well_formed_e ((s, Group_By T g, l) :: env) = true ->
  well_formed_e
    ((s, Group_By T (map (fun a => A_Expr T (F_Dot T a)) ({{{s}}})),
     default_tuple T s :: nil) :: env) = true.
Proof.
intros s g l env We.
apply well_formed_e_well_formed_e with g l; [ | assumption].
unfold default_tuple.
apply labels_mk_tuple.
Qed.

Lemma well_formed_ag_variables_ag_env_alt :
  forall slc env f,
    well_formed_e (slc :: env) = true -> In f (groups_of_env_slice T slc) ->
    variables_ag T f subS att_of_env T (slc :: env).
Proof.
intros [[s g] l] env f We Hf.
destruct g as [g | ].
- assert (We' := well_formed_e_well_formed_e_default _ _ _ _ We).
  simpl in We, Hf.
  rewrite !Bool.Bool.andb_true_iff, forallb_forall in We.
  assert (Wf := proj1 (proj1 (proj1 (proj1 We))) _ Hf).
  rewrite <- (well_formed_ag_variables_ag_env _ _ We' Wf).
  apply Fset.subset_eq_2; apply Fset.equal_refl.
- simpl in Hf; rewrite in_map_iff in Hf.
  destruct Hf as [a [Hf Ha]]; subst f; simpl.
  rewrite att_of_env_unfold, Fset.subset_spec; intros b Hb.
  rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Hb; subst b.
  rewrite Fset.mem_union, Bool.Bool.orb_true_iff; left.
  apply Fset.in_elements_mem; assumption.
Qed.

Lemma well_formed_ft_in_tail_fresh_att_in_env :
forall env1 env2 f, 
  well_formed_e (env1 ++ env2) = true -> well_formed_ft env2 f = true ->
  fresh_att_in_env T (variables_ft T f) env1 = true.
Proof.
intros env1 env2 f We Wf.
assert (H := well_formed_ft_variables_ft_env env2 f (well_formed_e_tail _ _ We) Wf).
assert (K := well_formed_e_disj_att_of_env _ _ We).
rewrite Fset.subset_spec in H; rewrite Fset.equal_spec in K.
unfold fresh_att_in_env; rewrite Fset.is_empty_spec, Fset.equal_spec; intro b.
rewrite Fset.mem_inter.
case_eq (b inS? variables_ft T f); intro Hb; [ | rewrite Fset.mem_empty; apply refl_equal].
assert (Kb := K b); rewrite Fset.mem_inter, (H b Hb), Bool.Bool.andb_true_r in Kb.
apply Kb.
Qed.

Lemma well_formed_ft_proj_env_t :
  forall s g l env1 env2, well_formed_e (env1 ++ (s, g, l) :: env2) = true ->
  forall x d, labels T d =S= s -> 
    well_formed_ft (env1 ++ (s, g, l) :: env2) x = true ->
    well_formed_ft (env1 ++ env_t T env2 d) x = true.
Proof.
intros s g l env1 env2 W x d Hd; unfold well_formed_ft.
apply is_built_upon_ft_trans; clear x; intros x; unfold env_t.
rewrite !flat_map_app, !extract_funterms_app; simpl.
rewrite (Fset.elements_spec1 _ _ _ Hd).
destruct g as [g | ]; [ | apply in_is_built_upon_ft].
intro Hx; destruct (in_app_or _ _ _ Hx) as [Kx | Kx].
- apply in_is_built_upon_ft; apply in_or_app; left; assumption.
- rewrite extract_funterms_app in Kx; destruct (in_app_or _ _ _ Kx) as [Jx | Jx].
  + rewrite in_extract_funterms in Jx.
    assert (W' := well_formed_e_tail _ _ W).
    simpl in W'; rewrite !Bool.Bool.andb_true_iff, forallb_forall in W'.
    assert (Wx := proj1 (proj1 (proj1 (proj1 W'))) _ Jx); simpl in Wx;
      unfold well_formed_ft in Wx; revert Wx; apply is_built_upon_ft_incl.
    intros f Hf; unfold env_t in Hf; simpl in Hf.
    apply in_or_app; right; assumption.
  + apply in_is_built_upon_ft; apply in_or_app; right.
    rewrite extract_funterms_app; apply in_or_app; right; assumption.
Qed.

(*Lemma well_formed_ag_proj_env_t :
  forall s g l env1 env2, well_formed_e (env1 ++ (s, g, l) :: env2) = true ->
  forall x d, labels T d =S= s -> 
    well_formed_ag (env1 ++ (s, g, l) :: env2) x = true ->
    well_formed_ag (env1 ++ env_t T env2 d) x = true.
Proof.
intros s g l env1 env2 W x d Hd.
set (n := size_aggterm T x); assert (Hn := le_n n); unfold n at 1 in Hn.
clearbody n; revert x Hn; induction n as [ | n]; intros x Hn; [destruct x; inversion Hn | ].
destruct x as [f | a f | fc lf]; simpl.
- apply well_formed_ft_proj_env_t; assumption.
- case_eq (Fset.is_empty (A T) (variables_ft T f)); intro Hf; [exact (fun _ => refl_equal _) | ].
  rewrite !Bool.Bool.orb_false_l.
  case_eq (find_eval_env T (env1 ++ (s, g, l) :: env2) f); 
    [ | intros _ Abs; discriminate Abs].
  intros e He _.
  clear IHn; induction env1 as [ | [[sa1 g1] l1] env1]; simpl in He; simpl.
  + case_eq (find_eval_env T env2 f); [intros; apply refl_equal | ].
    intro Ke; rewrite Ke in He.
    case_eq (is_a_suitable_env T s env2 f); intro Jf; rewrite Jf in He; 
      [injection He; clear He; intro; subst e | discriminate He].
    assert (Aux : is_a_suitable_env T (labels T d) env2 (A_agg T a f) = true).
    {
    unfold is_a_suitable_env in *; simpl in Jf; simpl.
    rewrite (Fset.elements_spec1 _ _ _ Hd).
    apply Jf.
    }
    rewrite Aux; apply refl_equal.
  + simpl in W; rewrite !Bool.Bool.andb_true_iff in W.
    case_eq (find_eval_env T (env1 ++ (s, g, l) :: env2) (A_agg T a f)).
    * intros e' He'; rewrite He' in He; injection He; clear He; intro; subst e'.
      assert (IH := IHenv1 (proj2 W) He').
      destruct (find_eval_env T (env1 ++ env_t T env2 d) (A_agg T a f));
      [apply refl_equal | discriminate IH].
    * intro Ke; rewrite Ke in He.
      case_eq (is_a_suitable_env T sa1 (env1 ++ (s, g, l) :: env2) (A_agg T a f)); intro Kf;
        rewrite Kf in He; [ | discriminate He].
      injection He; clear He; intro; subst e.
      case_eq (find_eval_env T (env1 ++ env_t T env2 d) (A_agg T a f));
        [intros; apply refl_equal | intro Jf].
      revert Kf; unfold is_a_suitable_env; simpl.
      rewrite !Oset.mem_bool_app.
      case_eq (Oset.mem_bool (OAggT T) (A_agg T a f)
                             (map (fun a0 : attribute T => A_Expr T (F_Dot T a0)) ({{{sa1}}})));
        [intros; apply refl_equal | ].
      intros _; rewrite !Bool.Bool.orb_false_l, !flat_map_app, !Oset.mem_bool_app.
      case_eq (Oset.mem_bool (OAggT T) (A_agg T a f) (flat_map (groups_of_env_slice T) env1));
        intro Lf; unfold env_slice in *; rewrite Lf; [intros; apply refl_equal | ].
      rewrite !Bool.Bool.orb_false_l; destruct g as [g | ]; simpl.
      -- rewrite Oset.mem_bool_app; case_eq (Oset.mem_bool (OAggT T) (A_agg T a f) g); intro Mf.
         ++ intros _; rewrite Oset.mem_bool_true_iff in Mf.
            destruct W as [W1 W2]; assert (W' := well_formed_e_tail _ _ W2); simpl in W'.
            rewrite !Bool.Bool.andb_true_iff, forallb_forall in W'.
            assert (Wf:= proj1 (proj1 (proj1 (proj1 W'))) _ Mf); simpl in Wf.
            case_eq (Fset.is_empty (A T) (variables_ft T f)); intro Nf;
              [rewrite (empty_vars_is_built_upon_ft _ _ Nf), Bool.Bool.orb_true_r; 
               apply refl_equal | ].
            rewrite Nf, Bool.Bool.orb_false_l in Wf.
            assert (Pf := find_eval_env_none _ _ _ _ Ke); simpl in Pf.
            case_eq (find_eval_env T env2 (A_agg T a f));
              [intros e He; rewrite He in Pf; discriminate Pf | intro Qf; rewrite Qf in Wf].
            case_eq (is_a_suitable_env T (labels T (default_tuple T s)) env2 (A_agg T a f));
              intro Rf; rewrite Rf in Wf; [ | discriminate Wf].
            unfold is_a_suitable_env in Rf; simpl in Rf; rewrite Bool.Bool.orb_true_iff in Rf.
            unfold default_tuple in Rf; 
              rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)) in Rf.
            rewrite (Fset.elements_spec1 _ _ _ Hd).
            destruct Rf as [Rf | Rf]; [unfold env_slice in *; rewrite Rf; apply refl_equal | ].
            assert (Aux : is_built_upon_ft T
               (extract_funterms T
                (map (fun a0 : attribute T => A_Expr T (F_Dot T a0)) ({{{sa1}}}) ++
                 flat_map (groups_of_env_slice T) env1 ++
                 map (fun a0 : attribute T => A_Expr T (F_Dot T a0))
                   ({{{s}}}) ++ flat_map (groups_of_env_slice T) env2))
               f = true).
            {
              revert Rf; apply is_built_upon_ft_incl.
              intros x Hx; rewrite in_extract_funterms in Hx; rewrite in_extract_funterms.
              do 2 (apply in_or_app; right); apply Hx.
            }
            unfold env_slice in *; rewrite Aux, Bool.Bool.orb_true_r; apply refl_equal.
         ++ rewrite Bool.Bool.orb_false_l, Oset.mem_bool_app.
            case_eq (Oset.mem_bool 
                       (OAggT T) (A_agg T a f) (flat_map (groups_of_env_slice T) env2));
              intro Nf; unfold env_slice in *;  rewrite Nf;
                [intros _; rewrite Bool.Bool.orb_true_r; apply refl_equal | ].
            rewrite Bool.Bool.orb_false_l, Bool.Bool.orb_false_r; intro Pf.
            assert (Aux : is_built_upon_ft T
               (extract_funterms T
                (map (fun a0 : attribute T => A_Expr T (F_Dot T a0)) ({{{sa1}}}) ++
                 flat_map (groups_of_env_slice T) env1 ++
                 map (fun a0 : attribute T => A_Expr T (F_Dot T a0))
                   ({{{s}}}) ++ flat_map (groups_of_env_slice T) env2))
               f = true).
            {
              revert Pf; apply is_built_upon_ft_trans.
              intros x Hx; rewrite in_extract_funterms in Hx.
              destruct (in_app_or _ _ _ Hx) as [Kx | Kx].
              - apply in_is_built_upon_ft;
                  rewrite in_extract_funterms; apply in_or_app; left; assumption.
              - destruct (in_app_or _ _ _ Kx) as [Jx | Jx].
                + apply in_is_built_upon_ft;
                    rewrite in_extract_funterms; apply in_or_app; right;
                      apply in_or_app; left; assumption.
                + destruct (in_app_or _ _ _ Jx) as [Lx | Lx].
                  * assert (W' := well_formed_e_tail _ _ (proj2 W)); simpl in W'.
                    rewrite !Bool.Bool.andb_true_iff, forallb_forall in W'.
                    assert (Wx := proj1 (proj1 (proj1 (proj1 W'))) _ Lx); simpl in Wx;
                      unfold well_formed_ft in Wx; revert Wx; apply is_built_upon_ft_incl.
                    intros y Hy; rewrite in_extract_funterms in Hy; rewrite in_extract_funterms.
                    do 2 (apply in_or_app; right).
                    unfold env_t in Hy; simpl in Hy.
                    unfold default_tuple in Hy; 
                      rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)) in Hy; 
                      apply Hy.
                  * apply in_is_built_upon_ft;
                      rewrite in_extract_funterms; do 3 (apply in_or_app; right); assumption.
            }
            unfold env_slice in *; 
              rewrite (Fset.elements_spec1 _ _ _ Hd), Aux, Bool.Bool.orb_true_r.
            apply refl_equal.
      -- rewrite !Oset.mem_bool_app, (Fset.elements_spec1 _ _ _ Hd);
         case_eq (Oset.mem_bool 
                      (OAggT T) (A_agg T a f) 
                      (map (fun a0 : attribute T => A_Expr T (F_Dot T a0)) ({{{s}}})));
           intro Mf; [intros; apply refl_equal | ].
         intro Nf; rewrite Nf; apply refl_equal.
- rewrite !forallb_forall; intros H x Hx.
  apply IHn.
  + simpl in Hn; refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply in_list_size; assumption.
  + apply H; assumption.
Qed.
*)
(*
Lemma well_formed_ft_extend_env_slice :
  forall env1 env2 s sa g l,
    List.length s = List.length g ->
    all_diff s ->
    let env := (env1 ++ (sa, Group_By T g, l) :: env2) in
    (forall a, 
        In a s -> 
        a inS Fset.Union (A T) (List.map (fun x => match x with (sa, _, _) => sa end) env) -> 
        False) ->
    well_formed_e env = true ->
    forall x,
    well_formed_ft env x = true ->
    well_formed_ft
      (env1 ++ extend_env_slice T env2 s (sa, Group_By T g, l) :: env2) x = true.
Proof.
intros env1 env2 s sa g l L D env H W x;
  set (n := size_funterm T x); assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert env1 env2 s sa g l L D env H W x Hn; induction n as [ | n];
  intros env1 env2 s sa g l L D env H W x Hn Wx; [destruct x; inversion Hn | ].
destruct x as [c | a | f k].
- apply refl_equal.
- unfold well_formed_ft in Wx; simpl in Wx; rewrite Oset.mem_bool_true_iff in Wx.
  rewrite in_extract_funterms, in_flat_map in Wx.
  destruct Wx as [x [Hx Kx]].
  unfold env in Hx.
  unfold well_formed_ft; simpl; 
      rewrite Oset.mem_bool_true_iff, in_extract_funterms, in_flat_map.
  destruct (in_app_or _ _ _ Hx) as [Jx | Jx].
  + eexists; split; [apply in_or_app; left; apply Jx | ]; assumption.
  + simpl in Jx; destruct Jx as [Jx | Jx].
    * unfold env in W; assert (W' := well_formed_e_tail _ _ W); simpl in W'.
      rewrite !Bool.Bool.andb_true_iff, forallb_forall in W'.
      destruct W' as [[[[Wg _] _] _] _].
      subst x; simpl in Kx.
      assert (Wa := Wg _ Kx); simpl in Wa.
      unfold well_formed_ft in Wa; simpl in Wa.
      rewrite Oset.mem_bool_true_iff, in_extract_funterms in Wa.
      destruct (in_app_or _ _ _ Wa) as [Ha | Ha].
      -- rewrite 
    * subst x; simpl in Kx.
      eexists; split; [apply in_or_app; right; left; apply refl_equal | ].
      simpl.
; unfold well_formed_ft; induction env1 as [ | [[sa1 g1] l1] env1];
  intros env2 s sa g l L D H W x Wx; rewrite extend_env_slice_unfold.
- rewrite app_nil in Wx; rewrite app_nil.
  
Qed.

Lemma well_formed_ag_extend_env_slice :
  forall env1 env2 s sa g l,
    List.length s = List.length g ->
    all_diff s ->
    let env := (env1 ++ (sa, Group_By T g, l) :: env2) in
    (forall a, 
        In a s -> 
        a inS Fset.Union (A T) (List.map (fun x => match x with (sa, _, _) => sa end) env) -> 
        False) ->
    well_formed_e env = true ->
    forall x,
    well_formed_ag env x = true ->
    well_formed_ag
      (env1 ++ extend_env_slice T env2 s (sa, Group_By T g, l) :: env2) x = true.

Lemma well_formed_e_extend_env_slice :
  forall env1 env2 s sa g l,
    List.length s = List.length g ->
    all_diff s ->
    let env := (env1 ++ (sa, Group_By T g, l) :: env2) in
    (forall a, 
        In a s -> 
        a inS Fset.Union (A T) (List.map (fun x => match x with (sa, _, _) => sa end) env) -> 
        False) ->
    well_formed_e env = true ->
    well_formed_e (env1 ++ (extend_env_slice T env2 s (sa, Group_By T g, l)) :: env2) = true.
Proof.
intro env1; induction env1 as [ | [[sa1 g1] l1] env1];
  intros env2 s sa g l L D env H W.
- unfold env in *; 
    rewrite app_nil, well_formed_e_unfold, !Bool.Bool.andb_true_iff, forallb_forall in W.
  destruct W as [[[[W1 W2] W3] W4] W5].
  rewrite app_nil, extend_env_slice_unfold, well_formed_e_unfold.
  rewrite !Bool.Bool.andb_true_iff, forallb_forall; repeat split.
  + intros _a Ha; rewrite in_map_iff in Ha; destruct Ha as [a [_Ha Ha]]; subst _a; simpl.
    unfold well_formed_ft, env_t; subst; apply in_is_built_upon_ft.
    rewrite in_extract_funterms, in_flat_map; eexists; split; [left; apply refl_equal | ]; simpl.
    rewrite in_map_iff; eexists; split; [apply refl_equal | ].
    unfold default_tuple; rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
    apply Fset.mem_in_elements; rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
    rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff; assumption.
  + destruct l as [ | t1 l]; [discriminate W2 | ].
    rewrite (map_unfold _ (_ :: _)) at 1.
    rewrite forallb_forall.
    intros x Hx; rewrite in_map_iff in Hx; destruct Hx as [t [_Hx Ht]]; subst x.
    rewrite forallb_forall; intros e He; rewrite in_map_iff in He.
    destruct He as [a [_Ha Ha]]; subst e.
    rewrite Oset.eq_bool_true_iff; simpl.
    rewrite !(Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
    apply if_eq; [apply refl_equal | | intros; apply refl_equal].
    intro Ka; rewrite !dot_mk_tuple, !Ka.
    unfold pair_of_select, id_renaming; rewrite map_app, !map_map, !Oset.find_app.
    case_eq (Oset.find (OAtt T) a (map (fun x => (x, A_Expr T (F_Dot T x))) ({{{sa}}}))).
    * intros e He; assert (Ke := Oset.find_some _ _ _ He).
      rewrite in_map_iff in Ke; destruct Ke as [_b [_Hb Ke]].
      injection _Hb; do 2 intro; subst _b e; simpl.
      apply False_rec; apply (H a Ha).
      rewrite Fset.mem_Union; eexists; split; [left; apply refl_equal | ].
      apply Fset.in_elements_mem; assumption.
    * intro Ja; case_eq (Oset.find
                           (OAtt T) a
                           (map
                              (fun x : attribute T * aggterm T =>
                                 match (let (a0, e) := x in Select_As T e a0) with
                                 | Select_As _ e a0 => (a0, e)
                                 end) (combine s g)));
      [intros e He | intros; apply refl_equal].
      rewrite forallb_forall in W2; generalize (W2 _ Ht); rewrite forallb_forall.
      intros Kt; assert (Ke := Kt e); rewrite Oset.eq_bool_true_iff in Ke; apply Ke.
      assert (Je := Oset.find_some _ _ _ He); rewrite in_map_iff in Je.
      destruct Je as [[_a _e] [_Je Je]]; injection _Je; do 2 intro; subst _a _e.
      revert Je; apply in_combine_r.
  + rewrite forallb_forall; intros x Hx; rewrite in_map_iff in Hx.
    destruct Hx as [t [_Hx Ht]]; subst x.
    rewrite (Fset.equal_eq_1 _ _ _ _ (labels_projection _ _ _)).
    unfold id_renaming; rewrite map_app, !map_map, map_id; [ | intros; trivial].
    rewrite (Fset.equal_eq_1 _ _ _ _ (Fset.mk_set_app _ _ _)).
    apply Fset.union_eq; [apply Fset.mk_set_idem | ].
    rewrite Fset.equal_spec; intro a; do 2 apply f_equal.
    revert L; generalize s g; intro s1; 
      induction s1 as [ | a1 s1]; intros g1 L; [apply refl_equal | ].
    destruct g1 as [ | x1 g1]; [discriminate L | ].
    simpl; apply f_equal; apply IHs1; injection L; exact (fun h => h).
  + rewrite Fset.is_empty_spec, Fset.equal_spec; intro a.
    rewrite Fset.mem_inter, Fset.mem_union, Fset.mem_empty.
    rewrite Bool.Bool.andb_false_iff, Bool.Bool.orb_false_iff.
    case_eq (a inS? sa); intro Ha.
    * right.
      rewrite Fset.is_empty_spec, Fset.equal_spec in W4.
      generalize (W4 a); rewrite Fset.mem_inter, Ha, Bool.Bool.andb_true_l, Fset.mem_empty.
      exact (fun h => h).
    * case_eq (a inS? Fset.mk_set (A T) s); intro Ka; [ | left; split; trivial].
      right.
      rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff in Ka.
      rewrite <- not_true_iff_false; intro Ja; rewrite Fset.mem_Union in Ja.
      destruct Ja as [_s [_Hs Ja]].
      rewrite in_map_iff in _Hs; destruct _Hs as [[[_sa _g] _l] [_Hs Hs]]; subst _sa.
      apply (H _ Ka); rewrite Fset.mem_Union.
      eexists; split; [ | apply Ja].
      rewrite in_map_iff; eexists; split; [ | right; apply Hs].
      apply refl_equal.
  + assumption.
- unfold env in W; rewrite <- app_comm_cons, well_formed_e_unfold in W.
  rewrite !Bool.Bool.andb_true_iff in W.
  destruct W as [[[W1 W2] W3] W4].
  rewrite <- app_comm_cons, well_formed_e_unfold, !Bool.Bool.andb_true_iff; repeat split.
  + destruct g1 as [g1 | ]; [rewrite Bool.Bool.andb_true_iff in W1; destruct W1 as [W1 W1'];
                             rewrite Bool.Bool.andb_true_iff; split | ].
    * rewrite forallb_forall in W1.
      rewrite forallb_forall; intros x Hx.
      generalize (W1 _ Hx).
      
rewrite !Fset.mem_mk_set.
      rewrite Oset.find_map.

      apply projection_eq.
env_tac.
  progress cbv beta iota zeta.
  
Qed.
*)

Lemma well_formed_e_proj_env_t :
  forall s g l env, 
    well_formed_e ((s, g, l) :: env) = true ->
    forall t1 k, quicksort (OTuple T) l = t1 :: k ->
    well_formed_e (env_t T env t1) = true.
Proof.
intros s g l env W; unfold env_t; intros t1 k Ql;
  simpl in W; rewrite !Bool.Bool.andb_true_iff in W; simpl;
  destruct W as [[[W1 W2] W3] W4].
rewrite Fset.equal_refl, W4, Bool.Bool.andb_true_l, Bool.Bool.andb_true_r.
rewrite <- W3; apply Fset.is_empty_eq.
apply Fset.inter_eq_1.
rewrite forallb_forall in W2; apply W2.
rewrite (In_quicksort (OTuple T)), Ql; left; apply refl_equal.
Qed.
(*
- assert (IH := IHenv1 W4); clear IHenv1.
  rewrite W2, (IH _ _ Ql), !Bool.Bool.andb_true_r, !Bool.Bool.andb_true_iff; repeat split.
  + rewrite forallb_forall in W1; rewrite forallb_forall.
    intros x Hx; generalize (proj1 W1 _ Hx); unfold env_t.
    rewrite !app_comm_cons.
    apply well_formed_ag_proj_env_t; simpl.
    * rewrite Fset.equal_refl, W4, Bool.Bool.andb_true_r; simpl.
      rewrite <- W3; apply Fset.is_empty_eq; apply Fset.inter_eq_1.
      unfold default_tuple; apply labels_mk_tuple.
    * assert (W := well_formed_e_tail _ _ W4); simpl in W.
      rewrite !Bool.Bool.andb_true_iff, forallb_forall in W.
      apply (proj2 (proj1 (proj1 W))); rewrite (In_quicksort (OTuple T)), Ql; left.
      apply refl_equal.
  + destruct W1 as [W1 W1'].
    destruct l1 as [ | u1 l1]; [discriminate W1' | ].
    rewrite forallb_forall; intros x Hx; rewrite forallb_forall; intros y Hy.
    rewrite forallb_forall in W1; assert (Wy := W1 _ Hy); unfold env_t in *.
    rewrite Oset.eq_bool_true_iff.
    rewrite forallb_forall in W1'; assert (W := W1' _ Hx).
    rewrite forallb_forall in W; assert (W' := W _ Hy).
    rewrite Oset.eq_bool_true_iff in W'.
    revert W'; unfold env_t; clear Hy; 
      set (n := size_aggterm T y); assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
    revert y Hn Wy; induction n as [ | n]; intros y Hn Wy H; [destruct y; inversion Hn | ].
    destruct y as [f | a f | f _k]; simpl.
    * assert (Kx := interp_funterm_homogeneous 
                      T s g l (labels T t1) (@Group_Fine _) t1 k 
                      ((labels T x, Group_Fine T, x :: nil) :: env1) env2 f Ql).
      simpl in H, Kx; simpl; rewrite Kx in H.
      assert (Ku1 := interp_funterm_homogeneous 
                      T s g l (labels T t1) (@Group_Fine _) t1 k 
                      ((labels T u1, Group_Fine T, u1 :: nil) :: env1) env2 f Ql).
      simpl in Ku1; simpl; rewrite Ku1 in H; apply H.
    * apply f_equal.
      case_eq (Fset.is_empty (A T) (variables_ft T f)); intro Hf.
      -- unfold unfold_env_slice; rewrite !map_map, !quicksort_1; simpl.
         apply f_equal2; [ | apply refl_equal].
         rewrite !(interp_cst_funterm _ (_ :: _)); trivial.
      -- case_eq (find_eval_env 
                    T (env1 ++ (labels T t1, Group_Fine T, t1 :: nil) :: env2) (A_agg T a f));
           [intros; apply refl_equal | intro Kf].
         assert (Aux : is_a_suitable_env 
                         T (labels T x) (env1 ++ (labels T t1, Group_Fine T, t1 :: nil) :: env2)
                         (A_agg T a f) = 
                       is_a_suitable_env 
                         T (labels T u1) (env1 ++ (labels T t1, Group_Fine T, t1 :: nil) :: env2)
                         (A_agg T a f)).
         {
           unfold is_a_suitable_env; simpl.
           rewrite forallb_forall in W2.
           rewrite !(Fset.elements_spec1 _ _ _ (W2 _ Hx)),
             !(Fset.elements_spec1 _ _ _ (W2 _ (or_introl _ (refl_equal _)))).
           apply f_equal.
           rewrite !flat_map_app; simpl; apply refl_equal.
         }
         rewrite <- Aux.
         case_eq (is_a_suitable_env 
                    T (labels T x) (env1 ++ (labels T t1, Group_Fine T, t1 :: nil) :: env2)
                    (A_agg T a f)); intro Jf; [ | apply refl_equal].
         unfold unfold_env_slice; rewrite !map_map, !quicksort_1; simpl.
         apply f_equal2; [ | apply refl_equal].
         simpl in H; rewrite Hf in H.
         
         rewrite !interp_aggterm_unfold, !Hf in H.
         apply H.

         ++ intros e He; apply refl_equal.
         apply empty_vars_is_built_upon_ft
      rewrite <- map_eq. 
      
 apply refl_equal.
 in H.
      
simpl in Hn; generalize (le_S_n _ _ Hn); clear Hn; intro Hn.
      clear IHn; revert f Hn Wy; induction n as [ | n]; intros f Hn Wy; 
           [destruct f; inversion Hn | ].
      -- destruct f as [c | a | f _k]; [intros; apply refl_equal | | ]; intro H.
         ++ simpl in H; simpl.
            rewrite forallb_forall in W2.
            rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Hx)), 
              (Fset.mem_eq_2 _ _ _ (W2 _ (or_introl _ (refl_equal _)))) in H.
            rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Hx)), 
              (Fset.mem_eq_2 _ _ _ (W2 _ (or_introl _ (refl_equal _)))).
            destruct (a inS? sa1); [apply H | apply refl_equal].
         ++ simpl.
revert W2.
++ simpl in Wy; unfold well_formed_ft in Wy; simpl in Wy.
               rewrite extract_funterms_app, Oset.mem_bool_app in Wy; simpl.
               case_eq (Oset.mem_bool 
                          (OFun T) (F_Dot T a)
                          (extract_funterms 
                             T (map (fun a : attribute T => A_Expr T (F_Dot T a))
                                    ({{{labels T (default_tuple T sa1)}}})))); intro Ha;
                 [ | rewrite Ha, Bool.Bool.orb_false_l in Wy].
               --- rewrite Oset.mem_bool_true_iff, in_extract_funterms, in_map_iff in Ha.
                   destruct Ha as [_a [_Ha Ha]]; injection _Ha; intro; subst _a.
                   unfold default_tuple in Ha; 
                     rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)) in Ha.
                   simpl; rewrite forallb_forall in W2; rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Hx)).
                   rewrite (Fset.in_elements_mem _ _ _ Ha); apply refl_equal.
               --- simpl; case_eq (a inS? labels T x); [intros; apply refl_equal | intro Ka].
                   
               rewrite Oset.mem_bool_true_iff, in_extract_funterms in Wy.
               simpl; case_eq (a inS? labels T x); intro Ha; [apply refl_equal | ].
                              
               unfold interp_dot;
simpl in H; simpl.
            rewrite forallb_forall in W2; 
              rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Hx)), 
                (Fset.mem_eq_2 _ _ _ (W2 _ (or_introl _ (refl_equal _)))) in H.
            rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Hx)), 
                (Fset.mem_eq_2 _ _ _ (W2 _ (or_introl _ (refl_equal _)))).
            case_eq (a inS? sa1); intro Ha; rewrite Ha in H; [apply H | ].
            apply refl_equal.
         ++ simpl in H.
            ; apply f_equal.
            
    unfold env_t in W'; unfold env_t.
    r
    * 
    assumption.
; unfold env_t in Kx; unfold env_t.
    clear Hx; revert Kx.
 well_formed_ag
    ((labels T (default_tuple T sa1), Group_Fine T, default_tuple T sa1 :: nil)
     :: env1 ++ (s, g, l) :: env2) x = true ->
  well_formed_ag
    ((labels T (default_tuple T sa1), Group_Fine T, default_tuple T sa1 :: nil)
     :: env1 ++ (labels T (default_tuple T s), Group_Fine T, default_tuple T s :: nil) :: env2) x =
  true
 set (n := size_aggterm T x); assert (Hn := le_n n); unfold n at 1 in Hn.
    clearbody n; revert x Hn; induction n as [ | n]; intros x Hn; [destruct x; inversion Hn | ].
    destruct x as [f | a f | fc lf]; simpl; 
      unfold well_formed_ag, well_formed_ft; unfold env_t in *.
    * apply is_built_upon_ft_trans; intros x; rewrite !in_extract_funterms, !in_flat_map.
      intros [[[_sa _g] _l] [H1 H2]];  destruct H1 as [H1 | H1].
      -- injection H1; do 3 intro; subst _sa _g _l; simpl in H2; rewrite in_map_iff in H2.
         destruct H2 as [a [H2 H3]]; injection H2; intro; subst x.
         apply in_is_built_upon_ft; rewrite in_extract_funterms; apply in_or_app; left; simpl.
                  rewrite in_map_iff; eexists; split; [ | apply H3]; apply refl_equal.
      -- destruct (in_app_or _ _ _ H1) as [H3 | H3].
         ++ apply in_is_built_upon_ft; rewrite in_extract_funterms, flat_map_unfold.
            apply in_or_app; right; rewrite flat_map_app; apply in_or_app; left.
            rewrite in_flat_map; eexists; split; [apply H3 | apply H2].
         ++ simpl in H3; destruct H3 as [H3 | H3].
            *** injection H3; do 3 intro; subst _sa _g _l.
                destruct g as [g | ]; simpl in H2.
                ---- assert (W := well_formed_e_tail _ _ W4); simpl in W.
                     rewrite !Bool.Bool.andb_true_iff, forallb_forall in W.
                     assert (Wx := proj1 (proj1 (proj1 (proj1 W))) _ H2); simpl in Wx.
                     unfold well_formed_ft in Wx; revert Wx; apply is_built_upon_ft_incl.
                     intros y Hy; rewrite in_extract_funterms in Hy; rewrite in_extract_funterms.
                     rewrite flat_map_unfold; apply in_or_app; right.
                     rewrite flat_map_app; apply in_or_app; right.
                     apply Hy.
                ---- rewrite in_map_iff in H2.
                     destruct H2 as [a [_H2 H2]]; injection _H2; intro; subst x.
                     apply in_is_built_upon_ft; rewrite in_extract_funterms, flat_map_unfold.
                     apply in_or_app; right; rewrite flat_map_app; apply in_or_app; right; simpl.
                     apply in_or_app; left; unfold default_tuple.
                     rewrite in_map_iff, (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _ )).
                     eexists; split; [ | apply H2]; apply refl_equal.
            *** apply in_is_built_upon_ft; rewrite in_extract_funterms, flat_map_unfold.
                apply in_or_app; right; rewrite flat_map_app; apply in_or_app; right; simpl.
                apply in_or_app; right; rewrite in_flat_map; eexists; split; [apply H3 | apply H2].
    * case_eq (Fset.is_empty (A T) (variables_ft T f)); intro Hf; [intros; apply refl_equal | ].
      destruct g as [g | ].
      assert (Aux : find_eval_env T (env1 ++ (s, Group_By T g, l) :: env2) (A_agg T a f) =
                    find_eval_env T
           (env1 ++
            (labels T (default_tuple T s), Group_Fine T, default_tuple T s :: nil) :: env2)
           (A_agg T a f)).
weak_equiv_env 
                      T (env1 ++ (s, g, l) :: env2)
                      (env1 ++ (labels T (default_tuple T s), 
                                Group_Fine T, default_tuple T s :: nil) :: env2)).
      {
        apply Forall2_app; [apply equiv_env_weak_equiv_env; apply equiv_env_refl | ].
        constructor 2; [ | apply equiv_env_weak_equiv_env; apply equiv_env_refl].
        simpl.
      }
= find_eval_env_weak_eq  (A_agg T a f) (env1 ++ (s, g, l) :: env2) (env1 ++ (labels T (default_tuple T s), Group_Fine T, default_tuple T s :: nil) :: env2)).
      induction env1 as [ | [[_sa [_g | ]] _l] env1]; simpl.
      -- case_eq (find_eval_env T env2 (A_agg T a f)); [intros; apply refl_equal | ].
         exact (fun h => h).
  simpl in W; rewrite !Bool.Bool.andb_true_iff in W; simpl;
  destruct W as [[[W1 W2] W3] W4].      induction env1 as [
                assumption.
rewrite in_flat_map; eexists; split; [apply H3 | apply H2].

            rewrite in_flat_map; eexists; split; [apply H3 | apply H2].
                     
in_extract_funterms.
                     
apply is_built_upon_ft_incl with (groups_of_env_slice T (s, g, l)
        [eexists; split; [ | apply H2]; right; apply in_or_app; left; assumption | ].
      destruct H3 as [H3 | H3]; 
        [ | eexists; split; [ | apply H2]; right; apply in_or_app; do 2 right; assumption].
      injection H3; do 3 intro; subst _sa _g _l.
      simpl in H2; destruct g as [g | ].
      eexists; split; [right | ].
assumption ].
      destruct (in_split _ _ H1) as [e1 [e2 H3]].
 in Hx; rewrite in_extract_funterms.
      
    * revert
rewrite <- (proj1 W1); apply forallb_eq; intros x Hx; unfold env_t.
    unfold well_formed_ag.
    apply well_formed_ag_eq; constructor 2.
    * simpl; split; [apply Fset.equal_refl | apply refl_equal].
    * apply Forall2_app; [apply equiv_env_weak_equiv_env; apply equiv_env_refl | ].
      constructor 2; [ | apply equiv_env_weak_equiv_env; apply equiv_env_refl].
      simpl; split.
 [ | apply refl_equal].
apply weak_equiv_env_slice_refl.
[ | ].
destruct W1 as [W0 W1]; rewrite W2.
Qed.
*)

Lemma is_a_suitable_env_variables_ft :
  forall sa1 env f, well_formed_e env = true ->
 is_a_suitable_env T sa1 env f = true -> variables_ft T f subS (sa1 unionS att_of_env T env).
Proof.
intros sa1 env f We H.
unfold is_a_suitable_env in H.
refine (Fset.subset_trans _ _ _ _ (is_built_upon_ft_variables_ft_sub _ _ _ H) _).
rewrite !Fset.subset_spec; intros a Ha.
rewrite map_app, Fset.mem_Union_app, Bool.Bool.orb_true_iff in Ha.
destruct Ha as [Ha | Ha].
- rewrite map_map in Ha; simpl in Ha.
  rewrite (Fset.mem_eq_2 _ _ _ (Fset.Union_map_singleton_equal _ _)) in Ha.
  rewrite Fset.mem_union, Ha; apply refl_equal.
- rewrite Fset.mem_Union in Ha.
  destruct Ha as [s [Hs Ha]].
  rewrite in_map_iff in Hs.
  destruct Hs as [_f [Hs Hf]]; subst s.
  rewrite in_extract_funterms, in_flat_map in Hf.
  destruct Hf as [[[sa g] l] [Hx Hf]].
  destruct (in_split _ _ Hx) as [env1 [env2 Henv]]; subst env; simpl in Hf.
  assert (J := well_formed_ag_variables_ag_env_alt _ _ _ (well_formed_e_tail _ _ We) Hf).
  rewrite Fset.subset_spec in J.
  assert (Ja := J _ Ha); unfold env_slice in *.
  rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right.
  rewrite (Fset.mem_eq_2 _ _ _ (att_of_env_app _ env1 _)).
  rewrite Fset.mem_union, Bool.Bool.orb_true_iff; right; assumption.
Qed.

Lemma find_eval_env_some_variables_ft :
  forall env f e, well_formed_e env = true ->
    find_eval_env T env f = Some e -> (variables_ft T f) subS (att_of_env T env).
Proof.
intros env f; induction env as [ | [[sa1 g1] l1] env]; intros e We H.
- simpl in H.
  case_eq (is_built_upon_ft T nil f); intro Hf; rewrite Hf in H; [ | discriminate H].
  injection H; intro; subst e.
  assert (K := is_built_upon_ft_nil_empty_vars T _ Hf).
  rewrite Fset.is_empty_spec, Fset.equal_spec in K.
  rewrite Fset.subset_spec; intros a Ha.
  rewrite K, Fset.mem_empty in Ha; discriminate Ha.
- simpl in H.
  case_eq (find_eval_env T env f).
  + intros l Hl.
    simpl in We; rewrite Bool.Bool.andb_true_iff in We.
    generalize (IHenv _ (proj2 We) Hl).
    rewrite !Fset.subset_spec; intros J a Ha.
    rewrite att_of_env_unfold, Fset.mem_union, (J a Ha), Bool.Bool.orb_true_r; apply refl_equal.
  + intro K; rewrite K in H.
    destruct g1 as [g1 | ]; [ | discriminate H].
    case_eq (is_a_suitable_env T sa1 env f); intro Hf; rewrite Hf in H; [ | discriminate H].
    injection H; intro; subst e.
    rewrite att_of_env_unfold.
    apply is_a_suitable_env_variables_ft; [ | assumption].
    simpl in We; rewrite Bool.Bool.andb_true_iff in We.
    apply (proj2 We).
Qed.

(*Lemma find_eval_env_is_built_upon_ag :
  forall env a f sa g k env2, 
    find_eval_env T env (A_agg T a f) = Some ((sa, g, k) :: env2) ->
    (is_built_upon_ft T nil f = true \/
    exists env1, env = env1 ++ (sa, g, k) :: env2 /\
    is_built_upon_ag T
          (map (fun a : attribute T => A_Expr T (F_Dot T a)) ({{{sa}}}) ++
           flat_map (groups_of_env_slice T) env2) (A_agg T a f) = true).
Proof.
intro env; induction env as [ | [[sa1 g1] l1] env]; intros a f sa g k e H.
- simpl in H.
  case_eq (is_built_upon_ft T nil f); intro Hf; rewrite Hf in H; 
    [left; apply refl_equal | discriminate H].
- simpl in H.
  case_eq (find_eval_env T env (A_agg T a f)).
  + intros e' He'; rewrite He' in H; injection H; intro; subst e'.
    destruct (IHenv _ _ _ _ _ _ He') as [Hf | [env1 [Henv Hf]]];
      [left; assumption | ].
    right; exists ((sa1, g1, l1) :: env1); split; [ | assumption].
    rewrite Henv; simpl; apply refl_equal.
  + intro He'; rewrite He' in H.
    case_eq (is_a_suitable_env T sa1 env (A_agg T a f)); intro K; rewrite K in H; 
      [ | discriminate H].
    unfold is_a_suitable_env in K.
    injection H; do 4 intro; subst.
    right; exists nil; split; [apply refl_equal | apply K].
Qed.
*)
(*
Lemma find_eval_env_none_not_in_groups :
 forall env f, well_formed_e env = true -> find_eval_env T env f = None -> 
               In f (flat_map (groups_of_env_slice T) env) -> False.
Proof.
intro env; induction env as [ | [[sa g] l] env]; intros f We H K; [contradiction K | ].
simpl in H.
case_eq (find_eval_env T env f); [intros e He; rewrite He in H; discriminate H | ].
intros He; rewrite He in H.
case_eq (is_a_suitable_env T sa env f); intro Hf; rewrite Hf in H; [discriminate H | ].
simpl in K; destruct (in_app_or _ _ _ K) as [K1 | K2].
- destruct g as [g | ].
  + unfold is_a_suitable_env in Hf.
    rewrite well_formed_e_unfold, !Bool.Bool.andb_true_iff in We.
    destruct We as [[[[W1 W2] W3] W4] W5]; rewrite forallb_forall in W1.
    assert (Wf := W1 _ K1).
    assert (K2 := in_is_built_upon_ag _ _ _ K1).
    assert (K3 := is_built_upon_ag_trans 
                    T f g 
                    (map (fun a : attribute T => A_Expr T (F_Dot T a)) ({{{sa}}}) ++
                         flat_map (groups_of_env_slice T) env)).
    rewrite K3 in Hf.
    * discriminate Hf.
    * intros x Hx; assert (Wx := W1 _ Hx).
Locate well_formed_ag_variables_ag_env.
      SearchAbout [well_formed_ag].
      unfold well_formed_ag in Wx.
      SearchAbout well_formed_ag.
    SearchAbout [is_built_upon_ag].
 unfold well_formed_ag in Wf.
    
 progress simpl in Hf.
unfold is_built_upon_ag in Hf.
    

Qed.
*)
End Sec.
