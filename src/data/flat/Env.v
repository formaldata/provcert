(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Require Import List Bool Arith NArith.
Require Import BasicTacs BasicFacts Bool3 ListFacts ListSort ListPermut
        OrderedSet Partition FiniteSet FiniteBag FiniteCollection Join.

Require Export FTuples.
Require Export FTerms.
Require Export ATerms.

Import Tuple.

Section Sec.

Hypothesis T : Rcd.

Inductive group_by : Type :=
  | Group_By : list (aggterm T) -> group_by
  | Group_Fine.

Definition env_slice := (Fset.set (A T) * group_by * list (tuple T))%type.
Definition env := (list env_slice).

Definition att_of_env (env : env) :=
  (Fset.Union _ (map (fun slc => match slc with (sa, _, _) => sa end) env)).

Definition fresh_att_in_env s (env : env) := Fset.is_empty _ (s interS (att_of_env env)).

Definition env_t (env : env) t := (labels T t, Group_Fine, t :: nil) :: env. 

Definition env_g (env : env) g s := 
  (match quicksort (OTuple T) s with 
   | nil => Fset.empty _ 
   | t :: _ => labels T t 
   end, g, s) :: env.

Lemma att_of_env_unfold :
  forall sa g l env, att_of_env ((sa, g, l) :: env) = (sa unionS (att_of_env env)).
Proof.
intros sa g l env; unfold att_of_env; simpl; apply refl_equal.
Qed.

Lemma att_of_env_eq :
  forall s1 g1 l1 s2 g2 l2 env, 
    s1 =S= s2 -> att_of_env (((s1, g1), l1) :: env) =S= att_of_env (((s2, g2), l2) :: env).
Proof.
intros s1 g1 l1 s2 g2 l2 env Hs; simpl.
rewrite !att_of_env_unfold; apply Fset.union_eq_1; assumption.
Qed.

Lemma att_of_env_app : 
  forall env1 env2, att_of_env (env1 ++ env2) =S= (att_of_env env1 unionS att_of_env env2).
Proof.
intros env1 env2; unfold att_of_env.
rewrite map_app, Fset.equal_spec; intro a.
rewrite Fset.mem_Union_app, Fset.mem_union; apply refl_equal.
Qed.

Lemma fresh_att_in_env_union :
  forall env s1 s2, 
    fresh_att_in_env (s1 unionS s2) env = fresh_att_in_env s1 env && fresh_att_in_env s2 env.
Proof.
intros env s1 s2; unfold fresh_att_in_env; rewrite <- Fset.is_empty_union.
apply Fset.is_empty_eq.
rewrite (Fset.equal_eq_1 _ _ _ _ (Fset.inter_distr_union_2 _ _ _ _ ));
apply Fset.equal_refl.
Qed.

Lemma fresh_att_in_env_unfold :
  forall s g l env sa, fresh_att_in_env sa (((s, g), l) :: env) = 
                       (Fset.is_empty _ (sa interS s)) && fresh_att_in_env sa env.
Proof.
intros s g l env sa; unfold fresh_att_in_env, att_of_env; simpl.
rewrite (Fset.is_empty_eq _ _ _ (Fset.inter_distr_union_1 _ _ _ _)), Fset.is_empty_union.
apply refl_equal.
Qed.

Lemma fresh_att_in_env_app :
  forall env1 env2 sa, fresh_att_in_env sa (env1 ++ env2) = 
                       fresh_att_in_env sa env1 && fresh_att_in_env sa env2.
Proof.
intros env1 env2 s; unfold fresh_att_in_env.
rewrite <- Fset.is_empty_union; apply Fset.is_empty_eq.
rewrite <- (Fset.equal_eq_2 _ _ _ _ (Fset.inter_distr_union_1 _ _ _ _)).
apply Fset.inter_eq_2.
apply att_of_env_app.
Qed.

Lemma fresh_att_in_env_eq_1 :
  forall s1 s2 env, s1 =S= s2 -> fresh_att_in_env s1 env = fresh_att_in_env s2 env.
Proof.
intros s1 s2 env Hs; unfold fresh_att_in_env; apply Fset.is_empty_eq.
apply Fset.inter_eq_1; assumption.
Qed.

Lemma env_t_env_g : forall env t, env_t env t = env_g env Group_Fine (t :: nil).
Proof.
intros env t; apply refl_equal.
Qed.

Definition equiv_env_slice (e1 e2 : (Fset.set (A T) * group_by * (list (tuple T)))) := 
  match e1, e2 with
    | (sa1, g1, x1), (sa2, g2, x2) => 
      sa1 =S= sa2 /\ g1 = g2 /\ x1 =PE= x2
  end.

Definition equiv_env e1 e2 := Forall2 equiv_env_slice e1 e2.

Lemma equiv_env_refl : forall e, equiv_env e e.
Proof.
intro e; unfold equiv_env; induction e as [ | [[sa g] l] e]; simpl; trivial.
constructor 2; [simpl; repeat split | apply IHe].
- apply Fset.equal_refl.
- apply Oeset.permut_refl.
Qed.

Lemma equiv_env_sym :
  forall e1 e2, equiv_env e1 e2 <-> equiv_env e2 e1.
Proof.
assert (H : forall e1 e2, equiv_env e1 e2 -> equiv_env e2 e1).
{
  intro e1; induction e1 as [ | s1 e1]; intros [ | s2 e2].
  - exact (fun h => h).
  - intro H; inversion H.
  - intro H; inversion H.
  - intro H; simpl in H.
    inversion H; subst.
    simpl in H.
    constructor 2; [ | apply IHe1; assumption].
    destruct s1 as [[sa1 g1] l1]; destruct s2 as [[sa2 g2] l2]; simpl in H3; simpl.
    destruct H3 as [K1 [K2 K3]].
    split; [rewrite <- (Fset.equal_eq_1 _ _ _ _ K1); apply Fset.equal_refl | ].
    split; [apply sym_eq; assumption | ].
    apply Oeset.permut_sym; apply K3.
}
intros e1 e2; split; apply H.
Qed.

Definition weak_equiv_env_slice (e1 e2 : (Fset.set (A T) * group_by * (list (tuple T)))) := 
  match e1, e2 with
    | (sa1, g1, x1), (sa2, g2, x2) => sa1 =S= sa2 /\ g1 = g2
  end.

Definition weak_equiv_env e1 e2 := Forall2 weak_equiv_env_slice e1 e2.

Lemma weak_equiv_env_sym :
  forall e1 e2, weak_equiv_env e1 e2 <-> weak_equiv_env e2 e1.
Proof.
intro e1; induction e1 as [ | [[sa1 g1] x1] e1]; intro e2; simpl; split.
- intro H; inversion H; constructor 1.
- intro H; inversion H; constructor 1.
- destruct e2 as [ | [[sa2 g2] x2] e2]; intro H; inversion H; subst.
  constructor 2; [ | rewrite <- IHe1; assumption].
  simpl in H3; simpl; destruct H3 as [H3 H3']; split; [ | apply sym_eq; assumption].
  rewrite (Fset.equal_eq_2 _ _ _ _ H3); apply Fset.equal_refl.
- destruct e2 as [ | [[sa2 g2] x2] e2]; intro H; inversion H; subst.
  constructor 2; [ | rewrite IHe1; assumption].
  simpl in H3; destruct H3 as [H3 K3]; subst; simpl; repeat split.
  unfold weak_equiv_env in H; inversion H; subst; simpl in H3.
  rewrite (Fset.equal_eq_2 _ _ _ _ H3); apply Fset.equal_refl.
Qed.

Lemma equiv_env_weak_equiv_env :
  forall e1 e2, equiv_env e1 e2 -> weak_equiv_env e1 e2.
Proof.
intro e1; induction e1 as [ | [[sa1 g1] l1] e1]; intros [ | [[sa2 g2] l2] e2]; 
  intro He; inversion He; subst.
- constructor 1.
- constructor 2; [ | apply IHe1; trivial].
  unfold weak_equiv_env_slice; unfold equiv_env_slice in H2.
  split; [apply (proj1 H2) | apply (proj1 (proj2 H2))].
Qed.

Lemma env_slice_eq_1 :
  forall x1 x2, Oeset.compare (OLTuple T) x1 x2 = Eq ->
   match quicksort (OTuple T) x1 with
   | nil => emptysetS
   | t0 :: _ => labels T t0
   end =S=
   match quicksort (OTuple T) x2 with
   | nil => emptysetS
   | t0 :: _ => labels T t0
   end.
Proof.
intros l1 l2 Hl; unfold OLTuple, OLA in Hl; simpl in Hl.
unfold compare_OLA in Hl.
set (q1 := quicksort (OTuple T) l1) in *.
set (q2 := quicksort (OTuple T) l2) in *.
destruct q1 as [ | x1 q1]; destruct q2 as [ | x2 q2]; try discriminate Hl.
- apply Fset.equal_refl.
- simpl in Hl.
  case_eq (Oeset.compare (OTuple T) x1 x2); intro Hx; rewrite Hx in Hl; try discriminate Hl.
  rewrite tuple_eq in Hx; apply (proj1 Hx).
Qed.

Lemma env_t_eq_1 :
  forall e1 e2 x, equiv_env e1 e2 -> equiv_env (env_t e1 x) (env_t e2 x).
Proof.
intros e1 e2 x He.
constructor 2; [ | apply He].
simpl; repeat split.
- apply Fset.equal_refl.
- apply Oeset.permut_refl.
Qed.

Lemma env_t_eq_2 :
  forall e x1 x2, x1 =t= x2 ->  equiv_env (env_t e x1) (env_t e x2).
Proof.
intros e x1 x2 Hx.
constructor 2.
- simpl; repeat split.
  + rewrite tuple_eq in Hx; apply (proj1 Hx).
  + simpl; apply compare_list_t; unfold compare_OLA; simpl.
    rewrite Hx; apply refl_equal.
- apply equiv_env_refl.
Qed.

Lemma env_t_eq :
  forall e1 e2 x1 x2, equiv_env e1 e2 -> x1 =t= x2 -> equiv_env (env_t e1 x1) (env_t e2 x2).
Proof.
intros e1 e2 x1 x2 He Hx.
constructor 2; [ | apply He].
simpl; repeat split.
- rewrite tuple_eq in Hx; apply (proj1 Hx).
- simpl; apply compare_list_t; unfold compare_OLA; simpl.
  rewrite Hx; apply refl_equal.
Qed.

Lemma env_g_eq_1 :
  forall e1 e2 g x, equiv_env e1 e2 -> equiv_env (env_g e1 g x) (env_g e2 g x).
Proof.
intros e1 e2 g x He.
constructor 2; [ | apply He].
simpl; repeat split.
- apply Fset.equal_refl.
- apply Oeset.permut_refl.
Qed.

Lemma env_g_eq_2 :
  forall e g x1 x2, x1 =PE= x2 ->  equiv_env (env_g e g x1) (env_g e g x2).
Proof.
intros e g x1 x2 Hx.
constructor 2.
- simpl; repeat split.
  + apply env_slice_eq_1; simpl; rewrite <- compare_list_t; assumption.
  + assumption.
- apply equiv_env_refl.
Qed.

Lemma env_g_eq :
  forall e1 e2 g x1 x2, equiv_env e1 e2 -> x1 =PE= x2 -> equiv_env (env_g e1 g x1) (env_g e2 g x2).
Proof.
intros e1 e2 g x1 x2 He Hx.
constructor 2; [ | apply He].
- simpl; repeat split.
  + apply env_slice_eq_1; simpl; rewrite <- compare_list_t; assumption.
  + assumption.
Qed.

Lemma env_g_fine_eq_2 :
  forall e x1 x2, 
    x1 =t= x2 -> 
    equiv_env (env_g e Group_Fine (x1 :: nil)) (env_g e Group_Fine (x2 :: nil)).
Proof.
intros e x1 x2 Hx.
constructor 2.
- simpl; repeat split.
  + rewrite tuple_eq in Hx; apply (proj1 Hx).
  + simpl; apply compare_list_t; unfold compare_OLA; simpl.
    rewrite Hx; apply refl_equal.
- apply equiv_env_refl.
Qed.


Lemma fresh_att_in_env_incl :
  forall s1 s2 env, 
    fresh_att_in_env s1 env = true -> Fset.subset _ s2 s1 = true ->
    fresh_att_in_env s2 env = true.
Proof.
intros s1 s2 env; unfold fresh_att_in_env.
rewrite !Fset.is_empty_spec, !Fset.equal_spec, Fset.subset_spec.
intros F1 H a; rewrite Fset.mem_inter, Fset.mem_empty.
case_eq (a inS? s2); intro Ha; [ | apply refl_equal].
generalize (F1 a); rewrite Fset.mem_inter, (H _ Ha), Fset.mem_empty.
exact (fun h => h).
Qed.

Lemma fresh_att_in_env_incl_funterm :
  forall f l env, fresh_att_in_env (variables_ft T (F_Expr _ f l)) env = true ->
                  forall x, In x l -> fresh_att_in_env (variables_ft T x) env = true.
Proof.
intros f l env H x Hx.
refine (fresh_att_in_env_incl _ _ _ H _).
rewrite Fset.subset_spec; intros u Hu; simpl.
rewrite Fset.mem_Union.
eexists; split; [ | apply Hu].
rewrite in_map_iff; eexists; split; [ | apply Hx]; apply refl_equal.
Qed.

End Sec.

Ltac env_tac :=
  match goal with
    | |- equiv_env _ ?e ?e => apply equiv_env_refl

(* env_t *)
    | He : equiv_env _ ?e1 ?e2 
      |- equiv_env _ (env_t _ ?e1 ?x) (env_t _ ?e2 ?x) =>
      apply env_t_eq_1; assumption

    | Hx : ?x1 =t= ?x2 
    |- equiv_env _ (env_t _ ?e ?x1) (env_t _ ?e ?x2) =>
      apply env_t_eq_2; assumption

    | He : equiv_env _ ?e1 ?e2, Hx : ?x1 =t= ?x2 
      |- equiv_env _ (env_t _ ?e1 ?x1) (env_t _ ?e2 ?x2) =>
      apply env_t_eq; assumption

(* env_g *)
    | Hx : Oeset.compare (Tuple.OTuple ?T) ?x1 ?x2 = Eq 
    |- equiv_env ?T (env_g ?T ?e ?g (?x1 :: nil)) 
                       (env_g ?T ?e ?g (?x2 :: nil)) =>
      rewrite <- !env_t_env_g; apply env_t_eq_2; assumption

    | Hx : Oeset.compare (Tuple.OLTuple ?T) ?x1 ?x2 = Eq
      |- equiv_env ?T (env_g ?T ?e ?g ?x1) (env_g ?T ?e ?g ?x2) =>
      apply env_g_eq_2; rewrite compare_list_t; assumption

    | He : equiv_env _ ?e1 ?e2
      |- equiv_env _ (env_g _ ?e1 ?g ?x) (env_g _ ?e2 ?g ?x) =>
      apply env_g_eq_1; assumption

    | He : equiv_env _ ?e1 ?e2, 
      Hx : Oeset.compare _ ?x1 ?x2 = Eq 
      |- equiv_env _ (env_g _ ?e1 ?g ?x1) (env_g _ ?e2 ?g ?x2) =>
      apply env_g_eq; [ | rewrite compare_list_t]; assumption

    | Hl : comparelA (Oeset.compare _) ?l1 ?l2 = Eq 
      |- equiv_env ?T (env_g ?T ?e (Group_By ?T ?g) ?l1) 
                         (env_g ?T ?e (Group_By ?T ?g) ?l2) =>
      apply env_g_eq_2; apply Oeset.permut_refl_alt; assumption

    | He : equiv_env _ ?e1 ?e2,
      Ht : Oeset.compare (Tuple.OTuple ?T) ?x1 ?x2 = Eq 
      |- equiv_env _ (env_g ?T ?e1 ?g (?x1 :: nil)) (env_g ?T ?e2 ?g (?x2 :: nil)) =>
      apply env_g_eq; 
      [assumption 
      | rewrite compare_list_t; unfold compare_OLA; rewrite !quicksort_1; 
        simpl; rewrite Ht; apply refl_equal]

    | Ht : Oeset.compare (Tuple.OTuple ?T) ?x1 ?x2 = Eq 
      |- equiv_env _ (env_g ?T ?e ?g (?x1 :: nil)) (env_g ?T ?e ?g (?x2 :: nil)) =>
      apply env_g_eq; 
      [ apply equiv_env_refl 
      | rewrite compare_list_t; unfold compare_OLA; rewrite !quicksort_1; 
        simpl; rewrite Ht; apply refl_equal]

(*    | |- Tuple.equiv_env ?T (Tuple.env_g ?T ?e ?g (?x1 :: nil)) 
                         (Tuple.env_g ?T ?e ?g (?x2 :: nil)) => 
      let h := fresh "__YY" in 
      assert (h := refl_equal 2%N);
      rewrite <- !Tuple.env_t_env_g                         

    | |- Tuple.equiv_env ?T (Tuple.env_g ?T ?e Tuple.Group_Fine (?x1 :: nil)) 
                       (Tuple.env_g ?T ?e Tuple.Group_Fine (?x2 :: nil)) =>
      assert (__XX1 := refl_equal 1%N)
*)
    | _ =>  trivial; fail
  end.
