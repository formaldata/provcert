(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Bool List Arith NArith.

Require Import OrderedSet BasicFacts ListFacts ListPermut ListSort 
        FiniteSet FiniteBag FiniteCollection
        Tree Term Formula FlatData RelationalAlgebra ExtendedAlgebra.

Import Tuple Expression DatabaseSchema.

Section Embedding.

Hypothesis RelAlg : RA.Rcd.

Definition Ounit : Oset.Rcd unit.
split with (fun _ _ => Eq).
- intros a1 a2; destruct a1; destruct a2; apply refl_equal.
- intros; discriminate.
- intros; apply refl_equal.
Defined.

Definition ExtAlg :=
  Data.mk_rcd 
    (RA.T RelAlg)
    (RA.DBS RelAlg)
    (RA.OP RelAlg)
    Ounit
    Ounit
    (RA.default_type RelAlg).

Import RA EA.

Notation aggregate := (Data.aggregate ExtAlg).
Notation T := (Data.T ExtAlg).
Notation symb := (Data.symb ExtAlg).
Notation Code := (Code ExtAlg).
Notation Fine := (Fine ExtAlg).
Notation equery := (equery ExtAlg).
Notation query := (query RelAlg).

Definition coerce_att (a : attribute (RA.T RelAlg)) : attribute (Data.T ExtAlg) := a.
Definition coerce_renaming rho :=
  List.map (fun aa => match aa with (a1, a2) => (coerce_att a1, coerce_att a2) end) rho.

Fixpoint algebra_to_ealgebra (q : RA.query RelAlg) : equery :=
  match q with
    | Query_Empty s => Equery_Empty ExtAlg FlagSet s
    | Query_Empty_Tuple => Equery_Empty_Tuple ExtAlg FlagSet
    | Query_Basename r => Equery_Basename ExtAlg r
    | Query_Sigma f q1 =>
      let term_to_term :=
          (fix term_to_term t :=
             match t with
               | Var (Vrbl q n) => Var (Vrbl (algebra_to_ealgebra q) n)
               | Term (RA.Constant c) l => Term (Expression.Constant c) (map term_to_term l)
               | Term (RA.Dot a) l => Term (Expression.Dot a) (map term_to_term l)
             end) in
      let formula_to_formula :=
          (fix formula_to_formula f :=
             match f with
               | TTrue => @TTrue _ _ _
               | Atom p l => Atom p (List.map term_to_term l)
               | Conj a f1 f2 => Conj a (formula_to_formula f1) (formula_to_formula f2)
               | Not f => Not (formula_to_formula f)
               | Quant q (Vrbl qn n) f => 
                 Quant q (Vrbl (algebra_to_ealgebra qn) n) (formula_to_formula f)
             end) in
      match f with
        | TTrue => algebra_to_ealgebra q1
        | _ => sigma_equery (formula_to_formula f) (algebra_to_ealgebra q1)
      end
    | Query_Pi la q1 => 
      let sa := Fset.mk_set (A T) la in
      if Fset.equal (A T) sa (RA.sort q1)
      then (algebra_to_ealgebra q1)
      else pi_equery sa (algebra_to_ealgebra q1)
    | Query_Rename rho q1 =>
      if Fset.for_all 
           (A T) (fun a => Oset.eq_bool (OAtt T) a (apply_renaming T rho a)) 
           (RA.sort q1)
      then algebra_to_ealgebra q1
      else rename_equery
             (apply_renaming T (coerce_renaming rho))
             (algebra_to_ealgebra q1)
    | Query_NaturalJoin q1 q2 =>
        Equery_NaturalJoin FlagSet (algebra_to_ealgebra q1) (algebra_to_ealgebra q2)
    | Query_Set o q1 q2 => Equery_Set o FlagSet (algebra_to_ealgebra q1) (algebra_to_ealgebra q2)
  end.

Fixpoint term_to_term t :=
  match t with
    | Var (Vrbl q n) => Var (Vrbl (algebra_to_ealgebra q) n)
    | Term (RA.Constant c) l => Term (@Expression.Constant T symb c) (map term_to_term l)
    | Term (RA.Dot a) l => Term (Expression.Dot a) (map term_to_term l)
  end.

Fixpoint formula_to_formula (f : formula _ (RA.predicate RelAlg) _) :=
  match f with
    | TTrue => @TTrue _ _ _
    | Atom p l => Atom p (List.map term_to_term l)
    | Conj a f1 f2 => Conj a (formula_to_formula f1) (formula_to_formula f2)
    | Not f => Not (formula_to_formula f)
    | Quant q (Vrbl qn n) f => Quant q (Vrbl (algebra_to_ealgebra qn) n) (formula_to_formula f)
  end. 

Lemma term_to_term_unfold :
  forall t, term_to_term t =
            match t with
              | Var (Vrbl q n) => Var (Vrbl (algebra_to_ealgebra q) n)
              | Term (RA.Constant c) l => Term (Expression.Constant c) (map term_to_term l)
              | Term (RA.Dot a) l => Term (Expression.Dot a) (map term_to_term l)
            end.
Proof.
intro t; case t; intros; apply refl_equal.
Qed.

Lemma formula_to_formula_unfold :
  forall f, formula_to_formula f =
            match f with
              | TTrue => @TTrue _ _ _
              | Atom p l => Atom p (List.map term_to_term l)
              | Conj a f1 f2 => Conj a (formula_to_formula f1) (formula_to_formula f2)
              | Not f => Not (formula_to_formula f)
              | Quant q (Vrbl qn n) f => Quant q (Vrbl (algebra_to_ealgebra qn) n) (formula_to_formula f)
            end. 
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma algebra_to_ealgebra_unfold :
  forall q, algebra_to_ealgebra q =
  match q with
    | Query_Empty s => Equery_Empty ExtAlg FlagSet s
    | Query_Empty_Tuple => Equery_Empty_Tuple ExtAlg FlagSet
    | Query_Basename r => Equery_Basename ExtAlg r
    | Query_Sigma f q1 =>
      match f with
        | TTrue => algebra_to_ealgebra q1
        | _ => sigma_equery (formula_to_formula f) (algebra_to_ealgebra q1)
      end
    | Query_Pi la q1 => 
      let sa := Fset.mk_set (A T) la in
      if Fset.equal (A T) sa (RA.sort q1)
      then (algebra_to_ealgebra q1)
      else pi_equery sa (algebra_to_ealgebra q1)
    | Query_Rename rho q1 =>
      if Fset.for_all 
           (A T) 
           (fun a => Oset.eq_bool (OAtt T) a (apply_renaming T rho a)) 
           (RA.sort q1)
      then algebra_to_ealgebra q1
      else rename_equery (apply_renaming T (coerce_renaming rho)) (algebra_to_ealgebra q1)
    | Query_NaturalJoin q1 q2 =>
        Equery_NaturalJoin FlagSet (algebra_to_ealgebra q1) (algebra_to_ealgebra q2)
    | Query_Set o q1 q2 => Equery_Set o FlagSet (algebra_to_ealgebra q1) (algebra_to_ealgebra q2)
    end.
Proof.
intro q; case q; intros; apply refl_equal.
Qed.
    
Ltac simpl_tac := (simpl attribute in *; simpl OAtt in *; simpl A in *; simpl Data.T in *; 
                   simpl Data.symb in *; simpl Data.predicate in *; simpl Data.aggregate in *).

Lemma sort_algebra_to_ealgebra :
  forall q, (EA.sort (algebra_to_ealgebra q)) =S= RA.sort q.
Proof.
intro q; induction q;
rewrite Fset.equal_spec; intro a; trivial.
- rewrite Fset.equal_spec in IHq.
  rewrite algebra_to_ealgebra_unfold.
  unfold sigma_equery, id_renaming.
  destruct f; trivial;
    (rewrite (EA.sort_unfold (Equery_Omega _ _ _ _));
     rewrite (RA.sort_unfold (Query_Sigma _ _));
     rewrite map_map, Fset.mem_mk_set, eq_bool_iff, map_id, <- Fset.mem_elements, IHq);
    split; trivial.
- rewrite Fset.equal_spec in IHq.
  rewrite algebra_to_ealgebra_unfold, (RA.sort_unfold (Query_Pi _ _)).
  cbv zeta.
  case_eq (Fset.mk_set (A T) l =S?= RA.sort q); intro Hs.
  + rewrite Fset.equal_spec in Hs.
    rewrite IHq, Hs; apply refl_equal.
  + unfold pi_equery, id_renaming.
    rewrite sort_unfold, map_map, map_id; [ | intros; apply refl_equal].
    rewrite Fset.mem_mk_set, <- Fset.mem_elements.
    apply refl_equal.
- rewrite Fset.equal_spec in IHq1, IHq2.
  rewrite algebra_to_ealgebra_unfold, sort_unfold, RA.sort_unfold.
  rewrite 2 Fset.mem_union, IHq1, IHq2.
  apply refl_equal.
- rewrite algebra_to_ealgebra_unfold, (RA.sort_unfold (Query_Rename _ _)).
  case_eq (Fset.for_all (A T)
                 (fun a0 : attribute T =>
                  Oset.eq_bool (OAtt T) a0 (apply_renaming T r a0))
                 (RA.sort q));
    intro Hs.
  + rewrite Fset.equal_spec in IHq; rewrite IHq.
    unfold Fset.map; rewrite map_id.
    * rewrite Fset.mem_mk_set, <- Fset.mem_elements.
      apply refl_equal.
    * intros b Hb; apply sym_eq.
      rewrite Fset.for_all_spec, forallb_forall in Hs.
      rewrite <- (Oset.eq_bool_true_iff (OAtt (RA.T RelAlg))).
      apply Hs; trivial.
  + unfold rename_equery, rho_renaming, Fset.map; rewrite sort_unfold, map_map.
    rewrite (Fset.elements_spec1 _ _ _ IHq).
    do 2 apply f_equal; rewrite <- map_eq; intros b Hb.
    unfold apply_renaming, coerce_renaming; simpl.
    rewrite map_id; [apply refl_equal | ].
    intros x _; destruct x; trivial.
- rewrite Fset.equal_spec in IHq1.
  simpl; apply IHq1.
Qed.

Fixpoint ivar_xts (A : Type) (i : variable A -> tuple T) (l : list (N * tuple T)) :=
  match l with
    | nil => i
    | (n, t) :: l =>
      fun x => match x with
                 | Vrbl _ m => if Oset.eq_bool ON n m then t else ivar_xts i l x
               end
  end.

Lemma variables_t_term_to_term :
  forall t, variables_t (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg)) (term_to_term t) =S=
            Fset.map _ (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg)) 
                     (fun x => match x with 
                                 | Vrbl q n => Vrbl (algebra_to_ealgebra q) n
                               end)
                     (variables_t (FVR (RA.OQ RelAlg) (RA.OP RelAlg) (RA.OSymbol RelAlg)) t).
Proof.
intros t; rewrite Fset.equal_spec; intro x; pattern t; apply term_rec3; clear t.
- intros [qv nv]; rewrite term_to_term_unfold, 2 variables_t_unfold.
  unfold Fset.map; rewrite Fset.singleton_spec, Fset.elements_singleton, 2 map_unfold.
  rewrite Fset.mem_mk_set, 2 Oset.mem_bool_unfold, Bool.orb_false_r.
  apply refl_equal.
- intros f l IHl; rewrite term_to_term_unfold, variables_t_unfold.
  apply trans_eq with
  (x inS? Fset.Union (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg))
               (map
                  (variables_t
                     (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg))) (map term_to_term l)));
    [destruct f; trivial | ].
  rewrite map_map, (variables_t_unfold _ (Term _ _)).
  rewrite eq_bool_iff, Fset.mem_Union, Fset.mem_map; split.
  + intros [s [Hs H]]; rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    assert (IH := IHl t Ht); rewrite IH, Fset.mem_map in H.
    destruct H as [[qv nv] [Hx H]]; subst x.
    exists (Vrbl qv nv); split; trivial.
    rewrite Fset.mem_Union.
    exists (variables_t (FVR (RA.OQ RelAlg) (OP RelAlg) (RA.OSymbol RelAlg)) t); split; trivial.
    rewrite in_map_iff; exists t; split; trivial.
  + intros [[qv nv] [Hx H]]; subst x.
    rewrite Fset.mem_Union in H.
    destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    exists (variables_t (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg)) (term_to_term t)).
    split.
    * rewrite in_map_iff; exists t; split; trivial.
    * assert (IH := IHl t Ht); rewrite IH, Fset.mem_map.
      exists (Vrbl qv nv); split; trivial.
Qed.    

Lemma variables_f_formula_to_formula :
  forall f, variables_f (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg) (formula_to_formula f) =S=
            Fset.map _ (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg)) 
                     (fun x => match x with 
                                 | Vrbl q n => Vrbl (algebra_to_ealgebra q) n
                               end)
                     (variables_f (RA.OQ RelAlg) (RA.OP RelAlg) (RA.OSymbol RelAlg) f).
Proof.
intros f; rewrite Fset.equal_spec; intro x; induction f.
- rewrite formula_to_formula_unfold, 2 variables_f_unfold.
  rewrite eq_bool_iff, Fset.mem_map; split.
  + intro Hx; rewrite Fset.empty_spec in Hx; discriminate Hx.
  + intros [y [_ Hy]]; rewrite Fset.empty_spec in Hy; discriminate Hy.
- rewrite formula_to_formula_unfold, 2 variables_f_unfold, map_map.
  rewrite eq_bool_iff, Fset.mem_Union, Fset.mem_map; split.
  + intros [s [Hs Hx]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    assert (H := variables_t_term_to_term t).
    rewrite Fset.equal_spec in H; rewrite H, Fset.mem_map in Hx.
    destruct Hx as [[qv nv] [Hx K]]; subst x.
    exists (Vrbl qv nv); split; trivial.
    rewrite Fset.mem_Union.
    exists (variables_t (FVR (RA.OQ RelAlg) (OP RelAlg) (RA.OSymbol RelAlg)) t); split; trivial.
    rewrite in_map_iff; exists t; split; trivial.
  + intros [[qv nv] [Hx H]]; subst x.
    rewrite Fset.mem_Union in H.
    destruct H as [s [Hs H]].
    rewrite in_map_iff in Hs.
    destruct Hs as [t [Hs Ht]]; subst s.
    exists (variables_t (FVR (OQ ExtAlg) (Data.OP ExtAlg) (OSymbol ExtAlg)) (term_to_term t)).
    split.
    * rewrite in_map_iff.
      exists t; split; trivial.
    * assert (K := variables_t_term_to_term t).
      rewrite Fset.equal_spec in K; rewrite K, Fset.mem_map.
      exists (Vrbl qv nv); split; trivial.
- rewrite formula_to_formula_unfold, variables_f_unfold, eq_bool_iff, Fset.mem_union; 
  rewrite IHf1, IHf2, <- Fset.mem_union, (variables_f_unfold _ _ _ (Conj _ _ _)), <- eq_bool_iff.
  clear IHf1 IHf2; apply sym_eq; revert x; rewrite <- Fset.equal_spec.
  apply Fset.map_union.
- apply IHf.
- destruct v as [qv nv]; rewrite formula_to_formula_unfold, variables_f_unfold, Fset.add_spec.
  rewrite (variables_f_unfold _ _ _ (Quant _ _ _)).
  rewrite eq_bool_iff, Bool.orb_true_iff, Oset.eq_bool_true_iff, Fset.mem_map; split.
  + intros [H | H].
    * subst x; exists (Vrbl qv nv); split; trivial.
      rewrite Fset.add_spec, Bool.orb_true_iff; left; 
      rewrite Oset.eq_bool_true_iff; apply refl_equal.
    * rewrite IHf, Fset.mem_map in H.
      destruct H as [[q1 n1] [Hx H]]; subst x.
      exists (Vrbl q1 n1); split; trivial.
      rewrite Fset.add_spec, H, Bool.orb_true_r; apply refl_equal.
  + intros [[q1 n1] [Hx H]]; subst x;
    rewrite Fset.add_spec, Bool.orb_true_iff, Oset.eq_bool_true_iff in H.
    destruct H as [H | H].
    * injection H; clear H; do 2 intro; subst q1 n1; left; apply refl_equal.
    * right; rewrite IHf, Fset.mem_map.
      exists (Vrbl q1 n1); split; trivial.
Qed.

Lemma eval_query_translation_aux :
  forall ip is (ia : aggregate -> list (value T) -> value T) I,
    well_sorted_instance RelAlg I ->
    let I' := fun r => Fecol.Fset (I r) in
  forall n, 
    (forall (q : query), 
       query_size q <= n -> 
       (forall q1 m1 q2 m2, 
          Vrbl q1 m1 inS deep_variables_q q -> Vrbl q2 m2 inS deep_variables_q q ->
          m1 = m2 -> q1 = q2) ->
       forall t, t inSE? (eval_query ip I q) = 
                 (t inCE? (eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q)))) /\
    (forall f, tree_size (RA.tree_of_formula f) <= n -> 
       (forall q1 m1 q2 m2, 
          Vrbl q1 m1 inS deep_variables_f f -> Vrbl q2 m2 inS deep_variables_f f ->
          m1 = m2 -> q1 = q2) ->
       forall t l,    
         RA.interp_formula 
           ip
           (fun q : query =>
              Feset.elements (Fecol.CSet (CTuple (RA.T RelAlg))) (eval_query ip I q))
           (ivar_xts (fun _ : variable query => t) l) f =
         interp_formula (ExtAlg := ExtAlg)
           ip is
           (fun q : equery => Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is ia I' q))
           (ivar_xts (fun _ : variable equery => t) l) (formula_to_formula f)).
Proof.
intros ip is ia I WI I' n; induction n as [ | n]; repeat split.
- intros q Hn; destruct q; inversion Hn.
- intros f Hn; destruct f; inversion Hn. 
- intros q Hn Hq t.
  destruct q; try apply refl_equal.
  + (* Query_Sigma *)
    assert 
      (IHq : forall t : tuple (RA.T RelAlg),
               (t inSE? eval_query ip I q) =
               (t inCE? eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q))).
    {
      apply IHn.
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply Plus.le_plus_r.
      - do 6 intro; apply Hq; 
        rewrite deep_variables_q_unfold, Fset.mem_union, Bool.orb_true_iff; right; assumption.
    }
    apply eq_trans with 
    (t inCE? (eval_equery (ExtAlg := ExtAlg) ip is ia I' 
                          (sigma_equery (formula_to_formula f) (algebra_to_ealgebra q)))).
    * {
        rewrite (Fecol.mem_eq_2
                   _ _ (clean_eval_equery_sigma 
                          (ExtAlg := ExtAlg) ip is ia (I := I') 
                          (formula_to_formula f) (algebra_to_ealgebra q) WI)).
        rewrite (eval_query_unfold _ _ (Query_Sigma _ _)).
        rewrite Feset.filter_spec; [ | intros; apply RA.eval_f_eq; assumption].
        rewrite Fecol.mem_filter, <- IHq; [ | intros; apply eval_f_eq; assumption].
        apply f_equal.
        unfold RA.eval_f, eval_f.
        refine (proj2 IHn f _ _ _ nil).
        - rewrite query_size_unfold in Hn.
          refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
          apply Plus.le_plus_l.
        - do 6 intro; apply Hq; 
          rewrite deep_variables_q_unfold, Fset.mem_union, Bool.orb_true_iff; left; assumption.
      }
    * destruct f; try apply refl_equal.
      rewrite (Fecol.mem_eq_2
                 _ _ (clean_eval_equery_sigma (ExtAlg := ExtAlg) ip is ia (I := I') _ _ WI)).
      rewrite Fecol.mem_filter; [ | intros; apply refl_equal].
      rewrite Bool.andb_true_r.
      apply refl_equal.
  + (* Query_Pi *)
    assert (IHq : 
              forall t, 
                t inSE? eval_query ip I q = 
                (t inCE? (eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q)))).
    {
      apply (proj1 IHn).
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply Plus.le_plus_l.
      - do 6 intro; apply Hq; rewrite deep_variables_q_unfold; assumption.
    }
    rewrite algebra_to_ealgebra_unfold.
    cbv iota beta zeta.
    case_eq (Fset.mk_set (A T) l =S?= RA.sort q); intro Hs.
    * rewrite <- IHq, eval_query_unfold.
      cbv iota beta zeta.
      revert t; rewrite <- Feset.equal_spec; apply Feset.map_id.
      intros t Ht; apply mk_tuple_id.
      rewrite (Fset.equal_eq_1 _ _ _ _ (well_sorted_query ip WI _ _ Ht)).
      rewrite <- (Fset.equal_eq_1 _ _ _ _ Hs); apply Fset.equal_refl.
    * unfold pi_equery.
      rewrite (eval_query_unfold _ _ (Query_Pi _ _)), eval_equery_unfold.
      cbv beta iota zeta.
      rewrite filter_true; [ | intros; apply refl_equal].
      unfold interp_partition.
      rewrite map_map, eq_bool_iff, Fecol.mem_mk_col, Oeset.mem_bool_true_iff, Feset.mem_map.
      {
        split; intros [t1 [Ht Ht1]].
        - generalize (Feset.in_elements_mem _ _ _ Ht1); clear Ht1; rewrite IHq; intro Ht1.
          rewrite Fecol.mem_elements, Oeset.mem_bool_true_iff in Ht1.
          destruct Ht1 as [t2 [Ht1 Ht2]].
          exists (eval_codes_on_grp is ia (id_renaming ExtAlg (Fset.mk_set (A T) l)) (t2 :: nil)).
          split.
          + refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
            apply Oeset.compare_eq_sym.
            refine (Oeset.compare_eq_trans 
                      _ _ _ _ (eval_codes_on_grp_singleton_subset ExtAlg _ _ _ _) _).
            simpl_tac; apply mk_tuple_eq; [apply Fset.equal_refl | ].
            intros a Ha _; apply sym_eq; apply tuple_eq_dot; assumption.
          + rewrite in_map_iff; exists t2; split; trivial.
        - rewrite in_map_iff in Ht1.
          destruct Ht1 as [t2 [Ht1 Ht2]]; subst t1.
          generalize (Fecol.in_elements_mem _ _ Ht2); clear Ht2; rewrite <- IHq; intro Ht2.
          rewrite Feset.mem_elements, Oeset.mem_bool_true_iff in Ht2.
          destruct Ht2 as [t3 [Ht2 Ht3]].
          exists t3; split.
          + refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
            refine (Oeset.compare_eq_trans 
                      _ _ _ _ (eval_codes_on_grp_singleton_subset ExtAlg _ _ _ _) _).
            simpl_tac; apply mk_tuple_eq; [apply Fset.equal_refl | ].
            intros a Ha _; apply tuple_eq_dot; assumption.
          + assumption.
      } 
  + rewrite algebra_to_ealgebra_unfold.
    rewrite (eval_query_unfold _ _ (Query_NaturalJoin _ _)).
    rewrite (eval_equery_unfold _ _ _ _ (Equery_NaturalJoin _ _ _)).
    assert (IHq1 : 
              forall t, 
                t inSE? eval_query ip I q1 =
                (t inCE? (eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q1)))).
    {
      apply (proj1 IHn).
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply Plus.le_plus_l.
      - do 6 intro; apply Hq; 
        rewrite deep_variables_q_unfold, Fset.mem_union, Bool.orb_true_iff; left; assumption.
    }
    assert (IHq2 : 
              forall t, 
                t inSE? eval_query ip I q2 =
                (t inCE? eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q2))).
    {
      apply (proj1 IHn).
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply Plus.le_plus_r.
      - do 6 intro; apply Hq; 
        rewrite deep_variables_q_unfold, Fset.mem_union, Bool.orb_true_iff; right; assumption.
        }
    rewrite eq_bool_iff, mem_natural_join_set, mem_natural_join.
    split; intros [t1 [t2 [Ht1 [Ht2 [H Ht]]]]];
      exists t1; exists t2; repeat split; trivial.
    * rewrite <- IHq1; assumption.
    * rewrite <- IHq2; assumption.
    * rewrite IHq1; assumption.
    * rewrite IHq2; assumption.
  + rewrite algebra_to_ealgebra_unfold; unfold rename_equery.
    rewrite (eval_query_unfold _ _ (Query_Rename _ _)).
    assert (IHq : 
              forall t, 
                t inSE? eval_query ip I q =
                (t inCE? eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q))).
    {
      apply IHn.
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply le_S; apply le_n.
      - do 6 intro; apply Hq; assumption.
    }
    case_eq (Fset.for_all (A T)
                          (fun a : attribute T =>
                             Oset.eq_bool (OAtt T) a (apply_renaming T r a))
                          (RA.sort q)); intro Kr.
    * rewrite <- IHq.
      revert t; rewrite <- Feset.equal_spec.
      rewrite Fset.for_all_spec, forallb_forall in Kr.
      apply Feset.map_id.
      {
        intros t Ht; unfold rename_tuple; 
        refine (Oeset.compare_eq_trans 
                _ _ _ _ (mk_tuple_eq _ _ _ _ _ _ _) (mk_tuple_id _ _ (RA.sort q) _)).
        - unfold Fset.map; rewrite map_id, (Fset.equal_eq_1 _ _ _ _ (Fset.mk_set_idem _ _));
          [apply (well_sorted_query ip WI _ _ Ht) | ].
          intros a Ha; rewrite (Fset.elements_spec1 _ _ _ (well_sorted_query ip WI _ _ Ht)) in Ha.
          assert (Kra := Kr a Ha); rewrite Oset.eq_bool_true_iff in Kra; apply sym_eq; apply Kra.
        - intros a _ Ha.
          rewrite (Fset.elements_spec1 _ _ _ (well_sorted_query ip WI _ _ Ht)).
          assert (Kra := Kr a (Fset.mem_in_elements _ _ _ Ha)); 
            rewrite Oset.eq_bool_true_iff in Kra.
          case_eq (Oset.find (OAtt (RA.T RelAlg)) a
                             (map
                                (fun a0 : attribute (RA.T RelAlg) =>
                                   (apply_renaming (RA.T RelAlg) r a0, dot (RA.T RelAlg) t a0))
                                ({{{RA.sort q}}}))).
          + intros v Hv.
            assert (Kv := Oset.find_some _ _ _ Hv); rewrite in_map_iff in Kv.
            destruct Kv as [_a [Kv Ka]].
            injection Kv; clear Kv; intros Kv Ja; subst v.
            apply f_equal.
            rewrite <- Ja, <- (Oset.eq_bool_true_iff (OAtt T)); apply Kr; assumption.
          + intro Ka; apply False_rec.
            apply (Oset.find_none _ _ _ Ka (dot T t a)).
            rewrite in_map_iff; exists a; split; [ | apply Fset.mem_in_elements; assumption].
            apply f_equal2; [ | apply refl_equal].
            apply sym_eq; apply Kra.
        - apply (well_sorted_query ip WI _ _ Ht).
      }
    * rewrite eq_bool_iff, Feset.mem_map, eval_equery_unfold; cbv zeta.
      rewrite filter_true, Fecol.mem_mk_col, Oeset.mem_bool_true_iff; 
        [ | intros; apply refl_equal].
      unfold interp_partition; rewrite map_map.
      {
        split.
        - intros [t1 [Ht Ht1]].
          generalize (Feset.in_elements_mem _ _ _ Ht1); clear Ht1; intro Ht1.
          rewrite IHq, Fecol.mem_elements, Oeset.mem_bool_true_iff in Ht1.
          destruct Ht1 as [t2 [Ht1 Ht2]].
          exists (eval_codes_on_grp 
                    is ia
                    (rho_renaming ExtAlg (apply_renaming T (coerce_renaming r))
                                  (sort (algebra_to_ealgebra q))) (t2 :: nil)); split.
          + refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
            unfold rho_renaming.
            refine (Oeset.compare_eq_trans 
                      _ _ _ _ _ (Oeset.compare_eq_sym 
                                 _ _ _ (eval_codes_on_grp_singleton_rename ExtAlg _ _ _ _ _ _))).
            * apply rename_tuple_eq; [ | assumption].
              intros a Ha; unfold coerce_renaming, coerce_att; simpl_tac.
              rewrite map_id; [apply refl_equal | ].
              intros [a1 a2] _; apply refl_equal.
            * rewrite (Fset.equal_eq_2 _ _ _ _ (sort_algebra_to_ealgebra q)).
              apply (well_sorted_query ip WI).
              rewrite IHq.
              apply (Fecol.in_elements_mem _ _ Ht2).
          + rewrite in_map_iff; exists t2; split; trivial.
        - intros [t1 [Ht Ht1]]; rewrite in_map_iff in Ht1.
          destruct Ht1 as [t2 [Ht1 Ht2]]; subst t1.
          generalize (Fecol.in_elements_mem _ _ Ht2); clear Ht2; intro Ht2.
          rewrite <- IHq, Feset.mem_elements, Oeset.mem_bool_true_iff in Ht2.
          destruct Ht2 as [t3 [Ht2 Ht3]].
          exists t3; split; [ | assumption].
          refine (Oeset.compare_eq_trans _ _ _ _ Ht _).
          unfold rho_renaming.
          refine (Oeset.compare_eq_trans 
                    _ _ _ _ (eval_codes_on_grp_singleton_rename ExtAlg _ _ _ _ _ _) _).
          + rewrite (Fset.equal_eq_2 _ _ _ _ (sort_algebra_to_ealgebra q)).
            apply (well_sorted_query ip WI).
            rewrite Feset.mem_elements, Oeset.mem_bool_true_iff; exists t3; split; trivial.
          + apply rename_tuple_eq; [ | assumption].
            intros a Ha; unfold coerce_renaming, coerce_att; simpl_tac.
            rewrite map_id; [apply refl_equal | ].
            intros [a1 a2] _; apply refl_equal.
      }
  + assert (IHq1 : 
              forall t, 
                t inSE? eval_query ip I q1 =
                (t inCE? eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q1))).
    {
      apply (proj1 IHn).
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply Plus.le_plus_l.
      - do 6 intro; apply Hq; 
        rewrite deep_variables_q_unfold, Fset.mem_union, Bool.orb_true_iff; left; assumption.
    }
    assert (IHq2 : 
              forall t, 
                t inSE? eval_query ip I q2 =
                (t inCE? eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra q2))).
    {
      apply (proj1 IHn).
      - rewrite query_size_unfold in Hn.
        refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply Plus.le_plus_r.
      - do 6 intro; apply Hq; 
        rewrite deep_variables_q_unfold, Fset.mem_union, Bool.orb_true_iff; right; assumption.
    }
    rewrite (eval_query_unfold _ _ (Query_Set _ _ _)).
    rewrite algebra_to_ealgebra_unfold, eval_equery_unfold.
    rewrite (Fset.equal_eq_1 _ _ _ _ (sort_algebra_to_ealgebra _)).
    rewrite (Fset.equal_eq_2 _ _ _ _ (sort_algebra_to_ealgebra _)).
    simpl_tac; case (RA.sort q1 =S?= RA.sort q2);
      [ | rewrite Feset.empty_spec, Fecol.empty_spec; apply refl_equal].
    destruct s; simpl.
    * rewrite 2 Feset.mem_union, IHq1, IHq2, 2 Fecol.mem_mem_set; apply refl_equal.
    * rewrite 2 Feset.mem_union, IHq1, IHq2, 2 Fecol.mem_mem_set; apply refl_equal.
    * rewrite 2 Feset.mem_inter, IHq1, IHq2, 2 Fecol.mem_mem_set; apply refl_equal.
    * rewrite 2 Feset.mem_diff, IHq1, IHq2, 2 Fecol.mem_mem_set; apply refl_equal.
- intros f Hn Hf t l; unfold interp_formula, RA.interp_formula.
  rewrite formula_to_formula_unfold; destruct f; simpl.
  + apply refl_equal.
  + apply f_equal.
    rewrite map_map, <- map_eq; intros u Hu.
    destruct u as [[q m] | f k]; [trivial | ].
    destruct f as [c | a]; [destruct k; trivial | ]; simpl.
    destruct k as [ | u1 k]; [trivial | simpl].
    destruct u1 as [[q1 n1] | [c | a1] l1]; trivial.
    destruct k; simpl; trivial.
    apply f_equal2; [ | apply refl_equal].
    induction l as [ | [m1 t1] l]; [apply refl_equal | ]; simpl.
    case (Oset.eq_bool ON m1 n1); [apply refl_equal | ].
    apply IHl.
  + apply f_equal2.
    * apply (proj2 IHn); [apply (formula_size_conj_1 Hn) | ].
      do 6 intro; apply Hf; 
      rewrite deep_variables_f_unfold, Fset.mem_union, Bool.orb_true_iff; left; assumption.
    * apply (proj2 IHn); [apply (formula_size_conj_2 Hn) | ].
      do 6 intro; apply Hf; 
      rewrite deep_variables_f_unfold, Fset.mem_union, Bool.orb_true_iff; right; assumption.
  + apply f_equal; apply (proj2 IHn); [apply (formula_size_not_f Hn) | apply Hf].
  + destruct v as [qv nv]; simpl.
    assert (IHq : 
              forall t, 
                t inSE? eval_query ip I qv =
                (t inCE? eval_equery (ExtAlg := ExtAlg) ip is ia I' (algebra_to_ealgebra qv))).
    {
      apply IHn.
      - refine (Le.le_trans _ _ _ _ (formula_size_quant_v Hn)).
        simpl; apply le_S; apply le_plus_l.
      - do 6 intro; apply Hf; 
        rewrite deep_variables_f_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.orb_true_iff; 
        right; left; assumption.
    }
    apply (interp_quant_eq (OTuple T)).
    * intro x; rewrite <- Feset.mem_elements, IHq, Fecol.mem_elements; apply refl_equal.
    * intros x1 x2 Hx1 Hx.
      {
        apply trans_eq with 
        (Formula.interp_formula 
           (RA.OQ RelAlg) (OP RelAlg) 
           (RA.OSymbol RelAlg) ip (RA.interp_term (RA:=RelAlg))
           (fun q0 : query =>
              Feset.elements (Fecol.CSet (CTuple (RA.T RelAlg))) (eval_query ip I q0))
           (ivar_xts (fun _ : variable query => t) ((nv, x2) :: l)) f).
        - apply Formula.interp_formula_eq.
          + apply RA.interp_term_eq_rec.
          + intros u Hu; apply RA.interp_term_eq.
            intros y Hy; simpl.
            unfold ivar_xt; destruct y as [qy ny].
            case_eq (Oset.eq_bool ON nv ny); intro H.
            * rewrite Oset.eq_bool_true_iff in H; subst ny.
              assert (H : qv = qy).
              {
                apply (Hf qv nv qy nv).
                - rewrite deep_variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; left.
                  rewrite Oset.eq_bool_true_iff; apply refl_equal.
                - rewrite deep_variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; right.
                  rewrite Fset.mem_union, Bool.orb_true_iff; right.
                  assert (Aux1 := free_variables_f_variables_f 
                                    (RA.OQ RelAlg) (RA.OP RelAlg) (RA.OSymbol RelAlg) f).
                  assert (Aux2 := variables_f_deep_variables_f f).
                  rewrite Fset.subset_spec in Hu, Aux1, Aux2; apply Aux2; apply Aux1; apply Hu;
                  assumption.
                - apply refl_equal.
              }
              subst qy; rewrite Oset.compare_eq_refl; assumption.
            * case_eq (Oset.compare (OVR (RA.OQ RelAlg) (OP RelAlg) (RA.OSymbol RelAlg))
                                    (Vrbl qv nv) (Vrbl qy ny)); 
              try (intros; apply Oeset.compare_eq_refl; fail).
              intro Abs; rewrite Oset.compare_eq_iff in Abs.
              injection Abs; clear Abs; do 2 intro; subst qy ny.
              unfold Oset.eq_bool in H; rewrite Oset.compare_eq_refl in H; discriminate H.
        - apply trans_eq with
          (Formula.interp_formula 
             (OQ ExtAlg) (OP RelAlg) (OSymbol ExtAlg) ip (interp_term is) 
             (fun q0 : equery => Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is ia I' q0))
             (ivar_xts (fun _ : variable equery => t) ((nv, x2) :: l))
                      (formula_to_formula f)).
          + apply (proj2 IHn).
            * apply (formula_size_quant_f Hn).
            * do 6 intro; apply Hf;
              rewrite deep_variables_f_unfold, Fset.add_spec, Fset.mem_union, 2 Bool.orb_true_iff;
                do 2 right; assumption.
          + apply Formula.interp_formula_eq; [apply interp_term_eq_rec | ].
            intros u Hu.
            apply (interp_term_eq (ExtAlg := ExtAlg)).
            intros y Hy; simpl.
            unfold ivar_xt; destruct y as [qy ny].
            case_eq (Oset.eq_bool ON nv ny); intro H.
            * rewrite Oset.eq_bool_true_iff in H; subst ny.
              assert (H : algebra_to_ealgebra qv = qy).
              {
                assert (Aux1 := free_variables_f_variables_f 
                                   (OQ ExtAlg) (OP RelAlg) (OSymbol ExtAlg)
                                   (formula_to_formula f)).
                rewrite Fset.subset_spec in Hu, Aux1.
                assert (Ku := Aux1 _ (Hu _ Hy)).
                assert (Aux2 := variables_f_formula_to_formula f).
                rewrite Fset.equal_spec in Aux2.
                rewrite Aux2, Fset.mem_map in Ku.
                destruct Ku as [[q1 n1] [Hv H1]].
                injection Hv; clear Hv; intros Hn1 Hq1; subst n1; rewrite <- Hq1.
                apply f_equal.
                apply (Hf qv nv q1 nv).
                - rewrite deep_variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; left.
                  rewrite Oset.eq_bool_true_iff; apply refl_equal.
                - rewrite deep_variables_f_unfold, Fset.add_spec, Bool.orb_true_iff; right.
                  rewrite Fset.mem_union, Bool.orb_true_iff; right.
                  assert (Aux3 := variables_f_deep_variables_f f).
                  rewrite Fset.subset_spec in Aux3; apply Aux3; assumption.
                - apply refl_equal.
              } 
              rewrite <- H, Oset.compare_eq_refl.
              apply Oeset.compare_eq_refl.
            * case_eq (Oset.compare (OVR (OQ ExtAlg) (OP RelAlg) (OSymbol ExtAlg))
                                    (Vrbl (algebra_to_ealgebra qv) nv) (Vrbl qy ny));
              intro K; simpl_tac; rewrite K; try apply Oeset.compare_eq_refl.
              rewrite Oset.compare_eq_iff in K.
              injection K; clear K; do 2 intro; subst ny.
              unfold Oset.eq_bool in H; rewrite Oset.compare_eq_refl in H.
              discriminate H.
      } 
Qed.

(** * Soundness lemma for the embedding: semantics preservation *)
Lemma eval_query_translation :
  forall ip is (ia : aggregate -> list (value T) -> value T) I,
    well_sorted_instance RelAlg I ->
    let I' := fun r => Fecol.Fset (I r) in
  forall q t, 
    t inSE? (eval_query ip I q) = 
    (t inCE? (eval_equery 
                (ExtAlg := ExtAlg) ip is ia I' 
                (algebra_to_ealgebra (linearize_q (1 + max_q q)%N (n_q q) q)))).
Proof.
intros ip is ia I WI I' q t.
rewrite <- (proj1 (eval_query_translation_aux ip is ia WI _) 
                  (linearize_q (1 + max_q q)%N (n_q q) q) (le_n _)). 
- assert (Aux := proj1 (linearize_q_is_ok_etc RelAlg ip I (query_size q)) q (le_refl _) 
                       (1 + max_q q)%N (n_q q)).
  rewrite Feset.equal_spec in Aux; rewrite Aux.
  + apply refl_equal.
  + rewrite N.add_1_l, N.lt_succ_r.
    apply N.le_refl.
  + apply n_q_is_one_to_one.
- apply linearize_q_linearize.
  + rewrite N.add_1_l, N.lt_succ_r.
    apply N.le_refl.
  + apply n_q_is_one_to_one.
Qed.

End Embedding.

(** OCaml extraction (#<a href="extended_algebra.ml">.ml</a>#, #<a href="extended_algebra.mli">.mli</a>#) *)
Extraction "extended_algebra.ml" EA.eval_equery algebra_to_ealgebra.

