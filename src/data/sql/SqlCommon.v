(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Require Import List.
Require Import OrderedSet FiniteSet FiniteBag FiniteCollection ListSort ListPermut 
        Partition DExpressions FlatData2.

Set Implicit Arguments.

Section Sec.

Hypothesis T : Tuple.Rcd.

Import Tuple.

Notation symb := (symbol T).
Notation aggregate := (aggregate T).
Notation value := (value T).
Notation attribute := (attribute T).
Notation tuple := (tuple T).
Notation funterm := (funterm T).
Notation aggterm := (aggterm T).

Notation OSymb := (OSymb T).
Notation OAggregate := (OAgg T).

Notation "s1 '=PE=' s2" := (_permut (fun x y => Oeset.compare (OTuple _) x y = Eq) s1 s2) (at level 70, no associativity).

Inductive group_by : Type :=
  | Group_By : list funterm -> group_by
  | Group_Fine.

(** * SQL_COQ Syntax *)
Inductive select : Type := 
  | Select_As : aggterm -> attribute -> select.

Inductive select_item : Type :=
  | Select_Star 
  | Select_List : list select -> select_item.

Definition env_type := (list (Fset.set (A T) * group_by * list tuple)).

Definition env_t (env : env_type) t := (labels T t, Group_Fine, t :: nil) :: env. 

Definition env_g (env : env_type) g s := (match quicksort (OTuple T) s with 
                                          | nil => Fset.empty _ 
                                          | t :: _ => labels T t 
                                          end, g, s) :: env.

Lemma extract_from_groups :
  forall (x1 x2 : list tuple), 
    x1 =PE= x2 -> 
    comparelA (Oeset.compare (OTuple T)) (quicksort (OTuple T) x1) (quicksort (OTuple T) x2) = Eq.
Proof.
intros x1 x2 Hx.
apply (sort_is_unique (OA := OTuple T)).
- apply quick_sorted.
- apply quick_sorted.
- apply Oeset.permut_trans with x1.
  + apply Oeset.permut_sym; apply (quick_permut (OTuple T)).
  + apply Oeset.permut_trans with x2; [assumption | ].
    apply (quick_permut (OTuple T)).
Qed.

Lemma env_slice_eq_1 :
  forall x1 x2, Oeset.compare (OLTuple T) x1 x2 = Eq ->
   match quicksort (OTuple T) x1 with
   | nil => emptysetS
   | t0 :: _ => labels T t0
   end =S=
   match quicksort (OTuple T) x2 with
   | nil => emptysetS
   | t0 :: _ => labels T t0
   end.
Proof.
intros l1 l2 Hl.
rewrite <- (compare_list_t (OTuple T)) in Hl.
assert (H1 := extract_from_groups Hl).
set (q1 := quicksort (OTuple T) l1) in *.
set (q2 := quicksort (OTuple T) l2) in *.
clearbody q1 q2; revert q2 H1.
induction q1 as [ | x1 q1]; intros [ | x2 q2] H1; try discriminate H1.
- simpl; apply Fset.equal_refl.
- simpl in H1.
  case_eq (Oeset.compare (OTuple T) x1 x2); intro Hx; rewrite Hx in H1; try discriminate H1.
  rewrite tuple_eq in Hx; apply (proj1 Hx).
Qed.


Definition interp_dot_env_slice (l : listT) (a : attribute) :=
  match quicksort (OTuple T) l with
    | nil => None
    | t :: _ => if a inS? labels T t 
                 then Some (dot T t a)
                 else None
  end.

Lemma interp_dot_env_slice_unfold :
  forall l a, interp_dot_env_slice l a =
  match quicksort (OTuple T) l with
    | nil => None
    | t1 :: _ => if a inS? labels T t1 
                 then Some (dot T t1 a)
                 else None
  end.
Proof.
intros l a; unfold interp_dot_env_slice; apply refl_equal.
Qed.

Definition equiv_abstract_env_slice (e1 e2 : (Fset.set (A T) * group_by)) := 
  match e1, e2 with
    | (sa1, g1), (sa2, g2) => 
      sa1 =S= sa2 /\ g1 = g2 
  end.

Definition equiv_abstract_env := equiv_env equiv_abstract_env_slice.

Lemma equiv_abstract_env_slice_refl :
  forall e1, equiv_abstract_env_slice e1 e1.
Proof.
intros [sa g1]; simpl; repeat split.
apply Fset.equal_refl.
Qed.

Lemma equiv_abstract_env_refl : forall e, equiv_abstract_env e e.
Proof.
apply equiv_env_refl.
apply equiv_abstract_env_slice_refl.
Qed.

Definition equiv_env_slice (e1 e2 : (Fset.set (A T) * group_by * (list tuple))) := 
  match e1, e2 with
    | (sa1, g1, x1), (sa2, g2, x2) => 
      sa1 =S= sa2 /\ g1 = g2 /\ x1 =PE= x2
  end.

Lemma equiv_env_slice_refl :
  forall e1, equiv_env_slice e1 e1.
Proof.
intros [[sa g1] l1]; simpl; repeat split.
- apply Fset.equal_refl.
- apply Oeset.permut_refl.
Qed.

Lemma equiv_env_slice_sym :
  forall e1 e2, equiv_env_slice e1 e2 <-> equiv_env_slice e2 e1.
Proof.
intros [[sa1 g1] l1] [[sa2 g2] l2]; split; intros [H1 [H2 H3]]; simpl; repeat split.
- rewrite Fset.equal_spec in H1; rewrite Fset.equal_spec; intro; rewrite H1; trivial.
- subst; trivial.
- apply Oeset.permut_sym; assumption.
- rewrite Fset.equal_spec in H1; rewrite Fset.equal_spec; intro; rewrite H1; trivial.
- subst; trivial.
- apply Oeset.permut_sym; assumption.
Qed.

Definition equiv_env := equiv_env equiv_env_slice.

Lemma equiv_env_refl : forall env, equiv_env env env.
Proof.
apply equiv_env_refl.
apply equiv_env_slice_refl.
Qed.


Hypothesis interp_symb : symb -> list value -> value.
Hypothesis interp_aggregate : aggregate -> list value -> value.

Definition interp_funterm_ := 
  (interp_funterm 
     interp_symb
     (fun (x : (Fset.set (A T) * group_by * _)) a => 
        match x with (_, _, l) => interp_dot_env_slice l a end)
     (fun a => default_value T (type_of_attribute T a))).

Definition interp_aggterm_ := 
  (interp_aggterm (OVal T) OSymb (A T) interp_symb interp_aggregate
     (fun (x : (Fset.set (A T) * group_by * _)) a => 
        match x with 
          | (_,_,l) => interp_dot_env_slice l a 
        end)
     (fun x => match x with (sa, _, _) => sa end)
     (fun x => match x with 
              | (sa, Group_Fine, _) => map (fun a => F_Dot _ _ a) (Fset.elements _ sa) 
              | (_, Group_By g,_) => g 
            end)
     (fun x => match x with 
              | (sa, g, l) =>
                List.map (fun t => (sa, g, t :: nil)) (quicksort (OTuple T) l)
            end)
     (fun a => default_value T (type_of_attribute T a))).

(** * Building a flat tuple from a tuple or a group of tuples by applying a select part *)
Definition projection (env : list (setA * group_by * listT)) (las : select_item) :=
  match las with
    | Select_Star => 
      match env with
        | nil => default_tuple T (Fset.empty _)
        | (sa, gby, l) :: _ =>
          match l with
            | t :: nil => t
            | g =>
              match quicksort (OTuple T) g with
                | t :: _ => t
                | nil => default_tuple T (Fset.empty _)
              end
          end
      end
    | Select_List las =>
      let la' :=
          List.map 
            (fun x => match x with Select_As e a => (a, e) end) 
            las in
       mk_tuple T 
          (Fset.mk_set (A T) (List.map (@fst _ _) la'))
                  (fun a => 
                     match Oset.find (OAtt T) a la' with 
                         | Some e => interp_aggterm_ env e
                         | None => dot T (default_tuple T (Fset.empty _)) a
                     end)
  end.

(** * Partitioning a set of tuples into homogeneous lists w.r.t to a set of fun_term expressions *)

(*
Definition make_groups env (s : Febag.bag (Fecol.CBag (CTuple T))) gby := 
  match gby with
    | Group_By g =>
      partition_list_expr 
        (A := tuple T) (value := value T) (FVal T) (Febag.elements _ s) 
        (map (fun f => fun t => interp_funterm_ (env_t env t) f) g)
    | Group_Fine => List.map (fun x => x :: nil) (Febag.elements _ s)  
  end.
*)
Definition make_groups env (s : list tuple) gby := 
  match gby with
    | Group_By g =>
      partition_list_expr 
        (A := tuple) (value := value) (FVal T) s 
        (map (fun f => fun t => interp_funterm_ (env_t env t) f) g)
    | Group_Fine => List.map (fun x => x :: nil) s  
  end.

Lemma interp_dot_env_slice_eq :
  forall a l1 l2, l1 =PE= l2 -> interp_dot_env_slice l1 a = interp_dot_env_slice l2 a.
Proof.
  intros a l1 l2 H; unfold interp_dot_env_slice.
  assert (H1 := extract_from_groups H).
  set (q1 := quicksort (OTuple T) l1) in *.
  set (q2 := quicksort (OTuple T) l2) in *.
  clearbody q1 q2; revert q2 H1.
  induction q1 as [ | x1 q1]; intros [ | x2 q2] H1; try discriminate H1.
  + simpl; trivial.
  + simpl in H1.
    case_eq (Oeset.compare (OTuple T) x1 x2); intro Hx; rewrite Hx in H1; try discriminate H1.
    rewrite tuple_eq in Hx; rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 Hx)).
    case_eq (a inS? labels T x1); intro Ha; [ | apply refl_equal].
    rewrite <- (proj2 Hx); apply refl_equal.
Qed.

Lemma interp_funterm_eq :
  forall f env1 env2, equiv_env env1 env2 -> 
                      interp_funterm_ env1 f = interp_funterm_ env2 f.
Proof.
intros f e1 e2 H.
refine (interp_funterm_eq _ _ _ _ _ H).
intros a [[sa1 g1] l1] [[sa2 g2] l2] K; simpl in K.
apply interp_dot_env_slice_eq; apply (proj2 K).
Qed.

Lemma interp_aggterm_eq :
  forall e env1 env2, equiv_env env1 env2 -> interp_aggterm_ env1 e = interp_aggterm_ env2 e.
Proof.
intros e env1 env2 H.
refine (interp_aggterm_eq (OVal T) OSymb (A T) _ _ _ _ _ _ _ _ _ _ _ _ _ H).
- apply equiv_env_slice_sym.
- intros [[sa1 g1] l1] [[sa2 g2] l2] K; simpl in H; apply (proj1 K).
- intros [[sa1 g1] l1] [[sa2 g2] l2] K; simpl in K.
  assert (H1 := extract_from_groups (proj2 (proj2 K))).
  set (q1 := quicksort (OTuple T) l1) in *.
  set (q2 := quicksort (OTuple T) l2) in *.
  clearbody q1 q2; revert q2 H1.
  induction q1 as [ | x1 q1]; intros [ | x2 q2] H1; try discriminate H1.
  + simpl; trivial.
  + simpl in H1.
    case_eq (Oeset.compare (OTuple T) x1 x2); intro Hx; rewrite Hx in H1; try discriminate H1.
    simpl; constructor 2; [ | apply IHq1; assumption].
    simpl; split; [apply (proj1 K) | ].
    simpl; split; [apply (proj1 (proj2 K)) | ].
    apply Oeset.permut_refl_alt; simpl; rewrite Hx; trivial.
- intros [[sa1 g1] l1] [[sa2 g2] l2] K f; simpl in K.
  destruct K as [H1 [H2 H3]].
  subst g2; destruct g1 as [g1 | ]; [split; exact (fun h => h) | ].
  rewrite <- (Fset.elements_spec1 _ _ _ H1); split; exact (fun h => h).
- intros a [[sa1 g1] l1] [[sa2 g2] l2] K; simpl in K; destruct K as [H1 [H2 H3]].
  apply interp_dot_env_slice_eq; assumption.
Qed.

Lemma projection_eq :
  forall s env1 env2, equiv_env env1 env2 -> projection env1 s =t= projection env2 s.
Proof.
intros s env1 env2 He; unfold projection.
destruct s as [ | s].
- destruct env1 as [ | [[sa1 gb1] x1] env1]; destruct env2 as [ | [[sa2 gb2] x2] env2];
  try inversion He.
  + apply Oeset.compare_eq_refl.
  + subst.
    simpl in H2; destruct H2 as [Hsa [Hg Hx]].
    assert (Kx := extract_from_groups Hx).
    assert (L := _permut_length Hx).
    destruct x1 as [ | u1 x1]; destruct x2 as [ | u2 x2]; try discriminate L.
    * apply Oeset.compare_eq_refl.
    * {
        destruct x1 as [ | t1 x1]; destruct x2 as [ | t2 x2]; try discriminate L.
        - simpl in Kx.
          case_eq (Oeset.compare (OTuple T) u1 u2); 
            intro Hu; rewrite Hu in Kx; try discriminate Kx; trivial.
        - destruct (quicksort (OTuple T) (u1 :: t1 :: x1)) as [ | s1 l1]; 
          destruct (quicksort (OTuple T) (u2 :: t2 :: x2)) as [ | s2 l2]; try discriminate Kx.
          + apply Oeset.compare_eq_refl.
          + simpl in Kx.
            destruct (Oeset.compare (OTuple T) s1 s2); try discriminate Kx; trivial.
      }
- apply mk_tuple_eq_2.
  intros a Ha.
  case_eq (Oset.find 
               (OAtt T) a
               (map (fun x : select => match x with
                                         | Select_As e a0 => (a0, e)
                                       end) s));
    [intros e _He | intros _; apply refl_equal].
  apply interp_aggterm_eq; trivial.
Qed.

Lemma projection_app :
  forall s1 s2 env, 
    projection env (Select_List (s1 ++ s2)) =t= 
    join_tuple T (projection env (Select_List s1)) (projection env (Select_List s2)).
Proof.
intros s1 s2 env; unfold projection, join_tuple.
apply mk_tuple_eq.
- rewrite Fset.equal_spec; intro a.
  rewrite Fset.mem_mk_set, map_map, map_app, Oset.mem_bool_app.
  rewrite Fset.mem_union, 2 (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
  rewrite 2 map_map, 2 Fset.mem_mk_set.
  apply refl_equal.
- intros a Ha Ka; rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
  rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_1 _ _ _ _ (labels_mk_tuple _ _ _))) in Ka.
  rewrite (Fset.mem_eq_2 _ _ _ (Fset.union_eq_2 _ _ _ _ (labels_mk_tuple _ _ _))) in Ka.
  rewrite Fset.mem_union, Bool.orb_true_iff in Ka.
  case_eq (a inS? Fset.mk_set (A T)
             (map fst (map (fun x : select => match x with
                                              | Select_As e a => (a, e)
                                              end) s1))); intro Ja.
  + rewrite dot_mk_tuple; [ | apply Ja].
    rewrite map_app, Oset.find_app.
    case_eq (Oset.find (OAtt T) a
                       (map (fun x : select => match x with
                                               | Select_As e a0 => (a0, e)
                                               end) s1)).
    * intros; apply refl_equal.
    * intro La.
      apply False_rec; apply (Oset.find_none_alt _ _ _ La).
      rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff in Ja; apply Ja.
  + rewrite map_app, Oset.find_app.
    case_eq (Oset.find (OAtt T) a
                       (map (fun x : select => match x with
                                               | Select_As e a0 => (a0, e)
                                               end) s1)).
    * intros e1 La.
      apply False_rec.
      rewrite Fset.mem_mk_set, <- Bool.not_true_iff_false in Ja.
      apply Ja.
      rewrite Oset.mem_bool_true_iff, map_map, in_map_iff.
      assert (Ma := Oset.find_some _ _ _ La); rewrite in_map_iff in Ma.
      destruct Ma as [x [Hx Kx]]; eexists; split; [ | apply Kx]; rewrite Hx; apply refl_equal.
    * intro La; rewrite dot_mk_tuple; [apply refl_equal | ].
      rewrite Ja in Ka; destruct Ka as [Ka | Ka]; [discriminate Ka | assumption].
Qed.

Lemma projection_sub :
  forall env t t' rho, 
    labels T t subS labels T t' -> 
    (forall a, a inS labels T t -> dot T t a = dot T t' a) ->
    (forall a e, In (Select_As e a) rho -> 
                 match e with 
                 | A_Expr _ (F_Dot _ _ b) => b inS? labels T t 
                 | _ => false 
                 end = true) ->
    projection (env_g env Group_Fine (t' :: nil)) (Select_List rho) =t= 
    projection (env_g env Group_Fine (t :: nil)) (Select_List rho).
Proof.
intros env t t' rho H1 H2 H3.
rewrite tuple_eq; split; [simpl | ].
- rewrite (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)),
    (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
  apply Fset.equal_refl.
- intros a;
    case_eq (a inS? Fset.mk_set (A T)
               (map fst (map (fun x : select => match x with
                                                | Select_As e a0 => (a0, e)
                                                end) rho))); intro Ha;
      [ | unfold dot; simpl;
          rewrite 2 (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)), Ha; apply refl_equal].
  simpl; do 2 (rewrite dot_mk_tuple; [ | assumption]).
  case_eq ( Oset.find (OAtt T) a
      (map (fun x : select => match x with
                              | Select_As e a0 => (a0, e)
                              end) rho)); [ | intros _; apply refl_equal].
  intros e He.
  assert (Ke := Oset.find_some _ _ _ He).
  rewrite in_map_iff in Ke.
  destruct Ke as [[_e _a] [Ke Je]]; injection Ke; clear Ke; do 2 intro; subst _a _e.
  assert (Le := H3 _ _ Je).
  destruct e as [f | | ]; try discriminate Le.
  destruct f as [ | b | ]; try discriminate Le.
  simpl; unfold interp_dot_env_slice; simpl.
  rewrite Fset.subset_spec in H1.
  rewrite (H1 _ Le), Le.
  apply sym_eq; apply H2; assumption.
Qed.

End Sec.
