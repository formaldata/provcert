(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

(** printing inS? $\in_?$ #∈<SUB>?</SUB># *)
(** printing inS $\in$ #∈# *)(** printing subS? $\subseteq_?$ #⊆<SUB>?</SUB># *)
(** printing subS $\subseteq$ #⊆# *)
(** printing unionS $\cup$ #⋃# *)
(** printing interS $\cap$ #⋂# *)
(** printing inI $\in_I$ #∈<SUB><I>I</I></SUB># *)
(** printing theta $\theta$ #θ# *)
(** printing nu1 $\nu_1$ #ν<SUB><I>1</I></SUB># *)
(** printing nu $\nu$ #ν# *)
(** printing mu $\mu$ #μ# *)
(** printing sigma $\sigma$ #σ# *)
(** printing -> #⟶# *)
(** printing <-> #⟷# *)
(** printing => #⟹# *)
(** printing (emptysetS) $\emptyset$ #Ø# *)
(** printing emptysetS $\emptyset$ #Ø# *)
(** printing {{ $\{$ #{# *)
(** printing }} $\}$ #}# *)

Require Import Relations SetoidList List String Ascii Bool ZArith NArith.

Require Import BasicFacts ListFacts ListPermut ListSort OrderedSet 
        FiniteSet FiniteBag FiniteCollection Data FlatData Tree Term DExpressions Bool3 Formula 
        Partition SqlCommon.

(*************** SQL (Postgres) Fragment handled so far ******************
 SELECT [ ALL | DISTINCT ] * | expression [ [ AS ] output_name ] [, ...]
 [ FROM from_item [, ...] ]
 [ WHERE condition ]
 [ GROUP BY expression [, ...] ]
 [ HAVING condition [, ...] ]
 [ { UNION | INTERSECT | EXCEPT } [ ALL | DISTINCT ] select ]
   -- [ ORDER BY expression [ ASC | DESC | USING operator ] 
   -- [ NULLS { FIRST | LAST }                        ] [, ...] ]
   --     [ FETCH { FIRST | NEXT } [ count ] { ROW | ROWS } ONLY ]
   --     [ FOR { UPDATE | SHARE } [ OF table_name [, ...] ] [ NOWAIT ] [...] ]

where from_item can be one of:

    [ ONLY ] table_name [ * ] [ [ AS ] alias [ ( column_alias [, ...] ) ] ]
    ( select ) [ AS ] alias [ ( column_alias [, ...] ) ]
    with_query_name [ [ AS ] alias [ ( column_alias [, ...] ) ] ]
--     function_name ( [ argument [, ...] ] ) [ AS ] alias [ ( column_alias [, .--      ..] | column_definition [, ...] ) ]
--     function_name ( [ argument [, ...] ] ) AS ( column_definition [, ...] )
    from_item NATURAL JOIN from_item // on est conforme au modele theorique

and with_query is:

--  with_query_name [ ( column_name [, ...] ) ]
    AS ( select | insert | update | delete )

**************************************************************************)

Section Sec.

Hypothesis T : Tuple.Rcd.

Hypothesis relname : Type.
Hypothesis ORN : Oset.Rcd relname.

Hypothesis predicate : Type.
Hypothesis symb : Type.
Hypothesis aggregate : Type.
Hypothesis OPredicate : Oset.Rcd predicate.
Hypothesis OSymb : Oset.Rcd symb.
Hypothesis OAggregate : Oset.Rcd aggregate.

Import Tuple.

Notation E := (E T OSymb OAggregate).
Notation group_by := (group_by T OSymb OAggregate).
Arguments Group_By {T} {symb} {aggregate} {OSymb} {OAggregate} l.
Arguments Group_Fine {T} {symb} {aggregate} {OSymb} {OAggregate}.
Notation select := (select T OSymb OAggregate).
Notation select_item := (select_item T OSymb OAggregate).
Arguments Select_As {T} {symb} {aggregate} {OSymb} {OAggregate} e a.
Arguments Select_Star {T} {symb} {aggregate} {OSymb} {OAggregate}.
Notation env_type := (env_type T OSymb OAggregate).

(** * SQL_COQ Syntax *)
Inductive att_renaming : Type :=
  | Att_As : attribute T -> attribute T -> att_renaming.

Inductive att_renaming_item : Type :=
  | Att_Ren_Star
  | Att_Ren_List : list att_renaming -> att_renaming_item.

Inductive sql_query : Type := 
  | Sql_Table : relname -> sql_query 
  | Sql_Set : set_op -> sql_query -> sql_query -> sql_query
  | Sql_Select : 
      (** select *) select_item -> 
      (** from *) list sql_from_item -> 
      (** where *) sql_formula -> 
      (** group by *) group_by ->
      (** having *) sql_formula -> sql_query

with sql_from_item : Type := 
  | From_Item : sql_query -> att_renaming_item -> sql_from_item

with sql_formula : Type :=
  | Sql_Conj : and_or -> sql_formula -> sql_formula -> sql_formula 
  | Sql_Not : sql_formula -> sql_formula
  | Sql_Atom : sql_atom -> sql_formula

with sql_atom : Type :=
  | Sql_True : sql_atom
  | Sql_Pred : predicate -> list (aggterm E) -> sql_atom
  | Sql_Quant : quantifier -> predicate -> list (aggterm E) -> sql_query -> sql_atom
  | Sql_In : list select -> sql_query -> sql_atom
  | Sql_Exists : sql_query -> sql_atom.

Inductive All : Type :=
| All_relname : relname  -> All
| All_attribute : attribute T -> All
| All_predicate : predicate -> All
| All_funterm : funterm E -> All
| All_aggterm : aggterm E -> All.

Definition tree_of_attribute (a : attribute T) : tree All := Leaf (All_attribute a).

Definition tree_of_select s : tree All :=
  match s with
    | Select_As e a => Node 0 (Leaf (All_aggterm e) :: tree_of_attribute a :: nil)
  end.
         
Definition tree_of_select_item s : tree All :=
  match s with
    | Select_Star => Node 1 nil
    | Select_List s => Node 2 (map tree_of_select s)
  end.

Definition tree_of_att_renaming x : tree All :=
  match x with
    | Att_As a1 a2 => Node 3 (tree_of_attribute a1 :: tree_of_attribute a2 :: nil)
  end.

Definition tree_of_att_renaming_item r : tree All :=
  match r with
    | Att_Ren_Star => Node 4 nil
    | Att_Ren_List r => Node 5 (map tree_of_att_renaming r)
  end.

Definition tree_of_groupby g : tree All :=
  match g with
    | Group_By l => Node 6 (map (fun x => Leaf (All_funterm x)) l)
    | Group_Fine => Node 7 nil
  end.

Fixpoint tree_of_sql_query (sq : sql_query) : tree All :=
  match sq with
    | Sql_Table r => Node 8 (Leaf (All_relname r) :: nil)
    | Sql_Set o sq1 sq2 =>
      Node
        (match o with | Union => 9 | Inter => 10 | Diff => 11 | UnionMax => 22 end)
        (tree_of_sql_query sq1 :: tree_of_sql_query sq2 :: nil)
    | Sql_Select s f w g h =>
      let tree_of_sql_from_item :=
          fun f =>
            match f with
              | From_Item sq rho => 
                Node 12 (tree_of_sql_query sq :: tree_of_att_renaming_item rho :: nil)
            end in
      let tree_of_sql_formula :=
          (fix tree_of_sql_formula f : tree All :=
             match f with
               | Sql_Conj a f1 f2 =>
                 Node
                   (match a with And_F => 13 | Or_F => 14 end)
                   (tree_of_sql_formula f1 :: tree_of_sql_formula f2 :: nil)
               | Sql_Not f => Node 15 (tree_of_sql_formula f :: nil)
               | Sql_Atom a =>
                 let tree_of_sql_atom a :=
                     match a with
                       | Sql_True => Node 16 nil
                       | Sql_Pred p l =>
                         Node 17 (Leaf (All_predicate p) :: map (fun x => Leaf (All_aggterm x)) l)
                       | Sql_Quant qf p l q =>
                         Node
                           (match qf with Forall_F => 18 | Exists_F => 19 end)
                           (Leaf (All_predicate p) 
                                 :: tree_of_sql_query q 
                                 :: map (fun x => Leaf (All_aggterm x)) l)
                       | Sql_In s q =>
                         Node 20 (tree_of_select_item (Select_List s) :: tree_of_sql_query q :: nil)
                       | Sql_Exists q => Node 23 (tree_of_sql_query q :: nil)
                     end in
                 Node 21 (tree_of_sql_atom a :: nil)
             end) in
      Node 13
           (tree_of_select_item s ::
           Node 6 (map tree_of_sql_from_item f) ::
           tree_of_sql_formula w ::
           tree_of_groupby g ::
           tree_of_sql_formula h :: nil)
  end.

Definition tree_of_sql_from_item f : tree All :=
  match f with
    | From_Item sq rho => Node 12 (tree_of_sql_query sq :: tree_of_att_renaming_item rho :: nil)
  end.

Definition tree_of_sql_atom a :=
  match a with
    | Sql_True => Node 16 nil
    | Sql_Pred p l =>
      Node 17 (Leaf (All_predicate p) :: map (fun x => Leaf (All_aggterm x)) l)
    | Sql_Quant qf p l q =>
      Node
        (match qf with Forall_F => 18 | Exists_F => 19 end)
        (Leaf (All_predicate p) :: tree_of_sql_query q :: map (fun x => Leaf (All_aggterm x)) l)
    | Sql_In s q =>
      Node 20 (tree_of_select_item (Select_List s) :: tree_of_sql_query q :: nil)
    | Sql_Exists q => Node 23 (tree_of_sql_query q :: nil)
  end.

Fixpoint tree_of_sql_formula f : tree All :=
  match f with
    | Sql_Conj a f1 f2 =>
      Node
        (match a with And_F => 13 | Or_F => 14 end)
        (tree_of_sql_formula f1 :: tree_of_sql_formula f2 :: nil)
    | Sql_Not f => Node 15 (tree_of_sql_formula f :: nil)
    | Sql_Atom a => Node 21 (tree_of_sql_atom a :: nil)
  end.

Lemma tree_of_sql_from_item_unfold :
  forall f, tree_of_sql_from_item f =
       match f with
         | From_Item sq rho => 
           Node 12 (tree_of_sql_query sq :: tree_of_att_renaming_item rho :: nil)
       end.
Proof.
intros f; case f; intros; apply refl_equal.
Qed.

Lemma tree_of_sql_atom_unfold :
  forall a, tree_of_sql_atom a = 
  match a with
    | Sql_True => Node 16 nil
    | Sql_Pred p l =>
      Node 17 (Leaf (All_predicate p) :: map (fun x => Leaf (All_aggterm x)) l)
    | Sql_Quant qf p l q =>
      Node
        (match qf with Forall_F => 18 | Exists_F => 19 end)
        (Leaf (All_predicate p) :: tree_of_sql_query q :: map (fun x => Leaf (All_aggterm x)) l)
    | Sql_In s q =>
      Node 20 (tree_of_select_item (Select_List s) :: tree_of_sql_query q :: nil)
    | Sql_Exists q => Node 23 (tree_of_sql_query q :: nil)
  end.
Proof.
  intros a; case a; intros; apply refl_equal.
Qed.

Lemma tree_of_sql_formula_unfold :
  forall f, tree_of_sql_formula f =
  match f with
    | Sql_Conj a f1 f2 =>
      Node
        (match a with And_F => 13 | Or_F => 14 end)
        (tree_of_sql_formula f1 :: tree_of_sql_formula f2 :: nil)
    | Sql_Not f => Node 15 (tree_of_sql_formula f :: nil)
    | Sql_Atom a => Node 21 (tree_of_sql_atom a :: nil)
  end.
Proof.
intros f; case f; intros; apply refl_equal.
Qed.

Lemma tree_of_sql_query_unfold :
  forall q, tree_of_sql_query q =
  match q with
    | Sql_Table r => Node 8 (Leaf (All_relname r) :: nil)
    | Sql_Set o sq1 sq2 =>
      Node
        (match o with | Union => 9 | Inter => 10 | Diff => 11 | UnionMax => 22 end)
        (tree_of_sql_query sq1 :: tree_of_sql_query sq2 :: nil)
    | Sql_Select s f w g h =>
      Node 13
           (tree_of_select_item s ::
           Node 6 (map tree_of_sql_from_item f) ::
           tree_of_sql_formula w ::
           tree_of_groupby g ::
           tree_of_sql_formula h :: nil)
  end.
Proof.
  intros q; case q; intros; apply refl_equal.
Qed.

(** * Syntactic comparison of queries *)


Open Scope N_scope.

Definition N_of_All (a : All) : N :=
  match a with
    | All_relname _ => 0
    | All_attribute _ => 1
    | All_predicate _ => 2
    | All_funterm _ => 3
    | All_aggterm _ => 4
  end.

Close Scope N_scope.

Definition all_compare a1 a2 :=
   match Ncompare (N_of_All a1) (N_of_All a2) with
   | Lt => Lt
   | Gt => Gt
   | Eq =>
       match a1, a2 with
       | All_relname r1, All_relname r2 => Oset.compare ORN r1 r2
       | All_attribute a1, All_attribute a2 => Oset.compare (OAtt T) a1 a2
       | All_predicate p1, All_predicate p2 => Oset.compare OPredicate p1 p2
       | All_funterm f1, All_funterm f2 => Oset.compare (OFun E) f1 f2
       | All_aggterm a1, All_aggterm a2 => Oset.compare (OAgg E) a1 a2
       | _, _ => Eq
       end
   end.

Definition OAll : Oset.Rcd All.
split with all_compare; unfold all_compare.
(* 1/3 *)
- intros a1 a2.
  case a1; clear a1;
  (intro x1; case a2; clear a2; intro x2; simpl; try discriminate);
  try oset_compare_tac.
  + generalize (funterm_compare_eq_bool_ok x1 x2).
    case (funterm_compare x1 x2).
    * apply f_equal.
    * intros H K; apply H.
      injection K; exact (fun h => h).
    * intros H K; apply H.
      injection K; exact (fun h => h).
  + generalize (aggterm_compare_eq_bool_ok x1 x2).
    case (aggterm_compare x1 x2).
    * apply f_equal.
    * intros H K; apply H.
      injection K; exact (fun h => h).
    * intros H K; apply H.
      injection K; exact (fun h => h).
- intros a1 a2 a3.
  case a1; clear a1;
  (intro x1; case a2; clear a2; try discriminate; 
   (intro x2; case a3; clear a3; intro x3; simpl; 
    try (discriminate || intros; apply refl_equal)));
  try oset_compare_tac.
  + apply (Oset.compare_lt_trans (OFun E)).
  + apply (Oset.compare_lt_trans (OAgg E)).
- (* 1/1 *)
  intros a1 a2.
  case a1; clear a1; 
  (intro x1; case a2; clear a2; intro x2; simpl;
   try (discriminate || intros; apply refl_equal));
  try oset_compare_tac.
  + apply (Oset.compare_lt_gt (OFun E)).
  + apply (Oset.compare_lt_gt (OAgg E)).
Defined.

Ltac inv_tac x Hn := intros x Hn; destruct x; inversion Hn; fail.

Ltac sql_size_tac := 
  match goal with
    | Hn : tree_size (tree_of_sql_query (Sql_Set _ ?q1 _)) <= S ?n 
      |- tree_size (tree_of_sql_query ?q1) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Set _ _ ?q2)) <= S ?n 
      |- tree_size (tree_of_sql_query ?q2) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Select _ ?lsq _ _ _)) <= S ?n
      |- list_size (tree_size (All:=All)) (map tree_of_sql_from_item ?lsq) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)); 
      do 2 apply le_S; apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Select Select_Star ?lsq _ _ _)) <= S ?n,
      Hi : In ?fi ?lsq
      |- tree_size (tree_of_sql_from_item ?fi) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      do 2 apply le_S;
      refine (le_trans _ _ _ _ (le_plus_l _ _));
      apply in_list_size; rewrite in_map_iff; eexists; split; [apply refl_equal | trivial]

    | Hn : tree_size (tree_of_sql_query (Sql_Select (Select_List _) ?lsq _ _ _)) <= S ?n,
           Hy : In ?y ?lsq
      |- tree_size (tree_of_sql_from_item ?y) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_l _ _));
      apply in_list_size; rewrite in_map_iff; 
      eexists; split; [apply refl_equal | assumption]

    | Hn : tree_size (tree_of_sql_query (Sql_Select _ ?lsq _ _ _)) <= S ?n,
      Hi : In ?fi ?lsq
      |- tree_size (tree_of_sql_from_item ?fi) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)); 
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_l _ _));
      apply in_list_size;
      rewrite in_map_iff; exists fi; split; trivial

    | Hn : tree_size (tree_of_sql_query (Sql_Select _ ?lsq _ _ _)) <= S ?n,
           Hf : In (From_Item ?sq ?r) ?lsq
      |- tree_size (tree_of_sql_query ?sq) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      do 2 apply le_S;
      refine (le_trans _ _ _ _ (le_plus_l _ _));
      apply (le_trans _ (tree_size (tree_of_sql_from_item (From_Item sq r))) _);
        [simpl; apply le_S; apply le_plus_l | ];
      apply in_list_size; rewrite in_map_iff; eexists; split; [ | apply Hf]; trivial

    | Hn : tree_size (tree_of_sql_query (Sql_Select Select_Star _ ?f1 _ _)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f1) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      do 2 apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Select (Select_List _) _ ?f1 _ _)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f1) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Select _ _ ?f _ _)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)); 
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size  (tree_of_sql_query (Sql_Select _ _ (Sql_Atom Sql_True) _ ?f)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)); 
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      do 2 apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l
            
    | Hn : tree_size (tree_of_sql_query (Sql_Select Select_Star _ _ _ ?f2)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f2) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      do 2 apply le_S;
      do 3 refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Select (Select_List _) _ _ _ ?f2)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f2) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      do 3 refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_query (Sql_Select _ _ _ _ ?f)) <= S ?n
      |- tree_size (tree_of_sql_formula ?f) <= ?n =>
      rewrite tree_of_sql_query_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)); 
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_S;
      do 3 refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_from_item (From_Item ?sq _)) <= S ?n
      |- tree_size (tree_of_sql_query ?sq) <= ?n =>
      rewrite tree_of_sql_from_item_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_formula (Sql_Conj _ ?f _)) <= S ?n 
      |- tree_size (tree_of_sql_formula ?f) <= ?n =>
      rewrite tree_of_sql_formula_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      simpl; apply le_plus_l

    | Hn : tree_size (tree_of_sql_formula (Sql_Conj _ _ ?f)) <= S ?n 
      |- tree_size (tree_of_sql_formula ?f) <= ?n =>
      rewrite tree_of_sql_formula_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_formula (Sql_Not ?f)) <= S ?n 
      |- tree_size (tree_of_sql_formula ?f) <= ?n =>
      rewrite tree_of_sql_formula_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      simpl; apply le_plus_l

    | Hn : tree_size (tree_of_sql_formula (Sql_Atom ?a)) <= S ?n
      |- tree_size (tree_of_sql_atom ?a) <= ?n =>
      rewrite tree_of_sql_formula_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_atom (Sql_Quant _ _ _ ?sq)) <= S ?n
      |- tree_size (tree_of_sql_query ?sq) <= ?n =>
      rewrite tree_of_sql_atom_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_S; 
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_atom (Sql_In _ ?sq)) <= S ?n
      |- tree_size (tree_of_sql_query ?sq) <= ?n =>
      rewrite tree_of_sql_atom_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_S;
      refine (le_trans _ _ _ _ (le_plus_r _ _));
      apply le_plus_l

    | Hn : tree_size (tree_of_sql_atom (Sql_Exists ?sq)) <= S ?n
      |- tree_size (tree_of_sql_query ?sq) <= ?n =>
      rewrite tree_of_sql_atom_unfold in Hn; simpl in Hn;
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn));
      apply le_plus_l

    | _ => idtac
  end.

Lemma tree_of_sql_query_eq_etc :
  forall n,
  (forall q1, tree_size (tree_of_sql_query q1) <= n ->
   forall q2, tree_of_sql_query q1 = tree_of_sql_query q2 -> q1 = q2) /\

  (forall f1, tree_size (tree_of_sql_from_item f1) <= n ->
         forall f2, tree_of_sql_from_item f1 = tree_of_sql_from_item f2 -> f1 = f2) /\

  (forall f1, tree_size (tree_of_sql_formula f1) <= n ->
   forall f2, tree_of_sql_formula f1 = tree_of_sql_formula f2 -> f1 = f2) /\

  (forall a1, tree_size (tree_of_sql_atom a1) <= n ->
         forall a2, tree_of_sql_atom a1 = tree_of_sql_atom a2 -> a1 = a2).
Proof.
intro n; induction n as [ | n]; repeat split; try (inv_tac x Hn).
- intros q1 Hn q2 H. 
  destruct q1 as [r1 | o1 q11 q12 | s1 lsq1 f11 g1 f12];
    destruct q2 as [r2 | o2 q21 q22 | s2 lsq2 f21 g2 f22]; try discriminate H.
  + injection H; apply f_equal.
  + injection H; intros H2 H1 Ho; apply f_equal3.
    * destruct o1; destruct o2; try discriminate Ho; trivial.
    * apply (proj1 IHn); trivial; sql_size_tac.
    * apply (proj1 IHn); trivial; sql_size_tac.
  + rewrite 2 tree_of_sql_query_unfold in H; injection H; intros Hf2 Hg Hf1 Hlsq Hs.
    apply f_equal5.
    * destruct s1 as [ | l1]; destruct s2 as [ | l2]; trivial; try discriminate Hs.
      apply f_equal.
      injection Hs; clear Hs; intro Hs.
      clear Hn H.
      revert l2 Hs;
        induction l1 as [ | [a1 b1] l1];
        intros [ | [a2 b2] l2] Hs; try (apply refl_equal || discriminate Hs).
      simpl in Hs; injection Hs; clear Hs.
      intros Hl Hb Ha; apply f_equal2; [apply f_equal2; trivial | ].
      apply IHl1; assumption.
    * assert (IH : forall fi1, 
                      List.In fi1 lsq1 -> 
                      forall fi2, tree_of_sql_from_item fi1 = tree_of_sql_from_item fi2 -> 
                                  fi1 = fi2).
      {
        intros fi1 Hfi1 fi2 Hfi.
        apply (proj2 IHn); trivial; sql_size_tac.
      }
      clear Hn H; revert lsq2 Hlsq IH.
      induction lsq1 as [ | fi1 lsq1];
        intros [ | fi2 lsq2] Hlsq IH; try (apply refl_equal || discriminate Hlsq).
      simpl in Hlsq; injection Hlsq; clear Hlsq; intros Hlsq Hfi.
      apply f_equal2; [apply (IH fi1 (or_introl _ (refl_equal _)) fi2 Hfi) | ].
      apply IHlsq1; [assumption | ].
      do 2 intro; apply IH; right; assumption.
    * apply (proj1 (proj2 (proj2 IHn))); [ | assumption]; sql_size_tac.
    * destruct g1 as [g1 | ]; destruct g2 as [g2 | ];
      try (apply refl_equal || discriminate Hg).
      simpl in Hg; injection Hg; clear Hg; intro Hg.
      apply f_equal.
      clear Hn H; revert g2 Hg.
      induction g1 as [ | fu1 g1]; intros [ | fu2 g2] Hg;
      try (apply refl_equal || discriminate Hg).
      simpl in Hg; injection Hg; clear Hg; intros Hg Hfu.
      apply f_equal2; [apply Hfu | ].
      apply IHg1; assumption.
    * apply (proj1 (proj2 (proj2 IHn))); [ | assumption]; sql_size_tac.
- intros f1 Hn f2 H; destruct f1 as [sq1 r1]; destruct f2 as [sq2 r2].
  simpl in H; injection H; clear H.
  intros Ha Hs; apply f_equal2.
  + apply (proj1 IHn); [ | assumption]; sql_size_tac.
  + destruct r1 as [ | r1]; destruct r2 as [ | r2]; trivial; try discriminate Ha.
    simpl in Ha; injection Ha; clear Ha; intro Ha.
    apply f_equal.
    clear Hn; revert r2 Ha.
    induction r1 as [ | [a1 b1] l1];
      intros [ | [a2 b2] l2] H; trivial; try discriminate H.
    simpl in H; injection H; clear H.
    intros Hl Hb Ha; apply f_equal2; [apply f_equal2; trivial | ].
    apply IHl1; assumption.
- intros f1 Hn f2 H;
  destruct f1 as [o1 f11 f12 | f1 | a1]; 
  destruct f2 as [o2 f21 f22 | f2 | a2]; try discriminate H.
  + simpl in H; injection H; clear H; intros H2 H1 Ho.
    apply f_equal3.
    * destruct o1; destruct o2; trivial; discriminate Ho.
    * apply (proj1 (proj2 (proj2 IHn))); [ | assumption]; sql_size_tac.
    * apply (proj1 (proj2 (proj2 IHn))); [ | assumption]; sql_size_tac.
  + apply f_equal.
    simpl in H; injection H; clear H; intro H.
    apply (proj1 (proj2 (proj2 IHn))); [ | assumption]; sql_size_tac.
  + simpl in H; injection H; clear H; intro H.
    apply f_equal; apply (proj2 (proj2 (proj2 IHn))); [ | assumption]; sql_size_tac.
- intros a1 Hn a2 H;
  destruct a1 as [ | p1 l1 | [ | ] p1 l1 sq1 | s1 sq1 | sq1]; 
  destruct a2 as [ | p2 l2 | [ | ] p2 l2 sq2 | s2 sq2 | sq2]; try discriminate H.
  + apply refl_equal.
  + simpl in H; injection H; clear H; intros Hl Hp.
    apply f_equal2; [assumption | ].
    clear Hn; revert l2 Hl.
    induction l1 as [ | a1 l1]; intros [ | a2 l2] H;
    try (apply refl_equal || discriminate H).
    simpl in H; injection H; clear H.
    intros Hl Ha; apply f_equal2; [assumption | ].
    apply IHl1; assumption.
  + simpl in H; injection H; clear H.
    intros Hl Hs Hp.
    apply f_equal3; trivial.
    * revert l2 Hl; clear Hn.
      induction l1 as [ | a1 l1]; intros [ | a2 l2] H;
      try (apply refl_equal || discriminate H).
      simpl in H; injection H; clear H.
      intros Hl Ha; apply f_equal2; [assumption | ].
      apply IHl1; assumption.
    * apply (proj1 IHn); [ | assumption]; sql_size_tac.
  + simpl in H; injection H; clear H.
    intros H1 H2 H3.
    apply f_equal3; trivial.
    * clear Hn; revert l2 H1.
      induction l1 as [ | a1 l1];
        intros [ | a2 l2] H; trivial; try discriminate H.
      simpl in H; injection H; clear H.
      intros Hl  Ha; apply f_equal2; [trivial | ].
      apply IHl1; assumption.
    * apply (proj1 IHn); [ | assumption]; sql_size_tac.
  + simpl in H; injection H; clear H; intros H1 H2.
    apply f_equal2.
    * clear Hn; revert s2 H2; induction s1 as [ | [a1 b1] l1];
      intros [ | [a2 b2] l2] H; trivial; try discriminate H.
      simpl in H; injection H; clear H.
      intros Hl Hb Ha; apply f_equal2; [apply f_equal2; trivial | ].
      apply IHl1; assumption.
    * apply (proj1 IHn); [ | trivial]; sql_size_tac.
  + simpl in H; injection H; clear H; intro H.
    apply f_equal.
    apply (proj1 IHn); [ | trivial]; sql_size_tac.
Qed.

Lemma tree_of_sql_query_eq :
  forall q1 q2, tree_of_sql_query q1 = tree_of_sql_query q2 -> q1 = q2.
Proof.
intros q1 q2 H.  
apply (proj1 (tree_of_sql_query_eq_etc _) q1 (le_n _) q2 H).
Qed.

Definition OSQLQ : Oset.Rcd sql_query.
split with (fun q1 q2 => Oset.compare (OTree OAll) (tree_of_sql_query q1) (tree_of_sql_query q2)).
- intros a1 a2.
  generalize (Oset.eq_bool_ok (OTree OAll) (tree_of_sql_query a1) (tree_of_sql_query a2)).
  case (Oset.compare (OTree OAll) (tree_of_sql_query a1) (tree_of_sql_query a2)).
  + apply tree_of_sql_query_eq.
  + intros H K; apply H; subst; apply refl_equal.
  + intros H K; apply H; subst; apply refl_equal.
- do 3 intro; oset_compare_tac.
- do 2 intro; oset_compare_tac.
Defined.

Definition OFI : Oset.Rcd sql_from_item.
split with (fun q1 q2 => 
              Oset.compare (OTree OAll) (tree_of_sql_from_item q1) (tree_of_sql_from_item q2)).
- intros a1 a2.
  generalize (Oset.eq_bool_ok (OTree OAll) (tree_of_sql_from_item a1) (tree_of_sql_from_item a2)).
  case (Oset.compare (OTree OAll) (tree_of_sql_from_item a1) (tree_of_sql_from_item a2)).
  + apply (proj1 (proj2 (tree_of_sql_query_eq_etc (tree_size (tree_of_sql_from_item a1))))).
    apply le_n.
  + intros H K; apply H; subst; apply refl_equal.
  + intros H K; apply H; subst; apply refl_equal.
- do 3 intro; oset_compare_tac.
- do 2 intro; oset_compare_tac.
Defined.

Definition sql_id_renaming s : list select :=
  List.map (fun a => Select_As (A_Expr (F_Dot E a)) a) (Fset.elements (A T) s).

(** * SQL_COQ semantics *)
Notation setA := (Fset.set (A T)).
Notation BTupleT := (Fecol.CBag (CTuple T)).
Notation bagT := (Febag.bag BTupleT).
Hypothesis interp_predicate : predicate -> list (value T) -> bool3.
Hypothesis interp_symb : symb -> list (value T) -> (value T).
Hypothesis interp_aggregate : aggregate -> list (value T) -> (value T).
Hypothesis basesort : relname -> Fset.set (Tuple.A T).
Hypothesis instance : relname -> bagT.


(* Evaluation in an environment :-( *)

Notation interp_dot_env_slice := (interp_dot_env_slice T).
Notation projection := 
  (projection (T := T) (OSymb := OSymb) (OAggregate := OAggregate) interp_symb interp_aggregate).
Notation interp_aggterm_ := 
  (interp_aggterm_ (T := T) (OSymb := OSymb) (OAggregate := OAggregate) interp_symb interp_aggregate).
Notation interp_funterm_ := (interp_funterm_ (T := T) (OSymb := OSymb) (OAggregate := OAggregate) interp_symb).
Notation make_groups := (fun env b => make_groups (T := T) (OSymb := OSymb) (OAggregate := OAggregate) interp_symb env (Febag.elements (Fecol.CBag (CTuple T)) b)).
Notation N_product_bag ll := (Data.N_product_bag _ _ (build_data T) (Fecol.CBag (CTuple T)) ll).

(** * Building a Select_List from a renaming occuring in a from part *)
Definition att_renaming_item_to_from_item x :=
  match x with
    | Att_Ren_Star => Select_Star
    | Att_Ren_List la => 
      Select_List 
        (List.map 
           (fun y => 
              match y with 
                | Att_As a b => Select_As (A_Expr (F_Dot E a)) b
              end) 
           la)
  end.

(** Sort of an SQL query : a set of attributes *)
Definition select_as_as_pair (x : select) := match x with Select_As e a => (e, a) end.
Definition att_as_as_pair x := match x with Att_As e a => (e, a) end.

Fixpoint sql_sort (sq : sql_query) : setA :=
  match sq with
    | Sql_Table tbl => basesort tbl
    | Sql_Set o sq1 _ => sql_sort sq1 
    | Sql_Select s f _ _ _ => 
      match s with
        | Select_Star => Fset.Union (A T) (List.map sql_from_item_sort f)
        | Select_List la => 
           Fset.mk_set (A T) (List.map (@snd _ _) (List.map select_as_as_pair la))
      end
  end

with sql_from_item_sort x :=
  match x with
    | From_Item sq Att_Ren_Star => sql_sort sq
    | From_Item _ (Att_Ren_List la) =>
           Fset.mk_set (A T) (List.map (@snd _ _) (List.map att_as_as_pair la))
  end.

Lemma sql_sort_unfold :
  forall sq, sql_sort sq =
  match sq with
    | Sql_Table tbl => basesort tbl
    | Sql_Set o sq1 _ => sql_sort sq1
    | Sql_Select s f _ _ _ => 
      match s with
        | Select_Star => Fset.Union (A T) (List.map sql_from_item_sort f)
        | Select_List la => 
           Fset.mk_set (A T) (List.map (@snd _ _) (List.map select_as_as_pair la))
      end
  end.
Proof.
intro sq; case sq; intros; apply refl_equal.
Qed.

Lemma sql_from_item_sort_unfold :
  forall x, sql_from_item_sort x =
  match x with
    | From_Item sq Att_Ren_Star => sql_sort sq
    | From_Item _ (Att_Ren_List la) =>
           Fset.mk_set (A T) (List.map (@snd _ _) (List.map att_as_as_pair la))
  end.
Proof.
intros x; case x; intros; apply refl_equal.
Qed.

(** * evaluation of SQL queries with a three values' logic *)
Fixpoint eval_sql_query env (sq : sql_query) {struct sq} : bagT :=
  match sq with
  | Sql_Table tbl => instance tbl
  | Sql_Set o sq1 sq2 =>
    if sql_sort sq1 =S?= sql_sort sq2 
    then Febag.interp_set_op _ o (eval_sql_query env sq1) (eval_sql_query env sq2)
    else Febag.empty _
  | Sql_Select s lsq f1 gby f2  => 
    let sort_lsq := Fset.Union _ (map sql_from_item_sort lsq) in 
    let elsq := 
         (** evaluation of the from part *)
        List.map (eval_sql_from_item env) lsq in
    let cc :=
        (** selection of the from part by the where formula f1 (with old names) *)
        Febag.filter 
          BTupleT (fun t => match eval_sql_formula3 (env_t env t) f1 with true3 => true | _ => false end) 
          (N_product_bag elsq) in
    (** computation of the groups grouped according to gby *)
       let lg1 := make_groups env cc gby in
       (** discarding groups according the having clause f2 *)
       let lg2 := 
           List.filter 
             (fun g  => match eval_sql_formula3 (env_g env gby g) f2 with true3 => true | _ => false end) 
             lg1 in
       (** applying the outermost projection and renaming, the select part s *)
       Febag.mk_bag BTupleT
                    (List.map (fun g => projection (env_g env gby g) s) lg2)
  end

(** * evaluation of the from part *)
with eval_sql_from_item env x := 
       match x with
         | From_Item sqj sj =>
           Febag.map BTupleT BTupleT 
             (fun t => 
                projection (env_t env t) (att_renaming_item_to_from_item sj)) 
             (eval_sql_query env sqj)
       end

(** * evaluation of SQL formulas *)
with eval_sql_formula3 env (f : sql_formula) : bool3 := 
    match f with
    | Sql_Conj a f1 f2 => (interp_conj3 a) (eval_sql_formula3 env f1) (eval_sql_formula3 env f2)
    | Sql_Not f => negb3 (eval_sql_formula3 env f)
    | Sql_Atom a => eval_sql_atom3 env a
    end

(** * evaluation of SQL atoms *)
with eval_sql_atom3 env (a : sql_atom) : bool3 :=
  match a with
  | Sql_True => true3
  | Sql_Pred p l => interp_predicate p (map (interp_aggterm_ env) l)
  | Sql_Quant qtf p l sq =>
    let lt := map (interp_aggterm_ env) l in
    interp_quant3 qtf
                 (fun x => 
                    let la := Fset.elements _ (support T x) in
                    interp_predicate p (lt ++ map (dot T x) la))
                 (Febag.elements _ (eval_sql_query env sq))
  | Sql_In s sq => 
      if Febag.mem BTupleT (projection env (Select_List s)) (eval_sql_query env sq)
      then true3 else false3
  | Sql_Exists sq => if (Febag.is_empty _ (eval_sql_query env sq)) then false3 else true3
  end.

Lemma eval_sql_query_unfold :
  forall env sq, eval_sql_query env sq =
  match sq with
  | Sql_Table tbl => instance tbl
  | Sql_Set o sq1 sq2 =>
    if sql_sort sq1 =S?= sql_sort sq2 
    then Febag.interp_set_op _ o (eval_sql_query env sq1) (eval_sql_query env sq2)
    else Febag.empty _
  | Sql_Select s lsq f1 gby f2  => 
    let sort_lsq := Fset.Union _ (map sql_from_item_sort lsq) in 
    let elsq := 
         (** evaluation of the from part *)
        List.map (eval_sql_from_item env) lsq in
    let cc :=
        (** selection of the from part by the where formula f1 (with old names) *)
        Febag.filter 
          BTupleT (fun t => match eval_sql_formula3 (env_t env t) f1 with true3 => true | _ => false end) 
          (N_product_bag elsq) in
    (** computation of the groups grouped according to gby *)
       let lg1 := make_groups env cc gby in
       (** discarding groups according the having clause f2 *)
       let lg2 := 
           List.filter 
             (fun g  => match eval_sql_formula3 (env_g env gby g) f2 with true3 => true | _ => false end) 
             lg1 in
       (** applying the outermost projection and renaming, the select part s *)
       Febag.mk_bag BTupleT
                    (List.map (fun g => projection (env_g env gby g) s) lg2)
  end.
Proof.
  intros env sq; case sq; intros; apply refl_equal.
Qed.

Lemma eval_sql_atom3_unfold :
  forall env a, eval_sql_atom3 env a =
  match a with
  | Sql_True => true3
  | Sql_Pred p l => interp_predicate p (map (interp_aggterm_ env) l)
  | Sql_Quant qtf p l sq =>
    let lt := map (interp_aggterm_ env) l in
    interp_quant3 qtf
                 (fun x => 
                    let la := Fset.elements _ (support T x) in
                    interp_predicate p (lt ++ map (dot T x) la))
                 (Febag.elements _ (eval_sql_query env sq))
  | Sql_In s sq => 
      if Febag.mem BTupleT (projection env (Select_List s)) (eval_sql_query env sq)
      then true3 else false3
  | Sql_Exists sq => if (Febag.is_empty _ (eval_sql_query env sq)) then false3 else true3
  end.
Proof.
intros env a; case a; intros; apply refl_equal.
Qed.

Lemma eval_sql_formula3_unfold :
  forall env f, eval_sql_formula3 env f =
    match f with
    | Sql_Conj a f1 f2 => (interp_conj3 a) (eval_sql_formula3 env f1) (eval_sql_formula3 env f2)
    | Sql_Not f => negb3 (eval_sql_formula3 env f)
    | Sql_Atom a => eval_sql_atom3 env a
    end.
Proof.
intros env f; case f; intros; apply refl_equal.
Qed.

Lemma eval_sql_from_item_unfold :
  forall env x, eval_sql_from_item env x = 
       match x with
         | From_Item sqj sj =>
           Febag.map BTupleT BTupleT 
             (fun t => 
                projection (env_t env t) (att_renaming_item_to_from_item sj)) 
             (eval_sql_query env sqj)
       end.
Proof.
intros env x; case x; intros; apply refl_equal.
Qed.

Lemma equiv_abstract_env_slice_sym :
  forall e1 e2, equiv_abstract_env_slice e1 e2 <-> 
                equiv_abstract_env_slice (T := T) (OSymb := OSymb) (OAggregate := OAggregate) e2 e1.
Proof.
Proof.
intros [sa1 g1] [sa2 g2]; split; intros [H1 H2]; simpl; repeat split.
- rewrite Fset.equal_spec in H1; rewrite Fset.equal_spec; intro; rewrite H1; trivial.
- subst; trivial.
- rewrite Fset.equal_spec in H1; rewrite Fset.equal_spec; intro; rewrite H1; trivial.
- subst; trivial.
Qed.

Ltac env_tac :=
  match goal with
    | |- equiv_env ?e ?e => apply equiv_abstract_env_refl

(* env_g *)
    | Hx : ?x1 =t= ?x2 
      |- equiv_env (env_g ?e Group_Fine (?x1 :: nil)) (env_g ?e Group_Fine (?x2 :: nil)) =>
      constructor 2; [ | apply equiv_env_refl];
      simpl; repeat split; [ | apply compare_list_t; simpl; rewrite Hx; apply refl_equal];
      rewrite tuple_eq in Hx; apply (proj1 Hx)

    | Hx : Oeset.compare (OLTuple T) ?x1 ?x2 = Eq
      |- equiv_env (env_g ?e ?g ?x1) (env_g ?e ?g ?x2) =>
      unfold env_g; constructor 2; [ | apply equiv_env_refl];
      simpl; repeat split; 
      [apply env_slice_eq_1 | rewrite compare_list_t]; assumption

    | He : equiv_env ?e1 ?e2
      |- equiv_env (env_g ?e1 ?g ?x) (env_g ?e2 ?g ?x) =>
      unfold env_g; constructor 2; [ | apply He];
      simpl; repeat split; trivial;
      [apply Fset.equal_refl | 
       rewrite compare_list_t; apply Oeset.compare_eq_refl]

    | He : equiv_env ?e1 ?e2, 
      Hx : Oeset.compare (OLTuple T) ?x1 ?x2 = Eq
      |- equiv_env (env_g ?e1 ?g ?x1) (env_g ?e2 ?g ?x2) =>
      unfold env_g; constructor 2; [ | apply He];
      simpl; repeat split; 
      [apply env_slice_eq_1 | rewrite compare_list_t]; assumption

(* env_t *)
    | Hx : ?x1 =t= ?x2 
      |- equiv_env (env_t ?env ?x1) (env_t ?env ?x2) =>
      unfold env_t; constructor 2; [ | apply equiv_env_refl];
      simpl; repeat split; [ | apply compare_list_t; simpl; rewrite Hx; trivial];
      rewrite tuple_eq in Hx; apply (proj1 Hx)

(*    | He : equiv_env ?env1 ?env2 
      |- equiv_env (env_t ?env1 ?x) (env_t ?env2 ?x) =>
      unfold env_t; constructor 2; [ | assumption];
      simpl; repeat split; [ | apply compare_list_t; simpl; rewrite Hx; trivial];
      rewrite tuple_eq in Hx; apply (proj1 Hx)
*)
    | He : equiv_env ?env1 ?env2, Hx : ?x1 =t= ?x2 
      |- equiv_env (env_t ?env1 ?x1) (env_t ?env2 ?x2) =>
      unfold env_t; constructor 2; [ | assumption];
      simpl; repeat split; [ | apply compare_list_t; simpl; rewrite Hx; trivial];
      rewrite tuple_eq in Hx; apply (proj1 Hx)

(* env_t, env_g *)
    | He : equiv_env ?e1 ?e2, Hx : ?x1 =t= ?x2
      |- equiv_env (env_t ?e1 ?x1) (env_g ?e2 Group_Fine (?x2 :: nil)) =>
      unfold env_t, env_g; constructor 2; [ | apply He];
      simpl; repeat split; [ | rewrite compare_list_t; simpl; rewrite Hx; trivial];
      rewrite tuple_eq in Hx; apply (proj1 Hx)

    | He : equiv_env ?e1 ?e2
      |- equiv_env (env_t ?e1 ?x) (env_g ?e2 Group_Fine (?x :: nil)) =>
      unfold env_t, env_g; constructor 2; [ | apply He];
      simpl; repeat split; 
      [apply Fset.equal_refl | rewrite compare_list_t; simpl; rewrite Oeset.compare_eq_refl; trivial]

(* env *)
    | He : equiv_env ?e1 ?e2
      |- equiv_env ((?sa1, ?g, ?x) :: ?e1) ((?sa1, ?g, ?x) :: ?e2) =>
      constructor 2; [ | assumption];
      simpl; repeat split; trivial;
      [apply Fset.equal_refl | 
       rewrite compare_list_t; apply Oeset.compare_eq_refl]

    |  Hx : Oeset.compare (OLTuple T) ?x1 ?x2 = Eq
      |- equiv_env ((?sa1, ?g, ?x1) :: ?e1) ((?sa1, ?g, ?x2) :: ?e1) =>
       constructor 2; [ | apply equiv_env_refl];
       simpl; repeat split; [apply Fset.equal_refl | rewrite compare_list_t; assumption] 

    |  Hx : Oeset.compare (OTuple T) ?x1 ?x2 = Eq
      |- equiv_env ((?sa, ?g, ?x1 :: nil) :: ?e) ((?sa, ?g, ?x2 :: ?nil) :: ?e) =>
       constructor 2; [ | apply equiv_env_refl];
       simpl; repeat split; 
       [apply Fset.equal_refl | rewrite compare_list_t; simpl; rewrite Hx; trivial]

(*

      simpl; repeat split; trivial;
      [apply Fset.equal_refl | rewrite compare_list_t; assumption]

      simpl; repeat split; trivial;
      [apply Fset.equal_refl 
      | rewrite compare_list_t; simpl; rewrite Hx; trivial
      | apply equiv_env_refl]
*)
    | He : equiv_env ?e1 ?e2,
           Hx : Oeset.compare (OLTuple T) ?x1 ?x2 = Eq
      |- equiv_env ((?sa1, ?g, ?x1) :: ?e1) ((?sa1, ?g, ?x2) :: ?e2) =>
      constructor 2; [ | apply He];
      simpl; repeat split; 
      [apply Fset.equal_refl | rewrite compare_list_t; assumption]

    | He : equiv_env ?e1 ?e2,
           Hx : Oeset.compare (OTuple T) ?x1 ?x2 = Eq
      |- equiv_env ((?sa, ?g, ?x1 :: nil) :: ?e1) ((?sa, ?g, ?x2 :: ?nil) :: ?e2) =>
      constructor 2; [ | apply He]; 
      simpl; repeat split; 
      [apply Fset.equal_refl 
      | rewrite compare_list_t; simpl; rewrite Hx; trivial]
            
    | He : equiv_env ?e1 ?e2,
           Hx : Oeset.compare (OLTuple T) ?x1 ?x2 = Eq
      |- equiv_env ((?sa, ?g, ?x1) :: ?e1) (((?sa unionS (emptysetS)), ?g, ?x2) :: ?e2) => 
      constructor 2; [ | apply He];
      simpl; repeat split; 
      [ rewrite Fset.equal_spec; intro a;
        rewrite Fset.mem_union, Fset.empty_spec, Bool.orb_false_r; apply refl_equal
      | apply compare_list_t; trivial]

(*
       assert (KKKK : true = true)
      simpl; repeat split; trivial;
      [ rewrite Fset.equal_spec; intro a;
        rewrite Fset.mem_union, Fset.empty_spec, Bool.orb_false_r; apply refl_equal
      | apply compare_list_t; trivial]
*)
    | _ => trivial
  end.

Lemma make_groups_eq :
  forall env1 env2 g b1 b2, equiv_env env1 env2 -> b1 =BE= b2 -> 
     forall l, Oeset.nb_occ (OLTuple T) l (make_groups env1 b1 g) = 
               Oeset.nb_occ (OLTuple T) l (make_groups env2 b2 g).
Proof.
intros env1 env2 [g | ] b1 b2 He Hb l.
- apply permut_nb_occ; apply permut_refl_alt; apply partition_list_expr_eq_alt.
  + induction g as [ | fe g]; simpl; [constructor 1 | constructor 2; [ | apply IHg]].
    intros x y H.
    apply interp_funterm_eq; env_tac.
  + apply permut_refl_alt; apply Febag.elements_spec1; assumption.
- apply (Oeset.nb_occ_map_eq_3 (OTuple T)).
  + intros x1 x2 Hx1 Hx2 Hx; simpl; rewrite Hx; trivial.
  + intro x; rewrite <- 2 Febag.nb_occ_elements.
    revert x; rewrite <- Febag.nb_occ_equal; assumption.
Qed.

Lemma eval_sql_query_eq_etc :
  forall n,
    (forall q, 
       tree_size (tree_of_sql_query q) <= n -> 
       forall env1 env2, equiv_env env1 env2 ->
                         eval_sql_query env1 q =BE= eval_sql_query env2 q) /\
    
    (forall f, 
       tree_size (tree_of_sql_from_item f) <= n ->
       forall env1 env2, equiv_env env1 env2 ->
                         eval_sql_from_item env1 f =BE= eval_sql_from_item env2 f) /\
    
    (forall f, 
       tree_size (tree_of_sql_formula f) <= n ->
       forall env1 env2, equiv_env env1 env2 ->
                         eval_sql_formula3 env1 f = eval_sql_formula3 env2 f) /\
    
    (forall a, 
       tree_size (tree_of_sql_atom a) <= n ->
       forall env1 env2, equiv_env env1 env2 ->
                         eval_sql_atom3 env1 a = eval_sql_atom3 env2 a).
Proof.
intro n; induction n as [ | n]; repeat split.
- intros q Hn; destruct q; inversion Hn.
- intros f Hn; destruct f; inversion Hn.
- intros a Hn; destruct a; inversion Hn.
- intros f Hn; destruct f; inversion Hn.
- intros q Hn env1 env2 Henv; 
  destruct q as [r | o q1 q2 | s lsq f1 g f2]; rewrite Febag.nb_occ_equal; intro t.
  + apply refl_equal.
  + rewrite 2 (eval_sql_query_unfold _ (Sql_Set _ _ _)).
    assert (IH1 : eval_sql_query env1 q1 =BE= eval_sql_query env2 q1).
    {
      apply (proj1 IHn); [sql_size_tac | ]; trivial.
    }
    assert (IH2 : eval_sql_query env1 q2 =BE= eval_sql_query env2 q2).
    {
      apply (proj1 IHn); [sql_size_tac | ]; trivial.
    }
    rewrite Febag.nb_occ_equal in IH1, IH2.
    case (sql_sort q1 =S?= sql_sort q2); [ | apply refl_equal].
    destruct o; simpl.
    * rewrite 2 Febag.nb_occ_union, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_union_max, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_inter, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_diff, IH1, IH2; apply refl_equal.
  + rewrite 2 (eval_sql_query_unfold _ (Sql_Select _ _ _ _ _)); cbv beta iota zeta.
    rewrite 2 Febag.nb_occ_mk_bag.
    refine (trans_eq _ (Oeset.nb_occ_map_eq_3 (OLTuple T) _ _ _ _ _ _ _));
    [refine (Oeset.nb_occ_map_eq_2_alt (OLTuple T) _ _ _ _ _ _) | | ].
    * intros x1 Hx1; apply projection_eq; env_tac.
    * intros x1 x2 Hx1 Hx2 Hx; apply projection_eq; env_tac.
    * clear t; intro l.
      {
        apply Oeset.nb_occ_filter_eq.
        - clear l; intro l.
          apply make_groups_eq; [assumption | ].
          apply Febag.filter_eq.
          + unfold N_product_bag.
            rewrite Febag.nb_occ_equal; intro t; rewrite 2 Febag.nb_occ_mk_bag, 2 map_map.
            apply permut_nb_occ; apply permut_refl_alt; apply Data.N_product_list_map_eq.
            intros x Hx; apply Febag.elements_spec1.
            apply (proj1 (proj2 IHn)); [sql_size_tac | trivial].
          + intros x1 x2 Hx1 Hx.
            rewrite (proj1 (proj2 (proj2 IHn)) f1) with (env_t env1 x1) (env_t env2 x2);
              [apply refl_equal | sql_size_tac | env_tac].
        - intros x1 x2 Hx1 Hx.
          rewrite (proj1 (proj2 (proj2 IHn)) f2) with (env_g env1 g x1) (env_g env2 g x2); 
            [apply refl_equal | sql_size_tac | env_tac].
      }
- intros [sq r] Hn env1 env2 He.
  assert (IH : eval_sql_query env1 sq =BE= eval_sql_query env2 sq).
  {
    apply (proj1 IHn); [sql_size_tac | assumption].
  }
  destruct r as [ | r]; rewrite 2 (eval_sql_from_item_unfold _ (From_Item _ _)).
  + unfold Febag.map; rewrite 2 map_id; intros; trivial.
    rewrite Febag.nb_occ_equal; intro t.
    rewrite 2 Febag.nb_occ_mk_bag, <- 2 Febag.nb_occ_elements.
    rewrite Febag.nb_occ_equal in IH; apply IH.
  + rewrite Febag.nb_occ_equal; intro t.
    unfold Febag.map; rewrite 2 Febag.nb_occ_mk_bag.
    apply (Oeset.nb_occ_map_eq_2_3 (OTuple T)).
    * intros x1 x2 Hx; apply projection_eq; env_tac.
    * clear t; intro t; rewrite <- 2 Febag.nb_occ_elements.
    rewrite Febag.nb_occ_equal in IH; apply IH.
- intros [o f1 f2 | f | a] Hn env1 env2 He.
  + rewrite 2 (eval_sql_formula3_unfold _ (Sql_Conj _ _ _)).
    apply f_equal2.
    * apply (proj1 (proj2 (proj2 IHn))); [sql_size_tac | assumption].
    * apply (proj1 (proj2 (proj2 IHn))); [sql_size_tac | assumption].
  + rewrite 2 (eval_sql_formula3_unfold _ (Sql_Not _)).
    apply f_equal.
    apply (proj1 (proj2 (proj2 IHn))); [sql_size_tac | assumption].
  + rewrite 2 (eval_sql_formula3_unfold _ (Sql_Atom _)).
    apply (proj2 (proj2 (proj2 IHn))); [sql_size_tac | assumption].
- intros [ | p l | qtf p l sq | s sq | sq] Hn env1 env2 He.
  + trivial.
  + rewrite 2 (eval_sql_atom3_unfold _ (Sql_Pred _ _)).
    apply f_equal; rewrite <- map_eq; intros x Hx.
    apply interp_aggterm_eq; trivial.
  + rewrite 2 (eval_sql_atom3_unfold _ (Sql_Quant _ _ _ _)).
    cbv beta iota zeta.
    apply (interp_quant3_eq (OTuple T)).
    * intro x; rewrite <- 2 Febag.mem_unfold.
      apply Febag.mem_eq_2.
      apply (proj1 IHn); [sql_size_tac | assumption].
    * intros x1 x2 Hx1 Hx; apply f_equal.
      {
        apply f_equal2.
        - rewrite <- map_eq; intros a Ha.
          apply interp_aggterm_eq; trivial.
        - rewrite tuple_eq in Hx.
          rewrite <- (Fset.elements_spec1 _ _ _ (proj1 Hx)), <- map_eq.
          intros; apply (proj2 Hx).
      } 
  + rewrite 2 (eval_sql_atom3_unfold _ (Sql_In _ _)).
    assert (IH : eval_sql_query env1 sq =BE= eval_sql_query env2 sq).
    {
      apply (proj1 IHn); [sql_size_tac | assumption].
    }
    rewrite <- (Febag.mem_eq_2 _ _ _ IH).
    apply if_eq; [ | intros; trivial | intros; trivial].
    apply Febag.mem_eq_1.
    apply projection_eq; assumption.
  + rewrite 2 (eval_sql_atom3_unfold _ (Sql_Exists _)).
    apply if_eq; [ | intros; trivial | intros; trivial].
    rewrite 2 Febag.is_empty_spec.
    apply Febag.equal_eq_1.
    apply (proj1 IHn); [sql_size_tac | assumption].
Qed.

Lemma eval_sql_query_eq :
  forall q env1 env2, equiv_env env1 env2 -> eval_sql_query env1 q =BE= eval_sql_query env2 q.
Proof.
intros q env1 env2 He.
apply (proj1 (eval_sql_query_eq_etc _) _ (le_n _) _ _ He).
Qed.

Lemma eval_sql_from_item_eq :    
  forall f env1 env2, equiv_env env1 env2 -> 
                      eval_sql_from_item env1 f =BE= eval_sql_from_item env2 f.
Proof.
intros f env1 env2 He.
apply (proj1 (proj2 (eval_sql_query_eq_etc _)) _ (le_n _) _ _ He).
Qed.

Lemma eval_sql_formula3_eq :    
  forall f env1 env2, equiv_env env1 env2 -> eval_sql_formula3 env1 f = eval_sql_formula3 env2 f.
Proof.
intros f env1 env2 He.
apply (proj1 (proj2 (proj2 (eval_sql_query_eq_etc _))) _ (le_n _) _ _ He).
Qed.

Lemma eval_sql_atom3_eq :
  forall a env1 env2, equiv_env env1 env2 -> eval_sql_atom3 env1 a = eval_sql_atom3 env2 a.
Proof.
intros a env1 env2 He.
apply (proj2 (proj2 (proj2 (eval_sql_query_eq_etc _))) _ (le_n _) _ _ He).
Qed.

Definition well_sorted_sql_table :=
  forall tbl t, t inBE (instance tbl) -> support T t =S= basesort tbl.

Lemma interp_funterm_eq_ :
  forall env gby e, In e (map (fun (f : funterm E) (t : tuple T) =>
                         interp_funterm_ (env_t env t) f) gby) ->
    forall t1 t2 : tuple T, t1 =t= t2 -> e t1 = e t2.
Proof.
intros env gby e He; rewrite in_map_iff in He.
destruct He as [f [He Hf]]; subst e.
intros t1 t2 Ht; apply interp_funterm_eq; env_tac.
Qed.

Lemma well_sorted_sql_query_etc :
  well_sorted_sql_table -> 
  forall n, 
    (forall sq, tree_size (tree_of_sql_query sq) <= n ->
                forall t env, t inBE (eval_sql_query env sq) -> support T t =S= sql_sort sq) /\
    (forall lf, list_size (tree_size (All:=All)) (map tree_of_sql_from_item lf) <= n ->
                forall t env, t inBE N_product_bag (map (eval_sql_from_item env) lf) ->
                          support T t =S= Fset.Union (A T) (map sql_from_item_sort lf)).
Proof.
intros W n; induction n as [ | n]; repeat split.
- intros sq Hn; destruct sq; inversion Hn.
- intros lf Hn; destruct lf as [ | f1 lf]; [ | destruct f1; inversion Hn].
  intros t env Ht; simpl in Ht.
  rewrite Febag.mem_nb_occ in Ht; unfold Data.N_product_bag in Ht; simpl in Ht.
  rewrite Febag.nb_occ_add, Febag.nb_occ_empty, N.add_0_r in Ht.
  case_eq (Oeset.eq_bool (OTuple T) t (empty_tuple T)); intro Kt;
    rewrite Kt in Ht; try discriminate Ht.
  unfold Oeset.eq_bool in Kt; rewrite compare_eq_true, tuple_eq in Kt.
  rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Kt)); unfold empty_tuple.
  rewrite (Fset.equal_eq_1 _ _ _ _ (support_mk_tuple _ _ _)).
  simpl; apply Fset.equal_refl.
- intros sq Hn t env Ht; destruct sq as [r | o sq1 sq2 | s lsq f1 gby f2].
  + apply W; apply Ht.
  + rewrite eval_sql_query_unfold in Ht.
    rewrite sql_sort_unfold.
    case_eq (sql_sort sq1 =S?= sql_sort sq2); 
      intro Hsq; rewrite Hsq in Ht.
    * {
        destruct o; simpl in Ht.
        - rewrite Febag.mem_union, Bool.orb_true_iff in Ht.
          destruct Ht as [Ht | Ht].
          + apply (proj1 IHn) with env; [sql_size_tac | assumption].
          + rewrite (Fset.equal_eq_2 _ _ _ _ Hsq).
            apply (proj1 IHn) with env; [sql_size_tac | assumption].
        - rewrite Febag.mem_union_max, Bool.orb_true_iff in Ht.
          destruct Ht as [Ht | Ht].
          + apply (proj1 IHn) with env; [sql_size_tac | assumption].
          + rewrite (Fset.equal_eq_2 _ _ _ _ Hsq).
            apply (proj1 IHn) with env; [sql_size_tac | assumption].
        - apply (proj1 IHn) with env; [sql_size_tac | ].
          rewrite Febag.mem_inter, Bool.andb_true_iff in Ht.
          apply (proj1 Ht).
        - apply (proj1 IHn) with env; [sql_size_tac | ].
          apply (Febag.diff_spec_weak _ _ _ _ Ht).
      } 
    * rewrite Febag.empty_spec_weak in Ht; discriminate Ht.
  + rewrite eval_sql_query_unfold in Ht; cbv beta iota zeta in Ht.
    rewrite Febag.mem_mk_bag, Oeset.mem_bool_true_iff in Ht. 
    destruct Ht as [t' [Ht Ht']].
    rewrite in_map_iff in Ht'.
    destruct Ht' as [st [Ht' Hst]].
    subst t'.
    rewrite (Fset.equal_eq_1 _ _ _ _ (tuple_eq_support _ _ _ Ht)).
    destruct s as [ | s]; 
      [ | simpl; rewrite (Fset.equal_eq_1 _ _ _ _ (support_mk_tuple _ _ _)), 2 map_map; 
          unfold select_as_as_pair; apply Fset.equal_refl_alt; apply f_equal; rewrite <- map_eq;
          intros x _; destruct x; trivial].
    rewrite sql_sort_unfold.
    unfold projection.
    rewrite filter_In in Hst; destruct Hst as [Hst _].
    unfold SqlCommon.make_groups in Hst.
    destruct gby as [gby | ].
    * assert (Jst := in_partition_list_expr 
                       (OTuple T) (FVal T) 
                       (map
                          (fun (f : funterm E) (t : tuple T) =>
                             interp_funterm_ (env_t env t) f) gby)
                      _ _ (interp_funterm_eq_ _ _ ) Hst).
      rewrite (partition_list_expr_partition_list_expr2 (OTuple T)), filter_In, map_unfold in Hst;
      [ | apply interp_funterm_eq_].
      destruct Hst as [_ Hst].
      destruct st as [ | t1 st]; [discriminate Hst | ].
      clear Hst; assert (Hst := length_quicksort (OTuple T) (t1 :: st)).
      simpl; case_eq (quicksort (OTuple T) (t1 :: st)); 
        [intro Abs; rewrite Abs in Hst; discriminate Hst | ].
      clear t Ht; intros t l H; simpl.
      {
        apply (proj2 IHn) with env.
        - sql_size_tac.
        - assert (H' : In (match st with | nil => t1 | _ :: _ => t end) (t1 :: st)).
          {
            destruct st; [left; trivial | ].
            rewrite (In_quicksort (OTuple T)), H; left; trivial.
          }
          assert (J := Febag.in_elements_mem _ _ _ (Jst _ H')).
          rewrite Febag.mem_filter, Bool.andb_true_iff in J;
          [apply (proj1 J) | ].
          intros x1 x2 Hx1 Hx.
          rewrite (eval_sql_formula3_eq f1 (env1 := env_t env x1) (env2 := env_t env x2)); env_tac.
      }
    * rewrite in_map_iff in Hst.
      destruct Hst as [u1 [Hst Hu1]]; subst st.
      {
        apply (proj2 IHn) with env; [sql_size_tac | ].
        assert (Ku1 := Febag.in_elements_mem _ _ _ Hu1).
        rewrite Febag.mem_filter, Bool.andb_true_iff in Ku1; [apply (proj1 Ku1) | ].
        intros x1 x2 Hx1 Hx.
        rewrite (eval_sql_formula3_eq f1 (env1 := env_t env x1) (env2 := env_t env x2)); env_tac.
      }          
- assert (IH : forall f : sql_from_item,
                tree_size (tree_of_sql_from_item f) <= S n ->
                forall t env, t inBE (eval_sql_from_item env f) -> 
                              support T t =S= (sql_from_item_sort f)).
  {
    intros f Hn t env Ht; destruct f as [s [ | rho]].
    - simpl; apply (proj1 IHn) with env; [sql_size_tac | ].
      simpl in Ht; unfold Febag.map in Ht.
      rewrite Febag.mem_mk_bag, map_id, Oeset.mem_bool_true_iff in Ht; trivial.
      destruct Ht as [t' [Ht Ht']].
      rewrite (Febag.mem_eq_1 _ _ _ _ Ht); apply Febag.in_elements_mem; assumption.
    - rewrite eval_sql_from_item_unfold, Febag.mem_map in Ht.
      destruct Ht as [t' [Ht Ht']].
      rewrite tuple_eq in Ht.
      rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)).
      simpl.
      rewrite (Fset.equal_eq_1 _ _ _ _ (support_mk_tuple _ _ _)), 3 map_map.
      unfold att_as_as_pair.
      rewrite Fset.equal_spec; intro a; do 2 apply f_equal.
      rewrite <- map_eq; intros x; intros; destruct x; trivial.
      }
  intros lf Hn t env Ht.
  unfold Data.N_product_bag in Ht.
  rewrite Febag.mem_mk_bag, Oeset.mem_bool_true_iff in Ht.
  destruct Ht as [t' [Ht Ht']].
  rewrite tuple_eq in Ht; rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)); clear Ht.
  rewrite Data.in_N_product_list in Ht'.
  destruct Ht' as [llt [H1 [H2 H3]]]; subst t'.
  rewrite map_map in H2.
  assert (H3 : support T (Data.N_combine (tuple T) (OTuple T) (build_data T) (map snd llt)) =S=
               Fset.Union (A T) (map (support T) (map snd llt))).
  {
    clear H1 H2; set (l := map snd llt); clearbody l; clear llt.
    induction l as [ | t1 l]; simpl.
    - unfold empty_tuple; rewrite (Fset.equal_eq_1 _ _ _ _ (support_mk_tuple _ _ _)).
      apply Fset.equal_refl.
    - unfold join_tuple; rewrite (Fset.equal_eq_1 _ _ _ _ (support_mk_tuple _ _ _)).
      apply Fset.union_eq_2; apply IHl.
  }
  rewrite (Fset.equal_eq_1 _ _ _ _ H3), map_map; clear H3.
  revert llt H1 H2; induction lf as [ | f1 lf]; intros llt H1 H2.
  + destruct llt; [ | discriminate H2].
    apply Fset.equal_refl.
  + destruct llt as [ | [l1 t1] llt]; [discriminate H2 | ].
    simpl in H2; injection H2; clear H2; intros H2 H3; simpl map.
    rewrite 2 (Fset.Union_unfold _ (_ :: _)).
    apply Fset.union_eq.
    * apply IH with env; 
        [refine (le_trans _ _ _ _ Hn);
         apply in_list_size; rewrite in_map_iff; exists f1; split; [ | left]; trivial | ].
      rewrite Febag.mem_unfold; apply Oeset.in_mem_bool.
      apply H1; left; apply f_equal2; trivial.
    * apply IHlf; 
        [refine (le_trans _ _ _ _ Hn); simpl; apply le_plus_r | intros; apply H1; right | ]; 
        trivial.
Qed.

Lemma well_sorted_sql_query :
  well_sorted_sql_table -> 
  forall sq t env, t inBE (eval_sql_query env sq) -> support T t =S= sql_sort sq.
Proof.
intros W sq.
apply (proj1 (well_sorted_sql_query_etc W (tree_size (tree_of_sql_query sq)))).
apply le_n.
Qed.

Lemma well_sorted_sql_from_item :
  well_sorted_sql_table ->
  forall x t env, t inBE (eval_sql_from_item env x) -> support T t =S= sql_from_item_sort x.
Proof.
intros W [sq r] t env H; rewrite eval_sql_from_item_unfold, Febag.map_unfold in H.
rewrite Febag.mem_mk_bag, Oeset.mem_bool_true_iff in H.
destruct H as [t' [Ht H]]; rewrite in_map_iff in H.
destruct H as [t'' [H Ht'']]; subst t'.
rewrite tuple_eq in Ht; rewrite (Fset.equal_eq_1 _ _ _ _ (proj1 Ht)).
assert (Wt'' := well_sorted_sql_query W _ _ _ (Febag.in_elements_mem _ _ _ Ht'')).
destruct r as [ | r].
- simpl projection; rewrite (Fset.equal_eq_1 _ _ _ _ Wt''); simpl.
  apply Fset.equal_refl.
- simpl; rewrite (Fset.equal_eq_1 _ _ _ _ (support_mk_tuple _ _ _)), 3 map_map.
  rewrite Fset.equal_spec; intro a; do 2 apply f_equal.
  rewrite <- map_eq.
  intros [_a b] _; apply refl_equal.
Qed.

(** * Removing WHERE *)
(** * Removing WHERE lemma *)
Lemma eval_sql_query_remove_where :
  forall env1 env2 s lsq f1 gby f2, equiv_env env1 env2 ->
    eval_sql_query env1 (Sql_Select s lsq f1 gby f2) =BE=
    eval_sql_query env2
      (Sql_Select
         s
         (From_Item 
            (Sql_Select Select_Star lsq (Sql_Atom Sql_True) Group_Fine f1) Att_Ren_Star :: nil)
         (Sql_Atom Sql_True)
         gby f2).
Proof.
intros env1 env2 s lsq f1 gby f2 He.
rewrite Febag.nb_occ_equal, 2 (eval_sql_query_unfold _ (Sql_Select _ _ _ _ _)); intro t.
rewrite 2 Febag.nb_occ_mk_bag.
apply (Oeset.nb_occ_map_eq_2_3 (OLTuple T) (OTuple T)).
- intros x1 x2 Hx; simpl; apply projection_eq; env_tac.
- clear t; intro l; apply Oeset.nb_occ_filter_eq.
  + clear l; intro l; apply make_groups_eq; [assumption | ].
    simpl eval_sql_formula3 at 2; rewrite (Febag.equal_eq_2 _ _ _ _ (Febag.filter_true _ _)).
    rewrite (map_unfold _ (_ :: _)), (map_unfold _ nil).
    unfold Data.N_product_bag; rewrite (map_unfold _ (_ :: _)), (map_unfold _ nil).
    rewrite Febag.nb_occ_equal; intro x.
    rewrite Febag.nb_occ_filter;
      [ | intros x1 x2 _ Hx; rewrite (eval_sql_formula3_eq f1 (env2 := env_t env1 x2)); env_tac].
    rewrite 2 Febag.nb_occ_mk_bag, eval_sql_from_item_unfold, eval_sql_query_unfold.
    rewrite  (permut_nb_occ 
                (OTuple T) x (permut_refl_alt _ _ _ (Data.N_product_list_1 _ (OTuple T) _ _))).
    cbv beta iota zeta.
    simpl eval_sql_formula3; unfold SqlCommon.make_groups; rewrite filter_map, 2 map_map.
    simpl projection.
    rewrite (map_id (fun x : tuple T => x)); [ | intros; apply refl_equal].
    unfold N_product_bag, Febag.map; rewrite map_map, (map_id (fun x : tuple T => x)); 
      [ | intros; apply refl_equal].
    do 2 (rewrite <- Febag.nb_occ_elements, Febag.nb_occ_mk_bag).
    rewrite Oeset.nb_occ_filter; 
      [ | intros x1 x2 Hx1 Hx; 
          rewrite (eval_sql_formula3_eq f1 (env2 := env_g env2 Group_Fine (x2 :: nil))); env_tac].
    rewrite <- (eval_sql_formula3_eq 
                  f1 (env1 := env_t env1 x) (env2 := env_g env2 Group_Fine (x :: nil)));  
      [ | env_tac].
    case (eval_sql_formula3 (env_t env1 x) f1);
      [ | rewrite N.mul_0_r; apply refl_equal | rewrite N.mul_0_r; apply refl_equal].
    rewrite N.mul_1_r, <- Febag.nb_occ_elements, Febag.nb_occ_filter; [ | intros; trivial].
    rewrite N.mul_1_r, Febag.nb_occ_mk_bag.
    apply permut_nb_occ; apply permut_refl_alt; apply Data.N_product_list_map_eq.
    intros f Hf.
    apply Febag.elements_spec1; apply eval_sql_from_item_eq; trivial.
  + intros x1 x2 Hx1 Hx.
    rewrite (eval_sql_formula3_eq f2 (env2 := env_g env2 gby x2)); env_tac.
Qed.

(** * Removing WHERE Property *)
Fixpoint sql_query_has_no_where sq :=
  match sq with
    | Sql_Table _ => true
    | Sql_Set _ sq1 sq2 => (sql_query_has_no_where sq1) && (sql_query_has_no_where sq2)
    | Sql_Select _ lsq f1 _ f2 =>
      match f1 with
        | Sql_Atom Sql_True => true
        | _ => false
      end && forallb sql_item_has_no_where lsq && sql_formula_has_no_where f2
  end

with sql_item_has_no_where f :=
       match f with
         | From_Item sq _ => sql_query_has_no_where sq
       end

with sql_formula_has_no_where f :=
       match f with
         | Sql_Conj _ f1 f2 => (sql_formula_has_no_where f1) && (sql_formula_has_no_where f2)
         | Sql_Not f => sql_formula_has_no_where f
         | Sql_Atom a => sql_atom_has_no_where a
       end

with sql_atom_has_no_where a :=
       match a with
         | Sql_True => true
         | Sql_Pred _ _ => true
         | Sql_Quant _ _ _ sq => sql_query_has_no_where sq
         | Sql_In _ sq 
         | Sql_Exists sq => sql_query_has_no_where sq
       end.

(** * Fixpoint in order to remove WHERE *)
Fixpoint sql_query_to_sql1 sq :=
  match sq with
    | Sql_Table r => Sql_Table r
    | Sql_Set o q1 q2 => Sql_Set o (sql_query_to_sql1 q1) (sql_query_to_sql1 q2)
    | Sql_Select s lsq f1 g f2 =>
      Sql_Select
        s
        (From_Item 
           (Sql_Select 
              Select_Star 
              (map sql_item_to_sql1 lsq) 
              (Sql_Atom Sql_True) 
              Group_Fine 
              (sql_formula_to_sql1 f1))
           Att_Ren_Star :: nil)
        (Sql_Atom Sql_True) 
        g 
        (sql_formula_to_sql1 f2)
  end

with sql_item_to_sql1 x :=
  match x with 
    | From_Item sq r => From_Item (sql_query_to_sql1 sq) r
  end
  
with sql_formula_to_sql1 f :=
   match f with
     | Sql_Conj o f1 f2 => Sql_Conj o (sql_formula_to_sql1 f1) (sql_formula_to_sql1 f2)
     | Sql_Not f => Sql_Not (sql_formula_to_sql1 f)
     | Sql_Atom a => Sql_Atom (sql_atom_to_sql1 a)
   end

with sql_atom_to_sql1 a :=
   match a with
     | Sql_True => Sql_True
     | Sql_Pred p l => Sql_Pred p l
     | Sql_Quant qtf p l sq => Sql_Quant qtf p l (sql_query_to_sql1 sq)
     | Sql_In s sq => Sql_In s (sql_query_to_sql1 sq)
     | Sql_Exists sq => Sql_Exists (sql_query_to_sql1 sq)
   end.

Lemma sql_query_to_sql1_unfold :
  forall sq, sql_query_to_sql1 sq =
  match sq with
    | Sql_Table r => Sql_Table r
    | Sql_Set o q1 q2 => Sql_Set o (sql_query_to_sql1 q1) (sql_query_to_sql1 q2)
    | Sql_Select s lsq f1 g f2 =>
      Sql_Select
        s
        (From_Item
           (Sql_Select 
              Select_Star (map sql_item_to_sql1 lsq) (Sql_Atom Sql_True) 
              Group_Fine (sql_formula_to_sql1 f1))
           Att_Ren_Star :: nil)
        (Sql_Atom Sql_True) g (sql_formula_to_sql1 f2)
  end.
Proof.
intro sq; case sq; intros; apply refl_equal.
Qed.

(** Sort preservation of removing WHERE *)
Lemma sql_sort_sql_query_to_sql1 : 
  forall sq, sql_sort (sql_query_to_sql1 sq) =S= sql_sort sq.
Proof.
intro sq; set (n := tree_size (tree_of_sql_query sq)).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert sq Hn; induction n as [ | n]; [intros sq Hn; destruct sq; inversion Hn | ].
intros sq Hn; destruct sq as [r | o sq1 sq2 | s lsq f1 g f2]; simpl.
- apply Fset.equal_refl.  
- apply IHn; sql_size_tac.
- destruct s; [ | apply Fset.equal_refl].
  rewrite map_map, Fset.equal_spec; intro a.
  rewrite Fset.mem_union, Fset.empty_spec, Bool.orb_false_r.
  assert (IH : forall sq r, 
                 In (From_Item sq r) lsq -> sql_sort (sql_query_to_sql1 sq) =S= sql_sort sq).
  {
    intros sq r H; apply IHn; sql_size_tac.
  }
  rewrite eq_bool_iff, 2 Fset.mem_Union; split.
  + intros [s [Hs Ha]]; rewrite in_map_iff in Hs.
    destruct Hs as [[sq [ | r]] [Hs Hf]]; subst s.
    * rewrite (Fset.mem_eq_2 _ _ _ (IH _ _ Hf)) in Ha.
      eexists; split; [ | apply Ha]; 
      rewrite in_map_iff; eexists; split; [ | apply Hf]; trivial.
    * eexists; split; [ | apply Ha].
      rewrite in_map_iff; eexists; split; [ | apply Hf]; trivial.
  + intros [s [Hs Ha]]; rewrite in_map_iff in Hs.
    destruct Hs as [[sq [ | r]] [Hs Hf]]; subst s.
    * rewrite <- (Fset.mem_eq_2 _ _ _ (IH _ _ Hf)) in Ha.
      eexists; split; [ | apply Ha]; 
      rewrite in_map_iff; eexists; split; [ | apply Hf]; trivial.
    * eexists; split; [ | apply Ha].
      rewrite in_map_iff; eexists; split; [ | apply Hf]; trivial.
Qed.

Lemma from_item_sort_sql_item_to_sql1 : 
  forall f, sql_from_item_sort (sql_item_to_sql1 f) =S= sql_from_item_sort f.
Proof.
intros [sq [ | r]]; simpl.
- apply sql_sort_sql_query_to_sql1.
- apply Fset.equal_refl.
Qed.

(** Property is ensured : removing WHERE using sql_query_to_sql1 actually removes WHERE *)
Lemma sql_query_to_sql1_has_no_where :
  forall n, 
    (forall sq, tree_size (tree_of_sql_query sq) <= n -> 
                sql_query_has_no_where (sql_query_to_sql1 sq) = true) /\
    (forall f, tree_size (tree_of_sql_from_item f) <= n -> 
               sql_item_has_no_where (sql_item_to_sql1 f) = true) /\
    (forall f, tree_size (tree_of_sql_formula f) <= n ->
                    sql_formula_has_no_where (sql_formula_to_sql1 f) = true) /\
    (forall a, tree_size (tree_of_sql_atom a) <= n ->
                    sql_atom_has_no_where (sql_atom_to_sql1 a) = true).
Proof.
intro n; induction n as [ | n]; 
  (split; [intros sq Hn | 
           split; [intros f Hn | 
                   split; [intros f Hn | intros a Hn]]]).
- destruct sq; inversion Hn.
- destruct f; inversion Hn. 
- destruct f; inversion Hn. 
- destruct a; inversion Hn.
- destruct sq as [r | o sq1 sq2 | s lsq f1 g f2]; simpl; trivial.
  + rewrite 2 (proj1 IHn); trivial; sql_size_tac.
  + rewrite 2 (proj1 (proj2 (proj2 IHn))), 3 Bool.andb_true_r.
    * rewrite forallb_forall; intros x Hx; rewrite in_map_iff in Hx.
      destruct Hx as [y [Hx Hy]]; subst x.
      apply (proj1 (proj2 IHn)); sql_size_tac.
    * sql_size_tac.
    * sql_size_tac.
- destruct f as [sq r]; simpl.
  apply (proj1 IHn); sql_size_tac.
- destruct f as [o f1 f2 | f | a]; simpl.
  + rewrite 2 (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
  + rewrite (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
  + rewrite (proj2 (proj2 (proj2 IHn))); trivial; sql_size_tac.
- destruct a as [ | p l | qtf p l sq | s sq | sq]; simpl; trivial.
  + apply (proj1 IHn); sql_size_tac.
  + apply (proj1 IHn); sql_size_tac.
  + apply (proj1 IHn); sql_size_tac.
Qed.

(** Semantics' preservation of removing WHERE *)
Lemma sql_query_to_sql1_is_sound_etc :
  forall n, 
    (forall sq, tree_size (tree_of_sql_query sq) <= n -> 
                forall env1 env2, equiv_env env1 env2 ->
                eval_sql_query env1 (sql_query_to_sql1 sq) =BE= eval_sql_query env2 sq) /\
    (forall f, tree_size (tree_of_sql_from_item f) <= n -> 
                forall env1 env2, equiv_env env1 env2 ->
               eval_sql_from_item env1 (sql_item_to_sql1 f) =BE= eval_sql_from_item env2 f) /\
    (forall f, tree_size (tree_of_sql_formula f) <= n ->
                forall env1 env2, equiv_env env1 env2 ->
                    eval_sql_formula3 env1 (sql_formula_to_sql1 f) = eval_sql_formula3 env2 f) /\
    (forall a, tree_size (tree_of_sql_atom a) <= n ->
                forall env1 env2, equiv_env env1 env2 ->
                    eval_sql_atom3 env1 (sql_atom_to_sql1 a) = eval_sql_atom3 env2 a).
Proof.
intro n; induction n as [ | n]; repeat split.
- intros sq Hn; destruct sq; inversion Hn.
- intros f Hn; destruct f; inversion Hn.
- intros f Hn; destruct f; inversion Hn.
- intros a Hn; destruct a; inversion Hn.
- intros sq Hn env1 env2 He.
  destruct sq as [r | o sq1 sq2 | s lsq f1 g f2].
  + rewrite Febag.nb_occ_equal; intro; apply refl_equal.
  + simpl; rewrite Febag.nb_occ_equal; intro t.
    assert (IH1 : eval_sql_query env1 (sql_query_to_sql1 sq1) =BE= eval_sql_query env2 sq1).
    {
      apply (proj1 IHn); trivial; sql_size_tac.
    }
    assert (IH2 : eval_sql_query env1 (sql_query_to_sql1 sq2) =BE= eval_sql_query env2 sq2).
    {
      apply (proj1 IHn); trivial; sql_size_tac.
    }
    rewrite Febag.nb_occ_equal in IH1, IH2.
    rewrite (Fset.equal_eq_1 _ _ _ _ (sql_sort_sql_query_to_sql1 _)).
    rewrite (Fset.equal_eq_2 _ _ _ _ (sql_sort_sql_query_to_sql1 _)).
    case (sql_sort sq1 =S?= sql_sort sq2); [ | apply refl_equal].
    destruct o; simpl.
    * rewrite 2 Febag.nb_occ_union, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_union_max, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_inter, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_diff, IH1, IH2; apply refl_equal.
  + rewrite sql_query_to_sql1_unfold.
    assert (_He : equiv_env env2 env1).
    {
      apply equiv_env_sym.
      - apply equiv_env_slice_sym.
      - assumption.
    }
    rewrite <- (Febag.equal_eq_1 
               _ _ _ _ (eval_sql_query_remove_where _ _ _ _ _ _He)).
    rewrite 2 (eval_sql_query_unfold _ (Sql_Select _ _ _ _ _)); cbv beta iota zeta.
    rewrite Febag.nb_occ_equal; intro t.
    rewrite 2 Febag.nb_occ_mk_bag.
    apply (Oeset.nb_occ_map_eq_2_3 (OLTuple T)).
    * intros; apply projection_eq; env_tac.
    * {
        intro l; apply Oeset.nb_occ_filter_eq.
        - clear l; intro l; apply make_groups_eq; [apply equiv_env_refl | ].
          apply Febag.filter_eq.
          + rewrite map_map; unfold N_product_bag; rewrite Febag.nb_occ_equal.
            intro; rewrite 2 Febag.nb_occ_mk_bag, 2 map_map.
            apply permut_nb_occ; apply permut_refl_alt; apply Data.N_product_list_map_eq.
            intros x Hx; apply Febag.elements_spec1.
            apply (proj1 (proj2 IHn)); [sql_size_tac | ].
            apply equiv_env_refl.
          + intros x1 x2 Hx1 Hx.
            rewrite (proj1 (proj2 (proj2 IHn)) f1) with (env_t env2 x1) (env_t env2 x2);
              [apply refl_equal | sql_size_tac | env_tac].
        - intros x1 x2 Hx1 Hx.
          rewrite (proj1 (proj2 (proj2 IHn)) f2) with (env_g env2 g x1) (env_g env2 g x2);
            [apply refl_equal | sql_size_tac | env_tac].
      }
- intros f Hn env1 env2 He.
  destruct f as [sq r].
  simpl sql_item_to_sql1; rewrite 2 eval_sql_from_item_unfold.
  rewrite Febag.nb_occ_equal.
  intro t; unfold Febag.map.  
  rewrite 2 Febag.nb_occ_mk_bag.
  apply (Oeset.nb_occ_map_eq_2_3 (OTuple T)); clear t.
  + intros x1 x2 Hx; apply projection_eq; env_tac.
  + intro t; rewrite <- 2 Febag.nb_occ_elements; revert t; rewrite <- Febag.nb_occ_equal.
    apply (proj1 IHn); trivial; sql_size_tac.
- intros f Hn env1 env2 He.
  destruct f as [a f1 f2 | f | a].
  + assert (IH1 : eval_sql_formula3 env1 (sql_formula_to_sql1 f1) = eval_sql_formula3 env2 f1).
    {
      apply (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
    }
    assert (IH2 : eval_sql_formula3 env1 (sql_formula_to_sql1 f2) = eval_sql_formula3 env2 f2).
    {
      apply (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
    }
    simpl; rewrite IH1, IH2; trivial.
  + assert (IH : eval_sql_formula3 env1 (sql_formula_to_sql1 f) = eval_sql_formula3 env2 f).
    {
      apply (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
    }
    simpl; rewrite IH; trivial.
  + simpl; apply (proj2 (proj2 (proj2 IHn))); trivial; sql_size_tac.
- intros a Hn env1 env2 He.
  destruct a as [ | p l | qtf p l sq | s sq | sq]; simpl sql_atom_to_sql1; 
  try (apply refl_equal; fail);
  rewrite 2 eval_sql_atom3_unfold.
  + apply f_equal.
    rewrite <- map_eq; intros x Hx.
    apply interp_aggterm_eq; trivial.
  + cbv beta iota zeta; apply (interp_quant3_eq (OTuple T)).
    * intros x; rewrite <- 2 Febag.mem_unfold; apply Febag.mem_eq_2.
      apply (proj1 IHn); trivial; sql_size_tac.
    * {
        intros x1 x2 Hx1 Hx; apply f_equal; apply f_equal2.
        - rewrite <- map_eq; intros x _Hx.
          apply interp_aggterm_eq; trivial.
        - rewrite tuple_eq in Hx.
          rewrite <- (Fset.elements_spec1 _ _ _ (proj1 Hx)), <- map_eq.
          intros x _Hx; apply (proj2 Hx).
      } 
  + apply if_eq; trivial.
    apply Febag.mem_eq.
    * apply (proj1 IHn); trivial; sql_size_tac.
    * apply projection_eq; trivial.
  + apply if_eq; trivial.
    rewrite 2 Febag.is_empty_spec; apply Febag.equal_eq_1.
    apply (proj1 IHn); trivial; sql_size_tac.
Qed.    

(** * Removing STAR *)
(** Lemma stating how to remove STAR *)

Lemma mk_tuple_id_alt :
  forall env g l sa, l <> nil ->
   (forall t, In t l -> support T t =S= sa) ->
   projection (env_g env g l) Select_Star =t=
   projection (env_g env g l)
     (Select_List
        (map (fun a : attribute T => Select_As (A_Expr (F_Dot E a)) a)
           ({{{sa}}}))).
Proof.
intros env g l sa H1 H2; unfold projection, env_g.
rewrite 3 map_map.
case_eq (quicksort (OTuple T) l).
- intro Hql.
  assert (Abs := length_quicksort (OTuple T) l).
  rewrite Hql in Abs; destruct l; [apply False_rec; apply H1; apply refl_equal | discriminate Abs].
- intros y1 ql Hql.
  destruct l as [ | x1 l1]; [apply False_rec; apply H1; apply refl_equal | ].
  rewrite tuple_eq, (Fset.equal_eq_2 _ _ _ _ (support_mk_tuple _ _ _)), map_id; 
    [ | intros; apply refl_equal].
  rewrite (Fset.equal_eq_2 _ _ _ _ (Fset.mk_set_idem _ _)).
  destruct l1 as [ | x2 l1]; split.
  + apply H2; left; apply refl_equal.
  + rewrite quicksort_equation in Hql; simpl in Hql; rewrite quicksort_equation in Hql.
    injection Hql; clear Hql; do 2 intro; subst y1 ql.
    intro a.
    assert (Ha' : (a inS? sa) = (a inS? support T x1)).
    {
      apply sym_eq; apply (Fset.mem_eq_2 _ _ _ (H2 _ (or_introl _ (refl_equal _)))).
    }
    case_eq (a inS? support T x1); intro Ha.
    * {
        rewrite dot_mk_tuple, Oset.find_map.
        - simpl; unfold interp_dot_env_slice; simpl; rewrite Ha; trivial.
        - apply Fset.mem_in_elements; rewrite Ha'; assumption.
        - rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _)), Ha'; assumption.
      } 
    * unfold dot; rewrite Ha.
      rewrite (Fset.mem_eq_2 _ _ _ (support_mk_tuple _ _ _)).
      rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _)), Ha', Ha.
      apply refl_equal.
  + apply H2; apply (In_quicksort (OTuple T)).
    rewrite Hql; left; apply refl_equal.
  + intro a. 
    assert (Ha' : (a inS? sa) = (a inS? support T y1)).
    {
      apply sym_eq; apply Fset.mem_eq_2.
      apply H2; apply (In_quicksort (OTuple T)); rewrite Hql; left; trivial.
    }
     case_eq (a inS? support T y1); intro Ha.
    * {
        rewrite dot_mk_tuple, Oset.find_map.
        - simpl; unfold interp_dot_env_slice; simpl; rewrite Hql, Ha; trivial.
        - apply Fset.mem_in_elements; rewrite Ha'; assumption.
        - rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _)), Ha'; assumption.
      } 
    * unfold dot; rewrite Ha.
      rewrite (Fset.mem_eq_2 _ _ _ (support_mk_tuple _ _ _)).
      rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _)), Ha', Ha.
      apply refl_equal.
Qed.

(*
Lemma mk_tuple_id_alt :
  forall x sa g t l env, 
    x =t= match quicksort (OTuple T) (t :: l) with nil => t | t1 :: _ => t1 end ->
    support T x =S= sa ->
    x =t=
    mk_tuple T
     (Fset.mk_set (A T)
        (map (fun x : attribute T => fst (x, A_Expr (F_Dot E x))) ({{{sa}}})))
     (fun a : attribute T =>
      match
        Oset.find (OAtt T) a
          (map (fun x : attribute T => (x, A_Expr (F_Dot E x))) ({{{sa}}}))
      with
      | Some e =>
          interp_aggterm_ ((sa, g, t :: l) :: env) e
      | None => dot T (default_tuple T (emptysetS)) a
      end).
Proof.
intros x sa g t l env Hx Hsa.
rewrite tuple_eq, (Fset.equal_eq_1 _ _ _ _ Hsa), 
  (Fset.equal_eq_2 _ _ _ _ (support_mk_tuple _ _ _)).
rewrite map_id; [ | intros; apply refl_equal].
split; 
  [rewrite Fset.equal_spec; intro a; rewrite Fset.mem_mk_set, <- Fset.mem_elements; trivial | ].
intros a; unfold dot.
rewrite (Fset.mem_eq_2 _ _ _ Hsa), Fset.mem_elements, 
      (Fset.mem_eq_2 _ _ _ (support_mk_tuple _ _ _)), Fset.mem_mk_set.
case_eq (Oset.mem_bool (OAtt T) a ({{{sa}}})); [intro Ha | intros; trivial].
unfold mk_tuple; rewrite dot_mk_tuple_; rewrite Fset.mem_mk_set, Ha; trivial.
case_eq (Oset.find (OAtt T) a (map (fun x : attribute T => (x, A_Expr (F_Dot E x))) ({{{sa}}}))).
- intros e He.
  generalize (Oset.find_some _ _ _ He); clear He; intro He.
  rewrite in_map_iff in He.
  destruct He as [_a [_Ha He]]; 
              simpl in _Ha; injection _Ha; clear _Ha; do 2 intro; subst _a e.
  simpl; unfold interp_dot_env_slice.
  assert (L := length_quicksort (OTuple T) (t :: l)).
  case_eq (quicksort (OTuple T) (t :: l)); [intro Hq; rewrite Hq in L; discriminate L | ].
  intros t1 q1 Hq; rewrite Hq in Hx.
  rewrite tuple_eq in Hx; rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 Hx)), (Fset.mem_eq_2 _ _ _ Hsa).
  rewrite (Fset.in_elements_mem _ _ _ He).
  rewrite <- (proj2 Hx).
  unfold dot.
  rewrite (Fset.mem_eq_2 _ _ _ Hsa), (Fset.in_elements_mem _ _ _ He); trivial.
- intro Ka; apply False_rec.
  eapply (Oset.find_none (OAtt T)); [apply Ka | ].
  rewrite in_map_iff; exists a; 
        split; [apply refl_equal |rewrite Oset.mem_bool_true_iff in Ha ]; trivial.
Qed.
*)

Lemma eval_sql_query_remove_star :
  well_sorted_sql_table ->
  forall env1 env2 lsq f1 g f2, equiv_env env1 env2 ->
    let s := map (fun a => Select_As (A_Expr (F_Dot E a)) a)
                 ({{{Fset.Union (A T) (List.map sql_from_item_sort lsq)}}}) in
    eval_sql_query env1 (Sql_Select Select_Star lsq f1 g f2) =BE=
    eval_sql_query env2 (Sql_Select (Select_List s) lsq f1 g f2).
Proof.
intros WI env1 env2 lsq f1 g f2 Henv s.
rewrite <- (Febag.equal_eq_2 _ _ _ _ (eval_sql_query_eq _ Henv)).
rewrite 2 (eval_sql_query_unfold _ (Sql_Select _ _ _ _ _)).
cbv beta iota zeta.
rewrite Febag.nb_occ_equal; intro t.
rewrite 2 Febag.nb_occ_mk_bag.
apply (Oeset.nb_occ_map_eq_2_alt (OLTuple T)).
clear t; intros l Hl.
rewrite filter_In in Hl; destruct Hl as [Hl _].
unfold SqlCommon.make_groups in Hl; destruct g as [g | ].
- assert (Kl := in_partition_list_expr (OTuple T) (FVal T) _ _ _ (interp_funterm_eq_ _ _ ) Hl).
   rewrite (partition_list_expr_partition_list_expr2 (OTuple T)), filter_In in Hl.
  destruct Hl as [_ Hl].
  apply mk_tuple_id_alt.
  + destruct l; [discriminate Hl | discriminate].
  + intros t Ht.
    apply (proj2 (well_sorted_sql_query_etc WI _) lsq (le_n _)) with env1. 
    assert (Kt := Febag.in_elements_mem _ _ _ (Kl _ Ht)).
    rewrite Febag.mem_nb_occ, Febag.nb_occ_filter in Kt;
      [ | intros x1 x2 _ Hx; 
          rewrite (eval_sql_formula3_eq f1) with (env_t env1 x1) (env_t env1 x2);
          env_tac].
    apply Febag.nb_occ_mem.
    case_eq (Febag.nb_occ BTupleT t (N_product_bag (map (eval_sql_from_item env1) lsq))); 
      [ | discriminate].
    intro J; rewrite J, N.mul_0_l in Kt; discriminate Kt.
  + apply interp_funterm_eq_.
- rewrite in_map_iff in Hl.
  destruct Hl as [t [Hl Ht]]; subst l.
  apply mk_tuple_id_alt; [discriminate | ].
  intros t1 Ht1; simpl in Ht1; destruct Ht1 as [Ht1 | Ht1]; [subst t1 | contradiction Ht1].
  apply (proj2 (well_sorted_sql_query_etc WI _) lsq (le_n _)) with env1. 
  assert (Kt := Febag.in_elements_mem _ _ _ Ht).
  rewrite Febag.mem_nb_occ, Febag.nb_occ_filter in Kt;
    [ | intros x1 x2 _ Hx; 
        rewrite (eval_sql_formula3_eq f1) with (env_t env1 x1) (env_t env1 x2);
        env_tac].
  apply Febag.nb_occ_mem.
    case_eq (Febag.nb_occ BTupleT t (N_product_bag (map (eval_sql_from_item env1) lsq))); 
      [ | discriminate].
    intro J; rewrite J, N.mul_0_l in Kt; discriminate Kt.
Qed.

(** Property of having no STAR *)
Fixpoint sql_query_has_no_star sq :=
  match sq with
    | Sql_Table _ => true
    | Sql_Set _ sq1 sq2 => (sql_query_has_no_star sq1) && (sql_query_has_no_star sq2)
    | Sql_Select s lsq f1 _ f2 =>
      match s with
        | Select_Star => false
        | _ => true
      end && forallb sql_item_has_no_star lsq 
          && sql_formula_has_no_star f1
          && sql_formula_has_no_star f2
  end

with sql_item_has_no_star f :=
   match f with
     | From_Item sq _ => sql_query_has_no_star sq
   end

with sql_formula_has_no_star f :=
   match f with
     | Sql_Conj _ f1 f2 => (sql_formula_has_no_star f1) && (sql_formula_has_no_star f2)
     | Sql_Not f => sql_formula_has_no_star f
     | Sql_Atom a => sql_atom_has_no_star a
   end

with sql_atom_has_no_star a :=
   match a with
     | Sql_True => true
     | Sql_Pred _ _ => true
     | Sql_Quant _ _ _ sq  
     | Sql_In _ sq
     | Sql_Exists sq => sql_query_has_no_star sq
   end.

Lemma sql_query_has_no_star_unfold :
  forall sq, sql_query_has_no_star sq =
  match sq with
    | Sql_Table _ => true
    | Sql_Set _ sq1 sq2 => (sql_query_has_no_star sq1) && (sql_query_has_no_star sq2)
    | Sql_Select s lsq f1 _ f2 =>
      match s with
        | Select_Star => false
        | _ => true
      end && forallb sql_item_has_no_star lsq 
          && sql_formula_has_no_star f1
          && sql_formula_has_no_star f2
  end.
Proof.
intro sq; case sq; intros; trivial.
Qed.

Lemma sql_item_has_no_star_unfold :
  forall f, sql_item_has_no_star f =
   match f with
     | From_Item sq _ => sql_query_has_no_star sq
   end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma sql_formula_has_no_star_unfold :
  forall f, sql_formula_has_no_star f =
   match f with
     | Sql_Conj _ f1 f2 => (sql_formula_has_no_star f1) && (sql_formula_has_no_star f2)
     | Sql_Not f => sql_formula_has_no_star f
     | Sql_Atom a => sql_atom_has_no_star a
   end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma sql_atom_has_no_star_unfold :
  forall a, sql_atom_has_no_star a =
   match a with
     | Sql_True => true
     | Sql_Pred _ _ => true
     | Sql_Quant _ _ _ sq 
     | Sql_In _ sq
     | Sql_Exists sq => sql_query_has_no_star sq
   end.
Proof.
intro a; case a; intros; apply refl_equal.
Qed.

(** Fixpoint in order to recursively remove STAR *)
Fixpoint sql_query_to_sql2 (sq : sql_query) := 
  match sq with
    | Sql_Table r => Sql_Table r
    | Sql_Set o sq1 sq2 => Sql_Set o (sql_query_to_sql2 sq1) (sql_query_to_sql2 sq2)
    | Sql_Select s lsq f1 g f2 =>
        Sql_Select 
          (match s with
             | Select_Star => 
               Select_List (sql_id_renaming (Fset.Union _ (map sql_from_item_sort lsq)))
             | Select_List _ => s
           end)
          (map sql_from_item_to_sql2 lsq) (sql_formula_to_sql2 f1) g (sql_formula_to_sql2 f2)
  end

with sql_from_item_to_sql2 x :=
   match x with
     | From_Item sq rho => From_Item (sql_query_to_sql2 sq) rho
   end

with sql_formula_to_sql2 f :=
  match f with
    | Sql_Conj o f1 f2 => Sql_Conj o (sql_formula_to_sql2 f1) (sql_formula_to_sql2 f2)
    | Sql_Not f => Sql_Not (sql_formula_to_sql2 f)
    | Sql_Atom a => Sql_Atom (sql_atom_to_sql2 a)
  end

with sql_atom_to_sql2 a :=
  match a with
    | Sql_True => Sql_True
    | Sql_Pred p l => Sql_Pred p l
    | Sql_Quant qtf p l sq => Sql_Quant qtf p l (sql_query_to_sql2 sq)
    | Sql_In s sq => Sql_In s (sql_query_to_sql2 sq)
    | Sql_Exists sq => Sql_Exists (sql_query_to_sql2 sq)
  end.
         
Lemma sql_query_to_sql2_unfold : 
  forall sq, sql_query_to_sql2 sq =
  match sq with
    | Sql_Table r => Sql_Table r
    | Sql_Set o sq1 sq2 => Sql_Set o (sql_query_to_sql2 sq1) (sql_query_to_sql2 sq2)
    | Sql_Select s lsq f1 g f2 =>
        Sql_Select 
          (match s with
             | Select_Star => 
               Select_List (sql_id_renaming (Fset.Union _ (map sql_from_item_sort lsq)))
             | Select_List _ => s
           end)
          (map sql_from_item_to_sql2 lsq) (sql_formula_to_sql2 f1) g (sql_formula_to_sql2 f2)
  end.
Proof.
intro sq; case sq; intros; apply refl_equal.
Qed.

Lemma sql_from_item_to_sql2_unfold :
  forall x, sql_from_item_to_sql2 x =
   match x with
     | From_Item sq rho => From_Item (sql_query_to_sql2 sq) rho
   end.
Proof.
intro x; case x; intros; apply refl_equal.
Qed.

Lemma sql_formula_to_sql2_unfold :
  forall f, sql_formula_to_sql2 f =
  match f with
    | Sql_Conj o f1 f2 => Sql_Conj o (sql_formula_to_sql2 f1) (sql_formula_to_sql2 f2)
    | Sql_Not f => Sql_Not (sql_formula_to_sql2 f)
    | Sql_Atom a => Sql_Atom (sql_atom_to_sql2 a)
  end.
Proof.
intro f; case f; intros; apply refl_equal.
Qed.

Lemma sql_atom_to_sql2_unfold :
  forall a, sql_atom_to_sql2 a =
  match a with
    | Sql_True => Sql_True
    | Sql_Pred p l => Sql_Pred p l
    | Sql_Quant qtf p l sq => Sql_Quant qtf p l (sql_query_to_sql2 sq)
    | Sql_In s sq => Sql_In s (sql_query_to_sql2 sq)
    | Sql_Exists sq => Sql_Exists (sql_query_to_sql2 sq)
  end.
Proof.
intro a; case a; intros; apply refl_equal.
Qed.

(** Sort preservation of removing STAR *)
Lemma sql_sort_sql_query_to_sql2 : 
  forall sq, sql_sort (sql_query_to_sql2 sq) =S= sql_sort sq.
Proof.
intro sq; set (n := tree_size (tree_of_sql_query sq)).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert sq Hn; induction n as [ | n]; [intros sq Hn; destruct sq; inversion Hn | ].
intros sq Hn; destruct sq as [r | o sq1 sq2 | s lsq f1 g f2]; simpl.
- apply Fset.equal_refl.  
- apply IHn; sql_size_tac.
- destruct s; [ | apply Fset.equal_refl].
  rewrite map_map, Fset.equal_spec; intro a.
  unfold sql_id_renaming, select_as_as_pair; rewrite map_map, map_id; [ | intros; trivial].
  rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _)); trivial.
Qed.

Lemma sql_from_item_sort_sql_item_to_sql2 : 
  forall f, sql_from_item_sort (sql_from_item_to_sql2 f) =S= sql_from_item_sort f.
Proof.
intros [sq [ | r]]; simpl.
- apply sql_sort_sql_query_to_sql2.
- apply Fset.equal_refl.
Qed.

(** Property is ensured : removing STAR using sql_query_to_sql2 actually removes STAR *)
Lemma sql_query_to_sql2_has_no_star :
  forall n, 
    (forall sq, tree_size (tree_of_sql_query sq) <= n -> 
                sql_query_has_no_star (sql_query_to_sql2 sq) = true) /\
    (forall f, tree_size (tree_of_sql_from_item f) <= n -> 
               sql_item_has_no_star (sql_from_item_to_sql2 f) = true) /\
    (forall f, tree_size (tree_of_sql_formula f) <= n ->
                    sql_formula_has_no_star (sql_formula_to_sql2 f) = true) /\
    (forall a, tree_size (tree_of_sql_atom a) <= n ->
                    sql_atom_has_no_star (sql_atom_to_sql2 a) = true).
Proof.
intro n; induction n as [ | n]; [repeat split; inv_tac x Hn | repeat split].
- intros sq Hn; destruct sq as [r | o sq1 sq2 | [ | s] lsq f1 g f2]; simpl; trivial.
  + rewrite 2 (proj1 IHn); trivial; sql_size_tac.
  + rewrite 2 Bool.andb_true_iff; split; [split | ].
    * rewrite forallb_forall; intros x Hx; rewrite in_map_iff in Hx.
      destruct Hx as [y [Hx Hy]]; subst x.
      apply (proj1 (proj2 IHn)); sql_size_tac.
    * apply (proj1 (proj2 (proj2 IHn))); sql_size_tac.
    * apply (proj1 (proj2 (proj2 IHn))); sql_size_tac.
  + rewrite 2 Bool.andb_true_iff; split; [split | ].
    * rewrite forallb_forall; intros x Hx; rewrite in_map_iff in Hx.
      destruct Hx as [y [Hx Hy]]; subst x.
      apply (proj1 (proj2 IHn)); sql_size_tac.
    * apply (proj1 (proj2 (proj2 IHn))); sql_size_tac.
    * apply (proj1 (proj2 (proj2 IHn))); sql_size_tac.
- intros f Hn; destruct f as [sq r]; simpl.
  apply (proj1 IHn); sql_size_tac.
- intros f Hn; destruct f as [o f1 f2 | f | a]; simpl.
  + rewrite 2 (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
  + rewrite (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
  + rewrite (proj2 (proj2 (proj2 IHn))); trivial; sql_size_tac.
- intros a Hn; destruct a as [ | p l | qtf l sq | s sq | sq]; simpl; trivial.
  + apply (proj1 IHn); sql_size_tac.
  + apply (proj1 IHn); sql_size_tac.
  + apply (proj1 IHn); sql_size_tac.
Qed.

(** Semantics' preservation of removing STAR *)
Lemma sql_query_to_sql2_is_sound_etc :
  well_sorted_sql_table ->
  forall n, 
    (forall sq, tree_size (tree_of_sql_query sq) <= n -> 
                forall env1 env2, equiv_env env1 env2 ->
                eval_sql_query env1 (sql_query_to_sql2 sq) =BE= eval_sql_query env2 sq) /\
    (forall f, 
       tree_size (tree_of_sql_from_item f) <= n -> 
       forall env1 env2, 
         equiv_env env1 env2 ->
         eval_sql_from_item env1 (sql_from_item_to_sql2 f) =BE= eval_sql_from_item env2 f) /\
    (forall f, tree_size (tree_of_sql_formula f) <= n ->
                forall env1 env2, equiv_env env1 env2 ->
                    eval_sql_formula3 env1 (sql_formula_to_sql2 f) = eval_sql_formula3 env2 f) /\
    (forall a, tree_size (tree_of_sql_atom a) <= n ->
                forall env1 env2, equiv_env env1 env2 ->
                 eval_sql_atom3 env1 (sql_atom_to_sql2 a) = eval_sql_atom3 env2 a).
Proof.
intros WI n; induction n as [ | n]; [repeat split; inv_tac x Hn | repeat split].
- intros sq Hn env1 env2 Henv; destruct sq as [r | o sq1 sq2 | s lsq f1 g f2].
  + rewrite Febag.nb_occ_equal; intro; apply refl_equal.
  + simpl; rewrite Febag.nb_occ_equal; intro t.
    assert (IH1 : eval_sql_query env1 (sql_query_to_sql2 sq1) =BE= eval_sql_query env2 sq1).
    {
      apply (proj1 IHn); trivial; sql_size_tac.
    }
    assert (IH2 : eval_sql_query env1 (sql_query_to_sql2 sq2) =BE= eval_sql_query env2 sq2).
    {
      apply (proj1 IHn); trivial; sql_size_tac.
    }
    rewrite Febag.nb_occ_equal in IH1, IH2.
    rewrite (Fset.equal_eq_1 _ _ _ _ (sql_sort_sql_query_to_sql2 _)).
    rewrite (Fset.equal_eq_2 _ _ _ _ (sql_sort_sql_query_to_sql2 _)).
    case (sql_sort sq1 =S?= sql_sort sq2); [ | apply refl_equal].
    destruct o; simpl.
    * rewrite 2 Febag.nb_occ_union, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_union_max, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_inter, IH1, IH2; apply refl_equal.
    * rewrite 2 Febag.nb_occ_diff, IH1, IH2; apply refl_equal.
  + simpl sql_query_to_sql2.
    destruct s as [ | s].
    * assert (Henv' : equiv_env env2 env1).
      {
        apply equiv_env_sym.
        - apply equiv_env_slice_sym.
        - assumption.
      }
      rewrite (Febag.equal_eq_2 
                 _ _ _ _ (eval_sql_query_remove_star WI _ _ _ _ Henv')).
    rewrite Febag.nb_occ_equal; intro t.
    rewrite 2 (eval_sql_query_unfold _ (Sql_Select _ _ _ _ _)).
    cbv beta iota zeta.
    rewrite 2 Febag.nb_occ_mk_bag.
    {
      apply (Oeset.nb_occ_map_eq_2_3 (OLTuple T)).
      - intros x1 x2 Hx.
        apply projection_eq; env_tac.
      - intro l; apply Oeset.nb_occ_filter_eq.
        + clear l; intro l; apply make_groups_eq; [apply equiv_env_refl | ].
          apply Febag.filter_eq.
          * rewrite map_map; unfold N_product_bag; rewrite Febag.nb_occ_equal.
            intro; rewrite 2 Febag.nb_occ_mk_bag, 2 map_map.
            apply permut_nb_occ; apply permut_refl_alt; apply Data.N_product_list_map_eq.
            intros x Hx; apply Febag.elements_spec1.
            apply (proj1 (proj2 IHn)); [sql_size_tac | apply equiv_env_refl].
          * intros x1 x2 Hx1 Hx.
            rewrite (proj1 (proj2 (proj2 IHn)) f1) with (env_t env1 x1) (env_t env1 x2);
              [apply refl_equal | sql_size_tac | env_tac].
        + intros x1 x2 Hx1 Hx.
          rewrite (proj1 (proj2 (proj2 IHn)) f2) with (env_g env1 g x1) (env_g env1 g x2); 
            [apply refl_equal | sql_size_tac | env_tac].
    } 
    * rewrite Febag.nb_occ_equal; intro t.
      rewrite 2 (eval_sql_query_unfold _ (Sql_Select _ _ _ _ _)).
      cbv beta iota zeta.
      rewrite 2 Febag.nb_occ_mk_bag.
      {
        apply (Oeset.nb_occ_map_eq_2_3 (OLTuple T)).
        - intros x1 x2 Hx; apply projection_eq; env_tac.
        - intro l; apply Oeset.nb_occ_filter_eq.
          + clear l; intro l; apply make_groups_eq; [assumption | ].
            apply Febag.filter_eq.
            * rewrite map_map; unfold N_product_bag; rewrite Febag.nb_occ_equal.
              intro; rewrite 2 Febag.nb_occ_mk_bag, 2 map_map.
              apply permut_nb_occ; apply permut_refl_alt; apply Data.N_product_list_map_eq.
              intros x Hx; apply Febag.elements_spec1.
              apply (proj1 (proj2 IHn)); [sql_size_tac | assumption].
            * intros x1 x2 Hx1 Hx.
              rewrite (proj1 (proj2 (proj2 IHn)) f1) with (env_t env1 x1) (env_t env2 x2); 
                [apply refl_equal | sql_size_tac | env_tac].
          + intros x1 x2 Hx1 Hx.
            rewrite (proj1 (proj2 (proj2 IHn)) f2) with (env_g env1 g x1) (env_g env2 g x2); 
              [apply refl_equal | sql_size_tac | env_tac].
      }
- intros f Hn env1 env2 Henv; destruct f as [sq r]; simpl.
  rewrite Febag.nb_occ_equal.
  intro t; unfold Febag.map.  
  rewrite 2 Febag.nb_occ_mk_bag; apply (Oeset.nb_occ_map_eq_2_3 (OTuple T)); clear t.
  + intros x1 x2 Hx; apply projection_eq; env_tac.
  + intro t; rewrite <- 2 Febag.nb_occ_elements; revert t; rewrite <- Febag.nb_occ_equal.
    apply (proj1 IHn); [sql_size_tac | assumption].
- intros f Hn env1 env2 Henv.
  destruct f as [a f1 f2 | f | a].
  + assert (IH1 : eval_sql_formula3 env1 (sql_formula_to_sql2 f1) = eval_sql_formula3 env2 f1).
    {
      apply (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
    }
    assert (IH2 : eval_sql_formula3 env1 (sql_formula_to_sql2 f2) = eval_sql_formula3 env2 f2).
    {
      apply (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
    }
    simpl; rewrite IH1, IH2; trivial.
  + assert (IH : eval_sql_formula3 env1 (sql_formula_to_sql2 f) = eval_sql_formula3 env2 f).
    {
      apply (proj1 (proj2 (proj2 IHn))); trivial; sql_size_tac.
    }
    simpl; rewrite IH; trivial.
  + simpl; apply (proj2 (proj2 (proj2 IHn))); trivial; sql_size_tac.
- intros a Hn env1 env2 Henv.
  destruct a as [ | p l | qtf p l sq | s sq | sq]; simpl.
  + apply refl_equal.
  + apply f_equal; rewrite <- map_eq; intros; apply interp_aggterm_eq; trivial.
  + assert (IH : eval_sql_query env1 (sql_query_to_sql2 sq) =BE= eval_sql_query env2 sq).
    {
      apply (proj1 IHn); trivial; sql_size_tac.
    }
    apply (interp_quant3_eq (OTuple T)).
    * rewrite Febag.nb_occ_equal in IH.
      intros x; rewrite eq_bool_iff; split; 
      intro Hx; apply Oeset.nb_occ_mem; rewrite <- Febag.nb_occ_elements;
      [rewrite <- IH | rewrite IH]; rewrite Febag.nb_occ_elements; apply Oeset.mem_nb_occ;
      trivial.
    * intros t1 t2 H Ht; apply f_equal.
      {
        apply f_equal2.
        - rewrite <- map_eq; intros a Ha; apply interp_aggterm_eq; trivial.
        - rewrite tuple_eq in Ht.
          rewrite <- (Fset.elements_spec1 _ _ _ (proj1 Ht)), <- map_eq.
          intros; apply (proj2 Ht).
      } 
  + assert (IH : eval_sql_query env1 (sql_query_to_sql2 sq) =BE= eval_sql_query env2 sq).
    {
      apply (proj1 IHn); trivial; sql_size_tac.
    }
    rewrite (Febag.mem_eq_2 _ _ _ IH); apply if_eq; trivial.
    apply Febag.mem_eq_1.
    apply mk_tuple_eq_2; intros a Ha.
    case (Oset.find (OAtt T) a
                    (map (fun x : select => match x with
                                              | Select_As e a0 => (a0, e)
                                            end) s)); [ | apply refl_equal].
    intro; apply interp_aggterm_eq; trivial.
  + apply if_eq; trivial.
    rewrite 2 Febag.is_empty_spec; apply Febag.equal_eq_1.
    apply (proj1 IHn); trivial; sql_size_tac.
Qed.

(*
Definition groups_of_env_slice (s : (setA * group_by * (list (tuple T)))) :=
  match s with
    | (_, Group_By g, _) => g
    | (sa, Group_Fine, _) => List.map (fun a => F_Dot _ _ a) ({{{sa}}})
  end.

Definition is_a_grouping_env (sa : setA) env f :=
  built_upon_fun 
    (T := T) 
    (map (fun a => F_Dot T symb a) ({{{sa}}}) ++ flat_map groups_of_env_slice env) f.

Fixpoint find_smallest_grouping_env (env : list (setA * group_by * list (tuple T))) f := 
  match env with
    | nil => if built_upon_fun (T := T) nil f 
             then Some nil
             else None
    | (sa1, _, _) :: env' => 
      match find_smallest_grouping_env env' f with
        | Some _ as e => e
        | None =>
          if is_a_grouping_env sa1 env' f
          then Some env
          else None
      end
  end.
  
Lemma find_smallest_grouping_env_unfold :
  forall env f, find_smallest_grouping_env env f =
  match env with
    | nil => if built_upon_fun (T := T) nil f 
             then Some nil
             else None
    | (sa1, _, _) :: env' => 
      match find_smallest_grouping_env env' f with
        | Some _ as e => e
        | None =>
          if is_a_grouping_env sa1 env' f
          then Some env
          else None
      end
  end.
Proof.
intros env f; case env; intros; apply refl_equal.
Qed.

Fixpoint interp_aggterm (env : list (setA * group_by * listT)) (ag : aggterm _ _ _) := 
  match ag with
  | A_Expr ft => interp_funterm env ft
  | A_agg ag ft => 
    let env' := 
        if Fset.is_empty (A T) (att_of_funterm ft)
        then Some env 
        else find_smallest_grouping_env env ft in
    let lenv := 
        match env' with 
          | None | Some nil => nil
          | Some ((sa1, lg1, lt1) :: env'') =>
            map (fun x => (sa1, lg1, x :: nil) :: env'') (quicksort (OTuple T) lt1)
        end in
        interp_aggregate ag (List.map (fun e => interp_funterm e ft) lenv)
  | A_fun f lag => interp_symb f (List.map (fun x => interp_aggterm env x) lag)
  end.

Lemma interp_aggterm_unfold :
  forall env ag, interp_aggterm env ag =
  match ag with
  | A_Expr ft => interp_funterm env ft
  | A_agg ag ft => 
    let env' := 
        if Fset.is_empty (A T) (att_of_funterm ft)
        then Some env 
        else find_smallest_grouping_env env ft in
    let lenv := 
        match env' with 
          | None | Some nil => nil
          | Some ((sa1, lg1, lt1) :: env'') =>
            map (fun x => (sa1, lg1, x :: nil) :: env'') (quicksort (OTuple T) lt1)
        end in
        interp_aggregate ag (List.map (fun e => interp_funterm e ft) lenv)
  | A_fun f lag => interp_symb f (List.map (fun x => interp_aggterm env x) lag)
  end.
Proof.
intros env ag; case ag; intros; apply refl_equal.
Qed.



Inductive tuple_or_group : Type :=
  | Tpl : tuple T -> tuple_or_group
  | Grp : list (tuple T) -> tuple_or_group.

(** * Homogeneous behavior for groups w.r.t the expressions in the having and in the select parts *)





Fixpoint built_upon_agg T g (xs : aggterm T symb aggregate) :=
  match xs with
    | A_Expr f => built_upon_fun g f
    | A_agg _ _ => true
    | A_fun _ l => forallb (built_upon_agg g) l
  end.

Lemma built_upon_fun_in_att_of_funterm :
  forall f g, built_upon_fun g f = true ->
              forall a, a inS att_of_funterm f -> 
                        a inS Fset.Union _ (map (att_of_funterm (T := T) (symb := symb)) g).
Proof.
intros f; set (n := size_funterm f); assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert f Hn; induction n as [ | n]; [intros f Hn; destruct f; inversion Hn | ].
intros f Hn g Hf a Ha.
destruct f as [c | a1 | f l]; rewrite att_of_funterm_unfold in Ha.
- rewrite Fset.empty_spec in Ha; discriminate Ha.
- rewrite Fset.singleton_spec, Oset.eq_bool_true_iff in Ha; subst a1.
  simpl in Hf; rewrite Oset.mem_bool_true_iff in Hf.
  rewrite Fset.mem_Union.
  eexists; split; [rewrite in_map_iff; eexists; split; [apply refl_equal | apply Hf] | ].
  rewrite att_of_funterm_unfold, Fset.singleton_spec, Oset.eq_bool_refl; trivial.
- simpl in Hf.
  rewrite Bool.orb_true_iff in Hf; destruct Hf as [Hf | Hf].
  + rewrite Oset.mem_bool_true_iff in Hf.
    rewrite Fset.mem_Union.
    eexists; split; [rewrite in_map_iff; eexists; split; [apply refl_equal | apply Hf] | ].
    rewrite att_of_funterm_unfold; apply Ha.
  + rewrite Fset.mem_Union in Ha.
    destruct Ha as [s [Hs Ha]]; rewrite in_map_iff in Hs.
    destruct Hs as [e [Hs He]]; subst s.
    apply (IHn e).
    * simpl in Hn; refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
      apply in_list_size; assumption.
    * rewrite forallb_forall in Hf; apply Hf; assumption.
    * assumption.
Qed.


Definition groups_of_abstract_env_slice (s : (setA * group_by)) :=
  match s with
    | (_, Group_By g) => g
    | (sa, Group_Fine) => List.map (fun a => F_Dot _ _ a) ({{{sa}}})
  end.




Definition is_a_grouping_abstract_env (sa : setA) env f :=
  built_upon_fun
    (T := T)
    (map (fun a => F_Dot T symb a) ({{{sa}}}) ++ flat_map groups_of_abstract_env_slice env) f.

Fixpoint find_smallest_grouping_abstract_env (env : list (setA * group_by)) f := 
  match env with
    | nil => if built_upon_fun (T := T) nil f 
             then Some nil
             else None
    | (sa1, _) :: env' => 
      match find_smallest_grouping_abstract_env env' f with
        | Some _ as e => e
        | None =>
          if is_a_grouping_abstract_env sa1 env' f
          then Some env
          else None
      end
  end.


(** * Well-sortedness *)


Ltac abstract_env_tac :=
  match goal with
    | |- equiv_abstract_env ?e ?e => apply equiv_abstract_env_refl

    | He : equiv_abstract_env ?e1 ?e2
      |- equiv_abstract_env ((?sa1,?g) :: ?e1) ((?sa1,?g) :: ?e2) =>
      simpl; repeat split; trivial;
      apply Fset.equal_refl

    | _ => trivial
  end.


Fixpoint equiv_envs (le1 le2 : list (list (setA * group_by * (list (tuple T))))) :=
  match le1, le2 with
    | nil, nil => True
    | e1 :: le1, e2 :: le2 => equiv_env e1 e2 /\ equiv_envs le1 le2
    | _, _ => False
  end.


Lemma equiv_env_app :
  forall e1 e2 e1' e2', equiv_env e1 e2 -> equiv_env e1' e2' -> equiv_env (e1 ++ e1') (e2 ++ e2').
Proof.
intro e1; induction e1 as [ | [[sa1 g1] l1] e1]; intros [ | [[sa2 g2] l2] e2] e1' e2' He He'.
- apply He'.
- contradiction He.
- contradiction He.
- simpl in He.
  destruct He as [H1 [H2 [H3 H4]]].
 repeat split; trivial.
 apply IHe1; trivial.
Qed.



Lemma interp_dot_eq :
  forall a env1 env2, equiv_env env1 env2 -> interp_dot env1 a = interp_dot env2 a.
Proof.
intros a env1; induction env1 as [ | [[sa1 gb1] x1] env1];
intros [ | [[sa2 gb2] x2] env2] He; try contradiction He; trivial.
simpl in He; simpl.
destruct He as [Hsa [Hg [Hx He]]].
assert (Kx := extract_from_groups Hx).
destruct (quicksort (OTuple T) x1) as [ | t1 l1];
destruct (quicksort (OTuple T) x2) as [ | t2 l2]; try discriminate; trivial.
- apply IHenv1; trivial.
- simpl in Kx.
  case_eq (Oeset.compare (OTuple T) t1 t2); intro Ht; rewrite Ht in Kx; try discriminate Kx.
  rewrite tuple_eq in Ht.
  rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 Ht)).
  case (a inS? support T t1).
  + apply (proj2 Ht).
  + apply IHenv1; trivial.
Qed.


Lemma find_smallest_grouping_env_eq :
  forall f env1 env2, 
    equiv_env env1 env2 -> 
    match (find_smallest_grouping_env env1 f), (find_smallest_grouping_env env2 f) with
        | None, None => True
        | Some e1, Some e2 => equiv_env e1 e2
        | _, _ => False
    end.
Proof.
intros f env1; induction env1 as [ | [[sa1 gb1] x1] env1];
intros [ | [[sa2 gb2] x2] env2] He; try contradiction He; trivial.
- case (find_smallest_grouping_env nil f); [ | intros; trivial].
  intro; apply equiv_env_refl.
- simpl in He; simpl.
  destruct He as [Hsa [Hg [Hx He]]].
  assert (H : is_a_grouping_env sa1 env1 f = is_a_grouping_env sa2 env2 f).
  {
    clear IHenv1.  
    unfold is_a_grouping_env.
    apply f_equal2; [ | apply refl_equal].
    apply f_equal2.
    - rewrite (Fset.elements_spec1 _ _ _ Hsa); trivial.
    - clear sa1 sa2 Hsa gb1 gb2 Hg x1 x2 Hx.
      revert env2 He.
      induction env1 as [ | [[sa1 gb1] x1] env1];
        intros [ | [[sa2 gb2] x2] env2] He; try contradiction He; trivial.
      simpl in He; simpl.
      destruct He as [Hsa [Hg [Hx He]]].
      apply f_equal2.
      + subst gb2; destruct gb1 as [gb1 | ]; [trivial | ].
        rewrite (Fset.elements_spec1 _ _ _ Hsa); trivial.
      + apply IHenv1; trivial.
  }
  rewrite <- H.
  generalize (IHenv1 _ He).
  case (find_smallest_grouping_env env1 f); case (find_smallest_grouping_env env2 f);
  try contradiction.
  + exact (fun _ _ h => h).
  + case (is_a_grouping_env sa1 env1 f).
    * intros _; simpl; repeat split; trivial.
    * exact (fun h => h).
Qed.

*)

(* Abstracting expressions *)
Fixpoint abstract_funterm lfun e :=
  match Oset.find (OFun E) e lfun with
    | Some a => F_Dot _ a
    | None =>
      match e with
        | F_Constant _ x => e
        | F_Dot _ _ => e
        | F_Expr f l => F_Expr f (map (abstract_funterm lfun) l)
      end
  end.
    
Fixpoint abstract_aggterm lfun lagg e :=
  match Oset.find (OAgg E) e lagg with
    | Some a => A_Expr (F_Dot _ a)
    | None => 
      match e with
        | A_Expr f => A_Expr (abstract_funterm lfun f)
        | A_agg a f => A_agg a (abstract_funterm lfun f)
        | A_fun f le => A_fun f (map (abstract_aggterm lfun lagg) le)
      end
  end.

Fixpoint abstract_sql_query lfun lagg sq :=
  match sq with
    | Sql_Table _ => sq
    | Sql_Set o sq1 sq2 => 
        Sql_Set o (abstract_sql_query lfun lagg sq1) (abstract_sql_query lfun lagg sq2)
    | Sql_Select s lsq f1 g f2 => 
      Sql_Select 
        (match s with
           | Select_Star => Select_Star
           | Select_List s =>
             Select_List (map (fun x => match x with 
                                          | Select_As e a => 
                                            Select_As (abstract_aggterm lfun lagg e) a 
                                        end) s)
         end)
        (map (fun x => match x with
                         | From_Item sq r => From_Item (abstract_sql_query lfun lagg sq) r
                       end) lsq)
        (abstract_sql_formula lfun lagg f1) 
        (match g with 
             | Group_Fine => Group_Fine
             | Group_By g => Group_By (map (abstract_funterm lfun) g)
         end)
        (abstract_sql_formula lfun lagg f2)
  end

with abstract_sql_formula lfun lagg f :=
  match f with
    | Sql_Conj a f1 f2 => 
        Sql_Conj a (abstract_sql_formula lfun lagg f1) (abstract_sql_formula lfun lagg f2)
    | Sql_Not f => Sql_Not (abstract_sql_formula lfun lagg f)
    | Sql_Atom a => Sql_Atom (abstract_sql_atom lfun lagg a)
  end   

with abstract_sql_atom lfun lagg a :=
  match a with
    | Sql_True => Sql_True
    | Sql_Pred p l => Sql_Pred p (map (fun e => abstract_aggterm lfun lagg e) l)
    | Sql_Quant qtf p l sq => 
        Sql_Quant 
          qtf p
          (map (fun e => abstract_aggterm lfun lagg e) l) 
          (abstract_sql_query lfun lagg sq)
    | Sql_In s sq => 
      Sql_In 
        (map (fun x => match x with 
                         | Select_As e a => Select_As (abstract_aggterm lfun lagg e) a 
                       end) s) (abstract_sql_query lfun lagg sq)
    | Sql_Exists sq => Sql_Exists (abstract_sql_query lfun lagg sq)
  end.

Lemma abstract_funterm_unfold :
  forall lfun e, abstract_funterm lfun e =
  match Oset.find (OFun E) e lfun with
    | Some a => F_Dot _ a
    | None =>
      match e with
        | F_Constant _ x => e
        | F_Dot _ _ => e
        | F_Expr f l => F_Expr f (map (abstract_funterm lfun) l)
      end
  end.
Proof.
intros lfun e; case e; intros; trivial.
Qed.
    
Lemma abstract_aggterm_unfold :
  forall lfun lagg e, abstract_aggterm lfun lagg e =
  match Oset.find (OAgg E) e lagg with
    | Some a => A_Expr (F_Dot _ a)
    | None => 
      match e with
        | A_Expr f => A_Expr (abstract_funterm lfun f)
        | A_agg a f => A_agg a (abstract_funterm lfun f)
        | A_fun f le => A_fun f (map (abstract_aggterm lfun lagg) le)
      end
  end.
Proof.
intros lfun lagg e; case e; intros; trivial.
Qed.

Lemma abstract_sql_query_unfold :
  forall lfun lagg sq,
    abstract_sql_query lfun lagg sq =
  match sq with
    | Sql_Table _ => sq
    | Sql_Set o sq1 sq2 => 
        Sql_Set o (abstract_sql_query lfun lagg sq1) (abstract_sql_query lfun lagg sq2)
    | Sql_Select s lsq f1 g f2 => 
      Sql_Select 
        (match s with
           | Select_Star => Select_Star
           | Select_List s =>
             Select_List (map (fun x => match x with 
                                          | Select_As e a => 
                                            Select_As (abstract_aggterm lfun lagg e) a 
                                        end) s)
         end)
        (map (fun x => match x with
                         | From_Item sq r => From_Item (abstract_sql_query lfun lagg sq) r
                       end) lsq)
        (abstract_sql_formula lfun lagg f1) 
        (match g with 
             | Group_Fine => Group_Fine
             | Group_By g => Group_By (map (abstract_funterm lfun) g)
         end)
        (abstract_sql_formula lfun lagg f2)
  end.
Proof.
intros lfun lagg sq; case sq; intros; trivial.
Qed.

Lemma abstract_sql_formula_unfold :
  forall lfun lagg f, abstract_sql_formula lfun lagg f =
  match f with
    | Sql_Conj a f1 f2 => 
        Sql_Conj a (abstract_sql_formula lfun lagg f1) (abstract_sql_formula lfun lagg f2)
    | Sql_Not f => Sql_Not (abstract_sql_formula lfun lagg f)
    | Sql_Atom a => Sql_Atom (abstract_sql_atom lfun lagg a)
  end.
Proof.
intros lfun lagg f; case f; intros; trivial.
Qed.

Lemma abstract_sql_atom_unfold :
  forall lfun lagg a, abstract_sql_atom lfun lagg a =
  match a with
    | Sql_True => Sql_True
    | Sql_Pred p l => Sql_Pred p (map (fun e => abstract_aggterm lfun lagg e) l)
    | Sql_Quant qtf p l sq => 
        Sql_Quant 
          qtf p
          (map (fun e => abstract_aggterm lfun lagg e) l) 
          (abstract_sql_query lfun lagg sq)
    | Sql_In s sq => 
      Sql_In 
        (map (fun x => match x with 
                         | Select_As e a => Select_As (abstract_aggterm lfun lagg e) a 
                       end) s) (abstract_sql_query lfun lagg sq)
    | Sql_Exists sq => Sql_Exists (abstract_sql_query lfun lagg sq)
  end.
Proof.
intros lfun lagg a; case a; intros; apply refl_equal.
Qed.

Fixpoint all_expressions_q (sq : sql_query) :=
  match sq with
    | Sql_Table x => Fset.empty _
    | Sql_Set _ sq1 sq2 => (all_expressions_q sq1) unionS (all_expressions_q sq2)
    | Sql_Select s lsq f1 g f2 => 
      Fset.mk_set _
        (map (fun x => match x with
                       | Select_As e _ => e
                       end) (match s with Select_List s => s | _ => nil end)) unionS 
        (Fset.Union _ (map (fun x => match x with
                                     | From_Item sq _ => all_expressions_q sq
                                   end)  lsq)) unionS 
        all_expressions_f f1 unionS 
        (match g with 
           | Group_Fine => Fset.empty _
           | Group_By g => Fset.mk_set _ (map (A_Expr (E := E)) g) end) unionS
        all_expressions_f f2
  end

with all_expressions_f (f : sql_formula) :=
  match f with
    | Sql_Conj _ f1 f2 => Fset.union (FAgg E) (all_expressions_f f1) (all_expressions_f f2)
    | Sql_Not f => all_expressions_f f
    | Sql_Atom a => all_expressions_a a
  end

with all_expressions_a (a : sql_atom) :=
  match a with
  | Sql_True => Fset.empty (FAgg E)
  | Sql_Pred _ l => Fset.mk_set _ l
  | Sql_Quant _ _ l sq => Fset.union _ (Fset.mk_set _ l) (all_expressions_q sq)
  | Sql_In s sq => 
    Fset.mk_set _ (map (fun x => match x with | Select_As e _ => e end) s) 
                unionS all_expressions_q sq
  | Sql_Exists sq => all_expressions_q sq
  end.

Lemma all_expressions_q_unfold :
  forall sq, all_expressions_q sq =
  match sq with
    | Sql_Table x => Fset.empty _
    | Sql_Set _ sq1 sq2 => (all_expressions_q sq1) unionS (all_expressions_q sq2)
    | Sql_Select s lsq f1 g f2 => 
      Fset.mk_set _
        (map (fun x => match x with
                       | Select_As e _ => e
                       end) (match s with Select_List s => s | _ => nil end)) unionS 
        (Fset.Union _ (map (fun x => match x with
                                     | From_Item sq _ => all_expressions_q sq
                                   end)  lsq)) unionS 
        all_expressions_f f1 unionS 
        (match g with 
           | Group_Fine => Fset.empty _
           | Group_By g => Fset.mk_set _ (map (A_Expr (E := E)) g) end) unionS
        all_expressions_f f2
  end.
Proof.
intros sq; case sq; intros; apply refl_equal.
Qed.

Lemma all_expressions_f_unfold :
  forall f, all_expressions_f f =
  match f with
    | Sql_Conj _ f1 f2 => Fset.union (FAgg E) (all_expressions_f f1) (all_expressions_f f2)
    | Sql_Not f => all_expressions_f f
    | Sql_Atom a => all_expressions_a a
  end.
Proof.
intro f; case f; intros; trivial.
Qed.

Lemma all_expressions_a_unfold :
  forall a, all_expressions_a a =
  match a with
  | Sql_True => Fset.empty (FAgg E)
  | Sql_Pred _ l => Fset.mk_set _ l
  | Sql_Quant _ _ l sq => Fset.union _ (Fset.mk_set _ l) (all_expressions_q sq)
  | Sql_In s sq => 
    Fset.mk_set _ (map (fun x => match x with | Select_As e _ => e end) s) 
                unionS all_expressions_q sq
  | Sql_Exists sq => all_expressions_q sq
  end.
Proof.
intro a; case a; intros; apply refl_equal.
Qed.

Definition funterm_is_homogeneous (h : attribute T -> bool)  (f : funterm E) :=
  forallb h ({{{att_of_funterm f}}}).

Definition aggterm_is_homogeneous (h : attribute T -> bool) (a : aggterm E) :=
  forallb h ({{{att_of_aggterm a}}}).

Fixpoint funterms_to_abstract (h : attribute T -> bool) fu :=
  match fu with
    | F_Constant _ _ => Fset.empty (FFun E)
    | F_Dot _ a => 
      if funterm_is_homogeneous h fu
      then Fset.singleton _ fu 
      else Fset.empty _
    | F_Expr f l => 
      if funterm_is_homogeneous h fu 
      then Fset.singleton _ fu
      else Fset.Union _ (map (funterms_to_abstract h) l)
  end.
    
Fixpoint agg_funterms_to_abstract_rec h a :=
  match a with
    | A_Expr f => funterms_to_abstract h f
    | A_agg _ f => if aggterm_is_homogeneous h a 
                   then Fset.empty (FFun E)
                   else funterms_to_abstract h f
    | A_fun _ la => Fset.Union _ (map (agg_funterms_to_abstract_rec h) la)
  end.

Fixpoint aggterms_to_abstract h (a : aggterm E) :=
    match a with
      | A_Expr f => Fset.empty _
      | A_agg _ f =>   
        if aggterm_is_homogeneous h a
        then Fset.singleton (FAgg E) a
        else Fset.empty _
      | A_fun _ la => Fset.Union _ (map (aggterms_to_abstract h) la)
    end.

Lemma funterm_is_homogeneous_unfold :
  forall h f, funterm_is_homogeneous h f = forallb h ({{{att_of_funterm f}}}).
Proof.
intros h f; trivial.
Qed.

Lemma aggterm_is_homogeneous_unfold :
  forall h a, aggterm_is_homogeneous h a = forallb h ({{{att_of_aggterm a}}}).
Proof.
intros h a; apply refl_equal.
Qed.

Lemma funterms_to_abstract_unfold :
  forall h fu, funterms_to_abstract h fu =
  match fu with
    | F_Constant _ _ => Fset.empty (FFun E)
    | F_Dot _ a => 
      if funterm_is_homogeneous h fu
      then Fset.singleton _ fu 
      else Fset.empty _
    | F_Expr f l => 
      if funterm_is_homogeneous h fu 
      then Fset.singleton _ fu
      else Fset.Union _ (map (funterms_to_abstract h) l)
  end.
Proof.
intros h fu; case fu; intros; trivial.
Qed.

Lemma agg_funterms_to_abstract_rec_unfold :
  forall h a, agg_funterms_to_abstract_rec h a =
  match a with
    | A_Expr f => funterms_to_abstract h f
    | A_agg _ f => if aggterm_is_homogeneous h a 
                   then Fset.empty (FFun E)
                   else funterms_to_abstract h f
    | A_fun _ la => Fset.Union _ (map (agg_funterms_to_abstract_rec h) la)
  end.
Proof.
intros h a; case a; intros; trivial.
Qed.

Lemma aggterms_to_abstract_unfold :
  forall h a, aggterms_to_abstract h a =
    match a with
      | A_Expr f => Fset.empty _
      | A_agg _ f =>   
        if aggterm_is_homogeneous h a
        then Fset.singleton (FAgg E) a
        else Fset.empty _
      | A_fun _ la => Fset.Union _ (map (aggterms_to_abstract h) la)
    end.
Proof.
intros h a; case a; intros; trivial.
Qed.

Definition fun_and_aggterms_to_abstract h l :=
  (Fset.Union (FFun E) (map (agg_funterms_to_abstract_rec h) l),
   Fset.Union _ (map (aggterms_to_abstract h) l)).

Definition fun_and_aggterms_to_abstract_f h f :=
  let exp_f := all_expressions_f f in
  fun_and_aggterms_to_abstract h ({{{exp_f}}}).

Lemma sql_sort_abstract_sql_query :
  forall lfun lagg sq, sql_sort (abstract_sql_query lfun lagg sq) =S= sql_sort sq.
Proof.
intros lfun lagg sq; set (n := tree_size (tree_of_sql_query sq)).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert sq Hn; induction n as [ | n]; [intros sq Hn; destruct sq; inversion Hn | ].
intros sq Hn; destruct sq as [r | o sq1 sq2 | s lsq f1 g f2]; simpl.
- apply Fset.equal_refl.  
- apply IHn; sql_size_tac.
- destruct s as [ | s].
  + rewrite map_map; apply Fset.Union_map_eq.
    intros [sq [ | r]] H; simpl; [ | apply Fset.equal_refl].
    apply IHn; sql_size_tac.
  + unfold select_as_as_pair; rewrite 3 map_map, Fset.equal_spec; intro a.
    do 2 apply f_equal; rewrite <- map_eq; intros x Hx; destruct x; trivial.
Qed.

End Sec.
