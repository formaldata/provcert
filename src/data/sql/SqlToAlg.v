(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

(** printing inS? $\in_?$ #∈<SUB>?</SUB># *)
(** printing inS $\in$ #∈# *)
(** printing subS? $\subseteq_?$ #⊆<SUB>?</SUB># *)
(** printing subS $\subseteq$ #⊆# *)
(** printing unionS $\cup$ #⋃# *)
(** printing interS $\cap$ #⋂# *)
(** printing inI $\in_I$ #∈<SUB><I>I</I></SUB># *)
(** printing theta $\theta$ #θ# *)
(** printing nu1 $\nu_1$ #ν<SUB><I>1</I></SUB># *)
(** printing nu $\nu$ #ν# *)
(** printing mu $\mu$ #μ# *)
(** printing sigma $\sigma$ #σ# *)
(** printing -> #⟶# *)
(** printing <-> #⟷# *)
(** printing => #⟹# *)
(** printing (emptysetS) $\emptyset$ #Ø# *)
(** printing emptysetS $\emptyset$ #Ø# *)
(** printing {{ $\{$ #{# *)
(** printing }} $\}$ #}# *)

Require Import Relations SetoidList List String Ascii Bool ZArith NArith.

Require Import Translation FlatData RelationalAlgebra 
        BasicFacts ListFacts ListPermut ListSort OrderedSet FiniteSet FiniteBag FiniteCollection 
        Tree Sql Term Formula.

Section Sec.

(** We assume given an SQL framework *)
Hypothesis predicate : Type.
Hypothesis symb : Type.
Hypothesis aggregate : Type.
Hypothesis T : Tuple.Rcd.
Hypothesis DBS : DatabaseSchema.Rcd (Tuple.A T).
Hypothesis OP : Oset.Rcd predicate.
Hypothesis OSymb : Oset.Rcd symb.
Hypothesis OAggregate : Oset.Rcd aggregate.
Hypothesis default_type : Tuple.type T.
Hypothesis default_att : Tuple.attribute T.

Import DatabaseSchema Tuple Expression EA.

(** The parser from SQL_COQ to extended queries. [n] is a "fuel", that is the intended maximum of recursive calls. It has to be greater than the size of the SQL query to be parsed in order to effectively obtaining a result *)

(** Here, we assume that the SQL query [sq] has been preprocessed by [normalize_sql_query] 
 in order to enjoy the following properties :
  - sql_query_has_no_star sq = true 
  - sql_query_has_no_group_by_and_having sq = true 
  - _well_formed_sql_query sq' = true 
*)

(*
(** We need an equality predicate in formulas, so we force to have one *) 
Inductive predicate : Type :=
  | Pred_Equal : predicate
  | Pred_User : pred -> predicate.

(* begin hide *)
Definition predicate_compare p1 p2 :=
  match p1, p2 with
    | Pred_Equal, Pred_Equal => Eq
    | Pred_Equal, Pred_User _ => Lt
    | Pred_User _, Pred_Equal => Gt
    | Pred_User p1, Pred_User p2 => Oset.compare OPred p1 p2
  end.
(* end hide *)
Definition OP : Oset.Rcd predicate.
split with predicate_compare.
- intros a1 a2.
  case a1; clear a1; [ | intro p1]; 
  (case a2; clear a2; [ | intro p2]); simpl;
  try (trivial || discriminate).
  generalize (Oset.eq_bool_ok OPred p1 p2).
  case (Oset.compare OPred p1 p2).
  + apply f_equal.
  + intros H1 H2; apply H1; injection H2; exact (fun h => h).
  + intros H1 H2; apply H1; injection H2; exact (fun h => h).
- intros a1 a2 a3.
  case a1; clear a1; [ | intro p1];
  (case a2; clear a2; [ | intro p2];
   (case a3; clear a3; [ | intro p3]));
  simpl; try (trivial || discriminate).
  apply Oset.compare_lt_trans.
- intros a1 a2.
  case a1; clear a1; [ | intro p1]; 
  (case a2; clear a2; [ | intro p2]); simpl; trivial.
  apply Oset.compare_lt_gt.
Defined.
*)

(** Since expressions built upon standard functions anf aggregates are translated into usual first-order terms built upon (variables and) a single type for symbols, we have to embed agregates and symbols into a single typle [function] *)
Inductive function : Type :=
| Symb : symb -> function
| Agg : aggregate -> function.

(* begin hide *)
Definition function_compare f1 f2 :=
  match f1, f2 with
    | Symb f1, Symb f2 => Oset.compare OSymb f1 f2
    | Symb _, Agg _ => Lt
    | Agg _, Symb _ => Gt
    | Agg a1, Agg a2 => Oset.compare OAggregate a1 a2
  end.
(* end hide *)
Definition OF : Oset.Rcd function.
split with function_compare.
- intros [f1 | a1] [f2 | a2]; try discriminate.
  + generalize (Oset.eq_bool_ok OSymb f1 f2); simpl.
    case (Oset.compare OSymb f1 f2).
    * apply f_equal.
    * intros H1 H2; apply H1.
      injection H2; exact (fun h => h).
    * intros H1 H2; apply H1.
      injection H2; exact (fun h => h).
  + generalize (Oset.eq_bool_ok OAggregate a1 a2); simpl.
    case (Oset.compare OAggregate a1 a2).
    * intro; subst a2; apply refl_equal.
    * intros H1 H2; apply H1.
      injection H2; exact (fun h => h).
    * intros H1 H2; apply H1.
      injection H2; exact (fun h => h).
- intros [f1 | a1] [f2 | a2] [f3 | a3]; simpl.
  + apply (Oset.compare_lt_trans OSymb f1 f2 f3). 
  + exact (fun _ h => h).
  + intros _ Abs; discriminate Abs.
  + exact (fun h _ => h).
  + exact (fun h _ => h).
  + intro Abs; discriminate Abs.
  + exact (fun _ h => h).
  + apply (Oset.compare_lt_trans OAggregate a1 a2 a3). 
- intros [f1 | a1] [f2 | a2]; simpl; trivial.
  + apply (Oset.compare_lt_gt OSymb f1 f2).
  + apply Oset.compare_lt_gt.
Defined.

(** The extended algebra built for hosting SQL *)
Definition ExtAlg := Data.mk_rcd T DBS OP OF OAggregate default_type.

(** Simple renaming *)
Definition rename_alg s (q : equery ExtAlg) :=
  match s, q with
    | Att_Ren_Star, _ => q
    | Att_Ren_List s, q => 
        rename_equery 
           (apply_renaming _ (List.map (att_as_as_pair (T := Data.T ExtAlg)) s))
           q
  end.

(** Embedding of expressions into usual first order terms *)
Fixpoint term_of_funterm x (ft : funterm T symb) : term (equery ExtAlg) (symbol T function) :=
   match ft with
     | F_Constant v => Term (Constant v) nil
     | F_Dot a => Term (Dot a) (x :: nil)
     | F_Expr f l => Term (Symbol (Symb f)) (List.map (term_of_funterm x) l)
   end.

Fixpoint term_of_aggterm x (t : aggterm T symb aggregate) : 
  term (equery ExtAlg) (symbol T function) :=
   match t with
     | A_Expr ft => term_of_funterm x ft
     | A_agg a ft => Term (Symbol (Agg a)) (term_of_funterm x ft :: nil)
     | A_fun f lag => Term (Symbol (Symb f)) (List.map (term_of_aggterm x) lag)
   end.

(** Coercions *)
Fixpoint funterm_of_funterm (ft : funterm T symb) : funterm T function :=
   match ft with
     | F_Constant v => F_Constant v
     | F_Dot a => F_Dot a
     | F_Expr f l => F_Expr (Symb f) (List.map funterm_of_funterm l)
   end.

Fixpoint aggterm_of_aggterm (a : aggterm T symb aggregate) : aggterm T function aggregate :=
  match a with
    | A_Expr ft => A_Expr (funterm_of_funterm ft)
    | A_agg a ft => A_agg a (funterm_of_funterm ft)
    | A_fun f lag => A_fun (Symb f) (List.map aggterm_of_aggterm lag)
  end.

Lemma term_of_funterm_unfold :
  forall x ft, term_of_funterm x ft =
   match ft with
     | F_Constant v => Term (Constant v) nil
     | F_Dot a => Term (Dot a) (x :: nil)
     | F_Expr f l => Term (Symbol (Symb f)) (List.map (term_of_funterm x) l)
   end.
Proof.
intros x ft; case ft; intros; apply refl_equal.
Qed.

Lemma term_of_aggterm_unfold :
  forall x t, term_of_aggterm x t =
   match t with
     | A_Expr ft => term_of_funterm x ft
     | A_agg a ft => Term (Symbol (Agg a)) (term_of_funterm x ft :: nil)
     | A_fun f lag => Term (Symbol (Symb f)) (List.map (term_of_aggterm x) lag)
   end.
intros x t; case t; intros; apply refl_equal.
Qed.

Fixpoint build_conjunction_rec q q1 (l : list (predicate * _ * _)) f0 := 
  match l with
    | nil => f0
    | (p, l, l1) :: _l => 
      build_conjunction_rec 
        q q1 _l 
        (Conj And_F 
              (Atom p
                    ((map (term_of_aggterm (Var (Vrbl q 0))) l) ++ 
                         map (term_of_aggterm (Var (Vrbl q1 1))) l1)) f0)
  end.

Definition build_conjunction q q1 l :=
  match l with
    | nil => TTrue
    | (p, l, l1) :: _l => 
      build_conjunction_rec 
        q q1 _l
        (Atom p 
              ((map (term_of_aggterm (Var (Vrbl q 0))) l) ++ 
                 map (term_of_aggterm (Var (Vrbl q1 1))) l1))
  end.

Fixpoint sql_to_alg (sq : _sql_query predicate symb aggregate T DBS) : equery ExtAlg :=
  match sq with
    | _Sql_Table tbl => Equery_Basename ExtAlg tbl
    | _Sql_Set o sq1 sq2 => Equery_Set o FlagBag (sql_to_alg sq1) (sql_to_alg sq2)
    | _Sql_Select (Select_List s) lsq Group_Fine f => 
      let sql_to_alg_star_true :=
          (fix sql_to_alg_star_true lsq := 
             @N_ary_Equery_NaturalJoin 
               ExtAlg FlagBag
               (map 
                  (fun x => 
                     match x with
                       | _From_Item sqj rhoj => rename_alg rhoj (sql_to_alg sqj)
                     end)
                  lsq)) in
      let q1 := sql_to_alg_star_true lsq in
      let f' := sql_formula_to_alg q1 f in
      Equery_Omega
        (Fine ExtAlg)
        TTrue
        (List.map 
           (fun x =>
              match x with
                | Select_As e a => Code ExtAlg a (aggterm_of_aggterm e)
              end) s)
        (Equery_Omega
           (Fine ExtAlg)
           f'
           (List.map (fun a => Code ExtAlg a (A_Expr (F_Dot a))) ({{{sort q1}}}))
           q1)

    | _Sql_Select (Select_List s) lsq (Group_By g) (_Sql_Atom _Sql_True) =>
      let sql_to_alg_star_true :=
          (fix sql_to_alg_star_true lsq := 
             @N_ary_Equery_NaturalJoin 
               ExtAlg FlagBag
               (map 
                  (fun x => 
                     match x with
                       | _From_Item sqj rhoj => rename_alg rhoj (sql_to_alg sqj)
                     end)
                  lsq)) in
      let q1 := sql_to_alg_star_true lsq in
      let g' := Partition ExtAlg (List.map funterm_of_funterm g) in
      Equery_Omega
        g'
        TTrue
        (List.map 
           (fun x =>
              match x with
                | Select_As e a => Code ExtAlg a (aggterm_of_aggterm e)
              end) s) q1
    | _ => Equery_Empty _ FlagBag nil
  end

with sql_formula_to_alg q f :=
   match f with
     | _Sql_Conj o f1 f2 => Conj o (sql_formula_to_alg q f1) (sql_formula_to_alg q f2)
     | _Sql_Not f => Not (sql_formula_to_alg q f)
     | _Sql_Atom a =>
       match a with
         | _Sql_True => @TTrue _ _ _
         | _Sql_Pred p l => 
           let xq := Var (Vrbl q 0) in 
           (Atom p (List.map (term_of_aggterm xq) l))
         | _Sql_Quant qtf l sq1 =>
           let q1 := sql_to_alg sq1 in 
           Quant qtf (Vrbl q1 1) (build_conjunction q q1 l)
       end
   end.

Definition sql_to_alg_star_true :=
  (fix sql_to_alg_star_true lsq := 
     @N_ary_Equery_NaturalJoin 
       ExtAlg FlagBag
       (map 
          (fun x => 
             match x with
               | _From_Item sqj rhoj => rename_alg rhoj (sql_to_alg sqj)
             end)
          lsq)).

Lemma sql_to_alg_unfold :
  forall sq, sql_to_alg sq =
  match sq with
    | _Sql_Table tbl => Equery_Basename ExtAlg tbl
    | _Sql_Set o sq1 sq2 => Equery_Set o FlagBag (sql_to_alg sq1) (sql_to_alg sq2)
    | _Sql_Select (Select_List s) lsq Group_Fine f => 
      let q1 := sql_to_alg_star_true lsq in
      let f' := sql_formula_to_alg q1 f in
      Equery_Omega
        (Fine ExtAlg)
        TTrue
        (List.map 
           (fun x =>
              match x with
                | Select_As e a => Code ExtAlg a (aggterm_of_aggterm e)
              end) s)
        (Equery_Omega
           (Fine ExtAlg)
           f'
           (List.map (fun a => Code ExtAlg a (A_Expr (F_Dot a))) ({{{sort q1}}}))
           q1)

    | _Sql_Select (Select_List s) lsq (Group_By g) (_Sql_Atom _Sql_True) =>
      let q1 := sql_to_alg_star_true lsq in
      let g' := Partition ExtAlg (List.map funterm_of_funterm g) in
      Equery_Omega
        g'
        TTrue
        (List.map 
           (fun x =>
              match x with
                | Select_As e a => Code ExtAlg a (aggterm_of_aggterm e)
              end) s) q1
    | _ => Equery_Empty _ FlagBag nil
  end.
Proof.
intro sq; case sq; intros; apply refl_equal.
Qed.

Lemma sql_formula_to_alg_unfold :
  forall q f,  sql_formula_to_alg q f =
   match f with
     | _Sql_Conj o f1 f2 => Conj o (sql_formula_to_alg q f1) (sql_formula_to_alg q f2)
     | _Sql_Not f => Not (sql_formula_to_alg q f)
     | _Sql_Atom a =>
       match a with
         | _Sql_True => @TTrue _ _ _
         | _Sql_Pred p l => 
           let xq := Var (Vrbl q 0) in 
           (Atom p (List.map (term_of_aggterm xq) l))
         | _Sql_Quant qtf l sq1 =>
           let q1 := sql_to_alg sq1 in 
           Quant qtf (Vrbl q1 1) (build_conjunction q q1 l)
       end
   end.
Proof.
intros q f; case f; intros; apply refl_equal.
Qed.

Lemma sql_to_alg_star_true_unfold :
  forall lsq, sql_to_alg_star_true lsq = 
              @N_ary_Equery_NaturalJoin 
                ExtAlg FlagBag
                (map 
                   (fun x => 
                      match x with
                        | _From_Item sqj rhoj => rename_alg rhoj (sql_to_alg sqj)
                      end)
                   lsq).
Proof.
intro lsq; induction lsq as [ | [sq1 r1] lsq]; trivial.
Qed.

(** * Interpretation of SQL queries and their translations *)
Hypothesis ip : predicate -> list (value T) -> bool.
Hypothesis is : symb -> list (value T) -> value T.
Hypothesis ia : aggregate -> list (value (Data.T ExtAlg)) -> value (Data.T ExtAlg).
Hypothesis I : relname DBS -> Febag.bag (Fecol.CBag (CTuple T)).
Hypothesis WI : well_sorted_sql_table T DBS I.

Definition I' := (fun sq => Fecol.Fbag (I sq)).

Lemma WI' : well_sorted_instance T DBS I'.
Proof.
unfold well_sorted_instance, I'.
intros r t Ht; apply WI.
apply Ht.
Qed.

Definition is' f :=
  match f with
    | Symb f => is f
    | Agg a => ia a
  end.
    
Lemma sort_sql_to_alg : 
  forall sq,           
    _well_formed_sql_query OP OSymb OAggregate sq = true ->
    sql_query_has_no_star sq = true ->
    sql_query_has_no_group_by_and_having sq = true -> 
    sort (sql_to_alg sq) =S= _sql_sort sq.
Proof.
intro sq; set (n := tree_size (_tree_of_sql_query sq)).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert sq Hn; induction n as [ | n]; [intros sq Hn; destruct sq; inversion Hn | ].
intros sq Hn W Wstar Wgby ; destruct sq as [r | o sq1 sq2 | s lsq g f2].
- apply Fset.equal_refl.  
- simpl; rewrite _sql_sort_unfold; apply IHn.
  + rewrite _tree_of_sql_query_unfold in Hn; simpl in Hn.
    refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply le_plus_l.
  + rewrite _well_formed_sql_query_unfold, 3 Bool.andb_true_iff in W.
    apply (proj2 (proj1 (proj1 W))).
  + rewrite sql_query_has_no_star_unfold, Bool.andb_true_iff in Wstar.
    apply (proj1 Wstar).
  + rewrite sql_query_has_no_group_by_and_having_unfold, Bool.andb_true_iff in Wgby.
    apply (proj1 Wgby).
- rewrite sql_to_alg_unfold; destruct s as [ | a]; [discriminate Wstar | ].
  destruct g as [g | ].
  + destruct f2 as [o f1 f2 | f2 | atm]; try discriminate Wgby.
    destruct atm; try discriminate Wgby.
    cbv beta iota zeta; rewrite sort_unfold, _sql_sort_unfold, 2 map_map.
    rewrite Fset.equal_spec; intro; do 2 apply f_equal.
    rewrite <- map_eq; intros x _; destruct x; trivial.
  + cbv beta iota zeta; rewrite sort_unfold, _sql_sort_unfold, 2 map_map.
    rewrite Fset.equal_spec; intro; do 2 apply f_equal.
    rewrite <- map_eq; intros x _; destruct x; trivial.
Qed.

Lemma sort_sql_item_to_alg : 
  forall sq r,           
    _well_formed_from_item OP OSymb OAggregate (_From_Item sq r) = true ->
    sql_item_has_no_star (_From_Item sq r) = true ->
    sql_item_has_no_group_by_and_having (_From_Item sq r) = true -> 
    sort (rename_alg r (sql_to_alg sq)) =S= _from_item_sort (_From_Item sq r).
Proof.
intros sq [ | r] W1 W2 W3; [apply sort_sql_to_alg; trivial | ].
rewrite _from_item_sort_unfold; simpl.
rewrite _well_formed_from_item_unfold, 3 Bool.andb_true_iff in W1.
destruct W1 as [[[W11 W12] W13] W14].
unfold att_as_as_pair, apply_renaming, rho_renaming.
rewrite 2 map_map.
rewrite (Fset.elements_spec1 _ _ _ (sort_sql_to_alg sq W14 W2 W3)).
simpl Data.T in *; rewrite  <- (Fset.elements_spec1 _ _ _ W12).
unfold att_as_as_pair; rewrite map_map.
rewrite Fset.equal_spec; intro a; 
rewrite eq_bool_iff, 2 Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff; split.
- intros [_a [_Ha Ha]].
  generalize (Fset.in_elements_mem _ _ _ Ha); clear Ha; intro Ha.
  rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff in Ha.
  destruct Ha as [[a1 a2] [__Ha Ha]]; simpl in __Ha; subst a1.
  case_eq (Oset.find (OAtt T) _a
            (map
               (fun x : att_renaming T =>
                match x with
                | Att_As e a => (e, a)
                end) r)).
  + intros b Hb; rewrite Hb in _Ha; subst b.
    rewrite Oset.mem_bool_true_iff, in_map_iff.
    exists (Att_As _a a); split; [trivial | ].
    generalize (Oset.find_some _ _ _ Hb); clear Hb; intro Hb; rewrite in_map_iff in Hb.
    destruct Hb as [[a1 _a2] [_Hb Hb]]; simpl in _Hb; injection _Hb; clear _Hb.
    do 2 intro; subst _a2 a1; apply Hb.
  + intro Abs; apply False_rec.
    apply (Oset.find_none _ _ _ Abs a2).
    rewrite in_map_iff; eexists; split; [ | apply Ha]; trivial.
- rewrite Oset.mem_bool_true_iff, in_map_iff.
  intros [[b _a] [_Ha Ha]]; simpl in _Ha; subst _a.
  exists b; split.
  + case_eq (Oset.find (OAtt T) b
                       (map
                          (fun x : att_renaming T =>
                             match x with
                               | Att_As e a0 => (e, a0)
                             end) r)). 
    * intros c Hc.
      assert (Kc := Oset.find_some _ _ _ Hc).
      apply (Oset.all_diff_bool_fst _ _ W11) with b; [apply Kc | ].
      rewrite in_map_iff; eexists; split; [ | apply Ha]; trivial.
    * intro Abs; apply False_rec; apply (Oset.find_none _ _ _ Abs a).
      rewrite in_map_iff; eexists; split; [ | apply Ha]; trivial.
  + apply Fset.mem_in_elements.
    rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff.
    eexists; split; [ | apply Ha]; trivial.
Qed.

Lemma flag_sql_to_alg_star_true :
  forall lsq, Fecol.flag 
                (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg_star_true lsq)) = FlagBag.
Proof.
intro lsq.
rewrite sql_to_alg_star_true_unfold.
set (l := (map
             (fun
                 x : _from_item predicate symb aggregate (Data.T ExtAlg) DBS =>
                 match x with
                   | _From_Item sqj rhoj => rename_alg rhoj (sql_to_alg sqj)
                 end) lsq)) in *.
induction l as [ | x1 l]; trivial.
Qed.

Lemma flag_sql_to_alg :
  forall sq q, 
    sql_to_alg sq = q -> Fecol.flag (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q) = FlagBag.
Proof.
intros sq q; set (n := tree_size (_tree_of_sql_query sq)).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert sq q Hn; induction n as [ | n]; [intros sq q Hn; destruct sq; inversion Hn | ].
intros sq q Hn H; destruct sq as [r | o sq1 sq2 | s lsq g f].
- subst q; apply refl_equal.
- subst q; simpl.
  case_eq (sort (sql_to_alg sq1) =S?= sort (sql_to_alg sq2)); intro Hs; simpl in Hs; rewrite Hs.
  + destruct o; apply refl_equal.
  + assert (IH := IHn sq1 (sql_to_alg sq1)); simpl in *; rewrite IH; trivial.
    refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
    apply le_plus_l.
- rewrite sql_to_alg_unfold in H; subst q.
  destruct s as [ | s]; [apply refl_equal | ].
  assert (IH := flag_sql_to_alg_star_true lsq).
  destruct g as [g | ].
  + destruct f as [ | | atm]; try apply refl_equal.
    destruct atm; try apply refl_equal.
    cbv beta iota zeta.
    rewrite eval_equery_unfold; cbv beta iota zeta.
    rewrite IH; apply refl_equal.
  + cbv beta iota zeta; rewrite eval_equery_unfold; cbv beta iota zeta.
    rewrite eval_equery_unfold; cbv beta iota zeta.
    rewrite IH; apply refl_equal.
Qed.

(** interpretations of coercions *)
Lemma interp_funterm_funterm_of_funterm :
  forall t e, interp_funterm is' (funterm_of_funterm e) t =
         interp_funterm is e t.
Proof.
intros t f.
set (n := size_funterm f).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert f Hn; induction n as [ | n];
intros f Hn; [destruct f; inversion Hn | ].
destruct f as [v | b | f l].
- apply refl_equal.
- apply refl_equal.
- simpl; rewrite map_map.
  apply f_equal.
  rewrite <- map_eq.
  intros a Ha; apply IHn.
  refine (le_trans _ _ _ _ (Le.le_S_n _ _  Hn)).
  apply in_list_size; assumption.
Qed.

Lemma interp_aggterm_aggterm_of_aggterm :
  forall s e, interp_aggterm is' ia (aggterm_of_aggterm e) s =
         interp_aggterm is ia e s.
Proof.
intros s e.
set (n := size_aggterm e).
assert (Hn := le_n n); unfold n at 1 in Hn; clearbody n.
revert e Hn; induction n as [ | n];
intros e Hn; [destruct e; inversion Hn | ].
destruct e as [f | e f | f l]; simpl.
- destruct s as [ | t1 s]; [destruct f; apply refl_equal | ].
  apply interp_funterm_funterm_of_funterm.
- apply f_equal.
  rewrite <- map_eq; intros a Ha.
  apply interp_funterm_funterm_of_funterm.
- apply f_equal.
  rewrite map_map, <- map_eq.
  intros a Ha.
  apply IHn.
  refine (le_trans _ _ _ _ (Le.le_S_n _ _  Hn)).
  apply in_list_size; assumption.
Qed.

Lemma eval_codes_on_grp_projection_grp :
  forall s l, eval_codes_on_grp
           (ExtAlg := ExtAlg) is' ia
           (map
              (fun x0 : select symb aggregate T =>
                 match x0 with
                   | Select_As e a => Code ExtAlg a (aggterm_of_aggterm e)
                 end) l) s =t= 
         projection is ia (Select_List l) (Grp T s).
Proof.
intros s l; unfold eval_codes_on_grp, projection.
set (la := (map
             (fun x : select symb aggregate T =>
              match x with
              | Select_As e a0 => (a0, e)
              end) l)) in *.
assert (Hl : l = map (fun x => match x with (a, e) => Select_As e a end) la).
{
  subst la; rewrite map_map, map_id; [apply refl_equal | ].
  intros [e a] _; apply refl_equal.
}
clearbody la; subst l; rewrite 5 map_map.
apply mk_tuple_eq.
- rewrite Fset.equal_spec; intro a.
  do 2 apply f_equal; rewrite <- map_eq; intros x _; destruct x; apply refl_equal.
- intros a _ _; induction la as [ | [a1 e1] la]; simpl.
  + unfold dot, default_tuple.
    rewrite (Fset.mem_eq_2 _ _ _ (support_mk_tuple _ _ _)), Fset.empty_spec; trivial.
  + case (Oset.eq_bool (OAtt T) a a1);
      [rewrite interp_aggterm_aggterm_of_aggterm; apply refl_equal | ].
      apply IHla.
Qed.

Lemma interp_funterm_interp_term :
  forall f t t' x, 
    t =t= t' -> 
   interp_term (ExtAlg := ExtAlg) is' (fun _ : variable (equery ExtAlg) => t)
     (term_of_funterm (Var x) f) =
   interp_funterm is f t'.
Proof.
intro f.
set (n := size_funterm f).
assert (Hn := le_n n).
unfold n at 1 in Hn; clearbody n.
revert f Hn.
induction n as [ | n];
intros f Hn t t' x Ht;
[ destruct f; inversion Hn | ].
destruct f as [c | a | f l]; simpl.
- apply refl_equal.
- apply (tuple_eq_dot T); assumption.
- apply f_equal.
  rewrite map_map, <- map_eq.
  intros a Ha; apply IHn; [ | assumption].
  simpl in Hn.
  refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
  apply in_list_size; assumption.
Qed.

Lemma interp_aggterm_interp_term :
  forall a t t' x, 
    t =t= t' -> 
   interp_term (ExtAlg := ExtAlg) is' (fun _ : variable (equery ExtAlg) => t)
     (term_of_aggterm (Var x) a) =
   interp_aggterm is ia a (t' :: nil).
Proof.
intro a.
set (n := size_aggterm a).
assert (Hn := le_n n).
unfold n at 1 in Hn; clearbody n.
revert a Hn.
induction n as [ | n];
intros a Hn t t' x Ht;
[ destruct a; inversion Hn | ].
destruct a as [f | a d f | s l]; simpl.
- apply (interp_funterm_interp_term f _ _ _ Ht).
- apply f_equal; apply f_equal2; [ | apply refl_equal].
  apply interp_funterm_interp_term; assumption.
- rewrite map_map.
  apply f_equal.
  rewrite <- map_eq.
  intros a Ha; apply IHn; [ | assumption].
  simpl in Hn.
  refine (Le.le_trans _ _ _ _ (le_S_n _ _ Hn)).
  apply in_list_size; assumption.
Qed.

(** [build_conjunction] behaves as expected *)
Lemma build_conjunction_rec_true_iff :
  forall q q1 ll f t x,
    interp_formula
      (ExtAlg := ExtAlg) ip is'
      (fun q0 : equery ExtAlg =>
         Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q0))
      (ivar_xt (OQ ExtAlg) OP (OSymbol ExtAlg)
               (fun _ : variable (equery ExtAlg) => t) (Vrbl q1 1) x)
      (build_conjunction_rec q q1 ll f) = true <->
    (interp_formula
      (ExtAlg := ExtAlg) ip is'
      (fun q0 : equery ExtAlg =>
         Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q0))
      (ivar_xt (OQ ExtAlg) OP (OSymbol ExtAlg)
               (fun _ : variable (equery ExtAlg) => t) (Vrbl q1 1) x)
      f = true /\
     (forall p l l1,
       In (p, l, l1) ll ->
       interp_formula
         (ExtAlg := ExtAlg) ip is'
         (fun q0 : equery ExtAlg =>
            Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q0))
         (ivar_xt (OQ ExtAlg) OP (OSymbol ExtAlg)
               (fun _ : variable (equery ExtAlg) => t) (Vrbl q1 1) x)
         (Atom p
               ((map (term_of_aggterm (Var (Vrbl q 0))) l)
                  ++ map (term_of_aggterm (Var (Vrbl q1 1))) l1)) = true)).
Proof.
intros q q1 ll; induction ll as [ | [[p l] l1] ll]; intros f t x; split.
- intro H; split; [apply H | ].
  intros p l l1 H1; contradiction H1.
- intros [H1 H2]; apply H1.
- intro H; simpl in H; rewrite IHll in H; destruct H as [H1 H2].
  simpl in H1; rewrite Bool.andb_true_iff in H1; destruct H1 as [H11 H12].
  split; [apply H12 | ].
  intros p' k k1 H; simpl in H; destruct H as [H | H].
  + injection H; clear H; do 3 intro; subst; apply H11.
  + apply H2; trivial.
- intros [H1 H2]; simpl; rewrite IHll; split.
  + simpl; rewrite Bool.andb_true_iff; split; [ | apply H1].
    apply H2; left; trivial.
  + intros; apply H2; right; trivial.
Qed.


Lemma build_conjunction_true_iff :
  forall q q1 ll t x,
    interp_formula
      (ExtAlg := ExtAlg) ip is'
      (fun q0 : equery ExtAlg =>
         Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q0))
      (ivar_xt (OQ ExtAlg) OP (OSymbol ExtAlg)
               (fun _ : variable (equery ExtAlg) => t) (Vrbl q1 1) x)
      (build_conjunction q q1 ll) = true <->
    (forall p l l1,
       In (p, l, l1) ll ->
       interp_formula
         (ExtAlg := ExtAlg) ip is'
         (fun q0 : equery ExtAlg =>
            Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q0))
         (ivar_xt (OQ ExtAlg) OP (OSymbol ExtAlg)
               (fun _ : variable (equery ExtAlg) => t) (Vrbl q1 1) x)
         (Atom p
               ((map (term_of_aggterm (Var (Vrbl q 0))) l)
                  ++ map (term_of_aggterm (Var (Vrbl q1 1))) l1)) = true).
Proof.
intros q q1 ll t x.
unfold build_conjunction.
destruct ll as [ | [[p l] l1] ll]; split.
- intros H p l l1 K; contradiction K.
- intros _; apply refl_equal.
- intro H; rewrite build_conjunction_rec_true_iff in  H.
  intros p' k k1 K; simpl in K; destruct K as [K | K].
  + injection K; do 3 intro; subst; apply (proj1 H).
  + apply (proj2 H); assumption.
- intro H.
  rewrite build_conjunction_rec_true_iff; split.
  + apply H; left; trivial.
  + intros; apply H; right; trivial.
Qed.

Lemma sql_to_alg_is_sound_etc :
  forall n, 
  (forall sq, tree_size (_tree_of_sql_query sq) <= n ->
           _well_formed_sql_query OP OSymb OAggregate sq = true ->
           sql_query_has_no_star sq = true ->
           sql_query_has_no_group_by_and_having sq = true -> 
           forall q, sql_to_alg sq = q -> 
           forall t, Fecol.nb_occ t (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q) =
                     Febag.nb_occ _ t (_eval_sql_query ip is ia I sq)) /\
  (forall sq r, tree_size (_tree_of_from_item (_From_Item sq r)) <= n ->
           _well_formed_from_item OP OSymb OAggregate (_From_Item sq r) = true ->
           sql_item_has_no_star (_From_Item sq r) = true ->
           sql_item_has_no_group_by_and_having (_From_Item sq r) = true -> 
           forall y,  rename_alg r (sql_to_alg sq) = y -> 
           forall t, Fecol.nb_occ t (eval_equery (ExtAlg := ExtAlg) ip is' ia I' y) =
                     Febag.nb_occ _ t (_eval_from_item ip is ia I (_From_Item sq r))) /\
  (forall sa f, tree_size (_tree_of_sql_formula f) <= n ->
             _well_formed_sql_formula OP OSymb OAggregate sa f = true ->
             sql_formula_has_no_star f = true ->
             sql_formula_has_no_group_by_and_having f = true -> 
             forall q f', 
               sql_formula_to_alg q f = f' -> 
               let interp_dom := 
                   fun q => Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q) in
               forall t, EA.interp_formula (ExtAlg := ExtAlg) ip is' interp_dom (fun _ => t) f' =
              _eval_sql_formula ip is ia I f (Tpl _ t)).
Proof.
intro n; induction n as [ | n]; repeat split.
- intros sq Hn; destruct sq; inversion Hn.
- intros sq r Hn; inversion Hn.
- intros sa f Hn; destruct f; inversion Hn.
- intros sq Hn W Wstar Wgby q Hq t.
  destruct sq as [r | o sq1 sq2 | s lsq g f].
  + simpl in Hq; subst q; unfold I'.
    rewrite eval_equery_unfold, _eval_sql_query_unfold, Fecol.nb_occ_bag; apply refl_equal.
  + rewrite sql_to_alg_unfold in Hq; subst q.
    rewrite _well_formed_sql_query_unfold, 3 Bool.andb_true_iff in W.
    destruct W as [[[W1 W2] W3] W4].
    rewrite sql_query_has_no_star_unfold, Bool.andb_true_iff in Wstar.
    rewrite sql_query_has_no_group_by_and_having_unfold, Bool.andb_true_iff in Wgby.
    rewrite eval_equery_unfold, _eval_sql_query_unfold.
    rewrite (Fset.equal_eq_1 _ _ _ _ (sort_sql_to_alg _ W2 (proj1 Wstar) (proj1 Wgby))).
    rewrite (Fset.equal_eq_2 _ _ _ _ (sort_sql_to_alg _ W3 (proj2 Wstar) (proj2 Wgby))).
    simpl Data.T in *; rewrite W1.
    assert (IH1 : Fecol.nb_occ t (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg sq1)) =
                    Febag.nb_occ _ t (_eval_sql_query ip is ia I sq1)).
    {
      apply (proj1 IHn).
      - rewrite _tree_of_sql_query_unfold in Hn; simpl in Hn.
        refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
        apply le_plus_l.
      - assumption.
      - apply (proj1 Wstar).
      - apply (proj1 Wgby).
      - apply refl_equal.
    }
    assert (IH2 : Fecol.nb_occ t (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg sq2)) =
                  Febag.nb_occ _ t (_eval_sql_query ip is ia I sq2)).
    {
      apply (proj1 IHn).
      - rewrite _tree_of_sql_query_unfold in Hn; simpl in Hn.
        refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
        refine (le_trans _ _ _ _ (le_plus_r _ _)).
        apply le_plus_l.
      - assumption.
      - apply (proj2 Wstar).
      - apply (proj2 Wgby).
      - apply refl_equal.
    }
    destruct o; simpl.
    * rewrite Febag.nb_occ_union, <- IH1, <- IH2.
      rewrite Fecol.nb_occ_bag; simpl; rewrite Febag.nb_occ_union; apply f_equal2;
      rewrite Fecol.nb_occ_bag; trivial.
    * rewrite Febag.nb_occ_union_max, <- IH1, <- IH2.
      rewrite Fecol.nb_occ_bag; simpl; rewrite Febag.nb_occ_union_max; apply f_equal2;
      rewrite Fecol.nb_occ_bag; trivial.
    * rewrite Febag.nb_occ_inter, <- IH1, <- IH2.
      rewrite Fecol.nb_occ_bag; simpl; rewrite Febag.nb_occ_inter; apply f_equal2;
      rewrite Fecol.nb_occ_bag; trivial.
    * rewrite Febag.nb_occ_diff, <- IH1, <- IH2.
      rewrite Fecol.nb_occ_bag; simpl; rewrite Febag.nb_occ_diff; apply f_equal2;
      rewrite Fecol.nb_occ_bag; trivial.
  + destruct s as [ | s]; [discriminate Wstar | ].
    rewrite _well_formed_sql_query_unfold, 9 Bool.andb_true_iff in W.
    destruct W as [[[[[[[[[W1 W2] W3] W4] W5] W6] W7] W8] W9] W10].
    rewrite sql_query_has_no_star_unfold, Bool.andb_true_l, andb_true_iff in Wstar.
    destruct Wstar as [W11 W12].
    rewrite sql_query_has_no_group_by_and_having_unfold, 2 Bool.andb_true_iff in Wgby.
    destruct Wgby as [[W13 W14] W15].
    rewrite sql_to_alg_unfold in Hq.
    assert (Hs : forall sq r, In (_From_Item sq r) lsq ->
                              _from_item_sort (_From_Item sq r) =S=
                              sort (rename_alg r (sql_to_alg sq))).
    {
      intros sq r H.
      rewrite forallb_forall in W3, W11, W14.
      - rewrite (Fset.equal_eq_2 _ _ _ _ (sort_sql_item_to_alg _ _ (W3 _ H) (W11 _ H) (W14 _ H))).
        apply Fset.equal_refl.
    }
    assert (IH : forall x,
                   Fecol.nb_occ 
                     x (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg_star_true lsq)) =
                   Febag.nb_occ (Fecol.CBag (CTuple T)) x
                                (N_product_bag T (map (_eval_from_item ip is ia I) lsq))).
    {
      intro x; rewrite sql_to_alg_star_true_unfold.
      assert (H : let lq := (map
                               (fun x0  =>
                                  match x0 with
                                    | _From_Item sqj rhoj => rename_alg rhoj (sql_to_alg sqj)
                                  end) lsq) in
                  eval_equery (ExtAlg := ExtAlg) ip is' ia I' 
                              (N_ary_Equery_NaturalJoin FlagBag lq) =CE=
                  Fecol.mk_col 
                    (CTuple (Data.T ExtAlg)) FlagBag
                    (N_product_list 
                       (Data.T ExtAlg)
                       (map
                          (fun q : equery ExtAlg =>
                             Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q)) lq))).
      {
        apply (eval_N_ary_Equery_NaturalJoin_FlagBag (ExtAlg := ExtAlg) ip is' ia WI').
        - revert W3 W4 W5 W8 W11 W14 Hs; generalize lsq; intro l.
          induction l as [ | [sq1 r1] l]; intros W3 W4 W5 W8 W11 W14 Hs; [trivial | ].
          rewrite map_unfold, Oset.all_diff_bool_unfold, IHl, Bool.andb_true_r.
          + rewrite negb_true_iff, <- not_true_iff_false, Oset.mem_bool_true_iff, in_map_iff.
            intros [[sq2 r2] [H1 H2]].
            rewrite forallb_forall in W3, W4, W8, W11, W14.
            assert (W := W4 _ (or_introl _ (refl_equal _))); rewrite forallb_forall in W.
            generalize (W _ (or_intror _ H2)); clear W; intro W.
            assert (Ha : Fset.is_empty 
                           (A T)
                           (_from_item_sort (_From_Item sq1 r1)
                                            interS _from_item_sort (_From_Item sq2 r2)) = false).
            {
              assert (Ha : _from_item_sort (_From_Item sq1 r1) =S= 
                           _from_item_sort (_From_Item sq2 r2)).
              {
                rewrite (Fset.equal_eq_1 _ _ _ _ (Hs _ _ (or_introl _ (refl_equal _)))).
                rewrite (Fset.equal_eq_2 _ _ _ _ (Hs _ _ (or_intror _ H2))), H1.
                apply Fset.equal_refl.
              }
              rewrite <- not_true_iff_false, Fset.is_empty_spec, Fset.equal_spec; intro H.
              assert (W' := W8 _ (or_introl _ (refl_equal _)));
                rewrite negb_true_iff, <- not_true_iff_false in W'; apply W'.
              rewrite Fset.is_empty_spec, Fset.equal_spec; intro a.
              rewrite <- H, Fset.mem_inter.
              rewrite (Fset.mem_eq_2 _ _ _ Ha), Bool.andb_diag; apply refl_equal.
            }
            simpl Data.T in *; rewrite Ha, Oset.eq_bool_true_iff in W.
            rewrite Oset.all_diff_bool_unfold, Bool.andb_true_iff, negb_true_iff, 
              <- not_true_iff_false in W5.
            apply (proj1 W5).
            rewrite Oset.mem_bool_true_iff; rewrite W; assumption.
          + simpl in W3; rewrite Bool.andb_true_iff in W3; apply (proj2 W3).
          + rewrite forallb_forall; intros x1 Hx1.
            rewrite forallb_forall in W4; generalize (W4 _ (or_intror _ Hx1)).
            rewrite 2 forallb_forall; intros W4' x2 Hx2.
            apply W4'; right; assumption.
          + rewrite Oset.all_diff_bool_unfold, Bool.andb_true_iff in W5; apply (proj2 W5).
          + simpl in W8; rewrite Bool.andb_true_iff in W8; apply (proj2 W8).
          + simpl in W11; rewrite Bool.andb_true_iff in W11; apply (proj2 W11).
          + simpl in W14; rewrite Bool.andb_true_iff in W14; apply (proj2 W14).
          + do 3 intro; apply Hs; right; assumption.
        - intros q1 q2 Hq1 Hq2 _Hq; rewrite in_map_iff in Hq1, Hq2.
          destruct Hq1 as [[sq1 r1] [_Hq1 Hq1]]; subst q1.
          destruct Hq2 as [[sq2 r2] [_Hq2 Hq2]]; subst q2.
          rewrite forallb_forall in W4.
          generalize (W4 _ Hq1); rewrite forallb_forall; intro W4'.
          generalize (W4' _ Hq2); clear W4'; intro W4'.
          assert (H : Fset.is_empty 
                        (A T)
                        (_from_item_sort (_From_Item sq1 r1)
                                         interS _from_item_sort (_From_Item sq2 r2)) = false).
          {
            rewrite Fset.is_empty_spec, <- not_true_iff_false, Fset.equal_spec; intro H.
            rewrite Fset.is_empty_spec, <- not_true_iff_false, Fset.equal_spec in _Hq.
            apply _Hq; intro a.
            rewrite <- (Fset.mem_eq_2 _ _ _ (Fset.inter_eq_1 _ _ _ _ (Hs _ _ Hq1))).
            rewrite <- (Fset.mem_eq_2 _ _ _ (Fset.inter_eq_2 _ _ _ _ (Hs _ _ Hq2))), H.
            apply refl_equal.
          }
          simpl Data.T in *; rewrite H, Oset.eq_bool_true_iff in W4'.
          injection W4'; do 2 intro; subst; trivial.
      }
      cbv beta iota zeta in H.
      rewrite (Fecol.nb_occ_eq_2 _ _ H), Fecol.nb_occ_mk_col_flagbag, map_map; [ | trivial].
      rewrite (Febag.nb_occ_eq_2 _ _ _ (N_product_bag_N_product_list _ _)), Febag.nb_occ_mk_bag.
      rewrite map_map; apply permut_nb_occ; apply permut_refl_alt.
      apply N_product_list_map_eq.
      intros [sq r] K.
      rewrite Fecol.elements_bag.
      - apply Febag.elements_spec1.
        rewrite Febag.nb_occ_equal.
        clear t; intro t; simpl.
        rewrite <- Fecol.nb_occ_bag.
        apply (proj1 (proj2 IHn)).
        + rewrite _tree_of_sql_query_unfold in Hn; simpl in Hn.
          refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
          apply le_S.
          refine (le_trans _ _ _ _ (le_plus_r _ _)).
          apply le_S.
          refine (le_trans _ _ _ _ (le_plus_l _ _)).
          apply in_list_size.
          rewrite in_map_iff; eexists; split; [ | apply K]; trivial.
        + rewrite forallb_forall in W3; apply W3; trivial.
        + rewrite forallb_forall in W11; apply W11; trivial.
        + rewrite forallb_forall in W14; apply W14; trivial.
        + apply refl_equal.
      - unfold rename_alg; destruct r.
        + rewrite (flag_sql_to_alg sq); apply refl_equal.
        + unfold rename_equery; rewrite flag_equery_omega.
          rewrite (flag_sql_to_alg sq); apply refl_equal.
    } 
    destruct g as [g | ].
    * destruct f as [ | | atm]; try discriminate W13.
      destruct atm; try discriminate W13.
      cbv beta iota zeta in Hq; subst q.
      rewrite eval_equery_unfold, (_eval_sql_query_unfold _ _ _ _ (_Sql_Select _ _ _ _)).
      cbv beta iota zeta.
      rewrite 2 filter_true; [ | intros; apply refl_equal | intros; apply refl_equal].
      rewrite Fecol.nb_occ_mk_col_flagbag, Febag.nb_occ_mk_bag; 
        [ | apply flag_sql_to_alg_star_true].
      simpl interp_partition; simpl make_groups.
      {
        apply (Oeset.nb_occ_map_eq_2_3 (OLTuple T) (OTuple T)).
        - intros l1 l2 Hl.
          refine (Oeset.compare_eq_trans _ _ _ _ (eval_codes_on_grp_projection_grp _ _) _).
          apply projection_grp_eq; rewrite compare_list_t; assumption.
        - intros x; apply permut_nb_occ.
          assert (_IH : forall x : tuple (Data.T ExtAlg),
                          Oeset.nb_occ 
                            (OTuple T) x 
                            (Fecol.elements 
                               (eval_equery 
                                  (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg_star_true lsq))) =
                          Oeset.nb_occ 
                            (OTuple T) x
                            (Febag.elements 
                               _ (N_product_bag T (map (_eval_from_item ip is ia I) lsq)))).
          {
            intro _x; unfold Fecol.nb_occ in IH; rewrite IH.
            rewrite Febag.nb_occ_elements; apply refl_equal.
          }
          set (l := Fecol.elements
                      (eval_equery 
                         (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg_star_true lsq))) in *.
          assert (Hl := refl_equal l); unfold l at 2 in Hl; clearbody l.
          simpl Fecol.elements in *; rewrite <- Hl; clear Hl IH.
          set (l' :=  (Febag.elements 
                         (Fecol.CBag (CTuple T))
                         (N_product_bag T (map (_eval_from_item ip is ia I) lsq)))) in *.
          clearbody l'.
          apply permut_trans with (partition_list_expr is l g).
          + apply permut_refl_alt; apply comparelA_eq_refl_alt.
            * intros; apply Oeset.compare_eq_refl.
            * clear l' _IH Hn W7; revert l; induction g as [ | x1 g]; 
              intro l; [apply refl_equal | ].
              simpl; rewrite IHg; apply flat_map_eq.
              intros a _; rewrite 2 partition_expr_partition_expr2.
              apply f_equal.
              unfold partition_expr2.
              assert (H : {{{Fset.mk_set (FVal T)
                                         (map (interp_funterm is' (funterm_of_funterm x1)) a)}}} =
                          {{{Fset.mk_set (FVal T) (map (interp_funterm is x1) a)}}}).
              {
                apply Fset.elements_spec1.
                rewrite Fset.equal_spec; intro v; do 2 apply f_equal.
                rewrite <- map_eq; intros f _; apply interp_funterm_funterm_of_funterm.
              }
              rewrite H; clear H.
              set (lv := ({{{Fset.mk_set (FVal T) (map (interp_funterm is x1) a)}}})) in *.
              clearbody lv.
              generalize (a :: nil); clear a l IHg.
              induction lv as [ | v1 lv]; intro l; simpl; [trivial | ].
              rewrite IHlv.
              case (partition_value is x1 lv l); [trivial | ].
              intros l1 ll1.
              assert (H : List.partition
                            (fun t0 : tuple T =>
                               Oset.eq_bool 
                                 (OVal T)
                                 (interp_funterm is' (funterm_of_funterm x1) t0) v1) l1 =
                          List.partition
                            (fun t0 : tuple T =>
                               Oset.eq_bool (OVal T) (interp_funterm is x1 t0) v1) l1).
              {
                apply partition_eq.
                intros; rewrite interp_funterm_funterm_of_funterm; trivial.
              }
              rewrite H; apply refl_equal.
          + apply permut_refl_alt; apply partition_list_expr_eq_alt.
            apply nb_occ_permut; apply _IH.
      }
    * subst q; cbv beta iota zeta; 
      rewrite eval_equery_unfold, _eval_sql_query_unfold; cbv beta iota zeta.
      rewrite filter_true; [ | intros; apply refl_equal].
      rewrite flag_equery_omega, flag_sql_to_alg_star_true.
      rewrite Fecol.nb_occ_mk_col_flagbag, Febag.nb_occ_mk_bag; [ | trivial].
      simpl interp_partition; simpl make_groups; rewrite map_map, flag_sql_to_alg_star_true.
      rewrite 3 filter_map, 2 map_map.
      {
        apply (Oeset.nb_occ_map_eq_2_3 (OTuple T) (OTuple T)).
        - intros t1 t2 Ht.
          refine (Oeset.compare_eq_trans _ _ _ _ (eval_codes_on_grp_projection_grp _ _) _).
          apply projection_grp_eq; apply permut_refl_alt; simpl; rewrite Ht; trivial.
        - clear t; intro t.
          rewrite <- Fecol.nb_occ_elements, Fecol.nb_occ_mk_col_flagbag; [ | trivial].
          rewrite Oeset.nb_occ_id.
          + rewrite 2 Oeset.nb_occ_filter.
            * rewrite <- Fecol.nb_occ_elements, IH, <- Febag.nb_occ_elements.
              {
                case_eq (Febag.nb_occ (Fecol.CBag (CTuple T)) t
                                    (N_product_bag T (map (_eval_from_item ip is ia I) lsq))).
                - intro Ht.
                  case_eq (interp_formula
                          (ExtAlg := ExtAlg) ip is'
                          (fun q : equery ExtAlg =>
                             Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q))
                          (fun _ : variable (equery ExtAlg) =>
                             eval_codes_on_grp
                               (ExtAlg := ExtAlg) is' ia
                               (map
                                  (fun a : attribute T =>
                                     Code ExtAlg a (A_Expr (F_Dot a)))
                                  ({{{sort (sql_to_alg_star_true lsq)}}})) 
                               (t :: nil)) (sql_formula_to_alg (sql_to_alg_star_true lsq) f));
                    intro Hf; simpl in Hf; simpl; rewrite Hf;
                    case (_eval_sql_formula ip is ia I f (Grp T (t :: nil))); trivial.
                - intros p Ht.
                  apply match_bool_eq.
                  rewrite <- (_eval_sql_formula_tpl_grp_eq ip is ia I f t); [ | apply Oeset.compare_eq_refl].
                  assert (Hn' : tree_size (_tree_of_sql_formula f) <= n).
                  {
                    rewrite _tree_of_sql_query_unfold in Hn; simpl in Hn.
                    refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
                    apply le_S.
                    refine (le_trans _ _ _ _ (le_plus_r _ _)).
                    apply le_S.
                    refine (le_trans _ _ _ _ (le_plus_r _ _)).
                    apply le_S; apply le_plus_l.
                  }
                  assert (IHf := proj2 
                               (proj2 IHn) 
                               (Fset.Union (A T) (map (_from_item_sort (DBS:=DBS)) lsq))
                               f Hn' W6 W12 W15 (sql_to_alg_star_true lsq) _ (refl_equal _) t).
                  simpl in IHf; rewrite <- IHf.
                  apply interp_formula_eq; intros _.
                  apply eval_codes_on_grp_singleton_equal.
                  apply (well_sorted_equery (ExtAlg := ExtAlg) ip is' ia WI').
                  apply Fecol.nb_occ_mem; rewrite IH, Ht; discriminate.
              } 
            * intros x1 x2 _ Hx; apply _eval_sql_formula_grp_eq.
              apply permut_refl_alt; simpl; rewrite Hx; trivial.
            * intros x1 x2 Hx1 Hx.
              apply interp_formula_eq; intros _.
              apply eval_codes_on_grp_eq; apply permut_refl_alt; simpl; rewrite Hx; trivial.
          + intros x Hx.
            assert (Kx := eval_codes_on_grp_singleton_equal
                            ExtAlg is' ia (sort (sql_to_alg_star_true lsq)) x).
            apply Kx.
            apply (well_sorted_equery (ExtAlg := ExtAlg) ip is' ia WI').
            rewrite Oeset.mem_filter in Hx.
            * case_eq (interp_formula (ExtAlg := ExtAlg)
                         ip is'
                         (fun q : equery ExtAlg =>
                            Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q))
                         (fun _ : variable (equery ExtAlg) =>
                            eval_codes_on_grp (ExtAlg := ExtAlg)
                              is' ia
                              (map
                                 (fun a : attribute T =>
                                    Code ExtAlg a (A_Expr (F_Dot a)))
                                 ({{{sort (sql_to_alg_star_true lsq)}}})) 
                              (x :: nil)) (sql_formula_to_alg (sql_to_alg_star_true lsq) f));
              intro Hf; simpl in Hf; simpl in Hx; rewrite Hf in Hx; try discriminate Hx.
              rewrite Fecol.mem_elements; simpl Data.T in *; apply Hx.
            * intros x1 x2 Hx1 _Hx.
              apply interp_formula_eq; intros _.
              apply eval_codes_on_grp_eq; apply permut_refl_alt; simpl; rewrite _Hx; trivial.
      }
- intros sq r Hn W Wstar Wgby y Hy t; subst y.
  unfold rename_alg; rewrite _eval_from_item_unfold.
  destruct r as [ | r].
  + simpl att_renaming_item_to_from_item; simpl projection.
    unfold Febag.map; rewrite map_id; [ | intros; trivial].
    rewrite Febag.nb_occ_mk_bag, <- Febag.nb_occ_elements.
    apply (proj1 IHn).
    * rewrite _tree_of_from_item_unfold in Hn; simpl in Hn.
      refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
      apply le_plus_l.
    * rewrite _well_formed_from_item_unfold in W; apply W.
    * simpl in Wstar; apply Wstar.
    * simpl in Wgby; apply Wgby.
    * apply refl_equal.
  + unfold rename_equery; rewrite eval_equery_unfold; cbv beta iota zeta.
    rewrite (flag_sql_to_alg sq); [ | apply refl_equal].
    rewrite Fecol.nb_occ_mk_col_flagbag; [ | trivial].
    rewrite filter_true; [ | intros; apply refl_equal].
    unfold Febag.map; rewrite Febag.nb_occ_mk_bag.
    unfold interp_partition; rewrite map_map.
    apply (Oeset.nb_occ_map_eq_2_3 (OTuple T) (OTuple T)).
    * intros x1 x2 Hx.
      rewrite _well_formed_from_item_unfold, 3 Bool.andb_true_iff in W.
      destruct W as [[[W1 W2] W3] W4].
      unfold att_as_as_pair in W2; rewrite map_map in W2.
      unfold rho_renaming, apply_renaming, att_as_as_pair.
      rewrite (Fset.elements_spec1 _ _ _ (sort_sql_to_alg sq W4 Wstar Wgby)).
      simpl Data.T in *; rewrite <- (Fset.elements_spec1 _ _ _ W2).
      unfold att_renaming_item_to_from_item, eval_codes_on_grp, projection.
      rewrite 6 map_map.
      { 
        apply mk_tuple_eq.
        - rewrite Fset.equal_spec; intro a; 
          rewrite 2 Fset.mem_mk_set, eq_bool_iff, 2 Oset.mem_bool_true_iff, 2 in_map_iff; split.
          + intros [_a [_Ha _Hx]].
            generalize (Fset.in_elements_mem _ _ _ _Hx); clear _Hx; intro _Hx.
            rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff in _Hx.
            destruct _Hx as [[__a _b] [__Ha _Hx]]; simpl in __Ha; subst __a.
            simpl in _Ha.
            case_eq (Oset.find (OAtt T) _a
                               (map
                                (fun x : att_renaming T =>
                                   match x with
                                     | Att_As e a => (e, a)
                                   end) r)).
            * intros b Hb.
              assert (Kb := Oset.find_some _ _ _ Hb); rewrite in_map_iff in Kb.
              destruct Kb as [[__a __b] [_Kb Kb]]; 
              injection _Kb; clear _Kb; do 2 intro; subst __a __b.
              eexists; split; [ | apply Kb].
              rewrite Hb in _Ha; simpl; trivial.
            * intro Abs; apply False_rec.
              apply (Oset.find_none _ _ _ Abs _b); rewrite in_map_iff.
              eexists; split; [ | apply _Hx]; trivial.
          + intros [[_a b] [_Ha _Hx]]; simpl in _Ha; subst b; simpl.
            case_eq (Oset.find (OAtt T) _a
                               (map
                                (fun x : att_renaming T =>
                                   match x with
                                     | Att_As e a => (e, a)
                                   end) r)).
            * intros b Hb.
              assert (Kb := Oset.find_some _ _ _ Hb); rewrite in_map_iff in Kb.
              destruct Kb as [[__a __b] [_Kb Kb]]; 
                injection _Kb; clear _Kb; do 2 intro; subst __a __b.
              {
                exists _a; rewrite Hb; split.
                - apply (Oset.all_diff_bool_fst (OAtt T) _ W1 _a); 
                  unfold att_as_as_pair; rewrite in_map_iff; 
                  eexists; split; [ | apply Kb | | apply _Hx]; trivial.
                - apply Fset.mem_in_elements; 
                  rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff.
                  eexists; split; [ | apply _Hx]; trivial.
              }
            * intros Abs; apply False_rec.
              apply (Oset.find_none _ _ _ Abs a).
              rewrite in_map_iff; eexists; split; [ | apply _Hx]; trivial.
        - intros a Ha Ka; rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff in Ha, Ka.
          case_eq (Oset.find
                     (OAtt (Data.T ExtAlg)) a
                     (map
                        (fun x : attribute T =>
                           (match
                               Oset.find (OAtt T) x
                                         (map
                                            (fun x0 : att_renaming T =>
                                               match x0 with
                                                 | Att_As e a0 => (e, a0)
                                               end) r)
                             with
                               | Some b => b
                               | None => x
                             end,
                            @A_Expr _ _ (Data.aggregate ExtAlg) (@F_Dot T (Data.symb ExtAlg) x)))
                        ({{{Fset.mk_set (A T)
                                        (map
                                           (fun x : att_renaming T =>
                                              fst match x with
                                                    | Att_As e a0 => (e, a0)
                                                  end) r)}}}))).
          + intros b Hb; simpl Data.T in *; rewrite Hb.
            case_eq (Oset.find (OAtt T) a
                               (map
                                  (fun x : att_renaming T =>
                                     match
                                       match x with
                                         | Att_As a0 b0 =>
                                           Select_As (@A_Expr _ _ aggregate (@F_Dot T symb a0)) b0
                                       end
                                     with
                                       | Select_As e a0 => (a0, e)
                                     end) r)).
            * intros b' Hb'.
              simpl; rewrite quicksort_equation; simpl ListSort.partition.
              cbv beta iota zeta; rewrite quicksort_equation; simpl in *.
              assert (K : b = (aggterm_of_aggterm b')).
              {
                assert (Kb := Oset.find_some _ _ _ Hb); rewrite in_map_iff in Kb.
                destruct Kb as [_a [Kb _Ha]]; injection Kb; clear Kb.
                intros _Hb __Ha; rewrite <- _Hb.
                assert (Kb' := Oset.find_some _ _ _ Hb'); rewrite in_map_iff in Kb'.
                destruct Kb' as [[_a' _b'] [Kb' _Ha']]; injection Kb'; clear Kb'; 
                do 2 intro; subst _b' b'; simpl; do 2 apply f_equal.
                case_eq (Oset.find (OAtt T) _a
                                   (map
                                      (fun x0 : att_renaming T =>
                                         match x0 with
                                           | Att_As e a0 => (e, a0)
                                         end) r)).
                - intros c Hc; rewrite Hc in __Ha; subst c.
                  assert (Kc := Oset.find_some _ _ _ Hc); rewrite in_map_iff in Kc.
                  destruct Kc as [[a1 a2] [_Ha1 Kc]]; injection _Ha1; clear _Ha1; 
                  do 2 intro; subst a1 a2.
                  unfold one_to_one_renaming_bool, Oset.one_to_one_bool in W3.
                  rewrite forallb_forall in W3.
                  assert (_Ja : In _a ({{{_sql_sort sq}}})).
                  {
                    rewrite <- (Fset.elements_spec1 _ _ _ W2).
                    apply Fset.mem_in_elements; 
                      rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff.
                    eexists; split; [ | apply Kc]; trivial.
                  }
                  assert (_Ja' : In _a' ({{{_sql_sort sq}}})).
                  {
                    rewrite <- (Fset.elements_spec1 _ _ _ W2).
                    apply Fset.mem_in_elements; 
                      rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff.
                    eexists; split; [ | apply _Ha']; unfold att_as_as_pair; trivial.
                  }
                  assert (W := W3 _ _Ja); rewrite forallb_forall in W.
                  generalize (W _ _Ja'); clear W; intro W.
                  unfold apply_renaming, att_as_as_pair in W; rewrite Hc in W.
                  assert (Ja : Oset.find (OAtt T) _a'
                                         (map
                                            (fun x : att_renaming T =>
                                               match x with
                                                 | Att_As e a => (e, a)
                                               end) r) = Some a).
                  {
                    case_eq (Oset.find (OAtt T) _a'
                                       (map
                                          (fun x : att_renaming T => match x with
                                                                       | Att_As e a0 => (e, a0)
                                                                     end) r)).
                    - intros d Hd; apply f_equal.
                      assert (Kd := Oset.find_some _ _ _ Hd); rewrite in_map_iff in Kd.
                      destruct Kd as [[a1 a2] [Ha1 Kd]]; simpl in Ha1; injection Ha1; clear Ha1.
                      do 2 intro; subst a1 a2.
                      apply (Oset.all_diff_bool_fst (OAtt T) _ W1 _a'); 
                        unfold att_as_as_pair; rewrite in_map_iff. 
                      + eexists; split; [ | apply Kd]; trivial.
                      + eexists; split; [ | apply _Ha']; trivial.
                    - intro Abs; apply False_rec; apply (Oset.find_none _ _ _ Abs a).
                      rewrite in_map_iff; eexists; split; [ | apply _Ha']; trivial.
                  }
                  rewrite Ja, Oset.compare_eq_refl in W.
                  case_eq (Oset.compare (OAtt T) _a _a'); 
                    intro __Ha; rewrite __Ha in W; try discriminate W.
                  rewrite Oset.compare_eq_iff in __Ha; assumption.
                - intro Abs.
                  generalize (Fset.in_elements_mem _ _ _ _Ha); clear _Ha; intro _Ha.
                  rewrite Fset.mem_mk_set, Oset.mem_bool_true_iff, in_map_iff in _Ha.
                  destruct _Ha as [[a1 a2] [_Ka _Ha]]; simpl in _Ka; subst a1.
                  apply False_rec; apply (Oset.find_none _ _ _ Abs a2).
                  rewrite in_map_iff; eexists; split; [ | apply _Ha]; trivial.
              }
              rewrite K, interp_aggterm_aggterm_of_aggterm; apply interp_aggterm_eq.
              simpl; rewrite Hx; trivial.
            * intro Abs.
              rewrite in_map_iff in Ka.
              destruct Ka as [[_b _a] [_Ka Ka]]; simpl in _Ka; subst _a.
              apply False_rec; apply (Oset.find_none _ _ _ Abs (A_Expr (F_Dot _b))).
              rewrite in_map_iff; eexists; split; [ | apply Ka]; trivial.
          + intro Abs; apply False_rec.
            rewrite in_map_iff in Ha.
            destruct Ha as [_a [_Ha Ha]]; simpl in _Ha.
            apply (Oset.find_none _ _ _ Abs (A_Expr (F_Dot _a))).
            rewrite in_map_iff; exists _a; rewrite _Ha; split; trivial.
      }
    * clear t; intro t; rewrite <- Fecol.nb_occ_elements, <- Febag.nb_occ_elements.
      {
        apply (proj1 IHn).
        - rewrite _tree_of_from_item_unfold in Hn; simpl in Hn.
          refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
          apply le_plus_l.
        - rewrite _well_formed_from_item_unfold, Bool.andb_true_iff in W; apply (proj2 W).
        - simpl in Wstar; apply Wstar.
        - simpl in Wgby; apply Wgby.
        - apply refl_equal.
      }
- intros sa f Hn W Wstar Wgby q f' Hf interp_dom t; subst f'.
  destruct f as [o f1 f2 | f | [ | p l | qtf l sq]].
  + rewrite sql_formula_to_alg_unfold, _eval_sql_formula_unfold; simpl.
    rewrite _tree_of_sql_formula_unfold in Hn; simpl in Hn.
    rewrite _well_formed_sql_formula_unfold, Bool.andb_true_iff in W.
    rewrite sql_formula_has_no_star_unfold, Bool.andb_true_iff in Wstar.
    rewrite sql_formula_has_no_group_by_and_having_unfold, Bool.andb_true_iff in Wgby.
    apply f_equal2.
    * {
        apply (proj2 (proj2 IHn)) with sa q.
        - refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
          apply le_plus_l.
        - apply (proj1 W).
        - apply (proj1 Wstar).
        - apply (proj1 Wgby).
        - apply refl_equal.
      } 
    * {
        apply (proj2 (proj2 IHn)) with sa q.
        - refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
          refine (le_trans _ _ _ _ (le_plus_r _ _)).
          apply le_plus_l.
        - apply (proj2 W).
        - apply (proj2 Wstar).
        - apply (proj2 Wgby).
        - apply refl_equal.
      }
  + rewrite _tree_of_sql_formula_unfold in Hn; simpl in Hn.
    rewrite sql_formula_to_alg_unfold, _eval_sql_formula_unfold; simpl.
    apply f_equal.
    apply (proj2 (proj2 IHn)) with sa q.
    * refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
      apply le_plus_l.
    * apply W.
    * apply Wstar.
    * apply Wgby.
    * apply refl_equal.
  + apply refl_equal.
  + rewrite sql_formula_to_alg_unfold, _eval_sql_formula_unfold, _eval_sql_atom_unfold; simpl.
    apply f_equal; rewrite map_map, <- map_eq.
    intros e He; simpl.
    apply interp_aggterm_interp_term; apply Oeset.compare_eq_refl.
  +  rewrite sql_formula_to_alg_unfold, _eval_sql_formula_unfold, _eval_sql_atom_unfold; simpl.
     assert (H : forall t1 t2 x1 x2, t1 =t= t2 -> x1 =t= x2 ->
                   interp_formula
                     (ExtAlg := ExtAlg) ip is'
                     (fun q0 : equery ExtAlg =>
                        Fecol.elements (eval_equery (ExtAlg := ExtAlg) ip is' ia I' q0))
                     (ivar_xt (OQ ExtAlg) OP (OSymbol ExtAlg)
                              (fun _ : variable (equery ExtAlg) => t1) (Vrbl (sql_to_alg sq) 1) x1)
                     (build_conjunction q (sql_to_alg sq) l) =
                   forallb
                     (fun
                         a : predicate * list (aggterm T symb aggregate) *
                             list (aggterm T symb aggregate) =>
                         let (y, l2) := a in
                         let (p, l1) := y in
                         ip p
                            (map (eval_expression is ia (Tpl T t2)) l1 ++
                                 map (eval_expression is ia (Tpl T x2)) l2)) l).
     {
       intros t1 t2 x1 x2 HHt HHx.
       rewrite _well_formed_sql_formula_unfold, _well_formed_sql_atom_unfold in W.
       rewrite sql_formula_has_no_star_unfold, sql_atom_has_no_star_unfold in Wstar.
       rewrite sql_formula_has_no_group_by_and_having_unfold, 
         sql_atom_has_no_group_by_and_having_unfold in Wgby.
       rewrite eq_bool_iff, build_conjunction_true_iff, forallb_forall; split; intro H.
       - intros [[p k] k1] H1.
         rewrite <- (H _ _ _ H1); simpl; rewrite map_app, 2 map_map.
         apply f_equal; apply f_equal2; rewrite <- map_eq.
         + intros a _; simpl.
           set (m := size_aggterm a).
           assert (Hm := le_n m).
           unfold m at 1 in Hm; clearbody m.
           revert a Hm.
           induction m as [ | m];
             intros a Hm; [ destruct a; inversion Hm | ].
           destruct a as [f | a d f | s _l]; simpl.
           * clear m IHm Hm.
             set (m := size_funterm f).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert f Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt.
                 case_eq ( Oset.compare (OVR (OQ ExtAlg) OP (OSymbol ExtAlg))
                                        (Vrbl (sql_to_alg sq) 1) (Vrbl q 0)).
                 + rewrite Oset.compare_eq_iff; discriminate.
                 + intro K; simpl in *; rewrite K; apply sym_eq; apply tuple_eq_dot; trivial.
                 + intro K; simpl in *; rewrite K; apply sym_eq; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * apply f_equal; apply f_equal2; [ | apply refl_equal].
             clear m IHm Hm.
             set (m := size_funterm d).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert d Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt.
                 case_eq ( Oset.compare (OVR (OQ ExtAlg) OP (OSymbol ExtAlg))
                                        (Vrbl (sql_to_alg sq) 1) (Vrbl q 0)).
                 + rewrite Oset.compare_eq_iff; discriminate.
                 + intro K; simpl in *; rewrite K; apply sym_eq; apply tuple_eq_dot; trivial.
                 + intro K; simpl in *; rewrite K; apply sym_eq; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros _a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * rewrite map_map; apply f_equal; rewrite <- map_eq.
             intros _a Ha; apply IHm.
             simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
             apply in_list_size; assumption.
         + intros a _; simpl.
           set (m := size_aggterm a).
           assert (Hm := le_n m).
           unfold m at 1 in Hm; clearbody m.
           revert a Hm.
           induction m as [ | m];
             intros a Hm; [ destruct a; inversion Hm | ].
           destruct a as [f | a d f | s _l]; simpl.
           * clear m IHm Hm.
             set (m := size_funterm f).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert f Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt; rewrite Oset.compare_eq_refl; 
                 apply sym_eq; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * apply f_equal; apply f_equal2; [ | apply refl_equal].
             clear m IHm Hm.
             set (m := size_funterm d).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert d Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt; rewrite Oset.compare_eq_refl; 
                 apply sym_eq; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros _a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * rewrite map_map; apply f_equal; rewrite <- map_eq.
             intros _a Ha; apply IHm.
             simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
             apply in_list_size; assumption.
       - intros p k k1 H1.
         rewrite <- (H _ H1); simpl; rewrite map_app, 2 map_map.
         apply f_equal; apply f_equal2; rewrite <- map_eq.
         + intros a _; simpl.
           set (m := size_aggterm a).
           assert (Hm := le_n m).
           unfold m at 1 in Hm; clearbody m.
           revert a Hm.
           induction m as [ | m];
             intros a Hm; [ destruct a; inversion Hm | ].
           destruct a as [f | a d | s _l]; simpl.
           * clear m IHm Hm.
             set (m := size_funterm f).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert f Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt.
                 case_eq ( Oset.compare (OVR (OQ ExtAlg) OP (OSymbol ExtAlg))
                                        (Vrbl (sql_to_alg sq) 1) (Vrbl q 0)).
                 + rewrite Oset.compare_eq_iff; discriminate.
                 + intro K; simpl in *; rewrite K; apply tuple_eq_dot; trivial.
                 + intro K; simpl in *; rewrite K; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * apply f_equal; apply f_equal2; [ | apply refl_equal].
             clear m IHm Hm.
             set (m := size_funterm d).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert d Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt.
                 case_eq ( Oset.compare (OVR (OQ ExtAlg) OP (OSymbol ExtAlg))
                                        (Vrbl (sql_to_alg sq) 1) (Vrbl q 0)).
                 + rewrite Oset.compare_eq_iff; discriminate.
                 + intro K; simpl in *; rewrite K; apply tuple_eq_dot; trivial.
                 + intro K; simpl in *; rewrite K; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros _a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * rewrite map_map; apply f_equal; rewrite <- map_eq.
             intros _a Ha; apply IHm.
             simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
             apply in_list_size; assumption.
         + intros a _; simpl.
           set (m := size_aggterm a).
           assert (Hm := le_n m).
           unfold m at 1 in Hm; clearbody m.
           revert a Hm.
           induction m as [ | m];
             intros a Hm; [ destruct a; inversion Hm | ].
           destruct a as [f | a d f | s _l]; simpl.
           * clear m IHm Hm.
             set (m := size_funterm f).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert f Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt; rewrite Oset.compare_eq_refl; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * apply f_equal; apply f_equal2; [ | apply refl_equal].
             clear m IHm Hm.
             set (m := size_funterm d).
             assert (Hm := le_n m).
             unfold m at 1 in Hm; clearbody m.
             revert d Hm.
             {
               induction m as [ | m];
               intros f Hm; [ destruct f; inversion Hm | ].
               destruct f as [v | b | f _l]; simpl; trivial.
               - unfold ivar_xt; rewrite Oset.compare_eq_refl; apply tuple_eq_dot; trivial.
               - rewrite map_map; apply f_equal; rewrite <- map_eq.
                 intros _a Ha; apply IHm.
                 simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
                 apply in_list_size; assumption.
             }
           * rewrite map_map; apply f_equal; rewrite <- map_eq.
             intros _a Ha; apply IHm.
             simpl in Hm; refine (le_trans _ _ _ _ (le_S_n _ _ Hm)).
             apply in_list_size; assumption.
     }
     assert (IH : forall t : tuple (Data.T ExtAlg),
                    Fecol.nb_occ t (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg sq)) =
                    Febag.nb_occ (Fecol.CBag (CTuple T)) t
                                 (_eval_sql_query ip is ia I sq)).
     {
       apply (proj1 IHn).
       - rewrite _tree_of_sql_formula_unfold in Hn; simpl in Hn.
         refine (le_trans _ _ _ _ (le_S_n _ _ Hn)).
         apply le_S.
         refine (le_trans _ _ _ _ (le_plus_l _ _)).
         apply le_plus_l.
       - rewrite _well_formed_sql_formula_unfold, _well_formed_sql_atom_unfold, 
           Bool.andb_true_iff in W.
         apply (proj2 W).
       - apply Wstar.
       - apply Wgby.
       - apply refl_equal.
     }
     destruct qtf; simpl.
     * {
         rewrite eq_bool_iff, 2 forallb_forall; split.
         - intros _H x Hx.
           unfold interp_dom in _H.
           assert (Kx : x inCE (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg sq))).
           {
             apply Fecol.nb_occ_mem; rewrite IH, Febag.nb_occ_elements.
             apply Oeset.mem_nb_occ; rewrite Oeset.mem_bool_true_iff.
             exists x; split; [apply Oeset.compare_eq_refl | assumption].
           }
           rewrite Fecol.mem_elements, Oeset.mem_bool_true_iff in Kx.
           destruct Kx as [x' [Kx Hx']].
           rewrite <- (_H _ Hx').
           apply sym_eq; apply H; [apply Oeset.compare_eq_refl | ].
           apply Oeset.compare_eq_sym; apply Kx.
         - intros _H x Hx.
           unfold interp_dom in Hx.
           assert (Kx : x inBE (_eval_sql_query ip is ia I sq)).
           {
             apply Febag.nb_occ_mem; rewrite <- IH.
             apply Fecol.mem_nb_occ.
             apply Fecol.in_elements_mem; assumption.
           }
           rewrite Febag.mem_unfold, Oeset.mem_bool_true_iff in Kx.
           destruct Kx as [x' [Kx Hx']].
           rewrite <- (_H _ Hx').
           apply H; [apply Oeset.compare_eq_refl | apply Kx].
       }
     * {
         rewrite eq_bool_iff, 2 existsb_exists; split.
         - intros [x [_H Hx]].
           unfold interp_dom in _H.
           assert (Kx : x inBE (_eval_sql_query ip is ia I sq)).
           {
             apply Febag.nb_occ_mem; rewrite <- IH.
             apply Fecol.mem_nb_occ.
             apply Fecol.in_elements_mem; assumption.
           }
           rewrite Febag.mem_unfold, Oeset.mem_bool_true_iff in Kx.
           destruct Kx as [x' [Kx Hx']].
           exists x'; split; [trivial | ].
           rewrite <- (H _ _ _ _ (Oeset.compare_eq_refl _ _) Kx); apply Hx.
         - intros [x [_H Hx]].
           assert (Kx : x inCE (eval_equery (ExtAlg := ExtAlg) ip is' ia I' (sql_to_alg sq))).
           {
             apply Fecol.nb_occ_mem; rewrite IH, Febag.nb_occ_elements.
             apply Oeset.mem_nb_occ; rewrite Oeset.mem_bool_true_iff.
             exists x; split; [apply Oeset.compare_eq_refl | assumption].
           }
           rewrite Fecol.mem_elements, Oeset.mem_bool_true_iff in Kx.
           destruct Kx as [x' [Kx Hx']].
           exists x'; split; [assumption | ].
           rewrite (H _ _ x' x (Oeset.compare_eq_refl _ _)).
           + apply Hx.
           + apply Oeset.compare_eq_sym; assumption.
       } 
Qed.

Hypothesis Pred_Equal : predicate.
Hypothesis interp_Pred_Equal :
  forall v1 v2, ip Pred_Equal (v1 :: v2 :: nil) = Oset.eq_bool (OVal T) v1 v2.

Hypothesis extract_N_of_att : attribute T -> N.
Hypothesis incr_att : N -> attribute T -> attribute T.
Hypothesis extract_N_of_incr_att : 
  forall a n, extract_N_of_att (incr_att n a) = (n + extract_N_of_att a)%N.
Hypothesis incr_att_inj : forall m a1 a2, incr_att m a1 = incr_att m a2 -> a1 = a2.
Hypothesis extract_N_of_default_att : extract_N_of_att default_att = 0%N.

Lemma sql_to_alg_is_sound :
  forall sq, 
    well_formed_sql_query OP OSymb OAggregate sq = true ->
    forall t, Fecol.nb_occ 
                t (eval_equery 
                     (ExtAlg := ExtAlg) ip is' ia I' 
                     (sql_to_alg 
                        (normalize_sql_query 
                            OSymb OAggregate
                           Pred_Equal extract_N_of_att incr_att default_att sq))) =
              Febag.nb_occ _ t (eval_sql_query ip is ia I sq).
Proof.
intros sq W t.
assert (H := normalize_sql_query_is_sound 
                 (T := T) (DBS := DBS) OP OSymb OAggregate ip is ia
                 Pred_Equal interp_Pred_Equal 
                 extract_N_of_att incr_att extract_N_of_incr_att default_att
                 extract_N_of_default_att WI sq W).
destruct H as [H1 [H2 [H3 H4]]].
rewrite (proj1 (sql_to_alg_is_sound_etc _) 
               (normalize_sql_query OSymb OAggregate Pred_Equal extract_N_of_att
                                    incr_att default_att sq) (le_n _)); trivial.
rewrite Febag.nb_occ_equal in H4; rewrite H4; apply refl_equal.
Qed.

End Sec.

(** OCaml extraction (#<a href="parser.ml">.ml</a>#, #<a href="parser.mli">.mli</a>#) *)
Extraction "parser.ml" sql_to_alg. 
