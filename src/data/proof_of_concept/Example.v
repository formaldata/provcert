(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Relations SetoidList List String Ascii Bool ZArith NArith.

Require Import Bool3 FlatData ListFacts OrderedSet
        FiniteSet FiniteBag FiniteCollection Tree Formula Sql.

Require Import Values TuplesImpl GenericInstance SqlSyntax KAlgebra KAlgebraproof.

Import Tuple.
Import NullValues.

Require Import Plugins.

Definition db0 := init_db_ TNull.

Parse_sql "create table table1 (name text, remission bool, treatment text, stage int);" t0.
Parse_sql "create table table2 (name text, remission bool, treatment text, stage int);" t1.
Parse_sql "create table table3 (name text, city text);" t2.
Parse_sql "insert into table1 values ('Smith', true, 'A', 1), ('Jones', false, 'B', 2), ('Garcia', false, 'A', 4), ('Smith', true, 'A', 2) ;" i0.
Parse_sql "insert into table2 values ('Miller', true, 'B', 2), ('Jones', false, 'B', 2), ('Johnson', true, 'A', 2);" i1.
Parse_sql "insert into table3 values ('Smith', 'Cambridge'), ('Jones', 'New York'), ('Bridges', 'Roma'), ('Garcia', 'Portland');" i2.
Parse_sql "select * from table1;" q0.
Parse_sql "select * from table2;" q1.
Parse_sql "table table1 union table table2;" q2.
Parse_sql "select * from (table table1 union table table2) table4 where (stage <= 2 and remission = true);" q3.
Parse_sql "select name, treatment from (table table1 union table table2) table4 where (stage <= 2 and remission = true);" q4.
Parse_sql "select count(*) as c from (table table1 union table table2) table4 where (stage <= 2 and remission = true) group by treatment;" q5.
Parse_sql "select * from table3;" q6.
Parse_sql "select table1.name as name, remission, treatment, stage, city from table1, table3 where table1.name = table3.name;" q7.

(*Tables without annotations*)
Definition db_example := i1 (i0 (t1 (t0 db0))).
Definition db_example2 := i2 (i0 (t2 (t0 db0))).

(*Table 1*)
Eval compute in (eval_sql_query_in_state db_example q0).

(*Table 2*)
Eval compute in (eval_sql_query_in_state db_example q1).

(*Table 3*)
Eval compute in (eval_sql_query_in_state db_example q2).

(*Table 4*)
Eval compute in (eval_sql_query_in_state db_example q3).

(*Table 5*)
Eval compute in (eval_sql_query_in_state db_example q4).

(*Table 6*)
Eval compute in (eval_sql_query_in_state db_example q5).

(*Table 7*)
Eval compute in (eval_sql_query_in_state db_example2 q6).

(*Table 8*)
Eval compute in (eval_sql_query_in_state db_example2 q7).

(*Sql queries to algebra queries*)
Definition sql_alg1 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example) q0).
Definition sql_alg2 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example) q1).
Definition sql_alg3 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example) q2).
Definition sql_alg4 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example) q3).
Definition sql_alg5 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example) q4).
Definition sql_alg6 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example) q5).
Definition sql_alg7 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example2) q6).
Definition sql_alg8 := (SqlAlgebra.sql_query_to_alg (_basesort TNull db_example2) q7).


Notation name := (Attr_string 0 "name").
Notation name1 := (Attr_string 0 "table1_name").
Notation rem1 := (Attr_bool 0 "table1_remission").
Notation tre1 := (Attr_string 0 "table1_treatment").
Notation stage1 := (Attr_Z 0 "table1_stage").
Notation name2 := (Attr_string 0 "table2_name").
Notation rem2 := (Attr_bool 0 "table2_remission").
Notation tre2 := (Attr_string 0 "table2_treatment").
Notation stage2 := (Attr_Z 0 "table2_stage").
Notation name3 := (Attr_string 0 "table3_name").
Notation rem3 := (Attr_bool 0 "table3_remission").
Notation tre3 := (Attr_string 0 "table3_treatment").
Notation stage3 := (Attr_Z 0 "table3_stage").
Notation name4 := (Attr_string 0 "table4_name").
Notation rem4 := (Attr_bool 0 "table4_remission").
Notation tre4 := (Attr_string 0 "table4_treatment").
Notation stage4 := (Attr_Z 0 "table4_stage").
Notation c := (Attr_Z 0 "c").
Notation city := (Attr_string 0 "table3_city").

Definition mk_t1 n r t s :=
  mk_tuple 
    TNull
    (Fset.mk_set _ (name1 :: tre1 :: stage1 :: rem1 :: nil))
    (fun a => match a with
              | name1 => Value_string (Some n)
              | rem1 => Value_bool (Some r)
              | tre1 => Value_string (Some t)
              | stage1 => Value_Z (Some s)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).

Definition mk_t2 n r t s :=
  mk_tuple 
    TNull
    (Fset.mk_set _ (name2 :: rem2 :: tre2 :: stage2 :: nil))
    (fun a => match a with
              | name2 => Value_string (Some n)
              | rem2 => Value_bool (Some r)
              | tre2 => Value_string (Some t)
              | stage2 => Value_Z (Some s)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).

Definition mk_t4 n r t s :=
  mk_tuple 
    TNull
    (Fset.mk_set _ (name4 :: rem4 :: tre4 :: stage4 :: nil))
    (fun a => match a with
              | name4 => Value_string (Some n)
              | rem4 => Value_bool (Some r)
              | tre4 => Value_string (Some t)
              | stage4 => Value_Z (Some s)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).


Definition mk_t_proj n t :=
  mk_tuple
    TNull
    (Fset.mk_set _ (name4 :: tre4 :: nil))
    (fun a => match a with
              | name4 => Value_string (Some n)
              | tre4 => Value_string (Some t)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).

Definition mk_t_aggr n :=
  mk_tuple
    TNull
    (Fset.mk_set _ (c :: nil))
    (fun a => match a with
              | c => Value_Z (Some n)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).

Definition mk_t_city n c :=
  mk_tuple
    TNull
    (Fset.mk_set _ (name3 :: city :: nil))
    (fun a => match a with
              | name3 => Value_string (Some n)
              | city => Value_string (Some c)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).

Definition mk_t_nat_join n r t s c :=
  mk_tuple
    TNull
    (Fset.mk_set _ (name :: rem1 :: tre1 :: stage1 :: city :: nil))
    (fun a => match a with
              | name => Value_string (Some n)
              | rem1 => Value_bool (Some r)
              | tre1 => Value_string (Some t)
              | stage1 => Value_Z (Some s)
              | city => Value_string (Some c)
              | Attr_string _ _ => Value_string None
              | Attr_Z _ _ => Value_Z None
              | Attr_bool _ _ => Value_bool None
              end).


(*Annotations *)
Definition eval_1 := f (eval_query_prov_Null db_example nil sql_alg1).
Definition eval_2 := f (eval_query_prov_Null db_example nil sql_alg2).
Definition eval_3 := f (eval_query_prov_Null db_example nil sql_alg3).
Definition eval_4 := f (eval_query_prov_Null db_example nil sql_alg4).
Definition eval_5 := f (eval_query_prov_Null db_example nil sql_alg5).
Definition eval_6 := f (eval_query_prov_Null db_example nil sql_alg6).
Definition eval_7 := f (eval_query_prov_Null db_example2 nil sql_alg7).
Definition eval_8 := f (eval_query_prov_Null db_example2 nil sql_alg8).

(*t_1*)
Eval compute in (eval_1 (mk_t1 "Smith" true "A" 1)).
(*t_2*)
Eval compute in (eval_1 (mk_t1 "Jones" false "B" 2)).
(*t_3*)
Eval compute in (eval_1 (mk_t1 "Garcia" false "A" 4)).
(*t_4*)
Eval compute in (eval_1 (mk_t1 "Smith" true "A" 2)).

(*t_5*)
Eval compute in (eval_2 (mk_t2 "Miller" true "B" 2)).
(*t_6*)
Eval compute in (eval_2 (mk_t2 "Jones" false "B" 2)).
(*t_7*)
Eval compute in (eval_2 (mk_t2 "Johnson" true "A" 2)).


(*t_8*)
Eval compute in (eval_3 (mk_t1 "Smith" true "A" 1)).
(*t_9*)
Eval compute in (eval_3 (mk_t1 "Jones" false "B" 2)).
(*t_10*)
Eval compute in (eval_3 (mk_t1 "Garcia" false "A" 4)).
(*t_11*)
Eval compute in (eval_3 (mk_t1 "Smith" true "A" 2)).
(*t_12*)
Eval compute in (eval_3 (mk_t1 "Miller" true "B" 2)).
(*t_13*)
Eval compute in (eval_3 (mk_t1 "Johnson" true "A" 2)).

(*t_14*)
Eval compute in (eval_4 (mk_t4 "Smith" true "A" 1)).
(*t_15*)
Eval compute in (eval_4 (mk_t4 "Smith" true "A" 2)).
(*t_16*)
Eval compute in (eval_4 (mk_t4 "Miller" true "B" 2)).
(*t_17*)
Eval compute in (eval_4 (mk_t4 "Johnson" true "A" 2)).

(*t_18*)
Eval compute in (eval_5 (mk_t_proj "Smith" "A")).
(*t_19*)
Eval compute in (eval_5 (mk_t_proj "Miller" "B")).
(*t_20*)
Eval compute in (eval_5 (mk_t_proj "Johnson" "A")).


(*t_21*)
Eval compute in (eval_6 (mk_t_aggr 3)).
(*t_22*)
Eval compute in (eval_6 (mk_t_aggr 1)).


(*s_1*)
Eval compute in (eval_7 (mk_t_city "Smith" "Cambridge")).
(*s_2*)
Eval compute in (eval_7 (mk_t_city "Jones" "New york")).
(*S_3*)
Eval compute in (eval_7 (mk_t_city "Bridges" "Roma")).
(*s_4*)
Eval compute in (eval_7 (mk_t_city "Garcia" "Portland")).


(*s_5*)
Eval compute in (eval_8 (mk_t_nat_join "Smith" true "A" 1 "Cambridge")).
(*s_6*)
Eval compute in (eval_8 (mk_t_nat_join "Jones" false "B" 2 "New york")).
(*S_7*)
Eval compute in (eval_8 (mk_t_nat_join "Garcia" false "A" 4 "Portland")).
(*s_8*)
Eval compute in (eval_8 (mk_t_nat_join "Smith" true "A" 2 "Cambridge")).
