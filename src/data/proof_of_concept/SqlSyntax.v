(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Relations SetoidList List String Ascii Bool ZArith NArith.

Require Import Bool3 FlatData ListFacts OrderedSet
        FiniteSet FiniteBag FiniteCollection Tree Formula Sql.

Import Tuple.
Require Import Values TuplesImpl GenericInstance.

Definition value := NullValues.value.
Definition Value_bool := NullValues.Value_bool.
Definition Value_Z := NullValues.Value_Z.
Definition Value_string := NullValues.Value_string.

Definition predicate := Values.predicate.
Definition Predicate := Values.Predicate.

Definition symbol := symbol value.
Definition Symbol := Symbol value.

Definition aggregate := Values.aggregate.
Definition Aggregate := Values.Aggregate.

Definition db_state := db_state_ TNull.
Definition init_db := init_db_ TNull.
Definition create_table := create_table_ TNull.
Definition insert_tuples_into := insert_tuples_into_ TNull.

Fixpoint mk_att att atts vals :=
  match atts, vals with
  | a::atts, v::vals => match attribute_compare a att with
                        | Eq => v
                        | _ => mk_att att atts vals
                        end
  | _, _ => match att with
                | Attr_string _ _ => Value_string None
                | Attr_Z _ _ => Value_Z None
                | Attr_bool _ _ => Value_bool None
                end
  end.

Definition mk_tuple_lists atts vals :=
  mk_tuple TNull (Fset.mk_set _ atts) (fun att => mk_att att atts vals).

Definition mk_insert db rel atts valss :=
  insert_tuples_into db (List.map (mk_tuple_lists atts) valss) rel.

(** Again, for the constructs of the SQL framework *)
Definition _Select_Star := (@Select_Star TNull).

Definition __Select_List l := (@Select_List TNull (@_Select_List TNull l)).

Definition _Select_As := (@Select_As TNull).

Definition _Att_Ren_Star := (@Att_Ren_Star TNull).

Definition _Att_Ren_List := (@Att_Ren_List TNull).

Definition _Att_As := (@Att_As TNull).

Definition _Sql_Table := (@Sql_Table TNull relname).

Definition _Sql_Set := (@Sql_Set TNull relname).

Definition _Sql_Select := (@Sql_Select TNull relname).

Definition _select := (@select TNull).

Definition _From_Item := (@From_Item TNull relname).

Definition _sql_from_item := (@sql_from_item TNull relname).

Definition _Group_Fine := (@Group_Fine TNull).

Definition __Sql_Dot a := (F_Dot TNull a).

Definition _Sql_Dot a := (@A_Expr TNull (__Sql_Dot a)).

Definition _A_Expr := (@A_Expr TNull).

Definition _Group_By l := (@Group_By TNull (List.map _A_Expr l)).

Definition _A_fun := (@A_fun TNull).

Definition _A_agg := (@A_agg TNull).

Definition _F_Constant := (@F_Constant TNull).

Definition _CstZ z := _F_Constant (Value_Z z).

Definition CstZ z := (_A_Expr (_CstZ z)).

Definition _F_Expr := (@F_Expr TNull).

Definition _sql_query := (sql_query TNull relname). 

Definition _Sql_True := (@Sql_True TNull (sql_query TNull relname)).

Definition _Sql_Not := (@Sql_Not TNull (sql_query TNull relname)). 


Definition _Sql_Conj := (@Sql_Conj TNull (sql_query TNull relname)).

Definition _Sql_Pred := (@Sql_Pred TNull (sql_query TNull relname)).

Definition _Sql_Quant := (@Sql_Quant TNull (sql_query TNull relname)).

Definition _Sql_Exists := (@Sql_Exists TNull (sql_query TNull relname)).

Definition _Sql_In := (@Sql_In TNull (sql_query TNull relname)).

Definition _aggterm := (aggterm TNull).

Definition _funterm := (funterm TNull).

Definition _Union := Union.
Definition _Inter := Inter.
Definition _Diff := Diff.

Definition SortedTuplesT := TNull.

Definition contains_nulls (t : tuple TNull) :=
 existsb (fun a => match dot TNull t a with
                   | NullValues.Value_Z None 
                   | NullValues.Value_string None
                   | NullValues.Value_bool None => true
                   | NullValues.Value_string (Some _) 
                   | NullValues.Value_bool (Some _) 
                   | NullValues.Value_Z (Some _) => false
                   end) ({{{labels TNull t}}}).

Lemma contains_nulls_eq :
  forall t1 t2, t1 =t= t2 -> contains_nulls t1 = contains_nulls t2.
Proof.
intros t1 t2 Ht.
unfold contains_nulls.
rewrite tuple_eq in Ht.
rewrite <- (Fset.elements_spec1 _ _ _ (proj1 Ht)).
apply existsb_eq.
intros a Ha; rewrite <- (proj2 Ht); [apply refl_equal | ].
apply Fset.in_elements_mem; assumption.
Qed.

(* Evaluation of SQL-COQ queries *)

Definition eval_sql_query_in_state (db : db_state) q :=
  show_bag_tuples _ (@eval_sql_query 
     TNull relname (_basesort _ db) (_instance _ db) unknown3 contains_nulls nil q).





Require Import SqlAlgebra.

Eval compute in (well_formed_q (fun r => Fset.mk_set (A TNull) ((Attr_Z 0 "table1_a")::(Attr_Z 0 "table1_b")::nil)) nil (Q_Pi
         (Projection._Select_List TNull (Select_As TNull
            (A_Expr _
               (F_Dot TNull (Attr_Z 0 "_c1")))
            (Attr_Z 0 "_c0") :: nil))
         (Q_Pi
            (Projection._Select_List TNull (Select_As _
               (A_fun TNull (Symbol "plus")
                  (A_Expr _
                     (F_Dot TNull
                        (Attr_Z 0 "table1_a"))
                   :: A_Expr _
                        (F_Dot TNull
                           (Attr_Z 0 "table1_b")) :: nil)) (Attr_Z 0 "_c1") :: nil))
            (Q_Table TNull (Rel "table1"))))).


Eval compute in (well_formed_q (fun r => Fset.mk_set (A TNull) ((Attr_Z 0 "table1_a")::(Attr_Z 0 "table1_b")::nil)) nil (Q_Pi
            (Projection._Select_List TNull (Select_As _
               (A_fun TNull (Symbol "plus")
                  (A_Expr _
                     (F_Dot TNull
                        (Attr_Z 0 "table1_a"))
                   :: A_Expr _
                        (F_Dot TNull
                           (Attr_Z 0 "table1_b")) :: nil)) (Attr_Z 0 "_c1") :: nil))
            (Q_Table TNull (Rel "table1")))).


Eval compute in (forallb (fun x => match x with Select_As _ (A_Expr _ _) _ => true | _ => false end) (Select_As _
               (A_fun TNull (Symbol "plus")
                  (A_Expr _
                     (F_Dot TNull
                        (Attr_Z 0 "table1_a"))
                   :: A_Expr _
                        (F_Dot TNull
                           (Attr_Z 0 "table1_b")) :: nil)) (Attr_Z 0 "_c1") :: nil)).


Eval compute in (well_formed_s TNull (env_t TNull nil (default_tuple TNull (sort (fun r => Fset.mk_set (A TNull) ((Attr_Z 0 "table1_a")::(Attr_Z 0 "table1_b")::nil)) (Q_Table TNull (Rel "table1"))))) (Select_As _
               (A_fun TNull (Symbol "plus")
                  (A_Expr _
                     (F_Dot TNull
                        (Attr_Z 0 "table1_a"))
                   :: A_Expr _
                        (F_Dot TNull
                           (Attr_Z 0 "table1_b")) :: nil)) (Attr_Z 0 "_c1") :: nil)).


(* Eval compute in (fresh_att_in_env (sort (fun r => Fset.mk_set (A TNull) ((Attr_Z 0 "table1_a")::(Attr_Z 0 "table1_b")::nil)) (Q_Pi (Projection._Select_List TNull (Select_As _ *)
(*                (A_fun TNull (Symbol "plus") *)
(*                   (A_Expr _ *)
(*                      (F_Dot TNull *)
(*                         (Attr_Z 0 "table1_a")) *)
(*                    :: A_Expr _ *)
(*                         (F_Dot TNull *)
(*                            (Attr_Z 0 "table1_b")) :: nil)) (Attr_Z 0 "_c1") :: nil)) (Q_Table TNull (Rel "table1")))) nil). *)


Eval compute in (well_formed_q (fun r => Fset.mk_set (A TNull) ((Attr_Z 0 "table1_a")::(Attr_Z 0 "table1_b")::nil)) nil (Q_Table TNull (Rel "table1"))).


Eval compute in (well_formed_q (fun r => Fset.mk_set (A TNull) ((Attr_Z 0 "table1_a")::(Attr_Z 0 "table1_b")::nil)) nil (Q_Pi
         (Projection._Select_List TNull (Select_As TNull
            (A_Expr _
               (F_Dot TNull (Attr_Z 0 "_c1")))
            (Attr_Z 0 "_c0") :: nil))
         (Q_Pi
            (Projection._Select_List TNull ((Select_As _
               (A_Expr _ (F_Expr TNull (Symbol "plus")
                     ((F_Dot TNull
                        (Attr_Z 0 "table1_a"))
                   :: (F_Dot TNull
                           (Attr_Z 0 "table1_b")) :: nil))) (Attr_Z 0 "_c1")) :: nil))
            (Q_Table TNull (Rel "table1"))))).
