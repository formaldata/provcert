(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Require Import Arith NArith ZArith String List.
Require Import OrderedSet FiniteSet Bool3.

Definition option_compare (A : Type) (c : A -> A -> comparison) x y :=
  match x, y with
  | Some x, Some y => c x y
  | Some _, None => Gt
  | None, Some _ => Lt
  | None, None => Eq
  end.

Section Sec.

Hypothesis value : Type.
Hypothesis OVal : Oset.Rcd value.

(** Types a.k.a domains in the database textbooks. *)
Inductive type := 
 | type_string 
 | type_Z
 | type_bool.

Open Scope N_scope.

Definition N_of_type := 
    fun d => 
    match d with   
    | type_string => 0
    | type_Z => 1
    | type_bool => 2
    end.

Definition OT : Oset.Rcd type.
apply Oemb with N_of_type.
intros d1 d2; case d1; case d2;
(exact (fun _ => refl_equal _) || (intro Abs; discriminate Abs)).
Defined.

Inductive predicate : Type := Predicate : string -> predicate.

Definition OP : Oset.Rcd predicate.
split with (fun x y => match x, y with Predicate s1, Predicate s2 => string_compare s1 s2 end).
- intros [s1] [s2]; generalize (Oset.eq_bool_ok Ostring s1 s2); simpl.
  case (string_compare s1 s2).
  + apply f_equal.
  + intros H1 H2; apply H1; injection H2; exact (fun h => h).
  + intros H1 H2; apply H1; injection H2; exact (fun h => h).
- intros [s1] [s2] [s3]; apply (Oset.compare_lt_trans Ostring s1 s2 s3).
- intros [s1] [s2]; apply (Oset.compare_lt_gt Ostring s1 s2).
Defined.

Inductive symbol : Type :=
  | Symbol : string -> symbol
  | CstVal : value -> symbol.

Definition symbol_compare (s1 s2 : symbol) :=
  match s1, s2 with
    | Symbol s1, Symbol s2 => string_compare s1 s2
    | Symbol _, CstVal _ => Lt
    | CstVal _, Symbol _ => Gt
    | CstVal v1, CstVal v2 => Oset.compare OVal v1 v2
  end.

Definition OSymbol : Oset.Rcd symbol.
split with symbol_compare.
- intros [s1 | s1] [s2 | s2]; simpl; try discriminate.
  + generalize (Oset.eq_bool_ok Ostring s1 s2); simpl.
    case (string_compare s1 s2).
    * apply f_equal.
    * intros H1 H2; apply H1; injection H2; exact (fun h => h).
    * intros H1 H2; apply H1; injection H2; exact (fun h => h).
  + generalize (Oset.eq_bool_ok OVal s1 s2); simpl.
    case (Oset.compare OVal s1 s2).
    * apply f_equal.
    * intros H1 H2; apply H1; injection H2; exact (fun h => h).
    * intros H1 H2; apply H1; injection H2; exact (fun h => h).
- intros [s1 | s1] [s2 | s2] [s3 | s3]; simpl;
  try (apply (Oset.compare_lt_trans Ostring) ||
             apply (Oset.compare_lt_trans OVal) ||
             trivial || discriminate).
- intros [s1 | s1] [s2 | s2]; simpl;
  try (apply (Oset.compare_lt_gt Ostring) ||
             apply (Oset.compare_lt_gt OVal) ||
             trivial || discriminate).
Defined.

Inductive aggregate : Type :=
  | Aggregate : string -> aggregate.

Definition OAgg : Oset.Rcd aggregate.
split with (fun x y => match x, y with Aggregate s1, Aggregate s2 => string_compare s1 s2 end).
- intros [s1] [s2]; generalize (Oset.eq_bool_ok Ostring s1 s2); simpl.
  case (string_compare s1 s2).
  + apply f_equal.
  + intros H1 H2; apply H1; injection H2; exact (fun h => h).
  + intros H1 H2; apply H1; injection H2; exact (fun h => h).
- intros [s1] [s2] [s3]; apply (Oset.compare_lt_trans Ostring s1 s2 s3).
- intros [s1] [s2]; apply (Oset.compare_lt_gt Ostring s1 s2).
Defined.

End Sec.

Module SimpleValues.

(** Embedding several coq datatypes (corresponding to domains) into a single uniform
    type for values.
*)
Inductive value : Set :=
  | String : string -> value
  | Integer : Z -> value
  | Bool : bool -> value
  | NULL : value.

Definition type_of_value v := 
match v with
  | String _  => type_string
  | Integer _ => type_Z
  | Bool _ => type_bool
  | NULL => type_string
  end.

(** Default values for each type. *)
Definition default_value := NULL.

(** injection of domain names into natural numbers in order to
    build an ordering on them.
*)

(** Comparison over values, in order to build an ordered type over values, and then
    finite sets.
*)

Definition value_compare x y := 
  match x, y with
    | String s1, String s2 => string_compare s1 s2
    | String _, _ => Lt 
    | Integer _, String _ => Gt
    | Integer z1, Integer z2 => Z.compare z1 z2
    | Integer _, _ => Lt
    | Bool _, String _ => Gt
    | Bool _, Integer _ => Gt
    | Bool b1, Bool b2 => bool_compare b1 b2
    | Bool _, NULL => Lt
    | NULL, NULL => Eq
    | NULL, _ => Gt
  end.

Definition OVal : Oset.Rcd value.
split with value_compare.
- (* 1/3 *)
  intros [s1 | z1 | b1 | ] [s2 | z2 | b2 | ]; 
    try discriminate; simpl; trivial.
  + generalize (Oset.eq_bool_ok Ostring s1 s2); simpl; case (string_compare s1 s2).
    * apply (f_equal (fun x => String x)).
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
  + generalize (Oset.eq_bool_ok OZ z1 z2); simpl; case (Z.compare z1 z2).
    * apply (f_equal (fun x => Integer x)).
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
  + generalize (Oset.eq_bool_ok Obool b1 b2); simpl; case (bool_compare b1 b2).
    * apply (f_equal (fun x => Bool x)).
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
- (* 1/2 *)
  intros [s1 | z1 | b1 | ] [s2 | z2 | b2 | ] [s3 | z3 | b3 | ]; trivial; try discriminate; simpl.
  + apply (Oset.compare_lt_trans Ostring).
  + apply (Oset.compare_lt_trans OZ).
  + apply (Oset.compare_lt_trans Obool).
- (* 1/1 *)
  intros [s1 | z1 | b1 | ] [s2 | z2 | b2 | ]; trivial; simpl.
  + apply (Oset.compare_lt_gt Ostring).
  + apply (Oset.compare_lt_gt OZ).
  + apply (Oset.compare_lt_gt Obool).
Defined.

Definition FVal := Fset.build OVal.

End SimpleValues.

Module NullValues.

(** Embedding several coq datatypes (corresponding to domains) into a single uniform
    type for values.
*)
Inductive value : Set :=
  | Value_string : option string -> value
  | Value_Z : option Z -> value
  | Value_bool : option bool -> value.

Definition type_of_value v := 
match v with
  | Value_string _  => type_string
  | Value_Z _ => type_Z
  | Value_bool _ => type_bool
  end.

(** Default values for each type. *)
Definition default_value d :=
  match d with
    | type_string => Value_string None
    | type_Z => Value_Z None
    | type_bool => Value_bool None
  end.

(** injection of domain names into natural numbers in order to
    build an ordering on them.
*)


(** Comparison over values, in order to build an ordered type over values, and then
    finite sets.
*)


Definition value_compare x y := 
  match x, y with
    | Value_string s1, Value_string s2 => option_compare _ string_compare s1 s2
    | Value_string _, _ => Lt 
    | Value_Z _, Value_string _ => Gt
    | Value_Z z1, Value_Z z2 => option_compare _ Z.compare z1 z2
    | Value_Z _, _ => Lt
    | Value_bool _, Value_string _ => Gt
    | Value_bool _, Value_Z _ => Gt
    | Value_bool b1, Value_bool b2 => option_compare _ bool_compare b1 b2
  end.

Definition OVal : Oset.Rcd value.
split with value_compare.
- (* 1/3 *)
  intros [[s1 | ] | [z1 | ] | [b1 | ]] [[s2 | ] | [z2 | ] | [b2 | ]]; 
    try discriminate; simpl; trivial.
  + generalize (Oset.eq_bool_ok Ostring s1 s2); simpl; case (string_compare s1 s2).
    * apply (f_equal (fun x => Value_string (Some x))).
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
  + generalize (Oset.eq_bool_ok OZ z1 z2); simpl; case (Z.compare z1 z2).
    * apply (f_equal (fun x => Value_Z (Some x))).
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
  + generalize (Oset.eq_bool_ok Obool b1 b2); simpl; case (bool_compare b1 b2).
    * apply (f_equal (fun x => Value_bool (Some x))).
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
- (* 1/2 *)
  intros [[s1 | ] | [z1 | ] | [b1 | ]] [[s2 | ] | [z2 | ] | [b2 | ]] [[s3 | ] | [z3 | ] | [b3 | ]]; trivial; try discriminate; simpl.
  + apply (Oset.compare_lt_trans Ostring).
  + apply (Oset.compare_lt_trans OZ).
  + apply (Oset.compare_lt_trans Obool).
- (* 1/1 *)
  intros [[s1 | ] | [z1 | ] | [b1 | ]] [[s2 | ] | [z2 | ] | [b2 | ]]; trivial; simpl.
  + apply (Oset.compare_lt_gt Ostring).
  + apply (Oset.compare_lt_gt OZ).
  + apply (Oset.compare_lt_gt Obool).
Defined.

Definition FVal := Fset.build OVal.

Definition interp_predicate p :=
  match p with
    | Predicate "<" =>
      fun l =>
        match l with
          | Value_Z (Some a1) :: Value_Z (Some a2) :: nil =>
            match Z.compare a1 a2 with Lt => true3 | _ => false3 end
          | _ => unknown3
        end
    | Predicate "<=" =>
      fun l =>
        match l with
          | Value_Z (Some a1) :: Value_Z (Some a2) :: nil =>
            match Z.compare a1 a2 with Gt => false3 | _ => true3 end
          | _ => unknown3
        end
    | Predicate ">" =>
      fun l =>
        match l with
          | Value_Z (Some a1) :: Value_Z (Some a2) :: nil =>
            match Z.compare a1 a2 with Gt => true3 | _ => false3 end
          | _ => unknown3
        end
    | Predicate ">=" =>
      fun l =>
        match l with
          | Value_Z (Some a1) :: Value_Z (Some a2) :: nil =>
            match Z.compare a1 a2 with Lt => false3 | _ => true3 end
          | _ => unknown3
        end
    | Predicate "=" =>
      fun l =>
        match l with
        | v1 :: v2 :: nil =>
          match Oset.compare OVal v1 v2 with
          | Eq => true3
          | _ => false3
          end
        | _ => unknown3
        end
    | Predicate "<>" =>
      fun l =>
        match l with
        | v1 :: v2 :: nil =>
          match Oset.compare OVal v1 v2 with
          | Eq => false3
          | _ => true3
          end
        | _ => unknown3
        end
   | _ => fun _ => unknown3
  end.


Definition interp_unop_Z u l :=
  match l with
  | Value_Z (Some a1) :: nil => Value_Z (Some (u a1))
  | _ => Value_Z None
  end.


Lemma interp_unop_Z_equiv u l1 l2 :
  Oeset.compare (Oeset.mk_list (Oset.oeset_of_oset OVal)) l1 l2 = Eq ->
  interp_unop_Z u l1 = interp_unop_Z u l2.
Proof.
  destruct l1 as [ |[s1|[z1| ]|b1] [ | v12 l1]]; destruct l2 as [ |[s2|[z2| ]|b2] [ | v22 l2]]; simpl; try discriminate; auto.
  - case_eq (z1 ?= z2)%Z; try discriminate.
    rewrite Z.compare_eq_iff. now intros ->.
  - case_eq (z1 ?= z2)%Z; discriminate.
  - case_eq (z1 ?= z2)%Z; discriminate.
Qed.


Definition interp_binop_Z b l :=
  match l with
  | Value_Z (Some a1) :: Value_Z (Some a2) :: nil => Value_Z (Some (b a1 a2))
  | _ => Value_Z None
  end.


Lemma interp_binop_Z_equiv b l1 l2 :
  Oeset.compare (Oeset.mk_list (Oset.oeset_of_oset OVal)) l1 l2 = Eq ->
  interp_binop_Z b l1 = interp_binop_Z b l2.
Proof.
  destruct l1 as [ |[s11|[z11| ]|b11] [ | [s12|[z12| ]|b12] [ | v13 l31]]]; destruct l2 as [ |[s21|[z21| ]|b21] [ | [s22|[z22| ]|b22] [ | v23 l2]]]; simpl; try discriminate; auto;
    try (case_eq (z11 ?= z21)%Z; try discriminate; rewrite Z.compare_eq_iff; intros ->).
  - case_eq ((z12 ?= z22)%Z); try discriminate.
    rewrite Z.compare_eq_iff. now intros ->.
  - case_eq (z12 ?= z22)%Z; discriminate.
  - case_eq (z12 ?= z22)%Z; discriminate.
Qed.


Definition comparison_case : forall r : comparison, {r = Eq} + {r <> Eq}.
Proof. intros [ | | ]; [now left|now right|now right]. Defined.


Definition interp_symbol f :=
  match f with
  | Symbol _ s =>
    if comparison_case (string_compare s "plus"%string) then
      interp_binop_Z Zplus
    else if comparison_case (string_compare s "mult"%string) then
      interp_binop_Z Zmult
    else if comparison_case (string_compare s "minus"%string) then
      interp_binop_Z Zminus
    else if comparison_case (string_compare s "opp"%string) then
      interp_unop_Z Z.opp
    else
      fun _ => Value_Z None
  | CstVal _ v =>
    fun l =>
      match l with
      | nil => v
      | _ => default_value (type_of_value v)
      end
  end.


Lemma interp_symbol_equiv f l1 l2 :
  Oeset.compare (Oeset.mk_list (Oset.oeset_of_oset OVal)) l1 l2 = Eq ->
  interp_symbol f l1 = interp_symbol f l2.
Proof.
  case f; simpl.
  - intros s H. case (comparison_case (string_compare s "plus")); intros _.
    + now apply interp_binop_Z_equiv.
    + case (comparison_case (string_compare s "mult")); intros _.
      * now apply interp_binop_Z_equiv.
      * case (comparison_case (string_compare s "minus")); intros _.
        -- now apply interp_binop_Z_equiv.
        -- case (comparison_case (string_compare s "opp")); intros _.
           ++ now apply interp_unop_Z_equiv.
           ++ reflexivity.
  - destruct l1 as [ |v1 l1]; destruct l2 as [ |v2 l2]; auto; discriminate.
Qed.

Fixpoint max_aux l acc :=
  match l with
  |nil => acc
  |a::tl =>
   match a with
   | Value_Z (Some _) => match Oset.compare OVal acc a with
                  |Gt => max_aux tl acc
                  |_ => max_aux tl a
                  end
   |_ => Value_Z (None)
   end
  end.

Definition max l :=
  match l with
  |nil => Value_Z (None)
  |a::tl => match a with
            | Value_Z (Some _) => max_aux tl a
            |_ => Value_Z (None)
            end
  end.

Definition interp_aggregate a := fun l =>
  match a with
  | Aggregate s =>
    if comparison_case (string_compare s "count"%string) then
      Value_Z (Some (Z_of_nat (List.length l)))
    else if comparison_case (string_compare s "sum"%string) then
      Value_Z
        (Some (fold_left (fun acc x => match x with Value_Z (Some x) => (acc + x)%Z | _ => acc end) l 0%Z))
    else if comparison_case (string_compare s "avg"%string) then
      Value_Z
        (Some (let sum :=
                   fold_left (fun acc x => match x with
                                           | Value_Z (Some x) => (acc + x)%Z
                                           | _ => acc end) l 0%Z in
               Z.div sum (Z_of_nat (List.length l))))
    else if comparison_case (string_compare s "max"%string) then
       max l
    else
      Value_Z None
  end.


Lemma interp_aggregate_equiv f l1 l2 :
  Oeset.compare (Oeset.mk_list (Oset.oeset_of_oset OVal)) l1 l2 = Eq ->
  interp_aggregate f l1 = interp_aggregate f l2.
Proof.
  case f; simpl.
  intros s H. case (comparison_case (string_compare s "count")); intros _.
  - now rewrite (comparelA_eq_length_eq _ _ _ H).
  - case (comparison_case (string_compare s "sum")); intros _.
    + assert (H1:(forall z1 z2 v1 v2,
    Oeset.compare (Oset.oeset_of_oset OZ) z1 z2 = Eq ->
    Oeset.compare (Oset.oeset_of_oset OVal) v1 v2 = Eq ->
    Oeset.compare (Oset.oeset_of_oset OZ)
      match v1 with
      | Value_Z (Some x0) => (z1 + x0)%Z
      | _ => z1
      end match v2 with
          | Value_Z (Some x0) => (z2 + x0)%Z
          | _ => z2
          end = Eq)).
      {
        intros z1 z2 [s1|[a1| ]|b1] [s2|[a2| ]|b2]; try discriminate; auto.
        simpl. rewrite !Z.compare_eq_iff. now intros -> ->.
      }
      generalize (comparelA_fold_left_eq_oset OZ OVal (fun (acc : Z) (x : value) =>
           match x with
           | Value_Z (Some x0) => (acc + x0)%Z
           | _ => acc
           end) H1 l1 l2 0%Z H).
      rewrite Oset.compare_eq_iff. now intros ->.
    + case (comparison_case (string_compare s "avg")); intros _.
      * assert (H1:(forall (x1 x2 : Z) (y1 y2 : value),
    Oset.compare OZ x1 x2 = Eq ->
    Oset.compare OVal y1 y2 = Eq ->
    Oset.compare OZ match y1 with
                    | Value_Z (Some x0) => (x1 + x0)%Z
                    | _ => x1
                    end match y2 with
                        | Value_Z (Some x0) => (x2 + x0)%Z
                        | _ => x2
                        end = Eq)).
      {
        intros z1 z2 [s1|[a1| ]|b1] [s2|[a2| ]|b2]; try discriminate; auto.
        simpl. rewrite !Z.compare_eq_iff. now intros -> ->.
      }
      generalize (comparelA_fold_left_eq_oset OZ OVal (fun (acc : Z) (x : value) =>
           match x with
           | Value_Z (Some x0) => (acc + x0)%Z
           | _ => acc
           end) H1 l1 l2 0%Z H).
      rewrite Oset.compare_eq_iff. intros ->.
      now rewrite (comparelA_eq_length_eq _ _ _ H).
      * case (comparison_case (string_compare s "max")); intros _.
        -- destruct l1; destruct l2; try solve [trivial; inversion H].
           simpl.
           inversion H.
           case_eq (value_compare v v0); intros; rewrite H0 in H1; try solve [inversion H1].
           destruct v ; destruct v0; inversion H0; trivial.
           clear H H0.
           destruct o; destruct o0; try solve [inversion H3|trivial].
           simpl in H3.
           rewrite Z.compare_eq_iff in H3.
           rewrite H3.
           clear z H3.
           revert z0 l2 H1.
           induction l1; intros; destruct l2; try solve [trivial|inversion H1].
           simpl.
           inversion H1.
           case_eq (value_compare a v); intros; rewrite H in H0; try solve [inversion H0].
           destruct v; destruct a; inversion H; trivial.
           destruct o; destruct o0; try solve [inversion H3|trivial].
           simpl in H3. rewrite Z.compare_eq_iff in H3.
           rewrite H3.
           simpl; rewrite ! (IHl1 _ l2); trivial.
        -- reflexivity.
Qed.
End NullValues.

(** Embedding several coq datatypes (corresponding to domains) into a single uniform
    type for values.
*)
Inductive value : Set :=
  | Value_string : string -> value
  | Value_Z : Z -> value
  | Value_bool : bool -> value.

Definition type_of_value v := 
match v with
  | Value_string _  => type_string
  | Value_Z _ => type_Z
  | Value_bool _ => type_bool
  end.

(** Default values for each type. *)
Definition default_value d :=
  match d with
    | type_string => Value_string EmptyString
    | type_Z => Value_Z 0
    | type_bool => Value_bool false
  end.

(** injection of domain names into natural numbers in order to
    build an ordering on them.
*)

(** Comparison over values, in order to build an ordered type over values, and then
    finite sets.
*)
Definition value_compare x y := 
  match x, y with
    | Value_string s1, Value_string s2 => string_compare s1 s2
    | Value_string _, _ => Lt 
    | Value_Z _, Value_string _ => Gt
    | Value_Z z1, Value_Z z2 => Z.compare z1 z2
    | Value_Z _, _ => Lt
    | Value_bool _, Value_string _ => Gt
    | Value_bool _, Value_Z _ => Gt
    | Value_bool b1, Value_bool b2 => bool_compare b1 b2
  end.

Definition OVal : Oset.Rcd value.
split with value_compare.
- (* 1/3 *)
  intros [s1 | z1 | b1] [s2 | z2 | b2]; try discriminate.
  + generalize (Oset.eq_bool_ok Ostring s1 s2); simpl; case (string_compare s1 s2).
    * apply f_equal.
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
  + generalize (Oset.eq_bool_ok OZ z1 z2); simpl; case (Z.compare z1 z2).
    * apply f_equal.
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
  + generalize (Oset.eq_bool_ok Obool b1 b2); simpl; case (bool_compare b1 b2).
    * apply f_equal.
    * intros H1 H2; injection H2; apply H1.
    * intros H1 H2; injection H2; apply H1.
- (* 1/2 *)
  intros [s1 | z1 | b1] [s2 | z2 | b2] [s3 | z3 | b3]; trivial; try discriminate; simpl.
  + apply (Oset.compare_lt_trans Ostring).
  + apply (Oset.compare_lt_trans OZ).
  + apply (Oset.compare_lt_trans Obool).
- (* 1/1 *)
  intros [s1 | z1 | b1] [s2 | z2 | b2]; trivial; simpl.
  + apply (Oset.compare_lt_gt Ostring).
  + apply (Oset.compare_lt_gt OZ).
  + apply (Oset.compare_lt_gt Obool).
Defined.

Definition FVal := Fset.build OVal.
