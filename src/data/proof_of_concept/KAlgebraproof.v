(************************************************************************************)
(**                                                                                 *)
(**                              The ProvCert Library                               *)
(**                                                                                 *)
(**                       LRI, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                           Copyright 2020 : FormalData                           *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Chantal Keller                                                 *)
(**                  Rébecca Zucchini                                               *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.
Require Import Bool Bool3 List Arith NArith FlatData SemiRing.

Require Import ListFacts ListPermut BasicFacts Facts OrderedSet FiniteSet FiniteBag FiniteCollection FlatData Tree Bool3 Formula Sql.
Import Tuple.

Require Import Values TuplesImpl GenericInstance SqlSyntax SemiModule KAlgebra.

Definition instance_rel_ := _instance TNull.


Definition FesetT := (Feset.build (OTuple TNull)).
Definition setT := (Feset.set FesetT).

Notation BTupleTNull := (Fecol.CBag (CTuple TNull)).

Definition f_inst db r t :=
  Febag.nb_occ BTupleTNull t (instance_rel_ db r).

Definition support_inst db r :=
  Feset.mk_set FesetT(Febag.elements BTupleTNull (instance_rel_ db r)).

Definition sort_krel_inst db r :=
  _basesort db r.

Definition instance_prov_ db r :=
  mk_kr TNull (f_inst db r) (support_inst db r) (sort_krel_inst db r).

Lemma instance_prov_nb_occ : forall db r e,
 f (instance_prov_ db r) e = Febag.nb_occ BTupleTNull e (instance_rel_ db r).
Proof.
intros. trivial.
Qed.

Lemma instance_support : forall db r,
    sort_krel (instance_prov_ db r) = _basesort TNull db r.
Proof.
  intros. trivial.
Qed.


Lemma not_labels_not_find_value : forall a t,
    a inS? labels TNull t = false -> Oset.find OAN a t = None.
Proof.
  intros. apply Oset.not_mem_find_none.
  rewrite <- (Fset.mem_mk_set FAN); trivial.
Qed.


Lemma not_labels_dot_eq : forall a t1 t2,
    a inS? labels TNull t1 = false ->
    eq (OTuple TNull) t1 t2 ->
    dot TNull t1 a = dot TNull t2 a.
Proof.
  intros.
  generalize H; intros.
  apply tuple_eq in H0 as [H0 _].
  rewrite (Fset.mem_eq_2 _ _ _ H0) in H1.
  apply not_labels_not_find_value in H; apply not_labels_not_find_value in H1.
  unfold dot; simpl; unfold Tuple2.dot.
  rewrite H; rewrite H1; trivial.
Qed.


Lemma labels_dot_eq : forall a t1 t2,
    eq (OTuple TNull) t1 t2 ->
    dot TNull t1 a = dot TNull t2 a.
Proof.
  intros.
  case_eq (a inS? labels TNull t1); intros.
  apply tuple_eq in H as [_ H].
  apply (H _ H0).
  apply (not_labels_dot_eq _ _ _ H0 H).
Qed.

Definition well_formed_db db r :=
  Feset.for_all FesetT (fun x => Fset.equal (A TNull) (labels TNull x) (_basesort TNull db r)) (support_inst db r).

Lemma wf_db_labels_basesort : forall db r t,
    well_formed_db db r = true ->
    t inSE (support_inst db r) ->
    labels TNull t =S= (_basesort TNull db r).
Proof.
  intros.
  unfold well_formed_db in H; rewrite Feset.for_all_spec in H.
  rewrite Feset.mem_elements in H0.
  apply Oeset.mem_bool_true_iff in H0 as [t' [H0 H1]].
  apply tuple_eq in H0 as [H0 _].
  apply (Fset_equal_trans _ _ _ _ H0).
  apply (proj1 (forallb_forall _ _ ) H); trivial.
Qed.

Lemma inst_prov_Krel_inv : forall db r, well_formed_db db r = true -> Krel_inv N.zero TN (instance_prov_ db r).
Proof.
  split; intros.
  unfold support in H0;
  simpl ((let (_, support, _) := instance_prov_ db r in support)) in H0;
  unfold support_inst in H0; rewrite Feset.mem_mk_set in H0;
  rewrite <- Febag.mem_unfold in H0.
  apply Febag.not_mem_nb_occ in H0.
  unfold instance_prov_; simpl; unfold f_inst.
  rewrite H0; trivial.
  apply (wf_db_labels_basesort _ _ _ H H0).
Qed.


Fixpoint delta_f (l : list N) : N :=
  match l with
  |nil => 0%N
  |hd :: tl => if Oeset.eq_bool TN hd 0%N then delta_f tl
               else 1%N
  end.


Fixpoint decompose_list (l : list (NullValues.value * N)) :=
  match l with
  |nil => nil
  |(x,y) :: tl => if Oeset.eq_bool TN y 0%N then decompose_list tl
                  else (repeat (x, 1%N) (BinNatDef.N.to_nat y)) ++ decompose_list tl
  end.


(* count *)
Definition count_prov (l : list (NullValues.value * N)) :=
  fold_left (fun acc x => N.add (snd x) acc) l 0%N.

Fixpoint count_prov2 (l : list (NullValues.value * N)) :=
  match l with
  |nil => 0%N
  |hd::tl => N.add (snd hd) (count_prov2 tl)
  end.

Lemma count_prov_add : forall A l a b,
    fold_left (fun acc (x : A*N) => N.add (snd x) acc) l (a + b) =
    a + fold_left (fun acc x => N.add (snd x) acc) l b.
Proof.
  induction l; intros; trivial.
  simpl. rewrite ! IHl.
  rewrite ! N.add_assoc.
  f_equal.
  apply N.add_comm.
Qed.

Lemma count_prov_eq : forall l,
    count_prov l = count_prov2 l.
Proof.
  induction l; trivial.
  unfold count_prov.
  simpl.
  rewrite count_prov_add.
  now rewrite <- IHl.
Qed.

Lemma count_prov_length : forall l,
    List.forallb (fun x => Oeset.eq_bool TN (snd x) 1%N) l = true -> 
    BinInt.Z.of_N (count_prov l) = BinInt.Z.of_nat (length (fst (split l))).
Proof.
  intros.
  rewrite split_length_l.
  rewrite count_prov_eq.
  induction l; trivial.
  simpl.
  rewrite Znat.Zpos_P_of_succ_nat.
  rewrite <- IHl.
  apply (proj1 (forallb_forall _ _ )) with a in H.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H.
  rewrite H.
  rewrite <- Znat.N2Z.inj_succ.
  f_equal.
  apply N.add_1_l.
  now left.
  simpl in H.
  now apply andb_true_iff in H.
Qed.

Lemma count_prov_length2 : forall l,
    List.forallb (fun x => Oeset.eq_bool TN (snd x) 1%N) l = true -> 
    count_prov2 l = N.of_nat (length (fst (split l))).
Proof.
  intros.
  rewrite <- count_prov_eq.
  apply Znat.N2Z.inj.
  rewrite count_prov_length; trivial.
  now rewrite Znat.nat_N_Z.
Qed.

Lemma count_prov_app : forall l1 l2,
    count_prov2 (l1++l2) = N.add (count_prov2 l1) (count_prov2 l2).
Proof.
  induction l1; trivial; intros.
  simpl.
  rewrite IHl1.
  apply N.add_assoc.
Qed.

(* sum *)
Definition sum_prov (l : list (NullValues.value * N)) :=
  fold_left (fun acc x =>
               match fst x with
               | NullValues.Value_Z (Some x0) =>
                 BinInt.Z.add acc (BinInt.Z.mul x0 (BinInt.Z.of_N (snd x)))
               | _ => acc
               end) l 0%Z.

Fixpoint sum_prov2 (l : list (NullValues.value * N)) :=
  match l with
  |nil => 0%Z
  |h::tl =>
   match fst h with
   | NullValues.Value_Z (Some x0) =>
     BinInt.Z.add (BinInt.Z.mul x0 (BinInt.Z.of_N (snd h))) (sum_prov2 tl)
   |_=> sum_prov2 tl
   end
  end.

Lemma sum_prov_sum : forall l,
    forallb (fun x : NullValues.value * N => Oeset.eq_bool TN (snd x) 1) l = true ->
    sum_prov l =
    fold_left
      (fun (acc : Z) (x : NullValues.value) =>
         match x with
         | NullValues.Value_Z (Some x0) => BinInt.Z.add acc x0
         | _ => acc
         end) (fst (split l)) 0%Z.
Proof.
  intros.
  unfold sum_prov.
  generalize 0%Z.  
  induction l; intros; trivial.
  simpl.
  destruct a.
  assert (fst (let (left, right) := split l in (v :: left, n :: right)) =  v :: (fst (split l))).
  destruct (split l); trivial.
  rewrite H0. clear H0.
  simpl.
  rewrite IHl.
  f_equal.
  destruct v; trivial.
  destruct o; trivial.
  f_equal.
  apply (proj1 (forallb_forall _ _ )) with (NullValues.Value_Z (Some z0), n) in H.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H.
  simpl in H. rewrite H.
  simpl.
  apply BinInt.Z.mul_1_r.
  now left.
  simpl in H.
  now apply andb_true_iff in H.
Qed.

Lemma sum_prov_add : forall l a,
     fold_left
    (fun (acc : Z) (x : NullValues.value * N) =>
     match fst x with
     | NullValues.Value_Z (Some x0) => BinInt.Z.add acc (BinInt.Z.mul x0 (BinInt.Z.of_N (snd x)))
     | _ => acc
     end) l a =
  BinInt.Z.add a (sum_prov l).
Proof.
  induction l; intros; simpl.
  symmetry. apply BinInt.Z.add_0_r.
  unfold sum_prov; simpl.
  destruct a; destruct v; destruct o; trivial.
  simpl.
  rewrite ! IHl.
  now rewrite BinInt.Z.add_assoc.
Qed.

Lemma sum_prov_eq : forall l,
    sum_prov l = sum_prov2 l.
Proof.
  induction l; trivial.
  unfold sum_prov. simpl.
  rewrite <- IHl.
  destruct a; destruct v; destruct o; trivial.
  apply sum_prov_add.
Qed.

Lemma sum_prov_app : forall l1 l2,
    sum_prov2 (l1++l2) = BinInt.Z.add (sum_prov2 l1) (sum_prov2 l2).
Proof.
  induction l1; intros; trivial.
  simpl.
  rewrite ! IHl1.
  destruct a; destruct v; destruct o; trivial.
  simpl. now rewrite BinInt.Z.add_assoc.
Qed.

Lemma sum_prov_repeat_1 : forall a n,
    sum_prov2 (repeat (NullValues.Value_string a, 1) n) = 0%Z.
Proof.
  induction n; trivial.
Qed.

Lemma sum_prov_repeat_2 : forall a n,
    sum_prov2 (repeat (NullValues.Value_bool a, 1) n) = 0%Z.
Proof.
  induction n; trivial.
Qed.

Lemma sum_prov_repeat_3 : forall n,
    sum_prov2 (repeat (NullValues.Value_Z None, 1) n) = 0%Z.
Proof.
  induction n; trivial.
Qed.

Lemma sum_prov_repeat_4 : forall n a,
    sum_prov2 (repeat (NullValues.Value_Z (Some a), 1) n) = BinInt.Z.mul a (BinInt.Z.of_nat n).
Proof.
  induction n; intros.
  now rewrite BinInt.Z.mul_0_r.
  simpl.
  rewrite IHn.
  rewrite BinInt.Z.mul_1_r.
  rewrite BinInt.Z.add_comm.
  rewrite BinInt.Z.mul_comm.
  rewrite BinInt.Zmult_succ_l_reverse.
  rewrite BinInt.Z.mul_comm.
  f_equal.
  now rewrite <- Znat.Nat2Z.inj_succ.
Qed.

(*avg*)
Definition avg_prov l :=
  let sum := sum_prov l in
  BinInt.Z.div sum (BinInt.Z.of_N (count_prov l)).



(* (*max*) *)
Fixpoint max_prov_aux (l : list (NullValues.value * N)) acc :=
  match l with
  |nil => acc
  |(a,n)::tl =>
   if Oeset.eq_bool TN n 0%N then max_prov_aux tl acc
   else
   match a with
   |NullValues.Value_Z (Some _) => match Oset.compare NullValues.OVal acc a with
                  |Gt => max_prov_aux tl acc
                  |_ => max_prov_aux tl a
                  end
   |_ => NullValues.Value_Z (None)
   end
  end.

Fixpoint max_prov (l : list (NullValues.value * N)) :=
  match l with
  |nil => NullValues.Value_Z (None)
  |(a,n)::tl =>
      if Oeset.eq_bool TN n 0%N then max_prov tl
      else match a with
            |NullValues.Value_Z (Some _) => max_prov_aux tl a
            |_ => NullValues.Value_Z (None)
            end
  end.

  
Lemma max_prov_aux_max_aux : forall l a,
    forallb (fun x : NullValues.value * N => Oeset.eq_bool TN (snd x) 1) l = true ->
    max_prov_aux l a = NullValues.max_aux (fst (split l)) a.
Proof.
  induction l; intros; trivial.
  simpl.
  destruct a.
  assert (forall (l' : list (NullValues.value * N)) v' n', fst (let (left, right) := split l' in (v' :: left, n' :: right)) =  v' :: (fst (split l'))).
  intros; destruct (split l'); trivial.
  rewrite H0.
  apply (proj1 (forallb_forall _ _)) with (v,n) in H as H2.
  apply Oeset.eq_bool_true_compare_eq in H2.
  apply N.compare_eq_iff in H2.
  simpl in H2. rewrite H2.
  simpl. simpl in H; apply andb_true_iff in H as [_ H].
  now rewrite ! IHl.
  now left.
Qed.
  

Lemma max_prov_max : forall l,
    forallb (fun x : NullValues.value * N => Oeset.eq_bool TN (snd x) 1) l = true ->
    max_prov l = NullValues.max (fst (split l)).
Proof.
  induction l; intros; trivial.
  simpl.
  destruct a.
  apply (proj1 (forallb_forall _ _)) with (v,n) in H as H2.
  apply Oeset.eq_bool_true_compare_eq in H2.
  apply N.compare_eq_iff in H2.
  simpl in H2. rewrite H2.
  simpl.
  assert (forall (l' : list (NullValues.value * N)) v' n', fst (let (left, right) := split l' in (v' :: left, n' :: right)) =  v' :: (fst (split l'))).
  intros; destruct (split l'); trivial.
  rewrite H0.
  simpl in H; apply andb_true_iff in H as [_ H].
  now rewrite max_prov_aux_max_aux.
  now left.
Qed.





(*interpretation of aggregates*)
Definition interp_aggregate_prov (a : Values.aggregate) (l : list (NullValues.value * N)) :=
  match a with
  | Values.Aggregate s =>
    if NullValues.comparison_case (string_compare s "count")
    then NullValues.Value_Z (Some (BinInt.Z.of_N (count_prov l)))
    else
      if NullValues.comparison_case (string_compare s "sum")
      then
        NullValues.Value_Z (Some (sum_prov l))
      else
        if NullValues.comparison_case (string_compare s "avg")
        then
          NullValues.Value_Z
            (Some (avg_prov l))
        else if NullValues.comparison_case (string_compare s "max")
             then max_prov l
             else NullValues.Value_Z None
  end.

Notation eval_query_prov_Null db :=
  (@eval_query_prov_k TNull relname unknown3 contains_nulls N 0%N N.one N.add N.mul TN (instance_prov_ db) interp_aggregate_prov delta_f).
Notation eval_query_rel_Null db :=
  (SqlAlgebra.eval_query (_basesort TNull db) (instance_rel_ db) unknown3 contains_nulls).

Lemma interp_aggregate_prov_prop1 : forall a l,
    List.forallb (fun x => Oeset.eq_bool TN (snd x) 1%N) l = true -> 
    interp_aggregate_prov a l = interp_aggregate TNull a (fst(List.split l)).
Proof.
  intros.
  destruct a.
  simpl.
  case_eq (NullValues.comparison_case (string_compare s "count")); intros.
  now rewrite count_prov_length.
  case_eq (NullValues.comparison_case (string_compare s "sum")); intros.
  now rewrite sum_prov_sum.
  case_eq (NullValues.comparison_case (string_compare s "avg")); intros; trivial.
  unfold avg_prov.
  rewrite <- count_prov_length; trivial.
  now rewrite sum_prov_sum.
  case_eq (NullValues.comparison_case (string_compare s "max")); intros; trivial.
  now apply max_prov_max.
Qed.

Lemma decompose_count : forall l,
    count_prov l = count_prov (decompose_list l).
Proof.
  intros.
  rewrite ! count_prov_eq.
  induction l; intros; trivial.
  simpl.
  destruct a.
  case_eq (Oeset.eq_bool TN n 0); intros.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H.
  now rewrite H.
  rewrite count_prov_app.
  rewrite IHl.
  f_equal.
  rewrite count_prov_length2.
  rewrite split_length_l.
  rewrite repeat_length.
  now rewrite N2Nat.id.
  apply forallb_forall; intros.
  apply repeat_spec in H0.
  rewrite H0.
  apply Oeset.eq_bool_refl.
Qed.

Lemma decompose_sum : forall l,
    sum_prov l = sum_prov (decompose_list l).
Proof.
  intros.
  rewrite ! sum_prov_eq.
  induction l; intros; trivial.
  simpl.
  destruct a.
  case_eq (Oeset.eq_bool TN n 0); intros.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H.
  rewrite H.
  simpl.
  destruct v; destruct o; trivial.
  now rewrite BinInt.Z.mul_0_r.
  rewrite sum_prov_app.
  case_eq v; simpl; intros.
  now rewrite sum_prov_repeat_1.
  destruct o; trivial.
  rewrite sum_prov_repeat_4.  
  rewrite IHl.
  f_equal.
  now rewrite Znat.N_nat_Z.
  now rewrite sum_prov_repeat_3.  
  now rewrite sum_prov_repeat_2.
Qed.

Lemma max_prov_aux_app : forall l1 l2 a,
    max_prov_aux (l1++l2) (NullValues.Value_Z (Some a)) =
    match max_prov_aux l1 (NullValues.Value_Z (Some a)) with
    |NullValues.Value_Z (Some b) => max_prov_aux l2 (NullValues.Value_Z (Some b))
    |_ => NullValues.Value_Z None
    end.
Proof.
  induction l1; intros; trivial.
  simpl. destruct a.
  destruct (Oeset.eq_bool TN n 0); intros.
  apply IHl1.
  destruct v; trivial.
  destruct o; trivial.
  destruct (BinInt.Z.compare a0 z); apply IHl1.
Qed.


Lemma max_prov_aux_repeat_value_z : forall p a,
    max_prov_aux (repeat (NullValues.Value_Z (Some a), 1) p) (NullValues.Value_Z (Some a)) = NullValues.Value_Z (Some a).
Proof.
  induction p; intros; trivial.
  simpl. rewrite BinInt.Z.compare_refl.
  now apply IHp.
Qed.

Lemma max_prov_aux_repeat_value_z_2 : forall p a z,
    BinInt.Z.compare z a = Gt ->
    max_prov_aux (repeat (NullValues.Value_Z (Some a), 1) p) (NullValues.Value_Z (Some z)) = NullValues.Value_Z (Some z).
Proof.
  induction p; intros; trivial.
  simpl. rewrite H.
  now apply IHp.
Qed.

Lemma max_prov_aux_repeat : forall l z,
    max_prov_aux l (NullValues.Value_Z (Some z)) = max_prov_aux (decompose_list l) (NullValues.Value_Z (Some z)).
Proof.
  induction l; intros; trivial.
  simpl.
  destruct a.
  case_eq (Oeset.eq_bool TN n 0); intros; trivial.
  case_eq v; intros.
  rewrite <- (N2Nat.id n) in H.
  destruct (BinNatDef.N.to_nat n); try solve [inversion H|trivial].
  rewrite <- (N2Nat.id n) in H.
  destruct (BinNatDef.N.to_nat n); try solve [inversion H].
  simpl.
  destruct o; trivial.
  case_eq (BinInt.Z.compare z z0); intros; rewrite max_prov_aux_app; try solve [rewrite max_prov_aux_repeat_value_z; apply IHl].
  rewrite max_prov_aux_repeat_value_z_2; trivial.
  rewrite <- (N2Nat.id n) in H.
  destruct (BinNatDef.N.to_nat n); try solve [inversion H|trivial].
Qed.

Lemma decompose_max : forall l,
    max_prov l = max_prov (decompose_list l).
Proof.
  induction l; intros; trivial.
  simpl.
  destruct a.
  case_eq (Oeset.eq_bool TN n 0); intros; trivial.
  case_eq v; intros.
  rewrite <- (N2Nat.id n) in H.
  destruct (BinNatDef.N.to_nat n); try solve [inversion H|trivial].
  rewrite <- (N2Nat.id n) in H.
  destruct (BinNatDef.N.to_nat n); try solve [inversion H].
  simpl.
  destruct o; trivial.
  rewrite max_prov_aux_app.
  rewrite max_prov_aux_repeat_value_z.
  apply max_prov_aux_repeat.
  rewrite <- (N2Nat.id n) in H.
  destruct (BinNatDef.N.to_nat n); try solve [inversion H|trivial].
Qed.

Lemma decompose_interp_aggregate : forall a l,
     interp_aggregate_prov a l =  interp_aggregate_prov a (decompose_list l).
Proof.
  destruct a. simpl.
  case_eq (NullValues.comparison_case (string_compare s "count")); intros.
  now rewrite decompose_count.
  case_eq (NullValues.comparison_case (string_compare s "sum")); intros.
  now rewrite decompose_sum.
  case_eq (NullValues.comparison_case (string_compare s "avg")); intros; trivial.
  unfold avg_prov.
  rewrite decompose_count.
  now rewrite decompose_sum.
  case_eq (NullValues.comparison_case (string_compare s "max")); intros; trivial.
  now rewrite decompose_max.
Qed.

Lemma filter_sum_not_zero : forall t l,
    filter_sum TNull t l <> 0%N -> exists a, In (t,a) l.
Proof.
  intros.
  induction l.
  now destruct H.
  simpl.
  simpl in H.
  case_eq (Oset.eq_bool NullValues.OVal (fst a) t); intros.
  apply Oset.eq_bool_true_iff in H0.
  rewrite <- H0.
  exists (snd a).
  destruct a; now left.
  rewrite if_eq_false in H; trivial.
  apply IHl in H as [a0 H].
  exists a0; now right.
Qed.

Lemma filter_sum_app : forall t l1 l2,
    filter_sum TNull t (l1++l2) = filter_sum TNull t l1 + filter_sum TNull t l2.
Proof.
  induction l1; intros; trivial.
  simpl.
  rewrite IHl1.
  apply N.add_assoc.
Qed.

               
Lemma filter_sum_permut : forall l1 l2,
    List.forallb (fun x => Oeset.eq_bool TN (snd x) 1%N) l1 = true -> 
    List.forallb (fun x => Oeset.eq_bool TN (snd x) 1%N) l2 = true -> 
    (forall t, filter_sum TNull t l1 = filter_sum TNull t l2) ->
    _permut (fun a1 a2 => a1 = a2) l1 l2.
Proof.
  induction l1; intros.
  - destruct l2.
    apply Pnil.
    simpl in H1.
    specialize (H1 (fst p)).
    rewrite Oset.eq_bool_refl in H1.
    apply (proj1 (forallb_forall _ _)) with p in H0.
    apply Oeset.eq_bool_true_compare_eq in H0.
    apply N.compare_eq_iff in H0.
    symmetry in H1.
    destruct (Nadd_1_diff_0 (filter_sum TNull (fst p) l2)).
    now rewrite <- H0.
    now left.
  - simpl in H1.
    generalize H; intros.
    apply (proj1 (forallb_forall _ _)) with a in H2; try now left.
    apply Oeset.eq_bool_true_compare_eq in H2.
    apply N.compare_eq_iff in H2.
    assert (In a l2).
    destruct (filter_sum_not_zero (fst a) l2).
    rewrite <- H1.
    rewrite Oset.eq_bool_refl.
    pose (Nadd_1_diff_0 (filter_sum TNull (fst a) l1)).
    now rewrite <- H2 in n.
    apply (proj1 (forallb_forall _ _)) with (fst a, x) in H0; trivial.
    apply Oeset.eq_bool_true_compare_eq in H0.
    apply N.compare_eq_iff in H0.
    destruct a.
    simpl in H0. simpl in H2.
    rewrite H0 in H3; now rewrite H2.
    apply in_split in H3 as [l3 [l4 H3]].
    rewrite H3.
    apply Pcons; trivial.
    apply IHl1.
    simpl in H; now apply andb_true_iff in H as [_ H].
    rewrite H3 in H0.
    rewrite forallb_app.
    rewrite forallb_app in H0.
    simpl in H0.
    apply andb_true_iff.
    split; repeat apply andb_true_iff in H0 as [H0a H0]; trivial.
    now apply andb_true_iff in H0 as [_ H0].
    intros.
    rewrite H3 in H1.
    specialize (H1 t).
    rewrite filter_sum_app in H1.
    simpl in H1.
    rewrite (N.add_comm (filter_sum TNull t l3)) in H1.
    rewrite <- N.add_assoc in H1.
    apply Nplus_reg_l in H1.
    rewrite N.add_comm in H1.
    now rewrite filter_sum_app.
Qed.


Lemma count_permut : forall l1 l2,
    _permut (fun a1 a2 => a1 = a2) l1 l2 ->
    count_prov l1 = count_prov l2.
Proof.
  induction l1; intros; inversion H; trivial.
  rewrite ! count_prov_eq.
  rewrite (cons_app_nil b).
  rewrite 2 count_prov_app.
  rewrite N.add_comm.
  rewrite H2.
  simpl.
  rewrite <- ! N.add_assoc.
  f_equal.
  simpl.
  rewrite N.add_comm.
  rewrite <- count_prov_app.
  rewrite <-! count_prov_eq.
  now apply IHl1.
Qed.

Lemma sum_permut : forall l1 l2,
    _permut (fun a1 a2 => a1 = a2) l1 l2 ->
    sum_prov l1 = sum_prov l2.
Proof.
  induction l1; intros; inversion H; trivial.
  rewrite ! sum_prov_eq.
  rewrite (cons_app_nil b).
  rewrite 2 sum_prov_app.
  rewrite BinInt.Z.add_comm.
  rewrite H2.
  rewrite <- ! BinInt.Z.add_assoc.
  rewrite (BinInt.Z.add_comm (sum_prov2 l3)).
  simpl.
  f_equal.
  rewrite BinInt.Z.add_comm.
  rewrite <- sum_prov_app.
  rewrite <-! sum_prov_eq.
  rewrite (IHl1 (l0++l3)); trivial.
  destruct (fst b); try now rewrite BinInt.Z.add_0_r.
  destruct o; rewrite BinInt.Z.add_0_r; trivial.
  apply BinInt.Z.add_comm.
Qed.

Lemma max_permut_one_elt_0 : forall l1 l2 z v n,
    Oeset.eq_bool TN n 0 = true ->
    max_prov_aux (l1 ++ (v, n) :: l2) (NullValues.Value_Z (Some z)) =
    max_prov_aux (l1 ++ l2) (NullValues.Value_Z (Some z)).
Proof.
  induction l1; intros.
  simpl.
  now rewrite H.
  simpl. destruct a.
  case_eq (Oeset.eq_bool TN n0 0); intros; trivial.
  now apply IHl1.
  destruct v0; trivial.
  destruct o; trivial.
  now rewrite ! IHl1.
Qed.


Lemma max_elt_not_Z_some : forall l1 l2 a n z,
    (forall v, a <> NullValues.Value_Z (Some v)) ->
    Oeset.eq_bool TN n 0 = false ->
    max_prov_aux (l1 ++ (a,n) :: l2) (NullValues.Value_Z (Some z)) =
    NullValues.Value_Z None.
Proof.
  induction l1; intros; simpl.
  rewrite H0.
  destruct a; trivial.
  destruct o; trivial.
  now destruct (H z0).
  destruct a.
  destruct (Oeset.eq_bool TN n0 0).
  now apply IHl1.
  destruct v; destruct o; trivial.
  destruct (BinInt.Z.compare z z0); now apply IHl1.
Qed.


Lemma max_aux_permut_one_elt : forall l1 l2 a z,
    max_prov_aux (l1 ++ a :: l2) (NullValues.Value_Z (Some z)) = max_prov_aux (a :: l1 ++ l2) (NullValues.Value_Z (Some z)).
Proof.
  induction l1; intros; trivial.
  simpl.
  destruct a; destruct a0; rewrite ! IHl1; simpl.
  case_eq (Oeset.eq_bool TN n 0); case_eq (Oeset.eq_bool TN n0 0); intros; trivial.
  - destruct v; destruct o; trivial.
    destruct (BinInt.Z.compare z z0); trivial; rewrite IHl1; simpl; now rewrite H.
  - destruct v; destruct v0; trivial.
    * destruct o0; destruct o; trivial.
    now destruct (BinInt.Z.compare z z0).
    now destruct (BinInt.Z.compare z z0).
    * destruct o; trivial.
      destruct (BinInt.Z.compare z z0); trivial; rewrite IHl1; simpl; now rewrite H.
    * destruct o0; destruct o; trivial; try rewrite ! IHl1; simpl; try rewrite H; try solve [destruct (BinInt.Z.compare z z0); trivial].
      -- rewrite (BinInt.Z.compare_antisym z1 z0).
         rewrite (BinInt.Z.compare_antisym z1 z).
         case_eq (BinInt.Z.compare z1 z); case_eq (BinInt.Z.compare z z0); case_eq (BinInt.Z.compare z1 z0); intros; simpl; trivial; try solve [apply BinInt.Z.compare_eq in H1; now rewrite H1].
         apply BinInt.Z.compare_eq in H3.
         rewrite H3 in H1.
         rewrite H1 in H2.
         inversion H2.
         apply BinInt.Z.compare_eq in H2.
         rewrite H2 in H3.
         rewrite H1 in H3.
         inversion H3.
         rewrite (Zcompare.Zcompare_Lt_trans _ _ _ H3 H2) in H1.
         inversion H1.
         rewrite (Zcompare.Zcompare_Gt_trans _ _ _ H3 H2) in H1.
         inversion H1.
    * destruct o; trivial.
      rewrite IHl1; simpl.
      rewrite H; destruct (BinInt.Z.compare z z0); trivial.
    * destruct o0; trivial. destruct (BinInt.Z.compare z z0); trivial.
Qed.      




Lemma max_aux_permut : forall l1 l2 z,
    _permut (fun a1 a2 : NullValues.value * N => a1 = a2) l1 l2 ->
    max_prov_aux l1 (NullValues.Value_Z (Some z)) = max_prov_aux l2 (NullValues.Value_Z (Some z)).
Proof.
  induction l1; intros; inversion H; trivial.
  rewrite max_aux_permut_one_elt.
  rewrite H2.
  simpl.
  destruct b; rewrite ! (IHl1 (l0++l3)); trivial.
  destruct (Oeset.eq_bool TN n 0); trivial.
  destruct v; destruct o; trivial.
  now rewrite ! (IHl1 (l0++l3)).  
Qed.

Lemma max_permut_one_elt : forall l1 l2 a,
    max_prov (l1 ++ a :: l2) = max_prov (a :: l1 ++ l2).
Proof.
  induction l1; intros; trivial.
  simpl.
  destruct a; destruct a0.
  case_eq (Oeset.eq_bool TN n 0); case_eq (Oeset.eq_bool TN n0 0); intros; try solve [rewrite IHl1; simpl; now rewrite H].
  destruct v; destruct o; trivial.
  rewrite max_aux_permut_one_elt.
  simpl.
  now rewrite H.
  destruct v; destruct o; trivial; try solve [destruct v0; now destruct o].
  destruct v0; rewrite max_aux_permut_one_elt; simpl; rewrite H; trivial.
  destruct o; trivial.
  simpl; rewrite BinInt.Z.compare_antisym.
  case_eq (BinInt.Z.compare z0 z); intros; trivial.
  apply BinInt.Z.compare_eq in H1.
  now rewrite H1.
Qed.

Lemma max_permut : forall l1 l2,
    _permut (fun a1 a2 : NullValues.value * N => a1 = a2) l1 l2 ->
    max_prov l1 = max_prov l2.
Proof.
  induction l1; intros; inversion H; trivial.
  simpl.
  destruct a.
  rewrite <- H2.
  rewrite max_permut_one_elt.
  simpl.
  case_eq (Oeset.eq_bool TN n 0); intros.
  apply (IHl1 (l0++l3)); trivial.
  destruct v; destruct o; trivial.
  now apply max_aux_permut.
Qed.

Lemma interp_aggregate_prov_permut : forall l1 l2 a,
    _permut (fun a1 a2 => a1 = a2) l1 l2 ->
    interp_aggregate_prov a l1 = interp_aggregate_prov a l2.
Proof.
  intros.
  destruct a.
  simpl.
  case_eq (NullValues.comparison_case (string_compare s "count")); intros.
  now rewrite (count_permut H).
  case_eq (NullValues.comparison_case (string_compare s "sum")); intros.
  now rewrite (sum_permut H).
  case_eq (NullValues.comparison_case (string_compare s "avg")); intros; trivial.
  unfold avg_prov. rewrite (count_permut H).
  now rewrite (sum_permut H). 
  case_eq (NullValues.comparison_case (string_compare s "max")); intros; trivial.
  now rewrite (max_permut H). 
Qed.


Lemma decompose_filter_sum : forall t l,
    filter_sum TNull t l = filter_sum TNull t (decompose_list l).
Proof.
  induction l; intros; trivial.
  simpl.
  destruct a.
  case_eq (Oeset.eq_bool TN n 0); intros.
  apply Oeset.eq_bool_true_compare_eq in H.
  apply N.compare_eq_iff in H.
  rewrite H.
  now rewrite if_same.
  rewrite filter_sum_app.
  rewrite IHl.
  f_equal.
  clear H IHl.
  symmetry.
  rewrite <- (N2Nat.id n) at 3.
  simpl.
  induction (BinNatDef.N.to_nat n).
  now rewrite if_same.
  simpl.
  rewrite IHn0.
  destruct (Oset.eq_bool NullValues.OVal v t); trivial.
  rewrite N.add_1_l.
  now rewrite <- Nat2N.inj_succ.
Qed.

Lemma decompose_forall_1 : forall l,
    forallb (fun x : NullValues.value * N => Oeset.eq_bool TN (snd x) 1) (decompose_list l) = true.
Proof.
  induction l; intros; trivial.
  simpl.
  destruct a.
  destruct (Oeset.eq_bool TN n 0); trivial.
  rewrite forallb_app.
  rewrite IHl.
  rewrite andb_true_r.
  apply forallb_forall; intros.
  apply repeat_spec in H.
  rewrite H.
  apply Oeset.eq_bool_refl.
Qed.

Lemma interp_aggregate_prov_prop2 : forall a l1 l2,
    (forall t, filter_sum TNull t l1 = filter_sum TNull t l2) ->
    interp_aggregate_prov a l1 = interp_aggregate_prov a l2.
Proof.
  intros.
  rewrite decompose_interp_aggregate.
  rewrite (decompose_interp_aggregate _ l2).
  apply interp_aggregate_prov_permut.
  apply filter_sum_permut; try apply decompose_forall_1.
  intros.
  now rewrite <- ! decompose_filter_sum.
Qed.

Lemma K_relations_extend_relational_algebra_inst : forall b db,
    (forall r, well_formed_db db r = true) ->
    forall env (q : SqlAlgebra.query TNull relname),
      well_formed (_basesort TNull db) env b q = true ->
      forall t : tuple TNull,
        f (eval_query_prov_Null db (create_env env) q) t =
        Febag.nb_occ BTupleTNull t (eval_query_rel_Null db env q).
Proof.
  intros b db H env q.
  apply K_relations_extend_relational_algebra; trivial. apply contains_nulls_eq.
  intros. apply (inst_prov_Krel_inv _ _ (H r)).
  apply interp_aggregate_prov_prop1.
  apply interp_aggregate_prov_prop2.
Qed.
